package models;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

/**
 * @author HanusaCloud on 12/11/2017
 */
@Table(name = "auto_sequence")
public class AutoSequence extends BaseTable {

    public static final int LENGTH = 9;

    @Id
    public Long id;

    public String getSequence() {
        this.id = (long) save();
        int dif = LENGTH - String.valueOf(this.id).length();
        return dif > 0 ? String.format("%09d", this.id) : String.valueOf(this.id);
    }

}
