package models.elasticsearch;

import models.elasticsearch.product.ProductCategoryElastic;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 9/29/2017
 */
public class KategoriElastic {

    public long currentId = 0;
    public long levelOneId = 0;
    public String levelOneName = "";
    public long levelTwoId = 0;
    public String levelTwoName = "";
    public long levelThreeId = 0;
    public String levelThreeName = "";
    public long levelFourId = 0;
    public String levelFourName = "";
    public long levelFiveId = 0;
    public String levelFiveName = "";

    public List<ProductCategoryElastic> getCategories() {
        List<ProductCategoryElastic> results = new ArrayList<>();
        results.add(new ProductCategoryElastic(levelOneId, levelOneName));
        if (isLevelTwoExists()) {
            results.add(new ProductCategoryElastic(levelTwoId, levelTwoName));
        }
        if (isLevelThreeExists()) {
            results.add(new ProductCategoryElastic(levelThreeId, levelThreeName));
        }
        if (isLevelFourExists()) {
            results.add(new ProductCategoryElastic(levelFourId, levelFourName));
        }
        if (isLevelFiveExists()) {
            results.add(new ProductCategoryElastic(levelFiveId, levelFiveName));
        }
        return results;
    }

    public boolean isLevelTwoExists() {
        return levelTwoId > 0;
    }

    public boolean isLevelThreeExists() {
        return levelThreeId > 0;
    }

    public boolean isLevelFourExists() {
        return levelFourId > 0;
    }

    public boolean isLevelFiveExists() {
        return levelFiveId > 0;
    }

}
