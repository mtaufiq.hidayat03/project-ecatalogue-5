package unit;

import models.prakatalog.Usulan;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import services.prakatalog.UsulanService;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class isNegotiationStageTest {
    public Usulan usulan;
    public Boolean expectation;

    public isNegotiationStageTest(Usulan usulan, Boolean expectation) {
        this.usulan = usulan;
        this.expectation = expectation;
    }

    @Test
    public void checkIsNegotiationStageTest() throws ParseException {
        assertEquals(
                expectation,
                UsulanService.isNegotiation(usulan)
        );
    }

    @Parameterized.Parameters
    public static Collection negotiationParameters() {
        return Arrays.asList(
                new Object[][]{
                        {
                                generateUsulan(239L),
                                true
                        },
                        {
                                generateUsulan(238L),
                                false
                        },
                }
        );
    }

    private static Usulan generateUsulan(Long id) {
        Usulan usulan = new Usulan();
        usulan.id = id;
        return usulan;
    }
}
