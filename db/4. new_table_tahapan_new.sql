-- Tabel ini dibuat untuk menggantikan tabel tahapan yang telah ada sebelumnya
-- TO DO : Migrasi data tabel tahapan lama ke baru, ubah nama tabel tahapan_new menjadi tahapan

CREATE TABLE `tahapan_new` (
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`nama_tahapan` VARCHAR(255) NOT NULL,
	`jenis_tahapan` CHAR(1) NOT NULL,
	`apakah_fitur_paten` INT(1) NULL,
	`created_date` DateTime NULL,
	`created_by` Int( 5 ) NULL,
	`modified_date` DateTime NULL,
	`modified_by` Int( 5 ) NULL,
	`deleted_date` DateTime NULL,
	`deleted_by` Int( 5 ) NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`auditdate` Timestamp NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`active` INT(1) NULL,
	PRIMARY KEY (`id`)
)
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;