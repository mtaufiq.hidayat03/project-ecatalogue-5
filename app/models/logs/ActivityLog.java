package models.logs;

import java.util.Date;

import javax.persistence.PersistenceUnit;

import com.google.gson.Gson;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

@Table(name="activity_log", schema="log")
@PersistenceUnit(name="log")
public class ActivityLog extends BaseTable {
	@Id(sequence="log.seq_activity_log")
	public Long log_id;
	public Date auditupdate;
	public Activity activity;
	public String data;
	public int user_id;
	public String class_name;
	private ActivityLog()
	{
		
	}
	
	public static void add(int user_id, Activity activity, Object data)
	{
		ActivityLog log= new ActivityLog();
		log.activity=activity;
		log.auditupdate=new Date();
		if(data!=null)
		{
			log.class_name=data.getClass().getName();
			Gson gson=new Gson();
			log.data=gson.toJson(data);
		}
		
		LogPersister.add(log);
	}
}
