package repositories.produkkategori;

import static constants.models.BaseConstant.ACTIVE;
import static constants.models.ProdukKategoriConstant.*;

import constants.models.ProdukKategoriAtributConstant;
import models.BaseKatalogTable;
import models.katalog.ProdukKategori;
import models.katalog.payload.ProdukKategoriPayload;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.cache.Cache;
import play.db.jdbc.Query;
import utils.LogUtil;

import java.util.*;
import java.util.stream.Collectors;

public class ProdukKategoriRepository {

	public static final String TAG = "ProdukKategoriRepository";
	public static final String CACHE_KEY = "cache_category_key_";

	private static List<ProdukKategori> getList(long komoditasId, String parentIds) {
		String parentFilter = " AND pk." + PARENT_ID + " = 0";
		String level = " AND pk." + LEVEL_KATEGORI + " = 0";
		if (!TextUtils.isEmpty(parentIds)) {
			parentFilter = " AND pk."+ PARENT_ID +" IN (" + parentIds + ")";
			level = "";
		}
		final String query = "SELECT pk." + ID
				+ ", pk." + PARENT_ID
				+ ", pk." + NAMA_KATEGORI
				+ ", pk." + POSISI_ITEM
				+ ", pk." + LEVEL_KATEGORI
				+ ", pka.total_kategori_atribut"
				+ ", pk." + KOMODITAS_ID
				+ ", (SELECT count(*)"
				+ " FROM " + TABLE_NAME
				+ " WHERE " + ACTIVE + " > 0 AND " + PARENT_ID + " = pk." + ID + ") total_produk_kategori "
				+ " FROM " + TABLE_NAME + " pk"
				+ " LEFT JOIN (SELECT COUNT(*) as total_kategori_atribut"
				+ ", " + ProdukKategoriAtributConstant.PRODUK_KATEGORI_ID
				+ " FROM " + ProdukKategoriAtributConstant.TABLE_NAME
				+ " WHERE " + ACTIVE + " > 0" +
				" GROUP BY " + ProdukKategoriAtributConstant.PRODUK_KATEGORI_ID + ") pka" +
				" ON pka." + ProdukKategoriAtributConstant.PRODUK_KATEGORI_ID + " = pk." + ID
				+ " WHERE pk." + KOMODITAS_ID + " =?" +
				" AND pk." + ACTIVE + " >= 1" +
				level +
				parentFilter +
				" ORDER BY " + NAMA_KATEGORI + " ASC";
		Logger.debug(TAG + " " + query);
		return Query.find(query, ProdukKategori.class, komoditasId).fetch();
	}

	/**
	 * @param komoditasId komoditas id
	 * @return produk kategori list with children max 5 level*/
	public static List<ProdukKategori> getProdukKategoriList(long komoditasId) {
		List<ProdukKategori> results;
		final String key = CACHE_KEY + komoditasId;
		if (isCacheNotExists(key)) {
			Logger.debug(TAG + " get from database");
			results = getList(komoditasId, "");
			Logger.debug(TAG + " level zero :" + results.size());

			List<ProdukKategori> levelOne = getLevel(results, 0);
			setCategoriesChildren(levelOne, komoditasId);
			Logger.debug(TAG + " ProdukKategori level one :" + levelOne.size());

			List<ProdukKategori> levelTwo = getLevel(levelOne, 1);
			setCategoriesChildren(levelTwo, komoditasId);
			Logger.debug(TAG + " ProdukKategori level two :" + levelTwo.size());

			List<ProdukKategori> levelThree = getLevel(levelTwo, 2);
			setCategoriesChildren(levelThree, komoditasId);
			Logger.debug(TAG + " ProdukKategori level three :" + levelThree.size());

			List<ProdukKategori> levelfour = getLevel(levelThree, 3);
			setCategoriesChildren(levelfour, komoditasId);
			Logger.debug(TAG + " ProdukKategori level four :" + levelfour.size());
			Cache.set(key, results, "10min");
		} else {
			Logger.debug(TAG + " get from cache");
			results = getFromCache(key);
		}
		return results;
	}

	public static List<ProdukKategori> getLevel(List<ProdukKategori> list, final int level) {
		if (level >= 1) {
			return list.stream()
					.filter(p -> p.total_produk_kategori > 0)
					.flatMap(p -> p.children.stream()
							.filter(e -> e.total_produk_kategori > 0))
					.collect(Collectors.<ProdukKategori>toList());
		}

		return list.stream()
				.filter(e -> e.total_produk_kategori > 0
						&& e.level_kategori == level)
				.collect(Collectors.<ProdukKategori>toList());
	}

	public static String getParentIds(List<ProdukKategori> products) {
		StringBuilder sb = new StringBuilder();
		String glue = "";
		if (!products.isEmpty()) {
			for (ProdukKategori model : products) {
				sb.append(glue).append("'").append(model.id).append("'");
				glue = ",";
			}
		}
		return sb.toString();
	}

	public static List<ProdukKategori> setCategoriesChildren(List<ProdukKategori> products, long komoditasId) {
		if (!products.isEmpty()) {
			final String ids = getParentIds(products);
			Map<Long, List<ProdukKategori>> productMap = getChildByParentIds(ids, komoditasId);
			if (!productMap.isEmpty()) {
				for (ProdukKategori model : products) {
					if (model.children != null) {
						model.children = productMap.containsKey(model.id)
								? productMap.get(model.id)
								: Collections.<ProdukKategori>emptyList();
					}
				}
			}

		}
		return products;
	}

	/**
	 * @param parentIds restructured parent ids in string form glued by ","
	 * @param komoditasId komoditas id
	 * @return map with parent_id as a key and children as value*/
	public static Map<Long, List<ProdukKategori>> getChildByParentIds(String parentIds, long komoditasId) {
		Map<Long, List<ProdukKategori>> results = new HashMap<Long, List<ProdukKategori>>();
		List<ProdukKategori> produkKategoris = getList(komoditasId, parentIds);
		if (produkKategoris != null && !produkKategoris.isEmpty()) {
			for (ProdukKategori model : produkKategoris) {
				if (results.containsKey(model.parent_id)) {
					List<ProdukKategori> products = results.get(model.parent_id);
					products.add(model);
				} else {
					List<ProdukKategori> products = new ArrayList<ProdukKategori>();
					products.add(model);
					results.put(model.parent_id, products);
				}
			}
		}
		return results;
	}

	/**
	 * clear category cache by komoditas id
	 * @param kid komuditas id
	 * */
	public static void clearCache(long kid) {
		final String key = CACHE_KEY + kid;
		Logger.debug(TAG + " clear cache by key: " + key);
		Cache.delete(key);
	}

	/**
	 * check if cache exists by given key
	 * @param key combination key with id*/
	private static boolean isCacheNotExists(String key) {
		List<ProdukKategori> results = getFromCache(key);
		return results == null;
	}

	private static List<ProdukKategori> getFromCache(String key) {
		return (List<ProdukKategori>) Cache.get(key);
	}

	public static long savePosisi(ProdukKategori model) {
		String query = "UPDATE " + TABLE_NAME
				+ " set " + NAMA_KATEGORI + "=?"
				+ ", " + LEVEL_KATEGORI + "=?"
				+ ", " + PARENT_ID + "=?"
				+ " WHERE " + ID + " =?";
		return Query.update(query, model.nama_kategori, model.level_kategori, model.parent_id, model.id);
	}

	public static ProdukKategori getChild(long parentId) {
		return Query.find("SELECT * FROM produk_kategori " +
				"WHERE " + PARENT_ID + " =? AND " + ACTIVE + " =?",
				ProdukKategori.class, parentId, BaseKatalogTable.AKTIF).first();
	}

	/**
	 * get detail by id
	 * @param id
	 * @return ProdukKategori*/
	public static ProdukKategori getDetail(long id) {
		return ProdukKategori.findById(id);
	}

	/**
	 * @param kid komoditas id
	 * @return list with komoditas id filter for dropdown*/
	public static List<ProdukKategori> getListForDropdown(long kid) {
		final String query = "SELECT " + ID
				+ ", " + NAMA_KATEGORI
				+ " FROM " + TABLE_NAME
				+ " WHERE " + KOMODITAS_ID + " =?"
				+ " ORDER BY " + NAMA_KATEGORI + " ASC ";
		return Query.find(query, ProdukKategori.class, kid).first();
	}

	/**
	 * @param model ProdukKategoriPayload
	 * @return ProdukKategori
	 * */
	public static ProdukKategori save(ProdukKategoriPayload model) {
		ProdukKategori produkKategori = getDetail(model.id);
		if (produkKategori == null) {
			produkKategori = new ProdukKategori(model);
			produkKategori.active = true;
		} else {
			produkKategori.setFromPayload(model);
		}
		produkKategori.save();
		clearCache(model.kid);
		return produkKategori;
	}

	public static Set<Long> traceCategories(Long id) {
		Set<Long> result = new HashSet<>();
		if (id != null) {
			ProdukKategori produkKategori = ProdukKategori.findById(id);
			if (produkKategori != null) {
				result.add(produkKategori.id);
				LogUtil.d(TAG, produkKategori);
				findParent(result, produkKategori);
			}
		}
		return result;
	}

	private static void findParent(Set<Long> result, ProdukKategori produkKategori) {
		if (produkKategori.parent_id > 0) {
			ProdukKategori parent = ProdukKategori.find("id =? AND active = 1", produkKategori.parent_id).first();
			if (parent != null) {
				LogUtil.d(TAG, parent);
				result.add(parent.id);
				findParent(result, parent);
			}
		}
	}

	public static String getCategoriesSql() {
		final String levelOne = "SELECT DISTINCT " +
				" lvone.id as one_id," +
				" lvone.nama_kategori as one_name," +
				" lvtwo.id as two_id," +
				" lvtwo.nama_kategori as two_name," +
				" lvthree.id as th_id," +
				" lvthree.nama_kategori as th_name," +
				" lvforth.id as ft_id," +
				" lvforth.nama_kategori as ft_name," +
				" lvfith.id as fi_id," +
				" lvfith.nama_kategori as fi_name," +
				" (CASE WHEN lvfith.id <> '' THEN lvforth.id" +
				" WHEN lvforth.id <> '' THEN lvforth.id" +
				" WHEN lvthree.id <> '' THEN lvthree.id" +
				" WHEN lvtwo.id <> '' THEN lvtwo.id" +
				" ELSE lvone.id END) as current_id" +
				" FROM produk_kategori lvone" +
				" JOIN produk_kategori lvtwo" +
				" ON lvtwo.parent_id = lvone.id" +
				" JOIN produk_kategori lvthree" +
				" ON lvthree.parent_id = lvtwo.id" +
				" JOIN produk_kategori lvforth" +
				" ON lvforth.parent_id = lvthree.id" +
				" JOIN produk_kategori lvfith" +
				" ON lvfith.parent_id = lvforth.id" +
				" WHERE (lvone.active > 0" +
				" OR lvtwo.active > 0" +
				" OR lvthree.active > 0" +
				" OR lvforth.active > 0)";
		final String levelTwo = "SELECT DISTINCT " +
				"lvone.id," +
				" lvone.nama_kategori as one_name," +
				" lvtwo.id as two_id," +
				" lvtwo.nama_kategori as two_name," +
				" lvthree.id as th_id," +
				" lvthree.nama_kategori as th_name," +
				" lvforth.id as ft_id," +
				" lvforth.nama_kategori as ft_name," +
				" '' as placeholder," +
				" '' as placeholder," +
				"(CASE WHEN lvforth.id <> '' THEN lvforth.id" +
				" WHEN lvthree.id <> '' THEN lvthree.id" +
				" WHEN lvtwo.id <> '' THEN lvtwo.id" +
				" ELSE lvone.id END) as current_id" +
				" FROM produk_kategori lvone" +
				" JOIN produk_kategori lvtwo" +
				" ON lvtwo.parent_id = lvone.id" +
				" JOIN produk_kategori lvthree" +
				" ON lvthree.parent_id = lvtwo.id" +
				" JOIN produk_kategori lvforth" +
				" ON lvforth.parent_id = lvthree.id" +
				" WHERE (lvone.active > 0" +
				" OR lvtwo.active > 0" +
				" OR lvthree.active > 0" +
				" OR lvforth.active > 0)";
		final String levelThree = "SELECT DISTINCT " +
				" lvone.id," +
				" lvone.nama_kategori as one_name," +
				" lvtwo.id as two_id," +
				" lvtwo.nama_kategori as two_name," +
				" lvthree.id as th_id," +
				" lvthree.nama_kategori as th_name," +
				" '' as placeholder," +
				" '' as placeholder," +
				" '' as placeholder," +
				" '' as placeholder," +
				" (CASE WHEN lvthree.id <> '' THEN lvthree.id" +
				" WHEN lvtwo.id <> '' THEN lvtwo.id" +
				" ELSE lvone.id END) as current_id" +
				" FROM produk_kategori lvone" +
				" JOIN produk_kategori lvtwo" +
				" ON lvtwo.parent_id = lvone.id" +
				" JOIN produk_kategori lvthree" +
				" ON lvthree.parent_id = lvtwo.id" +
				" WHERE (lvone.active > 0" +
				" OR lvtwo.active > 0" +
				" OR lvthree.active > 0)";
		final String levelFour = "SELECT DISTINCT" +
				" lvone.id," +
				" lvone.nama_kategori as one_name," +
				" lvtwo.id as two_id," +
				" lvtwo.nama_kategori as two_name," +
				" '' as placeholder," +
				" '' as placeholder," +
				" '' as placeholder," +
				" '' as placeholder," +
				" '' as placeholder," +
				" '' as placeholder," +
				" (CASE WHEN lvtwo.id <> '' THEN lvtwo.id" +
				" ELSE lvone.id END) as current_id" +
				" FROM produk_kategori lvone" +
				" JOIN produk_kategori lvtwo" +
				" ON lvtwo.parent_id = lvone.id" +
				" WHERE (lvone.active > 0" +
				" OR lvtwo.active > 0 )";
		final String levelFive = "SELECT DISTINCT" +
				" lvone.id," +
				" lvone.nama_kategori as one_name," +
				" '' as placeholder," +
				" '' as placeholder," +
				" '' as placeholder," +
				" '' as placeholder," +
				" '' as placeholder," +
				" '' as placeholder," +
				" '' as placeholder," +
				" '' as placeholder," +
				" lvone.id as current_id" +
				" FROM produk_kategori lvone" +
				" WHERE lvone.active > 0 AND lvone.parent_id = ''";
		final String glue = " UNION ALL ";
		return levelOne + glue +
				levelTwo + glue +
				levelThree + glue +
				levelFour + glue +
				levelFive;
	}


}
