package models.elasticsearch.query.aggregation;

import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

/**
 * @author HanusaCloud on 10/10/2017
 */
public class InnerTermAggregation {

    private TopHitAggregation topHitAggregation;
    private Map<String, MetricAggregation> metricAggregations = new HashMap<>();

    public InnerTermAggregation setTerm(MetricAggregation aggregation) {
        if (aggregation != null) {
            this.metricAggregations.put(aggregation.getAlias(), aggregation);
        }
        return this;
    }

    public InnerTermAggregation topHitAggregasion(TopHitAggregation model) {
        this.topHitAggregation = model;
        return this;
    }

    public JsonObject build() {
        JsonObject result = new JsonObject();
        if (!metricAggregations.isEmpty()) {
            for (Map.Entry<String, MetricAggregation> entry : metricAggregations.entrySet()) {
                MetricAggregation metricAggregation = entry.getValue();
                result.add(metricAggregation.getAlias(), metricAggregation.build());
            }
        }
        if (topHitAggregation != null) {
            result.add(topHitAggregation.getAlias(), topHitAggregation.build());
        }
        return result;
    }

}
