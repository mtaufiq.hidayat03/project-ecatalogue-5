package jobs.JobTrail;

import jobs.extdata.ExtData;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;

import java.util.Date;
import java.util.UUID;

@NoTransaction
public class JobSirupData extends Job {
    ExtData ext = new ExtData();
    String uniqueID = UUID.randomUUID().toString();
    Date tg_update = new Date();
    public JobSirupData(ExtData _ext, Date _tg_update){
        ext = _ext;
        tg_update = _tg_update;
    }
    @Override
    public void before() {
        Logger.info("job id:"+uniqueID+" for date:"+tg_update);
        super.before();
    }
    public void doJob(){
        Logger.info(">>>>>> Do Job with id:"+uniqueID+" and date:"+tg_update.toLocaleString());
        ext.getSyrupData(tg_update);
        Logger.info("%%%% done getDate, saving now %%%");
        ext.saveDataToDb();
        //sendEmailReport();
    }
    public void after(){
        Logger.info("<<<<<< Done Job with id:"+uniqueID+" and date:"+tg_update.toLocaleString());
        //mail will be here
    }
}
