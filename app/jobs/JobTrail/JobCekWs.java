package jobs.JobTrail;

import models.common.AktifUser;
import models.util.RekananDetail;
import play.db.jpa.NoTransaction;
import play.jobs.Job;

@NoTransaction
public class JobCekWs extends Job {
    RekananDetail rknDetail = new RekananDetail();
    public JobCekWs(RekananDetail rknd){
        rknDetail = rknd;
    }
    public void doJob(){
        try {
            AktifUser.cekBlacklist(rknDetail);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
