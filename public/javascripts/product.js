jQuery(document).ready(function () {

    function getComparisons() {
        var array = {};
        var saved = getCookie("COMPARISON_ARRAY");
        if (saved !== '') {
            array = JSON.parse(saved);
        }
        return array;
    }

    function getTotalKey(array) {
        var total = 0;
        for (var k in array) {
            total++;
        }
        return total;
    }

    jQuery(".compare").on('change', function () {
        var view = jQuery(this);
        var isAllow = view.data('allow');
        if (isAllow) {
            var array = getComparisons();
            printTemplate();
            var data = {};
            data['id'] = view.data("value");
            data['name'] = view.data('name');
            data['locationId'] = view.data("location");
            data['type'] = view.data("type");
            if (this.checked) {
                if (array.hasOwnProperty(data.id)) {
                    alert("Produk telah ditambahkan!")
                } else if (getTotalKey(array) > 2) {
                    alert("Maksimal 3 produk!")
                    this.checked = false;
                } else {
                    array[data.id] = data;
                    var results = JSON.stringify(array);
                    var comparisons = generateCacheTemplate(array);
                    setCookie("comparisons", comparisons, 1);
                    setCookie("COMPARISON_ARRAY", results, 1);
                    jQuery('.compare-list').append(generateComparisonTemplateItem(data));
                }
            } else {
                removeComparison(data.id);
                removeLayout(data.id);
            }
            hideAndShowContainer(array);
        } else {
            alert("Terdapat lebih dari satu harga, gunakan filter provinsi atau kabupaten!");
            this.checked = false;
        }
    });



    function hideAndShowContainer(array) {
        if(Object.keys(array).length > 0) {
            jQuery(".compare-list-container").removeClass("hide").show();
        } else {
            jQuery(".compare-list-container").addClass("hide").hide();
        }
    }

    function printTemplate() {
        var array = getComparisons();
        if (Object.keys(array).length > 0) {
            var elements = '';
            for (var key in array) {
                elements += generateComparisonTemplateItem(array[key]);
            }
            jQuery('.compare-list').html(elements);
        }
    }

    printTemplate();

    jQuery(document).on('click', '.remove-comparison', function () {
        var view = jQuery(this);
        var val = view.data('value');
        jQuery('.compare[data-value='+ val+']').prop('checked', false);
        var arr = removeComparison(val);
        view.remove();
        hideAndShowContainer(arr);
    });

    jQuery('.remove-all-comparison').click(function () {
        setCookie('COMPARISON_ARRAY', '{}', 1);
        setCookie('comparisons', '', 1);
        jQuery('.compare-list').html('');
        jQuery('.compare').prop('checked', false);
        hideAndShowContainer({});
    });



    function generateCacheTemplate(array) {
        var result = "";
        for (var key in array) {
            var data = array[key];
            result += data['id'] + "|" + data['locationId'] + "|" + data['type'] + ";";
        }
        return encodeURIComponent(result);
    }

    function removeComparison(id) {
        var array = {};
        var saved = getCookie("COMPARISON_ARRAY");
        if (saved !== '{}' || saved !== '') {
            array = JSON.parse(saved);
            if (array.hasOwnProperty(id)) {
                delete array[id];
            }
            setCookie('COMPARISON_ARRAY', JSON.stringify(array), 1);
            setCookie("comparisons", generateCacheTemplate(array), 1);
        }
        return array;
    }

    function removeLayout(id) {
        $('.remove-comparison[data-value="' + id + '"]').remove();
    }

    function generateComparisonTemplateItem(data) {
        return '<div class="col-md-3 remove-comparison button" data-value="' + data.id+ '" >\n' +
            '                            <div class="compare-item">\n' +
            '                                <div class="product-name">\n' +
            '                                    <div>\n' +
            '                                        <i class="fa fa-close"></i>\n' +
            '                                    '+ data['name'] +'\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                        </div>';
    }

    /*============================basket=================================================*/

    $('#addToBasketModal').on('hide.bs.modal', function (e) {
        $(".modal-body").html("");
    });

    $('[name="addToBasketBtn"]').click(function(e) {
        e.preventDefault();
        $('#addToBasketModal').modal('show').find('.modal-body').load($(this).attr('href'));
    });

    $(document).on('click', '.add-to-basket', function () {
        addToBasket($(this));
    });

    function addToBasket(view) {
        var jumlah = $("input[name='jumlah']").val();
        var jumlahDiKeranjang = document.getElementsByClassName("totalProdukKeranjang")[0].innerHTML;
        $('#alert').html('');
        if (!isNaN(jumlah) && jumlah > 0) {
            $.ajax({
                url: view.data('value'),
                data: {"jumlah": jumlah},
                type: 'POST',
                success: function (res) {
                    // $('#basketBtn').find('.badge').html(res.total);
                    jumlahDiKeranjang = parseInt(jumlahDiKeranjang)+ 1;
                    $('#basketBtn').find('.badge').html(jumlahDiKeranjang);
                    if (res.result === 'ok'){
                        location.reload();
                        $('#alert').html('<div class="alert alert-success">Berhasil masuk ke keranjang belanja </div>');
                    }else if (res.result === 'empty')
                        $('#alert').html('<div class="alert alert-danger">Gagal masuk ke keranjang belanja, Anda belum memilih produk/jumlah produk</div>');
                    else
                        $('#alert').html('<div class="alert alert-danger">Terjadi Kesalahan, Gagal memasukan barang ke keranjang belanja</div>');
                },
                error: function () {
                    $('#alert').html('<div class="alert alert-danger">Terjadi Kesalahan, Gagal memasukan barang ke keranjang belanja</div>');
                }
            });
        } else {
            $('#alert').html('<div id="alert"><div class="alert alert-danger">Jumlah harus melebih 0!</div></div>');
        }
    }

});
