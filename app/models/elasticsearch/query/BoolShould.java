package models.elasticsearch.query;

import com.google.gson.JsonObject;

/**
 * @author HanusaCloud on 10/11/2017
 */
public abstract class BoolShould {

    public abstract JsonObject build ();

}
