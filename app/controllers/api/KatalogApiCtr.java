package controllers.api;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import controllers.security.AllowAccess;
import ext.DateBinder;
import ext.FormatUtils;
import jobs.UpdateProdukJob;
import models.api.LogMessage;
import models.api.Sirup;
import models.appParam.AppParam;
import models.common.AktifUser;
import models.jcommon.http.SimpleHttpRequest;
import models.jcommon.util.CommonUtil;
import models.secman.Acl;
import models.util.Config;
import org.apache.commons.lang.time.StopWatch;
import play.Logger;
import play.cache.Cache;
import play.data.binding.As;
import play.libs.URLs;
import play.mvc.Controller;
import services.api.ApiService;
import utils.KatalogUtils;

import java.util.Date;
import java.util.List;

import static models.common.AktifUser.getAktifUser;

/**
 * Created by dadang on 12/11/17.
 */
public class KatalogApiCtr extends Controller {

	public static void index(){

		render("api/index.html");
	}

	private static long generateId(){
		Date date = new Date();
		return date.getTime();
	}

	private static void printMessage(LogMessage log){

		response.contentType = "text/event-stream";
		response.setHeader("cache-control", "no-cache");

		StringBuilder data = new StringBuilder();
		data.append("id: " + generateId() + "\n\n");
		data.append("event: message \n\n");
		JsonObject message = new JsonObject();
		message.addProperty("message", log.message);
		message.addProperty("newLine", log.newLine);
		message.addProperty("isEnd", log.isEnd);
		data.append("data: "+message.toString() + "\n\n");

		response.print(data);
	}

	public static void formRup(){

		AppParam appParam = AppParam.find("param_type = ?", AppParam.API_RUP).first();
		if(appParam == null){
			notFound();
		}

		renderArgs.put("tanggalUpdate", FormatUtils.formatDateToDatePickerView(appParam.param_date));
		render("api/form_rup.html");
	}

	public static void formRupSubmit(@As(binder = DateBinder.class) Date tanggal_update){
		Date startDate = new Date();
		LogMessage log = new LogMessage(
				FormatUtils.formatDateToDateTimePickerView(startDate) + " - Memulai service..",
				false,
				true
		);
		printMessage(log);

		try {
			Config conf = Config.getInstance();
			String date = KatalogUtils.toDateyyyyMMdd(tanggal_update);
			String url = conf.sirup_api + "?auditupdate=" + URLs.encodePart(date);
			try (SimpleHttpRequest connect = new SimpleHttpRequest()) {
				connect.get(url);
				String content = connect.getString();
				List<Sirup> sirupList = new Gson().fromJson(content, new TypeToken<List<Sirup>>(){}.getType());

				for (Sirup sp:sirupList){
					startDate = new Date();
					log = new LogMessage(FormatUtils.formatDateToDateTimePickerView(startDate)+" - "+ sp.nama,
							false,
							true
					);
					printMessage(log);
				}

				log = new LogMessage(
						FormatUtils.formatDateToDateTimePickerView(new Date()) + " Total Rup:" + sirupList.size() + " RUP baru..",
						true,
						true
				);
				printMessage(log);
			}

//			long total = ApiService.tarikRup(tanggal_update);
//			log = new LogMessage(
//					FormatUtils.formatDateToDateTimePickerView(new Date()) + " Berhasil insert : " + total + " RUP baru..",
//					true,
//					true
//			);
//			printMessage(log);
		} catch (Exception e) {
			e.printStackTrace();
			log = new LogMessage(
					FormatUtils.formatDateToDateTimePickerView(new Date()) + " Terdapat kesalahan saat melakukan penarikan..",
					true,
					true
			);
			printMessage(log);
		}
		Date endDate = new Date();
		long different = (endDate.getTime() - endDate.getTime()) / (24 * 60 * 60 * 1000);
		log = new LogMessage(
				FormatUtils.formatDateToDateTimePickerView(endDate) + " Selesai dieksekusi selama " + different + " Detik",
				true,
				true
				);
		printMessage(log);

	}

	public static void tes(){
		LogMessage l = new LogMessage();
		l.message = "tes...";
		l.isEnd = false;
		l.newLine = true;
		printMessage(l);
	}

    @AllowAccess({Acl.COM_EXEC_PRODUK_ELASTICS})
	public static void formProduk(){
		AktifUser aktifUser = getAktifUser();

		render("api/form_produk.html", aktifUser);
	}
    @AllowAccess({Acl.COM_EXEC_PRODUK_ELASTICS})
	public static void formProdukSubmit(){
		Date startDate = new Date();
		String keyTemp = "exc_job";
		Date first = Cache.get(keyTemp, Date.class);
		if(null ==first){

			LogMessage log = new LogMessage(
					FormatUtils.formatDateToDateTimePickerView(startDate) + " - Memulai service..",
					false,
					true
			);
			printMessage(log);
			Cache.set(keyTemp,new Date());
			Cache.set("msg_jobp","inisiating job.....");
			Logger.info("execute job here");

			UpdateProdukJob job  = new UpdateProdukJob();
			job.now();

		}else{
			String msg = " - Still running";
			try{
				msg = Cache.get("msg_jobp",String.class);
			}catch (Exception e){}
			LogMessage log = new LogMessage(
					FormatUtils.formatDateToDateTimePickerView(new Date()) + " "+ msg,
					false,
					true
			);
			printMessage(log);
			startDate = Cache.get(keyTemp, Date.class);
			Logger.info("next info here");

		}

		Date endDate = Cache.get("end_jobp", Date.class);
		if(null != endDate){
			long different = (endDate.getTime() - startDate.getTime()) / (24 * 60 * 60 * 1000);
			LogMessage log = new LogMessage(
				FormatUtils.formatDateToDateTimePickerView(endDate) + " Selesai dump semua Produk ",
					true,
					true
			);
			printMessage(log);
			try {
				//see UpdateProdukJob
				Cache.delete("end_jobp");
				Cache.delete(keyTemp);
				Cache.delete("msg_jobp");
			}catch (Exception e){}
		}

	}

}
