package models.katalog.payload;

import com.google.gson.annotations.SerializedName;

public class ProdukKategoriAtributPosisi {

	public long id = 0;
	public String name = "";
	@SerializedName("parent_id")
	public long parentId = 0;
	public int depth = 0;
	public long left;
	public long right;

}
