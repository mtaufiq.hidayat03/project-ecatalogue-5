package jobs.elasticsearch;

import jobs.TrackableJob;
import models.penyedia.Penyedia;
import play.Logger;
import play.cache.Cache;
import play.jobs.On;
import repositories.elasticsearch.ElasticsearchRepository;
import java.util.Date;

import static jobs.elasticsearch.ElasticSearchTextLog.writeLogGeneral;

// @On("0 0 2 * * ?")
public class NewElasticJob extends TrackableJob {
    @Override
    public void doJob(){
        String logs = "Job elastic mulai : "+ new Date();
        writeLogGeneral(logs);
        Logger.info(logs);
        // override file, hapus file yang ada
        boolean overide = false;
        // hapus file json yang ada
        boolean deleteFileJson = true;
        ElasticSearchTextLog.loopLimitProduk(overide,deleteFileJson);

        logs = "isi penyedia elastic";
        writeLogGeneral(logs);
        Penyedia.getAllPenyediaOnElastic();
        logs = "popular cahe, recommendation";
        writeLogGeneral(logs);
        try{
            Cache.delete(ElasticsearchRepository.POPULAR_CACHE_KEY);
            Cache.delete(ElasticsearchRepository.RECOMMENDATION_CACHE_KEY);
        }catch (Exception e){
            try{
                Cache.safeDelete(ElasticsearchRepository.POPULAR_CACHE_KEY);
                Cache.safeDelete(ElasticsearchRepository.RECOMMENDATION_CACHE_KEY);
            }catch (Exception ex){}
        }
        logs = "Job elastic selesai : "+ new Date();
        writeLogGeneral(logs);
        Logger.info(logs);

    }
}
