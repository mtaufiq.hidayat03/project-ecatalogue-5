package models.elasticsearch.product;

import com.google.gson.annotations.SerializedName;
import models.elasticsearch.log.ProductLogJson;
import models.katalog.ProdukWilayahJualKabupaten;
import models.katalog.ProdukWilayahJualProvinsi;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author HanusaCloud on 9/29/2017
 */
public class ProductPriceElastic implements Serializable {

    public static final String TYPE_PROVINCE = "province";
    public static final String TYPE_REGENCY = "regency";
    public static final String TYPE_GENERAL = "general";

    @SerializedName("id")
    public long id;
    @SerializedName("type")
    public String type;
    @SerializedName("harga_utama")
    public Long hargaUtama;
    @SerializedName("harga_pemerintah")
    public Long hargaPemerintah;
    @SerializedName("name")
    public String locationName;

    public ProductPriceElastic() {
    }

    public ProductPriceElastic(String type, long locationId, String locationName) {
        this.type = type;
        this.locationName = locationName;
        this.id = locationId;
    }

    public ProductPriceElastic(ProductLogJson model) {
        this.id = -999;
        this.type = TYPE_GENERAL;
        this.hargaUtama = new BigDecimal(model.harga_utama).longValue();
        this.hargaPemerintah = new BigDecimal(model.harga_pemerintah).longValue();
        this.locationName = "";
    }

    public ProductPriceElastic(ProdukWilayahJualKabupaten model) {
        this.id = model.kabupaten_id;
        this.type = TYPE_REGENCY;
        this.hargaUtama = model.harga_utama.longValue();
        this.hargaPemerintah = new BigDecimal(model.harga_pemerintah).longValue();
        this.locationName = model.nama_lokasi;
    }

    public ProductPriceElastic(ProdukWilayahJualProvinsi model) {
        this.id = model.provinsi_id;
        this.type = TYPE_PROVINCE;
        this.hargaUtama = model.harga_utama.longValue();
        this.hargaPemerintah = new BigDecimal(model.harga_pemerintah).longValue();
        this.locationName = model.nama_lokasi;
    }

    public static String getElasticPriceType(String type) {
        switch (type) {
            case "provinsi":
                return TYPE_PROVINCE;
            case "kabupaten":
                return TYPE_REGENCY;
            default:
                return TYPE_GENERAL;
        }
    }

}

