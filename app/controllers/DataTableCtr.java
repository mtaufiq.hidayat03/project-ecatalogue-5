package controllers;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import ext.DateBinder;
import models.common.AktifUser;
import models.datatable.ResultsetHandlers;
import models.jcommon.datatable.DatatableQuery;
import models.jcommon.datatable.DatatableResultsetHandler;
import models.jcommon.util.CommonUtil;
import models.katalog.Produk;
import models.prakatalog.Penawaran;
import models.prakatalog.Usulan;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.data.binding.As;
import utils.DateTimeUtils;
import utils.LogUtil;
import utils.RupDatatableQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**1. INGAT!!!!! DI DALAM ACTION CONTROLLER TIDAK BOLEH ADA TERLALU BANYAK CODE KARENA ADA BUG PADA PLAY! 
 * 2. Pastikan Anda gunakan StringEscapeUtil.escapeSql(param) untuk param yg didapat dari QueryString dan di-Concat
 * demi mencegah SQL Injection
 * @author Andik
 *
 */
public class DataTableCtr extends BaseController {

	public static void cariGrupRole() {
		//TODO Query ini perlu improvement. durasi 170-200 ms
		String leftJoinUser = "LEFT JOIN (" +
				"Select Count(*) total_user, user_role_grup_id " +
				"From user " +
				"Where active >= 0 Group By user_role_grup_id) u " +
				"ON u.user_role_grup_id = urg.id";

		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_GRUP_ROLE)
				.select("urg.id, urg.nama_grup, coalesce(u.total_user,0) total_user")
				.from("user_role_grup urg "+leftJoinUser)
				.where("urg.active >= ?")
				.params(1)
				.executeQuery();

		renderJSON(json);
	}

	public static void cariBlacklist() {
		String leftJoinUser = "LEFT JOIN (" +
				"Select Count(*) total_user, user_role_grup_id " +
				"From user " +
				"Where active >= 0 Group By user_role_grup_id) u " +
				"ON u.user_role_grup_id = urg.id";

		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_BLACKLIST)
				.select("bl.id, bl.nama_penyedia, bl.sk, bl.satker_nama, bl.provinsi_nama, bl.kabupaten_nama")
				.from("blacklist bl")
//				.where("urg.active >= ?")
//				.params(1)
				.executeQuery();
		Logger.info(new Gson().toJson(json));
		renderJSON(json);
	}

	public static void cariManufaktur(String keyword, String komoditas, String aktif){
		List<Object> params = new ArrayList<>();

		String whereString = " 1=1 ";

		if(!TextUtils.isEmpty(keyword)) {
			keyword = "%" + StringEscapeUtils.escapeSql(keyword) + "%";
			whereString += " and (m.nama_manufaktur LIKE ?)";
			params.add(keyword);
		}

		if(!TextUtils.isEmpty(aktif)){
			whereString += " and km.active = ?";
			params.add(aktif);
		}

		if(!TextUtils.isEmpty(komoditas)){
			whereString += " and k.id = ?";
			params.add(komoditas);
		}

		JsonObject json = new DatatableQuery(ResultsetHandlers.DAFTAR_MANUFAKTUR)
				.select("km.id, km.manufaktur_id, km.active, km.komoditas_id, k.nama_komoditas, m.nama_manufaktur, m.created_date, m.modified_date " +
						", case when m.created_by=-99 then 'SYSTEM' else coalesce(c.nama_lengkap,'') end as created_by " +
						", case when m.modified_by=-99 then 'SYSTEM' else coalesce(u.nama_lengkap,'') end as modified_by")
				.from( " komoditas_manufaktur km " +
						" left join manufaktur m ON km.manufaktur_id = m.id and km.active >= 0 " +
						" left join komoditas k ON km.komoditas_id = k.id and k.active >= 0 " +
						" left join user c on m.created_by = c.id " +
						" left join user u on m.modified_by = u.id "
						)
				.where(whereString)
				.params(params.toArray(new Object[params.size()]))
				.executeQuery();

		renderJSON(json);
	}

	public static void cariBerita(String keyword, String kategori_id, String status, String aktif){
		List<Object> params = new ArrayList<>();

		String whereString = "kk.active = 1";

		if(!TextUtils.isEmpty(keyword)) {
			keyword = "%" + StringEscapeUtils.escapeSql(keyword) + "%";
			whereString += " AND (kon.judul_konten LIKE ?)";
			params.add(keyword);
		}

		if(!TextUtils.isEmpty(status)){
			whereString += " AND kon.status = ?";
			params.add(status);
		}

		if(!TextUtils.isEmpty(aktif)){
			whereString += " AND kon.active = ?";
			params.add(aktif);
		}

		if(!TextUtils.isEmpty(kategori_id)){
			whereString += " AND kk.id = ?";
			params.add(kategori_id);
		}

		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_BERITA)
				.select("kon.id,kk.id,kk.nama_kategori,kon.judul_konten,kon.isi_konten,kon.status,kon.created_date,kon.active," +
						"kon.publish_date_from, kon.publish_date_to")
				.from("konten kon " +
						"left join konten_kategori kk on kon.default_konten_kategori_id = kk.id ")
				.where(whereString)
				.params(params.toArray(new Object[params.size()]))
				.executeQuery();

		renderJSON(json);
	}

	public static void cariUnduh(String keyword, String kategori_id, String status, String aktif){
		List<Object> params = new ArrayList<>();

		String whereString = "kk.active = 1";

		if(!TextUtils.isEmpty(keyword)) {
			keyword = "%" + StringEscapeUtils.escapeSql(keyword) + "%";
			whereString += " AND (pf.original_file_name LIKE ? or pf.deskripsi LIKE ?)";
			params.add(keyword);
			params.add(keyword);
		}

		if(!TextUtils.isEmpty(status)){
			whereString += " AND pf.status = ?";
			params.add(status);
		}

		if(!TextUtils.isEmpty(aktif)){
			whereString += " AND pf.active = ?";
			params.add(aktif);
		}

		if(!TextUtils.isEmpty(kategori_id)){
			whereString += " AND kk.id = ?";
			params.add(kategori_id);
		}

		JsonObject json = new DatatableQuery(ResultsetHandlers.DAFTAR_UNDUH)
				.select("pf.id,kk.nama_kategori,pf.original_file_name,pf.file_size,pf.deskripsi,pf.status," +
						"pf.created_date,pf.publish_date_from,pf.publish_date_to,pf.file_sub_location,usr.nama_lengkap,pf.active")
				.from("pustaka_file pf " +
						"left join konten_kategori kk on pf.konten_kategori_id = kk.id " +
						"left join user usr on pf.created_by = usr.id ")
				.where(whereString)
				.params(params.toArray(new Object[params.size()]))
				.executeQuery();

		renderJSON(json);
	}

	public static void cariKategoriKonten(String keyword, String kategori_id, String aktif){
		List<Object> params = new ArrayList<>();

		String whereString = "1 = 1";

		if(!TextUtils.isEmpty(keyword)) {
			keyword = "%" + StringEscapeUtils.escapeSql(keyword) + "%";
			whereString += " AND (kk.nama_kategori LIKE ? or kk.deskripsi LIKE ?)";
			params.add(keyword);
			params.add(keyword);
		}

		if(!TextUtils.isEmpty(aktif)){
			whereString += " AND kk.active = ?";
			params.add(aktif);
		}

		if(!TextUtils.isEmpty(kategori_id)){
			whereString += " AND kk.parent_id = ?";
			params.add(kategori_id);
		}

		JsonObject json = new DatatableQuery(ResultsetHandlers.DAFTAR_KATEGORI_KONTEN)
				.select("kk.id,kku.nama_kategori_utama,kk.nama_kategori,kk.deskripsi,kk.posisi_kategori,kk.active,kk.created_date")
				.from("konten_kategori kk "+
						"left join konten_kategori_utama kku on kk.konten_kategori_utama_id = kku.id ")
				.where(whereString)
				.params(params.toArray(new Object[params.size()]))
				.executeQuery();

		renderJSON(json);
	}

	public static void cariKategoriUtama(){
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_KATEGORI_UTAMA)
				.select("kku.id, kku.id as id_kategori, kku.nama_kategori_utama, kku.deskripsi, kku.active")
				.from("konten_kategori_utama kku")
				.executeQuery();
		renderJSON(json);
	}

	public static void cariFaq(String keyword, String kategori_id, String status, String aktif){
		List<Object> params = new ArrayList<>();

		String whereString = "kk.active = 1";

		if(!TextUtils.isEmpty(keyword)) {
			keyword = "%" + StringEscapeUtils.escapeSql(keyword) + "%";
			whereString += " AND (fq.judul_konten LIKE ?)";
			params.add(keyword);
		}

		if(!TextUtils.isEmpty(status)){
			whereString += " AND fq.status = ?";
			params.add(status);
		}

		if(!TextUtils.isEmpty(aktif)){
			whereString += " AND fq.active = ?";
			params.add(aktif);
		}

		if(!TextUtils.isEmpty(kategori_id)){
			whereString += " AND kk.id = ?";
			params.add(kategori_id);
		}

		JsonObject json = new DatatableQuery(ResultsetHandlers.DAFTAR_FAQ)
				.select("fq.id,kk.nama_kategori,fq.judul_konten,fq.isi_konten,fq.status,fq.created_date,fq.publish_date_from,fq.publish_date_to,fq.active")
				.from("faq fq " +
						"left join konten_kategori kk on fq.konten_kategori_id = kk.id")
				.where(whereString)
				.params(params.toArray(new Object[params.size()]))
				.executeQuery();

		renderJSON(json);
	}

	public static void cariPolling(){
		JsonObject json = new DatatableQuery(ResultsetHandlers.DAFTAR_POLLING)
				.select("p.id,p.subjek,p.deskripsi_singkat,p.status,p.terkunci,p.apakah_utama,p.created_date,p.publish_date_from,p.publish_date_to,p.active")
				.from("polling p")
				.where("1 = 1")
				.executeQuery();

		renderJSON(json);
	}

	public static void cariKontenStatis(String keyword, String kategori_id, String status, String aktif){
		List<Object> params = new ArrayList<>();

		String whereString = "kk.active = 1";

		if(!TextUtils.isEmpty(keyword)) {
			keyword = "%" + StringEscapeUtils.escapeSql(keyword) + "%";
			whereString += " AND (ks.judul_konten LIKE ?)";
			params.add(keyword);
		}

		if(!TextUtils.isEmpty(status)){
			whereString += " AND ks.status = ?";
			params.add(status);
		}

		if(!TextUtils.isEmpty(aktif)){
			whereString += " AND ks.active = ?";
			params.add(aktif);
		}

		if(!TextUtils.isEmpty(kategori_id)){
			whereString += " AND kk.id = ?";
			params.add(kategori_id);
		}

		JsonObject json = new DatatableQuery(ResultsetHandlers.DAFTAR_KONTEN_STATIS)
				.select("ks.id,ks.judul_konten,kk.nama_kategori,ks.isi_konten,ks.status,ks.active,ks.created_date,ks.publish_date_from,ks.publish_date_to")
				.from("konten_statis ks " +
						"left join konten_kategori kk on kk.id = ks.konten_kategori_id")
				.where(whereString)
				.params(params.toArray(new Object[params.size()]))
				.executeQuery();
		renderJSON(json);
	}

	public static void cariSumberKurs(){

		JsonObject json = new DatatableQuery(ResultsetHandlers.DAFTAR_SUMBER_KURS)
				.select("id,active,sumber, created_date, created_by, modified_date, modified_by")
				.from("(select k.id, k.active, k.sumber, k.created_date, k.modified_date "
						+", case when k.created_by=-99 then 'SYSTEM' else coalesce(c.nama_lengkap,'') end as created_by"
						+", case when k.modified_by=-99 then 'SYSTEM' else coalesce(u.nama_lengkap,'') end as modified_by"
						+ " from kurs_sumber k"
						+ " left join user c on k.created_by=c.id left join user u on k.modified_by=u.id) a")
				.executeQuery();

		renderJSON(json);
	}

	public static void cariKurs(){

		JsonObject json = new DatatableQuery(ResultsetHandlers.DAFTAR_KURS)
				.select("k.id,k.active,k.nama_kurs")
				.from("kurs k")
				.executeQuery();

		renderJSON(json);
	}

	public static void cariNilaiKurs(String kurs_id){
		Long kursId = Long.parseLong(kurs_id);
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_KURS_NILAI)
				.select("id,tanggal_kurs,nilai_jual,nilai_beli,nilai_tengah"+
		", created_date, created_by, modified_date, modified_by")
				.from("(select kn.id, kurs_id_from, kn.active, tanggal_kurs,nilai_jual,nilai_beli,nilai_tengah, kn.created_date, kn.modified_date"+
						", case when kn.created_by=-99 then 'SYSTEM' else coalesce(c.nama_lengkap,'') end as created_by"+
						", case when kn.modified_by=-99 then 'SYSTEM' else coalesce(u.nama_lengkap,'') end as modified_by"
						+" from kurs_nilai kn left join user c on kn.created_by=c.id left join user u on kn.modified_by=u.id) a")
				.where("active = ? and kurs_id_from = ?")
				.params(1, kursId)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariProvinsi(){

		JsonObject json = new DatatableQuery(ResultsetHandlers.DAFTAR_PROVINSI)
				.select("k.id,k.active,k.nama_provinsi")
				.from("provinsi k")
				.executeQuery();

		renderJSON(json);
	}

	public static void cariKabupaten(String provinsi_id){
		Long provinsiId = Long.parseLong(provinsi_id);
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_KABUPATEN)
				.select("k.id,k.active,k.nama_kabupaten")
				.from("kabupaten k")
				.where("provinsi_id = ?")
				.params(provinsiId)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariInstansi(){
		JsonObject json = new DatatableQuery(ResultsetHandlers.DAFTAR_INSTANSI)
				.select("k.id, k.nama, k.jenis,p.nama_provinsi")
				.from("kldi k join provinsi p on p.id=k.prp_id")
				.executeQuery();

		renderJSON(json);
	}

	public static void cariSatker(String instansi_id){
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_SATKER)
				.select("id, nama, alamat, active, idKldi")
				.from("kldi_satker")
				.where("idKldi like ?")
				.params(instansi_id)
				.executeQuery();
		renderJSON(json);
	}


	public static void cariSumberDana(){
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_SUMBERDANA)
				.select("id,nama_sumber_dana,deskripsi")
				.from("sumber_dana")
				.where("active=?")
				.params(1)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariMasterRup(String keyword, @As(binder = DateBinder.class)Date auditupdate){
		String tgl_auditupdate = DateTimeUtils.convertToStandardDate(auditupdate);

		List<Object> params = new ArrayList<>();

		String whereString = "1 = 1";

		if(!TextUtils.isEmpty(keyword)) {
			whereString += " AND (id = " + StringEscapeUtils.escapeSql(keyword);
			keyword = "%" + StringEscapeUtils.escapeSql(keyword) + "%";
			whereString += " OR nama LIKE ?)";
			params.add(keyword);
		}

		if(!TextUtils.isEmpty(tgl_auditupdate)){
			tgl_auditupdate = "%" + StringEscapeUtils.escapeSql(tgl_auditupdate) + "%";
			whereString += " AND audit_update LIKE ?";
			params.add(tgl_auditupdate);
		}

		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_MASTER_RUP)
				.select("id,nama,audit_update")
				.from("rup")
				.where(whereString)
				.params(params.toArray(new Object[params.size()]))
				.executeQuery();
		renderJSON(json);
	}

	public static void cariMasterRupEmpty(){
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_MASTER_RUP)
				.select("id,nama,audit_update")
				.from("rup")
				.where("id is null")
				.executeQuery();
		renderJSON(json);
	}

	public static void cariUnitPengukuran(){
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_UNITPENGUKURAN)
				.select("id,nama_unit_pengukuran,deskripsi,active,created_date,modified_date,created_by,modified_by")
				.from("(select k.id, k.nama_unit_pengukuran, k.deskripsi, k.active, k.created_date, k.modified_date "
						+", case when k.created_by=-99 then 'SYSTEM' else coalesce(c.nama_lengkap,'') end as created_by"
						+", case when k.modified_by=-99 then 'SYSTEM' else coalesce(u.nama_lengkap,'') end as modified_by"
						+ " from unit_pengukuran k"
						+ " left join user c on k.created_by=c.id left join user u on k.modified_by=u.id) a")
				.executeQuery();
		renderJSON(json);
	}

	public static void cariKategoriKomoditas() {
		String leftJoinKomoditas = "LEFT JOIN (" +
				"Select Count(*) total_komoditas, komoditas_kategori_id " +
				"From komoditas " +
				"Where active = ? Group By komoditas_kategori_id) k " +
				"ON k.komoditas_kategori_id = kk.id";

		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_KATEGORI_KOMODITAS)
				.select("kk.id, kk.nama_kategori, coalesce(k.total_komoditas,0) total_komoditas")
				.from("komoditas_kategori kk "+leftJoinKomoditas)
				.where("kk.active = ?")
				.params(1,1)
				.executeQuery();

		renderJSON(json);
	}

	public static void cariProdukAtributTipe(String komoditas_id){
		Long komoditasId = Long.parseLong(komoditas_id);
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_KOMODITAS_PRODUK_ATRIBUT_TIPE)
				.select("id,komoditas_id,tipe_atribut,deskripsi")
				.from("komoditas_produk_atribut_tipe")
				.where("active = ? and komoditas_id = ?")
				.params(1, komoditasId)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariTahapan(){
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_TAHAPAN)
				.select("id,nama_tahapan,urutan_tahapan,nama_tahapan_alias,flagging")
				.from("tahapan")
				.where("active=?")
				.params(1)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariBidang(){
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_BIDANG)
				.select("id, nama_bidang")
				.from("bidang")
				.where("active=?")
				.params(1)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariTemplateKontrak(long komoditas_id){
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_TEMPLATE_KONTRAK)
				.select("id,jenis_dokumen")
				.from("komoditas_template_kontrak")
				.where("active=? and komoditas_id=?")
				.params(1,komoditas_id)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariUnspsc(){
		JsonObject json = new DatatableQuery(ResultsetHandlers.DAFTAR_UNSPSC)
				.select("segment_title, family_title, class_title, commodity_title, commodity_id")
				.from("unspsc")
				.executeQuery();
		renderJSON(json);
	}

	public static void cariDokumenUpload(long penawaran_id, long kategori_id){
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_DOKUMEN_UPLOAD)
				.select("du.id, dk.nama_kategori, du.nama_dokumen, du.file_path, du.active, du.created_date")
				.from("dokumen_upload du " +
						"left join dokumen_kategori dk on du.dokumen_kategori_id = dk.id")
				.where("penawaran_id = ? and dokumen_kategori_id = ? ")
				.params(penawaran_id,kategori_id)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariPenawaran(long userId, String status, String tanggalPengajuan, String keyword){
		List<Object> params = new ArrayList<>();

		String whereString = "pn.active = ?";
		params.add(1);
		if (!TextUtils.isEmpty(keyword)) {
            whereString += " AND (u.nama_usulan LIKE ? OR k.nama_komoditas LIKE ? OR p.nama_lengkap LIKE ?)";
			params.add("%" + StringEscapeUtils.escapeSql(keyword) + "%");
			params.add("%" + StringEscapeUtils.escapeSql(keyword) + "%");
			params.add("%" + StringEscapeUtils.escapeSql(keyword) + "%");
        }
		if(getAktifUser().isAdmin()){
//			whereString += " and (up.pokja_id = ? or pn.status = ? or pn.status = ?)";
//			params.add(userId);
//			params.add(Penawaran.STATUS_KONTRAK_DIPROSES);
//			params.add(Penawaran.STATUS_SELESAI);
		}else if(getAktifUser().isPokja()){
			whereString += " and up.pokja_id = ?";
			params.add(userId);
		}


		listPenawaranQuery(whereString,params,tanggalPengajuan,status, ResultsetHandlers.DAFTAR_PENAWARANSELURUH);

	}

	public static void cariPenawaranByUsulan(long usulan_id, String status, String tanggalPengajuan){
		List<Object> params = new ArrayList<>();
		String whereString = "";
		if (getAktifUser() != null) {
			whereString = "pn.active = ?";
			params.add(1);
			if (getAktifUser().isAdmin() && (status == null || status.isEmpty())) {
				whereString += " and (pn.usulan_id=? or pn.status = ?)";
				params.add(usulan_id);
				params.add(Penawaran.STATUS_KONTRAK_DIPROSES);
			} else {
				whereString += " and pn.usulan_id=? ";
				params.add(usulan_id);
			}
		}
		listPenawaranQuery(whereString,params,tanggalPengajuan,status, ResultsetHandlers.DAFTAR_PENAWARANUSULAN);

	}

	public static void cariPenawaranPenyedia(long user_id, String status, String tanggalPengajuan, String keyword){
		List<Object> params = new ArrayList<>();

		String whereString = "pn.user_id=? and pn.active = ?";
		params.add(user_id);
		params.add(1);

		if (!TextUtils.isEmpty(keyword)) {
            whereString += " AND (u.nama_usulan LIKE ? OR k.nama_komoditas LIKE ?)";
			params.add("%" + StringEscapeUtils.escapeSql(keyword) + "%");
			params.add("%" + StringEscapeUtils.escapeSql(keyword) + "%");
        }

		listPenawaranQuery(whereString,params,tanggalPengajuan,status, ResultsetHandlers.DAFTAR_PENAWARANPENYEDIA);
	}

	private static void listPenawaranQuery(String whereString, List<Object> params,
										   String tanggalPengajuan, String status, DatatableResultsetHandler<String[]> dbHandler){

		if(status != null && !status.isEmpty()){
			whereString += " AND pn.status = ?";
			params.add(status);
		}

		if(tanggalPengajuan != null && !tanggalPengajuan.isEmpty()){
			String result = "";
			try{
				Date tanggal = DateTimeUtils.formatStringToDate(tanggalPengajuan);
				result = DateTimeUtils.formatDateToString(tanggal,"YYYY-MM-dd");
				result = StringEscapeUtils.escapeSql(result)+"%";
			}catch (Exception e){
				e.printStackTrace();
			}
			whereString += " AND pn.created_date LIKE ?";
			params.add(result);
		}

		JsonObject json= new DatatableQuery(dbHandler)
				.select("pn.id, pn.penyedia_id, pn.user_id, p.nama_lengkap, pn.status, pn.created_date, u.nama_usulan, k.id as komoditas_id, k.nama_komoditas, up.pokja_id, u.doc_review_id, u.id as usulan_id")
				.from("penawaran pn " +
						"JOIN user p on p.id = pn.user_id " +
						"JOIN usulan u on pn.usulan_id = u.id " +
						"JOIN usulan_pokja up on up.usulan_id = u.id " +
						"JOIN komoditas k ON pn.komoditas_id = k.id")
				.where(whereString+" GROUP BY pn.id")
				.params(params.toArray(new Object[params.size()]))
				.executeQuery();
		renderJSON(json);
	}

	public static void cariDistributor(long penyedia_id){
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_DISTRIBUTOR)
				.select("id, penyedia_id, nama_distributor, no_telp, email, website, minta_disetujui, setuju_tolak")
				.from("penyedia_distributor")
				.where("penyedia_id = ? and active = ?")
				.params(penyedia_id, 1)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariPenyediaDistributor(long penyedia_id){
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_PENYEDIA_DISTRIBUTOR)
				.select("id, penyedia_id, nama_distributor, no_telp, email, website, minta_disetujui, setuju_tolak")
				.from("penyedia_distributor")
				.where("penyedia_id = ? and active = ?")
				.params(penyedia_id, 1)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariPenyediaPenawaran(long penyedia_id){
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_PENYEDIA_PENAWARAN)
				.select("pw.id, pw.penyedia_id, pw.komoditas_id, us.judul_pengumuman")
				.from("penawaran pw " +
						"left join usulan us on pw.usulan_id = us.id ")
				.where("pw.penyedia_id = ? ")
				.params(penyedia_id)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariPenyediaKontrak(long penyedia_id){
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_PENYEDIA_KONTRAK)
				.select("id,no_kontrak,dok_id_attachment,file_name,original_file_name,tgl_masa_berlaku_mulai,tgl_masa_berlaku_selesai,file_sub_location,apakah_berlaku")
				.from("penyedia_kontrak")
				.where("penyedia_id = ?")
				.params(penyedia_id)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariPenyediaKontrakPenawaran(long penyedia_id, long penawaran_id){
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_PENYEDIA_KONTRAK)
				.select("id,no_kontrak,dok_id_attachment,file_name,original_file_name,tgl_masa_berlaku_mulai,tgl_masa_berlaku_selesai,file_sub_location,apakah_berlaku")
				.from("penyedia_kontrak")
				.where("penyedia_id = ? and penawaran_id = ?")
				.params(penyedia_id, penawaran_id)
				.executeQuery();
		renderJSON(json);
	}

	public static void getKontrakPenyedia(long penyedia_id){
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_KONTRAK_PENYEDIA)
				.select("pk.id, pk.no_kontrak, pk.dok_id_attachment, pk.file_name, pk.original_file_name, pk.tgl_masa_berlaku_mulai, pk.tgl_masa_berlaku_selesai, pk.file_sub_location")
				.from("penyedia_kontrak pk " +
						"left join penawaran pw on pk.penawaran_id = pw.id and pw.active = 1 " +
						"left join penyedia py on pw.user_id = py.user_id and py.active = 1 ")
				.where("pk.active = 1 and py.id = ?")
				.params(penyedia_id)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariDokumenTemplate(){
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_DOKUMEN_TEMPLATE)
				.select("dt.id, kk.id as konten_kategori_id, dt.jenis_dokumen, dt.template, dt.active")
				.from("dokumen_template dt " +
						"RIGHT JOIN konten_kategori kk ON dt.konten_kategori_id = kk.id")
				.where("kk.active = 1 and kk.konten_kategori_utama_id = 6")
				.executeQuery();
		renderJSON(json);
	}

	public static void daftarPersetujuanProduk(long penawaran_id, long user_id, String keyword, long komoditas_id, long penyedia_id){
		StringBuilder whereQuery = new StringBuilder();
		List<Object> params = new ArrayList<>();

		whereQuery.append("us.status = ?");
		params.add(Usulan.STATUS_PENAWARAN_DIBUKA);

		//Update 20180813:
		//Penyedia boleh isi produk walau penawaran masih
		//MENUNGGU VERIFIKASI. Produk yang ditambahkan menjadi
		//MENUNGGU PERSETUJUAN tetapi belum boleh disetujui
		whereQuery.append(" and not ( p.status=? and pen.status=? )");
		params.add(Produk.STATUS_MENUNGGU_PERSETUJUAN);
		params.add(Penawaran.STATUS_MENUNGGU_VERIFIKASI);

		if(!CommonUtil.isEmpty(keyword)){
			whereQuery.append(" and (p.no_produk like ? or p.nama_produk like ? or user.nama_lengkap like ? or k.nama_komoditas like ?)");
			params.add("%"+keyword+"%");
			params.add("%"+keyword+"%");
			params.add("%"+keyword+"%");
			params.add("%"+keyword+"%");
		}

		if (komoditas_id != 0){
			whereQuery.append(" and k.id = ?");
			params.add(komoditas_id);
		}


		if(penyedia_id != 0){
			whereQuery.append(" and user.id = ?");
			params.add(penyedia_id);
		}


		if(penawaran_id != 0){
			whereQuery.append(" and p.penawaran_id = ?");
			params.add(penawaran_id);
		}

		if(AktifUser.getAktifUser().isPokja()){
			whereQuery.append(" and up.pokja_id = ?");
			params.add(user_id);
		}


		LogUtil.d("where",whereQuery);
		LogUtil.d("params",new Gson().toJson(params));

		DatatableQuery query = new DatatableQuery(ResultsetHandlers.DAFTAR_PERSETUJUAN_PRODUK)
				.select("distinct pm.id, pm.produk_id, p.no_produk, p.nama_produk, " +
						"k.id, k.nama_komoditas, user.id, user.nama_lengkap")
				.from("produk_tunggu_setuju pm join produk p on pm.produk_id = p.id join penawaran pen on p.penawaran_id = pen.id " +
						"join usulan us on pen.usulan_id = us.id " +
						"join usulan_pokja up on us.id = up.usulan_id " +
						"join user on p.created_by = user.id " +
						"join komoditas k on k.id = p.komoditas_id")
				.where(whereQuery.toString())
				.params(params.toArray(new Object[params.size()]));

		JsonObject json = query.executeQuery();

		renderJSON(json);
	}

	public static void cariProdukPenawaran(long penawaran_id){
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_PRODUKPENAWARAN)
				.select("p.id, p.komoditas_id, p.no_produk, p.no_produk_penyedia, p.nama_produk, p.apakah_dapat_dibeli, p.apakah_ditayangkan, " +
						"p.jenis_produk, p.jumlah_stok, p.jumlah_stok_inden, p.status, " +
						"pk.nama_kategori, m.nama_manufaktur, p.penawaran_id")
				.from("produk p " +
						"LEFT JOIN produk_kategori pk on pk.id = p.produk_kategori_id " +
						"LEFT JOIN manufaktur m on m.id = p.manufaktur_id")
				.where("p.penawaran_id = ?")
				.params(penawaran_id)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariParameter(){
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_PARAMETER)
				.select("id, nama_parameter, isi_parameter, deskripsi")
				.from("parameter_aplikasi")
				.where("active = ?")
				.params(1)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariRup(String option, String keyword, Integer year, Object... params){
		StringBuilder whereParams = new StringBuilder();
		whereParams.append("r.aktif = ? ");
		params = ArrayUtils.add(params, String.valueOf(1));

		whereParams.append("AND r.tahun_anggaran = ? ");
		params = ArrayUtils.add(params, String.valueOf(year));

		if(!CommonUtil.isEmpty(option) && !CommonUtil.isEmpty(keyword)){
			if(option.equalsIgnoreCase("idRup")){
				whereParams.append("AND r.id = ? ");
				params = ArrayUtils.add(params, keyword);
			} else if(option.equalsIgnoreCase("namaRup")){
				whereParams.append("AND r.nama LIKE ? ");
				params = ArrayUtils.add(params, "%"+keyword+"%");
			}
		}

		JsonObject json= new RupDatatableQuery(ResultsetHandlers.DAFTAR_RUP)
				.select("r.id, r.nama, r.tahun_anggaran, r.sumber_dana, r.id_sat_ker, k.id, k.nama, k.jenis, ks.id, ks.idKldi, ks.nama, " +
						"ks.alamat, ks.idSatker, k.prp_id, k.kbp_id")
				.from("rup r join kldi k on r.kode_kldi = k.id left join kldi_satker ks on r.id_sat_ker = ks.id")
				.where(whereParams.toString())
				.params(params)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariPpk(String option, String keyword, Object... params){
		StringBuilder whereParams = new StringBuilder();
		whereParams.append("p.active = ? ");
		params = ArrayUtils.add(params, String.valueOf(1));
		whereParams.append("and p.role_basic_id = ? ");
		params = ArrayUtils.add(params, String.valueOf(2));


		if(!CommonUtil.isEmpty(option) && !CommonUtil.isEmpty(keyword)){
			if(option.equalsIgnoreCase("namalengkap")){
				whereParams.append("AND p.nama_lengkap like ? ");
				params = ArrayUtils.add(params, "%" + keyword + "%");
			} else if(option.equalsIgnoreCase("username")){
				whereParams.append("AND p.user_name like ? ");
				params = ArrayUtils.add(params, "%" + keyword + "%");
			} else if(option.equalsIgnoreCase("email")){
				whereParams.append("AND p.user_email like ? ");
				params = ArrayUtils.add(params, "%" + keyword + "%");
			}
		}

		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_PPK)
				.select("p.id, p.nama_lengkap, p.user_name, p.user_email, p.jabatan, p.no_telp, p.nip, p.nomor_sertifikat_pbj")
				.from("user p")
				.where(whereParams.toString())
				.params(params)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariUlp(String option, String keyword, Object... params){
		StringBuilder whereParams = new StringBuilder();
		whereParams.append("p.active = ? ");
		params = ArrayUtils.add(params, String.valueOf(1));
		whereParams.append("AND p.role_basic_id = ? ");
		params = ArrayUtils.add(params, String.valueOf(3));
		whereParams.append("AND p.nip is not null ");


		if(!CommonUtil.isEmpty(option) && !CommonUtil.isEmpty(keyword)){
			if(option.equalsIgnoreCase("namalengkap")){
				whereParams.append("AND p.nama_lengkap like ? ");
				params = ArrayUtils.add(params, "%" + keyword + "%");
			} else if(option.equalsIgnoreCase("username")){
				whereParams.append("AND p.user_name = ? ");
				params = ArrayUtils.add(params, keyword);
			} else if(option.equalsIgnoreCase("email")){
				whereParams.append("AND p.user_email = ? ");
				params = ArrayUtils.add(params, keyword);
			}
		}

		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_ULP)
				.select("p.id, p.nama_lengkap, p.user_name, p.user_email, p.jabatan, p.no_telp, p.nip, p.nomor_sertifikat_pbj")
				.from("user p")
				.where(whereParams.toString())
				.params(params)
				.executeQuery();
		renderJSON(json);
	}

	public static void listKontrakAll(long penyedia_id, long kategori_id){
		JsonObject json= new DatatableQuery(ResultsetHandlers.LIST_KONTRAK_ALL)
				.select("du.id, km.nama_komoditas, pn.nama_penawaran, pe.nama_penyedia, dk.nama_kategori, du.nama_dokumen, du.file_path, du.active, du.created_date, pp.id as perpanjangan_id,du.penawaran_id")
				.from("dokumen_upload du " +
						"left join dokumen_kategori dk on du.dokumen_kategori_id = dk.id "
						+" join penawaran pn on pn.id = du.penawaran_id " +
						" join komoditas km on km.id = pn.komoditas_id " +
						"  join penyedia_kontrak pk on pk.penawaran_id = pn.id " +
						"  join penyedia pe on pe.id = pn.penyedia_id " +
						"  left join perpanjangan_kontrak pp on pn.id = pp.penawaran_id"
				)
				.where(" dokumen_kategori_id = ? ")
				.params(kategori_id)//penyedia_id
				.executeQuery();
		renderJSON(json);
	}

	public static void cariKomoditasKldi(String kldi_id){
		JsonObject json= new DatatableQuery(ResultsetHandlers.LIST_KOMODITAS_KLDI)
				.select("id, nama_komoditas")
				.from("komoditas")
				.where("kldi_id=?")
				.params(kldi_id)
				.executeQuery();
		renderJSON(json);
	}

    public static void cariKomoditasKldiPenyedia(String kldi_id){
        JsonObject json= new DatatableQuery(ResultsetHandlers.LIST_KOMODITAS_KLDI_PENYEDIA)
                .select("id, nama_komoditas")
                .from("komoditas")
                .where("kldi_id=?")
                .params(kldi_id)
                .executeQuery();
        renderJSON(json);
    }

	public static void cariUserKomoditasKldi(String komoditas_id){
		JsonObject json= new DatatableQuery(ResultsetHandlers.USER_KOMODITAS_KLDI)
				.select("o.id, o.komoditas_id, u.user_name, u.nama_lengkap, u.user_email, u.pps_id")
				.from("user_role_komoditas_override o " +
						" left join user u on o.user_id = u.id ")
				.where("o.komoditas_id = ?")
				.params(komoditas_id)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariUserPenyediaLokal(String komoditas_id){
		JsonObject json= new DatatableQuery(ResultsetHandlers.USER_PENYEDIA_LOKAL)
				.select("pk.id, pk.komoditas_id, u.user_name, u.nama_lengkap, u.user_email, u.rkn_id")
				.from("penyedia_komoditas pk " +
						" left join penyedia py on pk.penyedia_id = py.id " +
						" left join user u on py.user_id = u.id ")
				.where("pk.komoditas_id = ? AND pk.active = 1")
				.params(komoditas_id)
				.executeQuery();
		renderJSON(json);
	}

	public static void cariKomoditasNasional() {
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_KOMODITAS_NASIONAL)
				.select("id, nama_komoditas, kode_komoditas, jenis_produk, kelas_harga")
				.from("komoditas")
				.where("active = 1 AND komoditas_kategori_id <> ? AND komoditas_kategori_id <> ?")
				.params(5,6)
				.executeQuery();

		renderJSON(json);
	}

	public static void cariKomoditasLokal(long komoditas_kategori_id) {
		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_KOMODITAS_LOKAL)
				.select("kldi.id, kldi.nama")
				.from("(select distinct kldi_id " +
						"from komoditas " +
						"where active = 1 and komoditas_kategori_id = ? and kldi_id is not null) k join kldi on k.kldi_id = kldi.id")
				.params(komoditas_kategori_id)
				.executeQuery();

		renderJSON(json);
	}

	public static void cariBanner(){

		JsonObject json = new DatatableQuery(ResultsetHandlers.DAFTAR_BANNER)
				.select("id, subjek, file_name, original_file_name, active")
				.from("banner")
				.executeQuery();

		renderJSON(json);
	}

    public static void pembaruanPenyediaDistributor(String idDist, long penyedia_id){
		if(idDist == null || idDist.isEmpty()){
			JsonObject json = new DatatableQuery(ResultsetHandlers.DAFTAR_DISTRIBUTOR)
					.select("id, penyedia_id, nama_distributor, no_telp, email, website, minta_disetujui, setuju_tolak")
					.from("penyedia_distributor")
					.where("penyedia_id = ? and active = ? ")
					.params(penyedia_id, 1)
					.executeQuery();
				renderJSON(json);
		}else{

			JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_DISTRIBUTOR)
					.select("id, penyedia_id, nama_distributor, no_telp, email, website, minta_disetujui, setuju_tolak")
					.from("penyedia_distributor")
					.where("penyedia_id = ? and active = ? and id not in ("+idDist+")")
					.params(penyedia_id, 1)
					.executeQuery();
			renderJSON(json);
		}
    }

	public static void pengajuanPembaruanPenyediaDistributor(List<String> idDist, long idPembaruan){
		if(idDist != null) {
			String joined = String.join(", ", idDist);
			JsonObject json = new DatatableQuery(ResultsetHandlers.DAFTAR_PEMBARUAN_PENYEDIA_DISTRIBUTOR)
					.select(" IF(pd.id IS NULL, NULL, pd.id) as id,\n" +
							"\tIF(pd.penyedia_id IS NULL, JSON_EXTRACT(pg.object_json,'$.distributor.penyedia_id') , pd.penyedia_id) as penyedia_id,\n" +
							"\tIF(pd.nama_distributor IS NULL, REPLACE(JSON_EXTRACT(pg.object_json,'$.distributor.nama_distributor'),'\"',''), pd.nama_distributor) as nama_distributor,\n" +
							"\tIF(pd.no_telp IS NULL, REPLACE( JSON_EXTRACT(pg.object_json,'$.distributor.no_telp'),'\"','') , pd.no_telp) as no_telp,\n" +
							"\tIF(pd.email IS NULL, REPLACE(JSON_EXTRACT(pg.object_json,'$.distributor.email'),'\"','') , pd.email) as email,\n" +
							"\tIF(pd.website IS NULL, REPLACE( JSON_EXTRACT(pg.object_json,'$.distributor.website'),'\"','') , pd.website) as website,\n" +
							"\tIF(pd.minta_disetujui IS NULL, REPLACE( JSON_EXTRACT(pg.object_json,'$.distributor.minta_disetujui'),'\"',''), pd.minta_disetujui) as minta_disetujui,\n" +
							"\tIF(pd.setuju_tolak IS NULL, REPLACE( JSON_EXTRACT(pg.object_json,'$.distributor.setuju_tolak'),'\"',''), pd.setuju_tolak) as setuju_tolak, " +
							"\tpg.apakah_disetujui, "+idPembaruan+" as idpemb , pg.id as pstgid")
					.from(" penyedia_distributor pd\n" +
							"RIGHT JOIN permohonan_pembaruan_staging pg on \n" +
							"JSON_EXTRACT(pg.object_json,'$.distributor.id') = pd.id and \n" +
							"JSON_EXTRACT(pg.object_json,'$.distributor.id') in ("+joined+")")
					.where("pg.active = 1 and pg.permohonan_pembaruan_id ="+idPembaruan)
					.executeQuery();
			renderJSON(json);
		}else{
			renderJSON(DatatableQuery.createEmptyResult());
		}
	}

	public static void listApprovalDistributor(List<String> idDist, long idPembaruan){
		if(idDist != null) {
			String joined = String.join(", ", idDist);
			JsonObject json = new DatatableQuery(ResultsetHandlers.DAFTAR_APPROVAL_PEMBARUAN_PENYEDIA_DISTRIBUTOR)
					.select(" IF(pd.id IS NULL, NULL, pd.id) as id,\n" +
							"\tIF(pd.penyedia_id IS NULL, JSON_EXTRACT(pg.object_json,'$.distributor.penyedia_id') , pd.penyedia_id) as penyedia_id,\n" +
							"\tIF(pd.nama_distributor IS NULL, REPLACE(JSON_EXTRACT(pg.object_json,'$.distributor.nama_distributor'),'\"',''), pd.nama_distributor) as nama_distributor,\n" +
							"\tIF(pd.no_telp IS NULL, REPLACE( JSON_EXTRACT(pg.object_json,'$.distributor.no_telp'),'\"','') , pd.no_telp) as no_telp,\n" +
							"\tIF(pd.email IS NULL, REPLACE(JSON_EXTRACT(pg.object_json,'$.distributor.email'),'\"','') , pd.email) as email,\n" +
							"\tIF(pd.website IS NULL, REPLACE( JSON_EXTRACT(pg.object_json,'$.distributor.website'),'\"','') , pd.website) as website,\n" +
							"\tIF(pd.minta_disetujui IS NULL, REPLACE( JSON_EXTRACT(pg.object_json,'$.distributor.minta_disetujui'),'\"',''), pd.minta_disetujui) as minta_disetujui,\n" +
							"\tIF(pd.setuju_tolak IS NULL, REPLACE( JSON_EXTRACT(pg.object_json,'$.distributor.setuju_tolak'),'\"',''), pd.setuju_tolak) as setuju_tolak, " +
							"\tpg.apakah_disetujui, "+idPembaruan+" as idpemb , pg.id as pstgid")
					.from(" penyedia_distributor pd\n" +
							"RIGHT JOIN permohonan_pembaruan_staging pg on \n" +
							"JSON_EXTRACT(pg.object_json,'$.distributor.id') = pd.id and \n" +
							"JSON_EXTRACT(pg.object_json,'$.distributor.id') in ("+joined+")")
					.where("pg.active = 1 and pg.permohonan_pembaruan_id ="+idPembaruan)
					.executeQuery();
			renderJSON(json);
		}else{
			renderJSON(DatatableQuery.createEmptyResult());
		}
	}

	public static void cariKldi(String keyword, String searchType){
		StringBuilder whereParams = new StringBuilder();

		if(!CommonUtil.isEmpty(searchType) && !CommonUtil.isEmpty(keyword)){
			if(searchType.equalsIgnoreCase("lokal")){
				whereParams.append("(jenis LIKE 'kabupaten' OR jenis LIKE 'kota' OR jenis LIKE 'provinsi') and nama like '%"+keyword+"%'");
			} else {
				whereParams.append("(jenis NOT LIKE 'kabupaten' AND jenis NOT LIKE 'kota' AND jenis NOT LIKE 'provinsi') and nama like '%"+keyword+"%'");
			}
		}

		JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_KLDI)
				.select("id, nama, jenis")
				.from("kldi")
				.where(whereParams.toString())
				.executeQuery();
		renderJSON(json);
	}

}
