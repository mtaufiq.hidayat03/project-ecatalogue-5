package models.purchasing.riwayat;

import models.BaseKatalogTable;
import models.common.AktifUser;
import models.penyedia.PenyediaDistributor;
import models.purchasing.Paket;
import models.purchasing.pembayaran.Pembayaran;
import models.purchasing.penerimaan.PaketRiwayatPenerimaan;
import models.purchasing.pengiriman.PaketRiwayatPengiriman;
import models.purchasing.status.PaketStatus;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.DateTimeUtils;
import utils.KatalogUtils;

import javax.persistence.Transient;
import java.util.List;

/**
 * @author HanusaCloud on 10/16/2017
 */
@Table(name = "paket_riwayat")
public class PaketRiwayat extends BaseKatalogTable {

    @Id
    public Long id;
    public String deskripsi;
    public Long paket_id;
    public String status_paket;
    public Integer active = 1;

    public static final String DESKRIPSI_PAKET_DIBUAT = "Paket Dibuat";
    public static final String DESKRIPSI_PAKET_DIUBAH = "Paket diubah";
    public static final String DESKRIPSI_PAKET_UPLOAD_KONTRAK = "Input dokumen kontrak baru. No. Dokumen Kontrak : ";

    @Transient
    public String nama_role_alias = "";
    @Transient
    public String nama_lengkap = "";
    @Transient
    public String user_name = "";

    public String getCreatedDate() {
        return DateTimeUtils.parseToReadableDateTime(created_date);
    }

    public String getNameAndUsername() {
        return String.format("%1s (%2s)", nama_lengkap, user_name);
    }

    public String getRoleAlias() {
        return nama_role_alias;
    }

    public static List<PaketRiwayat> getHistoriesByPackageId(Long id) {
        final String query = "SELECT pr.deskripsi," +
                "u.user_name, u.nama_lengkap, rb.nama_role_alias, pr.created_by, pr.created_date " +
                "FROM `paket_riwayat` pr " +
                "JOIN user u " +
                "ON u.id = pr.created_by " +
                "LEFT JOIN role_basic rb " +
                "ON rb.id = u.role_basic_id " +
                "WHERE paket_id =? " +
                "ORDER BY pr.created_date ASC;";
        return Query.find(query, PaketRiwayat.class, id).fetch();
    }

    public void addDeliveryHistory(PaketRiwayatPengiriman model) {
        String message = "Input dokumen pengiriman baru. No. Dokumen Pengiriman (LKPP): " + model.no_dokumen_sistem;
        set(model.paket_id, message);
        createHistory();
    }

    public void addAcceptanceHistory(PaketRiwayatPenerimaan model) {
        String message = "Input dokumen penerimaan baru. No. Dokumen Penerimaan (LKPP): " + model.paket_pengiriman_no_dokumen_sistem;
        set(model.paket_id, message);
        createHistory();
    }

    public void addPaymentHistory(Pembayaran model) {
        String message = "Input dokumen pembayaran baru, No. Invoice " + model.no_invoice;
        if (!model.isPayment()) {
            message = "Input dokumen penerimaan pembayaran No. Invoice " + model.no_invoice;
        }
        set(model.paket_id, message);
        createHistory();
    }

    public void addDistributorHistory(Long packageId, PenyediaDistributor model) {
        set(packageId, "Penyedia menetapkan distributor " + model.nama_distributor);
        createHistory();
    }

    public void addApprovalHistory(PaketStatus model, String user, boolean isApproved) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(KatalogUtils.ucfirst(user));
        if (isApproved) {
            stringBuilder.append(" menyetujui");
        } else {
            stringBuilder.append(" menolak");
        }
        stringBuilder.append(" paket : '" + model.deskripsi + "'");
        set(model.getPackageId(), stringBuilder.toString());
        this.status_paket = model.status_paket;
        createHistory();
    }

    public void addSendToPpk(Long packageId) {
        set(packageId, "Panitia mengirimkan paket ke PPK untuk persetujuan.");
        createHistory();
    }

    public void addPackageFinished(Paket model) {
        set(model.getId(), "PPK telah menyelesaikan paket.");
        createHistory();
    }

    public void addCancelHistory(Paket model) {
        AktifUser au = AktifUser.getAktifUser();
        boolean isPPK = au.isPpk();
        boolean ppkIsPanitia = au.getUser().id.equals(model.panitia_user_id) ? true : false;
        if (ppkIsPanitia || isPPK == false) {
            set(model.getId(), "Panitia membatalkan paket.");
        }
        if (isPPK && ppkIsPanitia == false){
            set(model.getId(), "PPK membatalkan paket.");
        }
        createHistory();
    }

    public void set(Long paket_id, String message) {
        this.paket_id = paket_id;
        this.deskripsi = message;
    }

    private void createHistory() {
        this.active = 1;
        save();
    }

    public static PaketRiwayat findLastesPaketRiwayat(Long idPaket){
        return PaketRiwayat.find("paket_id = ? and active = 1 ORDER BY created_date desc LIMIT 1", idPaket).first();
    }

}
