package models.elasticsearch.query;

import com.google.gson.JsonObject;

/**
 * @author HanusaCloud on 10/5/2017
 */
public class BoolFilter extends NestedObject {

    private String value;

    public BoolFilter setField(String field) {
        this.field = field;
        return this;
    }

    public BoolFilter setValue(String value) {
        this.value = value;
        return this;
    }

    public BoolFilter setValue(Boolean value) {
        this.value = String.valueOf(value);
        return this;
    }

    public BoolFilter setValue(Long value) {
        this.value = String.valueOf(value);
        return this;
    }

    @Override
    public JsonObject build() {
        JsonObject term = new JsonObject();
        JsonObject inner = new JsonObject();
        inner.addProperty(field, value);
        term.add("term", inner);
        return term;
    }
}
