package models.prakatalog.search;

import ext.contracts.ParamAble;
import org.apache.http.message.BasicNameValuePair;
import play.mvc.Scope;

import java.util.ArrayList;
import java.util.List;

public class UsulanSearch implements ParamAble {

    public String keyword = "";
    public long komoditas = 0;
    public String status = "";
    public String urutkan = "";
    public long uid = 0;
    public long per_page = 20;
    public int page = 0;

    public UsulanSearch(Scope.Params params) {
        this.keyword = !isNull(params,"keyword") ? params.get("keyword") : "";
        this.status = !isNull(params, "status") ? params.get("status") : "";
        this.urutkan = !isNull(params, "urutkan") ? params.get("urutkan") : "";
        this.komoditas = !isNull(params, "komoditas") ? params.get("komoditas", Long.class) : 0;
        this.uid = !isNull(params, "uid") ? params.get("uid", Long.class) : 0;
        this.per_page = !isNull(params, "per_page") ? params.get("per_page", Long.class) : 20;
        this.page = getCurrentPage(params);
    }

    @Override
    public List<BasicNameValuePair> getParams() {
        List<BasicNameValuePair> results = new ArrayList<>();
        results.add(new BasicNameValuePair("keyword", this.keyword));
        results.add(new BasicNameValuePair("status", this.status));
        results.add(new BasicNameValuePair("urutkan", this.urutkan));
        results.add(new BasicNameValuePair("komoditas", String.valueOf(this.komoditas)));
        results.add(new BasicNameValuePair("uid", String.valueOf(this.uid)));
        results.add(new BasicNameValuePair("per_page", String.valueOf(this.per_page)));
        return results;
    }

    @Override
    public int getPage() {
        return page;
    }
}
