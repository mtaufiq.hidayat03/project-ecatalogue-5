package constants.paket;

/**
 * @author HanusaCloud on 11/2/2017
 */
public class PaketPengirimanConstant {

    public static final String TABLE_NAME = "paket_pengiriman";

    public static final String ID = "id";
    public static final String PAKET_ID = "paket_id";
    public static final String NO_DOKUMEN_SISTEM = "no_dokumen_sistem";
    public static final String NO_DOKUMEN = "no_dokumen";
    public static final String TANGGAL_DOKUMEN = "tanggal_dokumen";
    public static final String DESKRIPSI = "deskripsi";
    public static final String TOTAL_PRODUK = "agr_paket_pengiriman_produk";
    public static final String VERIFICATION_ID = "paket_penerimaan_pembayaran_id";

    public static final String MODIFIED_DATE = "modified_date";
    public static final String MODIFIED_BY = "modified_by";

}
