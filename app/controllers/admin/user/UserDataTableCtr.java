package controllers.admin.user;

import com.google.gson.JsonObject;
import controllers.BaseController;
import models.datatable.ResultsetHandlers;
import models.jcommon.datatable.DatatableQuery;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.util.TextUtils;
import play.Logger;
import utils.LogUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 11/24/2017
 */
public class UserDataTableCtr extends BaseController {

    public static final String TAG = "UserDataTableCtr";

    public static void cariUser(UserSearch model) {
        LogUtil.d(TAG, model);
        String where = " ";
        List<Object> params = new ArrayList<>();

        if(!TextUtils.isEmpty(model.keyword)){
            model.keyword = "%" + StringEscapeUtils.escapeSql(model.keyword) + "%";
            where += "AND (nama_lengkap LIKE ? or user_name LIKE ?) ";
            params.add(model.keyword);
            params.add(model.keyword);
        }

        if(model.role > 0){
            where += "AND ul.id = ? ";
            params.add(model.role);
        }

        if(model.groupRole > 0){
            where += "AND urg.id = ? ";
            params.add(model.groupRole);
        }

        if(model.overrideGroupRole > -1){
            where += "AND override_role_grup = ? ";
            params.add(model.overrideGroupRole);
        }

        if(model.overrideCommodity > -1){
            where += "AND override_role_komoditas = ? ";
            params.add(model.overrideCommodity);
        }

        if(model.overridePackage > -1){
            where += "AND override_role_paket = ?";
            params.add(model.overridePackage);
        }

        JsonObject json = new DatatableQuery(UserResultHandler.DAFTAR_USER)
                .select("u.id, nama_lengkap, user_name, user_email, u.active, " +
                        "urg.nama_grup as grup_role, ul.nama_role_alias")
                .from("user u, user_role_grup urg, role_basic ul")
                .where("u.user_role_grup_id = urg.id AND urg.role_basic_id = ul.id" + where)
                .params(params.toArray(new Object[params.size()]))
                .executeQuery();

        renderJSON(json);
    }

    public static void pesertaUsulan(Long usulan_id){
        Logger.info("peserta "+usulan_id);
        JsonObject json= new DatatableQuery(UserResultHandler.DAFTAR_PESERTA)
                .select("u.id, u.nama_lengkap, u.user_name, u.user_email, pw.user_id, u.active ")
                .from("user u " +
                        "JOIN peserta_usulan pu ON pu.user_id = u.id " +
                        "JOIN usulan us ON us.id = pu.usulan_id " +
                        "LEFT JOIN penawaran pw ON pu.user_id = pw.user_id AND pu.usulan_id = pw.usulan_id ")
                .where("us.id = ?")
                .params(usulan_id)
                .executeQuery();
        renderJSON(json);
    }


}
