package functional;

import models.katalog.Komoditas;
import models.masterdata.KldiSatker;
import models.penyedia.Penyedia;
import models.purchasing.Paket;
import models.purchasing.search.PurchasingSearchResult;
import models.purchasing.status.PaketStatus;
import models.user.User;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

import static play.mvc.Http.Request;
import static play.mvc.Http.Response;

/**
 * @author HanusaCloud on 5/28/2020 10:49 AM
 */
public class PaketTest extends BaseCase {

    @Before
    public void setUp() throws IOException {
        super.setUp();
        cleanUp();
        setData();
    }

    public void cleanUp() {
        Komoditas.deleteAll();
        PaketStatus.deleteAll();
        Paket.deleteAll();
        KldiSatker.deleteAll();
        Penyedia.deleteAll();
    }

    private void setData() {
        try {
            Map<Long, User> userMap = initUsers("/json/paket/katalog5_user.json", userRoleGrupMap, null);
            Map<Long, Penyedia> providerMap = initProvider("/json/paket/katalog5_penyedia.json", userMap);
            Map<Long, Komoditas> commodityMap = initCommodity("/json/paket/katalog5_komoditas.json");
            Map<Long, KldiSatker> kldiSatkerMap = initKldiSatker("/json/paket/katalog5_kldi_satker.json");
            Map<Long, Paket> paketMap = initPaket(
                    "/json/paket/katalog5_paket.json",
                    commodityMap,
                    providerMap,
                    kldiSatkerMap
            );
            initPaketStatus("/json/paket/katalog5_paket_status.json", paketMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private PurchasingSearchResult sendRequest(String keyword) {
        Response loginResponse = loginAdmin();
        Request request = newRequest();
        request.cookies.putAll(loginResponse.cookies);
        request.method = "GET";
        request.remoteAddress = "0.0.0.0";
        GET(request, "/purchasing/paket?commodity=0&status=&position=&negotiation=&sortby=desc&per_page=20" + keyword);
        return castObj(renderArgs("model"), PurchasingSearchResult.class);
    }

    @Test
    public void shouldBeAbleToShowList() {
        PurchasingSearchResult result = sendRequest("");
        assertEquals(5, result.getTotal());
    }

    @Test
    public void shouldBeAbleToSearchByPackageNumber() {
        PurchasingSearchResult result = sendRequest("&keyword=T00-P2004-45");
        assertEquals(1, result.getTotal());
    }

}
