package models.purchasing.contracts;

import models.katalog.Produk;
import models.purchasing.Paket;
import models.purchasing.riwayat.PaketRiwayatNegosiasi;
import utils.UrlUtil;

import java.util.List;

/**
 * @author HanusaCloud on 11/5/2017
 */
public interface PackageProductContract {

    Long getId();

    Long getPackageId();

    Long getProductId();

    Long getPaketId();

    void setProduct(Produk product);

    void setPaket(Paket paket);

    void setPackageProductNegotiations(List<PaketRiwayatNegosiasi> list);

    default void withProductNegotiations() {
        if (getId() != null && getPackageId() != null) {
            setPackageProductNegotiations(PaketRiwayatNegosiasi.getHistoriesByPackageIdAndProductId(getPackageId(), getId()));
        }
    }

    default String getDetailHistoryUrl() {
        return UrlUtil.builder()
                .setUrl("purchasing.PaketCtr.detailRiwayatNegosiasi")
                .setParam("id", getId())
                .setParam("paket_id", getPackageId())
                .build();
    }

    default void withProduct(){
        if(getProductId() != null){
            setProduct(Produk.findById(getProductId()));
        }
    }

    default void withPaket(){
        if(getPaketId() != null){
            setPaket(Paket.findById(getPaketId()));
        }
    }

}
