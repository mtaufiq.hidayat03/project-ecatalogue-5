package models.penyedia;

import models.BaseKatalogTable;
import org.apache.http.util.TextUtils;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dadang on 7/11/17.
 */
@Table(name = "penyedia_representatif")
public class PenyediaRepresentatif extends BaseKatalogTable {

	@Id
	public Long id;
	public Long penyedia_id;
	public String nama;
	public String email;
	public String no_telp;
	public boolean active;

	public static List<PenyediaRepresentatif> setList(String[] nama_representatif,
													  String[] telp_representatif, String[] email_representatif){

		List<PenyediaRepresentatif> result = new ArrayList<PenyediaRepresentatif>();
		if(nama_representatif != null){
			Integer[] arrayTotal = {nama_representatif.length,telp_representatif.length,email_representatif.length};
			int maxTotal = getMaxLengthRepresentatif(arrayTotal);

			for(int i = 0; i < maxTotal ; i++){

				PenyediaRepresentatif rep = new PenyediaRepresentatif();
				rep.nama = TextUtils.isEmpty(nama_representatif[i]) ? "" : nama_representatif[i];
				rep.email = TextUtils.isEmpty(email_representatif[i]) ? "" : email_representatif[i];
				rep.no_telp = TextUtils.isEmpty(telp_representatif[i]) ? "" : telp_representatif[i];

				result.add(rep);

			}

			return result;
		}

		return null;
	}

	private static int getMaxLengthRepresentatif(Integer[] array) {
		int max = 0;

		for (int counter = 1; counter < array.length; counter++) {
			if (array[counter] > max) {
				max = array[counter];
			}
		}

		return max;
	}

	public static List<PenyediaRepresentatif> getByProviderId(Long id) {
		return find("penyedia_id =? AND active >0", id).fetch();
	}

	public static List<PenyediaRepresentatif> findByPenyedia(long id){
		return find("penyedia_id = ? and active = ?",id, AKTIF).fetch();
	}

	public static void deleteByPenyedia(long id){
		delete("penyedia_id = ?",id);
	}

}
