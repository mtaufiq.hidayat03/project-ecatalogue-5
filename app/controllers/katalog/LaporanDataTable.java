package controllers.katalog;

import com.google.gson.JsonObject;
import controllers.BaseController;
import models.common.AktifUser;
import models.datatable.ResultsetHandlers;
import models.jcommon.datatable.DatatableQuery;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.util.TextUtils;
import utils.LogUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Fajar on 06/04/2018
 */
public class LaporanDataTable extends BaseController {

    public static void cariLaporan(String keyword, String status, String kategori, String pengguna, String komoditas_id){
        List<Object> params = new ArrayList<>();

        String whereString = "1 = 1";

        if(!TextUtils.isEmpty(keyword)) {
            keyword = "%" + StringEscapeUtils.escapeSql(keyword) + "%";
            whereString += " AND (p.nama_produk LIKE ? or p.no_produk like ? or py.nama_penyedia LIKE ? or r.reporter_name LIKE ? or r.reporter_email LIKE ?)";
            params.add(keyword);
            params.add(keyword);
            params.add(keyword);
            params.add(keyword);
            params.add(keyword);
        }

        if(!TextUtils.isEmpty(status)){
            whereString += " AND r.status = ?";
            params.add(status);
        }

        if(!TextUtils.isEmpty(kategori)){
            whereString += " AND r.konten_kategori_id = ?";
            params.add(kategori);
        }

        if(!TextUtils.isEmpty(pengguna)){
            if(pengguna.equals("99")){
                whereString += " AND u.role_basic_id is null";
            }else{
                whereString += " AND u.role_basic_id = ?";
                params.add(pengguna);
            }
        }

        if(!TextUtils.isEmpty(komoditas_id)){
            whereString += " AND p.komoditas_id = ?";
            params.add(komoditas_id);
        }

        if(!AktifUser.getAktifUser().isAdmin()){
            whereString += " AND r.reporter_id = ?";
            params.add(AktifUser.getActiveUserId());
        }

        JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_PRODUKLAPORAN)
                .select("r.*, p.id as produk_id, p.komoditas_id, p.no_produk, p.nama_produk, py.nama_penyedia, u.role_basic_id, k.kelas_harga")
                .from("report r " +
                        "left join produk p on r.item_id = p.id " +
                        "join komoditas k on k.id = p.komoditas_id " +
                        "left join penyedia py on p.penyedia_id = py.id " +
                        "left join user u on r.reporter_id = u.id ")
                .where(whereString)
                .params(params.toArray(new Object[params.size()]))
                .executeQuery();

        renderJSON(json);
    }
}
