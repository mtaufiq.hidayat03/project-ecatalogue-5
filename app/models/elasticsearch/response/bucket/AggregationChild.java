package models.elasticsearch.response.bucket;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 10/10/2017
 */
public class AggregationChild {

    @SerializedName("sum_other_doc_count")
    public long sumDocCount = 0;
    public List<Bucket> buckets = new ArrayList<>();

}
