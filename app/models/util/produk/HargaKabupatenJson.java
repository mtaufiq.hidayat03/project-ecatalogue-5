package models.util.produk;

/**
 * Created by dadang on 8/23/17.
 */
public class HargaKabupatenJson {

	public long kabupatenId;
	public double harga_utama;
	public double harga;
	public double harga_pemerintah;
	public double ongkos_kirim;
}