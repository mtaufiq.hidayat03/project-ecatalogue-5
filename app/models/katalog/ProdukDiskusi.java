package models.katalog;

import models.BaseKatalogTable;
import models.cms.PollingPilihan;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import java.util.ArrayList;
import javax.persistence.Transient;
import java.util.List;

@Table(name ="produk_diskusi")

public class ProdukDiskusi extends BaseKatalogTable {
    @Id
    public Long id;
    public Long produk_id;
    public Long user_id;
    public int user_penerima;
    public int status_komentar;
    public String user_nama;
    public String user_foto;
    public String diskusi;
    public Boolean active;

    @Transient
    public String as_nama_produk;
    @Transient
    public int jumlah_diskusi_produk;

    public static List<ProdukDiskusi> findProdukDiskusiByIdProduk(Long produk_id){
        List<ProdukDiskusi> list = new ArrayList<>();
        try{
            list = ProdukDiskusi.find("produk_id = ?", produk_id).fetch();
        }catch (Exception e){}
        return list;
    }

    public static List<ProdukDiskusi> findProdukDiskusiByUserIdLogin(Long user_id){
        
        String query = "SELECT pd.produk_id, pd.modified_date, prdk.nama_produk as as_nama_produk, COUNT( pd.produk_id ) AS jumlah_diskusi_produk, SUM( CASE WHEN pd.status_komentar = 1 THEN 1 ELSE 0 END ) AS status_komentar "+
                        "FROM produk_diskusi pd "+
                        "LEFT JOIN produk prdk ON prdk.id = pd.produk_id "+
                        "WHERE pd.active = 1 and pd.user_penerima = "+user_id+" " +
                        "GROUP BY pd.produk_id "+
                        "ORDER BY pd.modified_date desc";
        return Query.find(query,ProdukDiskusi.class).fetch();
    }
}
