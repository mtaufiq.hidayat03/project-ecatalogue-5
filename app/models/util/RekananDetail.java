package models.util;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import models.penyedia.Penyedia;
import models.penyedia.PenyediaDistributor;
import play.libs.WS;
import repositories.penyedia.ProviderRepository;

import java.util.Date;
import java.util.List;

public class RekananDetail {

	public long rkn_id;
	public String rkn_nama;
	public String btu_id;
	public long kbp_id;
	public String rkn_alamat;
	public String rkn_kodepos;
	public String rkn_telepon;
	public String rkn_npwp;
	public String rkn_statcabang;
	public String rkn_email;
	public String rkn_fax;
	public String rkn_mobile_phone;
	public String rkn_pkp;
	public String rkn_website;
	public String rkn_telppusat;
	public String rkn_emailpusat;
	public String rkn_namauser;
	public int rkn_isactive;
	public int rkn_status;
	public String passw;
	public int isbuiltin;
	public int disableuser;
	public int paket;
	public long npwp;
	public int npwp_mirip;
	public Date rkn_tgl_daftar;
	public Date rkn_tgl_setuju;
	public Date auditupdate;
	public String audittype;
	public String audituser;
	public long repo_id;
	public long user_id;


	public static List<RekananDetail> populateIfNotPenyedia(RekananDetail rkn, List<RekananDetail> result){
		Penyedia penyedia = ProviderRepository.findByRknId(rkn.rkn_id);
		if(penyedia == null){
			result.add(rkn);
		}

		return result;
	}

	public static List<RekananDetail> populateIfNotDistributor(RekananDetail rkn, List<RekananDetail> result){
		PenyediaDistributor distributor = PenyediaDistributor.findByRknId(rkn.rkn_id);
		if(distributor == null){
			result.add(rkn);
		}

		return result;
	}

	public static RekananDetail getRekananDetailFromAdp(long rekananId){
		Config config = Config.getInstance();
		RekananDetail result = new RekananDetail();
		try{
			String rkn = config.adp_detail_user + "?q=" + Encryption.encodeUrlSafe(Encryption.encrypt(String.valueOf(rekananId).getBytes()));
			String content = WS.url(rkn).timeout("5min").getAsync().get().getString();
			String response = Encryption.decrypt(content);

			result = new Gson().fromJson(response, RekananDetail.class);
		}catch (Exception e){
			e.printStackTrace();
		}

		return result;
	}

	public static RekananDetail getIdRekananDefault(long rekananId){
		Config config = Config.getInstance();
		RekananDetail result = new RekananDetail();
		String q = Encryption.encodeUrlSafe(Encryption.encrypt(String.valueOf(rekananId).getBytes()));

		try{
			String json = WS.url(config.adp_aktivasi_rekanan + "?q=" + q).timeout("5min").getAsync().get().getString();
			String responseJson = Encryption.decrypt(json);
			JsonParser JP = new JsonParser();
			JsonElement JE = JP.parse(responseJson);
			JsonObject JO = JE.getAsJsonObject().get("aktivasiPK").getAsJsonObject();
			Long aktivasi_id = JO.get("aktivasi_id").getAsLong();
			q = Encryption.encodeUrlSafe(Encryption.encrypt(String.valueOf(aktivasi_id).getBytes()));

			try{
				String rkn = WS.url(config.adp_rekanan_terpilih + "?q=" + q).timeout("5min").getAsync().get().getString();
				String responseRkn = Encryption.decrypt(rkn);
				result = new Gson().fromJson(responseRkn, RekananDetail.class);
			}catch (Exception e){
				e.printStackTrace();
			}
		}catch (Exception e){
			e.printStackTrace();
		}

		return result;
	}
	
}
