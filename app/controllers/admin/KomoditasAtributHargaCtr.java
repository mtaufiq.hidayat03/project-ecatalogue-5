package controllers.admin;

import com.google.gson.Gson;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.katalog.ProdukHarga;
import models.katalog.komoditasmodel.KomoditasAtributHarga;
import models.katalog.form.FormAtributHarga;
import models.secman.Acl;
import utils.LogUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KomoditasAtributHargaCtr extends BaseController {

	private static final String createUrl = "admin/KomoditasAtributHargaCtr/create.html";

	@AllowAccess({Acl.COM_KOMODITAS_HARGA_ATRIBUT})
	public static void index(long komoditas_id) {
		render(komoditas_id);
	}

	@AllowAccess({Acl.COM_KOMODITAS_HARGA_ATRIBUT})
	public static void create(long komoditas_id) {
		List<KomoditasAtributHarga> atributHargaList = KomoditasAtributHarga.findByKomoditas(komoditas_id);
		FormAtributHarga atributHarga = new FormAtributHarga(atributHargaList, komoditas_id);
		LogUtil.d("create",new Gson().toJson(atributHarga));

		String json = KomoditasAtributHarga.getJsonUsedAtributFromList(atributHarga.atribut_id);
		render(komoditas_id, atributHarga, json);
	}

	@AllowAccess({Acl.COM_KOMODITAS_HARGA_ATRIBUT})
	public static void createSubmit(FormAtributHarga atributHarga){

		long komoditas_id = atributHarga.komoditas_id;

		if(atributHarga == null || atributHarga.nama_atribut == null){
			flash.error("Anda belum mengisi Atribut Harga Baru. Silakan tambah dan isi terlebih dahulu informasi atribut harga.");
			render(createUrl, komoditas_id, atributHarga);
		}else if(atributHarga.ongkir == null && atributHarga.harga_utama == null && atributHarga.harga_pemerintah == null && atributHarga.harga_retail == null){
			flash.error("Anda belum memilih jenis harga (ongkos kirim/harga utama/harga pemerintah/harga retail). Silakan cek kembali.");
			LogUtil.d("createSubmit",new Gson().toJson(atributHarga));
			render(createUrl, komoditas_id, atributHarga);
		}else{

			KomoditasAtributHarga.saveAtributHarga(atributHarga);
			index(atributHarga.komoditas_id);

		}

	}

	@AllowAccess({Acl.COM_KOMODITAS_KATEGORI_EDIT})
	public static void edit(long id){
		
	}

	@AllowAccess({Acl.COM_KOMODITAS_KATEGORI_EDIT})
	public static void editSubmit(long kk_id, String nama_kategori, String deskripsi){

	}

	@AllowAccess({Acl.COM_KOMODITAS_HARGA_ATRIBUT})
	public static void checkId(long id){
		Long countProdukHarga = ProdukHarga.countByHargaAtribut(id);
		LogUtil.d("countProdukHarga",countProdukHarga);
		boolean status = countProdukHarga == 0;
		renderJSON(status);
	}

	@AllowAccess({Acl.COM_KOMODITAS_HARGA_ATRIBUT})
	public static void delete(long id){
		Map<String,Object> result = new HashMap<>();
		boolean status = true;
		Long countProdukHarga = ProdukHarga.countByHargaAtribut(id);
		if(countProdukHarga > 0){
			status = false;
		}else{
			KomoditasAtributHarga.softDelete(id);
		}

		result.put("status",status);
		result.put("id",id);
		renderJSON(result);
	}

}
