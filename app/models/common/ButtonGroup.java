package models.common;

public class ButtonGroup {

	public String url;

	public String nama;

	public Long id;
	
	public boolean isDelete = false;

	public boolean isSelesaiNegosiasi = false;

	public boolean openEdit = false;

	public ButtonGroup(){}

	public ButtonGroup(String nama, String url){
		this.url = url;
		this.nama = nama;
	}

	public ButtonGroup(String nama, String url, Long id, boolean openEdit){
		this.url = url;
		this.nama = nama;
		this.id = id;
		this.openEdit = openEdit;
	}

}
