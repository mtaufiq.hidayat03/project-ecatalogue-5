ALTER TABLE `penawaran`
ADD COLUMN `pic_jabatan` VARCHAR(255);

ALTER TABLE `penawaran`
ADD COLUMN `alasan_tolak` VARCHAR(255);

ALTER TABLE `penawaran`
ADD COLUMN `alasan_revisi` VARCHAR(255);

ALTER TABLE `penawaran`
ADD COLUMN `data_sikap` JSON;

ALTER TABLE `penawaran`
ADD COLUMN `batas_akhir_revisi` Date;

