package services.prakatalog;

import models.common.AktifUser;
import models.common.JadwalUsulan;
import models.katalog.Komoditas;
import models.masterdata.Tahapan;
import models.prakatalog.*;
import models.prakatalog.form.FormBukaPenawaran;
import org.apache.commons.lang.time.DateUtils;
import play.Logger;
import utils.LogUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class UsulanService {

	public static final String TAG = "UsulanService";

	/**
	 * Menentukan button yang akan tampil di halaman detail usulan apakah 'Buka Penawaran' atau 'Ajukan Penawaran'
	 * @return FormBukaPenawaran
     */
	public static FormBukaPenawaran getButtonPenawaran(AktifUser aktifUser, Usulan usulan, Komoditas komoditas) throws ParseException {
		Logger.info("usulan template jadwal " + usulan.template_jadwal_id);
		FormBukaPenawaran form = new FormBukaPenawaran();
		if (aktifUser == null) {
			return form;
		}

		form.jadwalUsulanList = JadwalUsulan.getJadwalUsulan(usulan);
		form.isLelang = (komoditas.komoditas_kategori_id != Komoditas.KOMODITAS_DATAFEED
				&& !komoditas.perlu_negosiasi_harga
				&& komoditas.prakatalog == 0);

		form.isBukaPenawaran = (aktifUser.roleItems.contains("comBukaPenawaran")
				&& usulan.status.equals(Usulan.STATUS_BARU));

		form.allowToAddPenawaran = (aktifUser.roleItems.contains("comPenawaranAdd")
				&& usulan.status.equals(Usulan.STATUS_PENAWARAN_DIBUKA));

		if (form.allowToAddPenawaran) {
			//restriksi user
			if (form.isLelang) {
				//ref: bukapenawaran yang baru
				//#{if komoditas.komoditas_kategori_id != komoditasTipeDatafeed && !komoditas.perlu_negosiasi_harga}
				// #{if komoditas.prakatalog == 0}
				List<Long> listUser = UsulanPenyedia.findUserIdsByUsulan(usulan.id);//bukaPenawaranSubmit usulan ctr
				form.isAjukanPenawaran = listUser.contains(aktifUser.user_id);
			}

			form.isAjukanPenawaran = usulan.isValidKomoditasForAktifUser()
					&& (usulan.getKomoditas().isKomoditasLokal()
					|| usulan.getKomoditas().isKomodtiasSektoral())
					&& isPenawaran(usulan);
		}

		form.isTerdaftar = PesertaUsulan.isUserRegistedUsulan(aktifUser.user_id, usulan.id);
		if (form.isAjukanPenawaran) {
			form.isPenawaranExist = Penawaran.sudahMengajukanPenawaran(aktifUser.user_id, usulan.id);
		}
		form.isRegistration = isRegistration(usulan);
		form.isExplanation = isExplanation(usulan);
		form.isPenawaran = isPenawaran(usulan);
		return form;
	}

	public static Boolean isPenawaran(Usulan usulan) throws ParseException {
		Tahapan tahapan = Tahapan.getFlaggingTahapanPenawaran();
		return checkCurrentStep(tahapan, usulan);
	}

	public static Boolean isNegotiation(Usulan usulan) throws ParseException {
		Tahapan tahapan = Tahapan.getFlaggingTahapanNegosiasi();
		return checkCurrentStep(tahapan, usulan);
	}

	public static Boolean isRevision(Usulan usulan) throws ParseException {
		Tahapan tahapan = Tahapan.getFlaggingTahapanRevisi();
		return checkCurrentStep(tahapan, usulan);
	}

	public static Boolean isAfterPokjaAskedForRevision(Penawaran penawaran) throws ParseException  {
		if(penawaran.batas_akhir_revisi != null){
			Tahapan tahapan = Tahapan.getFlaggingTahapanRevisi();
			UsulanJadwal jadwalUsulan = UsulanJadwal.getBy(tahapan.id, penawaran.usulan_id);
			if (jadwalUsulan == null) {
				LogUtil.debug(TAG, "jadwal usulan is null");
				return false;
			}

			return isAfter(jadwalUsulan.tanggal_selesai);
//			return isAfter(penawaran.batas_akhir_revisi);
		} else{
			return true;
		}
	}

	public static Boolean isAfterRevision(Penawaran penawaran) throws ParseException  {
		if(penawaran.batas_akhir_revisi != null){
			return false;
		} else{
			return true;
		}
	}

	public static Boolean isRegistration(Usulan usulan) throws ParseException {
		Tahapan tahapan = Tahapan.getFlaggingTahapanPendaftaran();
		return checkCurrentStep(tahapan, usulan);
	}

	public static Boolean isExplanation(Usulan usulan) throws ParseException {
		Tahapan tahapan = Tahapan.getFlaggingTahapanPenjelasan();
		return checkCurrentStep(tahapan, usulan);
	}

	public static Boolean checkCurrentStep(Tahapan tahapan, Usulan usulan) throws ParseException {
		UsulanJadwal jadwalUsulan = UsulanJadwal.getBy(tahapan.id, usulan.id);
		if (jadwalUsulan == null) {
			LogUtil.debug(TAG, "jadwal usulan is null");
			return false;
		}
		return allowToActionFrom(
				jadwalUsulan.tanggal_mulai,
				jadwalUsulan.tanggal_selesai,
				new java.util.Date()
		);
	}

	public static Boolean isAfterPokjaAskedForRevision(Penawaran penawaran, Usulan usulan) {
		Tahapan tahapanRevisi = Tahapan.getFlaggingTahapanRevisi();
		Tahapan tahapanPenawaran = Tahapan.getFlaggingTahapanPenawaran();
		UsulanJadwal jadwalUsulanRevisi = UsulanJadwal.getBy(tahapanRevisi.id, usulan.id);
		UsulanJadwal jadwalUsulanPenawaran = UsulanJadwal.getBy(tahapanPenawaran.id, usulan.id);
		if (penawaran.status.equals(Penawaran.STATUS_MENUNGGU_VERIFIKASI)) {
			if (penawaran.batas_akhir_revisi == null) {
				if (isAfter(jadwalUsulanPenawaran.tanggal_selesai)){
					return true;
				}
				return false;
			}
			if (penawaran.batas_akhir_revisi != null) {
				if (isAfter(jadwalUsulanRevisi.tanggal_selesai)){
					return true;
				}
				return false;
			}
		}
		if (penawaran.status.equals("revisi")) {
			if (penawaran.batas_akhir_revisi != null) {
				if (isAfter(jadwalUsulanRevisi.tanggal_selesai)){
					return true;
				}
				return false;
			}
		}
		return false;
		/* if (penawaran.batas_akhir_revisi == null && penawaran.status.equals("menunggu_verifikasi")) {
			return true;
		}
		return penawaran.batas_akhir_revisi != null
				&& isAfter(penawaran.batas_akhir_revisi); */
	}

	public static Boolean isAfterPenawaran(Usulan usulan) {
		Tahapan tahapan = Tahapan.getFlaggingTahapanPenawaran();
		UsulanJadwal jadwalUsulan = UsulanJadwal.getBy(tahapan.id, usulan.id);
		if (jadwalUsulan.tanggal_selesai == null) {
			LogUtil.debug(TAG, "Oops.. start date or finish date is empty!");
			return false;
		}

		return isAfter(jadwalUsulan.tanggal_selesai);
	}

	public static Boolean isBeforeOrSameNegotiationDate(Usulan usulan) {
		Tahapan tahapan = Tahapan.getFlaggingTahapanNegosiasi();
		UsulanJadwal jadwalUsulan = UsulanJadwal.getBy(tahapan.id, usulan.id);
		if (jadwalUsulan.tanggal_selesai == null) {
			return false;
		}
		return isBeforeOrSame(jadwalUsulan.tanggal_selesai);
	}

	public static Boolean isAfter(Date finishDate) {
		Date truncateFinish = DateUtils.truncate(finishDate, Calendar.DATE);
		Date truncateNow = DateUtils.truncate(new java.util.Date(), Calendar.DATE);
		LogUtil.debug(TAG, "finish: " + truncateFinish);
		final Boolean result = truncateNow.after(truncateFinish);
		LogUtil.debug(TAG, "is after: " + result);
		return result;
	}

	public static Boolean isBeforeOrSame(Date finishDate) {
		Date truncateFinish = DateUtils.truncate(finishDate, Calendar.DATE);
		Date truncateNow = DateUtils.truncate(new java.util.Date(), Calendar.DATE);
		Boolean result = false;
		if (truncateNow.compareTo(truncateFinish) <= 0) {
			result = true;
		}
		return result;
	}

	public static Boolean allowToActionFrom(Date tanggalMulai, Date tanggalSelesai, Date currentDate) throws ParseException {
		if (tanggalMulai == null || tanggalSelesai == null) {
			LogUtil.debug(TAG, "Oops.. start date or finish date is empty!");
			return false;
		}
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date todayWithoutTime = formatter.parse(formatter.format(currentDate));
		final Date startJadwal = formatter.parse(formatter.format(tanggalMulai));
		final Date endJadwal = formatter.parse(formatter.format(tanggalSelesai));
		/*final Date truncateStart = DateUtils.truncate(tanggalMulai, Calendar.DATE);
		final Date truncateFinish = DateUtils.truncate(tanggalSelesai, Calendar.DATE);
		final Date truncateNow = DateUtils.truncate(currentDate, Calendar.DATE);
		LogUtil.debug(TAG, "start: " + truncateStart);
		LogUtil.debug(TAG, "finish: " + truncateFinish);*/
		final Boolean result =
				(todayWithoutTime.compareTo(startJadwal) >= 0 &&
						todayWithoutTime.compareTo(endJadwal) <= 0);
/*		final Boolean result = (truncateNow.equals(truncateStart)
				|| truncateNow.after(truncateStart))
				&& (truncateNow.before(truncateFinish)
				|| truncateNow.equals(truncateFinish));*/
		LogUtil.debug(TAG, "is date valid: " + result);
		return result;
	}

}
