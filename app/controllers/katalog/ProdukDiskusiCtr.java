package controllers.katalog;

import com.google.gson.JsonObject;
import controllers.BaseController;
import models.common.AktifUser;
import models.katalog.ProdukDiskusi;
import play.Logger;

import java.io.IOException;
import java.util.List;

public class ProdukDiskusiCtr extends BaseController {
    public static void createSubmitComment(Long produk_id, String comment, int user_penerima)throws IOException {
        JsonObject jsonDiskusi = new JsonObject();
        AktifUser aktifUser = getAktifUser();

        ProdukDiskusi prdkDiskusi = new ProdukDiskusi();
        prdkDiskusi.produk_id = produk_id;
        prdkDiskusi.user_id = aktifUser.user_id;
        prdkDiskusi.diskusi = comment;
        prdkDiskusi.user_nama = aktifUser.nama_lengkap;
        prdkDiskusi.user_penerima = user_penerima;
        prdkDiskusi.status_komentar = 1;
//        prdkDiskusi.user_foto = aktifUser.nama_lengkap;
        prdkDiskusi.active = true;
        prdkDiskusi.save();

        jsonDiskusi.addProperty("namaUser", aktifUser.nama_lengkap);
        renderJSON(jsonDiskusi);
    }

    public static void getProdukDiskusiByIdProduk(Long produk_id){
        List<ProdukDiskusi> listGetProdukDiskusiByIdProduk = ProdukDiskusi.findProdukDiskusiByIdProduk(produk_id);
    }

}
