package controllers.purchasing;

import controllers.BaseController;
import models.BaseKatalogTable;
import models.jcommon.datatable.DatatableQuery;
import models.purchasing.search.PurchasingSearch;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.util.TextUtils;
import utils.LogUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 12/4/2017
 */
public class PurchasingDataTable extends BaseController {

    public static final String TAG = "PurchasingDataTable";

    public static void searchPackage(PurchasingSearch model) {
        LogUtil.d(TAG, model);
        DatatableQuery query = new DatatableQuery(PurchasingResultHandler.PACKAGE_LIST)
                .select("p.id, p.nama_paket, k.nama_komoditas, p.agr_paket_produk, p.agr_konversi_harga_total, p.satuan_kerja_nama, ps.to_status")
                .from("`paket` p " +
                        "JOIN komoditas k on k.id = p.komoditas_id " +
                        "JOIN paket_status ps ON ps.paket_id = p.id ");
        List<Object> params = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append("p.active = ?");
        params.add(BaseKatalogTable.AKTIF);
        if (model != null) {
            if (!TextUtils.isEmpty(model.keyword)) {
                sb.append(" AND p.nama_paket LIKE ?");
                params.add("%" + StringEscapeUtils.escapeSql(model.keyword) + "%");
            }
            if (model.commodity > 0) {
                sb.append(" AND p.komoditas_id = ?");
                params.add(model.commodity);
            }

        }

        /*if (!TextUtils.isEmpty(model.status)) {
            sb.append(" AND ps.status_paket = ?");
            params.add(StringEscapeUtils.escapeSql(model.status));
        }*/
        /*if (!TextUtils.isEmpty(model.negotiationStatus)) {
            sb.append(" AND ps.status_negosiasi =?");
            params.add(StringEscapeUtils.escapeSql(model.negotiationStatus));
        }*/
        query.where(sb.toString()).params(params.toArray());
        renderJSON(query.executeQuery());
    }

}
