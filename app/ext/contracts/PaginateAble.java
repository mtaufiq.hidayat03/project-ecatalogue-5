package ext.contracts;

import ext.Pageable;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import play.db.jdbc.Query;

import java.util.List;

/**
 * @author HanusaCloud on 12/18/2017
 */
public interface PaginateAble {

    long getTotal();

    long getTotalCurrentPage();

    int getMax();

    int getPage();

    List<BasicNameValuePair> getParams();

    void setTotal(Long total);

    default int getFrom() {
        return getPage() > 1 ? (getPage() - 1) * getMax() : 0;
    }

    default String getQueryString() {
        if (getParams() != null) {
            return "?" + URLEncodedUtils.format(getParams(), "UTF-8");
        }
        return "";
    }

    default Pageable getPageAble() {
        return new Pageable(this);
    }

    default Pageable getPageAble(String extra) {
        return new Pageable(this, extra);
    }
    default void generateTotal(String query) {
        final Long total = Query.find("SELECT COUNT(*) FROM (" + query + ") p", Long.class).first();
        setTotal(total);
    }

    default boolean isEmpty() {
        return getTotal() == 0;
    }

}
