package models.elasticsearch.product;

import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class Hit {

    @SerializedName("_index")
    public String index;
    @SerializedName("_type")
    public String type;
    @SerializedName("_id")
    public String id;
    @SerializedName("_source")
    public ProductPriceElastic price;

}
