package repositories.paket;

import constants.paket.PembayaranConstant;
import models.purchasing.pembayaran.AcceptanceItem;
import models.purchasing.pembayaran.DeliveryItem;
import models.purchasing.pembayaran.Pembayaran;
import models.purchasing.pembayaran.PembayaranLampiran;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.db.jdbc.Query;
import utils.LogUtil;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static constants.paket.PaketPengirimanConstant.VERIFICATION_ID;
import static constants.paket.PembayaranConstant.*;
import static constants.paket.PembayaranConstant.PAKET_ID;
import static constants.paket.PembayaranConstant.TABLE_NAME;

/**
 * @author HanusaCloud on 11/1/2017
 */
public class PaymentRepository {

    public static final String TAG = "PaymentRepository";

    public static List<AcceptanceItem> searchAcceptanceItems(Long packageId, String keyword, String exception) {
        LogUtil.d(TAG, "search acceptance");
        StringBuilder sb = new StringBuilder("SELECT pp.id, " +
                "pp.paket_id as packageId, " +
                "pp.tanggal_dokumen as documentDate, " +
                "pp.no_dokumen as documentNumber, " +
                "pp.tanggal_terima as acceptanceDate, " +
                "(ap.total + coalesce(kr.ongkir,0)) as total, " +
                "pp.paket_pembayaran_id as paymentId, " +
                "pp.deskripsi as description " +
                "FROM `paket_penerimaan` pp " + 
                "JOIN `paket_pengiriman` kr on pp.`paket_pengiriman_id`=kr.id " + 
                "JOIN (SELECT paket_penerimaan_id, SUM(acp.kuantitas * pro.harga_satuan) as total " +
                "FROM paket_penerimaan_produk acp " +
                "JOIN paket_produk pro " +
                "ON acp.paket_produk_id = pro.id " +
                "WHERE acp.active > 0 AND pro.active > 0 " +
                "GROUP BY paket_penerimaan_id) ap " +
                "ON ap.paket_penerimaan_id = pp.id " +
                "WHERE pp.paket_id =? " +
                "AND pp.active > 0 " +
                "AND pp.paket_pembayaran_id IS NULL ");
        if (!TextUtils.isEmpty(keyword)) {
            sb.append("AND pp.no_dokumen LIKE '%").append(keyword).append("%' ");
        }
        if (!TextUtils.isEmpty(exception)) {
            sb.append("AND pp.id NOT IN (").append(exception).append(") ");
        }
        sb.append("GROUP BY pp.id ");
        LogUtil.d(TAG, sb.toString());
        return Query.find(sb.toString(), AcceptanceItem.class, packageId).fetch();
    }

    public static List<AcceptanceItem> searchDeliveryItems(Long packageId, String keyword, String exception) {
        StringBuilder sb = new StringBuilder("SELECT pp.id, " +
                "pp.paket_id as packageId, " +
                "pp.tanggal_dokumen as documentDate, " +
                "pp.no_dokumen as documentNumber, " +
                "pp.no_dokumen_sistem as systemDocumentNumber," +
                "pp.deskripsi as description, " +
                "SUM((ap.kuantitas * pd.harga_satuan) + pd.harga_ongkir) as total, " +
                "pp.paket_penerimaan_pembayaran_id as verificationId " +
                "FROM `paket_pengiriman` pp " +
                "JOIN paket_pengiriman_produk ap " +
                "ON ap.paket_pengiriman_id = pp.id AND ap.active > 0 " +
                "JOIN paket_produk pd " +
                "ON pd.id = ap.paket_produk_id AND pd.active > 0 " +
                "WHERE pp.paket_id =? " +
                "AND pp.active > 0 " +
                "AND pp." + VERIFICATION_ID + " IS NULL ");
        if (!TextUtils.isEmpty(keyword)) {
            sb.append("AND (pp.no_dokumen_sistem LIKE '%").append(keyword).append("%' ");
            sb.append(" OR pp.no_dokumen LIKE '%").append(keyword).append("%')");
        }
        if (!TextUtils.isEmpty(exception)) {
            sb.append("AND pp.id NOT IN (").append(exception).append(") ");
        }
        sb.append("GROUP BY pp.id ");
        return Query.find(sb.toString(), AcceptanceItem.class, packageId).fetch();
    }

    public static List<AcceptanceItem> getAcceptanceByPaymentId(Long packageId, Long paymentId) {
        StringBuilder sb = new StringBuilder("SELECT pp.id, " +
                "pp.paket_id as packageId, " +
                "pp.tanggal_dokumen as documentDate, " +
                "pp.no_dokumen as documentNumber, " +
                "pp.tanggal_terima as acceptanceDate, " +
                "ap.total, " +
                "pp.paket_pembayaran_id as paymentId, " +
                "pp.deskripsi as description " +
                "FROM `paket_penerimaan` pp " +
                "JOIN (SELECT paket_penerimaan_id, SUM(acp.kuantitas * pro.harga_satuan) as total " +
                "FROM paket_penerimaan_produk acp " +
                "JOIN paket_produk pro " +
                "ON acp.paket_produk_id = pro.id " +
                "WHERE acp.active > 0 AND pro.active > 0 " +
                "GROUP BY paket_penerimaan_id) ap " +
                "ON ap.paket_penerimaan_id = pp.id " +
                "WHERE pp.paket_id =? AND pp.paket_pembayaran_id =? " +
                "AND pp.active > 0 ");
        sb.append("GROUP BY pp.id ");
        return Query.find(sb.toString(), AcceptanceItem.class, packageId, paymentId).fetch();
    }

    public static List<Pembayaran> getPaymentHistoriesByPackageId(Long packageId) {
        final String query = "SELECT " + ID
                + ", " + NO_INVOICE
                + ", " + TOTAL_INVOICE
                + ", " + TANGGAL_INVOICE
                + ", " + TANGGAL_PEMBAYARAN
                + ", " + TANGGAL_PENERIMAAN
                + ", " + DESKRIPSI
                + ", " + PAKET_ID
                + ", " + TRANSACTION_TYPE
                + ", " + CREATED_BY
                + " FROM " + TABLE_NAME + " WHERE " + PAKET_ID + " =? AND active > 0";
        return Query.find(query, Pembayaran.class, packageId).fetch();
    }

    public static Map<Long, PembayaranLampiran> getPaymentAttachmentsMapModelId(Long packageId, String ids) {
        final String query = "SELECT id, " +
                "paket_id, " +
                "model_id, " +
                "file_name, " +
                "original_file_name, " +
                "file_sub_location, " +
                "category " +
                "FROM paket_pembayaran_lampiran " +
                "WHERE model_id IN ("+ids+") " +
                "AND paket_id =? " +
                "AND active > 0 " +
                "GROUP BY model_id " +
                "ORDER BY posisi_file DESC";
        List<PembayaranLampiran> results = Query.find(query, PembayaranLampiran.class, packageId).fetch();
        if (!results.isEmpty()) {
            return results.stream().collect(Collectors.toMap(PembayaranLampiran::getModelId, p -> p));
        }
        return Collections.emptyMap();
    }

    public static PembayaranLampiran getAttachment(Long modelId, Integer type) {
        return Query.find("SELECT * FROM pembayaran_lampiran" +
                " WHERE model_id =?" +
                " AND active > 0" +
                " AND category =?", PembayaranLampiran.class, modelId, type)
                .first();
    }

    public static void deactivatePaymentAttachments(Long modelId, Integer transactionType) {
        Query.update("UPDATE paket_pembayaran_lampiran SET active = 0 WHERE model_id =? AND category =?", modelId, transactionType);
    }

    public static int getTotalAttachment(Long modelId, Integer transactionType) {
        return Query.count("SELECT COUNT(*)" +
                " FROM paket_pembayaran_lampiran" +
                " WHERE model_id =?" +
                " AND active > 0" +
                " AND category =?", Integer.class, modelId, transactionType);
    }

    public static List<DeliveryItem> getDeliveredForPaymentVerification(Long packageId, Long id) {
        final String query = "SELECT pp.id, " +
                "pp.paket_id as packageId, " +
                "pp.tanggal_dokumen as documentDate, " +
                "pp.no_dokumen as documentNumber, " +
                "pp.no_dokumen_sistem as systemDocumentNumber," +
                "pp.deskripsi as description, " +
                "SUM(ap.kuantitas * pd.harga_satuan) as total, " +
                "pp.paket_penerimaan_pembayaran_id as verificationId " +
                "FROM `paket_pengiriman` pp " +
                "JOIN paket_pengiriman_produk ap " +
                "ON ap.paket_pengiriman_id = pp.id AND ap.active > 0 " +
                "JOIN paket_produk pd " +
                "ON pd.id = ap.paket_produk_id AND pd.active > 0 " +
                "WHERE pp.paket_id =? " +
                "AND pp.paket_penerimaan_pembayaran_id =? " +
                "AND pp.active > 0 " +
                "GROUP BY pp.id";
        return Query.find(query, DeliveryItem.class, packageId, id).fetch();
    }

    public static boolean isPaymentCompleted(Long packageId) {
        final int totalPayment = getTotalPaymentByType(packageId, PembayaranConstant.TYPE_PAYMENT);
        final int totalVerification = getTotalPaymentByType(packageId, PembayaranConstant.TYPE_PAYMENT_CONFIRMATION);
//        final boolean isCompleted = totalPayment > 0 && totalVerification > 0 && totalPayment == totalVerification;
        final boolean isCompleted = totalPayment > 0;
        Logger.debug("is payment completed: " + isCompleted);
        return isCompleted;
    }

    public static Integer getTotalPaymentByType(Long packageId, int type) {
        final String query = "SELECT count(*) as total " +
                "FROM paket_pembayaran " +
                "WHERE paket_id =? " +
                "AND transaction_type =? " +
                "AND active > 0";
        return Query.find(query, Integer.class, packageId, type).first();
    }

}
