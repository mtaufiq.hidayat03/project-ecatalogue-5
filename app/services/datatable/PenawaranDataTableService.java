package services.datatable;

import controllers.BaseController;
import models.common.AktifUser;
import models.common.ButtonGroup;
import models.katalog.Komoditas;
import models.prakatalog.Penawaran;
import play.Logger;
import play.i18n.Messages;
import play.mvc.Router;
import utils.KatalogUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class PenawaranDataTableService {

	public static String[] allPenawaranNonPenyedia(String[] results, ResultSet rs) throws SQLException{
		String status = rs.getString("pn.status");
		String statusLabel = KatalogUtils.getStatusLabel(status);

		results[0] = rs.getString("pn.id");
		results[1] = rs.getString("u.nama_usulan");
		results[2] = rs.getString("k.nama_komoditas");
		results[3] = rs.getString("p.nama_lengkap");
		results[4] = rs.getString("pn.created_date");
		results[5] = statusLabel;
		results[6] = makeButtonGroups(rs,status);

		return results;

	}

	public static String[] penawaranByUsulan(String[] results, ResultSet rs) throws SQLException{
		String status = rs.getString("pn.status");
		String statusLabel = KatalogUtils.getStatusLabel(status);

		results[0] = rs.getString("pn.id");
		results[1] = rs.getString("p.nama_lengkap");
		results[2] = rs.getString("pn.created_date");
		results[3] = statusLabel;
		results[4] = makeButtonGroups(rs,status);

		return results;

	}

	public static String[] penawaranByPenyedia(String[] results, ResultSet rs) throws SQLException{
		String status = rs.getString("pn.status");
		String statusLabel = KatalogUtils.getStatusLabel(status);

		results[0] = rs.getString("pn.id");
		results[1] = rs.getString("k.nama_komoditas");
		results[2] = rs.getString("u.nama_usulan");
		results[3] = rs.getString("pn.created_date");
		results[4] = statusLabel;
		results[5] = makeButtonGroups(rs,status);

		return results;
	}

	public static String makeButtonGroups(ResultSet rs, String status){
		try{
			AktifUser aktifUser = BaseController.getAktifUser();
			Komoditas komoditas = Komoditas.findById(rs.getLong("komoditas_id"));
			Map<String, Object> pkomo = new HashMap<String, Object>();
			pkomo.put("id", rs.getLong("komoditas_id"));
			pkomo.put("penawaran_id",rs.getString("pn.id"));

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id",rs.getString("pn.id"));

			String urlVerification = Router.reverse("controllers.prakatalog.penawaranCtr.verification",params).url;
			String urlDetail =Router.reverse("controllers.prakatalog.penawaranCtr.detail",params).url;
			String urlPersetujuan =Router.reverse("controllers.prakatalog.persetujuanCtr.persetujuanProduk",params).url;
			String urlProduk =Router.reverse("controllers.prakatalog.penawaranCtr.daftarProduk",params).url;
			String urlProdukKat =Router.reverse("controllers.prakatalog.penawaranCtr.inputMerekKategori",pkomo).url;
			String urlRevisi = Router.reverse("controllers.prakatalog.penawaranCtr.edit", params).url;
			String urlSelesaiNegosiasi = Router.reverse("controllers.prakatalog.PersetujuanCtr.selesaiNegosiasi", params).url;
			String urlEdit = Router.reverse("controllers.prakatalog.penawaranCtr.edit", params).url;

			Map<String, Object> contractParam = new HashMap<String, Object>();
			contractParam.put("pid",rs.getString("pn.id"));
			contractParam.put("type","penawaran");

			Map<String, Object> contractParamPenyedia = new HashMap<String, Object>();
			contractParamPenyedia.put("pid",rs.getString("pn.id"));
			contractParamPenyedia.put("type","penawaran");
			contractParamPenyedia.put("kategori", "kontrak katalog");

			String urlContract =Router.reverse("controllers.penyedia.KontrakCtr.create",contractParam).url;
			String urlPrintContract =Router.reverse("penyedia.KontrakCtr.cetakKontrak",contractParam).url;
			String urlPrintContractPenyedia =Router.reverse("penyedia.KontrakCtr.listDokumenUpload",contractParamPenyedia).url;
			String urlPrintPenetapanProduk =Router.reverse("penyedia.KontrakCtr.listLampiranKontrak",contractParam).url;
			String urlInputDataKontrak = Router.reverse("penyedia.KontrakCtr.renderInputDataKontrak",contractParam).url;

			Map<String, Object> paramrev = new HashMap<String, Object>();
			paramrev.put("usulan_id",rs.getString("usulan_id"));//usulan_id
			String docReviewid = rs.getString("u.doc_review_id");
			Boolean belumReview = docReviewid==null;
			String urlReviewUsulan = Router.reverse("controllers.admin.UsulanCtr.reviewUsulan",paramrev).url;


			List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();

			if(aktifUser != null){

				Long user_id = rs.getLong("pn.user_id");
				Long pokja_id = rs.getLong("up.pokja_id");

				//Logger.debug(user_id+", userId");
				//Logger.debug(aktifUser.user_id+", userIdAktifUser");

				if(aktifUser.isProvider()){
					if(aktifUser.user_id.equals(user_id)){
						buttonGroups.add(new ButtonGroup(Messages.get("detail.label"),urlDetail));

						if(!status.equals(Penawaran.STATUS_MENUNGGU_VERIFIKASI)){
							if(status.equals(Penawaran.STATUS_REVISI)){
								buttonGroups.add(new ButtonGroup(Messages.get("revisi.label"),urlRevisi));
							}else if(!komoditas.isDatafeed() && !status.equals(Penawaran.STATUS_GAGAL_KUALIFIKASI)){
								buttonGroups.add(new ButtonGroup(Messages.get("produk.label"),urlProduk));
							}
							else if(komoditas.isDatafeed()){
								buttonGroups.add(new ButtonGroup(Messages.get("merk.kategori.label"),urlProdukKat));
							}
						}
						else {
							if(!komoditas.isDatafeed()) {
								buttonGroups.add(new ButtonGroup(Messages.get("produk.label"),urlProduk));
							}
						}
						if(
							status.equals(Penawaran.STATUS_KONTRAK_DIPROSES)
							|| status.equals(Penawaran.STATUS_NEGOSIASI_SELESAI)
							|| status.equals(Penawaran.STATUS_SELESAI)
						){
							//buttonGroups.add(new ButtonGroup(Messages.get("cetak_kontrak.label"),urlPrintContractPenyedia));
							buttonGroups.add(new ButtonGroup(Messages.get("cetak_kontrak.label"),urlInputDataKontrak));
						}
						if(status.equals(Penawaran.STATUS_REVISI)) {
							buttonGroups.add(new ButtonGroup(Messages.get("revisi.label") + " " + Messages.get("penawaran.label"), urlEdit));
						}
						return DataTableService.generateButtonGroup(buttonGroups);
					}else{
						return "";
					}
				}else if(pokja_id.equals(aktifUser.user_id) || aktifUser.isAdmin()){
					buttonGroups.add(new ButtonGroup(Messages.get("detail.label"),urlDetail));

					if(status.equals(Penawaran.STATUS_MENUNGGU_VERIFIKASI)){
						buttonGroups.add(new ButtonGroup(Messages.get("verifikasi.label"),urlVerification));
					}else{

						if(!komoditas.isDatafeed()){
							buttonGroups.add(new ButtonGroup(Messages.get("produk.label"),urlProduk));
						}

						if(status.equals(Penawaran.STATUS_NEGOSIASI)){
							buttonGroups.add(new ButtonGroup(Messages.get("persetujuan_produk.label"),urlPersetujuan));

							ButtonGroup selesaiNego = new ButtonGroup(Messages.get("selesai_negosiasi.label"),urlSelesaiNegosiasi);
							selesaiNego.isSelesaiNegosiasi = true;
							buttonGroups.add(selesaiNego);

						}else if(status.equals(Penawaran.STATUS_KONTRAK_DIPROSES)){
							if(aktifUser.isAdmin()){
								buttonGroups.add(new ButtonGroup(Messages.get("cetak_kontrak.label"),urlPrintContract));
								if(belumReview ){
									//ada acl disini
									if(aktifUser.isAuthorized("comUsulanReview")){
										buttonGroups.add(new ButtonGroup(Messages.get("review_usulan.label"), urlReviewUsulan));
									}else{
										//memberikan info no akses ACL
										buttonGroups.add(new ButtonGroup(Messages.get("review_usulan.label")+"(Acl)", "#"));
									}
								}else {
									Map<String, Object> param = new HashMap<String, Object>();
									param.put("id",rs.getString("usulan_id"));
									String urlDetailUsulan = Router.reverse("controllers.admin.UsulanCtr.detail",param).url;
									buttonGroups.add(new ButtonGroup(Messages.get("detail_review_usulan.label"), urlDetailUsulan));
									buttonGroups.add(new ButtonGroup(Messages.get("unggah.label") + " " + Messages.get("kontrak.label"), urlContract));
								}
							}

						}else if((status.equals(Penawaran.STATUS_NEGOSIASI_SELESAI) ||
								status.equals(Penawaran.STATUS_PERSIAPAN_KONTRAK)) && aktifUser.isAdmin()){
							if(belumReview ){
								if(aktifUser.isAuthorized("comUsulanReview")){
										buttonGroups.add(new ButtonGroup(Messages.get("review_usulan.label"), urlReviewUsulan));
									}else{
										//memberikan info no akses ACL
										buttonGroups.add(new ButtonGroup(Messages.get("review_usulan.label")+"(Acl)", "#"));
									}
							}else {
								Map<String, Object> param = new HashMap<String, Object>();
								param.put("id",rs.getString("usulan_id"));
								String urlDetailUsulan = Router.reverse("controllers.admin.UsulanCtr.detail",param).url;
								buttonGroups.add(new ButtonGroup(Messages.get("detail_review_usulan.label"), urlDetailUsulan));
								buttonGroups.add(new ButtonGroup(Messages.get("cetak_kontrak.label"), urlPrintContract));
							}

						} else if(status.equals(Penawaran.STATUS_SELESAI)){
							if(aktifUser.isAdmin()){
								buttonGroups.add(new ButtonGroup(Messages.get("lampiran_kontrak.label"), urlPrintPenetapanProduk));
							}
						}

					}

					return DataTableService.generateButtonGroup(buttonGroups);

				}else{
					return "";
				}
			}else{
				return "";
			}

		}catch (SQLException e){
			Logger.error("Error penawarantatatableservice buttongroup:",e);
			e.printStackTrace();
			throw new UnsupportedOperationException(e);

		}
	}



}
