package controllers.katalog;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import controllers.BaseController;
import controllers.security.AllowAccess;
import ext.DateBinder;
import models.cms.KategoriKonten;
import models.common.AktifUser;
import models.katalog.*;
import models.secman.Acl;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import play.data.binding.As;
import play.i18n.Messages;
import play.mvc.Http;
import utils.DateTimeUtils;

import static models.cms.KategoriKonten.*;

public class LaporanProdukCtr extends BaseController{

    @AllowAccess({Acl.COM_PRODUK_REPORT_MANAGE})
    public static void index(){
        List<KategoriKonten> kategoriKontenList = findAllByKonten(5);
        List<Komoditas> komoditasList = Komoditas.findAllActive();
        render("katalog/ProdukCtr/listProdukLaporan.html", komoditasList, kategoriKontenList);
    }

    @AllowAccess({Acl.COM_PRODUK_REPORT_MANAGE})
    public static void detail(Long id) {
        Report report = Report.findById(id);
        Produk produk = Produk.findById(report.item_id);
        List<ReportRiwayat> rwt = ReportRiwayat.findByReportId(report.id);

        if(!AktifUser.getAktifUser().isAdmin()) {
            if (!report.reporter_id.equals(Long.valueOf(AktifUser.getActiveUserId())) || report.reporter_id == null) {
                forbidden();
            } else {
                render("katalog/ProdukCtr/detailLaporan.html", report, produk, rwt);
            }
        } else {
            render("katalog/ProdukCtr/detailLaporan.html", report, produk, rwt);
        }
    }

    @AllowAccess({Acl.COM_PRODUK_REPORT_MANAGE})
    public static void export(@As(binder = DateBinder.class)Date publish_date_from,
                              @As(binder = DateBinder.class)Date publish_date_end){

        String start_date = DateTimeUtils.convertToStandardDate(publish_date_from);
        String end_date = DateTimeUtils.convertToStandardDate(publish_date_end);

        if(publish_date_end.before(publish_date_from)){
            flash.error(Messages.get("laporan.export.date.alert"));
        }else {
            List<Report> report = Report.findReportDate(start_date, end_date);
            SimpleDateFormat dateToString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateString = new SimpleDateFormat("ddMMyyyy").format(new Date());

            //Create blank workbook
            XSSFWorkbook workbook = new XSSFWorkbook();

            //Create a blank sheet
            XSSFSheet spreadsheet = workbook.createSheet(Messages.get("laporan_produk.label"));

            //Create row object
            XSSFRow row;

            //This data needs to be written (Object[])
            Map < String, Object[] > empinfo = new TreeMap < String, Object[] >();
            empinfo.put( "1", new Object[] { "ID Laporan","Tanggal Laporan","Link Produk","No.Produk","Nama Produk","Nama Penyedia","Reporter Name","Reporter Email","Reporter NIK","Laporan","Status Laporan" });
            for(int i=1; i <= report.size(); i++){
                empinfo.put(Integer.toString(i+1), new Object[] {Long.toString(report.get(i-1).id),dateToString.format(report.get(i-1).created_date), Http.Request.current().host + "/katalog/produk/detail/" + Long.toString(report.get(i-1).item_id),report.get(i-1).no_produk,report.get(i-1).nama_produk,report.get(i-1).nama_penyedia,report.get(i-1).reporter_name,report.get(i-1).reporter_email,report.get(i-1).reporter_nik,report.get(i-1).message,report.get(i-1).status});
            }

            //Iterate over data and write to sheet
            Set < String > keyid = empinfo.keySet();
            int rowid = 0;

            for (String key : keyid) {
                row = spreadsheet.createRow(rowid++);
                Object [] objectArr = empinfo.get(key);
                int cellid = 0;

                for (Object obj : objectArr) {
                    Cell cell = row.createCell(cellid++);
                    cell.setCellValue((String)obj);
                }
            }

            //Create file system using specific name
            File file = new File("laporan_produk_" + dateString + ".xlsx");
            try{
                OutputStream out = new FileOutputStream(file);
                //write operation workbook using file out object
                workbook.write(out);
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            renderBinary(file);
        }
        index();
    }
}
