package services.produkkategori;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import models.katalog.Produk;
import models.katalog.ProdukAtributValue;
import models.katalog.ProdukKategoriAtribut;
import models.katalog.ProdukPosisi;
import models.katalog.komoditasmodel.KomoditasProdukAtributTipe;
import models.katalog.payload.ProdukKategoriAtributItem;
import models.katalog.payload.ProdukKategoriAtributPayload;
import models.katalog.payload.ProdukKategoriAtributPosisi;
import org.apache.http.util.TextUtils;
import play.Logger;
import repositories.produkkategori.ProdukKategoriAtributRepository;
import repositories.produkkategori.ProdukKategoriRepository;
import utils.LogUtil;

import java.lang.reflect.Type;
import java.util.*;

public class ProdukKategoriAtributeService {

	public static final String TAG = "ProdukKategoriAtributeService";

	public static List<ProdukKategoriAtributItem> save(ProdukKategoriAtributPayload model) {
		List<ProdukKategoriAtributItem> results = model.getItems();
		final String ids = getIds(results);
		Map<Long, ProdukKategoriAtribut> map = TextUtils.isEmpty(ids)
				? Collections.emptyMap()
				: ProdukKategoriAtributRepository.getByIds(ids);
		Logger.debug(TAG + " total map: " + map.size());
		if (results != null && !results.isEmpty()) {
			Map<Long, KomoditasProdukAtributTipe> mapAttr = KomoditasProdukAtributTipe.getAllByCommodityMap(model.kid);
			for (ProdukKategoriAtributItem item : results) {
				item.commodityId = model.kid;
				item.categoryProductId = model.pkid;
				KomoditasProdukAtributTipe attr = mapAttr.get(item.informationType);
				if (attr != null) {
					item.informationLabel = attr.tipe_atribut;
				}
				if (item.id > 0) {
					ProdukKategoriAtribut saved = map.get(item.id);
					if (saved != null) {
						saved.setUpdateValue(item);
						saved.save();
					}
				} else {
					ProdukKategoriAtribut insert = new ProdukKategoriAtribut(item);
					insert.save();
				}
			}
		}
		ProdukKategoriRepository.clearCache(model.kid);
		return results;
	}

	public static void savePosisi(String json, long id) {
		LogUtil.d(TAG, json);
		if (TextUtils.isEmpty(json)) {
			json = "[]";
		}
		Type collectionType = new TypeToken<Collection<ProdukKategoriAtributPosisi>>() {
		}.getType();
		List<ProdukKategoriAtributPosisi> results = new Gson().fromJson(json, collectionType);
		if (results != null && !results.isEmpty()) {
			int position = 0;
			final String ids = getIdsFromPosisi(results);
			Map<Long, ProdukKategoriAtribut> map = ProdukKategoriAtributRepository.getAtributMap(ids);
			if (!map.isEmpty()) {
				for (ProdukKategoriAtributPosisi model : results) {
					if (model.id > 0 && map.containsKey(model.id)) {
						ProdukKategoriAtribut atribut = map.get(model.id);
						atribut.setUpdatePosisi(model);
						atribut.posisi_item = position;
						atribut.save();
						position++;
					}
				}
			}
		}
	}

	public static boolean delete(long id) {
		ProdukAtributValue produkAtributValue = ProdukAtributValue.getDetail(id);
		if (produkAtributValue ==  null) {
			ProdukKategoriAtribut model = ProdukKategoriAtribut.findById(id);
			if (model != null && ProdukAtributValue.getTotalValueByAttribute(id) == 0) {
				model.softDelete();
				return true;
			}
			return false;
		}
		return false;
	}

	public static String getIds(List<ProdukKategoriAtributItem> list) {
		StringBuilder sb = new StringBuilder();
		String glue = "";
		if (!list.isEmpty()) {
			for (ProdukKategoriAtributItem model : list) {
				if (model.id > 0) {
					sb.append(glue).append("'").append(model.id).append("'");
					glue = ",";
				}
			}
		}
		Logger.debug(TAG + " ids: " + sb.toString());
		return sb.toString();
	}

	public static String getIdsFromPosisi(List<ProdukKategoriAtributPosisi> list) {
		StringBuilder sb = new StringBuilder();
		String glue = "";
		if (!list.isEmpty()) {
			for (ProdukKategoriAtributPosisi model : list) {
				if (model.id > 0) {
					sb.append(glue).append("'").append(model.id).append("'");
					glue = ",";
				}
			}
		}
		return sb.toString();
	}


}
