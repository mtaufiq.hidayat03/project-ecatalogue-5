package Migration;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import models.katalog.Produk;
import org.junit.Test;
import play.test.UnitTest;

import java.lang.reflect.Type;
import java.util.List;

public class MigrationAll extends UnitTest {

    //@Test
    public void ongkirMigration(){
        new MigrasiOngkir().startMigration();
    }

//    @Test
    public void provHargaMigration(){


    }

    //@Test
    public void nasionalmigration(){
        new MigrasiHargaNasional().start();
    }

    //@Test
    public void regHargaMigration(){
        new MigrasiHargaRegency().startMigration("kabupaten");
    }

    //@Test
    public void testUpdate(){
        Long idTest = new Long(274006);//34298
        Produk p = Produk.findById(idTest);
        MigrasiOngkir mo = new MigrasiOngkir();
        String jsOngkir = mo.getJsonOngkir(p.id);
        System.out.println(jsOngkir);
        p.ongkir = jsOngkir;
        int result = p.save();
        System.out.println(result+" is oke");
        Produk pexist = Produk.findById(idTest);
        Type tipe = new TypeToken<List<OngkosKirimRquery>>(){}.getType();
        List<OngkosKirimRquery> listExist = new Gson().fromJson(pexist.ongkir, tipe);
        System.out.println(listExist.size());
        System.out.println(new Gson().toJson(listExist));

    }

    //@Migrasi Ongkir Nasional
    @Test
    public void ongkirNasionalMigration(){
        System.out.println("oooo");
        new MigrasiOngkirHargaNasional().start();
    }
}
