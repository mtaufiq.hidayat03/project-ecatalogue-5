package models.api;

import models.jcommon.util.CommonUtil;
import play.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class SirupById {
    Long id;
    String nama;
    String kegiatan;
    Integer jenis_pengadaan;
    String volume;
    Integer metode_pengadaan;
    Integer metode_pengadaan_54;
    String tanggal_kebutuhan;
    String tanggal_awal_pengadaan;
    String tanggal_akhir_pengadaan;
    String tanggal_awal_pekerjaan;
    String tanggal_akhir_pekerjaan;
    String keterangan;
    int tahun_anggaran;
    Long id_satker;
    String kode_kldi;
    boolean umumkan;
    boolean aktif;
    String created_by;
    String created_on;
    boolean is_delete;
    Date tanggal_pengumuman;
    Date auditupdate;
    String sumber_dana_string;
    Long jumlah_pagu;
    String spesifikasi;
    Integer bulan_awal_pengadaan;
    Integer bulan_akhir_pengadaan;
    Integer bulan_awal_pekerjaan;
    Integer bulan_akhir_pekerjaan;
    Integer bulan_kebutuhan;
    boolean is_tkdn;
    boolean umkm;
    boolean is_final;
    String izin_tahun_jamak;
    String no_renja;
    boolean dikecualikan;
    Integer status;
    boolean notifikasi;
    Long id_ppk;

    public Date getTanggalAwalPengadaan(){
        if(!CommonUtil.isEmpty(tanggal_awal_pengadaan)){
            return convertDate(this.tanggal_awal_pengadaan);
        }
        return null;
    }
    public Date getTanggalAkhirPengadaan(){
        if(!CommonUtil.isEmpty(tanggal_akhir_pengadaan)){
            return convertDate(this.tanggal_akhir_pengadaan);
        }
        return null;
    }
    public Date getTanggalAwalPekerjaan(){
        if(!CommonUtil.isEmpty(tanggal_awal_pekerjaan)){
            return convertDate(this.tanggal_awal_pekerjaan);
        }
        return null;
    }
    public Date getTanggalAkhirPekerjaan(){
        if(!CommonUtil.isEmpty(tanggal_akhir_pekerjaan)){
            return convertDate(this.tanggal_akhir_pekerjaan);
        }
        return null;
    }

    public Rup getAsRup(){
        try{
            Rup rup = new Rup();

            rup.setRupFromSirupById(this);
            return rup;
        }catch (Exception e){
            Logger.error("error parse Sirup to rup"+ e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public static java.util.Date convertDate(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");
        try {
            if(date != null){
                return sdf.parse(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
