package models.api.produk.detail;

import models.api.produk.detail.spesifikasi.Item;
import play.Logger;
import play.data.binding.As;
import play.data.binding.types.DateBinder;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dadang on 1/5/18.
 */
public class Spesifikasi {

	public List<Item> item = new ArrayList<>();

	public String tanggal_update;

	public Date getTglUpdateAsDate(){
		return parseAsDate(tanggal_update);
	}
	public Date parseAsDate(String dateStr){
		try{
			DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime dateTime = LocalDateTime.parse(dateStr, format);
			Logger.info(dateTime.toString());
			Date in = new Date();
			LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
			Date result = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
			return result;
		}catch (Exception e){

		}
		return null;
	}

}
