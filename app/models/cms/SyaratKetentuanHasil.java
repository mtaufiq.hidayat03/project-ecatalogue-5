package models.cms;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

/**
 * Created by user on 11/14/2017.
 */
@Table(name = "syarat_ketentuan_hasil")
public class SyaratKetentuanHasil extends BaseKatalogTable {
    @Id
    public Long id;
    public Long syarat_ketentuan_id;
    public String client_ip;
    public Boolean active;

    public static SyaratKetentuanHasil findByExist(Long userId, Long syaratId){
        String query = "SELECT * FROM syarat_ketentuan_hasil WHERE active = 1 and created_by = " + userId + " and syarat_ketentuan_id = " + syaratId;
        return Query.find(query, SyaratKetentuanHasil.class).first();
    }
}
