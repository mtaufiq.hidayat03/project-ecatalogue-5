package models.purchasing.pembayaran;

import constants.paket.PembayaranConstant;
import ext.FormatUtils;
import models.BaseKatalogTable;
import models.common.AktifUser;
import models.purchasing.contracts.PaymentContract;
import models.purchasing.pembayaran.form.PaymentDataForm;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.DateTimeUtils;

import javax.persistence.Transient;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author HanusaCloud on 10/24/2017
 */
@Table(name = "paket_pembayaran")
public class Pembayaran extends BaseKatalogTable implements PaymentContract {

    @Id
    public Long id;
    public Long paket_id;
    public String no_invoice;
    public Double total_invoice = 0.0;
    public Date tanggal_invoice;
    public Date tanggal_pembayaran;
    public String deskripsi;
    public Boolean active;
    public Integer transaction_type;

    @Transient
    public PembayaranLampiran attachment;
    @Transient
    public List<AcceptanceItem> acceptances = new ArrayList<>();
    @Transient
    public List<DeliveryItem> deliveryItems = new ArrayList<>();

    @Transient
    public boolean enabledMenus = false;

    public Pembayaran() {}

    public Pembayaran(PaymentDataForm model) {
        set(model);
    }

    public Pembayaran set(PaymentDataForm model) {
        this.id = model.modelId;
        this.paket_id = model.packageId;
        this.no_invoice = model.invoiceNumber;
        this.total_invoice = model.getTotalInvoiceDouble();
        this.tanggal_invoice = model.getInvoiceDate();
        this.tanggal_pembayaran = model.getPaymentDate();
        this.active = true;
        this.deskripsi = model.description;
        return this;
    }

    @Override
    public boolean isPayment() {
        return transaction_type == PembayaranConstant.TYPE_PAYMENT;
    }

    public String getPaymentDate() {
        return DateTimeUtils.convertToDdMmYyyy(tanggal_pembayaran);
    }

    public String getInvoiceDate() {
        return DateTimeUtils.convertToDdMmYyyy(tanggal_invoice);
    }

    public String getTotalInvoiceString() {
        DecimalFormat df = new DecimalFormat("#.##");
        String formatted = df.format(total_invoice);
        return formatted;
    }

    public String getFormattedPaymentDate() {
        return DateTimeUtils.parseToReadableDate(tanggal_pembayaran);
    }

    public String getFormattedInvoiceDate() {
        return DateTimeUtils.parseToReadableDate(tanggal_invoice);
    }

    public String getTotalPayment() {
        return FormatUtils.formatCurrencyRupiah(total_invoice);
    }

    public void savePayment() {
        final long result = (long) save();
        if (this.id == null) {
            this.id = result;
        }
    }

    public String getDetailUrl() {
        return generateUrl(isPayment()
                ? "purchasing.PembayaranCtr.detailPembayaran"
                : "purchasing.PembayaranCtr.detailVerification");
    }

    public String getDeleteUrl() {
        return generateUrl(isPayment()
                ? "purchasing.PembayaranCtr.deletePembayaran"
                : "purchasing.PembayaranCtr.deleteVerification");
    }

    public String getUpdateUrl() {
        return generateUrl(isPayment()
                ? "purchasing.PembayaranCtr.updatePembayaran"
                : "purchasing.PembayaranCtr.updateVerification");
    }

    @Override
    public void setAcceptancePayment(List<AcceptanceItem> list) {
        this.acceptances = list;
    }

    @Override
    public void setDeliveryPayment(List<DeliveryItem> list) {
        this.deliveryItems = list;
    }

    @Override
    public void setAttachment(PembayaranLampiran model) {
        this.attachment = model;
    }

    @Override
    public Long getPackageId() {
        return paket_id;
    }

    @Override
    public String getAttachmentDownloadUrl() {
        if (attachment != null) {
            return attachment.getDownloadUrl();
        }
        return "#";
    }

    @Override
    public List<AcceptanceItem> getAcceptances() {
        return this.acceptances;
    }

    @Override
    public List<DeliveryItem> getDeliveryItem() {
        return this.deliveryItems;
    }

    @Override
    public Integer getTransactionType() {
        return transaction_type;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setEnabledMenus(AktifUser model) {
        this.enabledMenus = (isAllowedBuyer(model) && isPayment()) || (isAllowedProvider(model) && !isPayment());
    }

    public boolean isAllowedBuyer(AktifUser model) {
        return model.isPpk() && created_by == Math.toIntExact(model.user_id);
    }

    public boolean isAllowedProvider(AktifUser model) {
        return model.isProvider() && created_by == Math.toIntExact(model.user_id);
    }

    public void softDelete() {
        this.active = false;
        setDeleted();
        save();
    }

    public Boolean isPaymentExist(){
        Boolean retval = false;
        try{
            return (Query.find("select * from paket_pembayaran where paket_id=? and no_invoice=?", Pembayaran.class, paket_id, no_invoice).fetch().size() > 0);
        }catch (Exception e){}
        return  retval;
    }
}
