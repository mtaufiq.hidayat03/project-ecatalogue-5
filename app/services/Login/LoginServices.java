package services.Login;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import models.jcommon.util.CommonUtil;
import models.util.Config;
import models.util.Encryption;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.libs.URLs;
import play.libs.WS;
import play.mvc.Http;
import utils.LogUtil;

public class LoginServices {
    static String TAG = "Login Services";
    public static Long getRkn_IdActionDefault(Long rkn_id_spse ){
        Long rkn_id=-1l;
        Config config = Config.getInstance();
        String q= URLs.encodePart(Encryption.encrypt(String.valueOf(rkn_id_spse).getBytes()));
        try {
            String urls =  config.adp_url+ "/agregasi/adprest/aktivasi-rekanan?q="+q;
            //Logger.info(urls);
            String json= WS.url(urls).getAsync().get().getString();
            json= Encryption.decrypt(json);
            //Logger.info(json);
            JsonParser JP=new  JsonParser();
            JsonElement JE=JP.parse(json);
            JsonObject JO=JE.getAsJsonObject().get("aktivasiPK").getAsJsonObject();
            Long activasi_id=JO.get("aktivasi_id").getAsLong();
            rkn_id =  JO.get("rkn_id").getAsLong();
            q=URLs.encodePart(Encryption.encrypt(String.valueOf(activasi_id).getBytes()));
            try{
                json = WS.url(config.adp_url+"/agregasi/adprest/rekanan-terpilih?q="+q).getAsync().get().getString();
                json=Encryption.decrypt(json);
                JE=JP.parse(json);
                rkn_id=JE.getAsJsonObject().get("rkn_id").getAsLong();
            }catch(Exception e){
                Logger.error(e,"Rkn_id tidak diketemukan di mapping aktivasi ADP : rkn_id: %s,stackrace=>%s", rkn_id,e.getMessage());
            }

        } catch (Exception e) {
            Logger.error(e,"Error getRkn_IdActionDefault, Stackrace => %s  ", e.getMessage());
        }
        return rkn_id;
    }


    public static Long loginAdpGetRknidNoHash(String username, String password){
        Long rkn_id=-1l;
        String remoteadr = "127.0.0.1";//Http.Request.current().remoteAddress;

        Config config = Config.getInstance();
        JsonObject params = new JsonObject();
        params.addProperty("username", username.toUpperCase());
        params.addProperty("password", password);//DigestUtils.md5Hex(password)
        params.addProperty("remoteAddress", remoteadr);
        params.addProperty("repoId", 24);

        String param = URLs.encodePart(Encryption.encrypt(params.toString().getBytes()));
        String urlAuth = config.adp_login_api + "?q=" + param;
        //LogUtil.d(TAG, urlAuth);
        Logger.info(urlAuth);
        Logger.info(new Gson().toJson(params));
        try {
            String content = WS.url(urlAuth).timeout("5min").getAsync().get().getString();
            //Logger.info(content);
            String response = Encryption.decrypt(content);
            LogUtil.d(TAG, "response: " + response);
            //Logger.info(response);
            if(!TextUtils.isEmpty(response)){
                rkn_id = CommonUtil.fromJson(response, Long.class);
            }
        }catch (Exception e){
            Logger.error(TAG, "error login adp" );
        }
        return rkn_id;
    }
    public static Long loginAdpGetRknid(String username, String password){
        Long rkn_id=-1l;
        String remoteadr = "127.0.0.1";//Http.Request.current().remoteAddress;

        Config config = Config.getInstance();
        JsonObject params = new JsonObject();
        params.addProperty("username", username.toUpperCase());
        params.addProperty("password", DigestUtils.md5Hex(password));
        params.addProperty("remoteAddress", remoteadr);
        params.addProperty("repoId", 24);

        String param = URLs.encodePart(Encryption.encrypt(params.toString().getBytes()));
        String urlAuth = config.adp_login_api + "?q=" + param;
        //LogUtil.d(TAG, urlAuth);
        Logger.info(urlAuth);
        Logger.info(new Gson().toJson(params));
        try {
            String content = WS.url(urlAuth).timeout("5min").getAsync().get().getString();
            //Logger.info(content);
            String response = Encryption.decrypt(content);
            LogUtil.d(TAG, "response: " + response);
            //Logger.info(response);
            if(!TextUtils.isEmpty(response)){
                rkn_id = CommonUtil.fromJson(response, Long.class);
            }
        }catch (Exception e){
            Logger.error(TAG, "error login adp" );
        }
        return rkn_id;
    }
}
