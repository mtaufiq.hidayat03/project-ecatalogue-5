function douploadHarga(e,filenameId, jsonId, func) {
    var files = e.target.files;
    var f = files[0];
    {
        var reader = new FileReader();
        var name = f.name;
        $("#"+filenameId).html(name);
        
        reader.onload = function (e) {
            var data = e.target.result;
            try {
                var wb = XLSX.read(data, {type: 'binary'});
                var result = process_wb(wb,jsonId);
                if (func !== undefined) {
                    func(result);
                }
            } catch(err) {
                alert("Format template yang anda unggah tidak valid.");
            }
        };
        reader.readAsBinaryString(f);
    }
}

function getTotalEmpty(list) {
    var total = 0;
    var keyGovernment = "Harga Pemerintah";
    var keyRetail = "Harga Retail";
    for (var i in list) {
        var model = list[i];
        if (model.hasOwnProperty(keyGovernment) || model.hasOwnProperty(keyRetail)) {
            if (model[keyRetail] == 0 || model[keyGovernment] == 0) {
                total++;
            }
        }
    }
    return total;
}

function process_wb(wb, jsonId) {
    var output = to_json(wb, 3);
    var res = output['IMPORT_HARGA'];
    var data = {};
    data.totalEmpty = getTotalEmpty(res);
    data.json = JSON.stringify(res);
    $('#'+jsonId).val(data.json);
    return data;
}

function to_json(workbook, startRow) {
    var result = {};
    workbook.SheetNames.forEach(function (sheetName) {

        var ws = workbook.Sheets[sheetName];

        //skip row, dimulai dari row ke 'startRow'
        var range = XLSX.utils.decode_range(ws['!ref']);
        range.s.r = startRow;
        ws['!ref'] = XLSX.utils.encode_range(range);

        var roa = XLSX.utils.sheet_to_json(ws);

        if (roa.length > 0)
            result[sheetName] = roa;

    });
    
    return result;
}
