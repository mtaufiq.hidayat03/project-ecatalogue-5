package jobs.JobTrail;

import jobs.extdata.SirupData;
import play.db.jpa.NoTransaction;
import play.jobs.Job;

import java.util.Calendar;
import java.util.Date;

@NoTransaction
public class JobSirupData5 extends Job {
    Calendar c = Calendar.getInstance();
    int minuteWait = 0;
    public JobSirupData5(int _minuteWait){
        minuteWait = _minuteWait;
    }
    public void doJob(){
        SirupData objs = new SirupData();
        objs.getData(minuteWait++,c.getTime());
    }
}
