package services.prakatalog;

import models.prakatalog.UsulanPokja;

import java.util.List;

public class UsulanPokjaService {

    public static String listPokjaUsulan(List<UsulanPokja> listPokja){
        String tablePokja =
                "<table class='table table-bordered' align='center' border='1' cellspacing='0' style='width:75%'>" +
                        "<thead><tr>" +
                        "<th style='width:5%;text-align:center'><b>No</b></th>" +
                        "<th style='width:30%;text-align:center'><b>Nama</b></th>" +
                        "<th style='width:20%;text-align:center'><b>Jabatan</b></th>" +
                        "<th style='width:20%;text-align:center'><b>Tanda Tangan</b></th>" +


                        "</tr></thead><tbody>";


        for(int i=0;i<listPokja.size();i++){
            tablePokja+=
                    "<tr style='height:30px'>" +
                            "<td style='width:5%'><p style='text-align:center;margin:0;'>" + (i+1) +"</p></td>" +
                            "<td style='width:30%'><p style='text-align:left;margin:0;'>" + listPokja.get(i).nama_pokja+"</p></td>" +
                            "<td style='width:20%'><p style='text-align:center;margin:0;'>Pokja</p></td>" +
                            "<td style='width:20%'><p style='text-align:left;margin:0;'>" +  "</p></td>" +
                            "</tr>";

        }

        return tablePokja;

    }
}
