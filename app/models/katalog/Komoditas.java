package models.katalog;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import models.BaseKatalogTable;
import models.common.AktifUser;
import models.common.KomoditasKategori;
import models.katalog.form.FormProdukKategoriAtribut;
import models.katalog.komoditasmodel.KomoditasManufaktur;
import models.katalog.komoditasmodel.KomoditasProdukAtributTipe;
import models.katalog.komoditasmodel.contracts.CommodityContract;
import models.masterdata.Manufaktur;
import models.prakatalog.Penawaran;
import models.prakatalog.Usulan;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.Play;
import play.cache.CacheFor;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Lang;
import utils.LogUtil;
import utils.UrlUtil;

import javax.persistence.Transient;
import java.io.File;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

//import org.omg.PortableInterceptor.ACTIVE;

@Table(name = "komoditas")
public class Komoditas extends BaseKatalogTable implements CommodityContract {
	
	public static final int KOMODITAS_KATALOG = 1;
	public static final int KOMODITAS_LOKAL = 5;
	public static final int KOMODITAS_SEKTORAL = 6;
	public static final int KOMODITAS_DATAFEED = 3;
	public static final int KOMODITAS_INOVASI = 9;

	public static final int ACTIVE = 1;

	public static final String NASIONAL = "nasional";
	public static final String KABUPATEN = "kabupaten";
	public static final String PROVINSI = "provinsi";

	public static final String DRAFT = "draft";
	public static final String PUBLISH = "publish";

	@Id
	public Long id;
	@Required
	public String nama_komoditas;
	@Required
	public String kode_komoditas;
	@Required
	public boolean apakah_external_url;
	public String external_url;
	@Required
	public String jenis_produk;
	public String kldi_id;
	@Required
	public boolean jenis_produk_override;
	@Required
	public boolean perlu_negosiasi_harga;
	@Required
	public boolean perlu_ongkir;
	public boolean perlu_approval_produk;
	@Required
	public boolean apakah_iklan;
	@Required
	public boolean apakah_paket_produk_duplikat;
	@Required
	public boolean apakah_stok_unlimited;
	@Required
	public String kelas_harga;
	@Required
	public Double margin_harga;
	public int posisi_item;
	public int agr_produk;
	public int agr_paket;
	public int agr_penyedia_komoditas;
	public Integer agr_jumlah_produk_terjual;
	public boolean terkunci;
	public boolean active;
	public boolean penyedia_input_merk;
	public String status;

	@Required
	public Long bidang_id;
	@Transient
	public String nama_bidang;

	@Required
	public Long komoditas_kategori_id;

	@Required
	public int prakatalog;

	@Transient
	public String nama_kategori;
	@Transient
	public String kldi_nama;
	@Transient
	public int total_tipe_atribut;
	@Transient
	public String total_kategori_produk;
	@Transient
	public String total_kategori_produk_atribut;
	@Transient
	public String total_atribut_harga;
	@Transient
	public String file_name;

	public String getIcon(){
		String pathIcon ="/komoditas/";
		String imageRoot = Play.applicationPath.getAbsolutePath().replace(".","")+"/public/files/image";
		File dir = new File(imageRoot+pathIcon);
		if(!dir.exists()){
			try{
                dir.mkdir();
                //Logger.info("directory created %s",dir.getAbsolutePath()+" :"+dir.exists());
            }catch (Exception e){
				Logger.info("directory creation failed:"+e.getMessage());
			}
		}
		pathIcon += file_name;
		return pathIcon;
	}

	public String getDefaultIcon(){
		String pathIcon = "/";
		if(komoditas_kategori_id != null){
			if (komoditas_kategori_id == KOMODITAS_LOKAL) {
				pathIcon += "ikon-lokal.png";
			} else if (komoditas_kategori_id == KOMODITAS_SEKTORAL) {
				pathIcon += "ikon-sektoral.png";
			} else {
				pathIcon += "ikon-nasional.png";
			}
		}

		return pathIcon;
	}


	@Transient
	public String query;

	@Transient
	public List<KomoditasProdukAtributTipe> attributeTypes = new ArrayList<>();

	@Transient
	public List<String> lokasiKldi;

	public static Map<String,String> kelasHargaMap(){

		Map<String,String> map = new HashMap<String,String>();
		map.put(NASIONAL,"Nasional");
		map.put(PROVINSI,"Provinsi");
		map.put(KABUPATEN,"Kabupaten/Kota");

		return map;

	}

	public static List<Komoditas> findAllActive(){

		return find("active = 1 AND (status NOT IN ('" + DRAFT + "') OR status IS NULL) order by nama_komoditas ASC").fetch();
	}

	public static List<Komoditas> findAllData(){

		return find("active >= 0 order by nama_komoditas ASC").fetch();
	}

	public static List<Komoditas> findAllIconActive(){
		String query = "select k.id, k.nama_komoditas, ki.file_name from komoditas k left join komoditas_ikon ki on k.id = ki.komoditas_id " +
				"where k.active = ? order by k.nama_komoditas ASC";

		return Query.find(query, Komoditas.class, ACTIVE).fetch();
	}

	public static List<Komoditas> findByPenyedia(long penyedia_id){
		String query = "select k.id, k.nama_komoditas, k.kelas_harga, k.apakah_iklan from komoditas k join penyedia_komoditas pk on k.id = pk.komoditas_id and pk.active=1" +
				" where pk.penyedia_id = ?";

		return Query.find(query, Komoditas.class, penyedia_id).fetch();
	}

	public static List<Komoditas> findByKldi(String kldi_id){
		String query = "select k.id, k.nama_komoditas, k.kelas_harga, k.apakah_iklan from komoditas k where k.kldi_id = ?";
		return Query.find(query, Komoditas.class, kldi_id).fetch();
	}

	public static List<Komoditas> findByIdPenyedia(){
		String query = "select k.id, k.nama_komoditas, k.kelas_harga, k.apakah_iklan from komoditas k join penyedia_komoditas pk on k.id = pk.komoditas_id " +
				" where pk.penyedia_id = ?";

		return Query.find(query, Komoditas.class, 1).fetch();
	}

	public static Komoditas getDetailById(long id){
		String query = "select kom.*, kk.nama_kategori as nama_kategori, bid.nama_bidang as nama_bidang, kl.nama as kldi_nama, \n" +
				"(SELECT COUNT(*) FROM `komoditas_harga_atribut` where komoditas_id = ? and active = 1) as total_atribut_harga,\n" +
				"(SELECT COUNT(*) FROM `komoditas_produk_atribut_tipe` where komoditas_id = ? and active = 1) as total_tipe_atribut,\n" +
				"(SELECT COUNT(*) FROM `produk_kategori` where komoditas_id = ? and active = 1) as total_kategori_produk,\n" +
				"(SELECT COUNT(*) FROM `produk_kategori_atribut` where komoditas_id = ? and active = 1) as total_kategori_produk_atribut \n" +
				"from komoditas kom \n" +
				"join komoditas_kategori kk on kom.komoditas_kategori_id = kk.id \n" +
				"left join bidang bid on kom.bidang_id = bid.id \n" +
				"left join kldi kl on kom.kldi_id = kl.id COLLATE utf8_unicode_ci \n" +
				"left join komoditas_produk_atribut_tipe kpat on kpat.komoditas_id = kom.id \n" +
				"left join produk_kategori pk on pk.komoditas_id = kom.id \n" +
				"left join produk_kategori_atribut pka on pka.komoditas_id = kom.id \n" +
				"where kom.id = ?  \n" +
				"group by kom.id";


		return Query.find(query, Komoditas.class, id, id, id, id ,id).first();
	}
	
	public static List<Komoditas> getKomoditasNasional() {
		return Komoditas.find("active = 1 AND komoditas_kategori_id <> ? AND komoditas_kategori_id <> ?", KOMODITAS_LOKAL, KOMODITAS_SEKTORAL).fetch();
	}
	
	public static List<Komoditas> getKomoditasLokal() {
		return Komoditas.find("active = 1 AND komoditas_kategori_id = ?", KOMODITAS_LOKAL).fetch();
	}
	
	public static List<Komoditas> getKomoditasSektoral() {
		return Komoditas.find("active = 1 AND komoditas_kategori_id = ?", KOMODITAS_SEKTORAL).fetch();
	}

	public static List<Komoditas> findMostPopular(){
		return find("active = 1 order by agr_jumlah_produk_terjual DESC").fetch(12);
	}

	public static Map<String, List<Komoditas>> AllKomoditasMap(){
		Map<String, List<Komoditas>> allKomoditas = new TreeMap<>();
		List<Komoditas> komoditasList = findAllActive();
		for(Komoditas k : komoditasList){
			String key = k.nama_komoditas.substring(0, 1).toUpperCase();
			if(allKomoditas.get(key) == null){
				List<Komoditas> komoditas = new ArrayList<>();
				komoditas.add(k);
				allKomoditas.put(key, komoditas);
			} else {
				List<Komoditas> komoditas = allKomoditas.get(key);
				komoditas.add(k);
				allKomoditas.put(key, komoditas);
			}
		}

		return allKomoditas;
	}

	public static List<KomoditasKategori> findKomoditasKetegori(){
		List<Komoditas> komoditasList = findAllActive();
		List<KomoditasKategori> komoditasKategoriList = new ArrayList<>();

		for(Komoditas k : komoditasList){
			List<ProdukKategori> produkKategoriList = ProdukKategori
					.find("komoditas_id = ? and (parent_id is null or parent_id = '')", k.id).fetch();
			KomoditasKategori komoditasKategori = new KomoditasKategori();
			komoditasKategori.kode_komoditas = k.kode_komoditas;
			komoditasKategori.komoditas_id = k.id;
			komoditasKategori.nama_komoditas = k.nama_komoditas;
			komoditasKategori.produkKategoriList = produkKategoriList;
			komoditasKategoriList.add(komoditasKategori);
		}

		return komoditasKategoriList;
	}
	
	public static List<KomoditasKategori> findKomoditasKategoriByKomoditasId(Long id) {
		Komoditas komoditas = findById(id);
		KomoditasKategori komoditasKategori = new KomoditasKategori();
		
		List<KomoditasKategori> komoditasKategoriList = new ArrayList<>();
		List<ProdukKategori> produkKategoriList = new ArrayList<>();
		if (komoditas != null) {
			produkKategoriList = ProdukKategori
					.find("komoditas_id = ? and (parent_id is null or parent_id = '')", komoditas.id).fetch();
			komoditasKategori.kode_komoditas = komoditas.kode_komoditas;
			komoditasKategori.komoditas_id = komoditas.id;
			komoditasKategori.nama_komoditas = komoditas.nama_komoditas;
			komoditasKategori.produkKategoriList = produkKategoriList;
		}

		komoditasKategoriList.add(komoditasKategori);
		
		return komoditasKategoriList;
	}

	public static Map<Long, List<ProdukKategori>> findKategoriChilds(){
		List<ProdukKategori> produkKategoriList = ProdukKategori
				.find("parent_id is null or parent_id = ''").fetch();
		Map<Long, List<ProdukKategori>> results = new TreeMap<>();
		for(ProdukKategori pk : produkKategoriList){
			List<ProdukKategori> pkList = ProdukKategori.find("parent_id = ?", pk.id).fetch();
			results.put(pk.id, pkList);
		}

		return results;
	}

	public static void deleteById(long id){
		Komoditas komoditas = Komoditas.findById(id);
		komoditas.active = false;
		komoditas.save();
	}

	public static void saveManufaktur(String[] manufakturs, long id){
		//isi manufaktur
		for(String man : manufakturs){
			if (!TextUtils.isEmpty(man)) {
				Manufaktur m = new Manufaktur();
				m.nama_manufaktur = man;
				m.active = true;
				long manufaktur_id = m.save();

				KomoditasManufaktur km = new KomoditasManufaktur();
				km.komoditas_id = id;
				km.manufaktur_id = manufaktur_id;
				km.active = true;
				km.save();
			}
		}
	}

	public static HashMap<String,Long> saveTipeAtribut(String[] tipeAtribut, String[] deskripsi, long id){
		HashMap<String,Long> mapTipeAtribut = new HashMap<>();

		for(int i = 0; i< tipeAtribut.length;i++){
			String type = tipeAtribut[i];
			if (!TextUtils.isEmpty(type)) {
				KomoditasProdukAtributTipe kpat = new KomoditasProdukAtributTipe();

				kpat.tipe_atribut = type;
				kpat.deskripsi = deskripsi[i];
				kpat.komoditas_id = id;
				kpat.active = true;
				long kpat_id = kpat.save();

				mapTipeAtribut.put(tipeAtribut[i], kpat_id);
			}
		}

		return mapTipeAtribut;
	}

	public static void saveKategori(String json, long id, List<FormProdukKategoriAtribut> kategoriAtributList, HashMap<String,Long> mapTipeAtribut){
		LogUtil.d("saved", kategoriAtributList);
		Type type = new TypeToken<List<ProdukPosisi>>(){}.getType();
		List<ProdukPosisi> lists = new Gson().fromJson(json,type);
		LogUtil.d("jsonAtribut",new Gson().toJson(kategoriAtributList));
		LogUtil.d("jsonTipe",new Gson().toJson(mapTipeAtribut));

		Map<Long,ProdukPosisi> maps = new HashMap<Long, ProdukPosisi>();
		long posisi = 0L;
		int counter = 0;
		for(ProdukPosisi val : lists){
			boolean found = false;

			if(val.id != 0){
				ProdukKategori kategori = new ProdukKategori();
				kategori.setFromPosisi(val);
				kategori.posisi_item = posisi;
				kategori.komoditas_id = id;
				kategori.parent_id = val.parentId != 0 && maps.containsKey(val.parentId) ? maps.get(val.parentId).modelId : 0L;
				long kategori_id = kategori.save();

				for(FormProdukKategoriAtribut kategoriAtribut : kategoriAtributList){
					if (kategoriAtribut != null) {
						if(kategoriAtribut.kategoriId == val.id){
							for(ProdukKategoriAtribut atribut : kategoriAtribut.kategoriAtributList){
								if (atribut != null) {
									ProdukKategoriAtribut pka = new ProdukKategoriAtribut();
									pka.komoditas_id = id;
									pka.parent_id = 0L;
									pka.posisi_item = counter;
									pka.produk_kategori_id = kategori_id;
									pka.label_atribut = atribut.label_atribut;
									pka.deskripsi = atribut.deskripsi;
									pka.tipe_input = atribut.tipe_input;
									pka.komoditas_produk_atribut_tipe_label = atribut.tipe_atribut;
									pka.komoditas_produk_atribut_tipe_id = mapTipeAtribut.get(atribut.tipe_atribut);
									pka.wajib_diisi = atribut.wajib_diisi;
									pka.active = true;

									pka.save();
								}
							}

							found = true;
							break;
						}

						if(found){
							kategoriAtributList.remove(kategoriAtribut);
							break;
						}
					}
				}

				counter++;
				val.modelId = kategori_id;
				maps.put(val.id,val);
			}

			posisi++;

		}

	}

	public boolean isNegotiationRequired() {
		return perlu_negosiasi_harga;
	}

	public boolean isLelang(){
		return (!perlu_negosiasi_harga && this.komoditas_kategori_id != KOMODITAS_DATAFEED);
	}

	public boolean isDatafeed(){
		return (this.komoditas_kategori_id == KOMODITAS_DATAFEED);
	}

	public static Long getKategoriByUsulanId(Long usulan_id){
		String query = "SELECT k.komoditas_kategori_id FROM komoditas k " +
				"JOIN usulan u ON k.id = u.komoditas_id " +
				"WHERE u.id = ? ";

		Komoditas komoditas = Query.find(query,Komoditas.class, usulan_id).first();
		return komoditas.komoditas_kategori_id;

	}

	public static Long getKategoriByPenawaranId(long penawaran_id){
		String query = "SELECT k.komoditas_kategori_id FROM komoditas k " +
				"JOIN usulan u ON k.id = u.komoditas_id JOIN penawaran p ON p.usulan_id = u.id " +
				"WHERE p.id = ? ";

		Komoditas komoditas = Query.find(query,Komoditas.class, penawaran_id).first();
		return komoditas.komoditas_kategori_id;

	}

	public static Komoditas findByUsulan(long usulan_id){
		String query = "SELECT k.id, k.komoditas_kategori_id FROM komoditas k " +
				"JOIN usulan u ON k.id = u.komoditas_id " +
				"WHERE u.id = ? ";

		Komoditas komoditas = Query.find(query,Komoditas.class, usulan_id).first();
		return komoditas;
	}

	public static boolean isDatafeed(long komoditas_id){
		Komoditas komoditas = findById(komoditas_id);
		return (komoditas.komoditas_kategori_id == KOMODITAS_DATAFEED);
	}

	public boolean isNational() {
		return this.kelas_harga.equalsIgnoreCase(NASIONAL);
	}

	public boolean isProvince() {
		return this.kelas_harga.equalsIgnoreCase(PROVINSI);
	}

	public boolean isRegency() {
		return this.kelas_harga.equalsIgnoreCase(KABUPATEN);
	}

	public boolean isNeedDeliveryCost() { return this.perlu_ongkir; }

	public boolean isAllowToBeDeleted() {
		return agr_produk == 0
				&& agr_paket == 0
				&& agr_penyedia_komoditas == 0
				// && KomoditasTemplateJadwal.getTotalByCommodityId(this.id) == 0
				;
	}

	public void softDelete() {
		this.active = false;
		setDeleted();
		save();
	}

	public void setAktif() {
		this.active = true;
		prePersist();
		save();
	}

	public void setNonAktif() {
		this.active = false;
		prePersist();
		save();
	}

	public void setLock() {
		this.terkunci = true;
		prePersist();
		save();
	}

	public void setUnlock() {
		this.terkunci = false;
		prePersist();
		save();
	}

	@CacheFor("5mn")
	public static List<Komoditas> getForDropDown() {
		return Query.find("SELECT id, nama_komoditas FROM komoditas WHERE active =? order by nama_komoditas", Komoditas.class, AKTIF).fetch();
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setAttributes(List<KomoditasProdukAtributTipe> list) {
		this.attributeTypes = list;
	}

	public static List<Komoditas> getAllKomoditasByPokjaId(long pokja_id){
		String whereString = "";
		if(AktifUser.getAktifUser().isPokja()){
			whereString += " and up.pokja_id = " + pokja_id;
		}

		String query = "SELECT kom.id, kom.nama_komoditas FROM komoditas kom " +
				"JOIN usulan u ON u.komoditas_id = kom.id " +
				"JOIN usulan_pokja up ON u.id = up.usulan_id " +
				"JOIN penawaran pen ON pen.usulan_id = u.id " +
				"WHERE (pen.status = ? OR pen.status = ?) " + whereString + " GROUP BY u.komoditas_id";
		return Query.find(query,Komoditas.class, Penawaran.STATUS_NEGOSIASI, Penawaran.STATUS_LOLOS_KUALIFIKASI).fetch();
	}

	public static List<Komoditas> getAllKomoditasPersetujuanProdukByPokjaId(long pokja_id){
		String whereString = "WHERE p.active = 1 ";
		String query = "SELECT distinct k.id, k.nama_komoditas " +
				"FROM produk_tunggu_setuju pm " +
				"LEFT JOIN produk p on pm.produk_id = p.id " +
				"LEFT JOIN komoditas k on k.id = p.komoditas_id " +
				"LEFT JOIN penawaran pen on p.penawaran_id = pen.id " +
				"LEFT JOIN usulan us on pen.usulan_id = us.id ";

		if(AktifUser.getAktifUser().isPokja()){
			query += "LEFT JOIN usulan_pokja up on us.id = up.usulan_id ";
			whereString += " AND up.pokja_id = " + pokja_id + " AND us.status = '" + Usulan.STATUS_PENAWARAN_DIBUKA + "' AND NOT (p.status = '" + Produk.STATUS_MENUNGGU_PERSETUJUAN + "' AND pen.status = '" + Penawaran.STATUS_MENUNGGU_VERIFIKASI + "') ";
		}else{
			whereString += " AND p.status = '" + Produk.STATUS_MENUNGGU_PERSETUJUAN + "' ";
		}

		query = query + whereString;
		return Query.find(query,Komoditas.class).fetch();
	}

	public static  List<Komoditas> findLokalKldi(){
		return Query.find("SELECT DISTINCT kldi_id FROM komoditas JOIN kldi WHERE komoditas.kldi_id IS NOT NULL AND kldi.jenis IN('KEMENTERIAN', 'LEMBAGA', 'INSTANSI')",Komoditas.class).fetch();
	}

	public static  List<Komoditas> findSektoralKldi(){
		return Query.find("SELECT DISTINCT kldi_id FROM komoditas JOIN kldi WHERE komoditas.kldi_id IS NOT NULL AND kldi.jenis IN('KABUPATEN', 'KOTA', 'PROVINSI')",Komoditas.class).fetch();
	}

	public boolean isKomoditasLokal(){
		return this.komoditas_kategori_id.equals(new Long(Komoditas.KOMODITAS_LOKAL));
	}
	public boolean isKomodtiasSektoral(){
		return this.komoditas_kategori_id.equals(new Long(Komoditas.KOMODITAS_SEKTORAL));
	}

	public List<String> locationSelected(){
		List<String> result = getKomoditasWilayah().stream().map(x-> x.kldi_id).collect(Collectors.toList());
		return result;
	}

	public List<KomoditasWilayah> getKomoditasWilayah(){
		List<KomoditasWilayah> result = new ArrayList<>();
		try{
			result = KomoditasWilayah.find("komoditas_id = ?", getId()).fetch();
		}catch (Exception e){}
		return result;
	}
	public String locationSelectedAsJsArray(){
		List<String> loc = locationSelected();
		String result = "[";
		for (String l:loc) {
			result += "'"+l.trim()+"',";
		}
		result +="]";
		return result;
	}

	public String getUrl() {
		return generateUrl(null);
	}

	public String generateUrl(String keyword) {
		String lang = !Lang.get().isEmpty() ? Lang.get() : "id";
		UrlUtil.Builder builder = UrlUtil.builder()
				.setUrl("katalog.ProdukCtr.listProduk")
				.setParam("language", lang)
				.setParam("komoditas_slug", getSlug())
				.setParam("id", id);
		if (!TextUtils.isEmpty(keyword)) {
			builder.setParam("q", keyword);
		}
		String result = builder.build();
		if (!TextUtils.isEmpty(query)) {
			result += query;
		}
		return result+"?first=true";
	}

	public String modifiedUrl(String keyword) {
		String lang = !Lang.get().isEmpty() ? Lang.get() : "id";
		UrlUtil.Builder builder = UrlUtil.builder()
				.setUrl("katalog.ProdukCtr.listProduk")
				.setParam("language", lang)
				.setParam("komoditas_slug", getSlug())
				.setParam("id", id);
		if (!TextUtils.isEmpty(keyword)) {
			builder.setParam("q", keyword);
		}
		String result = builder.build();
		if (!TextUtils.isEmpty(query)) {
			result += query;
		}
		return result+"?umkm=1&first=true";
	}

	public String getSlug() {
		if (!TextUtils.isEmpty(nama_komoditas)) {
			return nama_komoditas.replace(" ", "-").toLowerCase();
		}
		return "";
	}

	public static List<Komoditas> findAllKomoditasOnlineShop(){
		String query = "SELECT * from komoditas kom WHERE kom.active = 1 and kom.komoditas_kategori_id = 3 ";
		return Query.find(query,Komoditas.class).fetch();
	}

	public static boolean isExistByName(Komoditas komoditas){
		boolean result = false;
		String qry = "SELECT * FROM komoditas k WHERE RTRIM(LTRIM(LOWER(k.nama_komoditas)))= " +
				"LTRIM(RTRIM(LOWER('" + komoditas.nama_komoditas + "')))" ;

		List<Produk> list = new ArrayList<>();
		try{
			list = Query.find(qry, Komoditas.class).fetch();
			if(komoditas.id == null){
				// new > 0 maka duplikat
				result = list.size() > 0;
			}else{
				// update > 1 maka duplikat
				result = list.size() > 1;
			}

		}catch (Exception e){}
		return result;
	}

	public String groupCategoryByName() {
		if (komoditas_kategori_id >= 1 && komoditas_kategori_id <= 4) {
			return "Nasional";
		} else if (isKomoditasLokal()) {
			return "Lokal";
		} else if (isKomodtiasSektoral()) {
			return "Sektoral";
		} else if (komoditas_kategori_id == KOMODITAS_INOVASI) {
			return "Inovasi";
		} else {
			return "Unknown";
		}
	}

	public Boolean isEditAble() {
		return !TextUtils.isEmpty(status) && status.equalsIgnoreCase(DRAFT);
	}

}
