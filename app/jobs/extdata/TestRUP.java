package jobs.extdata;

import models.api.Rup;
import models.api.Sirup;
import org.junit.Test;
import play.Logger;
import play.db.jdbc.Query;
import play.jobs.Job;
import play.test.UnitTest;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class TestRUP extends UnitTest {

    @Test
    public void doJob(){
        pullData();
//        new Job() {
//            String uniqueID = UUID.randomUUID().toString();
//            @Override
//            public void before() {
//                Logger.info("job id:" + uniqueID);
//                super.before();
//            }
//
//            public void doJob() {
//                pullData();
//            }
//        }.in(1);
    }
    private void pullData(){
        ExtData ext = new ExtData();
        Logger.info("Job daily SRUP RUP Start");
        //ext.getSirupBetweenDate();
        Date tgl = getSirupBetweenDate();
        List<Sirup> listSirup = ext.getSyrupData(tgl);
        List<Sirup> limitList = listSirup.subList(7,10);
        for (Sirup s: limitList) {
            Rup rupObj = Rup.findById(s.id);
            if (rupObj!=null){
                rupObj.nama = s.nama;
                rupObj.kegiatan = s.kegiatan;
                rupObj.jenis_belanja = null == s.jenisBelanja ?0:s.jenisBelanja ;
                rupObj.jenis_pengadaan = null == s.jenisPengadaan?0:s.jenisPengadaan;
                rupObj.volume = s.volume;
                rupObj.metode_pengadaan = null == s.metodePengadaan?0:s.metodePengadaan;
                rupObj.tanggal_awal_pengadaan= s.getTanggalAwalPengadaan();
                rupObj.tanggal_akhir_pengadaan = s.getTanggalAkhirPengadaan();
                rupObj.tanggal_awal_pekerjaan = s.getTanggalAwalPekerjaan();
                rupObj.tanggal_akhir_pekerjaan = s.getTanggalAkhirPekerjaan();
                rupObj.lokasi = s.lokasi;
                rupObj.keterangan = s.keterangan;
                rupObj.tahun_anggaran = s.tahunAnggaran;
                rupObj.id_sat_ker = s.idSatker;
                rupObj.kode_kldi = s.kodeKldi;
                rupObj.aktif = s.aktif;
                rupObj.is_delete = s.is_delete;
                rupObj.audit_update = new Timestamp(s.auditupdate.getTime());
                rupObj.kode_anggaran = s.kodeAnggaran;
                rupObj.sumber_dana = s.sumberDanaString;
                int result = rupObj.save();
                Logger.info("update:"+result+ " ID : "+rupObj.id);
            }else {
                rupObj = new Rup();
                //rupObj.id = s.id.toString();
                rupObj.id = s.id;
                rupObj.nama = s.nama;
                rupObj.kegiatan = s.kegiatan;
                rupObj.jenis_belanja = null == s.jenisBelanja ?0:s.jenisBelanja ;
                rupObj.jenis_pengadaan = null == s.jenisPengadaan?0:s.jenisPengadaan;
                rupObj.volume = s.volume;
                rupObj.metode_pengadaan = null == s.metodePengadaan?0:s.metodePengadaan;
                rupObj.tanggal_awal_pengadaan= s.getTanggalAwalPengadaan();
                rupObj.tanggal_akhir_pengadaan = s.getTanggalAkhirPengadaan();
                rupObj.tanggal_awal_pekerjaan = s.getTanggalAwalPekerjaan();
                rupObj.tanggal_akhir_pekerjaan = s.getTanggalAkhirPekerjaan();
                rupObj.lokasi = s.lokasi;
                rupObj.keterangan = s.keterangan;
                rupObj.tahun_anggaran = s.tahunAnggaran;
                rupObj.id_sat_ker = s.idSatker;
                rupObj.kode_kldi = s.kodeKldi;
                rupObj.aktif = s.aktif;
                rupObj.is_delete = s.is_delete;
                rupObj.audit_update = new Timestamp(s.auditupdate.getTime());
                rupObj.kode_anggaran = s.kodeAnggaran;
                rupObj.sumber_dana = s.sumberDanaString;
                int result = rupObj.save();
                Logger.info("Save:"+result+ " ID : "+rupObj.id);
            }
        }

        try {
            Thread.sleep(60);//
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Date getSirupBetweenDate(){
        Timestamp startSirup = getLatestAuditUpdate();
        LocalDate lcStartDate = startSirup.toLocalDateTime().toLocalDate();
        Date tanggal = new Date();
        LocalDate lcEndDate = LocalDate.now();
        if(lcStartDate.getYear() < lcEndDate.getYear()){
            lcStartDate = LocalDate.of(lcEndDate.getYear(),lcEndDate.getMonth(),1);
        }
        if(lcStartDate.isBefore(lcEndDate)) {
            for (LocalDate date = lcStartDate; date.isBefore(lcEndDate); date = date.plusDays(1)) {
                Date dates = Date.from(date.atStartOfDay()
                        .atZone(ZoneId.systemDefault())
                        .toInstant());
                Logger.info("chain job >>>>>>>>>>>>>>>>>>>" + dates.toString());
                tanggal=dates;
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }else{
            Date tgl = new Date();
            Logger.info("single job >>>>>>>>>>>>>>>>>>>" + tgl.toString());
            tanggal=tgl;
        }
        return tanggal;
    }

    @Test
    public void jalankanJob(){
        ExtData ext = new ExtData();
        Logger.info("Job daily SRUP RUP Start");
        ext.getSirupBetweenDate();
        try {
            Thread.sleep(60);//
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Timestamp getLatestAuditUpdate(){
        List<Rup> data = Query.find("select r.* from rup r order by r.audit_update desc limit 1",Rup.class).fetch();
        if(data.size()> 0){
            return data.get(0).audit_update;
        }
        return new Timestamp(new Date().getTime());
    }

//    @Test
    public void testSaveRup() {
        Rup rupObj = new Rup();
        try {
        String tanggal = "Feb 22, 2019 9:21:38 AM";
        String tgl= "Jun 1, 2019";
        rupObj.id = new Long(18996292);
        rupObj.nama = "Belanja Modal Pengadaan Printer";
        rupObj.kegiatan = "Penyediaan sarana dan prasarana kerja,";
        rupObj.jenis_belanja = 1;
        rupObj.jenis_pengadaan = 1;
        rupObj.volume = "1 Paket";
        rupObj.metode_pengadaan = 9;
        rupObj.tanggal_awal_pengadaan= convertDate(tgl);
        rupObj.tanggal_akhir_pengadaan = convertDate(tgl);
        rupObj.tanggal_awal_pekerjaan = convertDate(tgl);
        rupObj.tanggal_akhir_pekerjaan = convertDate(tgl);
        rupObj.lokasi = null;
        rupObj.keterangan = "Belanja Modal Pengadaan Printer";
        rupObj.tahun_anggaran = 2019;
        rupObj.id_sat_ker = new Long(143023);
        rupObj.kode_kldi = "D69";
        rupObj.aktif = true;
        rupObj.is_delete = false;
        rupObj.audit_update = new Timestamp(convertDate2(tanggal).getTime());
        rupObj.kode_anggaran = null;
        rupObj.sumber_dana = "APBD";
            Logger.info("Data RUP --> "+rupObj);
        rupObj.save();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static java.util.Date convertDate(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");
        try {
            if(date != null){
                return sdf.parse(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static java.util.Date convertDate2(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy hh:mm:ss a");
        try {
            if(date != null){
                return sdf.parse(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
