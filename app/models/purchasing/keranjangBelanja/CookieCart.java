package models.purchasing.keranjangBelanja;

import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 12/11/2017
 */
public class CookieCart {

    @SerializedName("produkId")
    public Long productId;
    @SerializedName("jumlah")
    public int quantity = 0;
    @SerializedName("daerahId")
    public Long locationId;
    @SerializedName("komoditasId")
    public Long komoditas_id;

}
