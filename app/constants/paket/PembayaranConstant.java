package constants.paket;

/**
 * @author HanusaCloud on 10/24/2017
 */
public class PembayaranConstant {

    public static final String TABLE_NAME = "paket_pembayaran";

    public static final String ID = "id";
    public static final String PAKET_ID = "paket_id";
    public static final String NO_INVOICE = "no_invoice";
    public static final String TOTAL_INVOICE = "total_invoice";
    public static final String TANGGAL_INVOICE = "tanggal_invoice";
    public static final String TANGGAL_PEMBAYARAN = "tanggal_pembayaran";
    public static final String TANGGAL_PENERIMAAN = "tanggal_penerimaan_produk";
    public static final String DESKRIPSI = "deskripsi";
    public static final String TRANSACTION_TYPE = "transaction_type";
    public static final String CREATED_BY = "created_by";

    public static final int TYPE_PAYMENT = 1;
    public static final int TYPE_PAYMENT_CONFIRMATION = 2;

}
