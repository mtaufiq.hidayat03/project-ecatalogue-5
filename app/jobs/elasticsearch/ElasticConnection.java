package jobs.elasticsearch;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import models.elasticsearch.contract.ElasticsearchRequestContract;
import models.elasticsearch.log.ProductLogJson;
import models.elasticsearch.response.ResponseInputElastic;
import models.jcommon.util.CommonUtil;
import models.util.Config;
import okhttp3.RequestBody;
import play.Logger;
import play.Play;
import retrofit2.Call;
import retrofit2.Response;
import services.elasticsearch.ElasticsearchConnection;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

/**
 * @author HanusaCloud on 11/22/2017
 */
public class ElasticConnection implements ElasticsearchRequestContract {
    public static String modelUrl = "produk";
    public static String bulklUrl = "/_bulk";
    private boolean logged = false;
    private Gson gson;

    public ElasticConnection(boolean logged) {
        this.logged = logged;
        this.gson = new Gson();
    }

    private void logging(String message) {
        if (logged) {
            //System.out.println(message);
            Logger.debug(message);
        }
    }

    private void checkFile(File file) {
        if (!file.exists()) {
            logging("file not exists: " + file.getAbsolutePath());
            return;
        }
    }

    private void sendToElastic(RequestBody request) {
        logging("bulk insert product");
        try {
            String urls = ElasticsearchConnection.getElasticurls()+modelUrl+bulklUrl;
            Call<ResponseInputElastic> call = ElasticsearchConnection.openBackaground().bulkInsertProduct(request, urls);
            Response<ResponseInputElastic> responseCall = call.execute();
            //logging("message: " + responseCall.message() + " code: " + responseCall.code() + " " + CommonUtil.toJson(responseCall.errorBody()));
            if (responseCall.isSuccessful()) {
                ResponseInputElastic response = responseCall.body();
                if (response != null) {
                    logging("finish bulk operation: " + gson.toJson(response));
                }
            } else {
                logging("error on inserting to elasticsearch");
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void bulkSend(File file) {
        checkFile(file);
        sendToElastic(getRequestBody(file));
    }

    public void bulkSend(List<ProductLogJson> list) {
        final String json = CommonUtil.toJson(list);
        logging(json);
        sendToElastic(getRequestBody(json));
    }

    public void bulkSend(String json) {
        logging(json);
        sendToElastic(getRequestBody(json));
    }

    public void indexing(File file) {
        checkFile(file);
        logging("indexing");
        try {
            String url = ElasticsearchConnection.getElasticurls();
            RequestBody request = getRequestBody(file);
            Call<JsonElement> call = ElasticsearchConnection.openBackaground().indexing(request,url);
            Response<JsonElement> responseCall = call.execute();
            if (responseCall.isSuccessful()) {
                JsonElement response = responseCall.body();
                if (response != null) {
                    logging("indexing: " + gson.toJson(response));
                }
            }
        } catch (Throwable e) {
            Logger.info("ERROR create indexing:"+e.getMessage());
            e.printStackTrace();
        }
        Logger.info("end execution indexing");
    }

    public void indexingAuditTrail(File file) {
        checkFile(file);
        logging("indexing audit trail");
        try {
            RequestBody request = getRequestBody(file);
            Call<JsonElement> call = ElasticsearchConnection.openBackaground().indexingAuditTrail(request);
            Response<JsonElement> responseCall = call.execute();
            if (responseCall.isSuccessful()) {
                JsonElement response = responseCall.body();
                if (response != null) {
                    logging("indexing: audit trail " + response.toString());
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void delete() {
        logging("delete index");
        try {
            String url = ElasticsearchConnection.getElasticurls();
            Call<JsonElement> call = ElasticsearchConnection.openBackaground().deleteCatalogue(url);
            Response<JsonElement> responseCall = call.execute();
            if (responseCall.isSuccessful()) {
                JsonElement response = responseCall.body();
                if (response != null) {
                    logging("finish delete: " + gson.toJson(response));
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
            Logger.info("ERROR delete index:"+e.getMessage());
        }
        Logger.info("end execution delete index");
    }

    public void deleteAuditTrail() {
        logging("delete index");
        try {
            Call<JsonElement> call = ElasticsearchConnection.openBackaground().deleteAuditTrail();
            Response<JsonElement> responseCall = call.execute();
            logging("response: " + responseCall.code() + " " + responseCall.message() );
            if (responseCall.isSuccessful()) {
                JsonElement response = responseCall.body();
                if (response != null) {
                    logging("finish delete: " + gson.toJson(response));
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public boolean bulkDeleteProduk(String json) {
        logging(json);
        return deleteToElasticWithResult(getRequestBody(json));
    }
    private boolean deleteToElasticWithResult(RequestBody request) {
        logging("bulk send product");
        try {
            String urls = ElasticsearchConnection.getElasticurls()+modelUrl+bulklUrl;
            Call<ResponseInputElastic> call = ElasticsearchConnection.openBackaground().deleteProduk(request, urls);
            Response<ResponseInputElastic> responseCall = call.execute();
            logging("message: " + responseCall.message() + " code: " + responseCall.code() + " " + CommonUtil.toJson(responseCall.errorBody()));
            if (responseCall.isSuccessful()) {
                ResponseInputElastic response = responseCall.body();
                if (response != null) {
                    logging("finish bulk operation: " + gson.toJson(response));
                    return response.errors;
                }
            } else {
                logging("error on send to elasticsearch");
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return false;
    }

}
