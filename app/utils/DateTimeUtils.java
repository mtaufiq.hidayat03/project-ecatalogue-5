package utils;

import org.apache.http.util.TextUtils;
import play.i18n.Messages;

import java.text.*;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class DateTimeUtils {

    public static final String DD_MM_YYYY = "dd-MM-yyyy";
    public static final String FILE_PATH_DATE = "yyyy/MM/dd";
    public static final String STANDARD_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
    public static final String STANDARD_DATE = "yyyy-MM-dd";

    private static final String[] monthNames = {
            Messages.get("januari"),
            Messages.get("februari"),
            Messages.get("maret"),
            Messages.get("april"),
            Messages.get("mei"),
            Messages.get("juni"),
            Messages.get("juli"),
            Messages.get("agustus"),
            Messages.get("september"),
            Messages.get("oktober"),
            Messages.get("november"),
            Messages.get("desember")
    };

    private static final String[] dayNames = {
            Messages.get("minggu"),
            Messages.get("senin"),
            Messages.get("selasa"),
            Messages.get("rabu"),
            Messages.get("kamis"),
            Messages.get("jumat"),
            Messages.get("sabtu")
    };

    public static String formatDateToString(Date date, String format){
        DateFormat df = new SimpleDateFormat(format);

        return df.format(date);
    }

    public static Date formatStringToDate(String string) throws Exception{
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");

        return df.parse(string);
    }

    public static String getReadableMonth(int month){
        return monthNames[month];
    }
    public static String getReadableDay(int day){
        return dayNames[day];
    }

    /**
     * required localization setting
     * it will produce string datetime: 5 Agustus 2016 06:59:35
     * @param date
     * @return String */
    public static String parseToReadableDateTime(Date date) {
        if (date != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar.get(Calendar.DATE)
                    + " " + getReadableMonth(calendar.get(Calendar.MONTH))
                    + " " + calendar.get(Calendar.YEAR)
                    + " " + String.format("%02d:%02d:%02d",
                    calendar.get(Calendar.HOUR),
                    calendar.get(Calendar.MINUTE),
                    calendar.get(Calendar.SECOND));
        }
        return "";
    }

    public static String parseToReadableDate(Date date) {
        if (date != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar.get(Calendar.DATE)
                    + " " + getReadableMonth(calendar.get(Calendar.MONTH))
                    + " " + calendar.get(Calendar.YEAR);
        }
        return "";
    }

    public static Date parseDdMmYyyy(String date) {
        try {
            SimpleDateFormat format = new SimpleDateFormat(DD_MM_YYYY);
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            return format.parse(date);
//            return new SimpleDateFormat(DD_MM_YYYY).parse(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String generateDirFormattedDate() {
        try {
            return new SimpleDateFormat(FILE_PATH_DATE).format(new Date());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String convertToDdMmYyyy(Date date) {
        if (date != null) {
            return formatDateToString(date, DD_MM_YYYY);
        }
        return "";
    }

    public static String convertToStandardDate(Date date) {
        if (date != null) {
            return formatDateToString(date, STANDARD_DATE);
        }
        return "";
    }

    public static String getStandardDateTime() {
        try {
            return new SimpleDateFormat(STANDARD_DATE_TIME).format(new Date());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String parseDdMmYyyyToStandardDate(String date) {
        if (!TextUtils.isEmpty(date)) {
            try {
                Calendar source = Calendar.getInstance();
                source.setTime(new SimpleDateFormat(DD_MM_YYYY).parse(date));
                return new SimpleDateFormat(STANDARD_DATE).format(source.getTime());
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
        return "";
    }

    public static Date parseDdMmYyyyToDate(String date) {
        if (!TextUtils.isEmpty(date)) {
            try {
                return new SimpleDateFormat(DD_MM_YYYY).parse(date);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

}
