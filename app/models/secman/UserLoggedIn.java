package models.secman;

import com.google.gson.Gson;
import models.common.AktifUser;
import models.jcommon.integrity.FileInfo;
import models.jcommon.util.CommonUtil;
import models.util.Config;
import models.util.Encryption;
import org.joda.time.DateTime;
import play.Logger;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.mvc.Http;
import play.mvc.Scope;

import javax.persistence.Transient;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Created by dadang on 9/14/17.
 */
@Table(name = "activeuser")
public class UserLoggedIn extends BaseTable {

	@Id
	public Long id;
	public String user_id;
	public String session_id;
	public String ip_address;
	public String payload;

	public Timestamp logindate = new Timestamp(new Date().getTime());
	public Timestamp logoutdate;
	public static boolean setOrUpdateActiveUser(String user_id, String ip_address, String session_id, AktifUser aktifUser){
		boolean result = false;

		//cari berdasarkan session juga, agar tidak melogout user sama pada device yang lain
		UserLoggedIn userLoggedIn = UserLoggedIn.find("user_id = ? and ip_address = ? and session_id=?",
				user_id, ip_address, session_id).first();
//		if(userLoggedIn != null){
//			userLoggedIn.session_id = session_id;
//			userLoggedIn.payload = CommonUtil.toJson(aktifUser);
//			result = userLoggedIn.save() > 0;
//		}
		if(userLoggedIn == null){
			userLoggedIn = new UserLoggedIn();
			userLoggedIn.ip_address = ip_address;
			userLoggedIn.user_id = user_id;
			userLoggedIn.session_id = session_id;
			userLoggedIn.payload = CommonUtil.toJson(aktifUser);
			userLoggedIn.logindate = new Timestamp(new Date().getTime());
			result = userLoggedIn.save() > 0;
		}

		if(!conf.aktifUserDb){
			//filebasegl 12-08-2019 perubahan baru, sesi file tanpa ip
			String directory = conf.aktifUserFileLocation+"/";
			File fileLog = new File(directory+"/"+session_id+".txt");
			//1, loged in
			//0, loged out
			String payload = Encryption.encryptInaproc(CommonUtil.toJson(aktifUser)+"|"+"1");
			//write session data
			writeNioFile(fileLog,payload);
		}

		return result;
	}

	public static void writeLogFile( String session_id, String url){
		String directory = conf.aktifUserFileLocation+"/";
		File dir = new File(directory);
		File[] matches = dir.listFiles((dir1, name) -> name.endsWith(session_id+".txt"));
		url = new DateTime().toString()+";"+url;
		if(null != matches && matches.length > 0){
			Logger.debug("log file match >:"+matches.length);
			if(matches.length == 2){
				//log user to session file loged in
				writeToFile(matches[1].getAbsolutePath(), url);
			}else{
				writeToFile(matches[0].getAbsolutePath(), url);
			}
		}else{
			Logger.info("log file match < or null ");
			String fileLogname = Http.Request.current().remoteAddress+"-"+session_id+".txt";
			String path = conf.aktifUserFileLocation+"/"+fileLogname;
			Logger.info("new log file :"+fileLogname);
			writeToFile(path, url);
		}
	}

	public static void writeLogFileGeneral(String content){
		File fileLog = null;
		String directory = conf.aktifUserLogTrail+"/";
		String strDatePath = UserLoggedIn.createSubDirDate(directory);
		final String fileGeneral = strDatePath+"/katalog_general_log.txt";
		String lineNew = System.getProperty("line.separator");
		content = Http.Request.current().remoteAddress+"|"+Scope.Session.current().getId()+"|"+ new Date().toString() +"|"+lineNew+content;
		AktifUser aktifUser = AktifUser.getAktifUser();
		if( conf.writeLogActivity && null == aktifUser){
			//log general access
			fileLog = new File(fileGeneral);
			writeNioFile(fileLog, content);
		}else{
			//log user
			if(conf.writeUserLogActivity && null != aktifUser) {
				directory = conf.aktifUserLogTrail + "/";
				directory = UserLoggedIn.createSubDirDate(directory);
				String logUser = directory +"/"+ aktifUser.user_id+".txt";
				fileLog = new File(logUser);
				writeNioFile(fileLog, content);
			}
		}
	}

	public static String createSubDirDate(String rootDir){
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		String strDatePath = rootDir+formatter.format(date);
		File fdir = new File(strDatePath+"/");
		if(!fdir.exists()){
			try{
				Files.createDirectories(Paths.get(fdir.getPath()));
			}catch (IOException e){}
		}
		return strDatePath;
	}

	private static void writeNioFile(File fileLog, String content){
		if(!fileLog.exists()){
			try {
				Files.write(Paths.get(fileLog.getPath()), (content + System.lineSeparator()).getBytes(UTF_8), StandardOpenOption.CREATE,StandardOpenOption.APPEND);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			try {
				Files.write(Paths.get(fileLog.getPath()), (content + System.lineSeparator()).getBytes(UTF_8), StandardOpenOption.WRITE,StandardOpenOption.APPEND);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Transient
	private static final String newLine = System.getProperty("line.separator");
	@Transient
	public static Config conf=Config.getInstance();
	private static synchronized void writeToFile( String path,String url)  {
		PrintWriter printWriter = null;
		File file = new File(path);
		try {
			if (!file.exists()) file.createNewFile();
			printWriter = new PrintWriter(new FileOutputStream(path, true));
			printWriter.write(newLine + url);
			printWriter.flush();
		} catch (IOException e) {
			Logger.error("WRITE log file error:"+e.getMessage());
			Logger.error("log file error path "+path);
			e.printStackTrace();
		} finally {
			//if (printWriter != null) {
			//	printWriter.flush();
				printWriter.close();
			//}
		}
	}

	public static AktifUser getAktifUserFile( String session_id){
		AktifUser result = null;
		String directory = conf.aktifUserFileLocation+"/";
		try {
			//UserLoggedIn userLoggedIn = UserLoggedIn.find("session_id = ?", session_id).first();
			String fileName = directory +session_id+".txt";
			File fileLog = new File(fileName);
			if(fileLog.exists()){
				String authLine = Files.lines(Paths.get(fileName)).findFirst().get();
				authLine = Encryption.decryptInaproc(authLine);
				String[] arAuth = authLine.split("\\|");
				if(arAuth.length > 1){
					//Logger.info("out 0 >"+arAuth[1]);
					if(arAuth[0].trim().equalsIgnoreCase("0")){
						//sudah logout
						return null;
					}else {
						try {
							String authStr = arAuth[0];
							result = new Gson().fromJson(authStr, AktifUser.class);
							AktifUser.saveToCache(result);
						}catch (Exception e){
							Logger.error("gagal parsing aktif user dari file");
						}
					}
				}
			}
//			File[] matches = dir.listFiles((dir1, name) -> name.endsWith(session_id+".txt"));
//			if(matches.length == 2){
//				Logger.info("<< READ (getAktifUserFile) session with file "+matches[1].getName());
//                Scanner scanner = new Scanner(matches[1]);
//                scanner.useDelimiter("\n");
//                String payload = "";
//                String isLogedIn = "0";
//                String otor = "";
//                while (scanner.hasNext()){
//                    otor = scanner.nextLine();// 1 line
//                    isLogedIn = scanner.nextLine(); //2 line
//                    break;//cuma butuh baca 2 line
//
//                }
//				if(isLogedIn.equalsIgnoreCase("1")){
//				    Logger.info("getAktifUserFile, file user aktif =aktif");
//                    result = new Gson().fromJson(otor, AktifUser.class);
//                }
//			}
		} catch (IOException e) {
			Logger.error("READ aktif user from file error:"+e.getMessage());
		}
		return result;
	}

	public static Boolean setAktifUserFileLogout( String session_id, AktifUser aktifUser){
		Boolean result = false;
		String directory = conf.aktifUserFileLocation+"/";
			String fileLog = directory+"/"+session_id+".txt";
			File fdir = new File(fileLog);
			if(fdir.exists()){
				result = fdir.delete();
			}
		return result;
	}
}
