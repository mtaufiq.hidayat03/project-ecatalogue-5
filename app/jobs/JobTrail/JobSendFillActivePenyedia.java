package jobs.JobTrail;

import models.penyedia.Penyedia;
import play.db.jpa.NoTransaction;
import play.jobs.Job;

@NoTransaction
public class JobSendFillActivePenyedia extends Job {

    public void doJob(){
        Penyedia.sendfillActivePenyediaToElastic();
    }
}
