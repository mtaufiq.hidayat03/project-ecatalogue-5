package models.user;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 11/24/2017
 */
public class RoleGroupKategori {

    public static final String CMS = "cms";
    public static final String KOMODITAS = "komoditas";
    public static final String SERVICES = "Services";
    public static final String PRAKATALOG = "prakatalog";
    public static final String Akses = "Akses";
    public static final String MASTERDATA = "master_data";

    public String name;
    public List<RoleGrup> items = new ArrayList<>();

    public RoleGroupKategori(String name) {
        this.name = name;
    }

}
