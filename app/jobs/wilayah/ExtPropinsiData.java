package jobs.wilayah;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import jobs.JobTrail.ExtPropinsiDataJob;
import models.api.DcePropinsi;
import models.jcommon.config.Configuration;
import models.jcommon.http.SimpleHttpRequest;
import models.masterdata.DceProvinsi;
import models.masterdata.Provinsi;
import models.util.Config;
import play.Logger;
import play.jobs.Job;
import utils.KatalogUtils;

import java.io.IOException;
import java.util.*;

public class ExtPropinsiData {
	private Properties propJob;
	public static final String DEFAULT_WAIT_TIME = "job.sirup.waittime";
	public static final String DEFAULT_WAIT_TIME_NEXT = "job.sirup.waittime.next";
	private String jsResult = "[]";
	private Boolean resultGet = false;
	public List<DcePropinsi> propinsiList = new ArrayList<>();
	private int defaultWaitTime = 0;
	private String propinsiUrl = Config.getInstance().propinsi_api;
	private String kabupatenUrl = Config.getInstance().kabupaten_api;
	private int nextWaitTime = 0;
	private int delayTaskJob = 5;// second
	private int delayJob = 10;// second
	//
	public ExtPropinsiData() {
		propJob = new Properties();
		propJob.put(DEFAULT_WAIT_TIME,
				Configuration.getConfigurationValue(DEFAULT_WAIT_TIME, ""));
		propJob.put(DEFAULT_WAIT_TIME_NEXT, Configuration
				.getConfigurationValue(DEFAULT_WAIT_TIME_NEXT, ""));

		defaultWaitTime = Integer
				.parseInt(propJob.getProperty(DEFAULT_WAIT_TIME));
		nextWaitTime = Integer
				.parseInt(propJob.getProperty(DEFAULT_WAIT_TIME_NEXT));

	}

	public List<DcePropinsi> getDceProvinsiData() {
		return getDceProvinsiData(defaultWaitTime);
	}
	public List<DcePropinsi> getDceProvinsiData(int minuteTimeOut) {
		if (fetchDceProvinsiData(minuteTimeOut)) {
			this.propinsiList = parseDceProvinsiData();
		}
		return this.propinsiList;
	}

	private Boolean fetchDceProvinsiData(Integer minuteTimeout) {
		resultGet = false;
		jsResult = "[]";
		Date tgl_update = new Date();
		String date = KatalogUtils.toDateyyyyMMdd(tgl_update);
		Integer seconds = 0;
		if (minuteTimeout > 0) {
			seconds = (minuteTimeout * 60);
		}
		Logger.info("run with minuteTimeout: " + minuteTimeout + ", and date "
				+ tgl_update.toLocaleString());
		try (SimpleHttpRequest connect = new SimpleHttpRequest(seconds)) {
			String params = "";
			connect.get(propinsiUrl + params);
			jsResult = connect.getString();
			resultGet = true;
		} catch (InterruptedException e) {
			Logger.error("Interupted Exception:" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Logger.error("IO Exception:" + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		if (resultGet == false) {
			defaultWaitTime++;
			nextWaitTime = defaultWaitTime;
			// Configuration.updateConfigurationValue(DEFAULT_WAIT_TIME_NEXT,
			// String.valueOf(newWait));
			// propJob.replace(DEFAULT_WAIT_TIME_NEXT, String.valueOf(newWait));

			// newJob(tgl_update);
		}
		return resultGet;
	}

	private List<DcePropinsi> parseDceProvinsiData() {
		try {
			propinsiList = new Gson().fromJson(jsResult,
					new TypeToken<List<DcePropinsi>>() {
					}.getType());
			Logger.info("total:" + propinsiList.size());
		} catch (Exception e) {
			Logger.error("error parse datasirup:" + e.getMessage());
			e.printStackTrace();
		}
		return propinsiList;
	}

	public void saveDataPropinsiToDb() {
		List<DceProvinsi> failed = new ArrayList<>();
		List<DceProvinsi> success = new ArrayList<>();
		Logger.info("save data Provinsi to dce_provinsi:");
		for (DcePropinsi s : propinsiList) {
			DceProvinsi rupObj = DceProvinsi.findById(s.prp_id);
			Provinsi prop = Provinsi.find("dce_id=?",s.prp_id).first();
			try {
				if (rupObj != null) {
					Long id = rupObj.id;
					rupObj = s.getAsDceProvinsi();
					rupObj.id = id;
					int result = rupObj.save();
					Logger.info("update:" + result);
				} else {
					rupObj = s.getAsDceProvinsi();
					int result = rupObj.save();
					Logger.info("save:" + result);
				}
				Logger.info("save data Provinsi to provinsi:");
				if (prop == null) {
					prop = new Provinsi();
					prop.nama_provinsi = rupObj.nama;
					prop.dce_id = rupObj.id;
					prop.active = true;
					prop.save();
				}else {
					prop.nama_provinsi = rupObj.nama;
					prop.dce_id = rupObj.id;
					prop.active = true;
					prop.save();
				}

				if (rupObj != null) {
					success.add(rupObj);
				}
			} catch (Exception e) {
				if (rupObj != null) {
					failed.add(rupObj);
				}
				Logger.info("error save or update rup:" + e.getMessage());
				e.printStackTrace();
			}
		}

	}

	private void newJob() {

		ExtPropinsiDataJob job1 = new ExtPropinsiDataJob(this, nextWaitTime);
		job1.in(delayJob);
//		new Job() {
//			String uniqueID = UUID.randomUUID().toString();
//			@Override
//			public void before() {
//				Logger.info("job id:" + uniqueID);
//				super.before();
//			}
//
//			public void doJob() {
//				Logger.info("on job execute, waittime:" + nextWaitTime);
//				getDceProvinsiData(nextWaitTime);
//				Logger.info("%%%%done getDate, saving now%%%");
//				saveDataPropinsiToDb();
//				Logger.info("done job:" + propinsiList.size());
//			}
//		}.in(delayJob);
	}

	public void run() {
		newJob();
	}
}
