-- Melakukan perubahan tabel tahapan lama menjadi tahapan yang baru
-- Menentukan fitur utama tahapan, yaitu Pemasukan Penawaran dan Klarifikasi Negosiasi dan Harga

ALTER TABLE `tahapan`
ADD `jenis_tahapan` CHAR(1) NULL,
ADD `apakah_fitur_paten` TinyInt(1) NOT NULL DEFAULT 0;

UPDATE `tahapan`
SET `apakah_fitur_paten` = 1
WHERE `nama_tahapan` = "pemasukan_penawaran" OR `nama_tahapan` = "klarifikasi_negosiasi_dan_harga";

ALTER TABLE `tahapan`
DROP COLUMN `active`;

ALTER TABLE `tahapan`
ADD `active` Int(11) NOT NULL DEFAULT 1;