package models.util.sikap;

import models.prakatalog.UsulanKualifikasi;

import java.io.Serializable;
import java.util.List;

/**
 * Created by raihaniqbal on 8/23/17.
 */
public class Sikap implements Serializable {

	public Identitas identitas;

	public List<IzinUsaha> izin_usaha;

	public Akta akta;

	public List<Pemilik> pemilik;

	public List<Pengurus> pengurus;

	public List<TenagaAhli> tenaga_ahli;

	public List<Peralatan> peralatan;

	public List<Pengalaman> pengalaman;

	public List<Pajak> pajak;

	public boolean is_verified = false;

	public boolean is_data_complete = false;

}
