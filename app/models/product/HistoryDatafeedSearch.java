package models.product;

import ext.contracts.ParamAble;
import org.apache.http.message.BasicNameValuePair;
import play.mvc.Scope;

import java.util.ArrayList;
import java.util.List;

public class HistoryDatafeedSearch implements ParamAble {
    public long komoditas = 0;
    public String penyedia = "";
    public String tanggal = "";
    public int page = 0;

    public HistoryDatafeedSearch(Scope.Params params) {
        this.komoditas = !isNull(params, "komoditas") ? params.get("komoditas", Long.class) : 0;
        this.penyedia = !isNull(params, "penyedia") ? params.get("penyedia") : "";
        this.tanggal = !isNull(params, "tanggal") ? params.get("tanggal") : "";
        this.page = getCurrentPage(params);
    }

    @Override
    public List<BasicNameValuePair> getParams() {
        List<BasicNameValuePair> results = new ArrayList<>();
        results.add(new BasicNameValuePair("komoditas", String.valueOf(this.komoditas)));
        results.add(new BasicNameValuePair("penyedia", this.penyedia));
        results.add(new BasicNameValuePair("tanggal", this.tanggal));
        return results;
    }

    @Override
    public int getPage() {
        return page;
    }
}
