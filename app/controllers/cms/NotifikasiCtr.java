package controllers.cms;

import java.text.SimpleDateFormat;
import java.util.Date;

import controllers.BaseController;
import controllers.security.AllowAccess;
import models.secman.Acl;
import models.cms.Notifikasi;
import ext.DatetimeBinder;
import play.data.binding.As;
import play.i18n.Messages;
import utils.HtmlUtil;


public class NotifikasiCtr extends BaseController {

    @AllowAccess({Acl.COM_NOTIFIKASI_EDIT})
    public static void edit(){
        Notifikasi viewNotifikasi = Notifikasi.findById(1);
        String publish_date_from;
        String publish_date_to;

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        publish_date_from = dateFormat.format(viewNotifikasi.publish_date_from);
        if(viewNotifikasi.publish_date_to != null){
            publish_date_to = dateFormat.format(viewNotifikasi.publish_date_to);
        }else{
            publish_date_to = null;
        }
        render("cms/NotifikasiCtr/edit.html",viewNotifikasi, publish_date_from, publish_date_to);
    }

    @AllowAccess({Acl.COM_NOTIFIKASI_EDIT})
    public static void editSubmit(Long data_id, String status,String param,
                                  @As(binder = DatetimeBinder.class)Date publish_date_from,
                                  @As(binder = DatetimeBinder.class)Date publish_date_end,
                                  String deskripsi){
        Notifikasi notifikasi       = Notifikasi.findById(data_id);

        String decodedText = HtmlUtil.decodeFromTinymce(deskripsi);
        notifikasi.deskripsi         = decodedText;

        notifikasi.status            = status;
        notifikasi.param             = param;
        notifikasi.publish_date_from = publish_date_from;
        notifikasi.publish_date_to   = publish_date_end;
        notifikasi.active = true;
        notifikasi.save();
        flash.success(Messages.get("notif.success.submit"));
        edit();
    }
}
