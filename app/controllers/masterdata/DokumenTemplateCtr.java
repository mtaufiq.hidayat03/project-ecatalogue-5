package controllers.masterdata;

import controllers.BaseController;
import controllers.security.AllowAccess;
import models.cms.KategoriKonten;
import models.masterdata.DokumenTemplate;
import models.secman.Acl;
import play.i18n.Messages;
import utils.HtmlUtil;

public class DokumenTemplateCtr extends BaseController {

	@AllowAccess({Acl.COM_DOCUMENT_TEMPLATE})
	public static void index() {
		render();
	}

	@AllowAccess({Acl.COM_DOCUMENT_TEMPLATE_EDIT})
	public static void edit(long template_id, long konten_id){
		DokumenTemplate dok = DokumenTemplate.findById(template_id);
		KategoriKonten kon = KategoriKonten.findById(konten_id);
		render(dok, kon);
	}

	@AllowAccess({Acl.COM_DOCUMENT_TEMPLATE_EDIT})
	public static void editSubmit(DokumenTemplate dok){
		//encode decode
		String decodedText = HtmlUtil.decodeFromTinymce(dok.template);
		dok.template = decodedText;

		if(dok.id == null){
			DokumenTemplate dokDb = new DokumenTemplate();
			dokDb.konten_kategori_id = dok.konten_kategori_id;
			dokDb.jenis_dokumen = dok.jenis_dokumen;
			dokDb.active = dok.active;
			dokDb.template = dok.template;
			dokDb.save();
		}else {
			DokumenTemplate dokDb = DokumenTemplate.findById(dok.id);
			dokDb.konten_kategori_id = dok.konten_kategori_id;
			dokDb.jenis_dokumen = dok.jenis_dokumen;
			dokDb.active = dok.active;
			dokDb.template = dok.template;
			dokDb.save();
		}
		flash.success(Messages.get("notif.success.update"));
		index();
	}

}
