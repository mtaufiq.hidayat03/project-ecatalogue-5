const MODE_PRICE = 'price';
const MODE_COST = 'cost';

var have_child = false;
var current_last_level = 1;
var checked = false;
var container = $('.alert-container');

var tempJson = {};
tempJson[MODE_PRICE] = [];
tempJson[MODE_COST] = [];

var xlsJson = {};
xlsJson[MODE_PRICE] = [];
xlsJson[MODE_COST] = [];

var page = 0;
var prevButton = $('.button-prev');
var nextButton = $('.button-next');
var classNames = [];
var checkLocations = {};
checkLocations.province = {};
checkLocations.regency = {};
var emptyExcel = 0;
var mode = 'price';

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

$('.form-horizontal').validate({
    highlight: function(element) {
        $(element).parents('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).parents('.form-group').removeClass('has-error');
    }
});

function rulesPageOne() {
    $('input[name="produk.nama_produk"]').rules('add', {
        required: true,
        alphanumeric: true
    });
    $('input[name="produk.no_produk_penyedia"]').rules('add', {
        required: true,
        maxlength: 200,
        alphanumeric: true
    });
    $('input[name="produk.berlaku_sampai"]').rules('add', {
        required: true,
        dateformat: true
    });
    $('input[name="produk.unit_pengukuran_id"]').rules('add', {
        required: true
    });
    var maxNumber = 1000000000000;
    try{
        maxNumber = Number.MAX_SAFE_INTEGER;
    }catch (e) {
        maxNumber = 1000000000000;
    }

    $('input[name="produk.jumlah_stok_form"]').rules('add', {
        required: true,
        max: maxNumber,
        min: 0,
        number: true
    });

    $('input[name="produk.jumlah_stok_inden_form"]').rules('add', {
        required: true,
        max: maxNumber,
        min: 0,
        number: true,
        greaterThan: 'input[name="produk.jumlah_stok_form"]'
    });

}

function rulesPageTwo() {
    $('input[name="produk.unspsc_id"]').rules('add', {
        required: true,
        number: true
    });
}

function priceDate() {
    $('input[name="tanggal_harga"]').rules('add', {
        required: true,
        dateformat: true
    });
}

function rulesPageThree() {
    priceDate();
    $('input[name=harga_utama]').rules('add', {
        price: true
    });
}

function rulesPageFive() {
    priceDate();
}

const validateProductEntryKeys = [
    'produk.nama_produk',
    'produk.no_produk_penyedia'
]

function automatedSaving() {
    let form = new FormData($('.product-form')[0])
    for (let entry of form.entries()) {
        if (validateProductEntryKeys.includes(entry[0]) && entry[1] === '') {
            return
        }
    }
    let currentStatus = $('#status').val();
    if (typeof currentStatus === 'undefined' || currentStatus == null || currentStatus === '') {
        form.set('model.status', 'draft')
    }
    let productId = $('#product-id').val()
    if (typeof productId !== 'undefined' && productId != null && productId !== '') {
        form.set('model.action', 'update')
    }
    $.ajax({
        type: 'POST',
        url: params.submitProduct,
        data: form,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        success: function(result){
            if (result.status && result.payload != null) {
                $('#product-id').val(result.payload.id)
                $('#form-action').val('update')
            }
        },
        error: function(data) {
        }
    })
}

function buttonValue(element) {
    if (typeof element === 'undefined' || element == null) {
          return null
    }
    return $(element).data('value')
}

function validateBeforeSave(element){

     if (buttonValue(element) === 'draft') {
        // set status to draft
        $('#status').val('draft')
     } else {
        $('#status').val('')
     }
    var isValid = $('textarea[name="model.reason"]').valid();
    var isDisclaimer = $('#disclaimer').prop('checked');

    if(!isDisclaimer){
        $.notify(
        {
            title : '<strong><h3>Warning</h3></strong>',
            message : 'Disclaimer harap di centang'
        }, 
        {
            type : 'warning',
            offset : { x : 70, y : 115}
        });
    }
    if(isValid && isDisclaimer){
        $('#btn-submit').click();
        $('#save').hide();
        jQuery.toast({
            heading: 'Please wait',
            text: 'Sending data . . . !!, dont refresh the page..!!',
            position: 'bottom-center',
            stack: false,
            icon: 'success',
            hideAfter: 30000
        })
    }
}

function addClassValidation(className) {
    classNames.push(className);
}

function rulesPriceCoverage() {
    var total = 0;
    $(".wilayah-list-item input[type=checkbox]:checked").each(function(){
        total++;
    });
    var status = total > 0;
    showAlert(!status, "Belum memilih wilayah jual");
    return status;
}


function isOngkirUploaded(data) {
    if (params.isDeliveryCostNeeded ===true && tempJson[MODE_COST].length == 0) {
        data.status = false;
        data.override = true;
        showAlert(true, "Belum upload file ongkir!");
    } else if (emptyExcel > 0) {
        data.status = false;
        data.override = true;
        showAlert(true, "Terdapat " + emptyExcel + " harga yang bernilai 0, harap upload ulang file excel, tekan tombol lihat untuk melihat data excel yang anda upload!");
    } else {
        //hide mesage
        showAlert(false, "");
    }
}

function isPriceUploaded(data) {
    if ((params.isDeliveryCostNeeded === true && tempJson[MODE_COST].length === 0) || tempJson[MODE_PRICE].length === 0) {
        data.status = false;
        data.override = true;
        showAlert(true, "Belum upload file data harga / ongkir!");
    } else if (emptyExcel > 0) {
        data.status = false;
        data.override = true;
        showAlert(true, "Terdapat " + emptyExcel + " harga yang bernilai 0, harap upload ulang file excel, tekan tombol lihat untuk melihat data excel yang anda upload!");
    } else {
        showAlert(false, "");
    }
}

$('#rootwizard').bootstrapWizard({
    onTabShow : function(tab, navigation, index) {
        $('.next').show();
        $('.simpan').hide();
        var $total = navigation.find('li').length;
        var $current = index + 1;
        var $percent = ($current / $total) * 100;
        $('#rootwizard .progress-bar').css({
            width: $percent + '%'
        });
        if ($current === $total) {
            $('.next').hide();
            $('.simpan').css('float', 'right');
            $('.simpan').show();
        }
        if (index === 2) {
            regenerateDynamicForm();
        }
        if (index === 5 || index === 4) {
            $('.unspsc-id').val($('#unspscId').val());
        }
        if (index === 5 || index === 6) {
            generateCheckedJson();
        }
        if (!(index === 0 || $current === 5)) {
            automatedSaving()
        }
    },
    onTabClick: function(tab, navigation, index,  clickedIndex) {
        return validateCurrentPage(validatePage(index, false), clickedIndex);
    },
    onNext: function (tab, navigation, index) {
        return validateCurrentPage(validatePage(index, true), -1);
    }
});

function validatePage(index, onNext) {
    var data = {};
    data.currentIndex = index;
    if (data.currentIndex === (onNext ? 1 : 0)) {
        rulesPageOne();
    } else if (data.currentIndex === (onNext ? 2 : 1)) {
        rulesPageTwo();
    } else if (data.currentIndex === (onNext ? 3 : 2)) {
        rulesPageThree();
        if ($('#images').children().length === 0) {
            showAlert(true, 'Upload gambar produk!');
            data.status = false;
            data.override = true;
        }
    }

    if (!params.isNational) {
        if (data.currentIndex === (onNext ? 4 : 3)) {
            data.status = rulesPriceCoverage();
            data.override = true;
        }
        if (data.currentIndex === ((onNext ? 5 : 4))) {
            rulesPageFive();
            isPriceUploaded(data);
        }
    } else {
        if (data.currentIndex === ((onNext ? 4 : 3))) {
            priceDate();
            //isPriceUploaded(data);
            isOngkirUploaded(data)
        }
    }
    data.status = data.override ? $('.form-horizontal').valid() && data.status : $('.form-horizontal').valid();
    return data;
}



// kelas harga
if(params.priceClass === 'kabupaten' || params.priceClass === 'provinsi'){
    $.ajax({
        type:'GET',
        url: params.priceCoverage,
        dataType:'json',
        success:function(data){
            regenerateCheckedLocationValues();
            generateWilayahJualList(data, params.priceClass);
        }
    });
}

function generateProvinsiList(prov, kelasHarga){

    var panel = $(document.createElement("div")).addClass("panel panel-default")
    var panel_heading = $(document.createElement("div")).addClass("panel-heading");
    var panel_head_row = $(document.createElement("div")).addClass("row");
    var wilayah_prov_item = $(document.createElement("div")).addClass("wilayah-prov-item");
    var prov_item_checkbox_div = $(document.createElement("div")).addClass("col-md-10");
    var prov_checkbox =$(document.createElement("div")).addClass("checkbox");
    var prov_checkbox_label = $(document.createElement("label"));
    var prov_checkbox_input = $(document.createElement("input"));
    var prov_checkbox_span = $(document.createElement("span"));

    prov_checkbox_input.attr("type","checkbox");
    prov_checkbox_span.html(prov.nama_provinsi);
    prov_checkbox_input.attr("prov",removeSpaceFromStr(prov.nama_provinsi));
    prov_checkbox_input.attr("provName",prov.nama_provinsi);
    prov_checkbox_input.attr("data-type", 1);
    prov_checkbox_input.attr("checked", checkLocations.province.hasOwnProperty(checkedKey(prov.id)));
    prov_checkbox_input.val(prov.id);
    if(kelasHarga === "provinsi"){
        prov_checkbox_input.attr("name","wilayah_jual");
        prov_checkbox_input.addClass("wilJualItems");
    }else{
        prov_checkbox_input.addClass("checkAllKab");
    }

    prov_checkbox_label.append(prov_checkbox_input);
    prov_checkbox_label.append(prov_checkbox_span);

    prov_checkbox.append(prov_checkbox_label);
    prov_item_checkbox_div.append(prov_checkbox);
    wilayah_prov_item.append(prov_item_checkbox_div);

    if(kelasHarga === "kabupaten" && (prov.kabupatens != null || prov.kabupatens.length > 0)){
        var prov_item_arrow_div = $(document.createElement("div")).addClass("col-md-1");
        var anchor = $(document.createElement("a")).addClass("collapsed");
        var anchor_icon = $(document.createElement("i")).addClass("fa fa-angle-down");


        anchor.attr("href","#"+removeSpaceFromStr(prov.nama_provinsi));
        anchor.attr("data-toggle","collapse");
        anchor.attr("aria-expanded","false");
        anchor.attr("aria-controls",prov.nama_provinsi);

        anchor.append(anchor_icon);

        prov_item_arrow_div.append(anchor);
        wilayah_prov_item.append(prov_item_arrow_div);
    }

    panel_head_row.append(wilayah_prov_item);
    panel_heading.append(panel_head_row);
    panel.append(panel_heading);

    return panel;
}

function generateKabupatenList(prov){
    var item_div = $(document.createElement("div")).addClass("collapse panel-collapse").attr("id",removeSpaceFromStr(prov.nama_provinsi));
    $.each(prov.kabupatens, function(index, kab){

        var wilayah_kab_item = $(document.createElement("div")).addClass("wilayah-kab-item");

        var kab_checkbox =$(document.createElement("div")).addClass("checkbox");
        var kab_checkbox_label = $(document.createElement("label"));

        var kab_checkbox_input = $(document.createElement("input")).addClass(removeSpaceFromStr(prov.nama_provinsi) +" wilJualItems");
        kab_checkbox_input.attr("type","checkbox");
        kab_checkbox_input.attr("name","wilayah_jual");
        kab_checkbox_input.attr("kabName",kab.nama_kabupaten);
        kab_checkbox_input.attr("provName",prov.nama_provinsi);
        kab_checkbox_input.attr("data-type", 2);
        kab_checkbox_input.val(kab.id);
        kab_checkbox_input.attr("checked", checkLocations.regency.hasOwnProperty(checkedKey(kab.id)));
        var kab_checkbox_span = $(document.createElement("span"));
        kab_checkbox_span.html(kab.nama_kabupaten);

        kab_checkbox_label.append(kab_checkbox_input);
        kab_checkbox_label.append(kab_checkbox_span);

        kab_checkbox.append(kab_checkbox_label);
        wilayah_kab_item.append(kab_checkbox);

        item_div.append(wilayah_kab_item);
    });

    return item_div;
}

function removeSpaceFromStr(str){
    return str.replaceAll(" ","");
}

function generateWilayahJualList(data, kelasHarga){

    $.each(data, function(index, prov){
        var col = $(document.createElement("div")).addClass("col-md-4");
        var div = $(document.createElement("div")).addClass("wilayah-list-item");
        var panel = generateProvinsiList(prov, kelasHarga);

        if(kelasHarga == "kabupaten" && (prov.kabupatens != null || prov.kabupatens.length > 0)){
            var item_div = generateKabupatenList(prov);
            panel.append(item_div);
        }

        col.append(div);
        div.append(panel);
        $(".wilayah-list").append(col);
    });
}

$("body").on("change", ".checkAllKab", function () {
    var prov = "."+$(this).attr("prov");
    $(prov).not(this).prop('checked', this.checked);
});

$("body").on("click", "#checkAll", function () {
    checked = !checked;
    $("input[type='checkbox']").not(this).prop('checked', checked);
});

$(document).on('click', '#addFile', function(){
    $("#fileupload").click();
});

$(document).on('click', '#fileupload', function(){
    $('#fileupload').fileupload({
        url: params.attachmentUrl,
        formData: {produkId: $('#product-id').val()},
        dataType: 'json',
        acceptFileTypes: /(\.|\/)(txt|doc|docx|xls|xlsx|pdf|zip|rar|rtf|jpe?g|png)$/i,
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                var content = '<div class="row"><div class="col-md-10 dok-file">'+file.name +'</div>' +
                    ' <div class="col-md-2"><a href="javascript:void(0)" class="removeDok pull-right" versi="'+file.versi+'" fileId="'+file.id+'">' +
                    '<i class="fa fa-trash-o" ></i>' +
                    '</a></div></div>' +
                    '<input type="hidden" name="model.file_id" value="' + file.id + '">';
                $('<li/>').addClass('list-group-item').html(content).appendTo('#files');
            });

            $("#lampiran").show();
            $('#progressFile').hide();
            $('#alert-upload').hide();

            if (data.result.errorx != null){
                //$('#alert-upload').attr('class', 'alert alert-danger').html("&{'paket.upload_failed'}");
                //alert("&{'paket.upload_failed'}");
                showToast("&{'paket.upload_failed'}");
            }
        },
        error: function (jqXHR, exception) {
            //alert(jqXHR.status);
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            alert(msg);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progressFile').show();
            $('#progressFile .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).on('fileuploadprocessalways', function (e, data) {
        var file = data.files[data.index];
        if (data.files.error && file.error) {
            var msg = file.name+' - '+file.error
            $('#alert-').attr('class', 'alert alert-danger').html(msg)
        }
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
});

$(document).on('click', '#addPicture', function(){
    $("#imageupload").click();
});

$(document).on('click', '#imageupload', function(){
    $('#imageupload').fileupload({
        url: params.imageUrl,
        dataType: 'json',
        formData: {produkId: $('#product-id').val()},
        acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
        maxFileSize: 1000000,
        done: function (e, data) {
            if(data.result.files == null){
                $('#alert-upload').html('<div class="alert alert-danger" role="alert">Gagal Upload, cek koneksi anda.</div>');
            }else {
                $.each(data.result.files, function (index, file) {

                    var div_col = $(document.createElement('div')).addClass("col-md-3");
                    var div_img = $(document.createElement('div')).css({"max-height": "310px", "overflow-y": "hidden"});
                    var img = $(document.createElement('img')).addClass("img img-responsive").css({"width": "300px"});
                    img.attr("src", file.file_url);

                    div_img.append(img);

                    var removeButton = $(document.createElement('a')).addClass("removeImg btn btn-primary");
                    removeButton.attr("href", "javascript:void(0)");
                    removeButton.attr("fileId", file.id);
                    removeButton.css({"text-align": "center", "margin-left": "60px", "margin-top": "10px"});

                    var icon = $(document.createElement('i')).addClass("fa fa-trash-o");
                    removeButton.append(icon);

                    var inputId = $(document.createElement('input'));
                    inputId.attr("type", "hidden");
                    inputId.attr("name", "model.pict_id");
                    inputId.val(file.id);

                    div_col.append(div_img);
                    div_col.append(removeButton);
                    div_col.append(inputId);

                    $("div#images").append(div_col);
                });

                $("#imgFiles").show();
                $('#progressImage').hide();
                $('#alert-upload-image').hide();
                updateImageCounter();
            }
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progressImage').show();
            $('#progressImage .progress-bar').css(
                'width',
                progress + '%'
            );
        },
        error: function (e, data) {
            $("#imgFiles").show();
            $('#progressImage').hide();
            $('#alert-upload-image').hide();
            updateImageCounter();
            $('#alert-upload').html('<div class="alert alert-danger" role="alert">Gagal Upload, cek koneksi anda.</div>');
        }
    }).on('fileuploadprocessalways', function (e, data) {
        var file = data.files[data.index];
        if (data.files.error && file.error) {
            var msg = file.name+' - '+file.error
            $('#alert-upload-image').attr('class', 'alert alert-danger').html(msg)
        }
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
});

function updateImageCounter() {
    var total = $('#images').children().length;
    $('#addPicture').val(total);
    showAlert(total === '0', "");
}

function showAlert(status, message) {
    if (status) {
        container.html('<div class="alert alert-danger fade in">\n' + message + '</div>');
        // setTimeout(function() {
        //     container.html('');
        // }, 5000);
    } else {
        container.html('');
    }
}
function showMessageSuccess(status, message) {
    if (status) {
        container.html('<div class="alert alert-success fade in">\n' + message + '</div>');
        setTimeout(function() {
            container.html('');
        }, 5000);
    } else {
        container.html('');
    }
}


$(document).on('click', '.removeDok', function(){
    var versi = $(this).attr('versi');
    var parent = $(this).parent();
    var fileId = $(this).attr('fileId');
    $.ajax({
        type: 'POST',
        url: params.deleteAttachmentUrl,
        data : {'fileId': fileId},
        success : function(data){

            if(data.status == 1){
                $(parent).parent().parent().remove();
                showMessageSuccess(true,"File Lampiran, telah terhapus");
                showMessageSuccess(false,'');
            }else{
                showMessageSuccess(true,"Gagal hapus File Lampiran, cek koneksi anda.");
                showMessageSuccess(false,'');
            }

        },
        error : function(){
            $('#alert-upload').html('<div class="alert alert-danger" role="alert">File tidak bisa dihapus</div>');
        }
    })
});

$(document).on('click', '.removeImg', function(){
    var parent = $(this).parent();
    var fileId = $(this).attr('fileId');
    $.ajax({
        type: 'POST',
        url: params.deleteImageUrl,
        data : {'fileId': fileId},
        success : function(data){
            console.log(data);
            if(data.status == 1){
                $('#alert-upload').html('<div class="alert alert-success" role="alert">Image telah terhapus</div>');
                parent.remove();
                updateImageCounter();
            }else{
                updateImageCounter();
                $('#alert-upload').html('<div class="alert alert-danger" role="alert">Gagal hapus Image, cek koneksi anda.</div>');
            }
        },
        error : function(){
            $('#alert-upload').html('<div class="alert alert-danger" role="alert">Gagal hapus Image, cek koneksi anda.</div>');
            updateImageCounter();
        }
    })
});

/*=============================================== category ===================================*/

$("body").on("change", ".pilih_kategori", function () {
    var value = $(this).val();
    $("#kategori_id").val(value);
    generateDynamicForm(value);
});

function generateDynamicForm(value) {
    $.ajax({
        url: params.attributeUrl,
        type: "post",
        dataType: "json",
        data: {
            kategori_id: value
        },
        success: function (data) {
            if(data.length > 0){
                var values = getAdditionalJson();
                var content="";
                $.each(data, function(idx, formAtr){
                    if(formAtr.tipe_input === 'label'){
                        content += "<div class='panel panel-default'>" +
                            "<div class='panel-heading'><span>"+formAtr.label_atribut+"</span></div>" +
                            "<div class='panel-body'>";
                        $.each(data, function (index, child) {
                            if(child.parent_id === formAtr.id){
                                content += generateDynamicInput(child, getAdditionalDataById(values, formAtr.id));
                            }
                        });
                        content += "</div></div>";
                    }
                });
                $.each(data, function(idx, formAtr){
                    if (formAtr.parent_id == '0' || formAtr.parent_id === '') {
                        content += generateDynamicInput(formAtr, getAdditionalDataById(values, formAtr.id));
                    }
                });
                $("#dynamic-content").html(content);
            }
        }
    });
}

function generateDynamicInput(model, value) {
    var attrClass = 'attr-id-' + model.id;
    var required = (model.wajib_diisi == 1) ? ' required' : '';
    if (required) {
        addClassValidation(attrClass);
    }
    var template = '';

    if (model.tipe_input !== 'label') {
        template = "<div class='form-group"+ required +"'>" +
            "<label class='col-sm-2 control-label'>" + model.label_atribut + "</label>" +
            "<div class='col-sm-4'>";
        if (model.tipe_input === 'text') {
            template += "<input type='text' " + required + " " +
                "data-value='" + model.id + "' " +
                "class='form-control additional-info' " +
                "value='" + value + "' " +
                "name='" + attrClass + "' " +
                "placeholder='" + model.label_atribut + "'/>";
        } else if (model.tipe_input === 'textarea') {
            template += "<textarea name='" + attrClass + "' " +
                "class='form-control additional-info' " +
                "data-value='" + model.id + "' " + required + ">" + value + "</textarea>"
        }
        template +="</div></div>";
    }
    return template;
}

function regenerateDynamicForm() {
    var cat = jQuery(".pilih_kategori").parent().find("select").val();
    if (cat !== '' && $("#dynamic-content").children().length === 0) {
        generateDynamicForm(cat);
    }
}

function getAdditionalJson() {
    var additional = $('.additional-json');
    var json = additional.val();
    var data = {};
    if (json !== '') {
        data = JSON.parse(json);
    }
    return data;
}

function getAdditionalDataById(data, key) {
    if (data.hasOwnProperty(key)) {
        return data[key];
    }
    return '';
}

$(document).on('keyup', '.additional-info', function () {
    var view = $(this);
    var value = view.val();
    var id = view.data('value');
    var additional = $('.additional-json');
    var data = getAdditionalJson();
    data[id] = value;
    additional.val(JSON.stringify(data));
});

/*=========================================== tab 2 =======================================================*/

$("#tblUnspsc").dataTable({
    ajax: {
        url: params.unspscUrl,
        cache: true},
    order: [[ 1, "asc" ], [ 1, "asc" ]],
    columns: [
        {"searchable": true },
        {"searchable": true},
        {"searchable": true },
        {"searchable": true },
        {"searchable": true},
        {"searchable": false }
    ]
});

$("body").on('click', '.addunspsc', function(){
    var value = $(this).attr("unspscid");
    $(".unspsc-id").val(value);
    $.notify(
        {
            title : '<strong><h3>Info</h3></strong>',
            message : 'Kode UNSPSC (' + value + ') berhasil ditambahkan'
        }, 
        {
            offset : { x : 70, y : 115}
        });
});

$("body").on('change', '.checkall', function(){
    var checked = $(this).is(":checked");
    if(checked){
        $(".wilJualItems").prop("checked", true);
    } else {
        $(".wilJualItems").prop("checked", false);
    }
});

$(".datepicker").datepicker({
    format: 'dd-mm-yyyy',
    language: 'id'
});

$('#dokLabel').change(function () {
    var label = $(this).val();
    if(label){
        $('#fileupload').attr("disabled", false);
    } else {
        $('#fileupload').attr("disabled", true);
    }
});

$("#download").click(function() {
    var checkedItems = $(".wilJualItems:checkbox:checked");
    var kelasHarga = params.priceClass;
    params['downloadTemplateHarga']['namaProduk'] = $("#namaProduk").val();
    var ids = new Array();
    if(kelasHarga == 'kabupaten'){
        checkedItems.each(function () {
            var item = $(this);
            var kabInfo = new Array();
            kabInfo['kabId'] = item.val();
            kabInfo['kabName'] = item.attr("kabName");
            kabInfo['provName'] = item.attr("provName");
            ids.push(kabInfo);
        });
    } else {
        checkedItems.each(function () {
            var item = $(this);
            var kabInfo = new Array();
            kabInfo['kabId'] = item.val();
            kabInfo['provName'] = item.attr("provName");
            ids.push(kabInfo);
        });
    }
    generateXlsx(ids, kelasHarga, params.downloadTemplateHarga);
});

function generateCheckedJson() {
    $(".wilayah-list-item input[type=checkbox]").each(function () {
        insertCheckedLocations($(this));
    });
    $('#checked-locations').val(JSON.stringify(checkLocations));
}

function regenerateCheckedLocationValues() {
    var json = $('#checked-locations').val();
    if (json !== '') {
        checkLocations = JSON.parse(json);
    }
}

function insertCheckedLocations(model) {
    var isChecked = model.prop('checked');
    var value = model.val();
    if (model.data('type') == 1) {
        if (isChecked) {
            if (!checkLocations.province.hasOwnProperty(value)) {
                checkLocations.province[checkedKey(value)] = true;
            }
        } else {
            if (checkLocations.province.hasOwnProperty(value)) {
                delete checkLocations.province[checkedKey(value)];
            }
        }
    } else {
        if (isChecked) {
            if (!checkLocations.regency.hasOwnProperty(value)) {
                checkLocations.regency[checkedKey(value)] = true;
            }
        } else {
            if (checkLocations.regency.hasOwnProperty(value)) {
                delete checkLocations.regency[checkedKey(value)];
            }
        }
    }
}

function checkedKey(value) {
    return "'" + value + "'";
}

$(document).on('change', '#harga_upload', function (e) {
    douploadHarga(e, "file-harga","harga-json", function (data) {
        tempJson.price = [];
        xlsJson.price = [];
        xlsJson.price = parse(data.json);
        tempJson.price = split(data.json);
        emptyExcel = data.totalEmpty;
        setToXlsJson();
    });
    $(this).val("");
});

$(document).on('change', '#ongkir_upload', function (e) {
    douploadHarga(e, "file-ongkir","ongkir-json", function (data) {
        tempJson.cost = [];
        xlsJson.cost = [];
        xlsJson.cost = parse(data.json);
        tempJson.cost = split(data.json);
        emptyExcel = data.totalEmpty;
        setToXlsJson();
    });
    $(this).val("");
});

$('#file-harga').click(function () {
    mode = MODE_PRICE;
    openModal();
});

function openModal() {
    jQuery('#myModal').modal({
        backdrop: 'static',
        keyboard: false
    });
}

jQuery('#myModal').on('show.bs.modal', function (e) {
    generateHeaderTemplate();
    changePage(page);
});

$('#addToBasketModal').on('hide.bs.modal', function (e) {
    $(".price-data-list").html('');
    $('.price-header').html('');
});

function splitForXlsJson() {
    var value = $("#xls-json").val();
    if (value !== '') {
        var data = JSON.parse(value);
        if (data !== null) {
            if (data.hasOwnProperty(MODE_PRICE)) {
                tempJson.price = splitter(data[MODE_PRICE]);
            }
            if (data.hasOwnProperty(MODE_COST)) {
                tempJson.cost = splitter(data[MODE_COST]);
            }
        }
    }
}

function setToXlsJson() {
    $("#xls-json").val(JSON.stringify(xlsJson));
}

splitForXlsJson();

function parse(json) {
    return json !== '' ? JSON.parse(json) : [];
}

function split(json) {
    var data = parse(json);
    return splitter(data);
}

function splitter(data) {
    var temporary = [];
    var i, j, chunk = 10;
    if (data.length > 0) {
        for (i = 0, j = data.length; i < j; i += chunk) {
            temporary[i/10] = data.slice(i, i + chunk);
        }
    }
    return temporary;
}

function generateHeaderTemplate() {
    var template = '<tr>';
    if (tempJson[mode][0] !== undefined && tempJson[mode][0].length > 0 && tempJson[mode][0].length > 0) {
        var model = tempJson[mode][0][0];
        for (var i in model) {
            var value = model[i];
            var className = !isNaN(value) ? 'header-right' : 'header-left';
            template += '<th class="' + className + '">' + i + '</th>';
        }
    }
    else if(mode == MODE_COST && ongkir.length > 0){ // Ipeng - 21-03-2018 : template header exiting ongkir
        template += '<th>Provinsi</th><th>Kabupaten</th><th>Ongkir</th>';
    }
    template += '</tr>';
    $('.price-header').html(template);
}

changePricePagination(prevButton);
changePricePagination(nextButton);

$('#jump').keypress(function(e) {
    if(e.which === 13) {
        var currentPage = parseInt($(this).val() - 1);
        var data = tempJson[mode][currentPage];
        if(mode == MODE_COST && tempJson[mode].length == 0 && ongkir.length > 0) data = ongkir.slice(currentPage * 10, (currentPage * 10) + 10); 
        if (data) {
            changePage(currentPage);
        }
    }
});

function changePricePagination(view) {
    view.click(function () {
        var currentPage = parseInt(view.attr('data-page'));
        var data = tempJson[mode][currentPage];
        if(mode == MODE_COST && tempJson[mode].length == 0 && ongkir.length > 0) data = ongkir.slice(currentPage * 10, (currentPage * 10) + 10); 
        if (data) {
            changePage(currentPage);
        }
    });
}

function changePageIndex(page) {
    var tempArraySize = tempJson[mode].length === 0 ? (ongkir.length > 0 && mode == MODE_COST ? Math.ceil(ongkir.length / 10) - 1 : 0) : tempJson[mode].length - 1;
    if (page >= 0 && page <= tempArraySize) {
        var prev = page - 1;
        var next = page > tempArraySize ? tempArraySize : page + 1;
        prevButton.attr('data-page', prev);
        nextButton.attr('data-page', next);
        if (prev >= 0)  {
            prevButton.removeClass('disabled');
        } else {
            prevButton.addClass('disabled');
        }

        if (next > tempArraySize) {
            nextButton.addClass('disabled');
        } else {
            nextButton.removeClass('disabled');
        }
        $('.page-counter').html('Page ' + (page + 1) + " of " + (tempArraySize + 1));
    }
}

function changePage(page) {
    var template = '';
    if (tempJson[mode][page]) {
        template = generateContentTemplate(page);
    }
    else if(mode == MODE_COST && ongkir.length > 0){
        template = generateContentTemplateExitingOngkir(page);
    }
    $('.price-data-list').html(template);
    changePageIndex(page);
}

function generateContentTemplate(page) {
    var content = '';
    for (var i in tempJson[mode][page]) {
        var template = '<tr>';
        var model = tempJson[mode][page][i];
        for (var k in model) {
            var value = model[k];
            var className;
            if (!isNaN(value)) {
                value = Number(value).toLocaleString('id');
                className = 'column-right';
            } else {
                className = 'column-left';
            }
            template += '<td class="' + className + '">' + value + '</td>';
        }
        template += '</tr>';
        content += template;
    }
    return content;
}

function generateContentTemplateExitingOngkir(page){
    var content = '';
    var start = page * 10;
    var end = start + 10;
    var records = ongkir.slice(start,end);
    $(records).each(function(idx,rec){
        var template = '<tr>';
        var kab = provKab.filter(function(item) { return item.kabId === rec.kabupatenId; })[0];
        if(kab != undefined) template += '<td class="text-center">'+kab.provName+'</td><td class="text-center">' + kab.kabName  + '</td>';
        else template += '<td></td><td></td>';
        template += '<td class="text-right">'+ rec.ongkir.toLocaleString() +'</td>';

        template += '</tr>';
        content += template;
    });
    return content;
}


$('#file-ongkir').click(function () {
    mode = MODE_COST;
    openModal();
});

function showToast(message) {
    jQuery.toast({
        heading: 'Oops',
        text: message,
        position: 'bottom-center',
        stack: false,
        icon: 'error',
        hideAfter: 6000
    })
}