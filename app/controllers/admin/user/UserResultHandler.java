package controllers.admin.user;

import models.jcommon.datatable.DatatableResultsetHandler;
import play.i18n.Messages;
import play.mvc.Router;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author HanusaCloud on 12/1/2017
 */
public class UserResultHandler {

    public final static DatatableResultsetHandler<String[]> DAFTAR_USER=
            new DatatableResultsetHandler("u.id,nama_lengkap, user_name, user_email, nama_role_alias, grup_role, active") {

                public String[] handle(ResultSet rs) throws SQLException {
                    String[] results=new String[columns.length];
                    String urlEdit = Router.reverse("controllers.admin.GrupRoleCtr.edit").url;
                    String urlDelete =Router.reverse("controllers.admin.GrupRoleCtr.delete").url;
                    boolean active = rs.getInt("active")==1;
                    String classButton = active ? "class='label label-success'":"class='label label-danger'";

                    results[0] = rs.getString("u.id");
                    results[1] = rs.getString("nama_lengkap");
                    results[2] = rs.getString("user_name");
                    results[3] = rs.getString("user_email");
                    results[4] = rs.getString("nama_role_alias");
                    results[5] = rs.getString("grup_role");
                    results[6] = String.format("<a href='#' class='column-center disabled'><span %s>%s</span></a>", classButton, rs.getInt("active")==1? Messages.get("aktif.label"):Messages.get("tidak_aktif.label"));
                    return results;
                }

            };

    public final static DatatableResultsetHandler<String[]> DAFTAR_PESERTA =
            new DatatableResultsetHandler("u.id,nama_lengkap, user_name, user_email, user_id, active") {
                public String[] handle(ResultSet rs) throws SQLException {
                    String[] results=new String[columns.length];
                    boolean active = (rs.getString("user_id") != null) ? true : false;
                    String classButton = active ? "class='label label-success'":"class='label label-danger'";

                    results[0] = rs.getString("u.id");
                    results[1] = rs.getString("nama_lengkap");
                    results[2] = rs.getString("user_name");
                    results[3] = rs.getString("user_email");
                    results[4] = String.format("<a href='#' class='column-center disabled'><span %s>%s</span></a>", classButton, active ? Messages.get("ya.label"):Messages.get("tidak.label"));
                    return results;
                }

            };
}
