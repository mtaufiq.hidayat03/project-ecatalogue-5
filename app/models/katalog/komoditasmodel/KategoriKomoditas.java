package models.katalog.komoditasmodel;

import models.BaseKatalogTable;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;

@Table(name = "komoditas_kategori")
public class KategoriKomoditas extends BaseKatalogTable {

	@Id
	public Long id;

	@Required
	public String nama_kategori;

	public String deskripsi;
	public boolean active;

    public static List<KategoriKomoditas> findAllActive(){
        String query = "select id, nama_kategori from komoditas_kategori where active = 1";

        return Query.find(query, KategoriKomoditas.class).fetch();
    }

	public static void deleteById(long id){
		KategoriKomoditas kk = KategoriKomoditas.findById(id);
		kk.active = false;
		kk.save();
	}

}
