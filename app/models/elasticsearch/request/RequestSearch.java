package models.elasticsearch.request;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class RequestSearch {

    public JsonObject query;
    public JsonObject aggs;
    public int size;
    public int from;
    public List<JsonObject> sort;
    public boolean version = true;
    @SerializedName("_source")
    public List<String> fields;

}
