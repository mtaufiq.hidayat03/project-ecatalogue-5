package PermohonanPembaruanTest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import models.katalog.Produk;
import models.katalog.ProdukAtributValue;
import models.permohonan.PembaruanStaging;
import models.permohonan.PermohonanPembaruan;
import models.permohonan.ProdukStagging;
import org.junit.Test;
import play.Logger;
import play.test.UnitTest;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

public class AllTestPembaruan extends UnitTest {
    //@Test
    public void test1(){
        Produk p = Produk.findById(762773l);
        p.withSpecifications();
        Logger.info(p.specifications.size()+" :size");
        String listObj = new Gson().toJson(p.specifications);
        ObjectMapper mapper = new ObjectMapper();
        try {
            List<ProdukAtributValue> pp3 = mapper.readValue(listObj, new TypeReference<ArrayList>() {});
            Logger.info("size:"+pp3.size());
        }
        catch (JsonParseException e) {}
        catch (JsonMappingException e) {}
        catch (IOException e) {}
    }

    //@Test
//    public void test2(){
//        Produk p = Produk.findById(762773l);
//        p.withSpecifications();
//        Logger.info(p.specifications.size()+" :size");
//        String listObj = new Gson().toJson(p.specifications);
//        PembaruanStaging pmb = new PembaruanStaging();
//        pmb.object_json = listObj;
//        List<ProdukAtributValue> list = pmb.getListProdukAtributValue();
//        Map<String, String> attributeMap = list.stream()
//                .collect(Collectors.toMap(ProdukAtributValue::getCategoriAttrId, a -> a.atribut_value));
//        Logger.info("test "+list.size());
//    }

    //@Test
    public void getListType(){
        List<ProdukAtributValue> list = new ArrayList<ProdukAtributValue>();
        list.add(new ProdukAtributValue());

        new TypeToken<List<ProdukAtributValue>>(){}.getType().getTypeName();
        Logger.info("type:"+new TypeToken<List<ProdukAtributValue>>(){}.getType().getTypeName());
    }

    //@Test
    public void testListDinamis(){
        PembaruanStaging pg = PembaruanStaging.findById(49l);
        ProdukStagging psg = pg.getIfProdukStagging();
        Produk produk = Produk.findById(psg.produk_id);
        List<ProdukAtributValue> list = psg.getListProdukAtributValue();
        Map<String, String> attributeMap = list.stream()
                .collect(Collectors.toMap(ProdukAtributValue::getCategoriAttrId, a -> a.atribut_value));
        Logger.info(attributeMap.toString());
    }

    @Test
    public void testApplyProdStagging(){
        Long id = 27l;
        PermohonanPembaruan pmb = PermohonanPembaruan.findById(id);
        pmb.applyStagging();
    }
}
