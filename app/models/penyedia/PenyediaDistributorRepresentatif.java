package models.penyedia;

import models.BaseKatalogTable;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.ArrayList;
import java.util.List;

@Table(name = "penyedia_distributor_representatif")
public class PenyediaDistributorRepresentatif extends BaseKatalogTable {

	@Id
	public Long id;
	public Long penyedia_distributor_id;
	public String nama;
	public String email;
	public String no_telp;
	public boolean active;

	public static List<PenyediaDistributorRepresentatif> setList(String[] nama_representatif,
																 String[] telp_representatif, String[] email_representatif){

		List<PenyediaDistributorRepresentatif> result = new ArrayList<PenyediaDistributorRepresentatif>();
		Integer[] arrayTotal = {nama_representatif.length,telp_representatif.length,email_representatif.length};
		int maxTotal = getMaxLengthRepresentatif(arrayTotal);

		for(int i = 0; i < maxTotal ; i++){

			PenyediaDistributorRepresentatif rep = new PenyediaDistributorRepresentatif();
			rep.nama = TextUtils.isEmpty(nama_representatif[i]) ? "" : nama_representatif[i];
			rep.email = TextUtils.isEmpty(email_representatif[i]) ? "" : email_representatif[i];
			rep.no_telp = TextUtils.isEmpty(telp_representatif[i]) ? "" : telp_representatif[i];

			result.add(rep);

		}

		return result;
	}

	private static int getMaxLengthRepresentatif(Integer[] array) {
		int max = 0;

		for (int counter = 1; counter < array.length; counter++) {
			if (array[counter] > max) {
				max = array[counter];
			}
		}

		return max;
	}

	public static List<PenyediaDistributorRepresentatif> findByDistributor(long id){
		return find("penyedia_distributor_id = ? and active = ?",id, AKTIF).fetch();
	}

	public static List<PenyediaDistributorRepresentatif> findByUserIdDistributor(long userId, long penyedia_id){
		String query = "SELECT pr.* FROM penyedia_distributor_representatif pr JOIN penyedia_distributor pd " +
				"ON pr.penyedia_distributor_id = pd.id " +
				"WHERE pd.user_id = ? AND pd.penyedia_id= ? " +
				"AND pr.active =1 " +
				"AND pd.active =1";
		return Query.find(query, PenyediaDistributorRepresentatif.class, userId, penyedia_id).fetch();
	}

	public static List<PenyediaDistributorRepresentatif> findByUserIdDistributor(long userId){
		String query = "SELECT pr.* FROM penyedia_distributor_representatif pr JOIN penyedia_distributor pd " +
				"ON pr.penyedia_distributor_id = pd.id " +
				"WHERE pd.user_id = ? " +
				"AND pr.active =1 " +
				"AND pd.active =1";
		return Query.find(query, PenyediaDistributorRepresentatif.class, userId).fetch();
	}

	public static List<PenyediaDistributorRepresentatif> findByPenyediaIdDistributor(long penyediaId){
		String query = "SELECT pr.* FROM penyedia_distributor_representatif pr JOIN penyedia_distributor pd " +
				"ON pr.penyedia_distributor_id = pd.id " +
				"WHERE pd.penyedia_id = ? " +
				"AND pr.active =1 " +
				"AND pd.active =1";
		return Query.find(query, PenyediaDistributorRepresentatif.class, penyediaId).fetch();
	}

	public static void deleteByDistributor(long id){
		delete("penyedia_distributor_id = ?",id);
	}

	public static List<PenyediaDistributorRepresentatif> setListForStagging(long[] id_representatif,String[] nama_representatif,
																			String[] telp_representatif, String[] email_representatif, long penyedia_distributor_id){

		List<PenyediaDistributorRepresentatif> result = new ArrayList<PenyediaDistributorRepresentatif>();
		Integer[] arrayTotal = {nama_representatif.length,telp_representatif.length,email_representatif.length};
		int maxTotal = getMaxLengthRepresentatif(arrayTotal);


		for(int i = 0; i < maxTotal ; i++){

			PenyediaDistributorRepresentatif rep = new PenyediaDistributorRepresentatif();

			rep.id = id_representatif[i];

			rep.penyedia_distributor_id = penyedia_distributor_id;
			rep.nama = TextUtils.isEmpty(nama_representatif[i]) ? "" : nama_representatif[i];
			rep.email = TextUtils.isEmpty(email_representatif[i]) ? "" : email_representatif[i];
			rep.no_telp = TextUtils.isEmpty(telp_representatif[i]) ? "" : telp_representatif[i];

			result.add(rep);

		}

		return result;
	}
}
