package jobs.JobTrail;

import jobs.ExtProduct.ExtProduct;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import java.util.UUID;

@NoTransaction
public class ExtProdukJob7 extends Job {
    UUID id = UUID.randomUUID();
    ExtProduct ext = new ExtProduct();

    public ExtProdukJob7(ExtProduct _ext){
        ext = _ext;
    }

    @Override
    public void doJob() {
        ext.getData(ext.defaultWaitime,ext.urlForExecutejob, ext.historyDatafeed);
    }

    public void before(){
        Logger.info("running job id:"+id+", 1st page, penyedia:"+ext.penyediaKomoditasApi.nama_penyedia);
    }
    @Override
    public void after() {
        //after finish send mail
        Logger.info(ext.penyediaKomoditasApi.nama_penyedia+" job is done, reporting job id:"+id);
    }
}
