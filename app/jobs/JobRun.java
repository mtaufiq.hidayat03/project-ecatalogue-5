package jobs;

import models.jcommon.config.Configuration;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.On;
import jobs.TrackableJob;
import play.jobs.Job;

//@On("0 40 10 * * ?")
@NoTransaction
public class JobRun extends Job {

    @Override
    public void doJob(){
        sendMail();
    }


    private void sendMail(){
        //String duration = begind+" - end:"+new Date();
       // Logger.info(begind+" - end:"+new Date());
        Logger.info("Sending mail test");
        String report = Configuration.getConfigurationValue(MailQueue.SMTP_MAIL_ADMIN, "");
        String[] ary = report.split(";");
        for (String to : ary) {
            MailQueue mq = new MailQueue();
            try {
                mq = EmailManager.createEmail(to, "Body email", "job update crawl produk");
            } catch (Exception e) {
                Logger.error("error create mail:"+e.getMessage());
            }
            Logger.info(mq.save()+"");
        }
        super.after();
        Logger.info("Sending mail test Done");
    }
}
