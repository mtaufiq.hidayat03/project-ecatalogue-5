package repositories.paket;

import com.google.gson.Gson;
import models.purchasing.PaketProduk;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.db.jdbc.Query;
import utils.LogUtil;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 11/5/2017
 */
public class PackageProductRepository {

    public static final String TAG = "PackageProductRepository";

    public static PaketProduk getDetailProduk(Long id) {
        final String query = "SELECT pp.kuantitas, p.nama_produk, p.produk_kategori_id, pp.paket_id, pp.id " +
                "FROM paket_produk pp " +
                "JOIN produk p " +
                "ON p.id = pp.produk_id " +
                "WHERE pp.id =?";
        return Query.find(query, PaketProduk.class, id).first();
    }

    /**
     * get products by paket id
     * @param id paket id
     * @return List of paket produk*/
    public static List<PaketProduk> getProducts(Long id) {
        StringBuilder sb = new StringBuilder("SELECT * " +
                "FROM paket_produk " +
                "WHERE paket_id =? AND active > 0");
        return Query.find(sb.toString(), PaketProduk.class, id).fetch();
    }

    /**
     * get all product in a package with specified ids, with product id as a key
     * @param packageId paket_id
     * @param ids reconstruct product id
     * @return map*/
    public static Map<Long, PaketProduk> getProducts(Long packageId, String ids) {
        LogUtil.d(TAG, "get product by package product ids: " + ids);
        StringBuilder sb = new StringBuilder("SELECT * " +
                "FROM paket_produk " +
                "WHERE paket_id =? AND active > 0 ");
        if (!TextUtils.isEmpty(ids)) {
            sb.append("AND id IN (").append(ids).append(")");
        }
        List<PaketProduk> results = Query.find(sb.toString(), PaketProduk.class, packageId).fetch();
        Logger.debug(" total products: " + results.size());
        return results.stream().collect(Collectors.toMap(PaketProduk::getId, p -> p));
    }

    /**
     * get products by paket id
     * @param id paket id
     * @return List of paket produk*/
    public static List<PaketProduk> getProductsFront(Long id) {
        StringBuilder sb = new StringBuilder("SELECT pp.id" +
                ", p.no_produk" +
                ", m.nama_manufaktur " +
                ", p.nama_produk" +
                ", pp.kuantitas" +
                ", pp.paket_id" +
                ", pp.produk_id" +
                ", pp.harga_satuan" +
                ", pp.harga_ongkir" +
                ", pp.batas_pengiriman"+
                ", pp.harga_total" +
                ", pp.konversi_harga_satuan" +
                ", pp.konversi_harga_ongkir" +
                ", pp.konversi_harga_total" +
                ", pp.catatan" +
                " FROM paket_produk pp" +
                " JOIN produk p"+
                " ON p.id = pp.produk_id" +
                " JOIN manufaktur m ON p.manufaktur_id = m.id " +
                " WHERE pp.paket_id =?" +
                " AND pp.active > 0");
        return Query.find(sb.toString(), PaketProduk.class, id).fetch();
    }

    /**
     * get products by paket id
     * @param id paket id
     * @return List of paket produk*/
    public static List<PaketProduk> getProductsFrontHistory(Long id) {
        StringBuilder sb = new StringBuilder("SELECT pp.id" +
                ", p.no_produk" +
                ", p.nama_produk" +
                ", pp.kuantitas" +
                ", pp.paket_id" +
                ", pp.produk_id" +
                ", pp.harga_satuan" +
                ", pp.harga_ongkir" +
                ", pp.batas_pengiriman"+
                ", pp.harga_total" +
                ", pp.konversi_harga_satuan" +
                ", pp.konversi_harga_ongkir" +
                ", pp.konversi_harga_total" +
                ", pp.catatan" +
                " FROM paket_produk pp" +
                " JOIN produk p"+
                " ON p.id = pp.produk_id" +
                " WHERE pp.paket_id =?");
        return Query.find(sb.toString(), PaketProduk.class, id).fetch();
    }

    public static Map<Long, PaketProduk> getBestPrice(String ids) {
        Logger.debug(ids);
        final String query = "SELECT pp.produk_id" +
                ", MIN(pp.harga_satuan) as best_price" +
                ", MIN(pp.konversi_harga_satuan) as conversion_best_price" +
                " FROM paket_produk pp" +
                " LEFT JOIN paket_status ps on pp.paket_id = ps.paket_id " +
                " LEFT JOIN paket on pp.paket_id = paket.id " +
                " WHERE pp.active > 0" +
//                " AND ps.status_negosiasi = 'sepakat' AND ps.status_paket = 'paket_selesai'" +
                " AND ps.status_paket = 'paket_selesai'" +
                " AND YEAR(paket.created_date) >= YEAR(NOW()) " +
                " AND produk_id IN ("+ids+")";
        List<PaketProduk> results = Query.find(query, PaketProduk.class).fetch();
        Logger.debug(new Gson().toJson(results));
        if (!results.isEmpty()) {
            return results.stream().collect(Collectors.toMap(PaketProduk::getProdukId, p -> p));
        }
        return Collections.emptyMap();
    }

    public static Map<Long, PaketProduk> getBestPriceHistory(String ids) {
        Logger.debug(ids);
        final String query = "SELECT pp.produk_id" +
                ", MIN(pp.harga_satuan) as best_price" +
                ", MIN(pp.konversi_harga_satuan) as conversion_best_price" +
                " FROM paket_produk pp" +
                " LEFT JOIN paket_status ps on pp.paket_id = ps.paket_id " +
                " LEFT JOIN paket on pp.paket_id = paket.id " +
                " WHERE " +
//                " AND ps.status_negosiasi = 'sepakat' AND ps.status_paket = 'paket_selesai'" +
                " ps.status_paket = 'paket_selesai'" +
                " AND YEAR(paket.created_date) >= YEAR(NOW()) " +
                " AND produk_id IN ("+ids+")";
        List<PaketProduk> results = Query.find(query, PaketProduk.class).fetch();
        Logger.debug(new Gson().toJson(results));
        if (!results.isEmpty()) {
            return results.stream().collect(Collectors.toMap(PaketProduk::getProdukId, p -> p));
        }
        return Collections.emptyMap();
    }

}
