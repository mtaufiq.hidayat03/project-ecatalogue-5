package models.masterdata;

import models.BaseKatalogTable;
import play.db.jdbc.CacheMerdeka;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by dadang on 8/7/17.
 */

@Table(name = "kabupaten")
public class Kabupaten extends BaseKatalogTable {

	@Id
	public Long id;

	public String nama_kabupaten;

	public Long provinsi_id;

	public Long kldi_id;

	public Long dce_id;

	public boolean active;

	public Long getId() {return id;}
	public String getNama() {return nama_kabupaten;}
	public static List<Kabupaten> findByProvId(long provinsi_id){

		return find("provinsi_id = ? and active = 1", provinsi_id).fetch();
	}

	@CacheMerdeka
	public static Map<Long, String> findAllActive(){
		List<Kabupaten> kabupatenList = Kabupaten.find("active = 1").fetch();
		Map<Long, String> kabupatens = kabupatenList.stream()
				.collect(Collectors.toMap(obj -> obj.id, obj -> obj.nama_kabupaten));

		return kabupatens;
	}

	public static Kabupaten findByName(String name){
		return find("nama_kabupaten = ? and active = ?",name, AKTIF).first();
	}

	public static List<Kabupaten> findByIds(List<String> ids){
		return Kabupaten.find("id in (" + String.join(",", ids) + ")").fetch();
	}

	public void setAktif() {
		this.active = true;
		prePersist();
		save();
	}

	public void setNonAktif() {
		this.active = false;
		prePersist();
		save();
	}

}
