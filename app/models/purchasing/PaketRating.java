package models.purchasing;

import models.Feedback;
import org.apache.http.util.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 11/21/2017
 */
public class PaketRating {

    public int accuracy = 0;
    public int service = 0;
    public int speed = 0;

    public String accuracyMessage = "";
    public String serviceMessage = "";
    public String speedMessage = "";

    public String additionalMessage = "";

    public Long packageId;

    public List<Feedback> getFeedBack() {
        List<Feedback> feedback = new ArrayList<>(4);
        feedback.add(new Feedback().setFromAccuracyRating(this));
        feedback.add(new Feedback().setFromSpeedRating(this));
        feedback.add(new Feedback().setFromServiceRating(this));
        feedback.add(new Feedback().setAdditionalMessage(this));
        return feedback;
    }

    public double getAverageRating() {
        final int total = accuracy + service + speed;
        return total > 0 ? Math.ceil((double) ((total) / 3)) : total;
    }

    public PaketRating fromArray(List<Feedback> list) {
        if (!list.isEmpty()) {
            for (Feedback model : list) {
                if (model.isPackageAccuracy()) {
                    this.accuracy = model.rating;
                }
                if (model.isPackageAdditionalMessage()) {
                    this.additionalMessage = TextUtils.isEmpty(model.message) ? "" : model.message;
                }
                if (model.isPackageSpeed()) {
                    this.speed = model.rating;
                }
                if (model.isPackageService()) {
                    this.service = model.rating;
                }
            }
        }
        return this;
    }

}
