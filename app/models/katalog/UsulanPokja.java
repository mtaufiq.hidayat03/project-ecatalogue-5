package models.katalog;

import models.BaseKatalogTable;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.sql.Timestamp;
import java.util.List;

@Table(name = "usulan_pokja")
public class UsulanPokja extends BaseKatalogTable {

    @Id
    public Long id;
    public Long usulan_id;
    public Long pokja_id;
    public boolean active;

	public void reactivate() {
		active = true;
        deleted_by = null;
        deleted_date = null;
        save();
	}

}