package models.util.sikap;

import play.data.validation.Required;

import java.util.List;

/**
 * Created by dadang on 7/11/17.
 */
public class IzinUsaha {

	@Required
	public long ius_id;
	@Required
	public String source_data;
	@Required
	public String jni_nama;
	@Required
	public String ius_no;
	@Required
	public String jni_id;
	@Required
	public String ius_berlaku;
	@Required
	public String ius_tanggal;
	@Required
	public String kls_id;
	@Required
	public long rkn_id;
	@Required
	public String ius_instansi;
	@Required
	public String kls_nama;
	public String status_verifikasi;
	public boolean is_verified = false;

	public boolean deleted = false;
	public String alasan_pembatalan = "";
	public String keterangan_pembuktian = "";

	public List<IzinUsahaKlasifikasi> ijin_usaha_klasifikasi;
}
