package features.komoditas.repositories;

import models.katalog.Komoditas;

import java.util.List;

/**
 * @author HanusaCloud on 5/20/2020 11:35 AM
 */
public interface KomoditasRepositoryContract {

    List<Komoditas> getUmkms();

}
