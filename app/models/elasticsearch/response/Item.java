package models.elasticsearch.response;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 * @author HanusaCloud on 10/15/2017
 */
public class Item {

    @SerializedName("_index")
    public String index;
    @SerializedName("_type")
    public String type;
    @SerializedName("_id")
    public String id;
    @SerializedName("_version")
    public long version;
    @SerializedName("result")
    public String result;
    @SerializedName("created")
    public boolean created;
    @SerializedName("status")
    public int status;
    @SerializedName("_shards")
    public Map<String, Object> shards = new HashMap<>();

}
