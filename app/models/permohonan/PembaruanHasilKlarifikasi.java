package models.permohonan;

import models.BaseKatalogTable;
import models.common.DokumenInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.DateTimeUtils;
import utils.LogUtil;

import javax.persistence.Transient;
import java.io.File;
import java.sql.Timestamp;
import java.util.List;

@Table(name = "permohonan_pembaruan_hasil_klarifikasi")
public class PembaruanHasilKlarifikasi extends BaseKatalogTable {
    @Id
    public Long id;
    public Long permohonan_pembaruan_id;
    public String deskripsi;
    public String file_hash;
    public String nama_file;
    public Long blb_id_content;
    public int active;

    @Transient
    public String nama_lengkap;

    public static List<PembaruanHasilKlarifikasi> findActiveByPermohonan(long permohonanId){
        String query = "select hslKlarif.*, u.nama_lengkap " +
                "from permohonan_pembaruan_hasil_klarifikasi hslKlarif " +
                "left join user u on hslKlarif.created_by = u.id and u.active = 1 " +
                "where hslKlarif.active = 1 and hslKlarif.permohonan_pembaruan_id = ? ";
        return Query.find(query, PembaruanHasilKlarifikasi.class, permohonanId).fetch();
    }

    public boolean hasilKlarifikasiSubmit(PembaruanHasilKlarifikasi hasilKlarifikasi, File file){
        try {
            BlobTable blob = null;
            if(file != null) {
//                if(file.length() <= 5242880) {
                blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, this.blb_id_content, PembaruanHasilKlarifikasi.class.getSimpleName());
                this.nama_file = blob.blb_nama_file;
                this.file_hash = blob.blb_hash;
                this.blb_id_content = blob.id;
//                }else{
//                    return false;
//                }
            }
            this.permohonan_pembaruan_id = hasilKlarifikasi.permohonan_pembaruan_id;
            this.deskripsi = hasilKlarifikasi.deskripsi;
            this.active = hasilKlarifikasi.active;

            Long idr = new Long(this.saveHasilKlarifAndRiwayat());
            if(null != blob){
                blob.blb_id_content = idr;
                blob.save();
            }

            return true;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            LogUtil.d("***************** error ***************", e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public int saveHasilKlarifAndRiwayat(){
        PembaruanRiwayat rw = new PembaruanRiwayat();
        int ret = this.save();
        if(null == this.id){
            rw.addPembaruanHasilKlarif(this.permohonan_pembaruan_id,this.deskripsi);
        }else{
            rw.editPembaruanHasilKlarif(this.permohonan_pembaruan_id,this.deskripsi);
        }
        return ret;
    }

    public String getFileUrl() {
        String url = "";

        if(blb_id_content != null && blb_id_content.toString() != ""){
            BlobTable blobTable = BlobTable.findByBlobId(blb_id_content);
            if(blobTable != null){
                DokumenInfo dokumenInfo = DokumenInfo.findDokumenKontrak(blobTable);
                url = dokumenInfo.download_url;
            }
        }

        return url;
    }

    public String getCreatedDate(){
        String getDate = getReadableDate(created_date);
        return getDate;
    }

    public String getModifiedDate(){
        String getDate = getReadableDate(modified_date);
        return getDate;
    }

    public String getReadableDate(Timestamp date) {
        String requestDate = DateTimeUtils.parseToReadableDate(date);
        if(requestDate != null && !requestDate.equalsIgnoreCase("")){
            return requestDate;
        }else{
            return "N/A";
        }
    }

    public static int removeDocument(Long dispoId){
        String query = "update permohonan_pembaruan_hasil_klarifikasi set file_hash = null, nama_file = null, blb_id_content = null where id = ?";
        return Query.update(query, dispoId);
    }
}
