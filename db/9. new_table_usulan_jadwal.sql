CREATE TABLE `usulan_jadwal`(
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`usulan_id` Int(11) NOT NULL,
	`tahapan_id` Int(11) NOT NULL,
	`tanggal_mulai` DateTime NOT NULL,
	`tanggal_selesai` DateTime NOT NULL,
	`adalah_awal_penawaran` INT(1) DEFAULT 0,
	`adalah_akhir_penawaran` INT(1) DEFAULT 0,
	`created_date` DateTime NULL,
	`created_by` Int( 5 ) NULL,
	`modified_date` DateTime NULL,
	`modified_by` Int( 5 ) NULL,
	`deleted_date` DateTime NULL,
	`deleted_by` Int( 5 ) NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`auditdate` Timestamp NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`active` INT(1) NULL,
	PRIMARY KEY (`id`)
)
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;