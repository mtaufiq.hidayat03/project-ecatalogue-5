package controllers.security;

import models.secman.Acl;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by dadang on 9/6/17.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface AllowAccess {
	public Acl[] value();
}
