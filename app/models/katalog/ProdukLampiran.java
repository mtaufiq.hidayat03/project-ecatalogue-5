package models.katalog;

import models.BaseKatalogTable;
import models.common.AktifUser;
import models.common.DokumenInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.io.File;
import java.util.List;

@Table(name = "produk_lampiran")
public class ProdukLampiran extends BaseKatalogTable {

	@Id
	public Long id;
	public Integer produk_id;
	public String file_name;
	public String original_file_name;
	public String file_sub_location;
	public String dok_hash;
	public String dok_signature;
	public Long dok_id_attachment;
	public Long file_size = 0L;
	public String deskripsi;
	public String file_url;
	public Integer posisi_file = 0;
    public Integer active;
	@Transient
	public String urlDownload;
	public static DokumenInfo simpanLampiran(File file, Long productId) throws Exception {
		return simpanLampiran(file,productId,1);//aktif
	}
	public static DokumenInfo simpanLampiran(File file, Long productId, Integer aktif) throws Exception {

		ProdukLampiran lampiran = new ProdukLampiran();
		BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, lampiran.dok_id_attachment, ProdukLampiran.class.getName());
		lampiran.dok_id_attachment = blob.id;
		lampiran.file_name = blob.blb_nama_file;
		lampiran.dok_hash = blob.blb_hash;
		lampiran.file_size = blob.blb_ukuran;
		lampiran.active = aktif;//active
		lampiran.produk_id = productId != null ? productId.intValue() : 0;
		lampiran.created_by = AktifUser.getActiveUserId();
		lampiran.save();

		return DokumenInfo.findByBlob(blob);
	}

	public static void updateProdukLampiran(long id, long produk_id){
		String sql = "update produk_lampiran set produk_id = :produk_id, active=1  where dok_id_attachment = :id";

		Query.update(sql, produk_id, id);
	}

	public static List<ProdukLampiran> getDocumentsByProductId(Long id) {
		return find("produk_id =? AND active =?", id, AKTIF).fetch();
	}
//select pl.* from produk_lampiran pl where pl.produk_id=? and pl.active=1 order by pl.modified_date desc

	public static ProdukLampiran getAktifByProdukId(Long produkid){
		String sql = "select pl.* from produk_lampiran pl where pl.produk_id=? and pl.active=1 order by pl.modified_date desc";
		ProdukLampiran pl = Query.find(sql,ProdukLampiran.class,produkid).first();
		return pl;
	}

	public String getDownloadUrlFromBlob(){
		if(null != dok_id_attachment){
			BlobTable blob = BlobTable.find("id = ?", dok_id_attachment).first();
			if (blob != null) {
				urlDownload = blob.getDownloadUrl(null);
			}
		}
		return this.urlDownload;
	}
}
