package models.prakatalog;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;


/**
 * Created by dadang on 7/21/17.
 */
@Table(name = "dokumen_kategori")
public class DokumenKategori extends BaseKatalogTable {

	@Id
	public Long id;
	public String nama_kategori;
	public Long active;

	public static DokumenKategori findByKategori(String kategori){
		String query = "SELECT id, nama_kategori FROM dokumen_kategori WHERE nama_kategori = '" + kategori + "' ";
		return Query.find(query,DokumenKategori.class).first();
	}
}
