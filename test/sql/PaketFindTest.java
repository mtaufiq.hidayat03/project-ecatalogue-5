package sql;

import java.util.List;

import org.apache.commons.lang.time.StopWatch;
import org.junit.Test;

import models.purchasing.Paket;
import play.Logger;
import play.test.UnitTest;

public class PaketFindTest extends UnitTest {

//	//@Test
	public void testLike()
	{
		StopWatch sw=new StopWatch();
		sw.start();
		List<Paket> list=Paket.findNamaLike("jalan");
		sw.stop();
		Logger.debug("[PaketFindTest] LIKE, size: %,d dur: %s", list.size(), sw);
	}
	
//	//@Test
	public void testMatch()
	{
		StopWatch sw=new StopWatch();
		sw.start();
		List<Paket> list=Paket.findNamaMatch("jalan");
		sw.stop();
		Logger.debug("[PaketFindTest] MATCH, size: %,d dur: %s", list.size(), sw);
	}
	
	//@Test
	public void findByKeywordAndKomoditas()
	{
		Paket.findByKeywordAndKomoditas("OK", 1, "");
	}
	
	
}
