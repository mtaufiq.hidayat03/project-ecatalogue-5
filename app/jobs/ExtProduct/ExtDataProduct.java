package jobs.ExtProduct;


import jobs.JobTrail.ExtProdukJob1;
import jobs.TrackableJob;
import models.api.produk.HistoryDatafeed;
import models.jcommon.config.Configuration;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import play.Logger;
import play.jobs.Job;
import play.jobs.On;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;

//@On("0 33 00 * * ?")
public class ExtDataProduct extends TrackableJob {

    public Date begind = new Date();

    @Override
    public void doJob(){

        ExtProduct ext = new ExtProduct();
        List<PenyediaKomoditasApi> list = ext.getUrlPenyedia(77l, 1l);
        HistoryDatafeed hdf = new HistoryDatafeed();
        int delaySecond = 1;
        for (PenyediaKomoditasApi pk: list) {
            Logger.debug(">>>>>>>>>:"+pk.nama_penyedia);
            ExtProdukJob1 exp1 = new ExtProdukJob1(pk,hdf);
            exp1.in(delaySecond++);
//            new Job(){
//                UUID id = UUID.randomUUID();
//                PenyediaKomoditasApi _pk = pk;
//                public void doJob(){
//                    ExtProduct ext = new ExtProduct();
//                    hdf.id_job = id.toString();
//                    hdf.penyedia_id = _pk.penyediaid;
//                    hdf.penyedia_name = _pk.nama_penyedia;
//                    hdf.start_time = new Timestamp(new Date().getTime()).toString();
//                    hdf.save();
//                    ext.executeOne(_pk,hdf);
//                }
//            }.in(delaySecond++);
        }
    }

    @Override
    public void after(){
        String duration = "start: "+begind+" - end:"+new Date()+", result child job will available separately";
       // Logger.info(begind+" - end:"+new Date());
        String report = Configuration.getConfigurationValue(MailQueue.SMTP_MAIL_ADMIN, "");
        String[] ary = report.split(";");
        for (String to : ary) {
            MailQueue mq = new MailQueue();
            try {
                mq = EmailManager.createEmail(to, duration, "job update crawl produk");
            } catch (Exception e) {
                Logger.error("error create mail:"+e.getMessage());
            }
            Logger.info(mq.save()+"");
        }
        super.after();
    }
}
