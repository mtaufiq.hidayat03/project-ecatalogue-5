package models.purchasing.status;

/**
 * @author HanusaCloud on 11/8/2017
 */
public class ProgressStatus {

    public String name;
    public String number;
    public boolean isActive;

    public ProgressStatus(String name, String number, boolean isActive) {
        this.name = name;
        this.number = number;
        this.isActive = isActive;
    }

}
