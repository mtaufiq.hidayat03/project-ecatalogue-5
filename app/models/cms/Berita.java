package models.cms;

import ext.DatetimeBinder;
import models.BaseKatalogTable;
import models.common.AktifUser;
import models.common.DokumenInfo;
import models.jcommon.blob.BlobTable;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import play.cache.CacheFor;
import play.data.binding.As;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.KatalogUtils;

import javax.persistence.Transient;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Table(name = "konten")
public class Berita extends BaseKatalogTable {
	
	public static int LIMIT_CHAR_TITLE = 50;
	public static int LIMIT_DESC = 100;

    @Id
    public Long id;
    public Long default_konten_kategori_id;
    public String judul_konten;
    public String isi_konten;
    public String status;
    public String slug;
    public String tags;
    public String param;
    public Long viewer;
    @Transient
    public String nama_kategori;
    public String file_hash;
    public String nama_file;
    public Long blb_id_content;
    public boolean active;
    @Transient
    public int jumlah_berita;
    @Transient
    public String nama_lengkap;


    @As(binder = DatetimeBinder.class)
    public Date tanggal_konten;

    @As(binder = DatetimeBinder.class)
    public Date publish_date_from;

    @As(binder = DatetimeBinder.class)
    public Date publish_date_to;

    public static void deleteById(long id){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String date_now = dateFormat.format(date);

        String query = "update konten set active = 0, deleted_by = " + AktifUser.getActiveUserId() + ", deleted_date = '"+date_now+"' " +
                "where id = ?";

        Query.update(query, id);
    }

    public static List<Berita> findAllActive(){
        return Berita.find("active = 1").fetch();
    }

    public static List<Berita> findAllPublished(){

        return Berita.find("active = 1 and status = 'published' and publish_date_from <= now() and " +
                "(publish_date_to is NULL OR publish_date_to >= now())").fetch();
    }

    public static List<Berita> findAllCountBerita(){
        String query = "SELECT kk.id, kk.nama_kategori, COUNT(kon.default_konten_kategori_id) AS jumlah_berita FROM konten kon LEFT JOIN konten_kategori kk ON kk.id = kon.default_konten_kategori_id " +
                "WHERE kon.active = 1 and kon.status = 'published' and kon.publish_date_from <= now() and (kon.publish_date_to is NULL or kon.publish_date_to >= now()) GROUP BY kon.default_konten_kategori_id";
        return Query.find(query,Berita.class).fetch();
    }

    public static List<Berita> findAllBeritaFromKategori(String kategori){
        String query = "SELECT kon.id, kon.judul_konten, kon.param, kk.nama_kategori, kon.tanggal_konten, kon.isi_konten, kon.file_hash, kon.nama_file, kon.blb_id_content " +
                "FROM konten kon JOIN konten_kategori kk ON kon.default_konten_kategori_id = kk.id " +
                "WHERE kon.active = 1 AND kon.status = 'published' AND kk.active =1 " +
                "AND kon.publish_date_from <= now() AND (kon.publish_date_to is NULL OR kon.publish_date_to >= now()) " +
                "AND kk.nama_kategori = '"+kategori+"' ORDER BY kon.created_date";

        return Query.find(query,Berita.class).fetch();
    }

    @CacheFor("5mn")
    public static List<Berita> findLastesBerita(){
        return Berita.find("active = 1 and status = 'published' and publish_date_from <= now() and (publish_date_to is NULL OR publish_date_to >= now()) ORDER BY created_date desc LIMIT 4").fetch();
    }

    @CacheFor("5mn")
    public static List<Berita> findPopularBerita(){
        return Berita.find("active = 1 and status = 'published' and publish_date_from <= now() and (publish_date_to is NULL OR publish_date_to >= now()) ORDER BY viewer desc, tanggal_konten desc " +
                "LIMIT 5").fetch();
    }

    public static List<Berita> findJoinKategoriAllActiveByLimit(Long start, Long end){

        String query = "SELECT kon.id,kon.judul_konten, kk.nama_kategori, kon.tanggal_konten, kon.slug, kon.isi_konten, kon.param, kon.file_hash, kon.nama_file, kon.blb_id_content " +
                        "FROM konten kon JOIN konten_kategori kk ON kon.default_konten_kategori_id = kk.id " +
                        "WHERE kon.active = 1 AND kk.active =1 AND kon.status = 'published' " +
                        "and kon.publish_date_from <= now() and (kon.publish_date_to is NULL OR kon.publish_date_to >= now())" +
                        "ORDER BY kon.created_date DESC LIMIT "+start+","+end;

        return Query.find(query,Berita.class).fetch();
    }

    public static List<Berita> findBerita(String keyword,Long start, Long end){
        keyword = keyword == null? "":"%"+keyword+"%";
        start = start == null ? 0 : start;
        end = end == null ? 12 : end;
        String query = "SELECT kon.id, kon.judul_konten, kon.param, kk.nama_kategori, kon.tanggal_konten," +
                " kon.isi_konten, kon.slug, kon.file_hash, kon.nama_file, kon.blb_id_content FROM konten kon JOIN konten_kategori kk ON kon.default_konten_kategori_id = kk.id " +
                "WHERE kon.active = 1 AND kon.status = 'published' AND kk.active =1 AND kon.judul_konten like ? "+
                "and kon.publish_date_from <= now() and (kon.publish_date_to is NULL OR kon.publish_date_to >= now())" +
                "ORDER BY kon.created_date DESC LIMIT ?,?";

        return Query.find(query,Berita.class,keyword,start,end).fetch();
    }

    public static List<Berita> findBeritaByKategori(String kategori, Long start, Long end){
        String query = "SELECT kon.id, kon.judul_konten, kon.param, kk.nama_kategori, kon.tanggal_konten," +
                " kon.isi_konten, kon.slug, kon.file_hash, kon.nama_file, kon.blb_id_content " +
                "FROM konten kon JOIN konten_kategori kk ON kon.default_konten_kategori_id = kk.id " +
                "WHERE kon.active = 1 AND kon.status = 'published' AND kk.active =1 " +
                "and kon.publish_date_from <= now() and (kon.publish_date_to is NULL OR kon.publish_date_to >= now()) " +
                "and kk.nama_kategori = '"+kategori+"' ORDER BY kon.created_date DESC LIMIT "+start+" , "+end;

        return Query.find(query,Berita.class).fetch();
    }

    public static Berita findDetailBerita(String slug){

        String queryView = "SELECT kon.judul_konten, kon.param, kk.nama_kategori, kon.tanggal_konten, kon.isi_konten, " +
                "usr.nama_lengkap, kon.viewer, kon.slug, kon.tags, kon.file_hash, kon.nama_file, kon.blb_id_content FROM konten kon LEFT JOIN konten_kategori kk " +
                "ON kon.default_konten_kategori_id = kk.id " +
                "LEFT JOIN user usr ON kon.created_by = usr.id WHERE kon.slug = ?";

//        String queryUpdateViewer = "UPDATE konten SET viewer = IFNULL(viewer,0) + 1 WHERE slug = ?";
//        Query.update(queryUpdateViewer, slug);

        return Query.find(queryView,Berita.class, slug).first();
    }

    public String getViewer(){
        if(this.viewer != null && this.viewer >= 1000){
            float number = viewer;
            DecimalFormat myFormatter = new DecimalFormat("#.# K");
            return  myFormatter.format(number/1000);
        }
        return String.valueOf(viewer != null ? viewer : 0);
    }

    public String getTanggal(String format){
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        if(tanggal_konten != null){
            return sdf.format(tanggal_konten);
        }
        return null;
    }

    public String getShortBerita(){
        int size = Jsoup.parse(isi_konten).text().length();
        if(isi_konten != null && size > LIMIT_DESC){
            return Jsoup.parse(isi_konten).text().substring(0, LIMIT_DESC) + "...";
        }else if(isi_konten != null){
            return Jsoup.parse(isi_konten).text();
        }
        return null;
    }
    
    public String getTrimmedTitle() {
		if(this.judul_konten.length() < LIMIT_CHAR_TITLE) {
			return this.judul_konten;
		}
		return this.judul_konten.substring(0, LIMIT_CHAR_TITLE) + "...";
	}

	public String getSlugUrl(){
        return this.slug.replace(" ","-");
    }

    public String getImageFileString(){
        String result = "#";
        if(null != param) {
            JSONParser parser = new JSONParser();
            JSONObject json = null;
            try {
                json = (JSONObject) parser.parse(param);
                String pict = (String) json.get("fotoPendamping");
                String[] arpath = pict.split("/");
                if (arpath.length > 0) {
                    result = arpath[(arpath.length - 1)];
                } else {
                    result = pict;
                }
            } catch (ParseException e) {}
        }
        result = "/public/files/image/berita/"+result;
        if(!KatalogUtils.fileExistInPathApps(result)){
            result = "";
        }
        return result;
    }

    public String getFileUrl() {
        String url = "";

        if(blb_id_content != null && blb_id_content.toString() != ""){
            BlobTable blobTable = BlobTable.findByBlobId(blb_id_content);
            if(blobTable != null){
                DokumenInfo dokumenInfo = DokumenInfo.findDokumenKontrak(blobTable);
                url = dokumenInfo.download_url;
            }
        }

        return url;
    }

    public static int removeFile(long beritaId){
        String query = "update konten set file_hash = null, nama_file = null, blb_id_content = null where id = ?";
        return Query.update(query,beritaId);
    }
}