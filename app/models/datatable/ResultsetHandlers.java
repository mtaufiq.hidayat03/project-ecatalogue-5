package models.datatable;

import com.google.gson.JsonObject;
import controllers.BaseController;
import ext.FormatUtils;
import models.common.AktifUser;
import models.common.ButtonGroup;
import models.common.DokumenInfo;
import models.elasticsearch.product.ProductPriceElastic;
import models.jcommon.blob.BlobTable;
import models.jcommon.datatable.DatatableResultsetHandler;
import models.katalog.Komoditas;
import models.katalog.Produk;
import models.katalog.ReportRiwayat;
import models.masterdata.Tahapan;
import models.permohonan.PermohonanPembaruan;
import models.prakatalog.Penawaran;
import models.prakatalog.Usulan;
import models.util.Config;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.i18n.Lang;
import play.i18n.Messages;
import play.mvc.Router;
import services.datatable.DataTableService;
import services.datatable.PenawaranDataTableService;
import services.prakatalog.UsulanService;
import utils.KatalogUtils;
import utils.UrlUtil;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**ResultsetHandler dimasukkan sebagai konstanta di dalam class ini
 * @author Andik
 */
public class ResultsetHandlers {
	public static final String _imageFoldersub = "/produk_gambar/";
	public final static DatatableResultsetHandler<String[]> DAFTAR_PRODUK=
			new DatatableResultsetHandler("p.id, m.nama_manufaktur, pen.nama_penyedia, p.no_produk_penyedia, p.apakah_dapat_dibeli, p.apakah_ditayangkan, p.status, p.komoditas_id, p.no_produk, p.setuju_tolak, " +
					"p.produk_gambar_file_sub_location, p.produk_gambar_file_name, file_url ,p.jumlah_stok, p.jumlah_stok_inden, pk.nama_kategori, " +
					"p.nama_produk, k.kelas_harga") {

				public String[] handle(ResultSet rs) {
					String[] results=new String[columns.length];
					Config conf=Config.getInstance();
					Produk model = new Produk();
					try {
						model.id = rs.getLong("id");

					model.kelas_harga = rs.getString("kelas_harga");
					//String gambar = rs.getString("p.produk_gambar_file_sub_location")+rs.getString("p.produk_gambar_file_name");
					String gambar = rs.getString("file_url");
                    results[0] = String.format("<input type='checkbox' name='idProduk' value='%s' class='idProduks' />", model.id);
					String localPath = "";
					if(StringUtils.isEmpty(gambar)){
						results[1] = "";
					}else{
						localPath =  Config.getInstance().fileImageGallery + _imageFoldersub+gambar;
						if(!KatalogUtils.fileExistInPathApps(localPath)){
							localPath = conf.cdnServerImage100+_imageFoldersub+gambar;
						}
						results[1] = String.format("<img src='%s' onerror=\"this.src='/public/images/image-not-found.png';\">", localPath);
					}
					results[2] =
							"<div>"+rs.getString("p.no_produk")+"</div>" +
									"<div>"+rs.getString("m.nama_manufaktur")+" "+rs.getString("p.nama_produk")+"</div>";
					results[3] = rs.getString("pen.nama_penyedia");
					results[4] = rs.getString("p.no_produk_penyedia");

					String spanClassDibeli = rs.getInt("p.apakah_dapat_dibeli") == 1 ? "<span style='color:green' class='fa fa-check fa-lg'></span>" : "<span style='color:red' class='fa fa-close fa-lg'></span>";
					String spanClassTayang = rs.getInt("p.apakah_ditayangkan") == 1 ? "<span style='color:green' class='fa fa-check fa-lg'></span>" : "<span style='color:red' class='fa fa-close fa-lg'></span>";

					JsonObject map = new JsonObject();
					Long idProds = rs.getLong("id");
					model.nama_produk = rs.getString("p.nama_produk");
					map.addProperty("id", rs.getLong("id"));

					HashMap<String, Object> infoProds = new HashMap<>();
					infoProds.put("id", rs.getLong("id"));

					if (rs.getString("p.setuju_tolak") != null){
						if (rs.getString("p.setuju_tolak").equalsIgnoreCase("setuju")){
							results[5] = Messages.get("disetujui.label");
						} else if (rs.getString("p.setuju_tolak").equalsIgnoreCase("tolak")){
							results[5] = Messages.get("ditolak.label");
						} else if (rs.getString("p.setuju_tolak").equalsIgnoreCase("0")){
							results[5] = Messages.get("menunggu_persetujuan.label");
						} else {
							results[5] = Messages.get("minta_persetujuan.label");
						}
					} else {
						results[5] = Messages.get("<font color='grey'>N/A</font>");
					}

//					results[4] += " " + String.format("<a onclick=showInfo(%s) name='infoProdukBtn'><span class='fa fa-info-circle fa-lg'/></a>", model.id);

					results[6] = String.format("%s", spanClassDibeli);
					results[7] = String.format("%s", spanClassTayang);
					List<ButtonGroup> buttonGroups = new ArrayList<>();
					buttonGroups.add(new ButtonGroup(Messages.get("detail.label"), model.getDetailUrl()));
					buttonGroups.add(new ButtonGroup(Messages.get("buka_tombol_ubah.label"), "#", model.id, true));
					results[8] = DataTableService.generateButtonGroup(buttonGroups);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_PRODUK_PENYEDIA=
			new DatatableResultsetHandler("p.id, m.nama_manufaktur, pen.nama_penyedia, p.no_produk_penyedia, p.apakah_dapat_dibeli, p.apakah_ditayangkan, p.status, p.komoditas_id, p.no_produk, p.setuju_tolak " +
					"p.jumlah_stok, p.jumlah_stok_inden, p.edit_able, pk.nama_kategori, p.nama_produk, k.kelas_harga, pn.status ") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					Config conf=Config.getInstance();
					Produk model = new Produk();
					model.id = rs.getLong("id");
					model.kelas_harga = rs.getString("kelas_harga");
					String gambar = rs.getString("p.produk_gambar_file_sub_location")+rs.getString("p.produk_gambar_file_name");
					results[0] = StringUtils.isEmpty(gambar)? "" :
							String.format("<img src='%s/produk_gambar/%s' onerror=\"this.src='/public/images/image-not-found.png';\">", conf.cdnServerImage100, gambar);
					results[1] =
							"<div>"+rs.getString("p.no_produk")+"</div>" +
									"<div>"+rs.getString("m.nama_manufaktur")+" "+rs.getString("p.nama_produk")+"</div>";
					results[2] = rs.getString("p.no_produk_penyedia");

					String spanClassDibeli = rs.getInt("p.apakah_dapat_dibeli") == 1 ? "<span style='color:green' class='fa fa-check fa-lg'></span>" : "<span style='color:red' class='fa fa-close fa-lg'></span>";
					String spanClassTayang = rs.getInt("p.apakah_ditayangkan") == 1 ? "<span style='color:green' class='fa fa-check fa-lg'></span>" : "<span style='color:red' class='fa fa-close fa-lg'></span>";

					JsonObject map = new JsonObject();
					Long idProds = rs.getLong("id");
					model.nama_produk = rs.getString("p.nama_produk");
					model.edit_able = rs.getDate("p.edit_able");
					map.addProperty("id", rs.getLong("id"));

					HashMap<String, Object> infoProds = new HashMap<>();
					infoProds.put("id", rs.getLong("id"));

					HashMap<String, Object> editProds = new HashMap<>();
					editProds.put("produk_id", rs.getLong("id"));

					if(model.edit_able != null){
						Logger.info(model.nama_produk + " bisa diedit");
					}

					String edit_url = Router.reverse("katalog.ProdukCtr.edit", editProds).url;

					if (rs.getString("p.setuju_tolak") != null){
						if (rs.getString("p.setuju_tolak").equalsIgnoreCase("setuju")){
							results[3] = Messages.get("disetujui.label");
						} else if (rs.getString("p.setuju_tolak").equalsIgnoreCase("tolak")){
							results[3] = Messages.get("ditolak.label");
						} else if (rs.getString("p.setuju_tolak").equalsIgnoreCase("0")){
							results[3] = Messages.get("menunggu_persetujuan");
						} else {
							results[3] = Messages.get("minta_persetujuan");
						}
					} else {
						results[3] = Messages.get("<font color='grey'>N/A</font>");
					}

//					results[3] += " " + String.format("<a onclick=showInfo(%s) name='infoProdukBtn'><span class='fa fa-info-circle fa-lg'/></a>", model.id);

					results[4] = String.format("%s", spanClassDibeli);
					results[5] = String.format("%s", spanClassTayang);
					List<ButtonGroup> buttonGroups = new ArrayList<>();
					buttonGroups.add(new ButtonGroup(Messages.get("detail.label"), model.getDetailUrl()));
					if(model.isEditAble()){
						buttonGroups.add(new ButtonGroup(Messages.get("ubah.label"), edit_url));
					}
					results[6] = DataTableService.generateButtonGroup(buttonGroups);
					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_PRODUKLAPORAN =
			new DatatableResultsetHandler("r.id, r.item_id, p.id as produk_id, p.komoditas_id, p.no_produk, p.nama_produk, k.kelas_harga, py.nama_penyedia, u.role_basic_id, r.status, r.reporter_id, " +
					"r.reporter_name,r.reporter_telp, r.reporter_email, r.reporter_nik, r.message, r.created_date") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];

					ReportRiwayat reportRiwayat = new ReportRiwayat();
					reportRiwayat.id = rs.getLong("r.id");

					HashMap<String, Object> map = new HashMap<>();
					map.put("id", rs.getLong("produk_id"));
					String statusLaporan = rs.getString("r.status");
					String url = Router.reverse("katalog.produkctr.detail", map).url;

					results[0] = "<div>Dilaporkan:<br><b>"+rs.getString("r.created_date")+"</b></div>";
					results[1] = "<div>"+String.format("<a href='%s?type=%s'>%s</a>", url, ProductPriceElastic.getElasticPriceType(rs.getString("k.kelas_harga")),
							"<b>"+rs.getString("p.no_produk")+"</b><br>"+rs.getString("p.nama_produk"))+
							"<br><br>Penyedia: "+rs.getString("py.nama_penyedia")+"</div";
					results[2] = "<div><b>"+rs.getString("r.message")+"</b><br><br>"
							+rs.getString("r.reporter_name")+"<br>"
							+rs.getString("r.reporter_telp")+"<br>"
							+rs.getString("r.reporter_email")+"<br>"
							+rs.getString("r.reporter_nik")+"</div>";
					results[3] = statusLaporan;
					List<ButtonGroup> buttonGroups = new ArrayList<>();
					buttonGroups.add(new ButtonGroup(Messages.get("detail.label"), reportRiwayat.getDetailUrl()));
					if(statusLaporan.matches("proses")){
						buttonGroups.add(new ButtonGroup(Messages.get("tutup_laporan.label"), "#", reportRiwayat.id, true));
					}
					results[4] = DataTableService.generateButtonGroup(buttonGroups);
					return results;
				}
			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_PRODUKPENAWARAN=
			new DatatableResultsetHandler("p.id,  p.nama_produk, p.jenis_produk, p.jumlah_stok, p.jumlah_stok_inden, p.status, " +
					"p.no_produk, p.no_produk_penyedia, p.apakah_dapat_dibeli, p.apakah_ditayangkan, pk.nama_kategori, m.nama_manufaktur, p.komoditas_id, p.penawaran_id") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					String url=Router.reverse("katalog.ProdukCtr.edit").url ;

					Komoditas komoditas = Komoditas.findById(rs.getLong("p.komoditas_id"));

					results[0] = rs.getString("p.id");
					results[1] =
							"<div>"+rs.getString("p.no_produk")+"</div>" +
									"<div>"+rs.getString("m.nama_manufaktur")+" "+rs.getString("p.nama_produk")+"</div>";
					results[2] = rs.getString("p.no_produk_penyedia");
					results[3] = komoditas.jenis_produk_override?rs.getString("p.jenis_produk").toUpperCase():komoditas.jenis_produk.toUpperCase();
					results[4] = komoditas.apakah_stok_unlimited ? "Unlimited" : FormatUtils.formatDesimal2(rs.getBigDecimal("p.jumlah_stok"));
					results[5] = rs.getString("p.jumlah_stok_inden");

					if (null == rs.getString("p.status")){
						results[6] = "";
					}else{
						results[6] = String.format("%s<br><button type='button' class='fa fa-info-circle fa-lg' id='info_produk' value='%s'></button>", KatalogUtils.getStatusLabel(rs.getString("p.status")), rs.getString("p.id"));
					}

					AktifUser aktifUser = BaseController.getAktifUser();
					Penawaran penawaran = Penawaran.findById(rs.getLong("p.penawaran_id"));

					String aksi = "";

					boolean isPenyedia = aktifUser.user_id.equals(penawaran.user_id);
					Usulan usulan = Usulan.findById(penawaran.usulan_id);
					boolean editable = false;
					System.out.println(results[6]);

					if (rs.getString("p.status").isEmpty()
							|| rs.getString("p.status").equalsIgnoreCase("DITOLAK")
							|| rs.getString("p.status").equalsIgnoreCase("MENUNGGU PERSETUJUAN")
							|| rs.getString("p.status").equalsIgnoreCase("DRAFT")
					){
						editable = true;
					}
					if( isPenyedia && penawaran.allowToAddProduct() && editable && UsulanService.isBeforeOrSameNegotiationDate(usulan)){
						aksi = String.format("<a href='%s?produk_id=%s' class='btn btn-primary btn-xs'>%s</a>", url, rs.getLong("p.id"), Messages.get("ubah.label"));
					}

					results[7] = aksi;
					return results;
				}

			};


	public final static DatatableResultsetHandler<String[]> DAFTAR_GRUP_ROLE=
			new DatatableResultsetHandler("id,nama_grup,total_user,aksi") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					HashMap<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
					String urlEdit = Router.reverse("controllers.masterdata.GrupRoleCtr.edit", map).url;
					String urlDelete = Router.reverse("controllers.masterdata.GrupRoleCtr.delete", map).url;
					results[1] = String.format("<a href='%s'>%s</a>", urlEdit, rs.getString("nama_grup"));
					results[2] = ext.FormatUtils.formatDesimal(rs.getInt("total_user"));
					results[3] = String.format("<a href='%s' onclick=\"return confirm('%s');\" class='btn btn-danger delete btn-xs' totalChild='"+rs.getInt("total_user")+"'>%s</a>", urlDelete, Messages.get("notif.delete_button.confirm"), Messages.get("hapus.label"));
					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_BLACKLIST=
			new DatatableResultsetHandler("bl.id ,bl.nama_penyedia, bl.sk, bl.satker_nama, bl.provinsi_nama, bl.kabupaten_nama, aksi") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					HashMap<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
					String urlDetail = Router.reverse("controllers.masterdata.BlacklistCtr.detail", map).url;
					//results[1] = String.format("<a href='%s'>%s</a>", urlDetail, rs.getString("nama_penyedia"));

//					results[2] = ext.FormatUtils.formatDesimal(rs.getInt("total_user"));
					results[1] = rs.getString("nama_penyedia");
					results[2] = rs.getString("sk");
					results[3] = rs.getString("satker_nama");
					results[4] = rs.getString("provinsi_nama");
					results[5] = rs.getString("kabupaten_nama");
					results[6] = rs.getString("nama_penyedia");
					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_MANUFAKTUR=
			new DatatableResultsetHandler("km.id, km.manufaktur_id, km.active, km.komoditas_id, k.nama_komoditas, m.nama_manufaktur, m.created_date, m.modified_date, created_by, modified_by") {

				public String[] handle(ResultSet rs) throws SQLException {
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					String[] results=new String[columns.length];
					results[0] = rs.getString("km.id");
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
					boolean active = rs.getInt("km.active") == 1;
					String classActive = active ? "class='btn btn-success btn-xs disabled'":"class='btn btn-danger btn-xs disabled'";
					String urlDelete = Router.reverse("masterdata.ManufakturCtr.delete", map).url;
					String urlAktif =Router.reverse("masterdata.ManufakturCtr.setAktif", map).url;
					String urlNonAktif =Router.reverse("masterdata.ManufakturCtr.setNonAktif", map).url;
					results[1] = String.format("<a href='#' class='nama_manufaktur' kmid='%s' mid='%s' kid='%s'>%s</a>",rs.getString("km.id"),rs.getString("km.manufaktur_id"),rs.getString("km.komoditas_id"),rs.getString("m.nama_manufaktur"));
					results[2] = rs.getString("k.nama_komoditas");
					results[3] = rs.getDate("m.created_date")==null?"" : df.format(rs.getDate("m.created_date"));
					results[4] = rs.getString("created_by");
					results[5] = rs.getDate("m.modified_date")==null?"" : df.format(rs.getDate("m.modified_date"));
					results[6] = rs.getString("modified_by");
					results[7] = String.format("<a href='#' %s style='width:80px'>%s</a>",classActive, active ? Messages.get("aktif.label"):Messages.get("tidak_aktif.label"));
					results[8] = String.format("<a href='%s' class='btn "+ (active?"btn-danger":"btn-success") +" btn-xs' style='width:80px'>%s</a>",active? urlNonAktif:urlAktif, (active?"Non Aktifkan":"Aktifkan"));
//					results[9] = String.format("<a href='%s' class='btn btn-danger delete btn-xs'>%s</a>", urlDelete, Messages.get("hapus.label"));
					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_BERITA=
			new DatatableResultsetHandler("kon.id,kk.nama_kategori,kon.judul_konten,kon.isi_konten,kon.status,kon.created_date,kon.active,kon.publish_date_from,kon.publish_date_to,edit,delete") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					String urlDetail =Router.reverse("controllers.cms.BeritaCtr.detail").url;
					String urlEdit =Router.reverse("controllers.cms.BeritaCtr.edit").url;
					String urlDelete =Router.reverse("controllers.cms.BeritaCtr.delete").url;

					String publish_to = rs.getString("kon.publish_date_to") != null ? rs.getString("kon.publish_date_to") : "<span style='color:#999'>n/a</span>";
					boolean status = rs.getString("kon.status").matches("published") ? true : false;
					String classStatus = status ? "class='btn btn-success btn-xs disabled'":"class='btn btn-warning btn-xs disabled'";
					boolean active = rs.getInt("kon.active")==1;
					String classButton = active ? "class='btn btn-success btn-xs disabled'":"class='btn btn-danger btn-xs disabled'";
					String classString = active ? "" : "class='disabled'";
					String classOnClick = active ? "" : "onclick='return false'";

					results[0] = rs.getString("kon.id");
					results[1] = rs.getString("kk.nama_kategori");
					results[2] = rs.getString("kon.judul_konten");
					results[3] = String.format("<a href='%s?id=%s' class='btn btn-default btn-xs' style='width:100px'>%s</a>",urlDetail,rs.getLong("kon.id"),Messages.get("lihat.label"));
					results[4] = String.format("<a href='#' %s style='width:70px'>%s</a>", classStatus, rs.getString("kon.status"));
					results[5] = String.format("<a href='#' %s style='width:50px'>%s</a>", classButton, rs.getInt("kon.active")==1?Messages.get("ya.label"):Messages.get("tidak.label"));
					results[6] = String.format("%s <span style='color:red'>%s</span> %s", rs.getString("kon.publish_date_from"), Messages.get("sampai_dengan.label"), publish_to);

					String btn_dropdown_open = "<div class='dropdown'><button class='btn btn-default dropdown-toggle' "
							+ "type='button' id='dropdown-option' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>" +
							Messages.get("aksi.label") + " <i class='fa fa-caret-down'></i></button>"
							+ "<ul class='dropdown-menu' aria-labelledby='dropdown-option'>";
					String editButton = "<li %s><a href='%s?id=%s' %s>%s</a></li>";
					String deleteButton = "<li %s><a href='%s?id=%s' %s>%s</a></li>";
					String btn_dropdown_close = "</ul></div>";

					results[7] = String.format(btn_dropdown_open + editButton + deleteButton + btn_dropdown_close,
							classString, urlEdit, rs.getLong("kon.id"), classOnClick, Messages.get("ubah.label"),
							classString, urlDelete, rs.getLong("kon.id"), classOnClick, Messages.get("hapus.label"));
					return results;
				}
			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_UNDUH=
			new DatatableResultsetHandler("pf.id,kk.nama_kategori,pf.original_file_name,pf.file_size, " +
					"pf.deskripsi, pf.status, usr.nama_lengkap, pf.created_date,pf.publish_date_from, pf.publish_date_to," +
					"pf.file_sub_location, pf.active, url, edit,delete") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					String urlEdit =Router.reverse("controllers.cms.UnduhCtr.edit").url;
					String urlDelete =Router.reverse("controllers.cms.UnduhCtr.delete").url;

					String publish_to = rs.getString("pf.publish_date_to") != null ? rs.getString("pf.publish_date_to") : "<span style='color:#999'>n/a</span>";
					Double convertSize = rs.getString("pf.file_size") != null ? Double.parseDouble(rs.getString("pf.file_size")) : 0;
					DecimalFormat df = new DecimalFormat("##.##");
					boolean status = rs.getString("pf.status").matches("published") ? true : false;
					String classStatus = status ? "class='btn btn-success btn-xs disabled'":"class='btn btn-warning btn-xs disabled'";
					boolean active = rs.getInt("pf.active")==1;
					String classButton = active ? "class='btn btn-success btn-xs disabled'":"class='btn btn-danger btn-xs disabled'";
					String classString = active ? "" : "class='disabled'";
					String classOnClick = active ? "" : "onclick='return false'";

					results[0] = rs.getString("pf.id");
					results[1] = rs.getString("kk.nama_kategori");
					results[2] = rs.getString("pf.deskripsi");
					results[3] = rs.getString("pf.original_file_name");
					results[4] = df.format(convertSize/1000)+" KB";
					results[5] = String.format("<a href='#' %s style='width:70px'>%s</a>", classStatus, rs.getString("pf.status"));
					results[6] = String.format("<a href='#' %s style='width:50px'>%s</a>", classButton, rs.getInt("pf.active")==1?Messages.get("ya.label"):Messages.get("tidak.label"));
					results[7] = String.format("%s <span style='color:red'>%s</span> %s", rs.getString("pf.publish_date_from"), Messages.get("sampai_dengan.label"), publish_to);
//					results[8] = Router.getBaseUrl()+"/unduh/download-file/"+rs.getString("pf.id");

					String btn_dropdown_open = "<div class='dropdown'><button class='btn btn-default dropdown-toggle' "
							+ "type='button' id='dropdown-option' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>" +
							Messages.get("aksi.label") + " <i class='fa fa-caret-down'></i></button>"
							+ "<ul class='dropdown-menu' aria-labelledby='dropdown-option'>";
					String editButton = "<li %s><a href='%s?id=%s' %s>%s</a></li>";
					String deleteButton = "<li %s><a href='%s?id=%s' %s>%s</a></li>";
					String btn_dropdown_close = "</ul></div>";

					results[8] = String.format(btn_dropdown_open + editButton + deleteButton + btn_dropdown_close,
							classString, urlEdit, rs.getLong("pf.id"), classOnClick, Messages.get("ubah.label"),
							classString, urlDelete, rs.getLong("pf.id"), classOnClick, Messages.get("hapus.label"));

					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_KATEGORI_KONTEN=
			new DatatableResultsetHandler("kk.id,kku.nama_kategori_utama,kk.nama_kategori,kk.deskripsi,kk.active,kk.created_date,edit,delete") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					String urlEdit = Router.reverse("controllers.cms.KategoriKontenCtr.edit").url;
					String urlDelete =Router.reverse("controllers.cms.KategoriKontenCtr.delete").url;
					String urlActive = Router.reverse("cms.KategoriKontenCtr.setAktivasi").url;
					boolean active = rs.getInt("kk.active") == 1;
					String classDisabled = (active) ? "" : "class='disabled'";
					String classOnClick = (active) ? "" : "onclick='return false'";
					String classActive = (active) ? "class='btn btn-success btn-xs'" : "class='btn btn-danger btn-xs'";

					results[0] = rs.getString("kk.id");
					results[1] = rs.getString("kku.nama_kategori_utama");
					results[2] = rs.getString("kk.nama_kategori");
					results[3] = rs.getString("kk.deskripsi");
					results[4] = String.format("<a href='%s?id=%s&param=%s' %s style='width:60px'>%s</a>", urlActive, rs.getString("kk.id"), active?"non_aktif":"aktif", classActive, active? Messages.get("ya.label"):Messages.get("tidak.label"));
					results[5] = rs.getString("kk.created_date");

					String btn_dropdown_open = "<div class='dropdown'><button class='btn btn-default dropdown-toggle' "
							+ "type='button' id='dropdown-option' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>" +
							Messages.get("aksi.label") + " <i class='fa fa-caret-down'></i></button>"
							+ "<ul class='dropdown-menu' aria-labelledby='dropdown-option'>";
					String editButton = "<li %s><a href='%s?id=%s' %s>%s</a></li>";
//					String deleteButton = "<li %s><a href='%s?id=%s' %s>%s</a></li>";
					String btn_dropdown_close = "</ul></div>";

					results[6] = String.format(btn_dropdown_open +
									editButton +
//									deleteButton +
									btn_dropdown_close
									,classDisabled, urlEdit, rs.getLong("kk.id"), classOnClick, Messages.get("ubah.label")
//									,classDisabled, urlDelete, rs.getLong("kk.id"), classOnClick, Messages.get("hapus.label")
					);

					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_KATEGORI_UTAMA=
			new DatatableResultsetHandler("kku.id, kku.id as id_kategori, kku.nama_kategori_utama, kku.deskripsi, kku.active") {

				public String[] handle(ResultSet rs) throws SQLException {
					String urlActive = Router.reverse("cms.KategoriKontenCtr.setAktivasiKategori").url;
					boolean active = rs.getInt("active") == 1;
					String classActive = active ? "class='btn btn-success btn-xs'":"class='btn btn-danger btn-xs'";
					String[] results = new String[columns.length];
					results[0] = rs.getString("id");
					results[1] = rs.getString("id_kategori");
					results[2] = rs.getString("nama_kategori_utama");
					results[3] = rs.getString("deskripsi");
					results[4] = String.format("<a href='%s?id=%s&param=%s' %s style='width:60px'>%s</a>", urlActive, rs.getString("id"), active?"non_aktif":"aktif", classActive, active? Messages.get("ya.label"):Messages.get("tidak.label"));
					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_FAQ=
			new DatatableResultsetHandler("fq.id,kk.nama_kategori,fq.judul_konten,fq.isi_konten,fq.status,fq.created_date,fq.publish_date_from,fq.publish_date_to,fq.active,edit,delete") {

				public String[] handle(ResultSet rs) throws SQLException{
					String[] results = new String[columns.length];
					String urlDetail = Router.reverse("controllers.cms.FaqCtr.detail").url;
					String urlEdit = Router.reverse("controllers.cms.FaqCtr.edit").url;
					String urlDelete = Router.reverse("controllers.cms.FaqCtr.delete").url;

					String publish_to = rs.getString("fq.publish_date_to") != null ? rs.getString("fq.publish_date_to") : "<span style='color:#999'>n/a</span>";
					boolean status = rs.getString("fq.status").matches("published") ? true : false;
					String classStatus = status ? "class='btn btn-success btn-xs disabled'":"class='btn btn-warning btn-xs disabled'";
					boolean active = rs.getInt("fq.active")==1;
					String classButton = active ? "class='btn btn-success btn-xs disabled'":"class='btn btn-danger btn-xs disabled'";
					String classString = active ? "" : "class='disabled'";
					String classOnClick = active ? "" : "onclick='return false'";

					results[0] = rs.getString("fq.id");
					results[1] = rs.getString("kk.nama_kategori");
					results[2] = rs.getString("fq.judul_konten");
					results[3] = String.format("<a href='%s?id=%s' class='btn btn-default btn-xs' style='width:100px'>%s</a>",urlDetail,rs.getLong("fq.id"),Messages.get("lihat.label"));
					results[4] = String.format("<a href='#' %s style='width:70px'>%s</a>", classStatus, rs.getString("fq.status"));
					results[5] = String.format("<a href='#' %s style='width:50px'>%s</a>", classButton, rs.getInt("fq.active")==1? Messages.get("ya.label"):Messages.get("tidak.label"));
					results[6] = String.format("%s <span style='color:red'>%s</span> %s", rs.getString("fq.publish_date_from"), Messages.get("sampai_dengan.label"), publish_to);

					String btn_dropdown_open = "<div class='dropdown'><button class='btn btn-default dropdown-toggle' "
							+ "type='button' id='dropdown-option' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>" +
							Messages.get("aksi.label") + " <i class='fa fa-caret-down'></i></button>"
							+ "<ul class='dropdown-menu' aria-labelledby='dropdown-option'>";
					String editButton = "<li %s><a href='%s?id=%s' %s>%s</a></li>";
					String deleteButton = "<li %s><a href='%s?id=%s' %s>%s</a></li>";
					String btn_dropdown_close = "</ul></div>";

					results[7] = String.format(btn_dropdown_open + editButton + deleteButton + btn_dropdown_close,
							classString, urlEdit, rs.getLong("fq.id"), classOnClick, Messages.get("ubah.label"),
							classString, urlDelete, rs.getLong("fq.id"), classOnClick, Messages.get("hapus.label"));

					return results;
				}
			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_POLLING =
			new DatatableResultsetHandler("p.id,p.subjek,p.deskripsi_singkat,p.status,p.terkunci,p.apakah_utama,p.created_date,p.publish_date_from,p.publish_date_to,p.active,edit,delete") {
				public String[] handle(ResultSet rs) throws SQLException{
					String[] results = new String[columns. length];
					String urlUpdate = Router.reverse("controllers.cms.PollingCtr.update").url;
					String urlEdit = Router.reverse("controllers.cms.PollingCtr.edit").url;
					String urlDelete = Router.reverse("controllers.cms.PollingCtr.delete").url;

					String publish_to = rs.getString("p.publish_date_to") != null ? rs.getString("p.publish_date_to") : "<span style='color:#999'>n/a</span>";
					boolean status = rs.getString("p.status").matches("published") ? true : false;
					boolean terkunci = rs.getInt("p.terkunci") == 0;
					boolean active = rs.getInt("p.active") == 1;
					String classStatus = status ? "class='btn btn-success btn-xs disabled'":"class='btn btn-warning btn-xs disabled'";
					String classButton = active ? "class='btn btn-success btn-xs disabled'":"class='btn btn-danger btn-xs disabled'";
					String classWarning = (terkunci && active) ? "class='btn btn-warning btn-xs'" : "class='btn btn-default btn-xs disabled'";
					String classUtama = active ? "class='btn btn-warning btn-xs'":"class='btn btn-default btn-xs disabled'";
					String classString = (terkunci && active)  ? "" : "class='disabled'";
					String classOnClick = (terkunci && active) ? "" : "onclick='return false'";

					String btn_dropdown_open = "<div class='dropdown'><button class='btn btn-default dropdown-toggle' "
							+ "type='button' id='dropdown-option' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>" +
							Messages.get("aksi.label") + " <i class='fa fa-caret-down'></i></button>"
							+ "<ul class='dropdown-menu' aria-labelledby='dropdown-option'>";
					String editButton = "<li %s><a href='%s?id=%s' %s>%s</a></li>";
					String deleteButton = "<li %s><a href='%s?id=%s' %s>%s</a></li>";
					String btn_dropdown_close = "</ul></div>";

					results[0] = rs.getString("p.id");
					results[1] = String.format("<button type='button' onclick='viewPolling(%s)' class='btn btn-link'>%s</button>", rs.getLong("p.id"),rs.getString("p.subjek"));
					results[2] = rs.getString("p.deskripsi_singkat");
					results[3] = String.format("<a href='#' %s style='width:70px'>%s</a>", classStatus, rs.getString("p.status"));
					results[4] = String.format("<a href='#' %s style='width:50px'>%s</a>",classButton,rs.getInt("p.active")==1? Messages.get("ya.label"):Messages.get("tidak.label"));
					results[5] = String.format("%s <span style='color:red'>%s</span> %s", rs.getString("p.publish_date_from"), Messages.get("sampai_dengan.label"), publish_to);
					results[6] = String.format("<a href='%s?id=%s&param=%s' %s style='width:60px'><i class='fa %s'></i> %s</a>", urlUpdate, rs.getLong("p.id"), "terkunci", classWarning, rs.getInt("p.terkunci")==1?"fa-lock":"fa-unlock", rs.getInt("p.terkunci")==1? Messages.get("ya.label"):Messages.get("tidak.label"));
					results[7] = String.format("<a href='%s?id=%s&param=%s' %s style='width:60px'><i class='fa %s'></i> %s</a>", urlUpdate, rs.getLong("p.id"), "apakah_utama", classUtama, rs.getInt("p.apakah_utama")==1?"fa-star":"fa-star-o", rs.getInt("p.apakah_utama")==1? Messages.get("ya.label"):Messages.get("tidak.label"));
					results[8] = String.format(btn_dropdown_open + editButton + deleteButton + btn_dropdown_close,
							classString, urlEdit, rs.getLong("p.id"), classOnClick, Messages.get("ubah.label"),
							active?"":"class='disabled'", urlDelete, rs.getLong("p.id"),active?"":"onclick='return false'", Messages.get("hapus.label"));

					return results;
				}
			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_KONTEN_STATIS=
			new DatatableResultsetHandler("ks.id,ks.judul_konten,kk.nama_kategori,ks.isi_konten,ks.status,ks.active,ks.created_date,ks.publish_date_from,ks.publish_date_to,edit,delete"){
				public String[] handle(ResultSet rs) throws SQLException{
					String[] results = new String[columns.length];
					String urlDetail = Router.reverse("controllers.cms.KontenStatisCtr.detail").url;
					String urlEdit = Router.reverse("controllers.cms.KontenStatisCtr.edit").url;
					String urlDelete = Router.reverse("controllers.cms.KontenStatisCtr.delete").url;

					String publish_to = rs.getString("ks.publish_date_to") != null ? rs.getString("ks.publish_date_to") : "<span style='color:#999'>n/a</span>";
					boolean status = rs.getString("ks.status").matches("published") ? true : false;
					String classStatus = status ? "class='btn btn-success btn-xs disabled'":"class='btn btn-warning btn-xs disabled'";
					boolean active = rs.getInt("ks.active")==1;
					String classButton = active ? "class='btn btn-success btn-xs disabled'":"class='btn btn-danger btn-xs disabled'";
					String classString = active ? "" : "class='disabled'";
					String classOnClick = active ? "" : "onclick='return false'";

					results[0] = rs.getString("ks.id");
					results[1] = rs.getString("kk.nama_kategori");
					results[2] = rs.getString("ks.judul_konten");
					results[3] = String.format("<a href='%s?id=%s' class='btn btn-default btn-xs' style='width:100px'>%s</a>",urlDetail,rs.getLong("ks.id"),Messages.get("lihat.label"));
					results[4] = String.format("<a href='#' %s style='width:70px'>%s</a>", classStatus, rs.getString("ks.status"));
					results[5] = String.format("<a href='#' %s style='width:50px'>%s</a>", classButton, rs.getInt("ks.active")==1? Messages.get("ya.label"):Messages.get("tidak.label"));
					results[6] = String.format("%s <span style='color:red'>%s</span> %s", rs.getString("ks.publish_date_from"), Messages.get("sampai_dengan.label"), publish_to);
					String btn_dropdown_open = "<div class='dropdown'><button class='btn btn-default dropdown-toggle' "
							+ "type='button' id='dropdown-option' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>" +
							Messages.get("aksi.label") + " <i class='fa fa-caret-down'></i></button>"
							+ "<ul class='dropdown-menu' aria-labelledby='dropdown-option'>";
					String editButton = "<li %s><a href='%s?id=%s' %s>%s</a></li>";
					String deleteButton = "<li %s><a href='%s?id=%s' %s>%s</a></li>";
					String btn_dropdown_close = "</ul></div>";

					results[7] = String.format(btn_dropdown_open + editButton + deleteButton + btn_dropdown_close,
							classString, urlEdit, rs.getLong("ks.id"), classOnClick, Messages.get("ubah.label"),
							classString, urlDelete, rs.getLong("ks.id"), classOnClick, Messages.get("hapus.label"));

					return results;
				}
			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_SUMBER_KURS=
			new DatatableResultsetHandler("id,sumber, created_date, created_by, modified_date, modified_by,status, ubah status, ksi") {

				public String[] handle(ResultSet rs) throws SQLException {
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					boolean active = rs.getInt("active") == 1;
					String classActive = active ? "class='btn btn-success btn-xs disabled'":"class='btn btn-danger btn-xs disabled'";
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
//					String urlDetail =Router.reverse("masterdata.SumberKursCtr.detail", map);
					String urlDelete =Router.reverse("masterdata.SumberKursCtr.delete", map).url;
					String urlAktif =Router.reverse("masterdata.SumberKursCtr.setAktif", map).url;
					String urlNonAktif =Router.reverse("masterdata.SumberKursCtr.setNonAktif", map).url;

					List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();
//					buttonGroups.add(new ButtonGroup(Messages.get("detail.label"), urlDetail));
					buttonGroups.add(new ButtonGroup(Messages.get("hapus.label"),urlDelete));

					results[1] = String.format("<a href='#' class='nama_sumber' sumber_id='%s'>%s</a>",rs.getString("id"),rs.getString("sumber"));
					results[2] = rs.getDate("created_date")==null?"" : df.format(rs.getDate("created_date"));
					results[3] = rs.getString("created_by");
					results[4] = rs.getDate("modified_date")==null?"" : df.format(rs.getDate("modified_date"));
					results[5] = rs.getString("modified_by");
					results[6] = String.format("<a href='#' %s style='width:80px'>%s</a>",classActive, active ? Messages.get("aktif.label"):Messages.get("tidak_aktif.label"));
					results[7] = String.format("<a href='%s' class='btn "+ (active?"btn-danger":"btn-success") +" btn-xs' style='width:80px'>%s</a>",active? urlNonAktif:urlAktif, (active?"Non Aktifkan":"Aktifkan"));
//					results[8] = DataTableService.generateButtonGroup(buttonGroups);
					results[8] = String.format("<a href='%s' class='btn btn-danger delete btn-xs'>%s</a>", urlDelete, Messages.get("hapus.label"));

					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_KURS=
			new DatatableResultsetHandler("k.id,k.nama_kurs,status, ubah status, ksi") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("k.id");
					boolean active = rs.getInt("active") == 1;
					String classActive = active ? "class='btn btn-success btn-xs disabled'":"class='btn btn-danger btn-xs disabled'";
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
					String urlDetail =Router.reverse("masterdata.KursCtr.detail", map).url;
//					String urlDelete =Router.reverse("masterdata.KursCtr.delete", map);
					String urlAktif =Router.reverse("masterdata.KursCtr.setAktif", map).url;
					String urlNonAktif =Router.reverse("masterdata.KursCtr.setNonAktif", map).url;

					List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();
					buttonGroups.add(new ButtonGroup(Messages.get("detail.label"), urlDetail));
//					buttonGroups.add(new ButtonGroup(Messages.get("hapus.label"),urlDelete));

					results[1] = String.format("<a href='#' class='nama_kurs' kurs_id='%s'>%s</a>",rs.getString("k.id"),rs.getString("k.nama_kurs"));
					results[2] = String.format("<a href='#' %s style='width:80px'>%s</a>",classActive, active ? Messages.get("aktif.label"):Messages.get("tidak_aktif.label"));
					results[3] = String.format("<a href='%s' class='btn "+ (active?"btn-danger":"btn-success") +" btn-xs' style='width:80px'>%s</a>",active? urlNonAktif:urlAktif, (active?"Non Aktifkan":"Aktifkan"));
//					results[4] = DataTableService.generateButtonGroup(buttonGroups);
					results[4] = String.format("<a href='%s' class='btn btn-primary btn-xs'>%s</a>", urlDetail, Messages.get("detail.label"));

					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_SUMBERDANA=
			new DatatableResultsetHandler("id,nama_sumber_dana,aksi") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
					String urlDelete =Router.reverse("masterdata.SumberDanaCtr.delete", map).url;
					results[1] = String.format("<a href='#' class='nama_sumber' sumber_id='%s'>%s</a>", rs.getLong("id"), rs.getString("nama_sumber_dana"));
					results[2] = String.format("<a href='%s' class='btn btn-danger delete btn-xs'>%s</a>", urlDelete, Messages.get("hapus.label"));
					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_MASTER_RUP=
			new DatatableResultsetHandler("id,id,nama,audit_update") {

				public String[] handle(ResultSet rs) throws SQLException {
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
					String urlDetail = Router.reverse("masterdata.RupCtr.detail", map).url;
					results[1] = rs.getString("id");
					results[2] = String.format("<a href='%s'>%s</a>", urlDetail, rs.getString("nama"));
					results[3] = rs.getString("audit_update");
					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_UNITPENGUKURAN=
			new DatatableResultsetHandler("id,nama_unit_pengukuran,deskripsi,created_date,created_by,modified_date,modified_by,active,aksi") {

				public String[] handle(ResultSet rs) throws SQLException {
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
					boolean active = rs.getInt("active") == 1;
					String classActive = active ? "class='btn btn-success btn-xs disabled'":"class='btn btn-danger btn-xs disabled'";


					String urlAktif = Router.reverse("masterdata.UnitPengukuranCtr.setAktifUnit", map).url;
					String urlNonAktif = Router.reverse("masterdata.UnitPengukuranCtr.setNonAktifUnit", map).url;

					results[1] = String.format("<a href='#' class='nama_unit_pengukuran' unit_des='%s' unit_id='%s'>%s</a>", rs.getString("deskripsi"), rs.getLong("id"), rs.getString("nama_unit_pengukuran"));
					results[2] = rs.getString("deskripsi");
					results[3] = rs.getDate("created_date")==null?"" : df.format(rs.getDate("created_date"));
					results[4] = rs.getString("created_by");
					results[5] = rs.getDate("modified_date")==null?"" : df.format(rs.getDate("modified_date"));
					results[6] = rs.getString("modified_by");
					results[7] = String.format("<a href='#' %s style='width:80px'>%s</a>",classActive, active ? Messages.get("aktif.label"):Messages.get("tidak_aktif.label"));
					results[8] = String.format("<a href='%s' class='btn "+ (active?"btn-danger":"btn-success") +" btn-xs' style='width:80px'>%s</a>",active? urlNonAktif:urlAktif, (active?"Non Aktifkan":"Aktifkan"));

					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_KURS_NILAI=
			new DatatableResultsetHandler("id,tanggal_kurs,nilai_jual,nilai_beli,created_at,created_by,updated_at,updated_by") {

				public String[] handle(ResultSet rs) throws SQLException {
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					String[] results=new String[columns.length];
					results[0] = rs.getString("id");

					results[1] = String.format("<a href='#' class='tanggal_kurs' nilai_beli='%s' nilai_jual='%s' nilai_id='%s'>%s</a>", rs.getString("nilai_beli"), rs.getString("nilai_jual"), rs.getLong("id"), rs.getString("tanggal_kurs"));
					results[2] = rs.getString("nilai_jual");
					results[3] = rs.getString("nilai_beli");
					results[4] = rs.getDate("created_date")==null?"" : df.format(rs.getDate("created_date"));
					results[5] = rs.getString("created_by");
					results[6] = rs.getDate("modified_date")==null?"" : df.format(rs.getDate("modified_date"));
					results[7] = rs.getString("modified_by");

					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_KATEGORI_KOMODITAS=
			new DatatableResultsetHandler("id,nama_kategori,total_komoditas,aksi") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
					String urlEdit =Router.reverse("admin.KategoriKomoditasCtr.edit", map).url;
//					String urlDelete =Router.reverse("admin.KategoriKomoditasCtr.delete", map);
					results[1] = String.format("<a href='%s'>%s</a>", urlEdit, rs.getString("nama_kategori"));
					results[2] = ext.FormatUtils.formatDesimal(rs.getInt("total_komoditas"));
//					results[3] = String.format("<a href='%s' class='btn btn-danger delete btn-xs' totalChild='"+rs.getInt("total_komoditas")+"'>%s</a>", urlDelete, Messages.get);
					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_KOMODITAS_PRODUK_ATRIBUT_TIPE=
			new DatatableResultsetHandler("id,komoditas_id,tipe_atribut,deskripsi,aksi") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
					map.put("komoditas_id", rs.getLong("komoditas_id"));
					String urlEdit = Router.reverse("admin.KomoditasProdukAtributTipeCtr.edit", map).url;
					String urlDelete = Router.reverse("admin.KomoditasProdukAtributTipeCtr.delete", map).url;
					String nama = "";
					try{
						nama = rs.getString("tipe_atribut");
						nama = nama.substring(0,50);
					}catch (Exception e){}
					results[1] = String.format("<a href='%s'>%s</a>", urlEdit, nama);
					results[2] = rs.getString("deskripsi");
					results[3] = String.format("<a href='%s' class='btn btn-danger delete btn-xs'>%s</a>", urlDelete, Messages.get("hapus.label"));
					return results;
				}

			};

	public final static String getTahapan() {
	    StringBuilder arrayTahapan = new StringBuilder();
        List<Tahapan> tahapans = Tahapan.getListTahapan();
        for (Tahapan tahapan : tahapans) {
           if (!tahapan.flagging.equals("-") && tahapan.flagging != null) {
               arrayTahapan.append(tahapan.flagging).append(",");
           }
        }
        if (arrayTahapan.toString().equals("")) {
            return arrayTahapan.toString();
        }
        return arrayTahapan.toString().substring(0, arrayTahapan.toString().length() - 1);
    }

	public final static DatatableResultsetHandler<String[]> DAFTAR_TAHAPAN=
			new DatatableResultsetHandler("id,nama_tahapan,urutan_tahapan,nama_tahapan_alias,jenis_tahapan,apakah_penawaran,apakah_negosiasi_penawaran,flagging,urutan_tahapan,aksi") {
				public String[] handle(ResultSet rs) throws SQLException {
                    String arrayFlagging = getTahapan();
					String[] results=new String[columns.length];
					String urlDelete =Router.reverse("controllers.masterdata.TahapanCtr.delete").url;

					results[0] = rs.getString("id");

					results[1] = String.format("<a href='#' class='nama_tahapan' tahapan_id='%s' urutan_tahapan='%s'>%s</a>", rs.getLong("id"), rs.getLong("urutan_tahapan"), rs.getString("nama_tahapan_alias"));

                    results[2] = rs.getString("urutan_tahapan");
                    results[3] = rs.getString("flagging");

					results[4] = String.format("<a href='%s?id=%s' class='btn delete btn-danger del-custom btn-xs' title='hapus'><i class='fa fa-trash'></i> <span class='hide'>%s</span></div></a>&nbsp;&nbsp;" +
									"<a class='btn btn-warning flaggingBtn btn-xs' tahapan_idflag='%s' flag_array='%s' nama_tahapan_alias='%s' flag='%s' title='flagging'><i class='fa fa-flag'></i> <span class='hide'>%s</span></a>",
                            urlDelete, rs.getLong("id"), Messages.get("hapus.label"), rs.getLong("id"), arrayFlagging , rs.getString("nama_tahapan_alias"), rs.getString("flagging") , Messages.get("flagging.label"));

					return results;
				}


			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_TEMPLATE_KONTRAK=
			new DatatableResultsetHandler("id,jenis_dokumen,edit") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					String urlEdit =Router.reverse("controllers.admin.KomoditasCtr.editTemplateKontrak").url;

					results[0] = rs.getString("id");
					results[1] = rs.getString("jenis_dokumen");
					results[2] = String.format("<a href='%s?id=%s' class='btn btn-primary btn-xs'>%s</a>", urlEdit, rs.getLong("id"), Messages.get("ubah.label"));

					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_UNSPSC=
			new DatatableResultsetHandler("commodity_id, commodity_title, segment_title, family_title, class_title") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length + 1];
					results[0] = rs.getString("commodity_id");
					results[1] = rs.getString("commodity_title");
					results[2] = rs.getString("segment_title");
					results[3] = rs.getString("family_title");
					results[4] = rs.getString("class_title");
					results[5] = String.format("<button type='button' class='btn btn-primary btn-xs addunspsc' unspscid='%s'>%s</button>", rs.getLong("commodity_id"), Messages.get("tambah.label"));
					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_PENAWARANSELURUH=
			new DatatableResultsetHandler("pn.id, u.nama_usulan, k.nama_komoditas, p.nama_lengkap, pn.created_date, pn.status, pn.user_id, up.pokja_id, u.doc_review_id,usulan_id") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					return PenawaranDataTableService.allPenawaranNonPenyedia(results,rs);
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_PENAWARANUSULAN=
			new DatatableResultsetHandler("pn.id, p.nama_lengkap, pn.status, pn.created_date, k.nama_komoditas, u.nama_usulan, pn.user_id, up.pokja_id") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					return PenawaranDataTableService.penawaranByUsulan(results,rs);
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_PENAWARANPENYEDIA=
			new DatatableResultsetHandler("pn.id, k.nama_komoditas, u.nama_usulan, pn.created_date, pn.status, pn.user_id, u.doc_review_id, usulan_id") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					return PenawaranDataTableService.penawaranByPenyedia(results,rs);
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_PENYEDIA=
			new DatatableResultsetHandler("p.id,p.user_id,p.nama_penyedia,u.nama_lengkap,u.user_name,p.website,total_kontrak,total_distributor,aksi") {

				public String[] handle(ResultSet rs) throws SQLException {
					Map<String, Object> params = new HashMap<String, Object>();
					final String id = rs.getString("p.id");
					params.put("id", id);

					Map<String, Object> distributorParams = new HashMap<String, Object>();
					distributorParams.put("pid",rs.getString("p.id"));
					distributorParams.put("type", "penyedia");

					String urlEdit =Router.reverse("controllers.masterdata.penyediaCtr.edit",params).url;
					String urlDetail =Router.reverse("controllers.masterdata.penyediaCtr.detail",params).url;
					String urlDistributor =Router.reverse("penyedia.distributorCtr.index",distributorParams).url;
					String urlContract =Router.reverse("controllers.penyedia.KontrakCtr.index",distributorParams).url;
					String urlPrint =Router.reverse("controllers.penyedia.KontrakCtr.cetakKontrak",distributorParams).url;

					List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();
					buttonGroups.add(new ButtonGroup(Messages.get("ubah.label"),urlEdit));
					buttonGroups.add(new ButtonGroup(Messages.get("detail.label"),urlDetail));
					buttonGroups.add(new ButtonGroup(Messages.get("distributor.label"),urlDistributor));
					buttonGroups.add(new ButtonGroup(Messages.get("kontrak.label"),urlContract));
					buttonGroups.add(new ButtonGroup(Messages.get("cetak_kontrak.label"),urlPrint));
					buttonGroups.add(new ButtonGroup(Messages.get("buka_tombol_ubah.label"), "#", Long.valueOf(id), true));

					String[] results=new String[columns.length];
					results[0] = rs.getString("p.id");
					results[1] = rs.getString("p.nama_penyedia");
					results[2] = rs.getString("u.nama_lengkap") +" ("+rs.getString("u.user_name")+")";
					results[3] = rs.getString("p.website");
					results[4] = rs.getString("total_kontrak");
					results[5] = rs.getString("total_distributor");
					results[6] = DataTableService.generateButtonGroup(buttonGroups);

					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_DISTRIBUTOR=
			new DatatableResultsetHandler("id,penyedia_id,nama_distributor,no_telp,email,website,minta_disetujui,setuju_tolak,aksi") {
				public String[] handle(ResultSet rs) throws SQLException {
					boolean minta_disetujui = rs.getInt("minta_disetujui")==1;
					String status = minta_disetujui?Messages.get("menunggu_persetujuan.label"):rs.getString("setuju_tolak");
					long idDistributor = rs.getLong("id");

					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
                    results[1] = String.format("<input type='checkbox' name='idDistributor' value='%s' class='idDistributors' />", idDistributor);
					results[2] = rs.getString("nama_distributor");
					results[3] = rs.getString("no_telp");
					results[4] = rs.getString("email");
					results[5] = rs.getString("website");
					results[6] = status;

					List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();

					Map<String, Object> params = new HashMap<String, Object>();
					params.put("id",rs.getLong("id"));

					Map<String, Object> editParams = new HashMap<String, Object>();
					editParams.put("id",rs.getLong("id"));
					editParams.put("pid",rs.getLong("penyedia_id"));

					String urlDetail = Router.reverse("penyedia.DistributorCtr.detail",editParams).url;
					buttonGroups.add(new ButtonGroup(Messages.get("detail.label"),urlDetail));

					String urlEdit = Router.reverse("penyedia.DistributorCtr.edit",editParams).url;
					buttonGroups.add(new ButtonGroup(Messages.get("ubah.label"),urlEdit));

					String urlDelete = Router.reverse("penyedia.DistributorCtr.delete", params).url;
					ButtonGroup delete = new ButtonGroup(Messages.get("hapus.label"),urlDelete);
					delete.isDelete = true;
					buttonGroups.add(delete);

					results[7] = DataTableService.generateButtonGroup(buttonGroups);

					return results;
				}
			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_PENYEDIA_DISTRIBUTOR=
			new DatatableResultsetHandler("id,penyedia_id,nama_distributor,no_telp,email,website,minta_disetujui,setuju_tolak,aksi") {
				public String[] handle(ResultSet rs) throws SQLException {
					boolean minta_disetujui = rs.getInt("minta_disetujui")==1;
					String status = minta_disetujui?Messages.get("menunggu_persetujuan.label"):rs.getString("setuju_tolak");

					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					results[1] = rs.getString("nama_distributor");
					results[2] = rs.getString("no_telp");
					results[3] = rs.getString("email");
					results[4] = rs.getString("website");
					results[5] = status;

					List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();

					Map<String, Object> params = new HashMap<String, Object>();
					params.put("id",rs.getLong("id"));

					Map<String, Object> editParams = new HashMap<String, Object>();
					editParams.put("id",rs.getLong("id"));
					editParams.put("pid",rs.getLong("penyedia_id"));

					String urlDetail = Router.reverse("penyedia.PenyediaDistributorCtr.detail",editParams).url;
					buttonGroups.add(new ButtonGroup(Messages.get("detail.label"),urlDetail));

					if(!AktifUser.getAktifUser().isProvider() && !AktifUser.getAktifUser().isDistributor()) {
						String urlEdit = Router.reverse("penyedia.PenyediaDistributorCtr.edit", editParams).url;
						buttonGroups.add(new ButtonGroup(Messages.get("ubah.label"), urlEdit));

						String urlDelete = Router.reverse("penyedia.PenyediaDistributorCtr.delete", params).url;
						ButtonGroup delete = new ButtonGroup(Messages.get("hapus.label"), urlDelete);
						delete.isDelete = true;
						buttonGroups.add(delete);
					}

					results[6] = DataTableService.generateButtonGroup(buttonGroups);

					return results;
				}
			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_DOKUMEN_UPLOAD=
			new DatatableResultsetHandler("du.id, dk.nama_kategori, du.nama_dokumen, du.file_path, du.active, du.created_date,unduh") {

				public String[] handle(ResultSet rs) throws SQLException{
					String[] results = new String[columns.length];
					boolean active = rs.getInt("du.active") == 1;
					String classActive = active ? "class='btn btn-success btn-xs disabled'":"class='btn btn-danger btn-xs disabled'";
					String classUnduh = "class='btn btn-primary btn-xs'";
					File filepath = new File(rs.getString("du.file_path")+rs.getString("du.nama_dokumen"));

					results[0] = rs.getString("du.id");
					results[1] = rs.getString("du.nama_dokumen");
					results[2] = String.format("<a href='#' %s>%s</a>", classActive, active ? Messages.get("aktif.label"):Messages.get("tidak_aktif.label"));
					results[3] = rs.getString("du.created_date");
					results[4] = String.format("<a href='%s' %s download>%s</a>", filepath, classUnduh, Messages.get("unduh.label"));

					return results;
				}
			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_DOKUMEN_TEMPLATE=
			new DatatableResultsetHandler("dt.id, kk.id, dt.jenis_dokumen, dt.template, dt.active") {

				public String[] handle(ResultSet rs) throws SQLException {
					String urlEditTemplate = Router.reverse("controllers.masterdata.DokumenTemplateCtr.edit").url;
					boolean active = rs.getInt("dt.active")==1;
					String classButton = active ? "class='label label-success'":"class='label label-danger'";

					String[] results = new String[columns.length];
					results[0] = rs.getString("id");
					results[1] = rs.getString("dt.jenis_dokumen");
					results[2] = String.format("<a href='#' class='column-center disabled'><span %s>%s</span></a>", classButton, rs.getInt("active")==1? Messages.get("aktif.label"):Messages.get("tidak_aktif.label"));
					results[3] = String.format("<a href='%s?template_id=%s&konten_id=%s' class='btn btn-primary btn-xs' style='width:50px'>%s</a>", urlEditTemplate, rs.getLong("id"), rs.getLong("konten_kategori_id"), Messages.get("ubah.label"));
					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_PERSETUJUAN_PRODUK=
			new DatatableResultsetHandler("pm.id, p.nama_produk, k.nama_komoditas, user.nama_lengkap, pm.produk_id, p.no_produk, k.id, user.id") {

				public String[] handle(ResultSet rs) throws SQLException {
					long idProduk = rs.getLong("pm.produk_id");
					long idPermintaan = rs.getLong("pm.id");
					Map<String, Object> ids = new HashMap<>();
					ids.put("id", idProduk);
					String urlPerbandinganHarga = Router.reverse("katalog.ProdukCtr.harga", ids).url;
					String urlPerbandinganSpek = Router.reverse("katalog.ProdukCtr.spesifikasi", ids).url;
					String urlProdukDetail = UrlUtil.builder().setUrl("katalog.ProdukCtr.detail").setParam("id", idProduk).build();
					ids = new HashMap<>();
					ids.put("id", idPermintaan);
					String[] results=new String[columns.length];
					results[0] = String.format("<input type='checkbox' name='idProduk' value='%s' class='idProduks' />", idProduk);
					results[1] = String.format("<p>%s</p><h5>%s</h5>", rs.getString("p.no_produk"), rs.getString("p.nama_produk"));
					results[2] = rs.getString("k.nama_komoditas");
					results[3] = rs.getString("user.nama_lengkap");

					List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();
					buttonGroups.add(new ButtonGroup(Messages.get("perubahan_spesifikasi.label"), urlPerbandinganSpek));
					buttonGroups.add(new ButtonGroup(Messages.get("perubahan_harga.label"), urlPerbandinganHarga));
					buttonGroups.add(new ButtonGroup(Messages.get("detail.label"), urlProdukDetail));

					results[4] = DataTableService.generateButtonGroup(buttonGroups);

					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_PARAMETER=
			new DatatableResultsetHandler("id,nama_parameter, isi_parameter,deskripsi, aksi") {

				public String[] handle(ResultSet rs) throws SQLException {
					String urlEdit =Router.reverse("controllers.masterdata.ParameterAplikasiCtr.edit").url;
					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					results[1] = rs.getString("nama_parameter");
					results[2] = rs.getString("isi_parameter");
					results[3] = rs.getString("deskripsi");
					results[4] = String.format("<a href='%s?id=%s' class='btn btn-primary btn-xs'>%s</a>", urlEdit, rs.getLong("id"), Messages.get("ubah.label"));
					return results;
				}
			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_PENYEDIA_PENAWARAN =
			new DatatableResultsetHandler("id, penyedia_id, komoditas_id, judul_pengumuman") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results = new String[columns.length];
					results[0] = rs.getString("id");
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
					map.put("pid", rs.getString("penyedia_id"));

					String urlKontrak = Router.reverse("penyedia.KontrakCtr.listKontrakPenawaran", map).url;

					results[1] = String.format("<a href='%s'>%s</a>",urlKontrak,rs.getString("judul_pengumuman"));
					return results;
				}
			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_PENYEDIA_KONTRAK=
			new DatatableResultsetHandler("id,no_kontrak,dok_id_attachment,file_name,original_file_name,tgl_masa_berlaku_mulai,tgl_masa_berlaku_selesai,file_sub_location,apakah_berlaku") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);

					String urlEdit = Router.reverse("penyedia.KontrakCtr.edit", map).url;
					String urlSetActive = Router.reverse("penyedia.KontrakCtr.setKontrakActive", map).url;
					String urlSetNonActive = Router.reverse("penyedia.KontrakCtr.setKontrakNonActive", map).url;

					results[1] = rs.getString("no_kontrak");

					String url;
					String file_name = rs.getString("file_name");
					String original_file_name = rs.getString("original_file_name");
					Long dokAttachment = rs.getLong("dok_id_attachment");
					Logger.debug(rs.getString("file_sub_location"));
					//BlobTable blobTable = BlobTable.find("blb_id_content = ?",dokAttachment).first();
					BlobTable blobTable = BlobTable.findByBlobId(dokAttachment);
					if(blobTable == null){
						// /data/files/upload/penyedia_kontrak/2016/08/16/14713178744276.pdf
						url = "/public/files/upload/penyedia_kontrak/"+rs.getString("file_sub_location") + file_name;

					}else{
						DokumenInfo dokumenInfo = DokumenInfo.findDokumenKontrak(blobTable);
						url = dokumenInfo.download_url;
					}

					Logger.debug(url);


					results[2] = String.format("<a target='_blank' href='%s'>%s</a>",url,original_file_name);

					String tanggalMulai = "-";
					String pattern = "dd MMMMM yyyy";
					SimpleDateFormat simpleDateFormat =
							new SimpleDateFormat(pattern, new Locale("id", "ID"));
					try{
						//tanggalMulai = DateTimeUtils.formatDateToString(rs.getDate("tgl_masa_berlaku_mulai"),"dd MMMM YYYY");
						tanggalMulai = simpleDateFormat.format(rs.getDate("tgl_masa_berlaku_mulai")); //.toLocaleString(); //DateTimeUtils.formatDateToString(,"dd MMMM YYYY");
					}catch (Exception e){}
					String tanggalAkhir = "-";
					try{
						//String date =
						tanggalAkhir = simpleDateFormat.format(rs.getDate("tgl_masa_berlaku_selesai")); //.toLocalDate().toString(); //DateTimeUtils.formatDateToString(,"dd MMMM YYYY");
					}catch (Exception e){}

					results[3] = tanggalMulai + " " + Messages.get("sampai_dengan.label") + " " + tanggalAkhir;

					//Logger.debug(results[3]);
					String check = "<span style=\"color:green\" class=\"fa fa-check fa-lg\"></span>";
					String uncheck = "<span style=\"color:red\" class=\"fa fa-close fa-lg\"></span>";
					results[4] = uncheck;
					if(rs.getInt("apakah_berlaku")==1){
						results[4] = check;
					}

					List<ButtonGroup> buttonGroups = new ArrayList<>();
					buttonGroups.add(new ButtonGroup(Messages.get("ubah.label"), urlEdit));
					if(rs.getInt("apakah_berlaku")==1){
						buttonGroups.add(new ButtonGroup(Messages.get("non_aktifkan.label"), urlSetNonActive));
					}else{
						buttonGroups.add(new ButtonGroup(Messages.get("aktifkan.label"), urlSetActive));
					}


//						buttonGroups.add(new ButtonGroup(Messages.get("nonaktifkan.label"), urlEdit));


					results[5] = DataTableService.generateButtonGroup(buttonGroups);

//					results[5] = String.format("<a href='%s' class='btn btn-primary btn-xs'>%s</a>", urlEdit, Messages.get("ubah.label"));
					return results;
				}
			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_KONTRAK_PENYEDIA =
			new DatatableResultsetHandler("pk.id, no_kontrak, dok_id_attachment, file_name, original_file_name,tgl_masa_berlaku_mulai, tgl_masa_berlaku_selesai, file_sub_location") {
				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);

					results[1] = rs.getString("no_kontrak");

					String url;
					String file_name = rs.getString("file_name");
					String original_file_name = rs.getString("original_file_name");
					Long dokAttachment = rs.getLong("dok_id_attachment");
					Logger.debug(rs.getString("file_sub_location"));
					//BlobTable blobTable = BlobTable.find("blb_id_content = ?",dokAttachment).first();
					BlobTable blobTable = BlobTable.findByBlobId(dokAttachment);
					if(blobTable == null){
						// /data/files/upload/penyedia_kontrak/2016/08/16/14713178744276.pdf
						url = "/public/files/upload/penyedia_kontrak/"+rs.getString("file_sub_location") + file_name;

					}else{
						DokumenInfo dokumenInfo = DokumenInfo.findDokumenKontrak(blobTable);
						url = dokumenInfo.download_url;
					}

					Logger.debug(url);


					results[2] = String.format("<a target='_blank' href='%s'>%s</a>",url,original_file_name);

					String tanggalMulai = "-";
					String pattern = "dd MMMMM yyyy";
					SimpleDateFormat simpleDateFormat =
							new SimpleDateFormat(pattern, new Locale("id", "ID"));
					try{
						//tanggalMulai = DateTimeUtils.formatDateToString(rs.getDate("tgl_masa_berlaku_mulai"),"dd MMMM YYYY");
						tanggalMulai = simpleDateFormat.format(rs.getDate("tgl_masa_berlaku_mulai")); //.toLocaleString(); //DateTimeUtils.formatDateToString(,"dd MMMM YYYY");
					}catch (Exception e){}
					String tanggalAkhir = "-";
					try{
						//String date =
						tanggalAkhir = simpleDateFormat.format(rs.getDate("tgl_masa_berlaku_selesai")); //.toLocalDate().toString(); //DateTimeUtils.formatDateToString(,"dd MMMM YYYY");
					}catch (Exception e){}

					results[3] = tanggalMulai + " " + Messages.get("sampai_dengan.label") + " " + tanggalAkhir;

					return results;
				}
			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_RUP=
			new DatatableResultsetHandler("r.id, r.nama, r.tahun_anggaran, r.id_sat_ker, r.sumber_dana, k.id, k.nama, k.jenis, " +
					"ks.id, ks.idKldi, ks.nama, ks.alamat, ks.idSatker, k.prp_id, k.kbp_id") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("r.id");
					results[1] = rs.getString("r.nama");
					results[2] = rs.getString("r.tahun_anggaran");
					results[3] = rs.getString("k.jenis");
					results[4] = rs.getString("k.nama");
					results[5] = rs.getString("ks.nama");
					results[6] = String.format("<a href='#' data-id='%s' data-jenisInstansiId='%s' data-jenisInstansi='%s' data-tahunAnggaran='%s'" +
									"data-instansiId='%s' data-instansi='%s' data-alamatSatker='%s' data-namasatker='%s' data-idsatker='%s' " +
									"data-prpId='%s' data-kbpId='%s' data-sumberdana='%s' data-namaRup='%s' class='rup-select-button btn btn-primary btn-xs'>%s</a>",
							rs.getLong("r.id"), rs.getString("k.jenis"), rs.getString("k.jenis"), rs.getString("r.tahun_anggaran"),
							rs.getString("k.id"), rs.getString("ks.nama"), rs.getString("ks.alamat"),
							rs.getString("k.nama"), rs.getString("r.id_sat_ker"),
							rs.getString("k.prp_id"), rs.getString("k.kbp_id"),
							rs.getString("r.sumber_dana"), rs.getString("r.nama"), Messages.get("pilih.label"));
					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_PPK=
			new DatatableResultsetHandler("p.id, p.nama_lengkap, p.user_name, p.user_email, p.jabatan, p.no_telp, p.nip, p.nomor_sertifikat_pbj") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("p.nama_lengkap");
					results[1] = rs.getString("p.user_name");
					results[2] = rs.getString("p.user_email");
					results[3] = String.format("<a href='#' data-id='%s' data-nip='%s' data-username='%s' data-nama='%s' data-jabatan='%s' data-email='%s'" +
									"data-notelp='%s' data-sertifikatpbj='%s' class='ppk-select-button btn btn-primary btn-xs'>%s</a>",
							rs.getLong("p.id"), rs.getString("p.nip"), rs.getString("p.user_name"), rs.getString("p.nama_lengkap"),
							rs.getString("p.jabatan"), rs.getString("p.user_email"),
							rs.getString("p.no_telp"), rs.getString("p.nomor_sertifikat_pbj"), Messages.get("pilih.label"));
					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_ULP=
			new DatatableResultsetHandler("p.id, p.nama_lengkap, p.user_name, p.user_email, p.jabatan, p.no_telp, p.nip, p.nomor_sertifikat_pbj") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("p.nama_lengkap");
					results[1] = rs.getString("p.user_name");
					results[2] = rs.getString("p.user_email");
					results[3] = String.format("<a href='#' data-id='%s' data-nip='%s' data-nama='%s' data-jabatan='%s' data-email='%s'" +
									"data-notelp='%s' data-sertifikatpbj='%s' class='ulp-select-button btn btn-primary btn-xs'>%s</a>",
							rs.getLong("p.id"), rs.getString("p.nip"), rs.getString("p.nama_lengkap"),
							rs.getString("p.jabatan"), rs.getString("p.user_email"),
							rs.getString("p.no_telp"), rs.getString("p.nomor_sertifikat_pbj"), Messages.get("pilih.label"));
					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_PROVINSI=
			new DatatableResultsetHandler("k.id,k.nama_provinsi,status, ubah status, aksi") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("k.id");
					boolean active = rs.getInt("active") == 1;
					String classActive = active ? "class='btn btn-success btn-xs disabled'":"class='btn btn-danger btn-xs disabled'";
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
					String urlDetail =Router.reverse("masterdata.WilayahCtr.detail", map).url;
//					String urlDelete =Router.reverse("masterdata.WilayahCtr.delete", map);
					String urlAktif =Router.reverse("masterdata.WilayahCtr.setProvinsiAktif", map).url;
					String urlNonAktif =Router.reverse("masterdata.WilayahCtr.setProvinsiNonAktif", map).url;

					List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();
					buttonGroups.add(new ButtonGroup(Messages.get("detail.label"), urlDetail));
//					buttonGroups.add(new ButtonGroup(Messages.get("hapus.label"),urlDelete));

					results[1] = rs.getString("k.nama_provinsi");
					results[2] = String.format("<a href='#' %s style='width:80px'>%s</a>",classActive, active ? Messages.get("aktif.label"):Messages.get("tidak_aktif.label"));
					results[3] = String.format("<a href='%s' class='btn "+ (active?"btn-danger":"btn-success") +" btn-xs' style='width:80px'>%s</a>",active? urlNonAktif:urlAktif, (active?"Non Aktifkan":"Aktifkan"));
//					results[4] = DataTableService.generateButtonGroup(buttonGroups);
					results[4] = String.format("<a href='%s' class='btn btn-primary btn-xs'>%s</a>", urlDetail, Messages.get("detail.label"));

					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_KABUPATEN=
			new DatatableResultsetHandler("k.id,k.nama_kabupaten, status, ubah status, aksi") {

				public String[] handle(ResultSet rs) throws SQLException {
//					String[] results=new String[columns.length];
					String[] results=new String[5];
					results[0] = rs.getString("k.id");
					boolean active = rs.getInt("active") == 1;
					String classActive = active ? "class='btn btn-success btn-xs disabled'":"class='btn btn-danger btn-xs disabled'";
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
//					String urlDetail =Router.reverse("masterdata.WilayahCtr.detail", map);
//					String urlDelete =Router.reverse("masterdata.WilayahCtr.delete", map);
					String urlAktif =Router.reverse("masterdata.WilayahCtr.setKabupatenAktif", map).url;
					String urlNonAktif =Router.reverse("masterdata.WilayahCtr.setKabupatenNonAktif", map).url;

//					List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();
//					buttonGroups.add(new ButtonGroup(Messages.get("detail.label"), urlDetail));
//					buttonGroups.add(new ButtonGroup(Messages.get("hapus.label"),urlDelete));

					results[1] = String.format("<a href='#' class='nama_kabupaten' kabupaten_id='%s'>%s</a>",rs.getString("k.id"),rs.getString("k.nama_kabupaten"));
					results[2] = String.format("<a href='#' %s style='width:80px'>%s</a>",classActive, active ? Messages.get("aktif.label"):Messages.get("tidak_aktif.label"));
					results[3] = String.format("<a href='%s' class='btn "+ (active?"btn-danger":"btn-success") +" btn-xs' style='width:80px'>%s</a>",active? urlNonAktif:urlAktif, (active?"Non Aktifkan":"Aktifkan"));
//					results[4] = DataTableService.generateButtonGroup(buttonGroups);

					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_INSTANSI=
			new DatatableResultsetHandler("k.id, k.nama, k.jenis,p.nama_provinsi") {

				public String[] handle(ResultSet rs) throws SQLException {
//					String[] results=new String[columns.length];
					String[] results=new String[5];
					results[0] = rs.getString("k.id");
					boolean active = true;
					String classActive = active ? "class='btn btn-success btn-xs disabled'":"class='btn btn-danger btn-xs disabled'";
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
					String urlDetail =Router.reverse("masterdata.InstansiSatkerCtr.detail", map).url;
//					String urlDelete =Router.reverse("masterdata.InstansiSatkerCtr.delete", map);
					String urlAktif =Router.reverse("masterdata.InstansiSatkerCtr.setInstansiAktif", map).url;
					String urlNonAktif =Router.reverse("masterdata.InstansiSatkerCtr.setInstansiNonAktif", map).url;

					List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();
					buttonGroups.add(new ButtonGroup(Messages.get("detail.label"), urlDetail));
//					buttonGroups.add(new ButtonGroup(Messages.get("hapus.label"),urlDelete));

					results[1] = rs.getString("k.nama");
					//results[1] = String.format("<a href='#' class='nama_instansi' instansi_id='%s'>%s</a>",rs.getString("k.id"),rs.getString("k.nama"));
					results[2] = String.format("%s",rs.getString("k.jenis"));
					results[3] = String.format("%s",rs.getString("p.nama_provinsi"));
//					results[4] = String.format("<a href='#' %s style='width:80px'>%s</a>",classActive, active ? Messages.get("aktif.label"):Messages.get("tidak_aktif.label"));
//					results[5] = String.format("<a href='%s' class='btn "+ (active?"btn-danger":"btn-success") +" btn-xs' style='width:80px'>%s</a>",active? urlNonAktif:urlAktif, (active?"Non Aktifkan":"Aktifkan"));
//					results[4] = DataTableService.generateButtonGroup(buttonGroups);
					results[4] = String.format("<a href='%s' class='btn btn-primary btn-xs'>%s</a>", urlDetail, Messages.get("detail.label"));

					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_SATKER=
			new DatatableResultsetHandler("id, nama, alamat, active, idKldi") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
//					String[] results=new String[5];
					results[0] = rs.getString("id");
					boolean active = true;
					String classActive = active ? "class='btn btn-success btn-xs disabled'":"class='btn btn-danger btn-xs disabled'";
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
//					String urlDetail =Router.reverse("masterdata.WilayahCtr.detail", map);
//					String urlDelete =Router.reverse("masterdata.WilayahCtr.delete", map);
					String urlAktif =Router.reverse("masterdata.WilayahCtr.setSatkerAktif", map).url;
					String urlNonAktif =Router.reverse("masterdata.WilayahCtr.setSatkerNonAktif", map).url;

//					List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();
//					buttonGroups.add(new ButtonGroup(Messages.get("detail.label"), urlDetail));
//					buttonGroups.add(new ButtonGroup(Messages.get("hapus.label"),urlDelete));

					results[1] = String.format("<a href='#' class='nama_satker' satker_id='%s'>%s</a>",rs.getString("id"),rs.getString("nama"));
					results[2] = String.format("%s",rs.getString("alamat")==null?"-":rs.getString("alamat"));
					results[3] = String.format("%s",rs.getString("idKldi"));
//					results[4] = String.format("<a href='#' %s style='width:80px'>%s</a>",classActive, active ? Messages.get("aktif.label"):Messages.get("tidak_aktif.label"));
//					results[5] = String.format("<a href='%s' class='btn "+ (active?"btn-danger":"btn-success") +" btn-xs' style='width:80px'>%s</a>",active? urlNonAktif:urlAktif, (active?"Non Aktifkan":"Aktifkan"));
//					results[6] = DataTableService.generateButtonGroup(buttonGroups);

					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> LIST_KONTRAK_ALL =
			new DatatableResultsetHandler("du.id, km.nama_komoditas, pn.nama_penawaran, pe.nama_penyedia, dk.nama_kategori, du.nama_dokumen, du.file_path, du.active, du.created_date,unduh, perpanjangan_id,du.penawaran_id") {

				public String[] handle(ResultSet rs) throws SQLException{
					String[] results = new String[columns.length];
					boolean active = rs.getInt("du.active") == 1;
					String classActive = active ? "class='btn btn-success btn-xs disabled'":"class='btn btn-danger btn-xs disabled'";
					String classUnduh = "class='btn btn-primary btn-xs'";
					File filepath = new File(rs.getString("du.file_path")+rs.getString("du.nama_dokumen"));
					Long idPenawaran = rs.getLong("du.penawaran_id");
					Penawaran penawaran = Penawaran.findById(idPenawaran);
					results[0] = rs.getString("du.id");
					String perpId = rs.getString("perpanjangan_id");
					results[1] = rs.getString("km.nama_komoditas");
					results[2] = rs.getString("pn.nama_penawaran");
					results[3] = rs.getString("pe.nama_penyedia");
					results[4] = rs.getString("du.nama_dokumen")+"-"+perpId;
					results[5] = String.format("<a href='#' %s>%s</a>", classActive, active ? Messages.get("aktif.label"):Messages.get("tidak_aktif.label"));
					results[6] = rs.getString("du.created_date");
					//results[4] = String.format("<a href='%s' %s download>%s</a>", filepath, classUnduh, Messages.get("unduh.label"));
					String linkProcess = "";
					linkProcess = String.format("<a href='%s' class='btn btn-primary' onclick=\"return confirm('%s')\">%s</a>","/prakatalog/perpanjang_penawaran/edit/"+idPenawaran, Messages.get("perpanjangan.confirm"),Messages.get("perpanjangan.label"));
					linkProcess += String.format("<a href='%s' class='btn btn-primary' >%s</a>","/admin.usulanCtr/goToList?usulan_id="+penawaran.usulan_id.toString(), Messages.get("dokumen.label"));
					linkProcess += String.format("<a href='%s' class='btn btn-primary' >%s</a>","/prakatalog.perpanjangPenawaranctr/goToList?perp_id="+perpId, Messages.get("produk.label"));
					linkProcess += String.format("<a href='%s' class='btn btn-primary' >%s</a>","/prakatalog.perpanjangPenawaranctr/beritaAcara?id="+perpId, Messages.get("berita_acara.label"));

					results[7] = linkProcess;

					results[8] = "Approval";

					return results;
				}
			};

	public final static DatatableResultsetHandler KOMODITAS_KLDI=
			new DatatableResultsetHandler("id,nama_komoditas") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
					Map<String,Object> params = new HashMap<String, Object>();

					params.put("language", Lang.get().isEmpty() ? "id" : Lang.get());
					params.put("komoditas_slug", KatalogUtils.generateSlug(rs.getString("nama_komoditas")));
					params.put("id",rs.getString("id"));

					String url = Router.reverse("katalog.ProdukCtr.listProduk",params).url;
					String nama_kom = rs.getString("nama_komoditas");
					results[1] = String.format("<a href='%s'>%s</a>", url, nama_kom);
					return results;
				}
			};

	public final static DatatableResultsetHandler LIST_KOMODITAS_KLDI=
			new DatatableResultsetHandler("id,nama_komoditas") {
				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
					Map<String,Object> params = new HashMap<String, Object>();

					params.put("language", Lang.get().isEmpty() ? "id" : Lang.get());
					params.put("komoditas_id",rs.getString("id"));

					String url = Router.reverse("admin.UserLokalCtr.createUserKomoditas",params).url;
					String nama_kom = rs.getString("nama_komoditas");
					results[1] = String.format("<a href='%s'>%s</a>", url, nama_kom);
					return results;
				}
			};

	public final static DatatableResultsetHandler LIST_KOMODITAS_KLDI_PENYEDIA=
			new DatatableResultsetHandler("id,nama_komoditas") {
				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
					Map<String,Object> params = new HashMap<String, Object>();

					params.put("language", Lang.get().isEmpty() ? "id" : Lang.get());
					params.put("komoditas_id",rs.getString("id"));

					String url = Router.reverse("admin.PenyediaLokalCtr.createPenyediaLokal",params).url;
					String nama_kom = rs.getString("nama_komoditas");
					results[1] = String.format("<a href='%s'>%s</a>", url, nama_kom);
					return results;
				}
			};

	public final static DatatableResultsetHandler<String[]> LIST_USULAN_KOMODITAS_KLDI=
			new DatatableResultsetHandler("id,nama_usulan") {
				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);
					Map<String,Object> params = new HashMap<String, Object>();

					params.put("usulan_id",rs.getString("id"));

					String url = Router.reverse("admin.PenyediaLokalCtr.createPenyediaLokal", params).url;
					results[1] = String.format("<a href='%s'>%s</a>", url, rs.getString("nama_usulan"));
					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> USER_KOMODITAS_KLDI=
			new DatatableResultsetHandler("o.id, o.komoditas_id, u.user_name, u.nama_lengkap, u.user_email, u.pps_id") {

				public String[] handle(ResultSet rs) throws SQLException{
					String[] results = new String[columns.length];
					String urlDelete = Router.reverse("admin.UserLokalCtr.deleteUserKomoditas").url;

					results[0] = rs.getString("o.id");
					results[1] = rs.getString("u.user_name");
					results[2] = rs.getString("u.nama_lengkap");
					results[3] = rs.getString("u.user_email");
					results[4] = rs.getString("u.pps_id");
					results[5] = String.format("<a href='%s?id=%s&idKom=%s' class='btn btn-danger btn-xs'><i class='fa fa-trash'><i></a>", urlDelete, rs.getLong("o.id"), rs.getLong("o.komoditas_id"));

					return results;
				}
			};

	public final static DatatableResultsetHandler<String[]> USER_PENYEDIA_LOKAL=
			new DatatableResultsetHandler("pk.id, pk.komoditas_id, u.user_name, u.nama_lengkap, u.user_email, u.rkn_id") {

				public String[] handle(ResultSet rs) throws SQLException{
					String[] results = new String[columns.length];
					String urlDelete = Router.reverse("admin.PenyediaLokalCtr.deleteUserKomoditas").url;

					results[0] = rs.getString("pk.id");
					results[1] = rs.getString("u.user_name");
					results[2] = rs.getString("u.nama_lengkap");
					results[3] = rs.getString("u.user_email");
					results[4] = rs.getString("u.rkn_id");
					results[5] = String.format("<a href='%s?id=%s' class='btn btn-danger btn-xs'><i class='fa fa-trash'><i></a>", urlDelete, rs.getLong("pk.id"));

					return results;
				}
			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_KOMODITAS_NASIONAL=
			new DatatableResultsetHandler("id, nama_komoditas, kode_komoditas, jenis_produk, kelas_harga") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					Map<String,Object> params = new HashMap<>();
					params.put("language", Lang.get().isEmpty() ? "id" : Lang.get());
					params.put("komoditas_slug", rs.getString("nama_komoditas").replace(" ", "-").toLowerCase());
					params.put("id",results[0]);
					String url =Router.reverse("katalog.ProdukCtr.listProduk", params).url;
					results[1] = String.format("<a href='%s'>%s</a>", url, rs.getString("nama_komoditas"));
					return results;
				}

			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_KOMODITAS_LOKAL=
			new DatatableResultsetHandler("id, nama") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					Map<String,Object> params = new HashMap<>();
					params.put("id",results[0]);
					String url =Router.reverse("PublikCtr.komoditasKldiIndex", params).url;
					results[1] = String.format("<a href='%s'>%s</a>", url, rs.getString("nama"));
					return results;
				}

			};

    public final static DatatableResultsetHandler<String[]> DAFTAR_BIDANG =
            new DatatableResultsetHandler("id, nama_bidang, aksi ") {

                public String[] handle(ResultSet rs) throws SQLException {
                    String[] results = new String[columns.length];
                    String urlDelete = Router.reverse("controllers.masterdata.BidangCtr.delete").url;

                    results[0] = rs.getString("id");

                    results[1] = String.format("<a href='#' class='nama_bidang' bidang_id='%s'>%s</a>", rs.getLong("id"), rs.getString("nama_bidang"));

                    results[2] = String.format("<a href='%s?id=%s' class='btn btn-danger delete btn-xs'>%s</a>", urlDelete, rs.getLong("id"), Messages.get("hapus.label"));

                    return results;
                }

            };

	public final static DatatableResultsetHandler<String[]> DAFTAR_BANNER=
			new DatatableResultsetHandler("id, subjek, file_name, original_file_name, active") {

				public String[] handle(ResultSet rs) throws SQLException {

					String[] results=new String[columns.length];
					results[0] = rs.getString("id");

					Map<String, Object> map = new HashMap<>();
					map.put("id", results[0]);

					boolean active = rs.getInt("active") == 1;
					String urlAktif =Router.reverse("cms.BannerCtr.setAktif", map).url;
					String urlNonAktif =Router.reverse("cms.BannerCtr.setNonAktif", map).url;

					Map<String, Object> params = new HashMap<String, Object>();
					params.put("id",rs.getLong("id"));

					List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();
					String urlEdit = Router.reverse("cms.BannerCtr.edit", params).url;
					buttonGroups.add(new ButtonGroup(Messages.get("edit.label"), urlEdit));
					String urlDelete = Router.reverse("cms.BannerCtr.delete", params).url;
					buttonGroups.add(new ButtonGroup(Messages.get("hapus.label"), urlDelete));

					results[1] = rs.getString("subjek");
					results[2] = String.format("<img src='/public/files/image/banner/%s' height='50px'/>", rs.getString("file_name"));
					results[3] = String.format("<a href='%s' class='btn "+ (active?"btn-danger":"btn-success") +" btn-xs' style='width:80px'>%s</a>",active? urlNonAktif:urlAktif, (active?"Non Aktifkan":"Aktifkan"));
					results[4] = DataTableService.generateButtonGroup(buttonGroups);


					return results;
				}

			};

    public final static DatatableResultsetHandler<String[]> DAFTAR_PEMBARUAN_PENYEDIA_DISTRIBUTOR=
			new DatatableResultsetHandler("id,penyedia_id,nama_distributor,no_telp,email,website,minta_disetujui,setuju_tolak,apakah_disetujui,aksi,idpemb,pstgid") {
				public String[] handle(ResultSet rs) throws SQLException {
					long apakah_disetujui = rs.getInt("apakah_disetujui");
					String status = "";
					if(apakah_disetujui == 1){
						status = Messages.get("disetujui.label");
					}else if(apakah_disetujui == 0){
						status = Messages.get("menunggu_persetujuan.label");
					}else{
						status = Messages.get("ditolak.label");
					}

					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					results[1] = rs.getString("nama_distributor");
					results[2] = rs.getString("no_telp");
					results[3] = rs.getString("email");
					results[4] = rs.getString("website");
					results[5] = status;

					Boolean showButtonKirim = false;
					PermohonanPembaruan pmb = PermohonanPembaruan.findById(rs.getLong("idpemb"));
					if (pmb.status.equalsIgnoreCase(pmb.STATUS_PERSIAPAN)) {
						showButtonKirim = true;
					}

					List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();

					Map<String, Object> detailParams = new HashMap<String, Object>();
					//detailParams.put("pid", rs.getLong("penyedia_id"));
					//detailParams.put("id", PenyediaDistributor.createDistributorPembaruanId(rs.getLong("idpemb"), rs.getLong("id")));
					detailParams.put("id", rs.getLong("pstgid"));
					String urlDetail = Router.reverse("katalog.PermohonanPembaruanCtr.detailDistributor", detailParams).url;
					buttonGroups.add(new ButtonGroup(Messages.get("detail.label"), urlDetail));

					if(showButtonKirim) {
						Map<String, Object> params = new HashMap<String, Object>();
						params.put("id", rs.getLong("pstgid"));

						Map<String, Object> editParams = new HashMap<String, Object>();
						editParams.put("id", rs.getLong("pstgid"));

						String urlEdit = Router.reverse("katalog.PermohonanPembaruanCtr.editDistributor",editParams).url;
						buttonGroups.add(new ButtonGroup(Messages.get("ubah.label"),urlEdit));

						String urlDelete = Router.reverse("katalog.PermohonanPembaruanCtr.hapusDistributorPermohonanPembaruan", params).url;
						ButtonGroup delete = new ButtonGroup(Messages.get("hapus.label"),urlDelete);
						delete.isDelete = true;
						buttonGroups.add(delete);
					}
					results[6] = DataTableService.generateButtonGroup(buttonGroups);

					return results;
				}
			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_APPROVAL_PEMBARUAN_PENYEDIA_DISTRIBUTOR=
			new DatatableResultsetHandler("id,penyedia_id,nama_distributor,no_telp,email,website,minta_disetujui,apakah_disetujui,setuju_tolak,aksi") {
				public String[] handle(ResultSet rs) throws SQLException {
					long apakah_disetujui = rs.getInt("apakah_disetujui");
					String status = "";
					if(apakah_disetujui == 1){
						status = Messages.get("disetujui.label");
					}else if(apakah_disetujui == 0){
						status = Messages.get("menunggu_persetujuan.label");
					}else{
						status = Messages.get("ditolak.label");
					}

					long idDistributor = rs.getLong("id");

					String[] results=new String[columns.length];
					results[0] = rs.getString("id");
					if(apakah_disetujui == 0) {
						results[1] = String.format("<input type='checkbox' name='idDistributor' value='%s' class='idDistributors' />", idDistributor);
					}else{
						results[1] = "";
					}
					results[2] = rs.getString("nama_distributor");
					results[3] = rs.getString("no_telp");
					results[4] = rs.getString("email");
					results[5] = rs.getString("website");
					results[6] = status;

					List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();

					Map<String, Object> params = new HashMap<String, Object>();
					params.put("id",rs.getLong("id"));

					Map<String, Object> editParams = new HashMap<String, Object>();
					editParams.put("id",rs.getLong("id"));
					editParams.put("pid",rs.getLong("penyedia_id"));

					String urlDetail = Router.reverse("penyedia.DistributorCtr.detail",editParams).url;
					buttonGroups.add(new ButtonGroup(Messages.get("detail.label"),urlDetail));

					results[7] = DataTableService.generateButtonGroup(buttonGroups);

					return results;
				}
			};

	public final static DatatableResultsetHandler<String[]> DAFTAR_KLDI=
			new DatatableResultsetHandler("id, nama, jenis, aksi") {

				public String[] handle(ResultSet rs) throws SQLException {
					String[] results=new String[columns.length];
					results[0] = rs.getString("nama");
					results[1] = rs.getString("jenis");
					results[2] = String.format("<a href='#' data-id='%s' data-nama= '%s' data-jenis='%s' class='kldi-select-button btn btn-primary btn-xs'>%s</a>",
							rs.getString("id"), rs.getString("nama"), rs.getString("jenis"), Messages.get("pilih.label"));
					return results;
				}

			};
}

