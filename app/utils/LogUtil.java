package utils;

import models.jcommon.util.CommonUtil;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;

/**
 * @author HanusaCloud on 11/29/2017
 */
public class LogUtil {

    @Deprecated
    public static void d(String tag, String message) {
        debug(tag, message);
    }

    @Deprecated
    public static void d(String tag, Object object) {
        Logger.debug(tag + " - " + CommonUtil.toJson(object));
    }

    @Deprecated
    public static void e(String tag, Throwable t) {
        t.printStackTrace();
        Logger.error(t, tag + " - " + t.getMessage());
    }

    public static void info(String TAG, String message) {
        Logger.info(TAG + " - " + message);
    }

    public static void info(String TAG, Object object) {
        info(TAG, CommonUtil.toJson(object));
    }

    public static void warn(String TAG, String message) {
        Logger.info(TAG + " - " + message);
    }

    public static void warn(String TAG, Object object) {
        info(TAG, CommonUtil.toJson(object));
    }

    public static void debug(String TAG, String message, Double value) {
        debug(TAG,  message + " " + String.format("%1$,.2f", value));
    }

    public static void debug(String TAG, String message) {
        Logger.debug(generateTag(TAG) + message);
    }

    public static void debug(String TAG, String message, Object arg) {
        if (!message.contains("%s")) {
            message += " %s";
        }
        Logger.debug(generateTag(TAG) + String.format(message, CommonUtil.toJson(arg)));
    }

    public static void debug(String TAG, Object object) {
        debug(TAG, CommonUtil.toJson(object));
    }

    private static String generateTag(String TAG) {
        if (Play.mode.isDev()) {
            return TAG + "@" + getMethodName(TAG) + " - ";
        }
        return TAG + " - ";
    }

    private static String getMethodName(String TAG) {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        if (elements == null || elements.length <= 0) {
            return "";
        }
        for (StackTraceElement stackTrace :elements) {
            final String className = stackTrace.getClassName();
            if (!className.equalsIgnoreCase("utils.LogUtil") && className.contains(TAG)) {
                return stackTrace.getMethodName() + "()[" + stackTrace.getLineNumber() + "]";
            }
        }
        return "";
    }

    public static void error(String TAG, Throwable t, String message) {
        t.printStackTrace();
        Logger.error(t, TAG + " - " + t.getMessage() + " - " + message);
    }

    public static void error(String TAG, Throwable t) {
        error(TAG, t, t.getMessage());
    }

    public static void multiline(String TAG, String message) {
        printLines(TAG, message);
    }

    public static void multiline(String TAG, Object obj) {
        final String message = CommonUtil.toJson(obj);
        printLines(TAG, message);
    }

    private static void printLines(String TAG, String message) {
        if (!StringUtils.isEmpty(message)) {
            int maxLogSize = 500;
            for (int i = 0; i <= message.length() / maxLogSize; i++) {
                int start = i * maxLogSize;
                int end = (i + 1) * maxLogSize;
                end = end > message.length() ? message.length() : end;
                debug(TAG, "[" + i + "]" + message.substring(start, end));
                if (i == 50) {
                    debug(TAG, "[end]Too much to show.....");
                    break;
                }
            }
        }
    }

}
