package controllers;

import java.io.File;

import models.util.Config;
import play.Play;
import play.mvc.Controller;

//Untuk ambil gambar dan file
public class FileDanGambarCtr extends Controller {

	/**URL untuk image harus sama persis dengan path di filesystem supaya
	 * bisa mudah pakai CDN
	 * @param th
	 * @param bl
	 * @param tgl
	 * @param nama
	 */
	public static void gambar1(String th, String bl, String tgl, String nama)
	{
		String root=Config.getInstance().fileStorageDir;
		if(root==null)
			root="";
		String fileName=String.format("%s/%s/%s/%s/%s", root, th, bl, tgl, nama);
		if(fileName.contains(".."))//untuk security
			forbidden();
		File file=new File(fileName);
		if(!file.exists())
			file=new File(Play.applicationPath+ "/public/images/image-not-found.png");
		renderBinary(file);
	}
	
	public static void gambar2(String path)
	{
		String root=Config.getInstance().fileStorageDir;
		if(root==null)
			root="";
		String fileName=String.format("%s/%s", root,  path);
		if(fileName.contains(".."))//untuk security
			forbidden();
		File file=new File(fileName);
		if(!file.exists())
			file=new File(Play.applicationPath+ "/public/images/image-not-found.png");
		renderBinary(file);
	}
}
