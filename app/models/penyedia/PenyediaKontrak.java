package models.penyedia;

import com.google.gson.Gson;
import ext.DateBinder;
import models.BaseKatalogTable;
import models.common.AktifUser;
import models.common.DokumenInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import play.Logger;
import play.data.binding.As;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.DateTimeUtils;

import javax.persistence.Transient;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by raihaniqbal on 8/11/17.
 */
@Table(name = "penyedia_kontrak")
public class PenyediaKontrak extends BaseKatalogTable {

	@Id
	public Long id;
	public Long penyedia_id;
	public Long komoditas_id;
	public Long penawaran_id;
	public String no_kontrak;
	public String deskripsi;
	public String file_name;
	//penyesuaian dengan table yang ada
	public String file_sub_location;
	public String file_hash;
	public String original_file_name;
	public Long dok_id_attachment;
	public Long file_size = new Long(0);
	public Integer posisi_file;
	public boolean active;
	public boolean apakah_berlaku;

	@As(binder = DateBinder.class)
	public Date tgl_masa_berlaku_mulai;

	@As(binder = DateBinder.class)
	public Date tgl_masa_berlaku_selesai;

	@Transient
	public DokumenInfo dokumenInfo;

	public static DokumenInfo simpanKontrakPenyedia(File file, Long idp) throws Exception {

		PenyediaKontrak kontrak = new PenyediaKontrak();
		//
		if(null == idp){
			idp = new Long(0);
		}
		kontrak.penyedia_id = idp;
		kontrak.active = true;
		kontrak.original_file_name = file.getName();
		kontrak.posisi_file = 0;
		kontrak.apakah_berlaku = false;
		kontrak.file_size= file.length();
		kontrak.no_kontrak = "-";
		kontrak.created_by = AktifUser.getActiveUserId();

		Long kontrakId = new Long(kontrak.save());
		//mekanisme pembuatan kontrak penyedia, sebelumnya gak berhasil upload, kontrak penyedia gagal dibuat
		//sehingga mengakibatkan error di belakang, tidak ada kontrak penyedia, sekarang saving dulu baru kaitkan
		//dengan file upload, bila filenya tidak ada, upload ulang saja nantinya. tidak peru ulang proses

		if( null != kontrakId){
			//sudah termasuk kontrak_id di blb_id_content
			BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, kontrakId, PenyediaKontrak.class.getSimpleName());
			//update kembali
			kontrak = PenyediaKontrak.findById(kontrakId);
			kontrak.dok_id_attachment = blob.id;
			kontrak.file_name = blob.blb_nama_file;
			kontrak.file_hash = blob.blb_hash;
			Long idsave = new Long(kontrak.save());
			Logger.info(kontrakId+"penyedia kontrak id nya hasil save oke = "+idsave);
			return new DokumenInfo(blob);
		}else{
			Logger.info("gagal save kontrak penyedia");
			return DokumenInfo.findByBlob(new BlobTable());
		}
	}

	public String getFormattedStartedDate() {
		return DateTimeUtils.convertToDdMmYyyy(this.tgl_masa_berlaku_mulai);
	}

	public String getFormattedFinishedDate() {
		return DateTimeUtils.convertToDdMmYyyy(this.tgl_masa_berlaku_selesai);
	}
	
	public static PenyediaKontrak getByDokIdAndKomoditas(int file_id){
		return find("dok_id_attachment = ? and komoditas_id is null",file_id).first();
	}

	public void updateKontrakPenyedia(long penyediaId, PenyediaKontrak kontrak, long penawaranId){
		this.penyedia_id = penyediaId;
		this.komoditas_id = kontrak.komoditas_id;
		this.penawaran_id = penawaranId;
		this.no_kontrak = kontrak.no_kontrak;
		this.deskripsi = kontrak.deskripsi;
		this.tgl_masa_berlaku_mulai = kontrak.tgl_masa_berlaku_mulai;
		this.tgl_masa_berlaku_selesai = kontrak.tgl_masa_berlaku_selesai;
		this.apakah_berlaku = kontrak.apakah_berlaku;
		this.active = true;
		this.save();
	}

	public void withAttachment() {
		if (this.dok_id_attachment != null) {
			//BlobTable blobTable = BlobTable.find("blb_id_content = ?", this.dok_id_attachment).first();
			BlobTable blobTable = BlobTable.findByBlobId(this.dok_id_attachment);
			if (blobTable != null) {
				this.dokumenInfo = DokumenInfo.findByBlob(blobTable);
				this.dokumenInfo.name = this.original_file_name;
			}
		}
	}

	public  void  setActive(){
		this.apakah_berlaku = true;
		prePersist();
		save();
	}

	public  void  setNonActive(){
		this.apakah_berlaku = false;
		prePersist();
		save();
	}

	public static List<PenyediaKontrak> getByPenyediaPenawaran(Long penyedia_id, Long penawaran_id){
		List<PenyediaKontrak> list = new ArrayList<>();
		String query = "SELECT * FROM penyedia_kontrak WHERE penyedia_id = ? and penawaran_id = ?";
		list = Query.find(query, PenyediaKontrak.class, penyedia_id, penawaran_id).fetch();
		return list;
	}

	public static List<PenyediaKontrak> getByPenawaranId(Long penawaran_id){
		List<PenyediaKontrak> list = new ArrayList<>();
		String query = "SELECT * FROM penyedia_kontrak WHERE penawaran_id = ?";
		list = Query.find(query, PenyediaKontrak.class, penawaran_id).fetch();
		return list;
	}

	public static void updateApakahBerlaku(Long penyedia_id, Long penawaran_id){
		String query = "UPDATE penyedia_kontrak set apakah_berlaku = 0 WHERE penyedia_id = ? and penawaran_id = ?";
		Query.update(query,penyedia_id, penawaran_id);
	}

}
