package models.purchasing.penerimaan;

import models.BaseKatalogTable;
import models.common.AktifUser;
import models.purchasing.contracts.AcceptanceContract;
import models.purchasing.penerimaan.form.AcceptanceDataForm;
import models.purchasing.pengiriman.PaketRiwayatPengiriman;
import models.purchasing.pengiriman.form.DeliveryItemJson;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import utils.DateTimeUtils;
import utils.KatalogUtils;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author HanusaCloud on 10/23/2017
 */
@Table(name = "paket_penerimaan")
public class PaketRiwayatPenerimaan extends BaseKatalogTable implements AcceptanceContract {

    @Id
    public Long id;
    public Long paket_id;
    public String no_dokumen;
    public String paket_pengiriman_no_dokumen_sistem;
    public Date tanggal_dokumen;
    public Date tanggal_terima;
    public String deskripsi;
    public Integer active = 1;
    public Boolean sudah_diterima_semua;
    public Integer agr_paket_penerimaan_produk;
    public Integer agr_paket_penerimaan_lampiran;
	public Double agr_total_harga_diterima_dan_ongkir=0.0;
    public Long paket_pembayaran_id;
    public Long paket_pengiriman_id;

    @Transient
    public List<ProdukPenerimaan> products = new ArrayList<>();
    @Transient
    public LampiranPenerimaan latestAttachment;
    @Transient
    public DeliveryItemJson delivery;

    public PaketRiwayatPenerimaan() {
    }

    public PaketRiwayatPenerimaan(AcceptanceDataForm model) {
        dataForm(model);
    }

    public PaketRiwayatPenerimaan dataForm(AcceptanceDataForm model) {
        if (!model.isCreateMode()) {
            this.id = model.model;
        }
        this.no_dokumen = model.docNumber;
        this.paket_id = model.packageId;
        this.tanggal_dokumen = model.getDocumentDate();
        this.tanggal_terima = model.getAcceptanceDate();
        this.deskripsi = model.note;
        this.active = 1;
        this.sudah_diterima_semua = model.allSent;
        this.agr_paket_penerimaan_produk = model.getSavedProducts().size();
        this.agr_paket_penerimaan_lampiran = model.attachment != null ? 1 : 0;
        return this;
    }

    @Override
    public String getDetailUrl() {
        return generateUrl("purchasing.PenerimaanCtr.detailRiwayatPenerimaan");
    }

    @Override
    public String getDeleteUrl() {
        return generateUrl("purchasing.PenerimaanCtr.deletePenerimaan");
    }

    @Override
    public String getUpdateUrl() {
        return generateUrl("purchasing.PenerimaanCtr.updatePenerimaan");
    }

    @Override
    public void saveHistory() {
        final long result = (long) save();
        if (this.id == null) {
            this.id = result;
        }
    }

    public PaketRiwayatPenerimaan deliveryId(PaketRiwayatPengiriman model) {
        this.paket_pengiriman_no_dokumen_sistem = model.no_dokumen_sistem;
        return this;
    }

    public void setDeliveryId(Long id) {
        this.paket_pengiriman_id = id;
    }

    public String getFormattedSystemDocNumber() {
        return KatalogUtils.getFormattedDocNumber(this.paket_pengiriman_no_dokumen_sistem);
    }

    public String getFormattedDocNumber() {
        return KatalogUtils.getFormattedDocNumber(this.no_dokumen);
    }

    public String getDdMmYyyyAcceptanceDate() {
        return DateTimeUtils.convertToDdMmYyyy(tanggal_terima);
    }

    public String getDdMmYyyyDocumentDate() {
        return DateTimeUtils.convertToDdMmYyyy(tanggal_dokumen);
    }

    public String getSystemDocNumber() {
        return paket_pengiriman_no_dokumen_sistem;
    }

    @Override
    public String getAttachmentDownloadUrl() {
        if (latestAttachment != null) {
            return latestAttachment.getDownloadUrl();
        }
        return "#";
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Long getPackageId() {
        return paket_id;
    }

    public String getAcceptanceDate() {
        return DateTimeUtils.parseToReadableDate(tanggal_terima);
    }

    public String getDocumentDate() {
        return DateTimeUtils.parseToReadableDate(tanggal_dokumen);
    }

    @Override
    public void setAttachment(LampiranPenerimaan model) {
        this.latestAttachment = model;
    }

    @Override
    public void setProducts(List<ProdukPenerimaan> list) {
        this.products = list;
    }

    @Override
    public void setDelivery(DeliveryItemJson model) {
        this.delivery = model;
    }

    @Override
    public Long getDeliveryId() {
        return this.paket_pengiriman_id;
    }

    @Override
    public DeliveryItemJson getDelivery() {
        return this.delivery;
    }

    @Override
    public List<ProdukPenerimaan> getProducts() {
        return this.products;
    }

    @Override
    public boolean isUserAllowed(AktifUser model) {
        return this.created_by == Math.toIntExact(model.user_id);
    }

    public void softDelete() {
        this.active = 0;
        setDeleted();
        save();
    }

}
