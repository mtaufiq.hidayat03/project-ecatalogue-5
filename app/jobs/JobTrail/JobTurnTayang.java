package jobs.JobTrail;

import jobs.elasticsearch.ElasticsearchProdukAdd;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;

@NoTransaction
public class JobTurnTayang extends Job{
    public Long idProduk = 0l;
    public JobTurnTayang(Long id_produk){
        idProduk = id_produk;
    }
    public void doJob(){
        ElasticsearchProdukAdd obj = new ElasticsearchProdukAdd();
        try {
            obj.preparation();
            obj.addRemoveProduk(idProduk);
        } catch (Exception e) {
            Logger.debug("Error-KontrakCtr.createSubmit-update elastic");
        }
    }
}
