package services.elasticsearch;

import com.google.gson.Gson;
import models.util.Config;
import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import play.Play;
import play.libs.WS;
//sudah tidak digunakan di versi berikutnya di playspse
//import play.libs.ws.WSAsync;
import services.elasticsearch.contract.ElasticsearchContract;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class ElasticsearchConnection {
    public static long connectTimeout = 1L;//minutes
    public static long readTimeout = 5L;
    public static long writeTimeout= 5L;
    private static Retrofit retrofit;
    public static ElasticsearchContract open(){
        Gson gson = new Gson();
        String urls =  getElasticurls();
        if(null == retrofit) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(urls)
                    .client(client())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit.create(ElasticsearchContract.class);
    }

    public static String getElasticurls(){
        Config config = Config.getInstance();
        return config.elasticsearchHost+"/"+config.elasticsearchIndex+"/";
    }
    public static ElasticsearchContract openBackaground(){
        Gson gson = new Gson();
        String urls = getElasticurls();
        if(null == retrofit) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(urls)
                    .client(client())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit.create(ElasticsearchContract.class);
    }

    private static OkHttpClient client() {
        int maxConnections = 50;
        int keepAliveDuration = 15000;
        ConnectionPool cp = new ConnectionPool(maxConnections, keepAliveDuration, TimeUnit.MILLISECONDS);
        return new OkHttpClient.Builder()
                .connectionPool(cp)
                .connectTimeout(connectTimeout, TimeUnit.MINUTES)
                .readTimeout(readTimeout, TimeUnit.MINUTES)
                .writeTimeout(writeTimeout, TimeUnit.MINUTES)
                .retryOnConnectionFailure(false)
                .build();
    }

    public void playWS(String url){
        WS.HttpResponse response = WS.url("").get();
    }

    public static boolean isSucceed(int code) {
        return code == 200;
    }

    public static boolean isCreated(int code) {
        return code == 201;
    }

    public static boolean isVersionConflict(int code) {
        return code == 409;
    }


}
