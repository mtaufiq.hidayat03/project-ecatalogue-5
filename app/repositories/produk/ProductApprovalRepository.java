package repositories.produk;

import models.common.AktifUser;
import models.prakatalog.Penawaran;
import models.prakatalog.Usulan;
import models.product.search.ProductApprovalSearch;
import models.product.search.ProductApprovalSearchResult;
import models.product.Produk;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.util.TextUtils;
import play.db.jdbc.Query;

public class ProductApprovalRepository {

    public static ProductApprovalSearchResult searchProductApproval(ProductApprovalSearch search) {
        ProductApprovalSearchResult model = new ProductApprovalSearchResult(search);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT DISTINCT p.id, " +
                "p.no_produk, " +
                "p.no_produk_penyedia, " +
                "p.nama_produk, " +
                "k.nama_komoditas, " +
                "kk.nama_kategori, " +
                "kldi.nama nama_kldi, " +
                "m.nama_manufaktur, " +
                "py.nama_penyedia, " +
                "p.penawaran_id, "+
                "us.nama_usulan, " +
                "k.kelas_harga, "+
                "pm.created_date ")
                .append("FROM produk_tunggu_setuju pm ")
                .append("LEFT JOIN produk p ON pm.produk_id = p.id ")
                .append("LEFT JOIN penyedia py on p.penyedia_id = py.id ")
                .append("LEFT JOIN komoditas k on k.id = p.komoditas_id ")
                .append("LEFT JOIN komoditas_kategori kk ON k.komoditas_kategori_id = kk.id ")
                .append("LEFT JOIN kldi on k.kldi_id = kldi.id ")
                .append("LEFT JOIN manufaktur m ON p.manufaktur_id = m.id ")
                .append("LEFT JOIN penawaran pen ON p.penawaran_id = pen.id ")
                .append("LEFT JOIN usulan us on pen.usulan_id = us.id ");

        if(AktifUser.getAktifUser().isPokja()){
            sb.append("LEFT JOIN usulan_pokja up on us.id = up.usulan_id ");
            sb.append(" WHERE up.pokja_id = ").append(search.uid);
            sb.append(" AND us.status = '").append(Usulan.STATUS_PENAWARAN_DIBUKA).append("'");
            sb.append(" AND NOT ( p.status = '").append(models.katalog.Produk.STATUS_MENUNGGU_PERSETUJUAN).append("' AND pen.status='").append(Penawaran.STATUS_MENUNGGU_VERIFIKASI).append("' )");
        }else{
            sb.append(" WHERE p.active = 1 ");
        }

        if (!TextUtils.isEmpty(search.keyword)) {
            sb.append(" AND (p.nama_produk LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%'");
            sb.append(" OR p.no_produk LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%'");
            sb.append(" OR p.no_produk_penyedia LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%'");
            sb.append(" OR us.nama_usulan LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%'");
            sb.append(" OR kldi.nama LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%'");
            sb.append(" OR m.nama_manufaktur LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%')");
        }

        if(search.pid != 0){
            sb.append(" AND pen.id = ").append(search.pid);
        }

        if (search.komoditas > 0) {
            sb.append(" AND k.id = ").append(search.komoditas);
        }

        if(!TextUtils.isEmpty(search.penyedia)){
            sb.append(" AND p.penyedia_id = ").append(search.penyedia);
        } else {
            sb.append(" AND p.status NOT IN ('DRAFT')");
        }

        Long total = ProductApprovalRepository.generateTotal(sb.toString());
        model.setTotal(total);

        if(!TextUtils.isEmpty(search.urutkan)){
            if(search.urutkan.equalsIgnoreCase("1")){
                sb.append(" ORDER BY p.nama_produk ASC ");
            }else if(search.urutkan.equalsIgnoreCase("2")){
                sb.append(" ORDER BY p.nama_produk DESC ");
            }else if(search.urutkan.equalsIgnoreCase("3")){
                sb.append(" ORDER BY k.nama_komoditas ASC ");
            }else if(search.urutkan.equalsIgnoreCase("4")){
                sb.append(" ORDER BY k.nama_komoditas DESC ");
            }else if(search.urutkan.equalsIgnoreCase("5")){
                sb.append(" ORDER BY py.nama_penyedia ASC ");
            }else if(search.urutkan.equalsIgnoreCase("6")){
                sb.append(" ORDER BY py.nama_penyedia DESC ");
            }else if(search.urutkan.equalsIgnoreCase("7")){
                sb.append(" ORDER BY pm.created_date ASC ");
            }else if(search.urutkan.equalsIgnoreCase("8")){
                sb.append(" ORDER BY pm.created_date DESC ");
            }
        }

        if (!model.isEmpty()) {
            sb.append(" LIMIT ").append(model.getMax()).append(" OFFSET ").append(model.getFrom());
            model.items = Query.find(sb.toString(), Produk.class).fetch();
        }
        return model;
    }

    private static Long generateTotal(String query) {
        Long total = Query.find("SELECT COUNT(id) FROM (" + query + ") p", Long.class).first();
        return total;
    }
}
