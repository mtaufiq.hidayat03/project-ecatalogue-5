package controllers.purchasing;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.Feedback;
import models.ServiceResult;
import models.api.Rup;
import models.chat.Chat;
import models.common.AktifUser;
import models.common.DokumenInfo;
import models.jcommon.util.CommonUtil;
import models.katalog.Komoditas;
import models.katalog.Produk;
import models.katalog.ProdukHarga;
import models.masterdata.*;
import models.penyedia.Penyedia;
import models.penyedia.PenyediaDistributor;
import models.prakatalog.Penawaran;
import models.purchasing.*;
import models.purchasing.form.PaketForm;
import models.purchasing.keranjangBelanja.CookieCart;
import models.purchasing.negosiasi.PaketProdukHeader;
import models.purchasing.negosiasi.PaketProdukNegoDetail;
import models.purchasing.riwayat.PaketRiwayat;
import models.purchasing.riwayat.PaketRiwayatNegosiasi;
import models.purchasing.search.DistributorSearch;
import models.purchasing.search.DistributorSearchResult;
import models.purchasing.search.PurchasingSearch;
import models.purchasing.search.PurchasingSearchResult;
import models.purchasing.status.PaketStatus;
import models.secman.Acl;
import models.user.User;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.Play;
import play.data.binding.As;
import play.data.validation.Error;
import play.data.validation.Validation;
import play.db.jdbc.Query;
import play.i18n.Lang;
import play.i18n.Messages;
import play.mvc.Http;
import repositories.paket.PackageProductRepository;
import repositories.paket.PackageRepository;
import services.produk.PaketSaveService;
import services.produk.ProdukService;
import utils.CurrencyUtil;
import utils.HtmlUtil;
import utils.KatalogUtils;
import utils.LogUtil;

import java.io.*;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static constants.paket.PaketStatusConstant.*;
import static models.purchasing.status.PaketStatus.*;


public class PaketCtr extends BaseController {

    public static final String TAG = "PaketCtr";

    @AllowAccess({Acl.COM_PAKET})
    public static void index() {
        PurchasingSearch query = new PurchasingSearch(params);
        PurchasingSearchResult model = new PurchasingSearchResult(query);

        if (params.all().size() > 1) {
            model = PackageRepository.searchPackage(query, AktifUser.getAktifUser());
        }
        List<Komoditas> commodities = Komoditas.getForDropDown();
        renderArgs.put("users", USERS);
        renderArgs.put("sorts", SORTS);
        renderArgs.put("negotiationStatuses", NEGOTIATION_STATUSES);
        renderArgs.put("paramQuery", query);

        if (getAktifUser().isPp() || getAktifUser().isPpk()) {
            for (Paket modelCheckHeader : model.items) {
                Logger.debug(modelCheckHeader.id + ":paket id");
                PaketProdukHeader paketProdukHeader = PaketProdukHeader.getLatestHeader(modelCheckHeader.id);
                if (null != paketProdukHeader) {
                    modelCheckHeader.setTotalRevisi(paketProdukHeader.revisi);
                }
            }
        }
        renderArgs.put("model", model);
        renderTemplate("purchasing/PaketCtr/index.html", commodities);
    }

    //  create paket dipindah di KeranjangBelanjaCtr
    @AllowAccess({Acl.COM_PAKET_ADD})
    public static void create() {
        render(); 
    }

    @AllowAccess({Acl.COM_PAKET_EDIT})
    public static void edit(String language, long paket_id) throws Exception {
//		paket?.panitia_user_id == aktifUser.user_id;
        try {
            AktifUser aktifUser = getAktifUser();
            Paket paket = Paket.findById(paket_id);
            notFoundIfNull(paket);
//			if(paket.panitia_user_id.longValue() != aktifUser.user_id.longValue()){
//				notFound();
//			}
            paket.withLatestStatus();
            paket.withAllowEdit(paket);

            if ((long) aktifUser.user_id != (long) paket.panitia_user_id) {
                Logger.debug("User %d mencoba mengakses edit paket %d tanpa hak", aktifUser.user_id, paket.id);
                forbidden();
                return;
            } else {
                if (paket.bolehEdit == false) {
                    Logger.debug("Akses edit paket sudah tidak diizinkan");
                    forbidden();
                    return;
                }
            }

            paket.withProducts();
            paket.withKurs();
            paket.withCommodity();
            paket.withKldi();
            paket.withPp();
            paket.withPpk();
            paket.withPenyedia();
            paket.withSumberDana();
            paket.withLatestStatus();

            PaketForm paketForm = populatePaketEditForm(paket);
            paketForm.instansi = paket.kldi.nama;
            Kabupaten satkerKab = paket.satuan_kerja_kabupaten == null ? null : Kabupaten.findById(paket.satuan_kerja_kabupaten);
            Provinsi satkerProv = satkerKab == null ? null : Provinsi.findById(satkerKab.provinsi_id);
            Kabupaten pengirimanKab = paket.pengiriman_kabupaten == null ? null : Kabupaten.findById(paket.pengiriman_kabupaten);
            Provinsi pengirimanProv = pengirimanKab == null ? null : Provinsi.findById(pengirimanKab.provinsi_id);
            List<SumberDana> sumberDanaList = SumberDana.find("active = 1").fetch();
            String urlReload = "/" + language + "/purchasing/paket/detail/" + paket_id;
            renderArgs.put("urlReload", urlReload);
            LogUtil.info(TAG, paket.products);
            LogUtil.info(TAG, paketForm);
            renderArgs.put("produkList", paket.products);
            renderArgs.put("totalHarga", paket.agr_konversi_harga_total);
            renderArgs.put("kuantitas", paket.agr_paket_produk);
            renderArgs.put("penyedia", paket.penyedia);
            renderArgs.put("komoditas", paket.commodity);
            renderArgs.put("paket", paketForm);
            renderArgs.put("detailpaket", paket);
            renderArgs.put("sumberDanaList", sumberDanaList);
            renderArgs.put("isEdit", true);
            renderArgs.put("kurs", paket.kurs);
            renderArgs.put("kurs_nilai", paket.kurs_nilai);
            renderArgs.put("kurs_tanggal", paket.kurs_tanggal);
            renderArgs.put("listProvinsi", Provinsi.findAllActive());

            renderArgs.put("satker_prov_id", satkerProv == null ? 0 : satkerProv.id);
            renderArgs.put("satker_kab_id", paket.satuan_kerja_kabupaten);
            renderArgs.put("pengiriman_prov_id", pengirimanProv == null ? 0 : pengirimanProv.id);
            renderArgs.put("pengiriman_kab_id", paket.pengiriman_kabupaten);
            renderArgs.put("listSatkerKab", satkerProv == null ? new ArrayList<Kabupaten>() : Kabupaten.findByProvId(satkerProv.id));
            renderArgs.put("listPengirimanKab", pengirimanProv == null ? new ArrayList<Kabupaten>() : Kabupaten.findByProvId(pengirimanProv.id));

            render("purchasing/PaketCtr/edit.html");
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            throw new Exception(e.getMessage());
        }
    }

    private static PaketForm populatePaketEditForm(Paket paket) {
        PaketForm paketForm = new PaketForm();

        //tab 2
        paketForm.paket_id = paket.id;
        Logger.info("paket_id nya " + paketForm.paket_id);
        paketForm.kode_paket = paket.no_paket;
        paketForm.komoditas_id = paket.komoditas_id;
        paketForm.nama_paket = paket.nama_paket;
        paketForm.tahun_anggaran = paket.tahun_anggaran;
        paketForm.jenis_instansi_id = paket.instansi_jenis_id;
        paketForm.instansi_id = paket.instansi_id;
        paketForm.alamat_satuan_kerja = paket.satuan_kerja_alamat;
        paketForm.satuan_kerja_kabupaten = paket.satuan_kerja_kabupaten;
        paketForm.pengiriman_alamat = paket.pengiriman_alamat;
        paketForm.pengiriman_kabupaten = paket.pengiriman_kabupaten;
        paketForm.npwp_satuan_kerja = paket.satuan_kerja_npwp;
        paketForm.rup_id = paket.rup_id;
        paketForm.satker_id = paket.satker_id;
        paketForm.kldi_jenis = paket.kldi_jenis;
        paketForm.kldi_id = paket.kldi_id;
        paketForm.nama_satuan_kerja = paket.satuan_kerja_nama;
        paketForm.kurs_id = paket.getKursId();

        //tab 3
        paketForm.id_pemesan = paket.userPp.id;
        paketForm.nama_pemesan = paket.userPp.nama_lengkap;
        paketForm.nip_pemesan = paket.panitia_nip;
        paketForm.jabatan_panitia = paket.panitia_jabatan;
        paketForm.email_pemesan = paket.panitia_email;
        paketForm.no_telp_pemesan = paket.panitia_no_telp;
        paketForm.no_sert_pbj_pemesan = paket.pp_sertifikat;

        //tab 4
        paketForm.nip_ppk = paket.ppk_nip;
        paketForm.id_ppk = paket.ppk_user_id;
        paketForm.nama_ppk = paket.buyer != null ? paket.buyer.nama_lengkap : null;
        paketForm.jabatan_ppk = paket.ppk_jabatan;
        paketForm.email_ppk = paket.ppk_email;
        paketForm.no_telp_ppk = paket.ppk_telepon;
        paketForm.no_sert_pbj_ppk = paket.ppk_sertifikat;

        //other
        paketForm.penyedia_id = paket.penyedia_id;
        paketForm.kurs_id = paket.kurs_id_from;

        int i = 0;
        paketForm.id_sumber_dana = new Long[paket.sumberDana.size()];
        paketForm.kode_anggaran = new String[paket.sumberDana.size()];
        for (PaketSumberDana ps : paket.sumberDana) {
            paketForm.id_sumber_dana[i] = ps.sumber_dana_id;
            paketForm.kode_anggaran[i] = ps.kode_anggaran;
            i++;
        }

        return paketForm;
    }

    private static boolean createPaketIsValid(PaketForm paket) {
        return true;
    }

    private static void reloadBuatPaket(long pid, PaketForm paket) { //pid = penyedia id
        List<Produk> produkList = new ArrayList<>();
        AktifUser aktifUser = getAktifUser();
        if(aktifUser != null) {
            Http.Cookie cookie = request.cookies.get(generateKey(aktifUser.user_id));
            Map<Long, Double> kuantitas = populateKuantitasKeranjang();
            Penyedia penyedia = Penyedia.findById(pid);
            Kurs kursUtama = Kurs.findKursUtama();
            List<SumberDana> sumberDanaList = SumberDana.find("active = 1").fetch();

            double totalHarga = 0;
            if (cookie != null) {
                JsonParser parser = new JsonParser();
                JsonArray jsonArray = parser.parse(URLDecoder.decode(cookie.value)).getAsJsonArray();
                for (int i = 0; i < jsonArray.size(); i++) {
                    JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                    Produk produk = ProdukService.getDetailProduk(jsonObject.get("produkId").getAsLong(), jsonObject.get("daerahId").getAsLong());
                    if (produk.penyedia_id != pid) {
                        continue;
                    }
                    totalHarga += (produk.harga_utama.multiply(new BigDecimal(kuantitas.get(produk.id))).doubleValue());
                    produkList.add(produk);
                }
            }
            if (produkList.size() > 0) {
                KursNilai kursNilai = KursNilai.getNewestKursByKursId(produkList.get(0).kurs_id);
                Komoditas komoditas = Komoditas.findById(produkList.get(0).komoditas_id);
                renderArgs.put("kursNilai", kursNilai);
                renderArgs.put("komoditas", komoditas);
            }

            Kabupaten satkerKab = paket.satuan_kerja_kabupaten == null ? null : Kabupaten.findById(paket.satuan_kerja_kabupaten);
            Provinsi satkerProv = satkerKab == null ? null : Provinsi.findById(satkerKab.provinsi_id);
            Kabupaten pengirimanKab = paket.pengiriman_kabupaten == null ? null : Kabupaten.findById(paket.pengiriman_kabupaten);
            Provinsi pengirimanProv = pengirimanKab == null ? null : Provinsi.findById(pengirimanKab.provinsi_id);

            if (paket.kode_paket.split(",").length > 1) {
                paket.kode_paket = paket.kode_paket.split(",")[0];
            }
            renderArgs.put("satker_prov_id", satkerProv == null ? 0 : satkerProv.id);
            renderArgs.put("satker_kab_id", paket.satuan_kerja_kabupaten);
            renderArgs.put("pengiriman_prov_id", pengirimanProv == null ? 0 : pengirimanProv.id);
            renderArgs.put("pengiriman_kab_id", paket.pengiriman_kabupaten);

            renderArgs.put("kursUtama", kursUtama);
            renderArgs.put("kuantitasProduk", kuantitas);
            renderArgs.put("produkList", produkList);
            renderArgs.put("totalHarga", totalHarga);
            renderArgs.put("penyedia", penyedia);
            renderArgs.put("kuantitas", kuantitas);
            renderArgs.put("paket", paket);
            renderArgs.put("sumberDanaList", sumberDanaList);

            renderArgs.put("listProvinsi", Provinsi.findAllActive());
            renderArgs.put("listSatkerKab", satkerProv == null ? new ArrayList<Kabupaten>() : Kabupaten.findByProvId(satkerProv.id));
            renderArgs.put("listPengirimanKab", pengirimanProv == null ? new ArrayList<Kabupaten>() : Kabupaten.findByProvId(pengirimanProv.id));
            render("purchasing/PaketCtr/create.html");
        }
    }

    private static void reloadEditPaket(PaketForm paketFormEdit) throws Exception {
        try {
            AktifUser aktifUser = getAktifUser();
            Paket paket = Paket.findById(paketFormEdit.paket_id);
            notFoundIfNull(paket);
            if (paket.panitia_user_id.longValue() != aktifUser.user_id.longValue()) {
                notFound();
            }
            paket.withProducts();
            paket.withKurs();
            paket.withCommodity();
            paket.withKldi();
            paket.withPp();
            paket.withPpk();
            paket.withPenyedia();
            paket.withSumberDana();

//			PaketForm paketForm = populatePaketEditForm(paket);
//			paketFormEdit.instansi = paket.kldi.nama;
            List<SumberDana> sumberDanaList = SumberDana.find("active = 1").fetch();

            renderArgs.put("produkList", paket.products);
            renderArgs.put("totalHarga", paket.agr_konversi_harga_total);
            renderArgs.put("kuantitas", paket.agr_paket_produk);
            renderArgs.put("penyedia", paket.penyedia);
            renderArgs.put("komoditas", paket.commodity);
            renderArgs.put("paket", paketFormEdit);
            renderArgs.put("sumberDanaList", sumberDanaList);
            renderArgs.put("isEdit", true);
            renderArgs.put("kurs", paket.kurs);
            renderArgs.put("kurs_nilai", paket.kurs_nilai);
            renderArgs.put("kurs_tanggal", paket.kurs_tanggal);

            render("purchasing/PaketCtr/edit.html");
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public static void createSubmit(PaketForm paket, String saveType, Long wId, Long kursId, Long komId) throws Exception {

        try {
            // populate paket dari form ke model paket
            AktifUser aktifUser = getAktifUser();
            Paket paketToSave = PaketSaveService.setReqs(paket, aktifUser, wId, kursId);

            validation.valid(paketToSave);
            if (Validation.hasErrors()) {
                LogUtil.debug(TAG, validation.errorsMap());
                reloadBuatPaket(paketToSave.penyedia.id, paket);
            }

            boolean isEdit = paket.paket_id != null;
            // paket produk
            ServiceResult<List<PaketProduk>> result = PaketSaveService.calculatePackageProduct(
                    paket,
                    wId,
                    paketToSave
            );
            if (!result.isSuccess()) {
                validation.addError("paketToSave.stok_" + result.getPayload()
                        .get(0).produk_id, result.getMessage());
                reloadBuatPaket(paketToSave.penyedia.id, paket);
            }
            List<PaketProduk> paketProdukList = result.getPayload();
            paketToSave.agr_konversi_harga_total = paketProdukList.stream()
                    .mapToDouble(PaketProduk::getKonversiHargaTotal)
                    .sum();

            LogUtil.debug(TAG, "total harga: ", paketToSave.agr_konversi_harga_total);

            ServiceResult checkedPriceResult = PaketSaveService.checkPriceAgainstUser(
                    aktifUser,
                    paketToSave.agr_konversi_harga_total
            );
            if (!checkedPriceResult.isSuccess()) {
                flash.error(checkedPriceResult.getMessage());
                reloadBuatPaket(paketToSave.penyedia.id, paket);
            }

            if (paket.id_sumber_dana == null || paket.id_sumber_dana.length == 0) {
                validation.addError("sumber_dana.label", Messages.get("paket.sumberdana.required"));
            }
            int errors = Validation.errors().size();
            if (errors > 0 && Play.mode.isDev()) {
                for (Error error : Validation.errors()) {
                    Logger.debug(error.message());
                }
            }
            if (errors > 0 || !createPaketIsValid(paket)) {
                LogUtil.debug(TAG, "*************************** ", validation.errors().get(0));
                String messages = "";
                for (Error error : Validation.errors()) {
                    messages += ", " + error.message();
                    Logger.debug(error.message());
                }
                validation.keep();
                params.flash();
                //cek data yang harus dilengkapi
                flash.error(Messages.get("paket.form.belum_lengkap") + " " + messages);
                reloadBuatPaket(paketToSave.penyedia.id, paket);
            }
            //long paketId = paketToSave.save();
            paketToSave.id = paket.paket_id;
            final Long id = Long.valueOf(paketToSave.save());
            if (paketToSave.id == null) {
                paketToSave.id = id;
            }
            // paket produk save
            PaketSaveService.savePaketProduk(paketProdukList, paketToSave.id);
            // end of paket produk save

            Paket.updateNopaketJob(paketToSave.id);

            // sumber dana
            PaketSaveService.saveSumberDana(paket, paketToSave.id);
            // end of sumber dana

            // angota ULP
            PaketSaveService.saveUlps(aktifUser, paket, paketToSave.id);
            // end of anggota ULP

            // paket negosiasi jika komoditas perlu nego
            long negoHeaderId = PaketSaveService.editMode(
                    isEdit, paketToSave.commodity, paketToSave.id, paketToSave.kursNilai, paketProdukList);
            // end of paket negosiasi

            // paket status
            PaketStatus paketStatus = PaketSaveService.savePaketStatus(
                    paketToSave.id,
                    aktifUser,
                    paketToSave.commodity,
                    paket,
                    negoHeaderId);
            // end of paket status

            // paket history
            PaketSaveService.savePaketRiwayat(paketToSave.id, paketStatus);

            PaketSaveService.updateProductStock(paketToSave, paket);

            Logger.debug("AKAN DELETE KERANJANG");
            Logger.debug("PENYEDIA_ID = " + paketToSave.penyedia_id);
            Logger.debug("wId = " + wId);
            deleteKeranjang(paketToSave.penyedia_id, wId, komId, paket);
            Logger.debug("SUDAH DELETE KERANJANG");

            //Paket pkt = Paket.findById(pak)

            Logger.debug("status paket = " + paketStatus.status_paket);
            if (paketStatus.status_paket.equals("persiapan")) {
                PaketSaveService.sendEmail(paketToSave, paket);
            }
            flash.success(Messages.get("paket.dibuat"));
            detail(paketToSave.id);

        } catch (Exception e) {
            LogUtil.error(TAG, e);
            throw new Exception("Terjadi kesalahan : " + e.getMessage());
        }
    }

    public static void automatedSaving(PaketForm paket, String saveType, Long wId, Long kursId, Long komId) {
        try {
            LogUtil.debug(TAG, paket);
            //Logger.info("paket.pengiriman_kabupaten");
            //Logger.info(""+paket.pengiriman_kabupaten);
            // populate paket dari form ke model paket
            AktifUser aktifUser = getAktifUser();
            if (aktifUser != null) {
                if (paket.isEdit.equals("yes")) {

                    Paket paketToSave = populatePaketEditForm(paket);
                    notFoundIfNull(paketToSave);

                    //paket ppk
                    if (aktifUser != null && aktifUser.isPpk()) {
                        paketToSave.ppk_user_id = aktifUser.user_id;
                        paketToSave.getPpkName();
                        paketToSave.ppk_email = paket.email_pemesan;
                        //paketToSave.ppk_jabatan = user.getJabatan(aktifUser.user_id);
                        paketToSave.ppk_jabatan = paket.jabatan_panitia;
                        paketToSave.ppk_sertifikat = paket.no_sert_pbj_pemesan;
                        paketToSave.ppk_nip = paket.nip_pemesan;
                        paketToSave.ppk_telepon = paket.no_telp_pemesan;
                    }

                    // get rup for validation
                    if (paketToSave.rup_id != null){
                        Rup rup = Rup.getRupForPaket(paketToSave.rup_id);
                        if (rup != null){
                            paketToSave.nama_paket = rup.nama;
                            paketToSave.tahun_anggaran = String.valueOf(rup.tahun_anggaran);
                            paketToSave.satker_id = String.valueOf(rup.id_sat_ker);
                            paketToSave.kldi_id = rup.kode_kldi;
                            paketToSave.satuan_kerja_nama = rup.nama_kldi;
                            paketToSave.kldi_jenis = rup.jenis_kldi;
                            paketToSave.provinsi_id = rup.prp_id;
                            paketToSave.kabupaten_id = rup.kbp_id;
                        }
                    }
                    //LogUtil.info(TAG, "paket edit =");
                    //LogUtil.info(TAG, paket);

                    paketToSave.save();
                    long paketId = paketToSave.id;

                    // paket produk and paket produk nego detail update dari data existings
                    if (paket.pengiriman_kabupaten != null) {
                        Paket checkPaket = Paket.findById(paketId);
                        Double agrKonversiHargaTotal = (double) 0;
                        List<PaketProduk> existings = PaketProduk.findIdPaketAndActive(paketId);
                        //Logger.info("data existings =" + existings.size());
                        //LogUtil.info(TAG, existings);
                        for (int i = 0; i < existings.size(); i++) {
                            if (!existings.isEmpty()) {
                                //Logger.info("get id paket produk = " + existings.get(i).produk_id);
                                //Logger.info("get id = " + existings.get(i).id);
                                Double ongkir = (double) 0;
                                try {
                                    Kabupaten pengirimanKab = paket.pengiriman_kabupaten == null ? null : Kabupaten.findById(paket.pengiriman_kabupaten);
                                    if (pengirimanKab != null) {
                                        //Logger.info("kabupaten id  = " + pengirimanKab.id);
                                        //Logger.info("provinsi id  = " + pengirimanKab.provinsi_id);
                                        ongkir = ProdukHarga.fixGetOngkosKirim(existings.get(i).produk_id, pengirimanKab.provinsi_id, pengirimanKab.id);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    throw new Exception("Terjadi kesalahan : " + e.getMessage());
                                }
                                //Logger.info("ongkirnya adalah = " + ongkir);
                                existings.get(i).konversi_harga_ongkir = ongkir;
                                existings.get(i).harga_ongkir = ongkir;
                                existings.get(i).harga_total = existings.get(i).kuantitas.doubleValue() * existings.get(i).harga_satuan + ongkir;
                                existings.get(i).konversi_harga_total = existings.get(i).harga_total;
                                // update agr total harga produk paket
                                agrKonversiHargaTotal += existings.get(i).konversi_harga_total;
                                existings.get(i).save();

                                PaketProdukNegoDetail.updateDataPaketProdukId(existings.get(i).id,
                                        existings.get(i).kuantitas, existings.get(i).harga_ongkir, existings.get(i).harga_total,
                                        existings.get(i).konversi_harga_satuan, existings.get(i).konversi_harga_ongkir,
                                        existings.get(i).konversi_harga_total, existings.get(i).id);
                            }
                        }
                        checkPaket.agr_konversi_harga_total = agrKonversiHargaTotal;
                        checkPaket.save();
                    }

                    // sumber dana
                    if (paket.id_sumber_dana != null) {
                        PaketSumberDana.deleteByPaketId(paketId);
                        for (int i = 0; i < paket.id_sumber_dana.length; i++) {
                            PaketSumberDana paketSumberDana = new PaketSumberDana();
                            if (!CommonUtil.isEmpty(paket.kode_anggaran[i])) {
                                paketSumberDana.paket_id = paketId;
                                paketSumberDana.sumber_dana_id = paket.id_sumber_dana[i];
                                paketSumberDana.kode_anggaran = paket.kode_anggaran[i];
                                paketSumberDana.save();
                            }
                        }
                        // end of sumber dana
                    }
                    renderJSON(new ServiceResult<>(true, Messages.get("paket.diedit"), paketToSave));
                } else {
                    //LogUtil.info(TAG, paket);
                    Paket paketToSave = PaketSaveService.setReqs(paket, aktifUser, wId, kursId);

                    //LogUtil.info(TAG, "paketToSave");
                    //LogUtil.info(TAG, paketToSave);
                    boolean isEdit = paket.paket_id != null;
                    // paket produk
                    ServiceResult<List<PaketProduk>> result = PaketSaveService.calculatePackageProduct(
                            paket,
                            wId,
                            paketToSave
                    );
                    //LogUtil.info(TAG, "payload = ");
                    //LogUtil.info(TAG, result.getPayload());
                    if (!result.isSuccess()) {
                        renderJSON(new ServiceResult<>(
                                false,
                                result.getMessage(),
                                "paketToSave.stok_" + result.getPayload().get(0).produk_id));
                    }
                    List<PaketProduk> paketProdukList = result.getPayload();
                    paketToSave.agr_konversi_harga_total = paketProdukList.stream()
                            .mapToDouble(PaketProduk::getKonversiHargaTotal)
                            .sum();

                    LogUtil.debug(TAG, "total harga: ", paketToSave.agr_konversi_harga_total);

                    ServiceResult checkedPriceResult = PaketSaveService.checkPriceAgainstUser(
                            aktifUser,
                            paketToSave.agr_konversi_harga_total
                    );
                    if (!checkedPriceResult.isSuccess()) {
                        renderJSON(new ServiceResult<>(checkedPriceResult.getMessage()));
                    }

                    int errors = Validation.errors().size();
                    if (errors > 0 && Play.mode.isDev()) {
                        for (Error error : Validation.errors()) {
                            Logger.debug(error.message());
                        }
                    }
                    if (errors > 0 || !createPaketIsValid(paket)) {
                        LogUtil.debug(TAG, "*************************** ", validation.errors().get(0));
                        String messages = "";
                        for (Error error : Validation.errors()) {
                            messages += ", " + error.message();
                            Logger.debug(error.message());
                        }
                        validation.keep();
                        params.flash();
                        //cek data yang harus dilengkapi
                        renderJSON(new ServiceResult<>(Messages.get("paket.form.belum_lengkap") + " " + messages));
                    }
                    //long paketId = paketToSave.save();
                    paketToSave.id = paket.paket_id;
                    Long id = Long.valueOf(paketToSave.save());
                    if (paketToSave.id == null) {
                        paketToSave.id = id;
                    }
                    //LogUtil.info(TAG, "paket produk list = ");
                    //LogUtil.info(TAG, paketProdukList);
                    // paket produk save
                    PaketSaveService.savePaketProduk(paketProdukList, paketToSave.id);
                    // end of paket produk save

                    Paket.updateNopaketJob(paketToSave.id);

                    // sumber dana
                    PaketSaveService.saveSumberDana(paket, paketToSave.id);
                    // end of sumber dana

                    // angota ULP
                    PaketSaveService.saveUlps(aktifUser, paket, paketToSave.id);
                    // end of anggota ULP

                    // paket negosiasi jika komoditas perlu nego
                    long negoHeaderId = PaketSaveService.editMode(
                            isEdit, paketToSave.commodity, paketToSave.id, paketToSave.kursNilai, paketProdukList);
                    // end of paket negosiasi

                    // paket status
                    PaketStatus paketStatus = PaketSaveService.savePaketStatus(
                            paketToSave.id,
                            aktifUser,
                            paketToSave.commodity,
                            paket,
                            negoHeaderId);
                    Logger.debug("status paket = " + paketStatus.status_paket);
                    // end of paket status

                    // paket history
                    PaketSaveService.savePaketRiwayat(paketToSave.id, paketStatus);

                    PaketSaveService.updateProductStock(paketToSave, paket);

                    Logger.debug("AKAN DELETE KERANJANG");
                    Logger.debug("PENYEDIA_ID = " + paketToSave.penyedia_id);
                    Logger.debug("wId = " + wId);
                    deleteKeranjang(paketToSave.penyedia_id, wId, komId, paket);
                    Logger.debug("SUDAH DELETE KERANJANG");
                    Logger.debug("status paket = " + paketStatus.status_paket);

                    if (paketStatus.status_paket.equals("persiapan")) {
                        PaketSaveService.sendEmail(paketToSave, paket);
                    }
                    renderJSON(new ServiceResult<>(true, Messages.get("paket.dibuat"), paketToSave));
                }
            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            renderJSON(new ServiceResult<>("Terjadi kesalahan : " + e.getMessage()));
        }
    }

    public static void editSubmit(PaketForm paket, String saveType) throws Exception {
        try {
            params.flash();
            AktifUser aktifUser = getAktifUser();
            KursNilai kursNilai = null;
            Penyedia penyedia = null;
            Komoditas komoditas = null;
            User user = new User();
            boolean isEdit = paket.paket_id != null;

            // populate paket dari form ke model paket
            Paket paketToSave = populatePaketEditForm(paket);
            notFoundIfNull(paketToSave);
            // get kurs
//			Logger.info("[Paket save] get kurs id: "+paketToSave.kurs_id_from + " - " + paketToSave.kurs_tanggal);
//			kursNilai = KursNilai.getByTanggalAndKurs(paketToSave.kurs_tanggal, paketToSave.kurs_id_from);
            // get data penyedia
            if (paket.penyedia_id != null) {
                penyedia = Penyedia.findById(paket.penyedia_id);
            }
            // get komoditas
            if (paket.komoditas_id != null) {
                komoditas = Komoditas.findById(paket.komoditas_id);
            }

//			paketToSave.agr_paket_produk = paket.produk_id != null ? paket.produk_id.length : 0;
//			paketToSave.agr_paket_produk_sudah_diterima_semua = 0;
//			paketToSave.agr_konversi_harga_total = new Double(0);

            //paket ppk
            if (aktifUser != null && aktifUser.isPpk()) {
                paketToSave.ppk_user_id = aktifUser.user_id;
                paketToSave.getPpkName();
                paketToSave.ppk_email = paket.email_pemesan;
                //paketToSave.ppk_jabatan = user.getJabatan(aktifUser.user_id);
                paketToSave.ppk_jabatan = paket.jabatan_panitia;
                paketToSave.ppk_sertifikat = paket.no_sert_pbj_pemesan;
                paketToSave.ppk_nip = paket.nip_pemesan;
                paketToSave.ppk_telepon = paket.no_telp_pemesan;
            }

            // get rup for validation
            if (paketToSave.rup_id != null){
                Rup rup = Rup.getRupForPaket(paketToSave.rup_id);
                if (rup != null){
                    paketToSave.nama_paket = rup.nama;
                    paketToSave.tahun_anggaran = String.valueOf(rup.tahun_anggaran);
                    paketToSave.satker_id = String.valueOf(rup.id_sat_ker);
                    paketToSave.kldi_id = rup.kode_kldi;
                    paketToSave.satuan_kerja_nama = rup.nama_kldi;
                    paketToSave.kldi_jenis = rup.jenis_kldi;
                    paketToSave.provinsi_id = rup.prp_id;
                    paketToSave.kabupaten_id = rup.kbp_id;
                }
            }

            validation.valid(paketToSave);
            // paket produk
//			List<PaketProduk> paketProdukList = new ArrayList<>();
//			for(int i=0; i < paket.produk_id.length; i++){
//				PaketProduk paketProduk = PaketProduk.findById(paket.paket_produk_id[i]);
//				paketProduk.kuantitas = paket.produk_kuantitas[i];
//				paketProduk.harga_total = paketProduk.kuantitas.doubleValue() * paketProduk.harga_satuan;
//				paketProduk.konversi_harga_total = paketProduk.harga_total * paketToSave.kurs_nilai;
//				paketProduk.catatan = paket.produk_catatan[i];
//				// agr total harga produk paket
//				paketToSave.agr_konversi_harga_total += paketProduk.konversi_harga_total;
//				paketProdukList.add(paketProduk);
//				if(!komoditas.apakah_stok_unlimited){
//					BigDecimal stok = Produk.cekStok(paketProduk.produk_id, paketProduk.kuantitas);
//					Produk produk = Produk.findById(paketProduk.produk_id);
//					if(stok.compareTo(paketProduk.kuantitas) < 0){
//						Validation.addError("paketToSave.stok_" + produk.id, Messages.get("paket.stok.kurang",
//								stok, paketProduk.kuantitas));
//					}
//				}
//			}

            if (paket.id_sumber_dana == null || paket.id_sumber_dana.length == 0) {
                Validation.addError("sumber_dana.label", Messages.get("paket.sumberdana.required"));

            }
            int errors = Validation.errors().size();
            if (errors > 0) {
                String messages = "";
                for (Error error : Validation.errors()) {
                    messages += ", " + error.message();
                    Logger.debug(error.message());
                }

                Validation.keep();

                flash.error(Messages.get("paket.form.belum_lengkap:" + messages));
                reloadEditPaket(paket);
            }
            paketToSave.save();
            long paketId = paketToSave.id;

            // paket produk save
//			PaketProduk.inactiveProdukByPaketId(paketId);
//			for(PaketProduk pp : paketProdukList){
//				PaketProduk ppOld = PaketProduk.findById(pp.id);
//				if(ppOld != null){
//					ppOld.kuantitas = pp.kuantitas;
//					ppOld.harga_satuan = pp.harga_satuan;
//					ppOld.harga_ongkir = pp.harga_ongkir;
//					ppOld.harga_total = pp.harga_total;
//					ppOld.konversi_harga_satuan = pp.konversi_harga_satuan;
//					ppOld.konversi_harga_ongkir = pp.konversi_harga_ongkir;
//					ppOld.konversi_harga_total = pp.konversi_harga_total;
//					ppOld.catatan = pp.catatan;
//					ppOld.active = true;
//					ppOld.save();
//					pp = ppOld;
//				} else {
//					pp.paket_id = paketId;
//					pp.id = Long.valueOf(pp.save());
//				}
//			}
            // end of paket produk save

            // sumber dana
            PaketSumberDana.deleteByPaketId(paketId);
            for (int i = 0; i < paket.id_sumber_dana.length; i++) {
                PaketSumberDana paketSumberDana = new PaketSumberDana();
                if (!CommonUtil.isEmpty(paket.kode_anggaran[i])) {
                    paketSumberDana.paket_id = paketId;
                    paketSumberDana.sumber_dana_id = paket.id_sumber_dana[i];
                    paketSumberDana.kode_anggaran = paket.kode_anggaran[i];
                    paketSumberDana.save();
                }
            }
            // end of sumber dana

            // angota ULP
            if (aktifUser != null && aktifUser.is_panitia_ulp && paket.ulp_anggota_id != null) {
                PaketAnggotaUlp.deleteByPaketId(paketId);
                for (int i = 0; i < paket.ulp_anggota_id.length; i++) {
                    long ulpUserId = paket.ulp_anggota_id[i];
                    String ulpNip = paket.ulp_anggota_nip[i];
                    PaketAnggotaUlp paketAnggotaUlp = new PaketAnggotaUlp();
                    paketAnggotaUlp.paket_id = paketId;
                    paketAnggotaUlp.anggota_user_id = ulpUserId;
                    paketAnggotaUlp.anggota_nip = ulpNip;
                    paketAnggotaUlp.active = true;
                    paketAnggotaUlp.save();
                }
            }
            // end of anggota ULP

            // paket negosiasi jika komoditas perlu nego
//			long negoHeaderId = -1;
//			if(komoditas.perlu_negosiasi_harga){
//				// nego detail
//				for(PaketProduk pp : paketProdukList){
//					if(!pp.active){
//						PaketProdukNegoDetail.inactiveByPaketProdukId(pp.id);
//					}
//				}
//			}
            // end of paket negosiasi

            // paket status
            PaketStatus paketStatus = PaketStatus.find("paket_id = ?", paketId).first();
            // end of paket status

            // paket history
            PaketRiwayat paketRiwayat = new PaketRiwayat();
            paketRiwayat.paket_id = paketId;
            paketRiwayat.deskripsi = PaketRiwayat.DESKRIPSI_PAKET_DIUBAH;
            if (paket.isDefaultDraft()) {
                paketStatus.status_paket = STATUS_PREPARATION;
            } else {
                paketStatus.status_paket = paket.status;
            }
            paketRiwayat.status_paket = paketStatus.getDetailStatus();
            paketStatus.save();
            paketRiwayat.save();

            flash.success(Messages.get("paket.diedit"));
            detail(paketId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Terjadi kesalahan : " + e.getMessage());
        }
    }

    @AllowAccess({Acl.COM_PAKET})
    public static void detail(Long paket_id) {
        LogUtil.d(TAG, "detail package");
        Paket model = new Paket();
        try {
            model = Paket.findById(paket_id);
            if (model != null) {
                model.withTotalNegotiations();
                model.withProducts();
                model.withSatker();
                model.withKldi();
                model.withPpk();
                model.withPp();
                model.withDistributor();
                model.withProvider(false);
                model.withTotalContractUploaded();
                model.withLatestStatus();
                model.withCommodity();
                model.withStatusMessage(getAktifUser());
                model.withSumberDana();
                model.withTransient();
                model.withAllowEdit(model);

                model.withPenyedia();
                // Logger.debug(new Gson().toJson(model.satker));

                //asepas: 06/02/2019; redudansi fungsi, lihat diatas model.withLatestStatus();
                model.withPaketStatus();
            }
            if (null == model) {
                forbidden();
            }
        } catch (Exception e) {
            Logger.error(e, "ADA EXCEPTION");
        }

        AktifUser aktifUser = getAktifUser();
        if (aktifUser.isPenyedia() && model.distributor != null) {
            if (aktifUser.isPenyedia() && model.provider.user_id.equals(model.distributor.user_id) && !aktifUser.user_id.equals(model.penyedia.getUserId())) {
                Logger.debug("Penyedia %d mencoba mengakses paket %d tanpa hak", aktifUser.user_id, model.id);
                forbidden();
                return;
            } else if (aktifUser.isPenyedia() && !aktifUser.user_id.equals(model.penyedia.getUserId()) && !aktifUser.user_id.equals(model.distributor.user_id)) {
                Logger.debug("Penyedia %d mencoba mengakses paket %d tanpa hak", aktifUser.user_id, model.id);
                forbidden();
                return;
            }
        } else {
            if (aktifUser.isPenyedia() && !aktifUser.user_id.equals(model.penyedia.getUserId())) {
                Logger.debug("Penyedia %d mencoba mengakses paket %d tanpa hak", aktifUser.user_id, model.id);
                forbidden();
                return;
            }
        }

        if (aktifUser.isPp() && model.getUserPp() != null && !aktifUser.user_id.equals(model.getUserPp().id)) {
            Logger.debug("PP %d mencoba mengakses paket %d tanpa hak", aktifUser.user_id, model.id);
            forbidden();
            return;
        }
        if (aktifUser.isPpk() && !aktifUser.user_id.equals(model.getBuyerId())) {
            Logger.debug("PPK %d mencoba mengakses paket %d tanpa hak", aktifUser.user_id, model.id);
            forbidden();
            return;
        }

        if (model.penyedia_distributor_id != null) {
            model.setIs_distributor(model.get_distributor_status(paket_id, getAktifUser()));

//			if (model.penyedia_distributor_id == aktifUser.user_id && model.penyedia_distributor_id == model.penyedia_id && model.penyedia_id == aktifUser.penyedia_id){
//				model.setIs_distributor(false);
//			}
        }

        //
        /*chat*/
        Chat chat;
        if (aktifUser.isPpk()) {
            chat = Chat.findByIdPaketPpk(aktifUser.user_id, paket_id);
        } else {
            chat = Chat.findByIdPengirimAndIdPaket(aktifUser.user_id, paket_id);
            if (chat == null) {
                chat = Chat.findByIdPenerimaAndIdPaket(aktifUser.user_id, paket_id);
            }
        }

        boolean chatCheck;
        if (chat != null) {
            chatCheck = true;
        } else {
            chatCheck = false;
        }

        if (model.latestStatus != null && model.latestStatus.status_paket != null && model.latestStatus.status_paket.contains("tolak")) {
            //model.withAllowApprovalWithRejectedStatus(model,aktifUser);
            //paket dibuat oleh pp
            if (!new Long(model.created_by).equals(model.ppk_user_id)) {
                if (model.latestStatus.status_paket.equalsIgnoreCase(STATUS_PROVIDER_REJECTED) && aktifUser.user_id.equals(model.panitia_user_id)) {
                    model.tampilkan_persetujuan_ketika_ditolak = true;
                }
                if (model.latestStatus.status_paket.equalsIgnoreCase(STATUS_PANITIA_REJECTED) && aktifUser.user_id.equals(model.penyedia.user_id)) {
                    model.tampilkan_persetujuan_ketika_ditolak = false;
                }
            } else { //paket dibuat oleh ppk
                if (model.latestStatus.status_paket.equalsIgnoreCase(STATUS_PROVIDER_REJECTED) && aktifUser.user_id.equals(model.ppk_user_id)) {
                    model.tampilkan_persetujuan_ketika_ditolak = true;
                }
                if (model.latestStatus.status_paket.equalsIgnoreCase(STATUS_PPK_REJECTED) && aktifUser.user_id.equals(model.penyedia.user_id)) {
                    model.tampilkan_persetujuan_ketika_ditolak = false;
                }
            }
        }

        renderArgs.put("paket", model);
        renderArgs.put("status", true);

        render(chat, chatCheck, paket_id);
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT})
    public static void riwayatPaket(Long paket_id) {
        Paket model = Paket.findById(paket_id);
        if (model != null) {
            model.withPackageHistories();
        }
        renderArgs.put("paket", model);
        renderArgs.put("status", false);
        render();
    }

    public static void updateLampiran() {

        render();
    }

    @AllowAccess({Acl.COM_PAKET_NEGOSIASI})
    public static void detailRiwayatNegosiasi(Long paket_id, Long id) {
        Paket model = Paket.findById(paket_id);
        if (model != null) {
            model.paketProduk = PackageProductRepository.getDetailProduk(id);
            model.paketProduk.withProductNegotiations();
            model.withCommodity();
        }
        Logger.debug(new GsonBuilder().setPrettyPrinting().create().toJson(model));
        renderArgs.put("paket", model);
        renderArgs.put("status", true);
        render("purchasing/PaketCtr/riwayatNegosiasi.html");
    }

    private static boolean hasPackageAccess(Paket paket) {
        AktifUser aktifUser = getAktifUser();
        paket.withPenyedia();
        if (aktifUser == null) {
            return false;
        }

        return (aktifUser.user_id.longValue() == paket.panitia_user_id.longValue()
                || aktifUser.user_id.longValue() == paket.ppk_user_id.longValue()
                || aktifUser.user_id.longValue() == paket.penyedia.user_id.longValue()
                || aktifUser.isAdmin());
    }

    private static boolean hasPackageSubmitAccess(Paket paket) {
        Boolean retval = false;
        AktifUser aktifUser = getAktifUser();
//		Logger.debug("aktifUser is null :"+(aktifUser==null));
//		Logger.debug("aktifUser is penyedia :"+(aktifUser.is_penyedia));
//		Logger.debug("aktifUser.user_id == paket.panitia_user_id :"+(aktifUser.user_id == paket.panitia_user_id.longValue()));
//		Logger.debug("aktifUser.user_id == paket.penyedia.user_id :"+(aktifUser.user_id == paket.penyedia.user_id.longValue()));
//		Logger.debug("paket status :"+(paket.paketStatus.to_status));
        if (aktifUser == null) {
            retval = false;
        }
        if ((aktifUser.user_id == paket.panitia_user_id.longValue()
                || aktifUser.user_id == paket.penyedia.user_id.longValue())) {
            if (aktifUser.is_penyedia) {
                retval = (paket.paketStatus.to_status != null && paket.paketStatus.to_status.equalsIgnoreCase(PaketStatus.STATUS_PENYEDIA));
            } else {
                Boolean panitiaIsPpk = false;

                if (paket.ppk_user_id.equals(new Long(paket.created_by))) {
                    panitiaIsPpk = true;
                }

                if (panitiaIsPpk) {
                    retval = (paket.paketStatus.to_status == null || paket.paketStatus.to_status.equalsIgnoreCase(PaketStatus.STATUS_PANITIA_PPK) || paket.paketStatus.to_status.equalsIgnoreCase(PaketStatus.STATUS_PANITIA_PAKET));
                } else {
                    retval = (paket.paketStatus.to_status == null || paket.paketStatus.to_status.equalsIgnoreCase(PaketStatus.STATUS_PANITIA) || paket.paketStatus.to_status.equalsIgnoreCase(PaketStatus.STATUS_PANITIA_PAKET));
                }

            }
        }
        return retval;
    }

    private static boolean isInNegotiation(Paket paket) {

        return paket.paketStatus.status_negosiasi.equalsIgnoreCase(PaketStatus.STATUS_NEGOSIASI_NEGOSIASI);
    }

    @AllowAccess({Acl.COM_PAKET_NEGOSIASI})
    public static void negosiasi(String language, long paket_id) {
        Paket paket = Paket.findById(paket_id);
        //paket.clearCacheOfClass(paket.getClass());
        boolean readonly = false;
//		boolean hasPackageSubmitAccess = false;
//		boolean isInNegotiation = false;
//		hasPackageSubmitAccess = hasPackageSubmitAccess(paket);
//		isInNegotiation = isInNegotiation(paket);
        if (paket == null) {
            String message = Messages.get("paket.negosiasi.notfound");
            notFound(message);
        }
        paket.withPaketStatus();
        if (!hasPackageAccess(paket)) {
            String message = Messages.get("paket.negosiasi.notfound");
            notFound(message);
        }

//		Logger.debug("hasPackageSubmitAccess:"+!hasPackageSubmitAccess(paket));
//		Logger.debug("isInNegotiation(paket):"+!isInNegotiation(paket));
//		Logger.debug("result IF:"+(!hasPackageSubmitAccess(paket) || !isInNegotiation(paket)));
        if (!hasPackageSubmitAccess(paket) || !isInNegotiation(paket)) {
            readonly = true;
        }
        paket.withProducts();
        paket.withCommodity();
        paket.withKurs();
        paket.withNegoHeader();

        renderArgs.put("paket", paket);
        renderArgs.put("readonly", readonly);
        render();
    }

    public static void riwayatNegosiasiProduk(Long paket_id) {
        Paket model = Paket.findById(paket_id);
        if (model != null) {
            model.withTotalNegotiations();
            model.withProductsHistory();
            model.withNegotiationHistory();
            //model.withCommodity();
            //model.withKurs();
            //model.withNegoHeader();
        }

        HashMap<Integer, PaketRiwayatNegosiasi> totalNego = new HashMap<Integer, PaketRiwayatNegosiasi>();
        for (PaketRiwayatNegosiasi prn : model.totalNegotiations) {
            totalNego.put(prn.revisi, prn);
        }

        HashMap<Long, PaketProduk> paketProduk = new HashMap<Long, PaketProduk>();
        for (PaketProduk pp : model.products) {
            paketProduk.put(pp.produk_id, pp);
        }

        renderArgs.put("paket", model);
        renderArgs.put("nego", totalNego);
        renderArgs.put("produk", paketProduk);
        renderArgs.put("status", false);
        render();
    }

    public static void negosiasiSubmit(String[] hargaSatuan, String[] hargaOngkir, String[] batasPengiriman, long[] produkIds, long[] paketProdukIds,
                                       long paket_id, String aksi, String[] catatan, String[] totalHarga) throws Exception {
        AktifUser aktifUser = getAktifUser();
        Paket paket = Paket.findById(paket_id);
        if (paket == null) {
            negosiasi(Lang.get(), paket_id);
        }
        Logger.debug("paket_id = %d", paket_id);
        Logger.debug("produkIds = %s", Arrays.toString(produkIds));
        Logger.debug("paketProdukIds = %s", Arrays.toString(paketProdukIds));
        Logger.debug("hargaSatuan = %s", Arrays.toString(hargaSatuan));

        String notifNego = cekTotalHargaProduk(totalHarga, paket);

//        PPK diizinkan nego dibawah 200jt
//        if (!notifNego.isEmpty() && !notifNego.equalsIgnoreCase("") && (aktifUser.isPp() || aktifUser.isPpk())) {
        if (!notifNego.isEmpty() && !notifNego.equalsIgnoreCase("") && (aktifUser.isPp())) {
            flash.error(notifNego.equalsIgnoreCase("ppk") ? Messages.get("peringatan_ppk_nego.label") : Messages.get("peringatan_pp_nego.label") );
            negosiasi(Lang.get(), paket_id);
        }

        List<PaketProduk> ppList = PaketProduk.find("paket_id = ? and active = ?", paket_id, true).fetch();
        for (int i = 0; i < ppList.size(); i++) {
            Date convBatasPengiriman = new SimpleDateFormat("dd-MM-yyyyy").parse(batasPengiriman[i]);
            ppList.get(i).batas_pengiriman = convBatasPengiriman;
//            ppList.get(i).catatan = catatan[i];
            ppList.get(i).save();
        }

        paket.withProducts();
        paket.withNegoHeader();
        paket.withPaketStatus();
        Logger.debug("paket.products = %s", paket.products.toString());

        //cek apakah punya otoriasisi
        if (!hasPackageAccess(paket) || !hasPackageSubmitAccess(paket)) {
            flash.error("Anda tidak memiliki akses untuk melakukan negosiasi.");
            negosiasi(Lang.get(), paket_id);
        }

        try {
            // update paket_produk
            List<PaketProduk> paketProdukList = PaketProduk.find("paket_id = ? and active = ?", paket_id, true).fetch();
            Map<Long, PaketProduk> paketProdukMap = paketProdukList.stream()
                    .collect(Collectors.toMap(pp -> pp.id, pp -> pp));
            double totalHargaUpdate = 0;
            boolean hargaIsUpdated = false;
            boolean catatanIsUpdated = false;
            boolean batasPengirimanUpdated = false;
            boolean adaHargaMelebihiKatalog = cekHargaProduk(paketProdukIds, hargaSatuan, paketProdukMap);
            if (adaHargaMelebihiKatalog) {
                flash.error(Messages.get("peringatan_harga_melebihi_katalog.label"));
                negosiasi(Lang.get(), paket_id);
            }

            if (aktifUser.isPp() && paket.agr_konversi_harga_total > 200000000) {
                flash.error("PP hanya bisa beli paket dibawah 200 juta. Untuk membeli barang diatas 200 juta harap hubungi PPK");
                negosiasi(Lang.get(), paket_id);
            }
            for (int i = 0; i < produkIds.length; i++) {
                long paketProdukId = paketProdukIds[i];
                long idProduk = produkIds[i];
                double hargaSatuanConverted = CurrencyUtil.formattedToDouble(hargaSatuan[i]);
                double hargaOngkirConverted = CurrencyUtil.formattedToDouble(hargaOngkir[i]);

                String catatanNew = catatan[i].replaceAll(" ","");
                String catatanNewLow = catatanNew.toLowerCase();

                PaketProduk paketProduk = paketProdukMap.get(paketProdukId);
                Logger.info("CATATAN PAKET PRODUK old " + paketProduk.catatan);
                Logger.info("CATATAN PAKET PRODUK " + catatanNewLow);
                Logger.info("CATATAN harga satuan old " + paketProduk.harga_satuan.doubleValue());
                Logger.info("hargaSatuanConverted new " + hargaSatuanConverted);
                String catatanOld = paketProduk.catatan.replaceAll(" ", "");
                String catatanOldLow = catatanOld.toLowerCase();

                if (paketProduk != null) {
                    Logger.info("[NEGOSIASI] update PaketProduk id :" + paketProdukId);

                    if (paketProduk.harga_satuan.doubleValue() == hargaSatuanConverted && paketProduk.harga_ongkir.doubleValue() == hargaOngkirConverted
                        && catatanOldLow.equalsIgnoreCase(catatanNewLow)) {
                        // untuk update harga total paket
                        totalHargaUpdate += paketProduk.konversi_harga_total;
                        continue;
                    }
                    Date convBatasPengiriman = new SimpleDateFormat("dd-MM-yyyyy").parse(batasPengiriman[i]);
                    paketProduk.harga_satuan = hargaSatuanConverted;
                    paketProduk.harga_ongkir = hargaOngkirConverted;
                    paketProduk.harga_total = (hargaSatuanConverted * paketProduk.kuantitas.doubleValue()) + hargaOngkirConverted;
                    paketProduk.konversi_harga_satuan = hargaSatuanConverted * paket.kurs_nilai;
                    paketProduk.konversi_harga_ongkir = hargaOngkirConverted * paket.kurs_nilai;
                    paketProduk.konversi_harga_total = paketProduk.harga_total * paket.kurs_nilai;
                    paketProduk.batas_pengiriman = convBatasPengiriman;
                    paketProduk.catatan = catatan[i];

                    // untuk update harga total paket
                    totalHargaUpdate += paketProduk.konversi_harga_total;
                    Logger.info("[NEGOSIASI] - Terdapat harga yang berubah");
                    hargaIsUpdated = true;
                    catatanIsUpdated = true;
                    batasPengirimanUpdated = true;
                    paketProduk.save();
                }
            }

            if (!hargaIsUpdated && !catatanIsUpdated && !aksi.equalsIgnoreCase("setuju")) {
                flash.success("Tidak ada update yang dilakukan.");
                negosiasi(Lang.get(), paket_id);
            }

            paket.agr_konversi_harga_total = totalHargaUpdate;
            paket.save();

            // paket nego header
            PaketProdukHeader currentNegoHeader = paket.negoHeader;
            PaketProdukHeader newNegoHeader = new PaketProdukHeader();
            boolean isUpdate = false;

            if (!hargaIsUpdated || !catatanIsUpdated || !currentNegoHeader.panitia_setuju && !currentNegoHeader.penyedia_setuju) {
                if (currentNegoHeader.revisi == 0) {
                    newNegoHeader = new PaketProdukHeader();
                    newNegoHeader.revisi = currentNegoHeader.revisi + 1;
                    isUpdate = false;
                } else {
                    newNegoHeader = currentNegoHeader;
                    isUpdate = true;
                }

                newNegoHeader.paket_id = currentNegoHeader.paket_id;
                //revisi keberapa tetap pada revisi sebelumnya karena sudah sepakat, jika belum sepakat + 1
                //pindah ke atas
                //newNegoHeader.revisi = currentNegoHeader.revisi + 1;

                newNegoHeader.kurs_id_from = currentNegoHeader.kurs_id_from;
                newNegoHeader.kurs_nilai = currentNegoHeader.kurs_nilai;
                newNegoHeader.kurs_tanggal = currentNegoHeader.kurs_tanggal;
                newNegoHeader.active = true;

                Logger.info("revisi update " + newNegoHeader.revisi.toString());
                Logger.info("status penyedia " + newNegoHeader.penyedia_setuju);
                Logger.info("status pp " + newNegoHeader.panitia_setuju);

            } else if (((currentNegoHeader.panitia_setuju && aktifUser.user_id != currentNegoHeader.panitia_setuju_user_id)
                    || (currentNegoHeader.penyedia_setuju && aktifUser.user_id != currentNegoHeader.penyedia_setuju_user_id))) {
                Logger.info("[NEGOSIASI] - Create Baru Nego Header");
                Logger.info("++ revisi sebelum update ++ " + currentNegoHeader.revisi.toString());
                newNegoHeader = new PaketProdukHeader();
                newNegoHeader.paket_id = currentNegoHeader.paket_id;
                newNegoHeader.revisi = currentNegoHeader.revisi + 1;
                newNegoHeader.kurs_id_from = currentNegoHeader.kurs_id_from;
                newNegoHeader.kurs_nilai = currentNegoHeader.kurs_nilai;
                newNegoHeader.kurs_tanggal = currentNegoHeader.kurs_tanggal;
                newNegoHeader.active = true;
                isUpdate = false;
                Logger.info("++ revisi setelah update ++ " + newNegoHeader.revisi.toString());
            } else {
                newNegoHeader = currentNegoHeader;
                isUpdate = true;
            }

            if (aksi.equalsIgnoreCase("setuju")) {
                Logger.info("[NEGOSIASI] aksi langsung SETUJU");
                if (aktifUser.is_penyedia) {
                    newNegoHeader.penyedia_setuju = true;
                    newNegoHeader.penyedia_setuju_user_id = aktifUser.user_id;
                    newNegoHeader.penyedia_setuju_tanggal = new Date();
                } else {
                    newNegoHeader.panitia_setuju = true;
                    newNegoHeader.panitia_setuju_user_id = aktifUser.user_id;
                    newNegoHeader.panitia_setuju_tanggal = new Date();
                }
                Logger.info("status penyedia " + newNegoHeader.penyedia_setuju);
                Logger.info("status pp " + newNegoHeader.panitia_setuju);
            } else {
                if (aktifUser.is_penyedia) {
                    newNegoHeader.penyedia_setuju = false;
                    newNegoHeader.penyedia_setuju_user_id = null;
                    newNegoHeader.penyedia_setuju_tanggal = null;
                } else {
                    newNegoHeader.panitia_setuju = false;
                    newNegoHeader.panitia_setuju_user_id = null;
                    newNegoHeader.panitia_setuju_tanggal = null;
                }
            }
            long idHeader = newNegoHeader.save();
            if (!isUpdate) {
                newNegoHeader.id = idHeader;
            }

            // nego detail
            PaketProdukNegoDetail.deleteByNegoHeaderId(newNegoHeader.id);
            //ambil lagi, update dari paket yang tersimpan di db.
            List<PaketProduk> paketProdukListUpdate = PaketProduk.find("paket_id = ? and active = ?", paket_id, true).fetch();
            Map<Long, PaketProduk> paketProdukMapUpdate = paketProdukListUpdate.stream()
                    .collect(Collectors.toMap(pp -> pp.id, pp -> pp));
            //
            for (PaketProduk pp : paketProdukMapUpdate.values()) {
                PaketProdukNegoDetail ppnd = new PaketProdukNegoDetail();
                ppnd.nego_header_id = newNegoHeader.id;
                ppnd.paket_produk_id = pp.id;
                ppnd.produk_id = pp.produk_id;
                ppnd.kuantitas = pp.kuantitas;
                ppnd.harga_satuan = pp.harga_satuan;
                ppnd.harga_ongkir = pp.harga_ongkir;
                ppnd.batas_pengiriman = pp.batas_pengiriman;
                ppnd.harga_total = pp.harga_total;
                ppnd.konversi_harga_satuan = pp.konversi_harga_satuan;
                ppnd.konversi_harga_ongkir = pp.konversi_harga_ongkir;
                ppnd.konversi_harga_total = pp.konversi_harga_total;
                ppnd.catatan = pp.catatan;
                ppnd.active = true;
                ppnd.save();
            }

            // paket status

            Boolean panitiaIsPpk = false;

            if (paket.ppk_user_id.equals(new Long(paket.created_by))) {
                panitiaIsPpk = true;
            }

            PaketStatus paketStatus = PaketStatus.find("paket_id = ?", paket.id).first();
            if (paketStatus != null) {
                paketStatus.nego_header_id = newNegoHeader.id;
                if (aksi.equalsIgnoreCase("setuju")) {
                    if (aktifUser.is_penyedia) {
                        paketStatus.from_status = PaketStatus.STATUS_PENYEDIA;
                        if (panitiaIsPpk) {
                            paketStatus.to_status = PaketStatus.STATUS_PANITIA_PPK;
                        } else {
                            paketStatus.to_status = PaketStatus.STATUS_PANITIA;
                        }
                    } else {
                        if (panitiaIsPpk) {
                            paketStatus.from_status = PaketStatus.STATUS_PANITIA_PPK;
                        } else {
                            paketStatus.from_status = PaketStatus.STATUS_PANITIA;
                        }
                        paketStatus.to_status = PaketStatus.STATUS_PENYEDIA;
                    }
                }
                if (newNegoHeader.penyedia_setuju && !newNegoHeader.panitia_setuju) {
                    paketStatus.status_negosiasi = PaketStatus.STATUS_NEGOSIASI_NEGOSIASI;
                } else if (!newNegoHeader.penyedia_setuju && newNegoHeader.panitia_setuju) {
                    paketStatus.status_negosiasi = PaketStatus.STATUS_NEGOSIASI_NEGOSIASI;
                } else if (newNegoHeader.penyedia_setuju && newNegoHeader.panitia_setuju) { //setuju nego
                    if (panitiaIsPpk) {
                        if (paketStatus.from_status == PaketStatus.STATUS_PANITIA_PPK) {
                            paketStatus.to_status = PaketStatus.STATUS_PANITIA_PPK;
                            paketStatus.status_paket = PaketStatus.STATUS_PERSIAPAN;
                            /*paketStatus.status_paket = PaketStatus.STATUS_PAKET_PPK_SETUJU;*/
                        } else if (paketStatus.from_status == PaketStatus.STATUS_PENYEDIA) {
                            paketStatus.to_status = PaketStatus.STATUS_PANITIA_PPK;
                            paketStatus.status_paket = PaketStatus.STATUS_PERSIAPAN;
                            /*paketStatus.status_paket = PaketStatus.STATUS_PAKET_PENYEDIA_SETUJU;*/
                        }
                    }else{ //Paksa posisi paket terakhir ketika negosiasi sepakat ada di PANITIA jika yang buat paket adalah PP
                        paketStatus.to_status = PaketStatus.STATUS_PANITIA;
                    }
                    paketStatus.status_negosiasi = PaketStatus.STATUS_NEGOSIASI_SEPAKAT;
                }
                paketStatus.save();
            }

            /**
             * Insert Paket Riwayat
             **/
            boolean panitiaPPK = false;
            Logger.info("panitia user id " + paket.panitia_user_id);
            Logger.info("ppk id " + paket.ppk_user_id);
            if (paket.panitia_user_id.toString().equals(paket.ppk_user_id.toString())) {
                panitiaPPK = true;
            }

            PaketRiwayat paketRiwayat = new PaketRiwayat();
            paketRiwayat.paket_id = paket_id;
            paketRiwayat.deskripsi = "Menyetujui negosiasi (rev. " + newNegoHeader.revisi.toString() + ")";
            if (aktifUser.isPp()) {
                paketRiwayat.status_paket = "panitia_setuju-panitia-penyedia";
            } else if (aktifUser.isPpk()) {
                paketRiwayat.status_paket = "ppk_setuju_baru-ppk-penyedia";
            } else if (panitiaPPK) {
                paketRiwayat.status_paket = "penyedia_setuju-penyedia-ppk";
            } else {
                paketRiwayat.status_paket = "penyedia_setuju-penyedia-panitia";
            }
            paketRiwayat.save();

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }

        //negosiasi(Lang.get(), paket_id);
        detail(paket_id);
    }

    public static boolean cekHargaProduk(long[] paketProdukIds, String[] hargaSatuan, Map<Long, PaketProduk> paketProdukMap) throws Exception {
        boolean adaHargaMelebihiKatalog = false;
        for (int i = 0; i < paketProdukIds.length; i++) {
            long paketProdukId = paketProdukIds[i];
            double hargaSatuanConverted = CurrencyUtil.formattedToDouble(hargaSatuan[i]);
            PaketProduk paketProduk = paketProdukMap.get(paketProdukId);

            if (paketProduk != null) {
                Produk produk = Produk.findById(paketProduk.produk_id);
//				List<KomoditasAtributHarga> atributHargaList = KomoditasAtributHarga.findByKomoditas(produk.komoditas_id);
//				List<ProdukHarga> produkHarga = ProdukHarga.findRiwayatByProdukId(paketProduk.produk_id);
//
//				if (produk.harga_utama.longValue() <= 0){
//					Map<String, List<ProdukHarga>> groupHarga = produkHarga.stream()
//							.collect(
//									Collectors.groupingBy(
//											x -> x.getStandarDateString()
//									)
//							);
//					NavigableMap<String, List<ProdukHarga>> gh = new TreeMap<>(groupHarga).descendingMap();
//					Logger.info("GH " + gh);
//					Logger.info("Grup Harga " + groupHarga);

//					KomoditasAtributHarga kah = atributHargaList.stream().parallel().filter(it -> it.apakah_harga_utama == true).findFirst().orElse(new KomoditasAtributHarga());
//					ProdukHarga ph = gh.firstEntry().getValue().stream().parallel().filter(it -> it.komoditas_harga_atribut_id.equals(kah.id)).findFirst().orElse(new ProdukHarga());
//					List<ProdukHargaJson> hargaProdukJson = new Gson().fromJson(ph.harga, new TypeToken<List<ProdukHargaJson>>(){}.getType());
//
//					ProdukHargaJson hkj = hargaProdukJson.stream()
//							.findFirst().orElse(new ProdukHargaJson());
//					produk.harga_utama = new BigDecimal(hkj.harga);

//				}

//					tambahan omen
                PaketProdukNegoDetail paketProdukNegoDetail = new PaketProdukNegoDetail();
//                ambil harga satuan dari riwayat nego rev 0
                Double hargaProdukSatuan = paketProdukNegoDetail.getHargaAwal(paketProdukId);
                produk.harga_utama = BigDecimal.valueOf(hargaProdukSatuan.doubleValue());

                Logger.info("HARGA UTAMA " + produk.harga_utama.doubleValue());
                Logger.info("HARGA SATUAN " + hargaSatuanConverted);

                if (produk.harga_utama.doubleValue() < hargaSatuanConverted)
                    adaHargaMelebihiKatalog = true;
            }
        }
        return adaHargaMelebihiKatalog;
    }

    public static String cekTotalHargaProduk(String[] totalHarga, Paket paket) throws Exception {
        String notifikasiNego = "";
        double totalNegoAkhir = 0;
        double batasTotalHarga = 200000000.1; //200.000.000 200Jt
        for (int i = 0; i < totalHarga.length; i++) {
            double totalHargaConverted = CurrencyUtil.formattedToDouble(totalHarga[i]);
            totalNegoAkhir = totalNegoAkhir + totalHargaConverted;
        }

//        PPK diizinkan nego dibawah 200jt
//        if (paket.created_by.equals(Integer.parseInt(paket.ppk_user_id.toString())) && paket.panitia_user_id.equals(paket.ppk_user_id)){ //ppk pembuat paket
//            if (totalNegoAkhir < batasTotalHarga){
//                notifikasiNego = "ppk";
//            }
//        }

        if (paket.created_by.equals(Integer.parseInt(paket.panitia_user_id.toString())) && !paket.panitia_user_id.equals(paket.ppk_user_id)) { //pp pembuat paket
            if (totalNegoAkhir > batasTotalHarga){
                notifikasiNego = "pp";
            }
        }

        return notifikasiNego;
    }

    public static void infoPenyedia(Long paket_id, Long id) {
        Paket model = Paket.findById(paket_id);
        List<DokumenInfo> dokumenUkmList = new ArrayList<>();
        List<Penawaran> komoditasList = new ArrayList<>();
        if (model != null) {
            model.withProvider(false);
            if (model.provider != null) {
                model.provider.withRepresentatives();
                model.provider.withDistributors();
                dokumenUkmList = DokumenInfo.listDokumenUkm(model.provider.id);
                komoditasList = Penawaran.infoKomoditasPenyedia(model.provider.id);
            }
        }
        renderArgs.put("paket", model);
        render("purchasing/PaketCtr/infoPenyedia.html",dokumenUkmList,komoditasList);
    }

    public static void representative(Long paket_id, Long id) {
        Paket model = Paket.findById(paket_id);
        if (model != null) {
            model.withProvider(false);
            if (model.provider != null) {
                model.provider.withRepresentatives();
            }
        }
        renderArgs.put("paket", model);
        render("purchasing/PaketCtr/representatif.html");
    }

    public static void infoDistributor(Long paket_id, Long id) {
        Paket model = Paket.findById(paket_id);
        if (model != null) {
            model.withDistributor();
            if (model.distributor != null) {
                model.distributor.withRepresentatives();
            }
            PenyediaDistributor penyediaDistributor = PenyediaDistributor.getUserDistributorById(id);
            model.distributor.nama_penyedia = penyediaDistributor.nama_penyedia;
            model.distributor.username = penyediaDistributor.username;
        }
        renderArgs.put("paket", model);
        render("purchasing/PaketCtr/infoDistributor.html");
    }

    public static void distributorRepresentative(Long paket_id, Long id) {
        Paket model = Paket.findById(paket_id);
        if (model != null) {
            model.withDistributor();
            if (model.distributor != null) {
                model.distributor.withRepresentatives();
            }
        }
        renderArgs.put("paket", model);
        render("purchasing/PaketCtr/distributorRepresentatif.html");
    }

    public static void productList(Long paket_id) {
        Paket model = Paket.findById(paket_id);
        boolean allowDeleteProduk = false;
        if (model != null) {
            model.withProducts();
            model.withNegoHeader();
            model.withCommodity();
            model.withLatestStatus();
        }
        if (model.commodity.perlu_negosiasi_harga && (model.negoHeader.panitia_setuju == false || model.negoHeader.penyedia_setuju == false) && (model.latestStatus.status_paket.equalsIgnoreCase(STATUS_PAKET_PERSIAPAN)
        || model.latestStatus.status_paket.equalsIgnoreCase(STATUS_PAKET_PANITIA_SETUJU)
                || model.latestStatus.status_paket.equalsIgnoreCase(STATUS_PAKET_PENYEDIA_TOLAK))){
            allowDeleteProduk = true;
        }
        renderArgs.put("paket", model);
        renderArgs.put("allowDelete", allowDeleteProduk);
        render("purchasing/PaketCtr/listProduk.html");
    }

    public static void setDistributor(Long paket_id) {
        DistributorSearch query = new DistributorSearch(params);
        if(paket_id==null){
            paket_id = query.pktid;
        }
        Paket model = Paket.findById(paket_id);
        DistributorSearchResult distributor = new DistributorSearchResult(query);
        query.pktid = paket_id;
        query.pid = model.penyedia_id;
        distributor = PackageRepository.searchDistributor(query);

        if (model != null) {
            model.withProvider(true);
        }
        renderArgs.put("paramQuery", query);
        renderArgs.put("paket", model);
        render("purchasing/PaketCtr/distributor.html", distributor);
    }

    @AllowAccess({Acl.COM_PAKET})
    public static void storeDistributor(Long paket_id, Long distributor_id) {
        checkAuthenticity();

        Paket model = Paket.findById(paket_id);
        model.withLatestStatus();

        Boolean panitiaIsPpk = false;
        if (model.ppk_user_id.equals(new Long(model.created_by))) {
            panitiaIsPpk = true;
        }

        if (panitiaIsPpk && model.penyedia_distributor_id == null) {
            PaketStatus paketStatus = model.latestStatus;
            String status_negosiasi = "";
            String to_status = "distributor";
            if (paketStatus != null) {
                status_negosiasi = paketStatus.status_negosiasi;

                if (status_negosiasi == null) { //status paket non nego
                    if (paketStatus.status_paket.equalsIgnoreCase(PaketStatus.STATUS_PAKET_PENYEDIA_SETUJU)) {
                        paketStatus.to_status = to_status;
                        paketStatus.save();
                    }
                } else { //status paket nego
                    if (status_negosiasi.equalsIgnoreCase(PaketStatus.STATUS_NEGOSIASI_SEPAKAT)
                            && paketStatus.status_paket.equalsIgnoreCase(PaketStatus.STATUS_PAKET_PENYEDIA_SETUJU)) {
                        paketStatus.to_status = to_status;
                        paketStatus.save();
                    }
                }
            }
        }

        if (distributor_id != null) {
            if (model != null) {
                PenyediaDistributor distributor = model.checkDistributor(distributor_id);
                if (distributor != null) {
                    model.penyedia_distributor_id = distributor_id;
                    model.save();

                    new PaketRiwayat().addDistributorHistory(model.id, distributor);
                    //saving chat
                    Chat chat = Chat.findByIdPaket(paket_id);
                    if (null != chat) {
                        chat.penyedia_distributor_id = distributor_id;
                        chat.save();
                    }
                }
            }
        }
        detail(paket_id);
    }

//	int komoditas_id, long penyedia_id, long rup_id, String no_paket, String nama_paket, String satuan_kerja_nama,
//	String satuan_kerja_alamat, String satuan_kerja_npwp, long panitia_user_id, String panitia_email, String panitia_no_telp,
//	long ppk_user_id, String ppk_jabatan, String ppk_nip, long kurs_id_from, Double kurs_nilai, Date kurs_tanggal,
//	int agr_paket_produk, Double agr_konversi_harga_total, String panitia_nip, String ppk_email, String ppk_telepon,
//	String satker_id, String kldi_jenis, String kldi_id, String tahun_anggaran, String ppk_sertifikat, String pp_sertifikat

    private static Paket populatePaketEditForm(PaketForm paketForm) {
        Paket paketPopulated = Paket.findById(paketForm.paket_id);
        if (paketPopulated != null) {
            paketPopulated.rup_id = paketForm.rup_id == null ? paketPopulated.rup_id : paketForm.rup_id;
            paketPopulated.satuan_kerja_npwp = paketForm.npwp_satuan_kerja;
            paketPopulated.satuan_kerja_alamat = paketForm.alamat_satuan_kerja;
            paketPopulated.satuan_kerja_kabupaten = paketForm.satuan_kerja_kabupaten;
            if (paketForm.gunakan_alamat_satker != null) {
                paketPopulated.pengiriman_alamat = paketForm.alamat_satuan_kerja;
                paketPopulated.pengiriman_kabupaten = paketForm.satuan_kerja_kabupaten;
            } else {
                paketPopulated.pengiriman_alamat = paketForm.pengiriman_alamat;
                paketPopulated.pengiriman_kabupaten = paketForm.pengiriman_kabupaten;
            }

            paketPopulated.panitia_email = paketForm.email_pemesan;
            paketPopulated.panitia_no_telp = paketForm.no_telp_pemesan;
            paketPopulated.panitia_nip = paketForm.nip_pemesan;
            paketPopulated.pp_sertifikat = paketForm.no_sert_pbj_pemesan;
            paketPopulated.panitia_jabatan = paketForm.jabatan_panitia;

            if (!getAktifUser().isPpk()) {
                paketPopulated.ppk_user_id = paketForm.id_ppk;
                paketPopulated.ppk_jabatan = paketForm.jabatan_ppk;
                paketPopulated.ppk_nip = paketForm.nip_ppk;
                paketPopulated.ppk_email = paketForm.email_ppk;
                paketPopulated.ppk_telepon = paketForm.no_telp_ppk;
                paketPopulated.ppk_sertifikat = paketForm.no_sert_pbj_ppk;
            }
        }

        return paketPopulated;
    }

    public static void approve(Long paket_id, String from, String to, String message) {
        checkAuthenticity();
        LogUtil.d(TAG, "approve");
        Paket model = storeApproval(paket_id, from, to, true, true, message);
        redirect(model.getDetailUrl());
    }

    public static void reject(Long paket_id, String from, String to, String message) {
        checkAuthenticity();
        LogUtil.d(TAG, "reject");
        Paket model = storeApproval(paket_id, from, to, true, false, message);
        redirect(model.getDetailUrl());
    }

    private static Paket storeApproval(Long paket_id, String from, String to,
                                       boolean isApproval, boolean isApproved, String message) {
        LogUtil.d(TAG, "approval");
        Paket model = Paket.findById(paket_id);
        model.withLatestStatus();

        Boolean panitiaIsPpk = false, ditolak = false;
        String statusPaket = "";

        if (model.ppk_user_id.equals(new Long(model.created_by))) {
            panitiaIsPpk = true;
        }

        if (model != null) {
            if (isApproval){ //tidak butuh kirim paket ke ppk
                if (getAktifUser().isPpk()){ //login ppk selaku ppk atau panitia
                    if (panitiaIsPpk) { //paket dibuat oleh ppk
                        statusPaket = isApproved ? STATUS_PANITIA_APPROVED: STATUS_PANITIA_REJECTED;
                        from = STATUS_PANITIA;
//                        if (model.penyedia_distributor_id != null) {
//                            to = "distributor";
//                        } else {
//                            to = "penyedia";
//                        }
                        to = isApproved ? STATUS_PENYEDIA : STATUS_PPK;
                    } else { //paket dibuat oleh pp
                        statusPaket = isApproved ? STATUS_PPK_APPROVED: STATUS_PPK_REJECTED;
                        from = STATUS_PPK;
                        to = isApproved ? STATUS_DISTRIBUTOR : STATUS_PANITIA;
                    }
                }

                if (getAktifUser().isPp()) { //login pp selaku panitia
                    statusPaket = isApproved ? STATUS_PANITIA_APPROVED : STATUS_PANITIA_REJECTED;
                    from = STATUS_PANITIA;
                    to = isApproved ? STATUS_PENYEDIA : STATUS_PANITIA;
                }

                if (getAktifUser().isProvider() || getAktifUser().isPenyedia()){ //login penyedia selaku penyedia
                    statusPaket = isApproved ? STATUS_PROVIDER_APPROVED : STATUS_PROVIDER_REJECTED;
                    from = STATUS_PENYEDIA;
                    if (panitiaIsPpk) { //paket dibuat oleh ppk
                        if (model.penyedia_distributor_id != null) {
                            to = isApproved ? STATUS_DISTRIBUTOR : STATUS_PPK;
                        } else {
                            to = isApproved ? STATUS_PENYEDIA : STATUS_PPK;
                        }
                    } else { //paket dibuat oleh pp
                        to = STATUS_PANITIA;
                    }
                }

                ditolak = isApproved ? false : true; //setuju atau tolak paket

                model.latestStatus.di_tolak = ditolak;
                model.latestStatus.deskripsi = message;

            } else { //butuh kirim paket ke ppk
                statusPaket = STATUS_PROVIDER_APPROVED;
                from = STATUS_PANITIA_PAKET;
                to = STATUS_PPK;
            }

            model.latestStatus.status_paket = statusPaket;
            model.latestStatus.to_status = to;
            model.latestStatus.from_status = from;
            model.latestStatus.save();

            if (isApproval) {
                String user = "";
                AktifUser au = AktifUser.getAktifUser();
                boolean isPPK = au.isPpk();
                boolean ppkIsPanitia = au.getUser().id.equals(model.panitia_user_id) ? true : false;
                if (isPPK && ppkIsPanitia == true){
                    user = "ppk";
                } else {
                    user = from;
                }
                new PaketRiwayat().addApprovalHistory(model.latestStatus, user, isApproved);
            } else {
                new PaketRiwayat().addSendToPpk(paket_id);
            }
        }

        return model;
    }

    public static void sendToPpk(Long paket_id) throws Exception {
        Paket model = Paket.findById(paket_id);
        if (model!=null) { //jika modelnya tidak kosong
            model.withLatestStatus();

            model.latestStatus.status_paket = STATUS_PROVIDER_APPROVED;
            model.latestStatus.to_status = STATUS_PANITIA_PPK;
            model.latestStatus.from_status = STATUS_PANITIA_PAKET;
            try {
                int saveStatus = model.latestStatus.save();

                if (saveStatus>0){ //jika sukses simpan status
                    model.withLatestStatus();
                    if (model.latestStatus.to_status.equals(STATUS_PANITIA_PPK) && model.latestStatus.from_status.equals(STATUS_PANITIA_PAKET)){
                        //Jika Posisi paket sudah tepat di ppk
                        new PaketRiwayat().addSendToPpk(paket_id);

                        flash.success("Paket berhasil dikirim ke PPK!");
                    } else {
                        //jika posisi paket masih belum di ppk maka rollback status kembali ke awal dan munculkan notif gagal mengirim paket ke ppk
                        model.latestStatus.status_paket = STATUS_PROVIDER_APPROVED;
                        model.latestStatus.to_status = STATUS_PANITIA;
                        model.latestStatus.from_status = STATUS_PENYEDIA;
                        model.latestStatus.save();

                        flash.error(Messages.get("Gagal mengirimkan paket ke PPK!"));
                    }

                } else { //jika tidak sukses simpan status
                    flash.error(Messages.get("Gagal mengirimkan paket ke PPK!"));
                }
            } catch (Exception e) {
                flash.error(Messages.get("Gagal mengirimkan paket ke PPK!"));
            }
        } else { //jika modelnya kosong
            flash.error(Messages.get("Paket tidak ditemukan!"));
        }

        //Paket model = storeApproval(paket_id, "panitia", "ppk", false, true, "");
        redirect(model.getDetailUrl());
    }

    public static void finishingPackage(Long paket_id, PaketRating rating) {
        checkAuthenticity();
        Paket model = Paket.findById(paket_id);
        Logger.debug(new Gson().toJson(rating));
        if (model != null && rating != null) {
            model.withStatusMessage(getAktifUser());
            rating.packageId = model.getId();
            Logger.debug("status message " + model.statusMessage.isFinishingPackage());
            if (model.latestStatus != null
                    && model.isUserAllowApproval(getAktifUser())
                    && model.statusMessage.isFinishingPackage()) {
                model.latestStatus.updateStatusToFinish();
                model.setFromRating(rating)
                        .setFinished()
                        .save();
                new PaketRiwayat().addPackageFinished(model);
                List<Feedback> feedbacks = rating.getFeedBack();
                if (!feedbacks.isEmpty()) {
                    for (Feedback feedback : feedbacks) {
                        feedback.save();
                    }
                    new Feedback().setFromProvider(model.getProvider(), (int) rating.getAverageRating()).save();
                }
            } else {
                Logger.debug("not allowed or latest status is null or has not met all the requirements");
            }
        }
        redirect(model.getDetailUrl());
    }

    public static void cancelPackage(Long paket_id, Paket paket) {
//		checkAuthenticity();
        Paket model = Paket.findById(paket_id);
        Komoditas komoditas = new Komoditas();
        List<PaketProduk> paketProduks = new ArrayList<>();

        if (model != null) {
            model.withStatusMessage(getAktifUser());
            model.withProducts();
            paketProduks = model.products;

            if (model.latestStatus != null) {
                model.latestStatus.updateStatusCancel();
                new PaketRiwayat().addCancelHistory(model);
                model.apakah_batal = 1;
                model.dibatalkan_oleh = AktifUser.getActiveUserId();
                java.util.Date currentDate = DateUtils.truncate(new java.util.Date(), Calendar.DATE);
                model.tanggal_batal = currentDate;
                model.alasan_batal = paket.alasan_batal;
                model.save();

                // get komoditas
                if (model.komoditas_id != null) {
                    komoditas = Komoditas.findById(model.komoditas_id);

                    for (PaketProduk produk : paketProduks) {
                        Long produk_id = produk.produk_id;
                        BigDecimal kuantitas = produk.kuantitas;

                        if (!komoditas.apakah_stok_unlimited) {
                            //update stok produk
                            Produk produks = new Produk();
                            produks.updateStokProduk(kuantitas, produk_id,"+");
                        }
                    }
                }

                flash.success(Messages.get("notif.success.batal"));
            }
        }
        redirect(model.getDetailUrl());
    }

    public static void ulpList(Long paket_id) {
        Paket model = Paket.findById(paket_id);
        notFoundIfNull(model);
        model.withUlpMembers();
        renderArgs.put("paket", model);
        render("purchasing/PaketCtr/ulpList.html");
    }

    @AllowAccess({Acl.COM_USER_GRUP_ROLE_ADD, Acl.COM_USER_GRUP_ROLE_EDIT})
    public static String searchPackages(@As("keyword:") String keyword, @As("komoditas") String komoditas, @As("excludes") String excludes) throws NumberFormatException {
        Logger.debug(TAG + " exclude: " + excludes);
        Integer komoditasId = Integer.parseInt(komoditas);
        List<Paket> paketList = Paket.findByKeywordAndKomoditas(keyword, komoditasId, KatalogUtils.generateExclude(excludes));

        JsonArray jsonArray = new JsonArray();
        JsonObject result = new JsonObject();

        if (paketList.isEmpty()) {
            result.addProperty("message", "Paket tidak ditemukan");
            result.addProperty("status", false);
        } else {
            for (Paket paket : paketList) {
                JsonObject obj = new JsonObject();
                obj.addProperty("name", paket.nama_paket);
                obj.addProperty("id", paket.id);

                jsonArray.add(obj);
            }

            result.addProperty("status", true);
            result.add("data", jsonArray);
        }

        return result.toString();
    }

    //id adalah penyedia_id, wId adalah wilayahd_id
    private static void deleteKeranjang(long id, Long wId, Long komId, PaketForm form) {
        Logger.debug("deleteKeranjang, id = %d, wId = %d", id, wId);
        AktifUser aktifUser = getAktifUser();
        try {
            final String key = generateKey(aktifUser.user_id);
            Logger.debug("cookie key = %s", key);
            Http.Cookie cookie = request.cookies.get(key);

            if (cookie != null) {
                Type collectionType = new TypeToken<Collection<CookieCart>>() {
                }.getType();
                final String json = URLDecoder.decode(cookie.value);
                Logger.debug("json = %s", json);
                if (!TextUtils.isEmpty(json)) {
                    List<CookieCart> cookieCarts = CommonUtil.fromJson(json, collectionType);
                    if (cookieCarts != null && !cookieCarts.isEmpty()) {
                        int totalAll = 0;
                        Iterator<CookieCart> iterator = cookieCarts.iterator();
                        while (iterator.hasNext()) {
                            totalAll++;
                            CookieCart model = iterator.next();
                            if (model != null) {
                                Logger.debug("model is not null, %s", model.toString());
                                Produk produk = Produk.findById(model.productId);
                                Logger.debug("produk penyedia_id = %d", produk != null ? produk.penyedia_id : null);
                                Logger.debug("param id = %d", id);
                                Logger.debug("model locationId = %d", model.locationId);
                                Logger.debug("wilayah ID (wId) = %d", wId);
                                //buang logic yang ribet karena seharusnya memang hanya tinggal hapus, karena produk sudah tersimpan dalam paket
                                //jadi gak perlu logic pembatasan dlsb, hanya penyamaan produk ini sudah dihapus dari keranjang, karena
                                //sudah dibuat paketnya

//								boolean produkIniAdalahDariPenyediaIni = produk.penyedia_id != null && produk.penyedia_id.equals(id);
//								boolean lokasiProdukAdalahDariLokasiIni = model.locationId.equals(wId);
//								boolean produkNonLokasi = model.locationId.equals(new Long(0))|| model.locationId.equals(new Long(-999));
//								boolean produkTidakDibatasiLokasi = (produkNonLokasi) && (null == wId) ;
//								Logger.debug("produkIniAdalahDariPenyediaIni = "+produkIniAdalahDariPenyediaIni);
//								Logger.debug("lokasiProdukAdalahDariLokasiIni = "+lokasiProdukAdalahDariLokasiIni);
//								Logger.debug("produkNonLokasi = "+produkNonLokasi);
//								Logger.debug("produkTidakDibatasiLokasi = "+produkTidakDibatasiLokasi);
//								boolean forRemoval =
//									produkIniAdalahDariPenyediaIni &&     // Jika id penyedia adalah penyedia dari produk ini
//									(lokasiProdukAdalahDariLokasiIni ||   // dan lokasi penjualan produk memang adalah lokasi pembeli
//									 produkTidakDibatasiLokasi            // atau produk memang tidak dibatasi lokasi
//									);

                                //new logic see reason above
                                boolean forRemoval = false;
                                boolean penyediaSama = produk.penyedia_id != null && produk.penyedia_id.equals(id);
                                boolean komoditasSama = produk.komoditas_id != null && produk.komoditas_id.equals(komId.intValue());
                                Logger.info("KOMODITAS SAMA " + produk.komoditas_id);
                                Logger.info("KOMODITAS SAMA ==  " + komId.intValue());
                                Logger.info("KOMODITAS SAMA ==  == " + komoditasSama);

                                boolean lokasiSama = true;
                                if (null != produk.daerah_id) {
                                    lokasiSama = produk.daerah_id.equals(wId);
                                }
                                boolean kursSama = produk.kurs_id.equals(form.kurs_id);
                                forRemoval = penyediaSama && lokasiSama && kursSama && komoditasSama;
                                Logger.debug("forRemoval = " + forRemoval + " %s %s %s", penyediaSama, lokasiSama, kursSama, komoditasSama);
                                Logger.debug("%s forRemoval = %s %s %s", produk.id, id, wId, form.kurs_id, komId);
                                if (forRemoval) {
                                    Logger.debug("removing iterator");
                                    iterator.remove();
                                }
                            }
                        }
                        Logger.debug("cookieCarts.size() = %d", cookieCarts.size());
                        Logger.debug("totalAll cart = %d", totalAll);
                        if (cookieCarts.size() != totalAll) {
                            Logger.debug("updating cookie");
                            cookie.value = URLEncoder.encode(CommonUtil.toJson(cookieCarts), "UTF-8");
                            Logger.debug(cookie.value);
                            cookie.name = key;
                            cookie.path = "/";
                            response.cookies.put(key, cookie);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.debug("EXCEPTION CAUGHT DELETE KERANJANG");
            LogUtil.e(TAG, e);
        }
    }

    public static void cetakPesanan(Long paket_id) {
        Paket model = Paket.findById(paket_id);
        notFoundIfNull(model);
        if (model != null) {
            model.withContractValue();
        }
        if (model != null) {
            model.withTotalNegotiations();
            model.withProducts();
            model.withKurs();
            model.withSatker();
            model.withPpk();
            model.withPp();
            model.withDistributor();
            model.withProvider(true);
            model.withTotalContractUploaded();
            model.withLatestStatus();
            model.withCommodity();
            model.withStatusMessage(getAktifUser());
            model.withSumberDana();
            model.withTransient();
        }

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        Logger.info("tanggaal " + now);

        Map<String, Object> params = new HashMap<>();
        params.put("paket", model);

        InputStream inp = HtmlUtil.generatePDFPesanan(params);

        String namafile = "cetak_pesanan_" + model.no_paket.toString() + ".pdf";
        File file = new File("/tmp/" + namafile);

        try {
            OutputStream os = new FileOutputStream(file);
            IOUtils.copy(inp, os);
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        renderBinary(file);
    }

    public static void getHistoryPrice(Long produkId) {
        final String query = "SELECT kldi.nama instansi " +
                ", paket.nama_paket " +
                ", paket.satuan_kerja_nama " +
                ", paket.satuan_kerja_alamat " +
                ", prov.nama_provinsi " +
                ", paket.no_paket " +
                ", pp.harga_satuan " +
                ", pp.kuantitas " +
                ", pp.harga_total " +
                ", YEAR(paket.created_date) tahun" +
                " FROM paket_produk pp" +
                " LEFT JOIN paket_status ps on pp.paket_id = ps.paket_id " +
                " LEFT JOIN paket on pp.paket_id = paket.id " +
                " LEFT JOIN kldi on kldi.id = paket.kldi_id " +
                " LEFT JOIN provinsi prov on kldi.prp_id = prov.id " +
                " WHERE pp.active > 0" +
//				" AND ps.status_negosiasi = 'sepakat' AND ps.status_paket = 'paket_selesai'" +
                " AND ps.status_paket = 'paket_selesai'" +
                " AND YEAR(paket.created_date) >= YEAR(NOW()) " +
                " AND pp.produk_id =  " + produkId;
        List<PaketProduk> results = Query.find(query, PaketProduk.class).fetch();
//		Logger.info(new Gson().toJson(results));
        renderJSON(results);
    }

    public static void removeFromPackage(Long paketProdukId, Long paketId) {
        AktifUser aktifUser = getAktifUser();
        BigDecimal kuantitas = new BigDecimal(0);
        long produk_id = 0l;
        Map<String, String> result = new HashMap<>();
        /**
         * Update Active PaketProduk
         **/
        PaketProduk paketProduk = PaketProduk.findById(paketProdukId);
        kuantitas = paketProduk.kuantitas;
        produk_id = paketProduk.produk_id;
        paketProduk.active = false;
        paketProduk.save();

        /**
        * Update stok produk
        **/
        Produk produks = new Produk();
        produks.updateStokProduk(kuantitas, produk_id,"+");

        PaketStatus paketStatus = PaketStatus.getLatestStatusByPackageId(paketId);

        if (paketStatus.nego_header_id != null) {
            PaketProdukHeader paketProdukHeader = PaketProdukHeader.getLatestHeader(paketId);

            /**
             * Update Active PaketProdukNegoDetail
             **/
            PaketProdukNegoDetail paketProdukNegoDetail = PaketProdukNegoDetail.getByHeaderId(paketProdukHeader.id);
            paketProdukNegoDetail.active = false;
            paketProdukNegoDetail.save();
        }

        List<PaketProduk> paketProdukRead = PaketProduk.findIdPaketAndActive(paketId);
        int AgrPaketProduk = paketProdukRead.size();
        double AgrKonversiHargaTotal = 0;
        for (int i = 0; i < paketProdukRead.size(); i++) {
            System.out.println(paketProdukRead.get(i).konversi_harga_total);
            AgrKonversiHargaTotal += paketProdukRead.get(i).konversi_harga_total;
        }

        /**
         * Update Paket
         **/
        Paket paket = Paket.findById(paketId);
        paket.agr_paket_produk = AgrPaketProduk;
        paket.agr_konversi_harga_total = AgrKonversiHargaTotal;
        paket.save();

        /**
         * Insert Paket Riwayat
         **/
        PaketRiwayat paketRiwayatLastes = PaketRiwayat.findLastesPaketRiwayat(paketId);
        Produk produk = Produk.findById(paketProduk.produk_id);

        PaketRiwayat paketRiwayat = new PaketRiwayat();
        paketRiwayat.paket_id = paketId;
        paketRiwayat.deskripsi = "Produk " + produk.nama_produk + " dibatalkan";
        paketRiwayat.status_paket = paketRiwayatLastes.status_paket;

        paketRiwayat.save();

        result.put("result", "empty");
        renderJSON(result);

    }


}
