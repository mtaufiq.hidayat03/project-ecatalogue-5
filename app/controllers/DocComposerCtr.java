package controllers;

import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xwpf.usermodel.*;
import play.Logger;
import play.mvc.Controller;
import play.templates.Template;
import play.templates.TemplateLoader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class DocComposerCtr extends Controller {

    public DocComposerCtr(){

    }

    public static void index() {
        XWPFDocument doc = new XWPFDocument();

        XWPFParagraph p3 = doc.createParagraph();
        p3.setWordWrap(true);
        p3.setPageBreak(true);

        //p3.setAlignment(ParagraphAlignment.DISTRIBUTE);
        p3.setAlignment(ParagraphAlignment.BOTH);
        p3.setSpacingAfterLines(1);
        p3.setIndentationFirstLine(600);
        p3.setSpacingBefore(2);
        XWPFRun r4 = p3.createRun();
        r4.setTextPosition(20);
        Template template = TemplateLoader.loadString("#{extends 'borderTemplate.html' /} " + "sebuah text");
        Map<String, Object> params = new HashMap<>();
        params.put("key","isinya key");
        String templateString = template.render(params);
        r4.setText(templateString);
//        r4.setText("To be, or not to be: that is the question: "
//                + "Whether 'tis nobler in the mind to suffer "
//                + "The slings and arrows of outrageous fortune, "
//                + "Or to take arms against a sea of troubles, "
//                + "And by opposing end them? To die: to sleep; ");
        r4.addBreak(BreakType.PAGE);

        File file = new File("simple.docx");
        try (FileOutputStream out = new FileOutputStream(file)) {
            doc.write(out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        renderBinary(file);
    }

    public static void create2(String isi_text) {
        Logger.info(isi_text+" isinyda");
        //renderArgs.put("isi_text", isi_text);
        render("MediaManager/create.html", isi_text);
    }
    public static void create(String isi_text) {

        XWPFDocument doc = new XWPFDocument();

        XSSFRichTextString str = new XSSFRichTextString();
        str.setString(isi_text);
        XWPFParagraph p3 = doc.createParagraph();
        p3.setWordWrap(true);
        p3.setPageBreak(true);

        //p3.setAlignment(ParagraphAlignment.DISTRIBUTE);
        p3.setAlignment(ParagraphAlignment.BOTH);
        p3.setSpacingAfterLines(1);
        p3.setIndentationFirstLine(600);
        p3.setSpacingBefore(2);
        XWPFRun r4 = p3.createRun();
        r4.setTextPosition(20);
        Template template = TemplateLoader.loadString("#{extends 'borderTemplate.html' /} " + "sebuah text");
        Map<String, Object> params = new HashMap<>();
        params.put("key","isinya key");
        r4.setText(isi_text);
        String templateString = template.render(params);
//        r4.setText("To be, or not to be: that is the question: "
//                + "Whether 'tis nobler in the mind to suffer "
//                + "The slings and arrows of outrageous fortune, "
//                + "Or to take arms against a sea of troubles, "
//                + "And by opposing end them? To die: to sleep; ");
        r4.addBreak(BreakType.PAGE);

        File file = new File("simple.docx");
        try (FileOutputStream out = new FileOutputStream(file)) {
            doc.write(out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        renderBinary(file);
    }

    public static void createDoc(){


    }
}
