package models.prakatalog;


import models.BaseKatalogTable;
import models.purchasing.contracts.AttachmentContract;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.DateTimeUtils;

import javax.persistence.Transient;
import java.io.File;
import java.util.Date;
import java.util.List;

@Table(name = "lampiran_kontrak")
public class LampiranKontrak extends BaseKatalogTable implements AttachmentContract {
    @Id
    public Long id;
    public Long penawaran_id;
    public String no_surat;
    public String perihal_surat;
    public Date tanggal_surat;
    public String no_berita_acara;
    public Date tanggal_berita_acara;
    public String dokumen_lampiran;
    public Long blb_id;
    public String file_name;
    public String original_file_name;
    public Long file_size;
    public String file_sub_location;
    public Integer posisi_file;
    public Integer active;

    public static List<LampiranKontrak> getByPenawaranId(Long penawaran_id) {
        final String query = "SELECT id, penawaran_id, no_surat, tanggal_surat, no_berita_acara, tanggal_berita_acara, file_name, original_file_name, file_url, blb_id " +
                "FROM lampiran_kontrak " +
                "WHERE penawaran_id = ? " +
                "AND active > 0 ORDER BY id DESC ";
        return Query.find(query,LampiranKontrak.class, penawaran_id).fetch();
    }

    public String getFormattedDate(Date date) {
        return DateTimeUtils.parseToReadableDate(date);
    }

    @Override
    public LampiranKontrak setFile(File file) {
        if (file != null) {
            this.original_file_name = file.getName();
            this.file_size = file.length();
            this.file_sub_location = DateTimeUtils.generateDirFormattedDate();
            this.file_name = regenerateFileName();
            renameTo(file);
        }
        return this;
    }
    @Override
    public LampiranKontrak position(int position) {
        this.posisi_file = position;
        return this;
    }
    @Override
    public String getDownloadUrl() {
        return generateDownloadUrl("penyedia.KontrakCtr.downloadLampiran");
    }
    @Override
    public Long getId() {
        return this.id;
    }
    @Override
    public Long getModelId() {
        return this.id;
    }
    @Override
    public String getFileName() {
        return this.file_name;
    }
    @Override
    public void saveAttachment() {
        this.id = (long) save();
    }
    @Override
    public String getOriginalFileName() {
        return this.original_file_name;
    }
    @Override
    public void setBlobId(Long blobId) {
        this.blb_id = blobId;
    }
    @Override
    public Long getBlobId() {
        return this.blb_id;
    }
    @Override
    public Long getPackageId() {
        return this.penawaran_id;
    }
    @Override
    public Integer getCreatedBy() {
        return created_by;
    }
    @Override
    public String getSubLocation() {
        return this.file_sub_location;
    }
}
