package models.util.sikap;

import play.data.validation.Required;

import java.util.List;

/**
 * Created by dadang on 7/11/17.
 */
public class Akta {

	@Required
	public long rkn_id;
	@Required
	public long lhk_id;
	@Required
	public String lhk_no;
	@Required
	public String lhk_tanggal;
	@Required
	public String lhk_notaris;
	@Required
	public String lhk_tipe;
	@Required
	public String source_data;
	@Required
	public String jenis_akta;
	public String status_verifikasi;
	public boolean is_verified = false;

	public List<Akta> akta_pendirian;
	public List<Akta> akta_perubahan;

//	public String listDokumen;

	public boolean deleted = false;
	public String alasan_pembatalan = "";
	public String keterangan_pembuktian = "";

	public String auditupdate;
	public String createdAt;

}
