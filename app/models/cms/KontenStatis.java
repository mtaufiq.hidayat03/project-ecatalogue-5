package models.cms;

import ext.DatetimeBinder;
import models.BaseKatalogTable;
import models.common.AktifUser;
import play.data.binding.As;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by user on 10/10/2017.
 */
@Table(name = "konten_statis")
public class KontenStatis extends BaseKatalogTable {
    @Id
    public Long id;
    public Long konten_kategori_id;
    public String judul_konten;
    public String slug;
    public String isi_konten;
    public String status;
    public Boolean active;

    @As(binder = DatetimeBinder.class)
    public Date publish_date_from;
    @As(binder = DatetimeBinder.class)
    public Date publish_date_to;

    public static void deleteById(Long id){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String dateNow = dateFormat.format(date);

        String query = "update konten_statis set active = 0, deleted_by = " + AktifUser.getActiveUserId() + ", deleted_date = '" + dateNow + "' where id = ?";
        Query.update(query,id);
    }

    public static KontenStatis findKontenByKategori(String category){
        String leftKontenStatisKategori = "left join konten_kategori kk on kk.id = ks.konten_kategori_id";

        String query = "select ks.id, kk.id as kategori_id, ks.judul_konten, ks.isi_konten from konten_statis ks " + leftKontenStatisKategori +
                " where ks.active = 1 and kk.active = 1 and ks.status = 'published' and kk.nama_kategori = '" + category +
                "' ORDER BY ks.id DESC";
        return Query.find(query,KontenStatis.class).first();
    }
}
