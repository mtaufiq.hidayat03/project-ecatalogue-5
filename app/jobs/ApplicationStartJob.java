package jobs;

import models.util.Config;
import play.Logger;
import play.Play;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import play.jobs.OnApplicationStart;

// @OnApplicationStart(async=true)
/** Job yg running saat applicationStart
 * Parameter yg bisa dipakai saat applicationStart
 *    -DApplicationStartJob.disabled=true  : jangan jalankan job ini
 *    -Dmail.sending.disabled=true		   : jangan jalankan job pengiriman email
 * @author Andik Yulianto<andikyulianto@yahoo.com>
 */
@NoTransaction
public class ApplicationStartJob extends Job {

	public void doJob()
	{
//		 Config conf=Config.getInstance();
//		 Logger.info("[StartUp] FileStorage: %s", conf.fileStorageDir);
//		 if("true".equals(System.getProperty("ApplicationStartJob.disabled")))
//		 	return;
//		 new UpdateKursJob().now();b
	}
}
