package models.purchasing.kontrak;

import ext.FormatUtils;
import models.BaseKatalogTable;
import models.purchasing.contracts.AttachmentContract;
import models.purchasing.kontrak.form.ContractDataForm;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.mvc.Router;
import utils.DateTimeUtils;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

@Table(name="paket_kontrak")
public class PaketKontrak extends BaseKatalogTable implements AttachmentContract {

	@Id
	public Long id;
	public Long paket_id;
	public Double nilai_kontrak = 0.0;
	public String no_kontrak;
	public Date tanggal_kontrak;
	public String file_sub_location;
	public String file_name;
	public String original_file_name;
	public Long file_size;
	public Integer posisi_file;
	public String deskripsi;
	public Long blb_id;
	public long active = 1;

	public String dokumen_kontrak;

	public PaketKontrak dataForm(ContractDataForm model) {
		this.nilai_kontrak = model.getContractValueDouble();
		this.tanggal_kontrak = model.parseContractDate();
		this.no_kontrak = model.contractNumber;
		this.deskripsi = model.note;
		this.paket_id = model.packageId;
		this.dokumen_kontrak = model.dokumen_kontrak;
		//not use again
		//setFile(model.attachment);
		//because not null
		this.file_size = new Long(0);
		return this;
	}

	public String getFormattedContractDate() {
		return DateTimeUtils.parseToReadableDate(tanggal_kontrak);
	}

	public String getFormattedContractValue() {
		return FormatUtils.formatCurrencyRupiah(nilai_kontrak);
	}

	public String getAttachmentDownloadUrl() {
		return "";
	}

//	public String getDeleteUrl() {
//		return Router.getFullUrl("purchasing.PaketKontrakCtr.deleteContract");
//	}

	public void softDelete() {
		this.active = 0;
		setDeleted();
		save();
	}

	public String getNilaiKontrakString() {
		DecimalFormat df = new DecimalFormat("#.#####");
		String formatted = df.format(nilai_kontrak);
		return formatted;
	}

	@Override
	public PaketKontrak setFile(File file) {
		if (file != null) {
			this.original_file_name = file.getName();
			this.file_size = file.length();
			this.file_sub_location = DateTimeUtils.generateDirFormattedDate();
			this.file_name = regenerateFileName();
			renameTo(file);
		}
		return this;
	}

	@Override
	public PaketKontrak position(int position) {
		this.posisi_file = position;
		return this;
	}

	@Override
	public String getDownloadUrl() {
		return generateDownloadUrl("purchasing.PaketKontrakCtr.downloadContract");
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public Long getPackageId() {
		return this.paket_id;
	}

	@Override
	public String getFileName() {
		return this.file_name;
	}

	@Override
	public String getSubLocation() {
		return this.file_sub_location;
	}

	@Override
	public String getOriginalFileName() {
		return this.original_file_name;
	}

	@Override
	public Integer getCreatedBy() {
		return created_by;
	}

	@Override
	public Long getModelId() {
		return this.id;
	}

	@Override
	public Long getBlobId() {
		return this.blb_id;
	}

	@Override
	public void saveAttachment() {
		this.id = (long) save();
	}

	@Override
	public void setBlobId(Long blobId) {
		this.blb_id = blobId;
	}
}
