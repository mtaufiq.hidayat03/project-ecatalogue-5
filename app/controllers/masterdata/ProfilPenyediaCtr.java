package controllers.masterdata;

import controllers.BaseController;
import controllers.security.AllowAccess;
import models.cms.KontenStatis;
import models.cms.Unduh;
import models.common.AktifUser;
import models.common.DokumenInfo;
import models.jcommon.blob.BlobTable;
import models.katalog.Komoditas;
import models.katalog.Produk;
import models.penyedia.DokumenUkm;
import models.penyedia.Penyedia;
import models.penyedia.PenyediaRepresentatif;
import models.prakatalog.Penawaran;
import models.prakatalog.UsulanKualifikasi;
import models.secman.Acl;
import models.user.User;
import models.util.RekananDetail;
import models.util.sikap.Identitas;
import models.util.sikap.Sikap;
import play.Logger;
import play.Play;
import play.i18n.Messages;
import play.cache.*;
import repositories.penyedia.ProviderRepository;
import utils.LogUtil;
import utils.SikapUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfilPenyediaCtr extends BaseController {

    public static final String TAG = "ProfilPenyediaCtr";

    @AllowAccess({Acl.COM_PROFIL_PENYEDIA})
    public static void index(){
        AktifUser aktifUser = getAktifUser();
        long rkn_id = 0;
        try{
            rkn_id = aktifUser.rkn_id;
        }catch (Exception e){}
        Logger.info("rkn id : " + rkn_id);

        //Data SIKap
        List<UsulanKualifikasi> jenisKualifikasiList = UsulanKualifikasi.findJenisKualifikasi();
        String cacheKey = String.valueOf(rkn_id);
        Identitas identitas = SikapUtils.getRekananIdentitas(rkn_id);
        validation.valid(identitas);
        if(validation.hasErrors() && identitas == null){
            params.flash();
            flash.error(Messages.get("prakatalog.penawaran.sikap.error"));
        }

        Sikap dataSikap = new Sikap();
        if(SikapUtils.isExistCachedData(cacheKey)){
            dataSikap = Cache.get(cacheKey, Sikap.class);
        }
        Cache.set(cacheKey,dataSikap);

        List<DokumenInfo> dokumenUkmList = new ArrayList<>();
        List<Penawaran> komoditasList = new ArrayList<>();

        //Data Representatif
        User user = User.findById(aktifUser.user_id);
        Penyedia penyedia = Penyedia.getProviderByUserId(aktifUser.user_id);
        List<PenyediaRepresentatif> representatifList = new ArrayList<>();
        if(null!=penyedia) {
            representatifList = PenyediaRepresentatif.find("penyedia_id = ?", penyedia.id).fetch();
            dokumenUkmList = DokumenInfo.listDokumenUkm(penyedia.id);
            komoditasList = Penawaran.infoKomoditasPenyedia(penyedia.id);
        }else{
            flash.error(Messages.get("notif.error.penyedia.informasi"));
        }

        render(rkn_id,identitas,jenisKualifikasiList,dataSikap,user,penyedia,representatifList,dokumenUkmList,komoditasList);
    }

    @AllowAccess({Acl.COM_PROFIL_PENYEDIA})
    public static void representatifSubmit(long penyedia_id, String[] nama_representatif,
                                           String[] telp_representatif, String[] email_representatif){
        if(nama_representatif==null)
            nama_representatif = new String[]{};
        if(telp_representatif==null)
            telp_representatif = new String[]{};
        if(email_representatif==null)
            email_representatif = new String[]{};

        if(penyedia_id!=0l){
            List<PenyediaRepresentatif> representatifList = PenyediaRepresentatif.setList(nama_representatif,telp_representatif,email_representatif);
            PenyediaRepresentatif.deleteByPenyedia(penyedia_id);
            for(PenyediaRepresentatif rep : representatifList){
                rep.penyedia_id = penyedia_id;
                rep.active = true;
                rep.save();
            }
        } else{
            flash.error(Messages.get("notif.error.penyedia.informasi"));
        }
        index();
    }

    public static void sinkronisasiPenyedia(){
        AktifUser aktifUser = getAktifUser();
        long rkn_id = 0;
        try{
            rkn_id = aktifUser.rkn_id;
        }catch (Exception e){}

        if(rkn_id != 0){
            Penyedia penyedia = ProviderRepository.findByUserId(aktifUser.user_id);
            if(penyedia == null){
                Long penyedia_id = ProviderRepository.saveNewByRknId(rkn_id, aktifUser.user_id);
                Logger.info("penyedia id : " + penyedia_id);
                flash.success(Messages.get("notif.success.update"));
            }else{
                RekananDetail rknDetail = RekananDetail.getIdRekananDefault(rkn_id);
                if(null != rknDetail) {
                    User user = User.findById(aktifUser.user_id);
                    user.nama_lengkap = rknDetail.rkn_nama;
                    user.user_password = rknDetail.passw;
                    user.user_email = rknDetail.rkn_email;
                    user.rkn_id = rknDetail.rkn_id;
                    user.save();

                    penyedia.nama_penyedia = rknDetail.rkn_nama;
                    penyedia.alamat = rknDetail.rkn_alamat;
                    penyedia.kode_pos = rknDetail.rkn_kodepos;
                    penyedia.website = rknDetail.rkn_website;
                    penyedia.email = rknDetail.rkn_email;
                    penyedia.no_telp = rknDetail.rkn_telepon;
                    penyedia.no_fax = rknDetail.rkn_fax;
                    penyedia.no_hp = rknDetail.rkn_mobile_phone;
                    penyedia.npwp = rknDetail.rkn_npwp;
                    penyedia.pkp = rknDetail.rkn_pkp;
                    penyedia.rkn_id = rknDetail.rkn_id;
                    penyedia.save();

                    flash.success(Messages.get("notif.success.update"));
                } else{
                    flash.error(Messages.get("notif.rkn.not_found"));
                }
            }
        }else{
            flash.error(Messages.get("notif.rkn.not_found"));
        }
        index();
    }

    @AllowAccess({Acl.COM_PROFIL_PENYEDIA, Acl.COM_PENYEDIA_EDIT})
    public static void penyediaUkm(){
        AktifUser aktifUser = getAktifUser();
        long rkn_id = 0;
        try{
            rkn_id = aktifUser.rkn_id;
        }catch (Exception e){}

        if(rkn_id != 0){
            Penyedia penyedia = ProviderRepository.findByUserId(aktifUser.user_id);
            if(penyedia == null){
                flash.error(Messages.get("notif.error.penyedia.informasi"));
            }else{
                KontenStatis syaratKetentuan = KontenStatis.findKontenByKategori("ukm");

                if(null == syaratKetentuan){
                    syaratKetentuan = new KontenStatis();
                    syaratKetentuan.isi_konten ="";
                }

                List<DokumenUkm> dokumenUkmList = DokumenUkm.find("penyedia_id = ?", penyedia.id).fetch();
                List<DokumenInfo> dokumenInfoList = DokumenInfo.getUploadInfoUkmList(dokumenUkmList);

                render(penyedia,syaratKetentuan,dokumenInfoList);
            }
        }else{
            flash.error(Messages.get("notif.rkn.not_found"));
        }
        index();
    }

    @AllowAccess({Acl.COM_PROFIL_PENYEDIA, Acl.COM_PENYEDIA_EDIT})
    public static void penyediaUkmSubmit(Penyedia penyedia){
        Penyedia py = Penyedia.findById(penyedia.id);
        if(py != null){
            py.is_umkm = penyedia.is_umkm;
            py.save();
            //update all produk
            int ukm = (penyedia.is_umkm == 1 || penyedia.is_umkm == 2) ? 1 : 0;
            Produk.updateIsUmkmbyPenyediaId(penyedia.id, ukm);

            flash.success(Messages.get("notif.success.update"));
        }else{
            flash.error(Messages.get("notif.error.penyedia.informasi"));
        }
        index();
    }

    public static void upload(File file, Long penyediaId, String dok_label) {
        LogUtil.d(TAG, "penyedia id: " + penyediaId);
        checkAuthenticity();
        Map<String, Object> result = new HashMap<String, Object>(1);
        try {
            DokumenInfo dokumenInfo = DokumenUkm.simpanDokUkm(penyediaId, file, dok_label);
            List<DokumenInfo> files = new ArrayList<DokumenInfo>();
            files.add(dokumenInfo);
            result.put("files", files);
        } catch (Exception e) {
            result.put("errorx", "upload failed");
            e.printStackTrace();
        }
        if (result.get("files") == null) {
            result.clear();
            result.put("errorx", "upload failed");
        }

        renderJSON(result);
    }

    public static void hapusDoc(Long dokId) throws Exception {
        Map<String, String> result = new HashMap<>();
        checkAuthenticity();

        Penyedia penyedia = ProviderRepository.findByUserId(getAktifUser().user_id);
        Long dokumen = DokumenUkm.countDokumen(penyedia.id);

        if(dokumen == 1){
            result.put("result", "empty");
        }else {
            DokumenUkm dokUkm = DokumenUkm.findByDokIdAttach(dokId);
            BlobTable blobTable = BlobTable.findByBlobId(dokUkm.dok_id_attachment);
            if (blobTable != null) {
                blobTable.delete();
            }
            if (dokUkm != null) {
                dokUkm.delete();
            }
            result.put("result", "ok");
        }

        renderJSON(result);
    }

    public static void getFileUnduh(){
        // hardcode id_file
        Long id = 74l;
        Unduh viewunduh = Unduh.findById(id);
        try {
            String generalPath = Play.applicationPath + "/public/files/pustaka_file"+viewunduh.file_sub_location;
            File test = new File (generalPath+"/"+viewunduh.file_name);
            if(test.exists() && !test.isDirectory()) {
                renderBinary(test,viewunduh.original_file_name);
            }else{
                params.flash();
                flash.error(Messages.get("file tidak ditemukan"));
                penyediaUkm();
            }
        }catch (Exception e){
            penyediaUkm();
        }
    }
}
