package Migration;

import com.google.gson.Gson;
import models.katalog.*;
import models.katalog.komoditasmodel.KomoditasAtributHarga;
import play.Play;
import play.db.jdbc.Query;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class MigrasiHargaNasional {
    String filename = "reportProdukHargaNasional_"+ UUID.randomUUID().toString().substring(0,5)+".txt";
    static String rootPath = Play.applicationPath.getAbsolutePath();
    String msg = "";
    int pageLength = 50;

    public void start(){
        msg=">>>>>>>>>>>>>>>>> Starting Migration Harga Nasional >>>>>>>>>>>>";writeReportToFile(msg);
        int start = 0;//10000;
        int end = pageLength;

        long totalProduk =  getTotal();//new Long(20000);
        msg = "total produk: "+totalProduk; writeReportToFile(msg);

        int totalPage = new Long(totalProduk/pageLength).intValue();
        int page = 1;

        while (totalProduk > start) {
            List<Produk> listProduk = new ArrayList<>();
            //ambil sebanyak page data
            msg = "page " + page + " of " + totalPage + " page data";writeReportToFile(msg);
            msg = "start " + start + " to " + end+" end "+totalProduk;writeReportToFile(msg);
            //dataproduk perpage
            listProduk = getProdukPerPage(start, pageLength, "nasional");
            msg = "totalpagedata:"+listProduk.size();writeReportToFile(msg);
            for (Produk prd: listProduk) {
                Komoditas komo = Komoditas.findById(prd.komoditas_id);
                List<KomoditasAtributHarga> kah = KomoditasAtributHarga.findByKomoditas(prd.komoditas_id);
                msg = "@@@@@ komoditashargaatribut produk_id:" + prd.id +" total:" + kah.size();writeReportToFile(msg);

                for (KomoditasAtributHarga kh:kah) {
                    msg = "atribut_id:"+kh.id+" label:"+kh.label_harga;writeReportToFile(msg);
                    List<ProdukHargaNasional> pha = ProdukHargaNasional.getByProductIdAndAtributid(prd.id, kh.id);
                    boolean newph = false;
                    for (ProdukHargaNasional phn : pha){
//                        if(null == phn){
//                            ProdukHargaNasional phg = new ProdukHargaNasional();
//                            phg.kurs_id = new Long(1);//IDR
//                        }
                        ProdukHarga ph = ProdukHarga.getByProductIdAndAtributidAndPriceDate(prd.id, kh.id, (phn.harga_tanggal).toString());
                        if(null == ph){
                            newph = true;
                            msg = "new  produk harga wit produk_id:"+prd.id;writeReportToFile(msg);
                            ph = new ProdukHarga();
                            ph.produk_id = prd.id;
                            ph.kurs_id = phn.kurs_id;
                            ph.tanggal_dibuat = phn.harga_tanggal;
                            ph.apakah_disetujui = phn.approved;
                            ph.tanggal_disetujui = phn.approved_date;
                            ph.tanggal_ditolak = null;
                            ph.created_by = phn.created_by;
                            ph.created_date = phn.created_date;
                            ph.modified_by = phn.modified_by;
                            ph.modified_date = phn.modified_date;
                            ph.deleted_by = phn.deleted_by;
                            ph.deleted_date = phn.deleted_date;
                            ph.active = phn.active;
                            ph.komoditas_harga_atribut_id = phn.komoditas_harga_atribut_id;
                            ph.kelas_harga = komo.kelas_harga;
                            ph.setuju_tolak_oleh = phn.approved_by;
                        }else{
                            ph.modified_date = new Timestamp(new Date().getTime());
                        }

                        HargaProdukNasionalMigration hpm = new HargaProdukNasionalMigration();
                        //ph.harga_xls_json = createJsonXls(prd.id);
                        //ph.harga = createHarga(prd.id, kh.id);
                        hpm.harga = new BigDecimal(phn.harga);
                        ph.harga = new Gson().toJson(hpm);
                        //msg = "hpm "+new Gson().toJson(hpm);writeReportToFile(msg);
                        //msg = "ph "+new Gson().toJson(ph);writeReportToFile(msg);
                        if(newph){
                            msg = ">> new produk harga:"+ph.save()+" produk_id:"+ph.produk_id;
                        }else {
                            msg = "<< update produk harga ok:" + ph.save()+" with id:"+ph.id;
                        }
                        writeReportToFile(msg);
                    }
                }
            }
            end+=pageLength;start+=pageLength;page++;
        }
        msg = "END MIGRATION NASIONAL";writeReportToFile(msg);
    }

    protected List<Produk> getProdukPerPage(int start, int page, String kelas_harga){
//        String sql = "select p.id, p.komoditas_id, p.nama_produk " +
//                "from produk p " +
//                "join komoditas k on p.komoditas_id = k.id " +
//                "JOIN penyedia pn ON p.penyedia_id = pn.id " +
//                "JOIN penyedia_kontrak pk ON pn.id = pk.penyedia_id " +
//                "where p.active = 1 AND p.apakah_ditayangkan = 1 " +
//                "AND k.kelas_harga = ? " +
//                "AND p.berlaku_sampai <> '' AND p.minta_disetujui = 0 AND p.setuju_tolak = 'setuju' " +
//                "AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1 " +
//                "AND (k.apakah_stok_unlimited = 1 OR (k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0)) limit ?, ?";

        String sql = "SELECT p.id, p.komoditas_id, p.nama_produk " +
                "FROM produk p " +
                "JOIN komoditas k on p.komoditas_id = k.id " +
                "WHERE k.kelas_harga = ? " +
                "LIMIT ?, ?";
        try{
            return Query.find(sql, Produk.class,kelas_harga, start, page).fetch();
        }catch (Exception e){
            msg = "error getProdukPerpage:"+e.getMessage();
            writeReportToFile(msg);
        }
        return new ArrayList<>();
    }
    public long getTotal(){
        msg = "Get TOTAL produk";writeReportToFile(msg);
        Long total = 0L;
        try {
            String sql = "select count(p.id) from produk p\n" +
                    "join komoditas k on k.id = p.komoditas_id\n" +
                    "where k.kelas_harga='nasional'";
            total = Query.count(sql);
        }catch (Exception e){
            msg = "error get total: "+e.getMessage(); writeReportToFile(msg);
        }
        return total;
    }
    public void writeReportToFile(String text) {
        System.out.println(text);
        text += " \n";
        if (!Files.exists(Paths.get(rootPath + "/"+filename))) {
            PrintWriter writer = null;
            try {
                System.out.println("new file name report:"+filename);
                writer = new PrintWriter(rootPath + "/"+filename, "UTF-8");
            } catch (FileNotFoundException e) {

            } catch (UnsupportedEncodingException e) {

            }
            writer.println("----<The first line >--- \n");
            writer.close();
        }
        try {
            Files.write(Paths.get(filename), text.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
            System.out.println("Eror write filereport:" + e.getMessage());
            e.printStackTrace();
        }
        msg = "";
    }
}
