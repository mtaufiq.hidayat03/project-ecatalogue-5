package RepairElastic;

import com.google.gson.Gson;
import jobs.JobTrail.viewmodels.ResultFeedBackEs;
import jobs.elasticsearch.ElasticConnection;
import jobs.elasticsearch.ElasticSearchTextLog;
import models.elasticsearch.log.ProductLogJson;
import models.util.Config;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;
import play.Logger;
import play.Play;
import play.libs.F;
import play.libs.WS;
import play.test.UnitTest;
import services.elasticsearch.ElasticsearchConnection;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class TestUp extends UnitTest {
    @Test
    public void testUpProduk () throws SQLException {
        boolean override = false;
        boolean hapusFileJson = false;
        ElasticSearchTextLog.loopLimitProduk(override, hapusFileJson);
    }

    //@Test
    public void testPost(){
        String indexFilePath = Play.applicationPath + "/logstash/index.json";
        //WSRequest req= WS.url(url);
        ElasticConnection elasticConnection = new ElasticConnection(true);
        //elasticConnection.delete();
        //elasticConnection.indexing(new File(indexFilePath));
        String file1 = "/Users/asepabdulsofyan/sourcework/lkpp2019/e-katalog-5/tmp_/elasticsearch/2019/09/23/1.txt";
        String urls = ElasticsearchConnection.getElasticurls()+ElasticConnection.modelUrl+ElasticConnection.bulklUrl;
        //"http://192.168.11.15:9201/ecatalogue/produk/_bulk";

        File fileLog = new File(file1);
        byte[] dt = new byte[0];
        try {
            dt = Files.readAllBytes(Paths.get(fileLog.getAbsolutePath()));
        } catch (IOException e) {}
        String sb = new String(dt);
        F.Promise<WS.HttpResponse> response = null;
        do{
            response = WS.url(urls).body(sb).timeout("10min").postAsync();
            if(null != response){
                Logger.info("done");
            }else{
                Logger.info("not done");
            }
            try {
                //report post
                String strResult= response.get().getString();
                Logger.info(strResult);
                ResultFeedBackEs restObj = new Gson().fromJson(strResult, ResultFeedBackEs.class);
                int okObjekt = restObj.items.stream().filter(f-> f.create.status==201).collect(Collectors.toList()).size();
                int nobjekt = restObj.items.stream().filter(f-> f.create.status==409).collect(Collectors.toList()).size();
                String logs = "response NOT null sending page , containt failed object:"+restObj.errors+", ok:"+okObjekt+", no:"+nobjekt;
                Logger.info(logs);
            } catch (InterruptedException e) {
            } catch (ExecutionException e) {}
        }while (null == response);


    }


    public void testGet(){
        Long total = ElasticSearchTextLog.getTotalDataElastics();
        Logger.info(total+ " total");
    }

    //@Test
    public void testGetElasticserver(){
        WS.HttpResponse exist = WS.url(Config.getInstance().elasticsearchHost+"/").get();
        if(exist.success()){
            Logger.info("success");
        }else{
            Logger.info("failed");
        }
    }

    //@Test
    public void testGetProduk(){
        long[] ar = {32,38,40,100,122,130,164,167,169,170,184,187,188,189,190,191,192,193,194,195,196,197};
        for (long page:ar) {
            List<ProductLogJson> list = ElasticSearchTextLog.getProduk((page * 1000));
            try {
                list = ElasticSearchTextLog.executeProduk(list, page);
            } catch (SQLException e) {
                Logger.info("ERROR:" + e.getMessage());
            }
        }
    }
}
