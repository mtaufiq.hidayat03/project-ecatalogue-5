package models.purchasing.base;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import models.purchasing.pengiriman.form.DeliveryItemJson;
import org.apache.http.util.TextUtils;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author HanusaCloud on 10/23/2017
 */
public class BaseDataForm {

    public File attachment;
    public String action;
    public Long model;
    public String savedProducts;
    public String deletedProducts;
    public Integer totalProducts;

    public String savedDelivery;
    public String deletedDelivery;

    public boolean isCreateMode() {
        return action.equalsIgnoreCase("create");
    }

    public List<BaseProductForm> getSavedProducts() {
        return generateList(savedProducts, false);
    }

    public List<BaseProductForm> getDeletedProducts() {
        return generateList(deletedProducts, true);
    }

    public List<BaseProductForm> generateList(String json, boolean isDeleted) {
        List<BaseProductForm> results = new ArrayList<>();
        if (TextUtils.isEmpty(json)) {
            json = "[]";
        }
        Type collectionType = new TypeToken<Collection<BaseProductForm>>() {
        }.getType();
        List<BaseProductForm> items = new Gson().fromJson(json, collectionType);
        if (items != null && !items.isEmpty()) {
            if (!isDeleted) {
                totalProducts = items.size();
            }
            results.addAll(items);
        }
        return results;
    }

    public String getIds(List<BaseProductForm> list) {
        StringBuilder sb = new StringBuilder();
        if (!list.isEmpty()) {
            String glue = "";
            for (BaseProductForm model : list) {
                sb.append(glue).append("'").append(model.packageProductId).append("'");
                glue = ",";
            }
        }
        return sb.toString();
    }

    public DeliveryItemJson getSavedDelivery() {
        if (TextUtils.isEmpty(savedDelivery)) {
            return null;
        }
        return new Gson().fromJson(savedDelivery, DeliveryItemJson.class);
    }

    public DeliveryItemJson getDeletedDelivery() {
        if (TextUtils.isEmpty(deletedDelivery)) {
            return null;
        }
        return new Gson().fromJson(deletedDelivery, DeliveryItemJson.class);
    }

}
