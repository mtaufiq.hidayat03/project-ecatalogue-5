package models.penyedia.contracts;

import models.penyedia.PenyediaDistributor;
import models.penyedia.PenyediaRepresentatif;
import models.user.User;

import java.util.List;

/**
 * @author HanusaCloud on 11/6/2017
 */
public interface ProviderContract {

    Long getId();

    Long getUserId();

    void setRepresentatives(List<PenyediaRepresentatif> list);

    void setDistributor(List<PenyediaDistributor> distributors);

    void setUser(User user);

    default void withRepresentatives() {
        if (getId() != null) {
            setRepresentatives(PenyediaRepresentatif.getByProviderId(getId()));
        }
    }

    default void withDistributors() {
        if (getId() != null) {
            setDistributor(PenyediaDistributor.findByPenyedia(getId()));
        }
    }

    default void withUser() {
        if (getUserId() != null) {
            setUser(User.findById(getUserId()));
        }
    }

}
