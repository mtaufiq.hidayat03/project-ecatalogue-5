package controllers.masterdata;

import controllers.BaseController;
import controllers.security.AllowAccess;
import jobs.sumberdana.JobSumberDana;
import models.jcommon.util.CommonUtil;
import models.masterdata.SumberDana;
import models.secman.Acl;
import play.i18n.Messages;

import java.util.Arrays;
import java.util.List;

public class SumberDanaCtr extends BaseController {

	@AllowAccess({Acl.COM_SUMBER_DANA})
	public static void index() {
		render("masterdata/SumberDanaCtr/index.html");
	}

	@AllowAccess({Acl.COM_SUMBER_DANA_ADD})
	public static void manualFetch() {
		JobSumberDana jw = new JobSumberDana();
		jw.doJob();
		index();
	}

	@AllowAccess({Acl.COM_SUMBER_DANA_ADD})
	public static void create() { render("masterdata/SumberDanaCtr/create.html"); }

	@AllowAccess({Acl.COM_SUMBER_DANA_ADD})
	public static void createSubmit(String[] nama_sumber_dana_arr) {
		checkAuthenticity();
		if(nama_sumber_dana_arr != null){

			//validasi sumber dana
			boolean validate = validateSumberDana(nama_sumber_dana_arr);

			if(!validate){

				if (validation.hasErrors())
					flash.error(Messages.get("notif.failed.submit"));

				List<String> sumberDanaList = Arrays.asList(nama_sumber_dana_arr);

				render("masterdata/SumberDanaCtr/create.html", sumberDanaList);

			}

			for(int i = 0; i < nama_sumber_dana_arr.length; i++){
				SumberDana sd = new SumberDana();
				sd.nama_sumber_dana = nama_sumber_dana_arr[i];
				sd.active = true;
				sd.save();
			}

			flash.success(Messages.get("notif.success.submit"));
			index();
		}

	}

	@AllowAccess({Acl.COM_SUMBER_DANA_EDIT})
	public static void editSubmit(long id_sumber, String nama_sumber_dana) {
		checkAuthenticity();
		if (CommonUtil.isEmpty(nama_sumber_dana)) {
			flash.error(Messages.get("notif.failed.update"));
		} else {
			SumberDana sd = SumberDana.findById(id_sumber);
			sd.nama_sumber_dana = nama_sumber_dana;
			sd.active = true;
			sd.save();
		}
		flash.success(Messages.get("notif.success.update"));
		index();
	}

	@AllowAccess({Acl.COM_SUMBER_DANA_DELETE})
	public static void delete(long id) {
		SumberDana model = SumberDana.findById(id);
		notFoundIfNull(model);
		if (SumberDana.checkChildByFundSourceId(model.id) == 0) {
			model.softDelete();
			flash.success(Messages.get("notif.success.delete"));
		} else {
			flash.error(Messages.get("notif.failed.delete.relation"));
		}
		index();
	}

	private static boolean validateSumberDana(String[] nama_sumber_dana_arr){

		boolean validate = true;

		for(int i = 0; i < nama_sumber_dana_arr.length; i++){

			if(CommonUtil.isEmpty(nama_sumber_dana_arr[i])) {

				validate = false;

				validation.addError("sumberDana_"+(i+1), "Nama Sumber Dana Harus Diisi", "var");

			}

		}

		return validate;

	}

	public static void getSumberDana(){
		List<SumberDana> sumberDanaList = SumberDana.find("active = 1").fetch();

		renderJSON(sumberDanaList);
	}

}
