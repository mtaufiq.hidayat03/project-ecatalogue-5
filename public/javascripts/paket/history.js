var tempProducts = {};
var deletedProducts = {};
var arrayDeleteProducts = [];

var tempDelivery = {};
var deletedDelivery = {};

var productList = jQuery('.product-list');
var modalSearchButton = jQuery('.modal-search-button');
var modalSearchView = jQuery('.modal-search-view');
var frontTableBody = jQuery('.products');

var deliveryListFront = jQuery('.delivery-list-front');
var deliveryList = jQuery('.delivery-list');


jQuery('.product-form').validate({
    highlight: function(element) {
        $(element).parents('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).parents('.form-group').removeClass('has-error');
    }
});


function generate() {
    var delivered = jQuery('.products').children('tr');
    var results = [];
    var totalInvalid = 0;
    if (delivered.length > 0) {
        for (var i = 0; i < delivered.length; i++) {
            var view = delivered.eq(i);
            var data = {};
            data['isChecked'] = true;
            data['packageProductId'] = view.data('value');
            data['productId'] = view.data('product');
            data['modelId'] = view.data('id');
            data['productQuantity'] = view.find('.product-quantity').val();
            if (data['productQuantity'] <= 0) {
                totalInvalid++;
            }
            data['productNote'] = view.find('.product-note').val();
            results.push(data);
        }
        jQuery('.saved-products').val(JSON.stringify(results));
    }
    setDelivery();
    return totalInvalid === 0 && results.length > 0;
}

jQuery('.add-button').click(function () {
    jQuery('#myModal').modal({
        backdrop: 'static',
        keyboard: false
    });
});

jQuery('#myModal').on('show.bs.modal', function (e) {
   productList.html('');
   tempProducts = {};
   modalSearchView.val('');
   deliveryList.html('');
});

jQuery('#rootwizard').bootstrapWizard({
    onTabShow : function(tab, navigation, index) {
        var next = jQuery('.next');
        next.show();
        var $total = navigation.find('li').length;
        var $current = index + 1;
        var $percent = ($current / $total) * 100;
        $('#rootwizard .progress-bar').css({
            width : $percent + '%'
        });
        if($current === $total) {
            next.addClass('simpan');
            next.removeClass('disabled');
            next.find('a').addClass('btn btn-md btn-primary').html(params.button.save);
        } else {
            next.removeClass('simpan');
            next.find('a').removeClass('btn btn-md btn-primary').html(params.button.next);
        }
    },
    onTabClick: function(tab, navigation, index) {
        alert('on tab click disabled');
        return false;
    },
    onNext: function (tab, navitaion, index) {
        return params.inputType === 2 ? validateAcceptance(index) : validateDelivery(index);
    }
});

function validateAcceptance(index) {
    if (index === 1) {
        $('input[name="model.docNumber"]').rules('add', {
            required: true,
            alphanumeric: true
        });

        $('input[name="model.documentDate"]').rules('add', {
            required: true,
            dateformat: true
        });
        $('input[name="model.acceptanceDate"]').rules('add', {
            required: true,
            dateformat: true
        });
    } else if (index === 2) {
        validateLastForm("diterima");
    }
    return jQuery('.product-form').valid();
}

function validateDelivery(index) {

    if (index === 1) {
        $('input[name="deliveryDataForm.docNumber"]').rules('add', {
            required: true,
            alphanumeric: true
        });
        $('input[name="deliveryDataForm.docDate"]').rules('add', {
            required: true,
            dateformat: true
        });
    } else if (index === 2) {
        validateLastForm("dikirimkan");
    }

    return jQuery('.product-form').valid();
}

function validateLastForm(type) {
    if(!generate()) {
        showToast((type === 'dikirimkan')
            ? params.messages.delivery_product_error_message
            : params.messages.acceptance_product_error_message);
        return false;
    }

    //-- update by sudi
    if (params.inputType===1){
        var totalItemValid = true;
        var MyRows = $('table#tblProduct').find('tbody').find('tr');
        var pesanError = "";
        var notValid = 0;

        for (var i = 0; i < MyRows.length; i++) {
            /*console.log("panjang tabel "+MyRows.length);*/
            var jmlPesanan = BigInt($(MyRows[i]).find('td:eq(4)').html());
            var jmlKirim = BigInt($(MyRows[i]).find("td:eq(5) input[id='leftOverEdit']").val());
            var jmlSisaKirim = BigInt($(MyRows[i]).find("td:eq(5) input[id='leftOverOri']").val());

            //aturan setelah konfirmasi ke mas fajar
            // jika jmlSisaKirim == jmlPesanan maka jmlKirim > 0 atau <= jmlPesanan
            // jika jmlSisaKirim < jmlPesanan maka jmlKirim > 0 atau <= jmlSisaKirim
            if (jmlSisaKirim===jmlPesanan){
                if (jmlKirim > jmlPesanan){
                    notValid++;
                    totalItemValid = false;
                    pesanError = params.messages.delivery_product_amount_error_message;
                    //alert("jmlKirim > jmlPesanan "+jmlKirim +" > "+ jmlPesanan)
                }
            } else {
                if (jmlKirim > jmlSisaKirim){
                    notValid++;
                    totalItemValid = false;
                    pesanError = params.messages.delivery_product_amount_leftover_error_message+" "+jmlSisaKirim+".";
                    //alert("jmlKirim > jmlSisaKirim "+jmlKirim +" > "+ jmlSisaKirim +" "+pesanError)
                }
            }

        }

        if (totalItemValid === false){
            if (notValid===1){
                showToast(pesanError);
            }
            if (notValid > 1){
                showToast(params.messages.delivery_product_amount_leftover_and_order_error_message);
            }
            //alert(params.messages.delivery_product_amount_error_message);
            totalItemValid = true;
            notValid = 0;
            return false;
        }

        //-- end update sudi
    }
    
    //alert("Data sukses tersimpan");
    jQuery('.product-form').submit();
}

function showToast(message) {
    jQuery.toast({
        heading: 'Oops',
        text: message,
        position: 'bottom-center',
        stack: false,
        icon: 'error',
        hideAfter: 6000
    })
}

/*======================= delete product ===================================*/

jQuery(document).on('click', '.delete-button', function () {
    deleteProduct(jQuery(this))
});

function deleteProduct(view) {
    var value = view.data('value');
    var productId = view.data('product');
    var packageProductId = view.data('packageproduct');
    if (currentProducts.hasOwnProperty(productId)) {
        var model = currentProducts[productId];
        //console.log(model);
        delete currentProducts[productId];
        if (value !== '') {
            deletedProducts[productId] = model;
            generatedDeletedItems();
        }
    }
    if (value!=null && value!=""){
        arrayDeleteProducts.push({'productId': productId, 'modelId': value, 'packageProductId':value});
        jQuery('.deleted-products').val(JSON.stringify(arrayDeleteProducts));
        //console.log(JSON.stringify(arrayDeleteProducts));
    }
    deleteItem(view);
    view.add('disabled');
}

function generatedDeletedItems() {
    if (Object.keys(deletedProducts).length > 0) {
        var array = [];
        for (var i in deletedProducts) {
            var data = deletedProducts[i];
            array.push({'productId': data.productId, 'modelId': data.id});
        }
        jQuery('.deleted-products').val(JSON.stringify(array));
    }
}

function deleteItem(view) {
    view.parent().parent().remove();
}

/*======================= search products ==================================*/

modalSearchButton.click(function () {
   if (params.inputType === 2) {
        getDelivery();
   } else {
       getProducts();
   }
});

function getProducts() {
    var loader = Ladda.create(document.querySelector('.modal-search-button'));
    loader.start();
    jQuery.ajax({
        url: params.queryAction,
        type: 'POST',
        data: {
            keyword: modalSearchView.val(),
            exception: generateException(),
            authenticityToken: params.token
        }
    }).success(function (s) {
        loader.stop();
        generateObject(s);
    }).fail(function (e) {
        loader.stop();
        showToast("Terjadi kesalahan");
    });
}

function generateObject(data) {
    tempProducts = {};
    var template = '';
    if (data.length > 0) {
        for (var i = 0; i < data.length; i++) {
            var model = data[i];
            // console.log(model);
            if (!tempProducts.hasOwnProperty(model.productId)) {
                tempProducts[model.packageProductId] = model;
                template += generateModalHtmlTemplateListItem(model);
            }
        }
    }
    productList.html(template);
}

function generateModalHtmlTemplateListItem(model) {
    return '<tr>' +
        '<td width="300">' +
        // '<a href="' + model.detailUrl + '">' +
        model.name +
        // '</a>' +
        '</td>' +
        '<td>' + model.quantity + '</td>' +
        '<td>' + model.noProduct + '</td>' +
        '<td>' + model.note + '</td>' +
        '<td><button class="btn btn-sm btn-primary item-add-button" type="button" data-value="' + model.packageProductId + '">' +
        '<i class="fa fa-plus"></i>' +
        '</button></td>' +
        '</tr>'
}

function generateHtmlTemplateListItem(model) {
    var tempalate = '<tr data-value="' + model.packageProductId + '" data-product="' + model.productId + '">' +
        '        <td><img class="img img-responsive" src="' + model.imageUrl + '"/></td>' +
        '        <td>' +
        '            <h5>' +
        // '               <a href="' + model.detailUrl + '">' +
                        model.noProduct +
        // '               </a>' +
        '            </h5>' +
        '            <h5>' + model.name + '</h5>' +
        '        </td>' +
        '        <td>' + model.note + '</td>' +
        '        <td>' + model.unitName + '</td>' +
        '        <td>' + model.quantity + '</td>';
        if (params.inputType === 1) {
            tempalate += '        <td>' +
                '            <input class="product-quantity" id="leftOverEdit" type="number" value="' + model.leftovers + '" required /><input class="product-quantity-ori" id="leftOverOri" type="number" value="' + model.leftovers + '" hidden />' +
                '        </td>';
        }
        if (params.inputType === 2) {
            tempalate += '        <td>' +
                '            <input class="product-quantity" type="number" value="' + model.leftovers + '" required disabled/>' +
                '        </td>';
        }
        tempalate +='        <td>' +
        '            <textarea class="product-note" rows="4" ></textarea>' +
        '        </td>' +
        '        <td>';
        if (params.inputType === 1) {
            tempalate += '            <button class="btn btn-sm btn-danger delete-button" type="button" data-value="" data-product="' + model.packageProductId + '">\n' +
            '                <i class="fa fa-trash"></i>\n' +
            '            </button>\n' +
            '        </td>';
        }
        tempalate += '    </tr>';
        return tempalate;
}

jQuery(document).on('click', '.item-add-button', function () {
    var view = jQuery(this);
    var value = view.data('value');
    if (tempProducts.hasOwnProperty(value) && !currentProducts.hasOwnProperty(value)) {
        var model = tempProducts[value];
        currentProducts[value] = model;
        frontTableBody.append(generateHtmlTemplateListItem(model));
    }
    view.addClass('disabled');
});

function generateException() {
    if (Object.keys(currentProducts).length > 0) {
        var array = [];
        for (var i in currentProducts) {
            array.push(i);
        }
        return JSON.stringify(array);
    }
    return '[]';
}

/*======================================= SEARCH Delivery ==================================*/

function getDelivery() {
    var loader = Ladda.create(document.querySelector('.modal-search-button'));
    loader.start();
    jQuery.ajax({
        url: params.searchAction,
        type: 'POST',
        data: {
            keyword: modalSearchView.val(),
            authenticityToken: params.token
        }
    }).success(function (s) {
        loader.stop();
        generateDeliveryObject(s);
    }).fail(function (e) {
        loader.stop();
        showToast("Terjadi kesalahan");
    });
}

jQuery(document).on('click', '.add-delivery', function () {
    addDelivery(jQuery(this));
});

jQuery(document).on('click', '.delete-delivery', function () {
    deleteDelivery(jQuery(this));
});

function addDelivery(view) {
    var value = view.data('value');
    if (!currentDelivery.hasOwnProperty(value)
        && tempDelivery.hasOwnProperty(value)
        && Object.keys(currentDelivery).length === 0) {
        var model = tempDelivery[value];
        deliveryListFront.html(generateDeliveryHtmlTemplate(model, false));
        currentDelivery[value] = model;
        getDeliveryProducts(view);
    }
}

function setDelivery() {
    if (Object.keys(currentDelivery).length > 0) {
        var model = currentDelivery[Object.keys(currentDelivery)[0]];
        jQuery('.saved-delivery').val(JSON.stringify(model));
    }
}

function getFirstDeliveryKey() {
    if (Object.keys(currentDelivery).length > 0) {
        return Object.keys(currentDelivery)[0];
    }
    return null;
}

function generateDeliveryObject(data) {
    var template = '';
    if (data.length > 0) {
        var firstKey = getFirstDeliveryKey();
        for (var i = 0; i < data.length; i++ ){
            var model = data[i];
            model['allowed'] = firstKey === null;
            tempDelivery[model.id] = model;
            if (firstKey !== model.id) {
                template += generateDeliveryHtmlTemplate(model, true);
            }
        }
    }
    deliveryList.html(template);
}

function generateDeliveryHtmlTemplate(model, isAdded) {
    return '<tr data-value="' + model.id + '">' +
        '        <td>' +
        '            <a href="' + model.detailUrl + '">' + model.systemDocumentNumber + '</a>' +
        '        </td>' +
        '        <td>' + model.documentNumber + '</td>' +
        '        <td>' + model.documentDate + '</td>' +
        '        <td class="column-right">' + model.total + '</td>' +
        '        <td class="column-right">' + model.deskripsi + '</td>' +
        '        <td class="column-center">' +
        '            <button ' +(isAdded ? 'id="add-delivery-'+model.id+'" ' : '')+ ' class="btn btn-sm btn-primary '+(isAdded ? 'add-delivery' : 'delete-delivery')+' ' + (model.allowed ? '' : 'disabled') + '" type="button" data-value="' + model.id + '">' +
        '                <i class="fa fa-'+(isAdded ? 'plus' : 'remove')+'"></i>' +
        '            </button>' +
        '        </td>' +
        '    </tr>';
}

function deleteDelivery(view) {
    var value = view.data('value');
    if (currentDelivery.hasOwnProperty(value)) {
        frontTableBody.html('');
        deliveryList.html('');
        deleteDeliveryLayout(view);
        if (Object.keys(currentProducts).length > 0) {
            for (var i in currentProducts) {
                deletedProducts[i] = currentProducts[i];
                delete currentProducts[i];
            }
        }
        if (params.action === 'update') {
            jQuery('.deleted-delivery').val(JSON.stringify(currentDelivery[value]));
        }
        if (Object.keys(currentDelivery).length > 0) {
            for (var i in currentDelivery) {
                delete currentDelivery[i];
            }
        }
        generatedDeletedItems();
    }
}

function getDeliveryProducts(view) {
    var loader = Ladda.create(document.querySelector('#add-delivery-' + view.data('value')));
    loader.start();
    jQuery.ajax({
        url: params.searchProductAction,
        type: 'POST',
        data: {
            id: view.data('value'),
            authenticityToken: params.token
        }
    }).success(function (s) {
        loader.stop();
        generateProductDeliveryList(s);
        jQuery('.add-delivery').addClass('disabled');
    }).fail(function (e) {
        loader.stop();
        showToast("Terjadi kesalahan");
    });
}

function generateProductDeliveryList(data) {
    for (var i = 0; i < data.length; i++) {
        var model = data[i];
        var value = model.packageProductId;
        tempProducts[value] = model;
        if (tempProducts.hasOwnProperty(value) && !currentProducts.hasOwnProperty(value)) {
            currentProducts[value] = model;
            frontTableBody.append(generateHtmlTemplateListItem(model));
        }
    }
}

function deleteDeliveryLayout(view) {
    view.parent().parent().remove();
}