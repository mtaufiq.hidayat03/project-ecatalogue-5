package models.purchasing.pembayaran;

import models.BaseKatalogTable;
import models.purchasing.contracts.AttachmentContract;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import utils.DateTimeUtils;

import java.io.File;

/**
 * @author HanusaCloud on 10/31/2017
 */
@Table(name = "paket_pembayaran_lampiran")
public class PembayaranLampiran extends BaseKatalogTable implements AttachmentContract {

    @Id
    public Long id;
    public Long model_id;
    public Integer category;
    public Long paket_id;
    public String file_name;
    public String original_file_name;
    public Long file_size;
    public String file_sub_location;
    public String deskripsi;
    public Integer posisi_file;
    public Integer active;
    public Long doc_attachment_id;

    public PembayaranLampiran() {}

    public PembayaranLampiran setPayment(Pembayaran pembayaran) {
        this.model_id = pembayaran.getId();
        this.paket_id = pembayaran.getPackageId();
        this.active = 1;
        this.category = pembayaran.getTransactionType();
        return this;
    }

    @Override
    public PembayaranLampiran position(int position) {
        this.posisi_file = position;
        return this;
    }

    @Override
    public PembayaranLampiran setFile(File file) {
        this.original_file_name = file.getName();
        this.file_size = file.length();
        this.file_sub_location = DateTimeUtils.generateDirFormattedDate();
        this.file_name = regenerateFileName();
        renameTo(file);
        return this;
    }

    @Override
    public String getDownloadUrl() {
        return generateDownloadUrl("purchasing.PembayaranCtr.downloadLampiran");
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Long getPackageId() {
        return paket_id;
    }

    @Override
    public String getFileName() {
        return file_name;
    }

    @Override
    public String getSubLocation() {
        return file_sub_location;
    }

    @Override
    public String getOriginalFileName() {
        return original_file_name;
    }

    @Override
    public Integer getCreatedBy() {
        return created_by;
    }

    @Override
    public Long getModelId() {
        return model_id;
    }

    @Override
    public Long getBlobId() {
        return this.doc_attachment_id;
    }

    @Override
    public void saveAttachment() {
        this.id = (long) save();
    }

    @Override
    public void setBlobId(Long blobId) {
        this.doc_attachment_id = blobId;
    }

}
