package models.user;

import models.BaseKatalogTable;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Table(name="user_role_komoditas_override")
public class UserRoleKomoditasOverride extends BaseKatalogTable {

	@Id
	public Long id;
	public boolean active;
	public Long komoditas_id;
	public Long user_id;

	@Transient
	public String komoditas_name;

	public UserRoleKomoditasOverride() {}

	public UserRoleKomoditasOverride(Long itemId, Long userId) {
		this.komoditas_id = itemId;
		this.user_id = userId;
		this.active = true;
	}

	public static List<UserRoleKomoditasOverride> findByUserId(long id){
		String query = "select uri.id, uri.komoditas_id, uri.user_id, ri.nama_komoditas as komoditas_name from user_role_komoditas_override uri " +
				"inner join komoditas ri on uri.komoditas_id = ri.id where uri.active = 1 and uri.user_id = ?";

		return Query.find(query, UserRoleKomoditasOverride.class, id).fetch();
	}

	public static Set<Long> findByUserIdMap(long id){
		List<UserRoleKomoditasOverride> userKomoditas = findByUserId(id);
		return userKomoditas.stream()
				.map(i -> i.komoditas_id)
				.collect(Collectors.toSet());
	}

	public static Integer getTotalByUserIdKomId(Long id, Long komId) {
		final String query = "SELECT COUNT(*) as total FROM user_role_komoditas_override WHERE user_id = ? AND komoditas_id = ?";
		return Query.find(query, Integer.class, id, komId).first();
	}

	public static Integer getTotalByUserId(Long id) {
		final String query = "SELECT COUNT(*) as total FROM user_role_komoditas_override WHERE user_id = ?";
		return Query.find(query, Integer.class, id).first();
	}

	public static void deleteByUserId(long userId) {
		String query = "delete from user_role_komoditas_override where user_id = ?";
		Query.update(query, userId);
	}

	public static void deleteById(Long id){
		String query = "delete from user_role_komoditas_override where id = ?";
		Query.update(query,id);
	}
}
