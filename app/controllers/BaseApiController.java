package controllers;

import play.mvc.After;
import play.mvc.Before;
import play.mvc.Controller;

public class BaseApiController extends Controller {

    @Before
    public static void check(){

    }

    @After
    public static void release(){

    }
}
