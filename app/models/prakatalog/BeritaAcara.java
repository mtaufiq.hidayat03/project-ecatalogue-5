package models.prakatalog;

//import com.sun.org.apache.xpath.internal.operations.Bool;
import models.BaseKatalogTable;
import models.common.DokumenInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import play.data.binding.As;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.DateTimeUtils;
import utils.LogUtil;

import javax.persistence.Transient;
import java.io.File;
import java.util.Date;
import java.util.List;

import org.hibernate.annotations.Columns;

import ext.DateBinder;

/**
 * Created by dadang on 7/21/17.
 */
@Table(name = "berita_acara")
public class BeritaAcara extends BaseKatalogTable {

	@Id
	public Long id;

    public Long penawaran_id;
    
    public Long dokumen_template_id;

	public String no_ba;

	public String dokumen_ba;

	public Date tanggal_ba;

	public String deskripsi;

	public String nama_file;

	public Long blb_id;

	public Boolean active;

	@Transient
	public String url_download;

	@Transient
	public String jenis_dokumen;

	@Transient
	public String tanggal_berita_acara;

	public static List<BeritaAcara> findByPenawaranId(long penawaran_id){

		String query = "SELECT ba.id, ba.penawaran_id, ba.no_ba, ba.dokumen_ba, ba.deskripsi, ba.tanggal_ba, dt.jenis_dokumen, ba.created_date, " +
				"ba.nama_file, ba.blb_id  FROM berita_acara ba JOIN dokumen_template dt ON ba.dokumen_template_id = dt.id " +
				"WHERE ba.active = 1 AND ba.penawaran_id="+penawaran_id;
		List<BeritaAcara> result = Query.find(query,BeritaAcara.class).fetch();
		for(BeritaAcara ba: result){
			if(ba.blb_id != null){
				BlobTable bt = BlobTable.findByBlobId(ba.blb_id);
				ba.url_download = bt.getDownloadUrl(null);
			}
		}

		return result;
	}


	public void simpanBA(Long penawaranId, Long dokumenId, String noBa, String mytextarea){
		this.penawaran_id = penawaranId;
		this.dokumen_template_id = dokumenId;
		this.no_ba = noBa;
		this.dokumen_ba = mytextarea;
		this.active = true;
		this.save();
	}


	public void updateBA(Long baId, String mytextarea){
		String query = "UPDATE berita_acara SET dokumen_ba = ? WHERE id=?";
		Query.update(query,mytextarea,baId);
	}

	public void deleteFileBA(Long baId){
		String query ="UPDATE berita_acara SET nama_file = null, blb_id = null WHERE id=?";
		Query.update(query,baId);

	}

	public void deleteBa(Long baId){
		BeritaAcara ba = BeritaAcara.findById(baId);
		ba.active = false;
		ba.setDeleted();
		ba.save();
	}

	public String getReadableCreatedDate() {
		return DateTimeUtils.parseToReadableDate(created_date);
	}

	public String getFormattedBaDate() {
		return DateTimeUtils.parseToReadableDate(tanggal_ba);
	}

	public Date parseBaDate() {
		return DateTimeUtils.parseDdMmYyyy(tanggal_berita_acara);
	}


	public String getFileUrl() {
		String url = "";

		if(blb_id != null && blb_id.toString() != ""){
			BlobTable blobTable = BlobTable.findByBlobId(blb_id);
			if(blobTable != null){
				DokumenInfo dokumenInfo = DokumenInfo.findDokumenKontrak(blobTable);
				url = dokumenInfo.download_url;
			}
		}

		return url;
	}
}
