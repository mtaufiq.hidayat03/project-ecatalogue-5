package models.katalog;

import java.util.ArrayList;
import java.util.List;

import models.BaseKatalogTable;
import models.common.AktifUser;
import models.penyedia.PenyediaKontrak;
import models.util.produk.Perbandingan;
import play.Logger;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.DateTimeUtils;

import javax.persistence.Transient;

@Table(name = "produk_atribut_value")
public class ProdukAtributValue extends BaseKatalogTable {

	@Id
	public Long id;
	public Long produk_id;
	public Long produk_kategori_atribut_id;
	public String atribut_value;
	public boolean active;

	@Transient
	public String label_atribut;

	public ProdukAtributValue() {}

	public ProdukAtributValue(Long produk_id, String attrValue, Long additionalId) {
		this.produk_id = produk_id;
		this.atribut_value = attrValue;
		this.produk_kategori_atribut_id = additionalId;
		this.active = true;
	}

	public static void UpdateProdukAtributValue(Long produk_id, Long additionalId){
		int userId = AktifUser.getActiveUserId();
		String date = DateTimeUtils.getStandardDateTime();
		String query = "update produk_atribut_value SET active = 0, modified_date = :date, modified_by = :userId WHERE produk_id = :produk_id AND produk_kategori_atribut_id = :additionalId AND active = 1";

		Query.update(query, date, userId, produk_id, additionalId);

	}

	public static List<ProdukAtributValue> findByProdukId(long produk_id){
		String query = "select pa.id,p.id as produk_id,pa.produk_kategori_atribut_id,pa.atribut_value,pk.label_atribut from produk p\n" +
				"left join produk_kategori_atribut pk on p.produk_kategori_id = pk.produk_kategori_id and pk.active = 1\n" +
				"join produk_atribut_value pa on pa.produk_kategori_atribut_id = pk.id and pa.produk_id = p.id and pa.active = 1\n" +
				"where p.id = ? order by pa.produk_kategori_atribut_id asc";

		return Query.find(query, ProdukAtributValue.class, produk_id).fetch();
	}

	public static List<Perbandingan> getPerbandinganSpek(long produk_id){
		List<ProdukAtributValue> produkAtValueAkctive = findByProdukId(produk_id);
		List<Perbandingan> perbandinganList = new ArrayList<>();
		for(ProdukAtributValue p : produkAtValueAkctive){
			Perbandingan perbandingan = new Perbandingan();
			perbandingan.nama = p.label_atribut;
			perbandingan.sesudah = p.atribut_value;
			perbandinganList.add(perbandingan);
		}

		ProdukAtributValue before = ProdukAtributValue
				.find("produk_id = ? and active != 1 order by created_date DESC", produk_id).first();
		if(before != null){
			String queryBefore = "select pa.id, pa.produk_id, pa.produk_kategori_atribut_id, pa.atribut_value, " +
					"pk.label_atribut as label_atribut" +
					" from produk_atribut_value pa join produk_kategori_atribut pk on pa.produk_kategori_atribut_id = pk.id" +
					" where pa.active != 1 and pa.produk_id = ? and pa.created_date = ? order by pa.created_date DESC";
			List<ProdukAtributValue> produkAtValueBefore = Query.
					find(queryBefore, ProdukAtributValue.class, produk_id, before.created_date).fetch();
			for(Perbandingan p : perbandinganList){
				for (ProdukAtributValue pb : produkAtValueBefore){
					if(p.nama.equalsIgnoreCase(pb.label_atribut)){
						p.sebelum = pb.atribut_value;
					}
				}
			}
		}

		return perbandinganList;
	}

	/**
	 * @param pkid produk_kategori_atribut_id*/

	public static ProdukAtributValue getDetail(long pkid){
		return ProdukAtributValue.find("produk_kategori_atribut_id = ? and active = 1", pkid).first();
	}

	public static Integer getTotalValueByAttribute(Long id) {
		return Query.find("SELECT COUNT(*) as total FROM produk_atribut_value " +
				"WHERE produk_kategori_atribut_id =? AND active =?", Integer.class, id, AKTIF).first();
	}

	public String getCategoriAttrId() {
		return String.valueOf(produk_kategori_atribut_id);
	}

	public boolean isRequired(){
		ProdukKategoriAtribut obj = ProdukKategoriAtribut.findById(produk_kategori_atribut_id);
		return obj.wajib_diisi;
	}
	//	public String komoditas_produk_atribut_tipe_label;
//	public String label_atribut;
//	public String atribut_value;
//	public static List<ProdukAtributValue> findByProduk(Produk produk)
//	{
//		String sql="SELECT komoditas_produk_atribut_tipe_label, label_atribut, atribut_value FROM produk_kategori_atribut pka"
//				+ " LEFT JOIN produk_atribut_value pkv ON pka.id=pkv.produk_kategori_atribut_id "
//				+ " WHERE produk_kategori_id=? AND produk_id=?"
//				+ " ORDER BY posisi_item";
//		return Query.find(sql, ProdukAtributValue.class, produk.produk_kategori_id, produk.id).fetch();
//	}
	
}
