package controllers;

import models.util.Config;
import play.mvc.Http.Cookie;
import play.mvc.Router;

public class LoginCtr extends BaseController{
	
	public static void index(){
		if(getAktifUser() != null){
			PublikCtr.home();
		}

		clearUserSession();
		Cookie cookie=request.cookies.get("loginTab");
		String loginTab="#penyedia";
		if(cookie!=null)
			loginTab=cookie.value;

		Config conf = Config.getInstance();
		String callbackUrl = Router.reverse("admin.UserCtr.loginCentrumCallback").url;
		String centrumUrl = String.format("%s/OAuthCtr/authenticate?client_id=%s&redirect_uri=%s",
				conf.centrum_url,conf.centrum_client_id, callbackUrl);

		render(centrumUrl, loginTab);
	}

}
