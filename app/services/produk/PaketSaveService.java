package services.produk;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import ext.FormatUtils;
import models.ServiceResult;
import models.api.Rup;
import models.common.AktifUser;
import models.jcommon.util.CommonUtil;
import models.katalog.Komoditas;
import models.katalog.Produk;
import models.katalog.ProdukHarga;
import models.katalog.komoditasmodel.KomoditasAtributHarga;
import models.masterdata.Kurs;
import models.masterdata.KursNilai;
import models.penyedia.Penyedia;
import models.purchasing.Paket;
import models.purchasing.PaketAnggotaUlp;
import models.purchasing.PaketProduk;
import models.purchasing.PaketSumberDana;
import models.purchasing.form.PaketForm;
import models.purchasing.negosiasi.PaketProdukHeader;
import models.purchasing.negosiasi.PaketProdukNegoDetail;
import models.purchasing.riwayat.PaketRiwayat;
import models.purchasing.status.PaketStatus;
import models.util.produk.ProdukHargaJson;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.Play;
import play.i18n.Messages;
import utils.DateTimeUtils;
import utils.LogUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static models.purchasing.status.PaketStatus.DRAFT;

/**
 * @author HanusaCloud on 4/9/2020 3:20 PM
 */
public class PaketSaveService {

    public static final String TAG = "PaketSaveService";

    public static List<PaketAnggotaUlp> saveUlps(AktifUser aktifUser, PaketForm paket, Long paketId) {
        List<PaketAnggotaUlp> paketAnggotaUlps = new ArrayList<>();
        if (paket.ulp_anggota_id == null || paket.ulp_anggota_id.length == 0) {
            return paketAnggotaUlps;
        }
        List<PaketAnggotaUlp> existings = PaketAnggotaUlp.getMembersByPackageIdFront(paketId);
        if (!existings.isEmpty()) {
            LogUtil.debug(TAG, "existings ulp.. delete all");
            PaketAnggotaUlp.deleteByPaketId(paketId);
        }
        if (aktifUser != null && aktifUser.is_panitia_ulp && paket.ulp_anggota_id != null) {
            for (int i = 0; i < paket.ulp_anggota_id.length; i++) {
                long ulpUserId = paket.ulp_anggota_id[i];
                String ulpNip = paket.ulp_anggota_nip[i];
                PaketAnggotaUlp paketAnggotaUlp = new PaketAnggotaUlp();
                paketAnggotaUlp.paket_id = paketId;
                paketAnggotaUlp.anggota_user_id = ulpUserId;
                paketAnggotaUlp.anggota_nip = ulpNip;
                paketAnggotaUlp.active = true;
                paketAnggotaUlp.id = Long.valueOf(paketAnggotaUlp.save());
                paketAnggotaUlps.add(paketAnggotaUlp);
            }
        }
        return paketAnggotaUlps;
    }

    public static PaketRiwayat savePaketRiwayat(Long paketId, PaketStatus paketStatus) {
        PaketRiwayat paketRiwayat = new PaketRiwayat();
        paketRiwayat.paket_id = paketId;
        paketRiwayat.deskripsi = PaketRiwayat.DESKRIPSI_PAKET_DIBUAT;
        paketRiwayat.status_paket = paketStatus.getDetailStatus();
        paketRiwayat.id = Long.valueOf(paketRiwayat.save());
        return paketRiwayat;
    }

    public static Long editMode(
            Boolean isEdit,
            Komoditas komoditas,
            Long paketId,
            KursNilai kursNilai,
            List<PaketProduk> paketProdukList
    ) {
        Long negoHeaderId = -1L;
        if (!isEdit) {
            if (komoditas.perlu_negosiasi_harga) {
                //nego header
                PaketProdukHeader negoHeader = new PaketProdukHeader();
                negoHeader.paket_id = paketId;
                negoHeader.revisi = 0;
                if (kursNilai != null && kursNilai.kurs_id_from != Kurs.kurs_utama) {
                    negoHeader.kurs_tanggal = kursNilai.tanggal_kurs;
                    negoHeader.kurs_nilai = kursNilai.nilai_tengah;
                    negoHeader.kurs_id_from = kursNilai.kurs_id_from;
                }
                negoHeaderId = Long.valueOf(negoHeader.save());
                Logger.info("revisi " + negoHeader.revisi.toString());

                // nego detail
                for (PaketProduk pp : paketProdukList) {
                    PaketProdukNegoDetail ppnd = new PaketProdukNegoDetail();
                    ppnd.nego_header_id = negoHeaderId;
                    ppnd.produk_id = pp.produk_id;
                    ppnd.paket_produk_id = pp.id;
                    ppnd.kuantitas = pp.kuantitas;
                    ppnd.harga_satuan = pp.harga_satuan;
                    ppnd.harga_ongkir = pp.harga_ongkir;
                    ppnd.harga_total = pp.harga_total;
//                            if(kursNilai != null && kursNilai.kurs_id_from != Kurs.kurs_utama){
                    ppnd.konversi_harga_satuan = pp.konversi_harga_satuan;
                    ppnd.konversi_harga_ongkir = pp.konversi_harga_ongkir;
                    ppnd.konversi_harga_total = pp.konversi_harga_total;
                    ppnd.catatan = pp.catatan;
//                            }
                    ppnd.save();
                }
            }
        }
        return negoHeaderId;
    }

    public static PaketStatus savePaketStatus(
            Long paketId,
            AktifUser aktifUser,
            Komoditas komoditas,
            PaketForm paket,
            Long negoHeaderId
    ) {
        PaketStatus paketStatus = PaketStatus.getLatestStatusByPackageId(paketId);
        if (paketStatus == null) {
            LogUtil.debug(TAG, "existing package not found, init..");
            paketStatus = new PaketStatus();
        }
        paketStatus.paket_id = paketId;
        if (paket.isDefaultDraft()) {
            LogUtil.debug(TAG, "it seems empty or draft, set to draft as default");
            paketStatus.status_paket = DRAFT;
        } else {
            paketStatus.status_paket = TextUtils.isEmpty(paket.status) ? PaketStatus.STATUS_PERSIAPAN : paket.status;
        }
        paketStatus.from_status = "panitia";
        if (aktifUser.role_basic_name.equalsIgnoreCase("PPK")) {
            paketStatus.from_status = aktifUser.role_basic_name.toLowerCase();
        }
        paketStatus.di_tolak = false;
        if (komoditas.perlu_negosiasi_harga) {
            paketStatus.status_negosiasi = PaketStatus.STATUS_NEGOSIASI_PERLU_NEGO;
            paketStatus.nego_header_id = negoHeaderId;
        }

        paketStatus.id = Long.valueOf(paketStatus.save());
        return paketStatus;
    }

    public static ServiceResult<List<PaketProduk>> calculatePackageProduct(
            PaketForm paket,
            Long wId,
            Paket paketToSave
    ) {
        List<PaketProduk> paketProdukList = new ArrayList<>();
        if (paket.produk_id == null || paket.produk_id.length == 0) {
            LogUtil.debug(TAG, "Oops.. paket produk is empty");
            return new ServiceResult<>(true, "success with empty response!", paketProdukList);
        }
        Logger.info("paket produk length " + paket.produk_id.length);
        for (int i = 0; i < paket.produk_id.length; i++) {
            PaketProduk paketProduk = new PaketProduk();
            if (paket.prp_id == null) {
                paket.prp_id = new Long(0);
            }
            if (paket.kbp_id == null) {
                paket.kbp_id = new Long(0);
            }

            paketProduk.produk_id = paket.produk_id[i];
            paketProduk.kuantitas = paket.produk_kuantitas[i];
            Logger.info("==== produk kuantitas " + paket.produk_kuantitas[i]);

            //				Double ongkir = new Double(0);
            Double dOngkir = new Double(0);
            // besok lanjut
            //sampe sini trace ongkir, wilayah id paket
            try {
                BigDecimal bd = new BigDecimal(0);
                bd = paket.harga_ongkir[i];
                dOngkir = bd.doubleValue();
            } catch (Exception e) {
            }

            paketProduk.harga_ongkir = dOngkir;
//				ongkir = ProdukHarga.getOngkosKirim(paket.produk_id[i], paket.prp_id, paket.kbp_id);

            Produk p = Produk.findById(paket.produk_id[i]);

            if(paket.produk_kelas_harga[i].equalsIgnoreCase("kabupaten")
                    || paket.produk_kelas_harga[i].equalsIgnoreCase("provinsi")){
                KomoditasAtributHarga kah = KomoditasAtributHarga.findHargaUtamaByKomoditas(Long.valueOf(p.komoditas_id));
                ProdukHarga phg = ProdukHarga.getByProductIdAndAtributidAndPriceDate(p.id, kah.id, p.harga_tanggal.toString());
                List<ProdukHargaJson> harga_utama = new ArrayList<>();
                if(null!=phg) {
                    harga_utama = new Gson().fromJson(phg.harga, new TypeToken<List<ProdukHargaJson>>() {
                    }.getType());
                }

                if (paket.produk_kelas_harga[i].equalsIgnoreCase("kabupaten")) {
                    for(int z = 0; z < harga_utama.size(); z++){
                        ProdukHargaJson phu = harga_utama.get(z);
                        if(phu.kabupatenId == paket.produk_daerah_id[i]) {
                            paketProduk.harga_satuan = phu.harga;
                        }
                    }
                    paketProduk.kabupaten_id = wId;
                    Logger.info("harga_ongkirnya " + paketProduk.harga_ongkir);
                } else if (paket.produk_kelas_harga[i].equalsIgnoreCase("provinsi")) {
                    for(int z = 0; z < harga_utama.size(); z++){
                        ProdukHargaJson phu = harga_utama.get(z);
                        if(phu.provinsiId == paket.produk_daerah_id[i]) {
                            paketProduk.harga_satuan = phu.harga;
                        }
                    }
                    paketProduk.provinsi_id = wId;
                    Logger.info("harga_ongkirnya " + paketProduk.harga_ongkir);
                }
            }else{
                paketProduk.harga_satuan = p.harga_utama.doubleValue();
                Logger.info("harga_ongkirnya " + paketProduk.harga_ongkir);
            }

            paketProduk.harga_total = (paketProduk.kuantitas.doubleValue() * paketProduk.harga_satuan) + paketProduk.harga_ongkir;
            paketProduk.konversi_harga_ongkir = paketProduk.harga_ongkir * paketToSave.kurs_nilai;
            paketProduk.konversi_harga_satuan = paketProduk.harga_satuan * paketToSave.kurs_nilai;
            paketProduk.konversi_harga_total = paketProduk.harga_total * paketToSave.kurs_nilai;

            paketProduk.catatan = paket.produk_catatan[i];

            // agr total harga produk paket
            paketProduk.active = true;

//				Logger.info("PAKET PRODUK HARGA WILAYAH ID " + paketProduk.wilayah_id );

            paketProdukList.add(paketProduk);
            if (!paketToSave.commodity.apakah_stok_unlimited) {
                BigDecimal stok = Produk.cekStok(paketProduk.produk_id, paketProduk.kuantitas);
                Produk produk = Produk.findById(paketProduk.produk_id);
                PaketProduk errorProduct = new PaketProduk();
                errorProduct.produk_id = produk.id;
                if (stok.compareTo(paketProduk.kuantitas) < 0) {
                    return new ServiceResult<>(
                            false,
                            Messages.get("paket.stok.kurang", stok, paketProduk.kuantitas),
                            Arrays.asList(errorProduct)
                    );
                }
            }

//                if(komoditas.apakah_stok_unlimited == false){
//                    Produk prod = Produk.findById(paketProduk.produk_id);
//                    BigDecimal stokBerkurang = new BigDecimal(String.valueOf(paketProduk.kuantitas));
//                    BigDecimal stokAwal = new BigDecimal(String.valueOf(prod.jumlah_stok));
//                    BigDecimal stokSisa = new BigDecimal(0);
//                    stokSisa = stokAwal.subtract(stokBerkurang);
//
//                    Produk p = Produk.findById(prod.id);
//                    p.jumlah_stok = stokSisa;
//                    p.save();
//                }
        }
        return new ServiceResult<>(true, "success", paketProdukList);
    }

    public static Paket setReqs(
            PaketForm paket,
            AktifUser aktifUser,
            Long wId,
            Long kursId
    ) {
        // get kurs
        // get kurs

        if (paket.gunakan_alamat_satker != null && paket.gunakan_alamat_satker == 1) {
            paket.pengiriman_alamat = paket.alamat_satuan_kerja;
            paket.pengiriman_kabupaten = paket.satuan_kerja_kabupaten;
        }

        paket.kurs_id = kursId;
        Paket paketToSave = populatePaketForm(paket);
        Logger.info("[Paket save] get kurs id: " + paket.kurs_id);
        if (paket.kurs_id != null) {
            paketToSave.kursNilai = KursNilai.getNewestKursByKursId(paket.kurs_id);
        }
        // get data penyedia
        if (paket.penyedia_id != null) {
            paketToSave.penyedia = Penyedia.findById(paket.penyedia_id);
        }
        // get komoditas
        if (paket.komoditas_id != null) {
            paketToSave.commodity = Komoditas.findById(paket.komoditas_id);
            //handling null peng
            if (null == paketToSave.pengiriman_kabupaten) {
                paketToSave.pengiriman_kabupaten = wId;
                paket.pengiriman_kabupaten = wId;
            }
        }
        // generate no paket
        if (paketToSave.commodity != null && paketToSave.commodity.kode_komoditas != null) {
            paketToSave.generateNoPaket(paketToSave.commodity.kode_komoditas);
        }
        paketToSave.kurs_nilai = new Double(1);
        paketToSave.kurs_tanggal = new Date();
        paketToSave.kurs_id_from = Kurs.kurs_utama;
        if (paketToSave.kursNilai != null) {
            paketToSave.kurs_nilai = paketToSave.kursNilai.nilai_tengah;
            paketToSave.kurs_tanggal = paketToSave.kursNilai.tanggal_kurs;
            paketToSave.kurs_id_from = paketToSave.kursNilai.kurs_id_from;
        }
        paketToSave.agr_paket_produk = paket.produk_id != null ? paket.produk_id.length : 0;
        paketToSave.agr_paket_produk_sudah_diterima_semua = 0;
        paketToSave.agr_konversi_harga_total = new Double(0);

        // get rup for validation
        if (paketToSave.rup_id != null){
            Rup rup = Rup.getRupForPaket(paketToSave.rup_id);
            if (rup != null){
                paketToSave.nama_paket = rup.nama;
                paketToSave.tahun_anggaran = String.valueOf(rup.tahun_anggaran);
                paketToSave.satker_id = String.valueOf(rup.id_sat_ker);
                paketToSave.kldi_id = rup.kode_kldi;
                paketToSave.satuan_kerja_nama = rup.nama_kldi;
                paketToSave.kldi_jenis = rup.jenis_kldi;
                paketToSave.provinsi_id = rup.prp_id;
                paketToSave.kabupaten_id = rup.kbp_id;
            }
        }
        //paket ppk
        if (aktifUser != null && aktifUser.isPpk()) {
            paketToSave.ppk_user_id = aktifUser.user_id;
            paketToSave.getPpkName();
            paketToSave.ppk_email = paket.email_pemesan;
            paketToSave.ppk_jabatan = paket.jabatan_panitia;
            paketToSave.ppk_sertifikat = paket.no_sert_pbj_pemesan;
            paketToSave.ppk_nip = paket.nip_pemesan;
            paketToSave.ppk_telepon = paket.no_telp_pemesan;
        }
        return paketToSave;
    }

    private static Paket populatePaketForm(PaketForm paketForm) {
        Paket paketPopulated = new Paket(paketForm.komoditas_id,
                paketForm.penyedia_id == null ? 0 : paketForm.penyedia_id,
                paketForm.rup_id == null ? 0 : paketForm.rup_id,
                null,
                paketForm.nama_paket == null ? null : paketForm.nama_paket,
                paketForm.nama_satuan_kerja == null ? null : paketForm.nama_satuan_kerja,
                paketForm.alamat_satuan_kerja == null ? null : paketForm.alamat_satuan_kerja,
                paketForm.satuan_kerja_kabupaten, paketForm.pengiriman_alamat, paketForm.pengiriman_kabupaten,
                paketForm.npwp_satuan_kerja == null ? null : paketForm.npwp_satuan_kerja,
                paketForm.id_pemesan == null ? 0 : paketForm.id_pemesan,
                paketForm.email_pemesan == null ? null : paketForm.email_pemesan,
                paketForm.no_telp_pemesan == null ? null : paketForm.no_telp_pemesan,
                paketForm.id_ppk == null ? 0 : paketForm.id_ppk,
                paketForm.jabatan_ppk == null ? null : paketForm.jabatan_ppk,
                paketForm.nip_ppk == null ? null : paketForm.nip_ppk,
                paketForm.kurs_id == null ? 0 : paketForm.kurs_id,
                new Double(0),
                new Date(),
                0,
                new Double(0),
                paketForm.nip_pemesan == null ? null : paketForm.nip_pemesan,
                paketForm.jabatan_panitia == null ? null : paketForm.jabatan_panitia,
                paketForm.email_ppk == null ? null : paketForm.email_ppk,
                paketForm.no_telp_ppk == null ? null : paketForm.no_telp_ppk,
                paketForm.satker_id == null ? null : paketForm.satker_id,
                paketForm.kldi_jenis == null ? null : paketForm.kldi_jenis,
                paketForm.kldi_id == null ? null : paketForm.kldi_id,
                paketForm.tahun_anggaran == null ? null : paketForm.tahun_anggaran,
                paketForm.no_sert_pbj_ppk == null ? null : paketForm.no_sert_pbj_ppk,
                paketForm.no_sert_pbj_pemesan == null ? null : paketForm.no_sert_pbj_pemesan,
                paketForm.prp_id == null ? 0 : paketForm.prp_id,
                paketForm.kbp_id == null ? 0 : paketForm.kbp_id,
                paketForm.gunakan_alamat_satker == null ? 0 : paketForm.gunakan_alamat_satker
        );

        return paketPopulated;
    }

    public static List<PaketSumberDana> saveSumberDana(PaketForm paket, Long paketId) {
        List<PaketSumberDana> sumberDanas = new ArrayList<>();
        if (paket.id_sumber_dana == null || paket.id_sumber_dana.length == 0) {
            LogUtil.debug(TAG, "Oops.. sumber dana is empty!");
            return sumberDanas;
        }
        List<PaketSumberDana> sources = PaketSumberDana.getFundSource(paketId);
        if (!sources.isEmpty()) {
            LogUtil.debug(TAG, "sumber dana existing exists, delete all");
            PaketSumberDana.deleteByPaketId(paketId);
        }
        for (int i = 0; i < paket.id_sumber_dana.length; i++) {
            PaketSumberDana paketSumberDana = new PaketSumberDana();
            if (!CommonUtil.isEmpty(paket.kode_anggaran[i])) {
                paketSumberDana.paket_id = paketId;
                paketSumberDana.sumber_dana_id = paket.id_sumber_dana[i];
                paketSumberDana.kode_anggaran = paket.kode_anggaran[i];
                paketSumberDana.id = Long.valueOf(paketSumberDana.save());
                sumberDanas.add(paketSumberDana);
            }
        }
        return sumberDanas;
    }

    public static List<PaketProduk> savePaketProduk(List<PaketProduk> paketProdukList, Long paketId) {
        List<PaketProduk> results = new ArrayList<>();
        if (paketProdukList.isEmpty()) {
            LogUtil.debug(TAG, "Oops.. package product is empty!");
            return results;
        }
        List<PaketProduk> existings = PaketProduk.findIdPaketAndActive(paketId);
        /* if (!existings.isEmpty()) {
            LogUtil.debug(TAG, "existings product not empty, delete all!");
            PaketProduk.deleteByPaketId(paketId);
        } */
        Logger.info("size"+paketProdukList.size());
        for (int i = 0; i < paketProdukList.size(); i++) {
            paketProdukList.get(i).paket_id = paketId;
            paketProdukList.get(i).id = (long) paketProdukList.get(i).save();
            Logger.info("id baru "+ paketProdukList.get(i).id);
            if (!existings.isEmpty()) {
                Logger.info("id lama" + existings.get(i).id);
                PaketProduk.deletePaketProdukById(existings.get(i).id);
                PaketProdukNegoDetail.updateDataPaketProdukId(paketProdukList.get(i).id,
                        paketProdukList.get(i).kuantitas, paketProdukList.get(i).harga_ongkir, paketProdukList.get(i).harga_total,
                        paketProdukList.get(i).konversi_harga_satuan, paketProdukList.get(i).konversi_harga_ongkir,
                        paketProdukList.get(i).konversi_harga_total, existings.get(i).id);
            }
            results.add(paketProdukList.get(i));
        }
        /*LogUtil.debug(TAG, "save products");
        for (PaketProduk pp : paketProdukList) {
            Logger.info("////// Produk " +pp.nama_produk+ " kuantitasnya " + pp.kuantitas);
            pp.paket_id = paketId;
            pp.id = Long.valueOf(pp.save());
            Logger.info("pp "+ pp.id);
            Logger.info("paket id"+paketId);
            results.add(pp);
            Logger.info("$$$$$$ Produk " +pp.nama_produk+ " kuantitasnya " + pp.kuantitas);
        } */
        return results;
    }

    public static ServiceResult checkPriceAgainstUser(AktifUser aktifUser, Double price) {
        if (aktifUser.isPp() && price > 200000000) {
            return new ServiceResult(Messages.get("PP hanya bisa beli paket dibawah 200 juta. " +
                    "Untuk membeli barang diatas 200 juta harap hubungi PPK"));
        }
        /* else if (aktifUser.isPpk() && price < 200000000) {
            return new ServiceResult(Messages.get("PPK hanya bisa beli paket diatas 200 juta"));
        } */
        return new ServiceResult(true, "success");
    }

    public static void sendEmail(Paket paketToSave, PaketForm paket) {
        String email = getPenyediaEmail(paketToSave.penyedia_id);
        String tglBuat = DateTimeUtils.parseToReadableDate(paketToSave.created_date);
        String totalHargaPaket = FormatUtils.formatCurrencyRupiah(paketToSave.agr_konversi_harga_total);
        Paket.sendMail(email,paketToSave.no_paket.replaceAll("-null","-"+paketToSave.id),
                paket.nama_satuan_kerja, paket.instansi, tglBuat, totalHargaPaket);

    }

    public static String getPenyediaEmail(Long penyediaId){
        String mail = "arieffajarpermana@gmail.com";
        Penyedia penyedia = Penyedia.findById(penyediaId);
        if (null != penyedia){
            if(Play.mode.isProd()){
                mail = penyedia.email;
            }
        }
        return mail;
    }

    public static void updateProductStock(Paket paketToSave, PaketForm paket) {
        for (int i = 0; i < paket.produk_id.length; i++) {
            Long produk_id = paket.produk_id[i];
            BigDecimal kuantitas = paket.produk_kuantitas[i];

            if (!paketToSave.commodity.apakah_stok_unlimited) {
                //update stok produk
                Produk.updateStokProduk(kuantitas, produk_id,"-");
            }
        }
    }

}
