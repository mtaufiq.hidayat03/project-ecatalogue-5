package models.util.sikap;

import java.io.Serializable;
import java.util.List;

/**
 * Created by raihaniqbal on 8/23/17.
 */
public class VerifikasiSikap implements Serializable {

	public Long[] identitas;
	public Long[] izin_usaha;
	public Long[] akta_pendirian;
	public Long[] akta_perubahan;
	public Long[] pemilik;
	public Long[] pengurus;
	public Long[] tenaga_ahli;
	public Long[] peralatan;
	public Long[] pengalaman;
	public Long[] pajak;
}
