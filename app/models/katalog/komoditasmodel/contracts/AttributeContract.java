package models.katalog.komoditasmodel.contracts;

import repositories.produkkategori.ProdukKategoriAtributRepository;

/**
 * @author HanusaCloud on 12/5/2017
 */
public interface AttributeContract {

    Long getId();

    default boolean isAllowedToBeDeleted() {
        return getId() != null && ProdukKategoriAtributRepository.getTotalCategoriAttributeByAttrId(getId()) == 0L;
    }

}
