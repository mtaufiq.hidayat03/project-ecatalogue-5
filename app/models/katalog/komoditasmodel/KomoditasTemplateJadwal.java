package models.katalog.komoditasmodel;

import models.BaseKatalogTable;
import models.katalog.form.FormKomoditas;
import models.prakatalog.Usulan;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

@Table(name = "komoditas_template_jadwal")
public class KomoditasTemplateJadwal extends BaseKatalogTable {

	@Id
	public Long id;
	public Long komoditas_id;
	@Required
	public String nama_template;
	public String deskripsi;
	public long tahapan_penawaran;
	public Long tahapan_negosiasi;
	public Integer agr_tahapan_jadwal;
	public boolean active;
	public boolean apakah_dipakai;

	@Transient
	public String nama_tahapan;

	public static List<KomoditasTemplateJadwal> getListDetailTemplate(long id){
		String query = "select template.id, template.nama_template, tahapan.nama_tahapan_alias as nama_tahapan " +
				"from komoditas_template_jadwal template " +
				"join komoditas_tahapan_template_jadwal tahapan_template on tahapan_template.template_id = template.id " +
				"join tahapan tahapan on tahapan_template.tahapan_id = tahapan.id " +
				"where template.id = ? and template.active = ? " +
				"order by template.id";

		return Query.find(query, KomoditasTemplateJadwal.class,id,1).fetch();
	}

	public static List<KomoditasTemplateJadwal> findByKomoditasId(long komoditas_id){
		return KomoditasTemplateJadwal.find("komoditas_id = ? and active = ?",komoditas_id, AKTIF).fetch();
	}

	public KomoditasTemplateJadwal setDataForm(FormKomoditas form) {
		if (!form.isCreateMode() && this.id == null) {
			this.id = form.templateJadwal.id;
		}
		this.komoditas_id = form.templateJadwal.komoditas_id;
		this.nama_template = form.templateJadwal.nama_template;
		this.deskripsi = form.templateJadwal.deskripsi;
		this.active = true;
		//this.agr_tahapan_jadwal = form.tahapan_arr.length;
		//this.tahapan_penawaran = form.penawaran;
		//this.tahapan_negosiasi = form.negosiasi;
		return this;
	}

	public void saveTemplateJadwal() {
		final long id = save();
		if (this.id == null) {
			this.id = id;
		}
	}

	public static Integer getTotalByCommodityId(Long id) {
		return Query.find("SELECT COUNT(*) FROM komoditas_template_jadwal " +
				"WHERE komoditas_id =? AND active =?", Integer.class, id, AKTIF).first();
	}

	public boolean isAllowToBeDeleted() {
		return Usulan.getTotalUsulanByTemplate(this.id) == 0;
	}

	public void softDelete() {
		this.active = false;
		setDeleted();
		save();
	}

}
