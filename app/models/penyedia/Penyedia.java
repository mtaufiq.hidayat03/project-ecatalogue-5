package models.penyedia;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import jobs.JobTrail.JobSendFillActivePenyedia;
import jobs.JobTrail.JobSetPenyediaElastic;
import models.BaseKatalogTable;
import models.elasticsearch.NewBase.HitsPenyedia;
import models.elasticsearch.NewBase.PenyediaElastic;
import models.common.AktifUser;
import models.katalog.Produk;
import models.masterdata.Kurs;
import models.penyedia.contracts.ProviderContract;
import models.prakatalog.Penawaran;
import models.prakatalog.Usulan;
import models.user.User;
import models.util.Config;
import models.util.RekananDetail;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import play.Logger;
import play.cache.Cache;
import play.data.validation.Required;
import play.db.jdbc.*;
import play.jobs.Job;
import play.libs.WS;
import services.elasticsearch.ElasticsearchConnection;

import javax.persistence.Transient;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * Created by dadang on 7/11/17.
 */
@CacheMerdeka
@Table(name = "penyedia")
public class Penyedia extends BaseKatalogTable implements ProviderContract {

	@Id
	public Long id;
	@Required
	public Long user_id;
	@Required
	public String nama_penyedia;
	public String alamat;
	public String website;
	public String email;
	public String no_fax;
	public String no_telp;
	public String no_hp;
	public String npwp;
	public String pkp;
	public String kode_pos;
	public String produk_api_url;
	public Long default_kurs_id;
	public Integer agr_penyedia_distributor;
	public Integer agr_penyedia_kontrak;
	public boolean active;
	public Long rkn_id;

	public Integer is_umkm;

	@Transient
	public Long komoditas_id;
	@Transient
	public String username;
	@Transient
	public String status_penyedia_distributor;

	public static final int MAX_SEARCH_USER_FROM_ADP  = 20;

	@Transient
	public List<String> lokasiKldi;

	@Transient
	public List<String> lokasiKldiSektoral;

	public Penyedia(){}
	
	public Penyedia(RekananDetail rkn, long user_id){
		this.nama_penyedia = rkn.rkn_nama;
		this.alamat = rkn.rkn_alamat;
		this.user_id = user_id;
		this.email = rkn.rkn_email;
		this.kode_pos = rkn.rkn_kodepos;
		this.no_fax = rkn.rkn_fax;
		this.no_telp = rkn.rkn_telepon;
		this.no_hp = rkn.rkn_mobile_phone;
		this.npwp = rkn.rkn_npwp;
		this.website = rkn.rkn_website;
		this.pkp = rkn.rkn_pkp;
		this.rkn_id = rkn.rkn_id;

		this.agr_penyedia_kontrak = 0;
		this.agr_penyedia_distributor = 0;
		this.default_kurs_id = Kurs.findKursUtama().id;
		this.active = true;
	}
	
	@Transient
	public transient List<PenyediaRepresentatif> representatives = new ArrayList<>();

	@Transient
	public transient List<PenyediaDistributor> distributors = new ArrayList<>();

	@Transient
	public String user_name;
	@Transient
	public User user;

	@CacheMerdeka
	public static List<Penyedia> getPenyedia(long penyedia_id){
		String query = "select p.id, p.nama_penyedia, p.active " +
				"from penyedia p " +
				"where p.active = ? and p.id = ? ";
		return Query.find(query, Penyedia.class,1, penyedia_id).fetch();
	}

	@Transient
	public static String key4 = "getCommodities";
	//@CacheMerdeka
	public static List<Penyedia> getCommodities(){
		List<Penyedia> result = Cache.get(key4,List.class);
		if(null == result){
			String query = "select p.id, p.nama_penyedia, p.active " +
					"from penyedia p " +
					"join penyedia_komoditas pk on pk.penyedia_id = p.id " +
					"where p.active = ? " +
					"order by p.nama_penyedia";
			try{
				result = Query.find(query, Penyedia.class,1).fetch();
				Cache.set(key4,result,"30mn");
			}catch (Exception e){
				result = new ArrayList<>();
			}
		}
		return result;
	}

	@Transient
	public static String key3 = "getVendorAPI";
	//@CacheMerdeka
	public static List<Penyedia> getVendorAPI(){
		List<Penyedia> result = Cache.get(key3,List.class);
		if(null == result){
			String query = "select py.id, pkm.komoditas_id, py.nama_penyedia " +
					"from penyedia py " +
					"left join penyedia_komoditas pkm on py.id = pkm.penyedia_id " +
					"join penyedia_kontrak pk on py.id = pk.penyedia_id " +
					"where pkm.komoditas_id in (77,78) and py.active = ? " +
					"group by py.id order by pkm.komoditas_id ";
			try{
				result = Query.find(query, Penyedia.class,1).fetch();
				Cache.set(key3,result,"30mn");
			}catch (Exception e){
				result = new ArrayList<>();
			}
		}
		return result;
	}

	//@CacheMerdeka
	@Transient
	public static String key2 = "getPenyediaKomoditas";
	public static List<Penyedia> getPenyediaKomoditas(long komoditas_id){

		List<Penyedia> result = Cache.get(key2+komoditas_id,List.class);
		if(null == result){
			String query = "select p.id, p.nama_penyedia, p.active " +
					"from penyedia p " +
					"join penawaran pw on p.id = pw.penyedia_id and pw.active = 1 " +
//					"join penyedia_komoditas pk on pk.penyedia_id = p.id " +
					"where p.active = ? and pw.komoditas_id = ? " +
					"group by p.id order by p.nama_penyedia";
			try{
				result = Query.find(query, Penyedia.class,1, komoditas_id).fetch();
				Cache.set(key2+komoditas_id,result,"30mn");
			}catch (Exception e){
				result = new ArrayList<>();
			}
		}


		return result;
	}

	@Transient
	public static String key1 = "getPenyediaKontrak";
	//@CacheMerdeka
	public static List<PenyediaElastic> getPenyediaKontrak(){
		List<PenyediaElastic> result = Cache.get(key1,List.class);
		if(null == result){
			result = new ArrayList<>();
			JobSetPenyediaElastic job1 = new JobSetPenyediaElastic(key1);
			job1.now();
//			new Job(){
//				public void doJob(){
//					List<PenyediaElastic> result2 = new ArrayList<>();
//					try{
//						result2 = Penyedia.getAllPenyediaOnElastic();
//						if(result2.size() > 0) {
//							Cache.set(key1, result2, "1d");
//						}
//					}catch (Exception e){
//					}
//				}
//			}.now();
		}
		return result;
	}

	public static Penyedia getUserProviderById(Long id) {
		final String query = "SELECT p.*, s.user_name, s.status_penyedia_distributor " +
				"FROM penyedia p " +
				"JOIN `user` s ON s.id = p.user_id " +
				"WHERE p.id =? AND p.active > 0";
		return Query.find(query, Penyedia.class, id).first();
	}

	public static Penyedia getProviderByUserId(Long id) {
		final String query = "SELECT p.*, u.user_name, u.status_penyedia_distributor " +
				"FROM penyedia p " +
				"JOIN `user` u ON u.id = p.user_id " +
				"WHERE p.user_id =? AND p.active > 0";
		return Query.find(query, Penyedia.class, id).first();
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public Long getUserId() {
		return this.user_id;
	}

	@Override
	public void setRepresentatives(List<PenyediaRepresentatif> list) {
		this.representatives = list;
	}

	@Override
	public void setDistributor(List<PenyediaDistributor> distributors) {
		this.distributors = distributors;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public boolean isUserIdEqual(Long user_id) {
		return this.user_id.equals(user_id);
	}

	public Long getRknId() {
		return this.rkn_id;
	}

	public String getUserName(){
		User u = User.findById(this.user_id);
		return u.user_name;
	}

	public static Penyedia getProviderByNpwp(String npwp2) {
		final String query = "SELECT p.* " +
				"FROM penyedia p " +
				"WHERE p.npwp =? AND p.active > 0";
		return Query.find(query, Penyedia.class, npwp2).first();
	}

	public List<String> locationSelected(){
		List<String> result = getPenyediaWilayah().stream().map(x-> x.kldi_id).collect(Collectors.toList());
		return result;
	}

	public  List<String> locationSektoralSelected(){
		List<String> result = getPenyediaSektoral().stream().map(x-> x.kldi_id).collect(Collectors.toList());
		return result;
	}

	public List<PenyediaWilayah> getPenyediaWilayah(){
		List<PenyediaWilayah> result = new ArrayList<>();
		try{
			result = PenyediaWilayah.find("penyedia_id = ?", getId()).fetch();
		}catch (Exception e){}
		return result;
	}

	public static List<Penyedia> getPenyediaPermohonan(){
		List<Penyedia> result = new ArrayList<>();
		try{
			result = Penyedia.find("id IN (SELECT DISTINCT penyedia_id FROM permohonan_pembaruan WHERE active = 1)").fetch();
		}catch (Exception e){}
		return result;
	}

	public List<PenyediaSektoral> getPenyediaSektoral(){
		List<PenyediaSektoral> result = new ArrayList<>();
		try{
			result = PenyediaSektoral.find("penyedia_id = ?", getId()).fetch();
		}catch (Exception e){}
		return result;
	}

	public String locationSelectedAsJsArray(){
		List<String> loc = locationSelected();
		String result = "[";
		for (String l:loc) {
			result += "'"+l.trim()+"',";
		}
		result +="]";
		return result;
	}
	@Transient
	public static String key5 = "getPenyediaKontrakDB";
	private static String collectPenyediaElastics(){
		String qry = "SELECT distinct(pn.id), pn.nama_penyedia, pn.active \n" +
				"\tFROM produk p \n" +
				"\tJOIN penawaran pw ON pw.id = p.penawaran_id \n" +
				"\tJOIN penyedia_kontrak pk ON pk.id = pw.kontrak_id and \n" +
				"\t\tpk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1\n" +
				"\tJOIN penyedia pn ON pn.id = pk.penyedia_id \n" +
				"\tJOIN komoditas k ON p.komoditas_id = k.id and \n" +
				"\t\t(k.apakah_stok_unlimited = 1 OR (k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0)) AND k.active = 1\n" +
				"\tWHERE p.active = 1 \n" +
				"\tAND p.apakah_ditayangkan = 1 \n" +
				"\tAND p.setuju_tolak = 'setuju'\n" +
				"\tAND (p.berlaku_sampai >= CURDATE() OR p.berlaku_sampai is null)\n" +
				"\tORDER BY pn.nama_penyedia;";
		List<Penyedia> list = Cache.get(key5,List.class);
		if(null == list) {
			list = Query.find(qry, Penyedia.class).fetch();
			if(list.size() >0 ){
				Cache.set(key5, list, "60mn");
			}
		}
		Logger.info("count :"+list.size());
		Map<Long,String> penyedia = list.stream().collect(Collectors.toMap(Penyedia::getId,
				Penyedia::setPreparePenyediaElastics));
		String result ="";
		if(null != penyedia) {
			List<String> dt = penyedia.values().stream().collect(Collectors.toList());
			result = dt.stream().collect(Collectors.joining(""));
		}
		return result;
	}
	private String setPreparePenyediaElastics(){
		String esIndex = Config.getInstance().elasticsearchIndex;
		String dt1 = "{ \"create\":  { \"_index\": \""+esIndex+"\", \"_type\": \""+ key1 +"\", \"_id\": \""+this.id+"\" }}\n";
		String dt2 = "{ \"id\": "+ id +", \"nama_penyedia\": \""+nama_penyedia+"\" }\n";
		return dt1+dt2;
	}
	public static Boolean sendfillActivePenyediaToElastic(){
		String modelUrl = key1;//getPenyediaKontrak
		String bulklUrl = "/_bulk";
		String penyediaJsonStr = Penyedia.collectPenyediaElastics();
		Logger.info("data : "+penyediaJsonStr);
		Boolean result = false;
		try {
			String urls = ElasticsearchConnection.getElasticurls()+modelUrl+bulklUrl;
			WS.HttpResponse response = WS.url(urls)
					.setHeader("Content-Type", "application/json")
					.body(penyediaJsonStr)
					.postAsync().get();
			result = response.success();
			Logger.info("result:"+result);
		} catch (InterruptedException e) {
			Logger.error("Error sendActivePenyediaToElastic:"+e.getMessage());
		} catch (ExecutionException e) {
			Logger.error("Error sendActivePenyediaToElastic:"+e.getMessage());
		}
		return result;
	}

	public static long getTotalPenyediaOnElastic(){
		String modelUrl = key1;//getPenyediaKontrak
		String bulklUrl = "/_count";
		Long result = 0l;
		try {
			String urls = ElasticsearchConnection.getElasticurls()+modelUrl+bulklUrl;
			WS.HttpResponse response = WS.url(urls).getAsync().get();
			if(response.success()){
				String jse = response.getString();
				JSONParser parser = new JSONParser();
				JSONObject json = (JSONObject) parser.parse(jse);
				return Long.parseLong(json.get("count").toString());
			}

		} catch (InterruptedException e) {
			Logger.error("Error sendActivePenyediaToElastic:"+e.getMessage());
		} catch (ExecutionException e) {
			Logger.error("Error sendActivePenyediaToElastic:"+e.getMessage());
		} catch (ParseException e) {
			Logger.error("Error sendActivePenyediaToElastic:"+e.getMessage());
		}
		return result;
	}

	public static List<PenyediaElastic> getAllPenyediaOnElastic(){
		Long total = Penyedia.getTotalPenyediaOnElastic();
		String modelUrl = key1;//getPenyediaKontrak
		String bulklUrl = "/_search?q=*&size="+total;
		List<PenyediaElastic> result = new ArrayList<>();
		if(total <= 0){
			new JobSendFillActivePenyedia().now();
//			new Job(){
//				public void doJob(){
//					Penyedia.sendfillActivePenyediaToElastic();
//				}
//			}.now();
		}else{
			try {
				String urls = ElasticsearchConnection.getElasticurls()+modelUrl+bulklUrl;
				WS.HttpResponse response = WS.url(urls).getAsync().get();
				if(response.success()){
					String jse = response.getString();
					JSONParser parser = new JSONParser();
					JSONObject json = (JSONObject) parser.parse(jse);
					JSONObject hits = (JSONObject) json.get("hits");
					JSONArray jsa = (JSONArray) hits.get("hits");
					Type listType = new TypeToken<List<HitsPenyedia>>() {}.getType();
					List<HitsPenyedia> list = new Gson().fromJson(jsa.toString(), listType);
					List<PenyediaElastic> lsp = list.stream().map(HitsPenyedia::get_source).collect(Collectors.toList());
					return lsp;
				}
			} catch (InterruptedException e) {
				Logger.error("Error sendActivePenyediaToElastic:"+e.getMessage());
			} catch (ExecutionException e) {
				Logger.error("Error sendActivePenyediaToElastic:"+e.getMessage());
			} catch (ParseException e) {
				Logger.error("Error sendActivePenyediaToElastic:"+e.getMessage());
			}
		}

		return result;
	}

	public static List<Penyedia> getAllPenyediaPersetujuanProdukByPokjaId(long pokja_id, long komoditas_id){
		String whereString = "WHERE p.active = 1 ";
		String query = "SELECT distinct py.id, py.nama_penyedia " +
				"FROM produk_tunggu_setuju pm " +
				"LEFT JOIN produk p on pm.produk_id = p.id " +
				"LEFT JOIN penyedia py on p.penyedia_id = py.id " +
				"LEFT JOIN penawaran pen on p.penawaran_id = pen.id " +
				"LEFT JOIN usulan us on pen.usulan_id = us.id ";

		if(AktifUser.getAktifUser().isPokja()){
			query += "LEFT JOIN usulan_pokja up on us.id = up.usulan_id ";
			whereString += " AND up.pokja_id = " + pokja_id + " AND us.status = '" + Usulan.STATUS_PENAWARAN_DIBUKA + "' AND NOT (p.status = '" + Produk.STATUS_MENUNGGU_PERSETUJUAN + "' AND pen.status = '" + Penawaran.STATUS_MENUNGGU_VERIFIKASI + "') ";
		}else{
			whereString += " AND p.status = '" + Produk.STATUS_MENUNGGU_PERSETUJUAN + "' ";
		}

		query = query + whereString + " AND p.komoditas_id = ? ";
		return Query.find(query,Penyedia.class, komoditas_id).fetch();
	}
}
