package models.katalog.komoditasmodel;

import models.BaseKatalogTable;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.sql.Timestamp;
import java.util.List;

@Table(name = "komoditas_manufaktur")
public class KomoditasManufaktur extends BaseKatalogTable {

    @Id
    public Long id;
    public Long manufaktur_id;
    public Long komoditas_id;
    public boolean active;

    public static KomoditasManufaktur findByKomoditasManufaktur(long manufaktur_id, long komoditas_id){
        return find("komoditas_id = ? and manufaktur_id = ? and active = ?", komoditas_id, manufaktur_id, AKTIF).first();
    }
}