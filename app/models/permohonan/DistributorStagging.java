package models.permohonan;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import models.penyedia.PenyediaDistributor;
import models.penyedia.PenyediaDistributorRepresentatif;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static play.db.jdbc.BaseTable.find;

public class DistributorStagging {
    public PenyediaDistributor distributor;
    public List<PenyediaDistributorRepresentatif> listRepresentatif = new ArrayList<>();

    public static List<DistributorStagging> findByPermohonanPembaruanId(Long id) {
        return find("WHERE permohonan_pembaruan_id = ?", DistributorStagging.class, id).fetch();
    }

    public Gson createGson(){
        return new GsonBuilder()
                .registerTypeAdapter(Date.class, new ISO8601DateAdapter())
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .create();
    }

    public void applyStaggingDistributor(){
        getDistributors().save();
    }

    public PenyediaDistributor getDistributors(){
        try {
            return (PenyediaDistributor) createGson().fromJson(String.valueOf(distributor), Class.forName(String.valueOf(distributor.getClass())));

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}
