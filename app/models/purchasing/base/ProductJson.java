package models.purchasing.base;

import models.purchasing.penerimaan.ProdukPenerimaan;
import models.purchasing.pengiriman.ProdukPengiriman;

/**
 * @author HanusaCloud on 10/27/2017
 */
public class ProductJson {

    public Long id;
    public Long productId;
    public String name;
    public String detailUrl;
    public String noProduct;
    public String note;
    public String unitName;
    public Long quantity;
    public Long leftovers;
    public String description;
    public Long packageProductId;
    public String imageUrl;

    public ProductJson(ProdukPenerimaan model) {
        this.id = model.id;
        this.productId = model.produk_id;
        this.name = model.nama_produk;
        this.detailUrl = model.getProductDetailUrl();
        this.noProduct = model.no_produk;
        this.note = model.catatan;
        this.unitName = model.nama_unit_pengukuran;
        this.quantity = model.kuantitas_awal;
        this.leftovers = model.leftovers;
        this.description = model.deskripsi;
    }

    public ProductJson(ProdukPengiriman model) {
        this.id = model.id;
        this.productId = model.produk_id;
        this.name = model.nama_produk;
        this.detailUrl = model.getProductDetailUrl();
        this.noProduct = model.no_produk;
        this.note = model.catatan;
        this.unitName = model.nama_unit_pengukuran;
        this.quantity = model.kuantitas_awal;
        this.leftovers = model.leftovers;
        this.description = model.deskripsi;
        this.packageProductId = model.paket_produk_id;
        this.imageUrl = model.setImage();
    }

    public Long getProductId() {
        return productId;
    }

    public Long getPackageProductId() {
        return packageProductId;
    }

}
