package models.elasticsearch.NewBase;

public class PenyediaElastic {
    public Long id;
    public String nama_penyedia;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama_penyedia() {
        return nama_penyedia;
    }

    public void setNama_penyedia(String nama_penyedia) {
        this.nama_penyedia = nama_penyedia;
    }



}
