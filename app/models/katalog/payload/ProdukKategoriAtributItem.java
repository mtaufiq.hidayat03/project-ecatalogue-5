package models.katalog.payload;

import org.apache.http.util.TextUtils;

public class ProdukKategoriAtributItem {

	public long id = -1;
	public String label;
	public String desc;
	public String type;
	public boolean isRequired;
	public long categoryProductId = 0;
	public long commodityId = 0;
	public String action = "delete";
	public Long informationType;
	public String informationLabel;
	public long parent = 0;

	public boolean isValid() {
		return !TextUtils.isEmpty(label);
	}

}
