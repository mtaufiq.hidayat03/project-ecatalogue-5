package service;

import models.user.User;
import models.user.User2;
import models.util.Config;
import org.jsoup.select.Collector;
import org.junit.Test;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import play.test.UnitTest;
import services.Login.JobUpdateRkn_Id;
import services.Login.LoginServices;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TestLoginADPSIKAP extends UnitTest {

    //@Test
    public void TestLogin(){
        String username = "rekanan10";
        String password = "1qaZXsw2!";

        Long rknid = LoginServices.loginAdpGetRknid(username,password);
        Logger.info("> > > > > > > > > > > > > > > >Rekananid user: %s",rknid);
        //Long rknid = new Long(12336);
        Long idDefault = LoginServices.getRkn_IdActionDefault(rknid);
        Logger.info("< < < < < < < < < < < < < < < < < < < < < < < Default Rekananid user: %s",idDefault);
        User u = User.findFirstByUserName(username);
        u.rkn_id = idDefault;
        Logger.info("save - - - - - - - - - - - - - "+u.user_name+" "+ u.save());

    }


    @Test
    public void LoopLogin(){

        List<User2> list = User2.findAllPenyediaDistributor();
        Map<Long, String> pass =  list.stream().collect(Collectors.toMap(User2::getId, User2::getUser_password));
        Map<Long, String> users = list.stream().collect(Collectors.toMap(User2::getId, User2::getUser_name));

        for (Map.Entry<Long, String> entry : users.entrySet()) {
            //System.out.println(userid + " / " + username + "/ "+ password);
            JobUpdateRkn_Id jobrkn = new JobUpdateRkn_Id();
            jobrkn.userid = entry.getKey();
            jobrkn.username = entry.getValue();
            jobrkn.password = pass.get(jobrkn.userid);
            jobrkn.now();

        }
        Logger.info("<<<<<<<<<<<< count >>>>>>>>>>>>: "+list.size());
    }

    @Test
    public void testLoopWait() throws InterruptedException {
        int repo_id = Config.getInstance().repo_id;
        Logger.info(repo_id+" id");
//        int sizedata = 2000;
//        int waitNext = 500;
//        int loopNow = 0;
//        for (int a =0; a < sizedata; a++){
//            Logger.info("count : "+a);
//            loopNow++;
//            if(loopNow==waitNext){
//                loopNow=0;
//                Logger.info("WAIT . . . .  . . .");
//                Thread.sleep(10000);
//            }
//        }
    }


}
