package models.cms;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.*;


/**
 * Created by user on 10/12/2017.
 */
@Table(name = "polling_pilihan")
public class PollingPilihan extends BaseKatalogTable {
    @Id
    public Long id;
    public Long polling_id;
    public String pilihan_jawaban;
    public Long posisi_pilihan;
    public Boolean active;

    @Transient
    public Long pilihan_hasil;

    public static List<PollingPilihan> findByPollingId(String id){
        String pollingId = (id!=""?"and polling_id in (" + id +")":"");
        String query = "SELECT * FROM polling_pilihan WHERE active = 1 " + pollingId;
        return Query.find(query,PollingPilihan.class).fetch();
    }

    public static List<PollingPilihan> viewPollingResult(Long id){
        String query = "SELECT pp.pilihan_jawaban, COUNT(ph.polling_pilihan_id) as pilihan_hasil " +
                        "FROM polling_pilihan pp " +
                        "LEFT JOIN polling_hasil ph on ph.polling_pilihan_id = pp.id " +
                        "WHERE pp.active = 1 and pp.polling_id = " + id +
                        " GROUP BY pp.id";
        return Query.find(query,PollingPilihan.class).fetch();
    }
}
