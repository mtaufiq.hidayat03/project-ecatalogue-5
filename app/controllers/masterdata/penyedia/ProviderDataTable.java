package controllers.masterdata.penyedia;

import controllers.BaseController;
import models.datatable.ResultsetHandlers;
import models.jcommon.datatable.DatatableQuery;
import org.apache.http.util.TextUtils;

/**
 * @author HanusaCloud on 11/30/2017
 */
public class ProviderDataTable extends BaseController {

    public static void cariPenyedia(String keyword, int commodity){
        if(commodity > 0) {
            DatatableQuery query = new DatatableQuery(ResultsetHandlers.DAFTAR_PENYEDIA)
                    .select("p.id, p.user_id, p.nama_penyedia, u.nama_lengkap, u.user_name, p.website, " +
                            "coalesce(pk.total_kontrak,0) total_kontrak, coalesce(d.total_distributor,0) total_distributor")
                    .from("penyedia p " +
                            "join `user` u on p.user_id = u.id " +
                            "left join (select count(*) total_kontrak, penyedia_id from penyedia_kontrak where active=1 group by penyedia_id) pk on pk.penyedia_id = p.id " +
                            "left join (select count(*) total_distributor, penyedia_id from penyedia_distributor where active=1 group by penyedia_id) d on d.penyedia_id = p.id " +
                            "LEFT JOIN penyedia_komoditas k ON k.penyedia_id = p.id ");
            StringBuilder sb = new StringBuilder();
            sb.append(" p.active = 1");
            if (commodity > 0) {
                sb.append(" AND k.komoditas_id = '").append(commodity).append("'");
            }
            if (!TextUtils.isEmpty(keyword)) {
                sb.append(" AND (p.nama_penyedia LIKE '%").append(keyword).append("%' OR u.user_name = '").append(keyword).append("')");
            }
            query.where(sb.toString());
            renderJSON(query.executeQuery());
        }
        else {
            DatatableQuery query = new DatatableQuery(ResultsetHandlers.DAFTAR_PENYEDIA)
                    .select("p.id, p.user_id, p.nama_penyedia, u.nama_lengkap, u.user_name, p.website, " +
                            "coalesce(pk.total_kontrak,0) total_kontrak, coalesce(d.total_distributor,0) total_distributor")
                    .from("penyedia p " +
                            "join `user` u on p.user_id = u.id " +
                            "left join (select count(*) total_kontrak, penyedia_id from penyedia_kontrak where active=1 group by penyedia_id) pk on pk.penyedia_id = p.id " +
                            "left join (select count(*) total_distributor, penyedia_id from penyedia_distributor where active=1 group by penyedia_id) d on d.penyedia_id = p.id " +
                            " ");
            StringBuilder sb = new StringBuilder();
            sb.append(" p.active = 1");
            if (!TextUtils.isEmpty(keyword)) {
                sb.append(" AND (p.nama_penyedia LIKE '%").append(keyword).append("%' OR u.user_name = '").append(keyword).append("')");
            }
            query.where(sb.toString());
            renderJSON(query.executeQuery());
        }
    }

}
