package models.katalog.payload;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import models.katalog.ProdukKategoriAtribut;
import org.apache.http.util.TextUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProdukKategoriAtributPayload {

	public long kid;
	public long pkid;
	public String specifications = "";

	public List<ProdukKategoriAtributItem> getItems() {
		List<ProdukKategoriAtributItem> items = new ArrayList<>();
		if (TextUtils.isEmpty(specifications)) {
			specifications = "[]";
		}
		Type collectionType = new TypeToken<Collection<ProdukKategoriAtributItem>>() {
		}.getType();
		List<ProdukKategoriAtributItem> results = new Gson().fromJson(specifications, collectionType);
		if (results != null && !results.isEmpty()) {
			items.addAll(results);
		}
		return items;
	}
}
