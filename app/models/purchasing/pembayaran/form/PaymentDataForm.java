package models.purchasing.pembayaran.form;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import org.apache.http.util.TextUtils;
import utils.DateTimeUtils;

import java.io.File;
import java.lang.reflect.Type;
import java.util.*;

/**
 * @author HanusaCloud on 10/31/2017
 */
public class PaymentDataForm {

    public Long modelId;
    public Long packageId;
    public String action = "create";
    public String invoiceNumber;
    public String invoiceDate;
    public String totalInvoice;
    public String paymentDate;
    public String description;
    @SerializedName("attachment")
    public File attachment;
    public String items;
    public String deletedItems;

    public Date getInvoiceDate() {
        return DateTimeUtils.parseDdMmYyyy(invoiceDate);
    }

    public Date getPaymentDate() {
        return DateTimeUtils.parseDdMmYyyy(paymentDate);
    }

    public Boolean isCreatedMode() {
        return action.equalsIgnoreCase("create") && modelId == null;
    }

    public List<SelectedItem> getItems() {
        return getItems(items);
    }

    private List<SelectedItem> getItems(String items) {
        List<SelectedItem> results = new ArrayList<>();
        if (TextUtils.isEmpty(items)) {
            items = "[]";
        }
        Type collectionType = new TypeToken<Collection<SelectedItem>>() {
        }.getType();
        results.addAll(new Gson().fromJson(items, collectionType));
        return results;
    }

    public List<SelectedItem> getDeletedItems() {
        return getItems(deletedItems);
    }

    public Double getTotalInvoiceDouble() {
        if (!TextUtils.isEmpty(totalInvoice)) {
            return Double.parseDouble(totalInvoice.replaceAll("[^0-9.]",""));
        }
        return 0D;
    }

}
