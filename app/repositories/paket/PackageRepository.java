package repositories.paket;

import constants.paket.PaketStatusConstant;
import models.common.AktifUser;
import models.penyedia.Penyedia;
import models.penyedia.PenyediaDistributor;
import models.purchasing.Paket;
import models.purchasing.search.DistributorSearch;
import models.purchasing.search.DistributorSearchResult;
import models.purchasing.search.PurchasingSearch;
import models.purchasing.search.PurchasingSearchResult;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.util.TextUtils;
import play.db.jdbc.Query;
import utils.DateTimeUtils;
import utils.LogUtil;


/**
 * @author HanusaCloud on 10/31/2017
 */
public class PackageRepository {

    public static final String TAG = "PackageRepository";

    public static void updateTotalProductCompleted(Long id, Integer total) {
        final Integer userId = AktifUser.getActiveUserId();
        Query.update("UPDATE paket SET agr_paket_produk_sudah_diterima_semua =?" +
                ", modified_by =?" +
                ", modified_date =?" +
                " WHERE id =? ", total, userId, DateTimeUtils.getStandardDateTime(), id);
    }

    public static DistributorSearchResult searchDistributor(DistributorSearch search) {
        DistributorSearchResult dist = new DistributorSearchResult(search);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT pd.*, s.user_name username")
                .append(" FROM penyedia_distributor pd")
                .append(" LEFT JOIN `user` s ON s.id = pd.user_id");
        sb.append(" WHERE pd.active > 0 and pd.minta_disetujui = 0 and pd.setuju_tolak like 'Setuju'");

        if (!TextUtils.isEmpty(search.keyword)) {
            sb.append(" AND (pd.nama_distributor LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%'");
            sb.append(" OR s.user_name LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%')");
        }
        if (search.pid > 0) {
            sb.append(" AND pd.penyedia_id = ").append(search.pid);
        }

        Long total = PackageRepository.generateTotal(sb.toString());
        dist.setTotal(total);

        if (!dist.isEmpty()) {
            sb.append(" LIMIT ").append(dist.getMax()).append(" OFFSET ").append(dist.getFrom());
            dist.items = Query.find(sb.toString(), PenyediaDistributor.class).fetch();
        }
        return dist;
    }

    public static PurchasingSearchResult searchPackage(PurchasingSearch search, AktifUser aktifUser) {
        PurchasingSearchResult model = new PurchasingSearchResult(search);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT p.id, " +
                "p.no_paket, " +
                "p.nama_paket, " +
                "py.nama_penyedia, " +
                "p.satuan_kerja_nama, " +
                "k.nama as nama_satker, " +
                "p.agr_paket_produk, " +
                "p.agr_konversi_harga_total, " +
                "p.created_by, " +
                "p.created_date, " +
                "p.apakah_batal")
                .append(" FROM paket p")
                .append(" LEFT JOIN penyedia py ON p.penyedia_id = py.id")
                .append(" LEFT JOIN kldi_satker k ON p.satker_id = k.id")
                .append(" JOIN paket_status ps ON ps.paket_id = p.id");

        sb.append(" WHERE p.active = 1");
        if (!TextUtils.isEmpty(search.keyword)) {
            sb.append(" AND (p.nama_paket LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%'");
            sb.append(" OR p.no_paket LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%'");
            sb.append(" OR k.nama LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%')");
        }
        if (search.commodity > 0) {
            sb.append(" AND p.komoditas_id = ").append(search.commodity);
        }
        if (aktifUser != null && !aktifUser.isAdmin()) {
            sb.append(" AND (p.panitia_user_id = '").append(aktifUser.user_id).append("'")
                    .append(" OR p.ppk_user_id = '").append(aktifUser.user_id).append("'");
            Penyedia penyedia = Penyedia.getProviderByUserId(aktifUser.user_id);
            if (penyedia != null) {
                sb.append(" OR p.penyedia_id = '").append(penyedia.id).append("'");
            }
            PenyediaDistributor distributor = PenyediaDistributor.findByUser(aktifUser.user_id);
            //penyedia komoditas di cek juga

            if (distributor != null) {
                //sb.append(" OR p.penyedia_distributor_id = '").append(distributor.id).append("'");
                sb.append(" OR p.penyedia_distributor_id IN(select id from penyedia_distributor where user_id= '").append(aktifUser.user_id).append("'").append(")");
            }
            sb.append(")");
        }
        if (aktifUser != null && aktifUser.isProvider()) {
            sb.append(" AND ps.status_paket NOT IN ('" + PaketStatusConstant.STATUS_DRAFT + "')");
        }
        if (!TextUtils.isEmpty(search.status)) {
            sb.append(" AND ps.status_paket = '").append(StringEscapeUtils.escapeSql(search.status)).append("'");
        }
        if (!TextUtils.isEmpty(search.negotiationStatus)) {
            sb.append(" AND ps.status_negosiasi = '").append(StringEscapeUtils.escapeSql(search.negotiationStatus)).append("'");
        }
        if (!TextUtils.isEmpty(search.position)) {
            sb.append(" AND ps.to_status = '").append(StringEscapeUtils.escapeSql(search.position)).append("'");
        }
        if (!TextUtils.isEmpty(search.sortby)) {
            sb.append(" ORDER BY p.created_date ").append(StringEscapeUtils.escapeSql(search.sortby)).append(" ");
        }
        LogUtil.debug(TAG, "query: " + sb.toString());
        Long total = PackageRepository.generateTotal(sb.toString());
        LogUtil.debug(TAG, "total: " + total);
        model.setTotal(total);
        if (!model.isEmpty()) {
            sb.append(" LIMIT ").append(search.per_page).append(" OFFSET ").append(model.getFrom());
            LogUtil.debug(TAG, sb.toString());
            model.items = Query.find(sb.toString(), Paket.class).fetch();
            if (!model.items.isEmpty()) {
                for (Paket paket : model.items) {
                    paket.withPaketStatus();
                }
            }
        }
        return model;
    }

    private static Long generateTotal(String query) {
        Long total = Query.find("SELECT COUNT(id) FROM (" + query + ") p", Long.class).first();
        return total;
    }

}
