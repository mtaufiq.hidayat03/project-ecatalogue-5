package models.masterdata;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

@Table(name="kldi_satker")
//TODO Lengkapi!. Class ini masih prototype untuk pengambilan data dari WS Sirup
public class Satker extends BaseTable {
	@Id
	public Integer id;
	public String idKldi;
	public String idSatker;
	public String nama;
	public String alamat;
	public Long instansi_id;
	public  String statusPerubahan;
	public Boolean active;

	public static Satker getBySatkerId(String idSatker) {
		return find("idSatker =?", idSatker).first();
	}

	public void setAktif() {
		this.active = true;
		prePersist();
		save();
	}

	public void setNonAktif() {
		this.active = false;
		prePersist();
		save();
	}

}
