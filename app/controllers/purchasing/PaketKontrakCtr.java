package controllers.purchasing;

import com.google.gson.Gson;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.masterdata.DokumenTemplate;
import models.purchasing.Paket;
import models.purchasing.kontrak.PaketKontrak;
import models.purchasing.kontrak.form.ContractDataForm;
import models.purchasing.riwayat.PaketRiwayat;
import models.purchasing.status.PaketStatus;
import models.secman.Acl;
import org.jsoup.Jsoup;
import play.Logger;
import play.i18n.Lang;
import play.i18n.Messages;
import repositories.paket.PackageContractRepository;
import utils.HtmlUtil;

import java.io.File;
import org.apache.commons.codec.binary.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang.StringEscapeUtils.unescapeHtml;

/**
 * @author HanusaCloud on 11/14/2017
 */
public class PaketKontrakCtr extends BaseController {

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_KONTRAK})
    public static void contractList(String language, Long paket_id) {
        Paket model = Paket.findById(paket_id);
        notFoundIfNull(model);
        if (model != null) {
            model.withContracts();
            model.withStatusMessage(getAktifUser());
        }
        renderArgs.put("paket", model);
        render("purchasing/PaketCtr/kontrak/listKontrak.html");
    }

    public static void downloadDoc(Long id) {
        //Logger.info(isi_text+" isinyda");

        PaketKontrak pk = PaketKontrak.findById(id);
        if (null == pk.dokumen_kontrak || pk.dokumen_kontrak.isEmpty()) {
            renderArgs.put("isi_text", "<p>Dokumen kosong</p>");
        } else {
            renderArgs.put("isi_text", pk.dokumen_kontrak);
        }
        renderArgs.put("nama_file", "dokumen_kontrak");
        render("MediaManager/create.html");
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_KONTRAK_INPUT})
    public static void createContract(Long paket_id) {
        Paket model = Paket.findById(paket_id);
        notFoundIfNull(model);
        if (model != null) {
            model.withContractValue();
        }
        if (model != null) {
            model.withTotalNegotiations();
            model.withProducts();
            model.withSatker();
            model.withKldi();
            model.withPpk();
            model.withPp();
            model.withDistributor();
            model.withProvider(true);
            model.withTotalContractUploaded();
            model.withLatestStatus();
            model.withCommodity();
            model.withStatusMessage(getAktifUser());
            model.withSumberDana();
            model.withTransient();
            // Logger.debug(new Gson().toJson(model.satker));
        }
        DokumenTemplate doc = DokumenTemplate.findById(3);
//        String template = doc.template;
//        template.replace("${paket.buyer.nama_lengkap}",model.buyer.nama_lengkap);
//        doc.template = template;

        Map<String, Object> params = new HashMap<>();
        params.put("paket", model);
        String terbilang = HtmlUtil.angkaToTerbilang(model.contractValue.longValue());
        params.put("terbilangHarga", terbilang + " Rupiah");
        String templates = HtmlUtil.renderHtml(doc.template, params);
        doc.template = templates;
        renderArgs.put("paket", model);
        renderArgs.put("dokumen", doc);
        render("purchasing/PaketCtr/kontrak/createKontrak.html");
    }

    public static void downloadContract(Long id) {
        PaketKontrak model = PaketKontrak.findById(id);
        if (model != null) {
            BlobTable blobTable = model.withBlobContent();
            if (blobTable != null && blobTable.getFile().exists()) {
                renderBinary(blobTable.getFile(), model.original_file_name);
            } else if (model.getFile().exists()) {
                renderBinary(model.getFile(), model.original_file_name);
            } else {
                error("File's not found!");
            }
        } else {
            error("File's not found!");
        }
    }

    public static void downloadUplContract(Long id) {
        PaketKontrak model = PaketKontrak.findById(id);

        if (model != null) {
            BlobTable blobTable = model.withBlobId();

            if (blobTable != null && blobTable.getFile().exists()) {
                renderBinary(blobTable.getFile(), model.file_name);
            } else if (model.getFile().exists()) {
                renderBinary(model.getFile(), model.file_name);
            } else {
                error("File's not found!");
            }
        } else {
            error("File's not found!");
        }
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_KONTRAK_INPUT})
    public static void storeContract(Long paket_id, ContractDataForm model) {
        if (model != null) {
            model.packageId = paket_id;
            //decode
            //replace default <p> pada tinymce
            //pastikan tinymce.getcontent
            String pureText = Jsoup.parse(model.dokumen_kontrak).text();
            byte[] decodedBytes = Base64.decodeBase64(pureText);
            String decodedString = new String(decodedBytes);
            model.dokumen_kontrak = decodedString;
            //replace
            //Logger.debug(new Gson().toJson(model));
            PaketKontrak contract = new PaketKontrak().dataForm(model);
            List<PaketKontrak> list = PackageContractRepository.getByPackageIdFront(paket_id);
            contract.position(list.size() + 1);
            contract.save();

            PaketStatus paketStatus = PaketStatus.getLatestStatusByPackageId(paket_id);

            // paket history
            PaketRiwayat paketRiwayat = new PaketRiwayat();
            paketRiwayat.paket_id = paket_id;
            paketRiwayat.deskripsi = PaketRiwayat.DESKRIPSI_PAKET_UPLOAD_KONTRAK+contract.no_kontrak;
            paketRiwayat.status_paket = paketStatus.getDetailStatus();
            paketRiwayat.save();
        }
        redirect(new Paket(paket_id).getContractListUrl());
    }


    public static void uploadContract(File file, Long kontrak_id, Long paket_id) {
        try {

            PaketKontrak model = PaketKontrak.findById(kontrak_id);
            if (model != null) {
                BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, model.id, PaketKontrak.class.getName());
                blob.blb_nama_file = file.getName();
                model.file_size = blob.blb_ukuran;
                model.blb_id = blob.id;
                model.file_name = blob.blb_nama_file;
                model.save();
            }
            if(null == paket_id){
                paket_id = model.paket_id;
            }
            flash.success(Messages.get("notif.success.upload"));
        } catch (Exception e) {
            e.printStackTrace();
            flash.error(Messages.get("notif.failed.upload"));
        }


        Paket p = Paket.findById(paket_id);
        redirect(p.getContractListUrl());
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_KONTRAK_DELETE})
    public void deleteContract(long id) {
        PaketKontrak model = PaketKontrak.findById(id);
        String lang = Lang.get();
        Logger.debug("delete contract " + model.id);
        if (model != null) {
            model.softDelete();
            flash.success(Messages.get("notif.success.delete"));
        } else {
            flash.error(Messages.get("notif.failed.delete"));
        }
        Paket p = Paket.findById(model.paket_id);
        redirect(p.getContractListUrl());
    }

}
