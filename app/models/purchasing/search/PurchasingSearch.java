package models.purchasing.search;

import ext.contracts.ParamAble;
import org.apache.http.message.BasicNameValuePair;
import play.mvc.Scope;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 12/4/2017
 */
public class PurchasingSearch implements ParamAble {

    public final String keyword;
    public final long commodity;
    public final String position;
    public final String status;
    public final String negotiationStatus;
    public final String sortby;
    public final long per_page;
    public final int page;

    public PurchasingSearch(
            String keyword,
            long commodity,
            String position,
            String status,
            String negotiationStatus,
            String sortby,
            long per_page,
            int page
    ) {
        this.keyword = keyword;
        this.commodity = commodity;
        this.position = position;
        this.status = status;
        this.negotiationStatus = negotiationStatus;
        this.sortby = sortby;
        this.per_page = per_page;
        this.page = page;
    }

    public PurchasingSearch(Scope.Params params) {
        this.keyword = !isNull(params,"keyword") ? params.get("keyword") : "";
        this.commodity = !isNull(params, "commodity") ? params.get("commodity", Long.class) : 0;
        this.position = !isNull(params, "position") ? params.get("position") : "";
        this.negotiationStatus = !isNull(params, "negotiation") ? params.get("negotiation") : "";
        this.status = !isNull(params, "status") ? params.get("status") : "";
        this.sortby = !isNull(params, "sortby") ? params.get("sortby") : "";
        this.per_page = !isNull(params, "per_page") ? params.get("per_page", Long.class) : 20;
        this.page = getCurrentPage(params);
    }

    @Override
    public List<BasicNameValuePair> getParams() {
        List<BasicNameValuePair> results = new ArrayList<>();
        results.add(new BasicNameValuePair("keyword", this.keyword));
        results.add(new BasicNameValuePair("commodity", String.valueOf(this.commodity)));
        results.add(new BasicNameValuePair("position", this.position));
        results.add(new BasicNameValuePair("negotiation", this.negotiationStatus));
        results.add(new BasicNameValuePair("status", this.status));
        results.add(new BasicNameValuePair("sortby", this.sortby));
        results.add(new BasicNameValuePair("per_page", String.valueOf(this.per_page)));
        return results;
    }

    @Override
    public int getPage() {
        return page;
    }
}
