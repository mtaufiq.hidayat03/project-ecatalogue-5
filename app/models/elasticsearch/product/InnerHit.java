package models.elasticsearch.product;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class InnerHit {

    @SerializedName("priceList")
    public PriceListHit priceList;

    public ProductPriceElastic getPrice() {
        if (priceList != null && priceList.priceHit != null) {
            List<Hit> hits = priceList.priceHit.hits;
            if (hits != null && !hits.isEmpty()) {
                return priceList.priceHit.hits.get(0).price;
            }
        }
        return null;
    }
}
