package controllers.purchasing;

import ext.FormatUtils;
import models.common.ButtonGroup;
import models.jcommon.datatable.DatatableResultsetHandler;
import models.purchasing.Paket;
import org.apache.http.util.TextUtils;
import play.i18n.Messages;
import services.datatable.DataTableService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author HanusaCloud on 12/4/2017
 */
public class PurchasingResultHandler {

    public final static DatatableResultsetHandler<String[]> PACKAGE_LIST =
            new DatatableResultsetHandler("p.id, p.nama_paket, " +
                    "k.nama_komoditas, p.agr_paket_produk, " +
                    "p.agr_konversi_harga_total, p.satuan_kerja_nama, ps.to_status") {

                public String[] handle(ResultSet rs) throws SQLException {
                    String[] results = new String[columns.length];

                    Paket model = new Paket();
                    model.id = rs.getLong("p.id");
                    results[0] = String.valueOf(model.id);
                    results[1] = rs.getString("p.nama_paket");
                    //results[2] = rs.getString("pe.nama_penyedia");
                    results[2] = rs.getString("p.agr_paket_produk");
                    results[3] = rs.getString("p.satuan_kerja_nama");
                    results[4] = FormatUtils.formatCurrencyRupiah(rs.getDouble("p.agr_konversi_harga_total"));
                    results[5] = rs.getString("ps.to_status");
                    if (TextUtils.isEmpty(results[5])) {
                        results[5] = "Panitia";
                    }
                    List<ButtonGroup> buttonGroups = new ArrayList<>();

                    buttonGroups.add(new ButtonGroup(Messages.get("detail.label"), model.getDetailUrl()));
                    buttonGroups.add(new ButtonGroup(Messages.get("ubah.label"), model.getUpdateUrl()));

                    results[6] = DataTableService.generateButtonGroup(buttonGroups);

                    return results;
                }
            };

}
