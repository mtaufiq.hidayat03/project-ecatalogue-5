package controllers.penyedia;

import com.google.gson.Gson;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.common.AktifUser;
import models.penyedia.Penyedia;
import models.penyedia.PenyediaDistributor;
import models.penyedia.PenyediaDistributorRepresentatif;
import models.secman.Acl;
import models.user.User;
import models.util.RekananDetail;
import play.Logger;
import play.i18n.Messages;

import java.util.ArrayList;
import java.util.List;

public class PenyediaDistributorCtr extends BaseController {

    private static final String formHtml = "penyedia/PenyediaDistributorCtr/form.html";

    @AllowAccess({Acl.COM_PROFIL_PENYEDIA_DISTRIBUTOR})
    public static void index(long pid){
        Logger.info("pid nyaa " + pid);
        if(getAktifUser().is_penyedia){
            Penyedia penyedia = Penyedia.getProviderByUserId(AktifUser.getAktifUser().user_id);
            if(null!=penyedia) {
                pid = penyedia.id;
            }else{
                pid = 0l;
                flash.error(Messages.get("notif.error.penyedia.informasi"));
            }
            render(pid);
        }else {
            render(pid);
        }

    }

    public static void getRepresentatif(Long userId){
        List<PenyediaDistributorRepresentatif> representatifList = new ArrayList<>();
        try{
            Penyedia penyedia = Penyedia.getProviderByUserId(userId);
            representatifList = PenyediaDistributorRepresentatif.findByPenyediaIdDistributor(penyedia.id);
            Logger.info("representatif: "+representatifList.size());
        }catch (Exception e){}
        renderJSON(representatifList);
    }

    //karena pencarian distributor berdasarkan searching pada table penyedia
    public static void getRepresentatifDist(Long userId, Long penyedia_id){
        List<PenyediaDistributorRepresentatif> list = new ArrayList<>();
        list = PenyediaDistributorRepresentatif.findByUserIdDistributor(userId,penyedia_id);
        renderJSON(list);
    }

    @AllowAccess({Acl.COM_PENYEDIA_DISTRIBUTOR_ADD})
    public static void create(long pid){
        PenyediaDistributor distributor = new PenyediaDistributor();
        render(formHtml, distributor, pid);
    }

    @AllowAccess({Acl.COM_PENYEDIA_DISTRIBUTOR, Acl.COM_PENYEDIA_DISTRIBUTOR_ADD,Acl.COM_PENYEDIA_DISTRIBUTOR_EDIT})
    public static void formSubmit(PenyediaDistributor distributor, String[] nama_representatif,
                                  String[] telp_representatif, String[] email_representatif,
                                  String username, Long userId, String rknDetailJson){

        long pid = distributor.penyedia_id;

        if(nama_representatif == null){
            params.flash();
            flash.error("Representatif belum diisi.");

            render(formHtml, distributor, pid, username);
        }

        List<PenyediaDistributorRepresentatif> representatifList = PenyediaDistributorRepresentatif.setList(nama_representatif, telp_representatif, email_representatif);

        Logger.info("userid " +userId);
        Logger.info("userid " +distributor.user_id);

        List<PenyediaDistributor> dist = new ArrayList<>();
        dist = PenyediaDistributor.findByPenyediaAll(pid);

        boolean isEdit = distributor.id != null;

        Logger.info("isedit " + isEdit);

        if(distributor.id == null && username.isEmpty()){
            params.flash();
            flash.error("Anda belum memilih user untuk menjadi distributor.");

            render(formHtml, distributor, pid, username, representatifList);
        }else{
            validation.valid(distributor);
            if(validation.hasErrors()) {
                params.flash();
                flash.error("Simpan Penyedia gagal. Silakan cek kembali isian Anda di tab Informasi Distributor.");

                render(formHtml, distributor, pid, username, representatifList);
            }else{

//                if(distributor.user_id == null){
//                    if(userId != null){
//                        distributor.user_id = userId;
//                    }else{
//                        RekananDetail rkn = new Gson().fromJson(rknDetailJson,RekananDetail.class);
//                        long user_id = User.saveOrUpdateUsrRkn(rkn);
//                        distributor.user_id = user_id;
//                    }
//                }

                if(dist != null && !isEdit){
                    List<PenyediaDistributor> d = new ArrayList<>();
                    d = PenyediaDistributor.findByPenyediaExist(distributor.user_id, pid);

                    if (d.size() > 0){
                        params.flash();
                        flash.error("Distributor sudah ada");

                        render(formHtml, distributor, pid, username, representatifList);
                    }

//                    for(PenyediaDistributor p : dist){
//                        if (distributor.user_id.toString().equalsIgnoreCase(p.user_id.toString())){
//                            params.flash();
//                            flash.error("Distributor sudah ditambahkan");
//
//                            render(formHtml, distributor, pid, username, representatifList);
//                        }
//                    }
                }

                distributor.active = true;
                distributor.minta_disetujui = 1L;
                distributor.setuju_tolak = "";
                long distributor_id = 0L;

                if(distributor.id != null){
                    PenyediaDistributorRepresentatif.deleteByDistributor(distributor.id);
                    distributor.save();
                    distributor_id = distributor.id;
                }else{
                    distributor_id = distributor.save();
                }

                for(PenyediaDistributorRepresentatif rep : representatifList){
                    rep.penyedia_distributor_id = distributor_id;
                    rep.active = true;
                    rep.save();
                }

                if(!isEdit){
                    flash.success("Data Distributor berhasil ditambahkan. Penambahan data memerlukan persetujuan admin");
                }else {
                    flash.success("Data Distributor berhasil diperbarui. Perubahan data memerlukan persetujuan admin");
                }

                Logger.info("pid yg dikirim " + pid);

//                index(pid);
                DistributorCtr.index(pid);
            }
        }
    }

    @AllowAccess({Acl.COM_PENYEDIA_DISTRIBUTOR_EDIT})
    public static void edit(long pid, Long id){
        PenyediaDistributor distributor  = PenyediaDistributor.findById(id);
        List<PenyediaDistributorRepresentatif> representatifList = PenyediaDistributorRepresentatif.findByDistributor(id);
        render(formHtml, distributor, pid, representatifList);
    }

    @AllowAccess({Acl.COM_PENYEDIA_DISTRIBUTOR})
    public static void detail(long pid, long id){
        PenyediaDistributor distributor = PenyediaDistributor.findDetailById(id);
        render(distributor, pid);
    }

    @AllowAccess({Acl.COM_PENYEDIA_DISTRIBUTOR_DELETE})
    public static void delete(long id){
        PenyediaDistributor model= new PenyediaDistributor().findByIdActive(id, PenyediaDistributor.class);
        notFoundIfNull(model);
        long pid = model.penyedia_id;
        if (model.isAllowToBeDeleted()) {
            model.softDelete();
            flash.success(Messages.get("notif.success.delete"));
        } else {
            flash.error(Messages.get("notif.failed.delete.relation"));
        }
        DistributorCtr.index(pid);
    }
}
