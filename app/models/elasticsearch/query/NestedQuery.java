package models.elasticsearch.query;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.http.util.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class NestedQuery {

    private String path;
    private List<String> innerHitFields = new ArrayList<>();
    private List<JsonObject> nesteds = new ArrayList<>();

    public NestedQuery setPath(String path) {
        this.path = path;
        return this;
    }

    public NestedQuery setInnerHit(String field) {
        this.innerHitFields.add(field);
        return this;
    }

    public NestedQuery setQuery(NestedObject model) {
        if (TextUtils.isEmpty(this.path)) {
            this.path = model.getPath();
        }
        this.nesteds.add(model.build());
        return this;
    }

    private JsonObject generateQuery() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("path", this.path);
        JsonArray mustArray = new JsonArray();
        if (!nesteds.isEmpty()) {
            for (JsonObject j : nesteds) {
                mustArray.add(j);
            }
        }
        JsonObject must = new JsonObject();
        must.add("must", mustArray);
        JsonObject bool = new JsonObject();
        bool.add("bool", must);
        jsonObject.add("query", bool);
        if (!innerHitFields.isEmpty()) {
            jsonObject.add("inner_hits", generateInnerHits());
        }
        return jsonObject;
    }

    public JsonObject build() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("nested", generateQuery());
        return jsonObject;
    }

    public JsonObject generateInnerHits() {
        JsonObject result = new JsonObject();
        JsonObject fieldObject = new JsonObject();
        JsonObject fObj = new JsonObject();
        for (String f : innerHitFields) {
            fObj.add(path + "." + f, new JsonObject());
        }
        fieldObject.add("fields", fObj);
        result.add("highlight", fieldObject);
        return result;
    }

}
