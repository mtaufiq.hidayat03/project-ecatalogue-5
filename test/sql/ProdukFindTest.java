package sql;

import java.util.List;

import org.apache.commons.lang.time.StopWatch;
import org.junit.Test;

import models.katalog.Produk;
import models.purchasing.Paket;
import play.Logger;
import play.test.UnitTest;

public class ProdukFindTest extends UnitTest {

	//@Test
	public void doTest()
	{

		StopWatch sw=new StopWatch();
		sw.start();
		List<Produk> list=Produk.findByProduk_kategori_id(154);

		sw.stop();
		Logger.debug("[ProdukFindTest] find, size: %,d dur: %s", list.size(), sw);
	}
}
