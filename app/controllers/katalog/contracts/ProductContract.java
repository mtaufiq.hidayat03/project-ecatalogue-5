package controllers.katalog.contracts;

import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.katalog.*;
import models.masterdata.Manufaktur;
import models.penyedia.Penyedia;
import models.prakatalog.Penawaran;
import play.mvc.Router;
import utils.LogUtil;

import java.util.List;

/**
 * @author HanusaCloud on 12/9/2017
 */
public interface ProductContract {

    Long getId();

    Integer getCreatedBy();

    Long getProviderId();

    Integer getCommodityId();

    List<ProdukGambar> getPictures();

    Long getManufactureId();

    Integer getCategoryId();

    Komoditas getCommodity();

    Penyedia getProvider();

    Integer getPenawaranId();

    void setPictures(List<ProdukGambar> pictures);

    void setDocuments(List<ProdukLampiran> documents);

    void setPrice(ProdukHarga price);

    void setPrices(List<ProdukHarga> prices);

    void setProvider(Penyedia penyedia);

    void setCommodity(Komoditas komoditas);

    void setSpecifications(List<ProdukAtributValue> specifications);

    void setProductHistory(List<ProdukRiwayat> histories);

    void setCategory(ProdukKategori category);

    void setManufacture(Manufaktur manufacture);

    void setRegencyPrices(ProdukWilayahJualKabupaten model);

    void setProvincePrices(ProdukWilayahJualProvinsi model);

    void setPenawaran(Penawaran model);

    default String getFirstImageUrl() {
        if (getPictures() != null && !getPictures().isEmpty()) {
            if(getPictures().get(0).posisi_file == ProdukGambar._POSISI_FILE_LOKAL){
                ProdukGambar pg =  getPictures().get(0);
                return pg.file_url = pg.getLocalImageStorage()+pg.file_sub_location+pg.file_name;
            }else{
                return getPictures().get(0).getActiveUrl();
            }

        }
        return "";
    }

    default void withPictures() {
        if (getId() != null) {
            List<ProdukGambar> list = ProdukGambar.findByProduk(getId());
            if (!list.isEmpty()) {
                setPictures(list);
            }
        }
    }

    default void withDocuments() {
        if (getId() != null) {
            List<ProdukLampiran> list = ProdukLampiran.getDocumentsByProductId(getId());
            if (!list.isEmpty()) {
                this.setDocuments(list);
            }
        }
    }

    default void withDocuments(boolean withUrlDownload) {
        if (getId() != null) {
            List<ProdukLampiran> attachments = ProdukLampiran.getDocumentsByProductId(getId());
            if (withUrlDownload && attachments != null && !attachments.isEmpty()) {
                for (ProdukLampiran attachment : attachments) {
                    if (attachment.dok_id_attachment != null) {
                        //BlobTable blob = BlobTable.find("blb_id_content = ?", attachment.dok_id_attachment).first();
                        BlobTable blob = BlobTable.find("id = ?", attachment.dok_id_attachment).first();
                        if (blob != null) {
                            attachment.original_file_name = attachment.file_name;
                            attachment.urlDownload = blob.getDownloadUrl(null);
                        }
                    }
                    else{
                        attachment.urlDownload = "/public/files/upload/produk_lampiran/"+attachment.file_sub_location+attachment.file_name;
                    }
                }
            }
            setDocuments(attachments);
        }
    }

    default void withPrice() {
        if (getId() != null) {
            ProdukHarga produkHarga = ProdukHarga.getByProductId(getId());
            if (produkHarga != null) {
                produkHarga.setPriceJson();
                setPrice(produkHarga);
            }
        }
    }

    default void withPricesEqualsDatePrice() {
        Produk p = Produk.findByIdCheck(getId());
        if (getId() != null) {
            List<ProdukHarga> list = ProdukHarga.geListByProductIdSetuju(getId(), p.harga_tanggal);
            if(list != null ){
                setPrices(list);
            }
        }
    }

    default void withProvider() {
        if (getProviderId() != null) {
            setProvider(Penyedia.findById(getProviderId()));

        }
    }

    default void withCommodity() {
        if (getCommodityId() != null) {
            setCommodity(Komoditas.findById(getCommodityId()));
        }
    }

    default void withSpecifications() {
        if (getId() != null) {
            setSpecifications(ProdukAtributValue.findByProdukId(getId()));
        }
    }

    default void withProductHistory() {
        if (getId() != null) {
            setProductHistory(ProdukRiwayat.findByProdukId(getId()));
        }
    }

    default void withManufacture() {
        if (getManufactureId() != null) {
            setManufacture(Manufaktur.findById(getManufactureId()));
        }
    }

    default void withCategory() {
        if (getCategoryId() != null) {
            setCategory(ProdukKategori.findById(getCategoryId()));
        }
    }

    default void withProvincePrice(Long provinceId) {
        if (getId() != null && provinceId != null && getCommodity() != null
                && getCommodity().isProvince()) {
            LogUtil.d("ProductContract", "get province price");
            setProvincePrices(ProdukWilayahJualProvinsi.getHargaByProvinsiId(getId(), provinceId));
        }
    }

    default void withRegencyPrice(Long regencyId) {
        if (getId() != null && regencyId != null && getCommodity() != null && getCommodity().isRegency()) {
            LogUtil.d("ProductContract", "get regency price " + getId() + " " + regencyId );
            setRegencyPrices(ProdukWilayahJualKabupaten.getHargaByKabupatenIdNonApproved(getId(), regencyId));
        }
    }

    default boolean allowToUpdate(Long userId) {
        if (getProvider() == null) {
            withProvider();
        }
        return (getProvider() != null && getProvider().isUserIdEqual(userId)) || (getCreatedBy() == userId.intValue());
    }

    default void withPenawaran() {
        if (getPenawaranId() != null) {
            setPenawaran(Penawaran.findById(getPenawaranId()));
        }
    }

    default void withPrices() {
        if (getId() != null) {
            List<ProdukHarga> list = ProdukHarga.geListByProductId(getId());
            if(list != null ){
                setPrices(list);
            }
        }
    }
    default void withPricesApproved() {
        if (getId() != null) {
            List<ProdukHarga> list = ProdukHarga.geListByProductIdSetuju(getId());
            if(list != null ){
                setPrices(list);
            }
        }
    }
}
