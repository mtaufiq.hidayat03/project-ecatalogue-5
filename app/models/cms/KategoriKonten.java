package models.cms;

import play.cache.Cache;
import play.db.jdbc.CacheMerdeka;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import models.common.AktifUser;
import models.BaseKatalogTable;

import javax.persistence.Transient;


/**
 * Created by user on 10/2/17.
 */

@Table(name="konten_kategori")
@CacheMerdeka
public class KategoriKonten extends BaseKatalogTable{
    @Id
    public Long id;
    public Long parent_id;
    public Long konten_kategori_utama_id;
    public String nama_kategori;
    public String deskripsi;
    public boolean active;

    @Transient
    public  String nama_konten;

    public static void deleteById(long id){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String date_now = dateFormat.format(date);

        String query = "update konten_kategori set active = 0, deleted_by =" + AktifUser.getActiveUserId() + ", deleted_date = '" + date_now + "' where id = ?";
        Query.update(query, id);
    }

    @Transient
    public static String key2 = "kategorikontenallactive";
    public static List<KategoriKonten> findAllActive(){
        List<KategoriKonten> result = Cache.get(key2,List.class);
        if(null == result){
            result = KategoriKonten.find("active = 1").fetch();
            Cache.set(key2,result,"30mn");
        }
        return result;
    }

    @Transient
    public static String key1 ="allkategorikontenbykategoriutamaid";
    public static List<KategoriKonten> findAllByKonten(int kontenId){
        List<KategoriKonten> result = Cache.get(key1+kontenId,List.class);
        if(null == result){
            result = KategoriKonten.find("active = 1 and konten_kategori_utama_id = " + kontenId).fetch();
            Cache.set(key1+kontenId, result,"1d");
        }
        return result;
    }

    public static List<KategoriKonten> findByKategori(String kategori){
        String query = "SELECT id, nama_kategori, deskripsi FROM konten_kategori WHERE nama_kategori = '" + kategori + "' ";
        return Query.find(query,KategoriKonten.class).fetch();
    }

    public static String getNamaKategori(String nama_kategori) {
        return nama_kategori;
    }

    public void setAktif() {
        this.active = true;
        prePersist();
        save();
    }

    public void setNonAktif() {
        this.active = false;
        prePersist();
        save();
    }
}