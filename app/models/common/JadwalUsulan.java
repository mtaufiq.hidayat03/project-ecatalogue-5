package models.common;

import models.katalog.komoditasmodel.KomoditasTahapanTemplateJadwal;
import models.katalog.komoditasmodel.KomoditasTemplateJadwal;
import models.masterdata.Tahapan;
import models.prakatalog.Usulan;
import models.prakatalog.UsulanJadwal;
import models.prakatalog.UsulanJadwalRiwayat;
import models.prakatalog.form.FormBukaPenawaran;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import play.Logger;
import utils.DateTimeUtils;
import utils.LogUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author raihaniqbal
 *
 */
public class JadwalUsulan {

	public static final String TAG = "JadwalUsulan";

	public long id;

	public String nama_tahapan;

	public String tanggal_mulai;

	public String tanggal_selesai;

	public boolean awal_penawaran;

	public boolean akhir_penawaran;

	public long tahapan_id;

	public Integer jumlah_perubahan;

	public String keterangan;

	public Integer urutan_tahapan;



	public static List<JadwalUsulan> getJadwalUsulan(Usulan usulan){
		List<JadwalUsulan> jadwalUsulanList = new ArrayList<JadwalUsulan>();
		
		if(usulan.status.equals("baru")){
			KomoditasTemplateJadwal template = KomoditasTemplateJadwal.findById(usulan.template_jadwal_id);
			if(template == null){
				return null;

			}else{

				List<KomoditasTahapanTemplateJadwal> templateTahapanList = KomoditasTahapanTemplateJadwal.findByTemplateId(template.id);

				for (KomoditasTahapanTemplateJadwal templateTahapan : templateTahapanList){
					JadwalUsulan jadwalUsulan = new JadwalUsulan();
					jadwalUsulan.nama_tahapan = templateTahapan.nama_tahapan;
					jadwalUsulan.tahapan_id = templateTahapan.tahapan_id;
					jadwalUsulan.tanggal_mulai = null;
					jadwalUsulan.tanggal_selesai = null;
					jadwalUsulan.awal_penawaran = false;
					jadwalUsulan.akhir_penawaran = false;
					jadwalUsulan.urutan_tahapan = Tahapan.getUrutanFlaggingInt(templateTahapan.tahapan_id);
					jadwalUsulanList.add(jadwalUsulan);
				}
			}
		}else{
			List<UsulanJadwal> usulanJadwalList = UsulanJadwal.listJadwal(usulan.id);

			for(UsulanJadwal usulanJadwal : usulanJadwalList){
				JadwalUsulan jadwalUsulan = new JadwalUsulan();
				jadwalUsulan.id = usulanJadwal.id;
				jadwalUsulan.nama_tahapan = usulanJadwal.nama_tahapan;
				jadwalUsulan.tanggal_mulai = DateTimeUtils.formatDateToString(usulanJadwal.tanggal_mulai,"dd-MM-yyyy");
				jadwalUsulan.tanggal_selesai = DateTimeUtils.formatDateToString(usulanJadwal.tanggal_selesai,"dd-MM-yyyy");
				jadwalUsulan.awal_penawaran = usulanJadwal.adalah_awal_penawaran;
				jadwalUsulan.akhir_penawaran = usulanJadwal.adalah_akhir_penawaran;
				jadwalUsulan.urutan_tahapan = Tahapan.getUrutanFlaggingInt(usulanJadwal.tahapan_id);
				jadwalUsulan.tahapan_id = usulanJadwal.tahapan_id;
				jadwalUsulan.jumlah_perubahan = usulanJadwal.jumlah_perubahan;
				jadwalUsulanList.add(jadwalUsulan);
			}
		}

		return jadwalUsulanList;
	}

	public static JadwalUsulan getSingleJadwalUsulan(Usulan usulan, Tahapan tahapan){
		UsulanJadwal usulanJadwal = UsulanJadwal.findByUsulanIdAndTahapanId(usulan.id, tahapan.id);

		JadwalUsulan jadwalUsulan = new JadwalUsulan();
		jadwalUsulan.id = usulanJadwal.id;
		jadwalUsulan.tanggal_mulai = DateTimeUtils.formatDateToString(usulanJadwal.tanggal_mulai,"dd-MM-yyyy");
		jadwalUsulan.tanggal_selesai = DateTimeUtils.formatDateToString(usulanJadwal.tanggal_selesai,"dd-MM-yyyy");

		return jadwalUsulan;
	}

	public static void simpanJadwal(FormBukaPenawaran form) {
		UsulanJadwal.delete("usulan_id = ?", form.usulan_id);
		for (JadwalUsulan jadwalUsulan : form.jadwalUsulanList) {

			UsulanJadwal usulanJadwal = new UsulanJadwal();
			usulanJadwal.tahapan_id = jadwalUsulan.tahapan_id;
			usulanJadwal.usulan_id = form.usulan_id;
			usulanJadwal.tanggal_mulai = JadwalUsulan.convertStringToDate(jadwalUsulan.tanggal_mulai);
			usulanJadwal.tanggal_selesai = JadwalUsulan.convertStringToDate(jadwalUsulan.tanggal_selesai);
			usulanJadwal.active = true;

			usulanJadwal.save();

		}
	}

	public static void updateAllJadwal(FormBukaPenawaran form){
		for(JadwalUsulan ju:form.jadwalUsulanList){
			Date tanggalMulai = JadwalUsulan.convertStringToDate(ju.tanggal_mulai);
			Date tanggalSelesai = JadwalUsulan.convertStringToDate(ju.tanggal_selesai);
			UsulanJadwal uj = UsulanJadwal.findById(ju.id);
			if(!tanggalMulai.equals(uj.tanggal_mulai) || !tanggalSelesai.equals(uj.tanggal_selesai)){
				// Simpan Riwayat Perubahan
				UsulanJadwalRiwayat ujr = new UsulanJadwalRiwayat();
				ujr.setUser(Long.valueOf(AktifUser.getActiveUserId()));
				ujr.setFromUsulanJadwal(uj);
				ujr.setNote(ju.keterangan);
				ujr.save();
				// Simpan Perubahan Usulan Jadwal
				uj.tanggal_mulai = tanggalMulai;
				uj.tanggal_selesai = tanggalSelesai;
				uj.save();
			}
			
		}
	}

	public static Date convertStringToDate(String string){
		if(!StringUtils.isEmpty(string)) {

			try {
				return DateTimeUtils.formatStringToDate(string);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	public boolean masih_berlaku() {
		final Date tglSelesai = convertStringToDate(tanggal_selesai);
		Date tglMulai = convertStringToDate(tanggal_mulai);
		boolean isBerlaku = true;
		java.util.Date currentDate= DateUtils.truncate(new java.util.Date(), Calendar.DATE);
		boolean cselesai = (currentDate.equals(tglSelesai) ^ currentDate.before(tglSelesai));
		boolean cmulai = (currentDate.equals(tglMulai) ^ currentDate.after(tglMulai));
		Logger.info("periode jadwal: %s %s", cmulai, cselesai);
		if(currentDate.equals(tglSelesai) && currentDate.equals(tglMulai)) {
			Logger.info("single day periode");
			return isBerlaku;
		}else if ( cmulai && cselesai){
			return isBerlaku;
		}else {
			isBerlaku=false;
		}
		return isBerlaku;
	}

	public boolean akhir_penawaran() {
		LogUtil.debug(TAG, "awal: " + tanggal_mulai);
		LogUtil.debug(TAG, "selesai: " + tanggal_selesai);
		final Date tglSelesai = convertStringToDate(tanggal_selesai);
		boolean isAkhir = true;
		java.util.Date currentDate= DateUtils.truncate(new java.util.Date(), Calendar.DATE);
		boolean cselesai = (currentDate.equals(tglSelesai) ^ currentDate.before(tglSelesai));
		Logger.info("akhir jadwal penawaran: %s ", cselesai);
		if(currentDate.equals(tglSelesai)) {
			Logger.info("single day periode");
			return isAkhir;
		}else if (cselesai){
			return isAkhir;
		}else {
			isAkhir = false;
		}
		return isAkhir;
	}
}
