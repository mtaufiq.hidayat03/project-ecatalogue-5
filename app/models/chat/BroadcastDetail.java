package models.chat;

import models.BaseKatalogTable;
import models.penyedia.PenyediaKomoditas;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

@Table(name = "broadcast_detail")
public class BroadcastDetail extends BaseKatalogTable {
    @Id
    public Long id;
    public Long broadcast_id;
    public Long user_id;
    public Boolean is_read;
    public Boolean active;
    @Transient
    public String broadcast_deskripsi;
    @Transient
    public String broadcast_judul;
    @Transient
    public Boolean getIs_read;

    public static List<BroadcastDetail> findBroadcastDetailByUserIdLoginUserRoleIdKomoditasId(Long user_id, Long  user_role_grup_id, List<PenyediaKomoditas> komoditas_id){
        String query = "SELECT bc.id as broadcast_id,bc.judul as broadcast_judul,bc.deskripsi as broadcast_deskripsi,bc.created_date,bd.id, bd.is_read, bd.user_id " +
        "FROM broadcast bc LEFT JOIN broadcast_detail bd ON  bc.id = bd.broadcast_id " +
        "WHERE bd.user_id = "+user_id+" " +
//        "WHERE bd.user_id = (select user_id from broadcast_detail where user_id = "+user_id+" )" +
        "or bc.user_role_grup_id = "+user_role_grup_id+" " +
        "or bc.komoditas_id IN "+komoditas_id+" " +
        "ORDER BY bc.created_date desc";

        if (komoditas_id == null){
            query=query.replace("IN null","IN ('')");
        }

        query=query.replace("[","(");
        query=query.replace("]",")");

        System.out.println(query);
        return Query.find(query,BroadcastDetail.class).fetch();
    }

    public static BroadcastDetail findByBroadcastIdAndUserId(Long broadcast_id, Long user_id){
        return BroadcastDetail.find("broadcast_id = ? and user_id = ?", broadcast_id, user_id).first();
    }

}
