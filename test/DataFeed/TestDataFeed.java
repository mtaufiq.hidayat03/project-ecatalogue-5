package DataFeed;

import com.google.gson.Gson;
import jobs.ExtProduct.ExtCrawlVendor;
import models.penyedia.Penyedia;
import org.junit.Test;
import play.Logger;
import play.test.UnitTest;

import java.util.List;

public class TestDataFeed extends UnitTest {
    //@Test
    public void Test(){
        ExtCrawlVendor varc = new ExtCrawlVendor();
        List<Penyedia> listPenyedia = Penyedia.getVendorAPI().subList(0,10);
        for (Penyedia py: listPenyedia) {
            varc.CrawlVendor(py.komoditas_id, py.id);
        }
    }
}
