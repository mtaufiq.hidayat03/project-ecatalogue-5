package models.api;

import models.jcommon.util.CommonUtil;
import models.blacklist.Blacklist;
import play.Logger;
import utils.KatalogUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by reza on 27/08/18
 */
public class DceBlacklist {
	public Long id;
	public String rkn_id;
	public String sk;
	public String nama_penyedia;
	public String npwp;
	public String alamat;
	public String alamat_tambahan;
	public String direktur;
	public String npwp_direktur;
	public String repo_id;
	public String tahun_anggaran;
	public String dibuat;
	public String sk_pencabutan;
	public DceBlacklistPropinsi provinsi;
	public DceBlacklistKabupaten kabupaten;
	public DceBlacklistSatker satuan_kerja;
	
	public List<DcePelanggaran> pelanggaran;
	
	private static final SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");


	public Blacklist getAsBlacklist(){
		try{
			Blacklist blacklist = new Blacklist();
			blacklist.blacklist_source_id = id;
			blacklist.rkn_id = rkn_id;
			blacklist.nama_penyedia = nama_penyedia;
			blacklist.npwp = npwp;
			blacklist.alamat = alamat;
			blacklist.alamat_tambahan = alamat_tambahan;
			blacklist.direktur = direktur;
			blacklist.npwp_direktur = npwp_direktur;
			blacklist.repo_id = repo_id;
			blacklist.tahun_anggaran = Integer.valueOf(tahun_anggaran);
			blacklist.created_at = df.parse(dibuat);
			blacklist.sk_pencabutan = sk_pencabutan;
			blacklist.sk = sk;
			if(provinsi!=null) {
				blacklist.provinsi_id = provinsi.id;
				blacklist.provinsi_nama = provinsi.nama;
			}
			if(kabupaten!=null) {
				blacklist.kabupaten_id = kabupaten.id;
				blacklist.kabupaten_nama = kabupaten.nama;
			}
			if(satuan_kerja!=null) {
				blacklist.satker_id = Long.valueOf(satuan_kerja.id);
				blacklist.satker_nama = satuan_kerja.nama;
			}
			return blacklist;
		}catch (Exception e){
			Logger.error("error parse DceBlacklist to blacklist"+ e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

}
