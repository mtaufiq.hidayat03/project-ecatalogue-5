package jobs.elasticsearch;

import models.jcommon.config.Configuration;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import play.jobs.On;

import java.util.Date;
//jam 13:15:1 every day
//@On("1 15 13 * * ?")
@NoTransaction
public class ExecuteJobTest extends Job {
    public void doJob()
    {
        sendEmailReport("Start job at:"+ new Date(),"Start Job Elastic");
        Logger.info("jalankan cron job elastic");
        ElasticsearchLog es = new ElasticsearchLog();
        try {
            es.processWithJob();
        } catch (Exception e) {
            Logger.info("error jalankan job elastic");
            e.printStackTrace();
        }
        sendEmailReport("End Job Elastic at:"+new Date(),"End Job Elastic");
    }

    public void sendEmailReport(String body, String title){
        //String title = "Report JOB elastics";
        String report = Configuration.getConfigurationValue(MailQueue.SMTP_MAIL_ADMIN, "");
        String[] ary = report.split(";");
        for (String to : ary) {
            MailQueue mq = new MailQueue();
            try {
                Logger.info(to+" "+body+" "+title);
                mq = EmailManager.createEmail(to, body, title);

            } catch (Exception e) {
                Logger.error("error create mail:"+e.getMessage());
            }
            Logger.info(mq.save()+"");
        }
    }
}
