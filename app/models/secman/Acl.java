package models.secman;

import play.db.jdbc.Enumerated;

import javax.persistence.EnumType;

/**
 * Created by dadang on 9/6/17.
 */
@Enumerated(EnumType.STRING)
public enum Acl {

//	-- START USER MANAGEMENT --

	COM_USER_GRUP_ROLE("comUserGrupRole","Akses daftar grup role"),
	COM_USER_GRUP_ROLE_ADD("comUserGrupRoleAdd","Tambah grup role"),
	COM_USER_GRUP_ROLE_EDIT("comUserGrupRoleEdit","Edit grup role"),
	COM_USER_GRUP_ROLE_DELETE("comUserGrupRoleDelete","Hapus grup role"),

	COM_USER("comUser","Akses daftar user"),
	COM_USER_ADD("comUserAdd","Tambah user"),
	COM_USER_EDIT("comUserEdit","Edit user"),
	COM_USER_DELETE("comUserDelete","Hapus user"),

	COM_USER_LOKAL("comUserLokal","Akses daftar user lokal sektoral"),
	COM_USER_LOKAL_ADD("comUserLokalAdd","Tambah user lokal sektoral"),
	COM_USER_LOKAL_EDIT("comUserLokalEdit","Edit user lokal sektoral"),
	COM_USER_LOKAL_DELETE("comUserLokalDelete","Hapus user lokal sektoral"),

//	-- END USER MANAGEMENT --

//	-- END BROADCAST --
	COM_BROADCAST_ADD("comBroadcastAdd","Tambah Broadcast"),
//	-- START BROADCAST --

//	-- START CMS --
	COM_BANNER("comBanner", "Akses Manajemen Banner"),
	COM_BANNER_ADD("comBannerAdd", "Akses Tambah Banner"),
	COM_BANNER_EDIT("comBannerEdit", "Akses Edit Banner"),
	COM_BANNER_DELETE("comBannerDelete", "Akses Hapus Banner"),

	COM_BERITA("comBerita", "Akses Manajemen Berita"),
	COM_BERITA_ADD("comBeritaAdd", "Akses Tambah Berita"),
	COM_BERITA_EDIT("comBeritaEdit", "Akses Edit Berita"),
	COM_BERITA_DELETE("comBeritaDelete", "Akses Hapus Berita"),

	COM_KONTEN_STATIS("comKontenStatis","Akses daftar konten statis"),
	COM_KONTEN_STATIS_DELETE("comKontenStatisDelete","Hapus daftar konten statis"),
	COM_KONTEN_STATIS_EDIT("comKontenStatisEdit","Edit konten statis"),
	COM_KONTEN_STATIS_ADD("comKontenStatisAdd","Tambah konten statis"),
	COM_KONTEN_STATIS_KATEGORI("comKontenStatisKategori","Akses kategori konten statis"),
	COM_KONTEN_STATIS_KATEGORI_DELETE("comKontenStatisKategoriDelete","Hapus kategori konten statis"),
	COM_KONTEN_STATIS_KATEGORI_EDIT("comKontenStatisKategoriEdit","Edit kategori konten statis"),
	COM_KONTEN_STATIS_KATEGORI_ADD("comKontenStatisKategoriAdd","Tambah kategori konten statis"),

	COM_KATEGORI_KONTEN("comKategoriKonten","Akses kategori konten"),
	COM_KATEGORI_KONTEN_ADD("comKategoriKontenAdd","Tambah kategori konten"),
	COM_KATEGORI_KONTEN_EDIT("comKategoriKontenEdit","Edit kategori konten"),
	COM_KATEGORI_KONTEN_DELETE("comKategoriKontenDelete","Hapus kategori konten"),

	COM_SYARAT_KETENTUAN("comSyaratKetentuan","Akses daftar syarat dan ketentuan"),
	COM_SYARAT_KETENTUAN_DELETE("comSyaratKetentuanDelete","Hapus daftar syarat dan ketentuan"),
	COM_SYARAT_KETENTUAN_EDIT("comSyaratKetentuanEdit","Edit syarat dan ketentuan"),
	COM_SYARAT_KETENTUAN_ADD("comSyaratKetentuanAdd", "Tambah syarat dan ketentuan"),

	COM_KONTEN_DINAMIS_ADD("comKontenDinamisAdd","Tambah konten dinamis"),
	COM_KONTEN_DINAMIS_EDIT("comKontenDinamisEdit","Edit konten dinamis"),
	COM_KONTEN_DINAMIS_DELETE("comKontenDinamisDelete","Hapus konten dinamis"),
	COM_KONTEN_DINAMIS("comKontenDinamis","Akses konten dinamis"),
	COM_KONTEN_DINAMIS_KATEGORI("comKontenDinamisKategori","Akses kategori konten dinamis"),
	COM_KONTEN_DINAMIS_KATEGORI_ADD("comKontenDinamisKategoriAdd","Tambah kategori konten dinamis"),
	COM_KONTEN_DINAMIS_KATEGORI_EDIT("comKontenDinamisKategoriEdit","Edit kategori konten dinamis"),
	COM_KONTEN_DINAMIS_KATEGORI_DELETE("comKontenDinamisKategoriDelete","Hapus kategori konten dinamis"),
	COM_KONTEN_DINAMIS_KATEGORI_HIRARKI("comKontenDinamisKategoriHirarki","Hirarki kategori konten dinamis"),

	COM_POLLING_ADD("comPollingAdd","Tambah polling"),
	COM_POLLING_EDIT("comPollingEdit","Edit polling"),
	COM_POLLING_DELETE("comPollingDelete","Hapus polling"),
	COM_POLLING("comPolling","Akses polling"),

	COM_GALERI_ALBUM_ADD("comGaleriAlbumAdd","Tambah galeri album"),
	COM_GALERI_ALBUM_EDIT("comGaleriAlbumEdit","Edit galeri album"),
	COM_GALERI_ALBUM_DELETE("comGaleriAlbumDelete","Hapus galeri album"),
	COM_GALERI_ALBUM("comGaleriAlbum","Akses galeri album"),
	COM_GALERI_FOTO_ADD("comGaleriFotoAdd","Tambah galeri foto"),
	COM_GALERI_FOTO_EDIT("comGaleriFotoEdit","Edit galeri foto"),
	COM_GALERI_FOTO_DELETE("comGaleriFotoDelete","Hapus galeri foto"),
	COM_GALERI_FOTO("comGaleriFoto","Akses galeri foto"),
	COM_GALERI_ALBUM_HIRARKI("comGaleriAlbumHirarki","Hirarki galeri album"),
	COM_GALERI_FOTO_HIRARKI("comGaleriFotoHirarki","Hirarki galeri foto"),

	COM_FAQ_KATEGORI_ADD("comFaqKategoriAdd","Tambah kategori FAQ"),
	COM_FAQ_KATEGORI_EDIT("comFaqKategoriEdit","Edit kategori FAQ"),
	COM_FAQ_KATEGORI_DELETE("comFaqKategoriDelete","Hapus kategori FAQ"),
	COM_FAQ_KATEGORI_HIRARKI("comFaqKategoriHirarki","Hirarki kategori FAQ"),
	COM_FAQ_KATEGORI("comFaqKategori","Akses kategori FAQ"),
	COM_FAQ_ADD("comFaqAdd","Tambah FAQ"),
	COM_FAQ_EDIT("comFaqEdit","Edit FAQ"),
	COM_FAQ_DELETE("comFaqDelete","Hapus FAQ"),
	COM_FAQ("comFaq","Akses FAQ"),
	COM_FAQ_HIRARKI("comFaqHirarki","Hirarki FAQ"),

	COM_PUSTAKA_FILE_KATEGORI_ADD("comPustakaFileKategoriAdd","Tambah kategori pustaka file"),
	COM_PUSTAKA_FILE_KATEGORI_EDIT("comPustakaFileKategoriEdit","Edit kategori pustaka file"),
	COM_PUSTAKA_FILE_KATEGORI_DELETE("comPustakaFileKategoriDelete","Hapus kategori pustaka file"),
	COM_PUSTAKA_FILE_KATEGORI_HIRARKI("comPustakaFileKategoriHirarki","Hirarki kategori pustaka file"),
	COM_PUSTAKA_FILE_KATEGORI("comPustakaFileKategori","Akses kategori pustaka file"),
	COM_PUSTAKA_FILE_ADD("comPustakaFileAdd","Tambah pustaka file"),
	COM_PUSTAKA_FILE_EDIT("comPustakaFileEdit","Edit pustaka file"),
	COM_PUSTAKA_FILE_DELETE("comPustakaFileDelete","Hapus pustaka file"),
	COM_PUSTAKA_FILE_HIRARKI("comPustakaFileHirarki","Hirarki pustaka file"),
	COM_PUSTAKA_FILE("comPustakaFile","Akses pustaka file"),

	COM_MENU_ADD("comMenuAdd","Tambah menu"),
	COM_MENU_EDIT("comMenuEdit","Edit menu"),
	COM_MENU_DELETE("comMenuDelete","Hapus menu"),
	COM_MENU("comMenu","Akses menu"),
	COM_MENU_HIRARKI("comMenuHirarki","Hirarki menu"),

//	-- END CMS --

//	-- START KOMODITAS --
	COM_PENGUMUMAN("comPengumuman", "Akses Pengumuman"),
//	-- END KOMODITAS --

//	-- START KOMODITAS --

	COM_KOMODITAS("comKomoditas","Akses komoditas"),
	COM_KOMODITAS_ADD("comKomoditasAdd","Tambah komoditas"),
	COM_KOMODITAS_EDIT("comKomoditasEdit","Edit komoditas"),
	COM_KOMODITAS_DELETE("comKomoditasDelete","Hapus komoditas"),
	COM_KOMODITAS_HIRARKI("comKomoditasHirarki","Hirarki komoditas"),

	COM_KOMODITAS_KATEGORI("comKomoditasKategori","Akses komoditas kategori"),
	COM_KOMODITAS_KATEGORI_EDIT("comKomoditasKategoriEdit","Edit komoditas kategori"),
	COM_KOMODITAS_KATEGORI_DELETE("comKomoditasKategoriDelete","Hapus komoditas kategori"),
	COM_KOMODITAS_KATEGORI_HIRARKI("comKomoditasKategoriHirarki","Hirarki komoditas kategori"),
	COM_KOMODITAS_KATEGORI_ADD("comKomoditasKategoriAdd","Tambah komoditas kategori"),

	COM_KOMODITAS_HARGA_ATRIBUT("comKomoditasHargaAtribut","Akses komoditas atribut harga produk"),
	COM_KOMODITAS_HARGA_ATRIBUT_HIRARKI("comKomoditasHargaAtributHirarki","Hirarki komoditas atribut harga produk"),

	COM_KOMODITAS_PRODUK_ATRIBUT_TIPE("comKomoditasProdukAtributTipe","Akses komoditas tipe atribut produk"),
	COM_KOMODITAS_PRODUK_ATRIBUT_TIPE_ADD("comKomoditasProdukAtributTipeAdd","Tambah komoditas tipe atribut produk"),
	COM_KOMODITAS_PRODUK_ATRIBUT_TIPE_EDIT("comKomoditasProdukAtributTipeEdit","Edit komoditas tipe atribut produk"),
	COM_KOMODITAS_PRODUK_ATRIBUT_TIPE_DELETE("comKomoditasProdukAtributTipeDelete","Hapus komoditas tipe atribut produk"),
	COM_KOMODITAS_PRODUK_ATRIBUT_TIPE_HIRARKI("comKomoditasProdukAtributTipeHirarki","Hirarki komoditas tipe atribut produk"),

	COM_KOMODITAS_ICON("comKomoditasIcon","Kelola ikon komoditas"),
	COM_KOMODITAS_KUNCI("comKomoditasKunci","Kunci Komoditas"),

	COM_MANUFAKTUR("comManufaktur","Akses manufaktur"),
	COM_MANUFAKTUR_ADD("comManufakturAdd","Tambah manufaktur"),
	COM_MANUFAKTUR_IMPORT("comManufakturImport","Import manufaktur"),
	COM_MANUFAKTUR_EDIT("comManufakturEdit","Edit manufaktur"),
	COM_MANUFAKTUR_DELETE("comManufakturDelete","Hapus manufaktur"),

//	-- END KOMODITAS --

//	-- START PENYEDIA --

	COM_PENYEDIA("comPenyedia","Akses penyedia"),
	COM_PENYEDIA_ADD("comPenyediaAdd","Tambah penyedia"),
	COM_PENYEDIA_EDIT("comPenyediaEdit","Edit penyedia"),
	COM_PENYEDIA_DELETE("comPenyediaDelete","Delete penyedia"),
	COM_PENYEDIA_DISTRIBUTOR("comPenyediaDistributor","Akses distributor penyedia"),
	COM_PENYEDIA_DISTRIBUTOR_ADD("comPenyediaDistributorAdd","Tambah distributor penyedia"),
	COM_PENYEDIA_DISTRIBUTOR_EDIT("comPenyediaDistributorEdit","Edit distributor penyedia"),
	COM_PENYEDIA_DISTRIBUTOR_DELETE("comPenyediaDistributorDelete","Delete distributor penyedia"),

	COM_PENYEDIA_LOKAL("comPenyediaLokal", "Akses Penyedia Lokal"),
	COM_PENYEDIA_LOKAL_ADD("comPenyediaLokalAdd","Tambah Penyedia Lokal"),
	COM_PENYEDIA_LOKAL_DELETE("comPenyediaLokalDelete","Hapus Penyedia Lokal"),

	COM_PENYEDIA_FILE_KONTRAK_KELOLA("comPenyediaFileKontrakKelola","Kelola file kontrak penyedia (upload, edit, delete)"),
	COM_PENYEDIA_FILE_KONTRAK_DOWNLOAD("comPenyediaFileKontrakDownload","Download file kontrak penyedia"),

	COM_PENYEDIA_DOKUMEN_UKM_VIEW("comDokumenUkmView","Download file dokumen pendukung ukm penyedia"),

//	-- END PENYEDIA --

//	-- START MASTER DATA --

	COM_PROVINSI("comProvinsi","Akses provinsi"),
	COM_PROVINSI_ADD("comProvinsiAdd","Tambah provinsi"),
	COM_PROVINSI_EDIT("comProvinsiEdit","Edit provinsi"),
	COM_PROVINSI_DELETE("comProvinsiDelete","Hapus provinsi"),
	COM_PROVINSI_IMPORT("comProvinsiImport","Import provinsi"),

	COM_INSTANSI("comInstansi","Akses instansi"),
	COM_INSTANSI_ADD("comInstansiAdd","Tambah instansi"),
	COM_INSTANSI_EDIT("comInstansiEdit","Edit instansi"),
	COM_INSTANSI_DELETE("comInstansiDelete","Hapus instansi"),

	COM_INSTANSI_JENIS("comInstansiJenis","Akses jenis instansi"),
	COM_INSTANSI_JENIS_ADD("comInstansiJenisAdd","Tambah jenis instansi"),
	COM_INSTANSI_JENIS_EDIT("comInstansiJenisEdit","Edit jenis instansi"),
	COM_INSTANSI_JENIS_DELETE("comInstansiJenisDelete","Hapus jenis instansi"),

	COM_KABUPATEN_KOTA("comKabupatenKota","Akses kabupaten/kota"),
	COM_KABUPATEN_KOTA_ADD("comKabupatenKotaAdd","Tambah kabupaten/kota"),
	COM_KABUPATEN_KOTA_EDIT("comKabupatenKotaEdit","Edit kabupaten/kota"),
	COM_KABUPATEN_KOTA_DELETE("comKabupatenKotaDelete","Hapus kabupaten/kota"),

	COM_KURS("comKurs","Akses kurs"),
	COM_KURS_ADD("comKursAdd","Tambah kurs"),
	COM_KURS_EDIT("comKursEdit","Edit kurs"),
	COM_KURS_DELETE("comKursDelete","Hapus kurs"),

	COM_SUMBER_KURS("comKursSumber","Akses sumber kurs"),
	COM_SUMBER_KURS_ADD("comKursSumberAdd","Tambah sumber kurs"),
	COM_SUMBER_KURS_EDIT("comKursSumberEdit","Edit sumber kurs"),
	COM_SUMBER_KURS_DELETE("comKursSumberDelete","Hapus sumber kurs"),

	COM_KURS_NILAI("comKursNilai","Akses input nilai kurs"),
	COM_KURS_NILAI_ADD("comKursNilaiAdd","Add input nilai kurs"),
	COM_KURS_NILAI_EDIT("comKursNilaiEdit","Edit input nilai kurs"),
	COM_KURS_NILAI_DELETE("comKursNilaiDelete","Hapus input nilai kurs"),

	COM_KATEGORI_PRODUK("comKategoriProduk","Akses kategori produk"),
	COM_KATEGORI_PRODUK_ADD("comKategoriProdukAdd","Tambah kategori produk"),
	COM_KATEGORI_PRODUK_EDIT("comKategoriProdukEdit","Edit kategori produk"),
	COM_KATEGORI_PRODUK_DELETE("comKategoriProdukDelete","Hapus kategori produk"),
	COM_KATEGORI_PRODUK_HIRARKI("comKategoriProdukHirarki","Hirarki kategori produk"),

	COM_UNIT_PENGUKURAN("comUnitPengukuran","Akses unit pengukuran"),
	COM_UNIT_PENGUKURAN_IMPORT("comUnitPengukuranImport","Import unit pengukuran"),
	COM_UNIT_PENGUKURAN_EDIT("comUnitPengukuranEdit","Edit unit pengukuran"),
	COM_UNIT_PENGUKURAN_DELETE("comUnitPengukuranDelete","Hapus unit pengukuran"),
	COM_UNIT_PENGUKURAN_ADD("comUnitPengukuranAdd","Tambah unit pengukuran"),

	COM_SUMBER_DANA("comSumberDana","Akses sumber dana"),
	COM_SUMBER_DANA_ADD("comSumberDanaAdd","Tambah sumber dana"),
	COM_SUMBER_DANA_EDIT("comSumberDanaEdit","Edit sumber dana"),
	COM_SUMBER_DANA_DELETE("comSumberDanaDelete","Hapus sumber dana"),

	COM_DOCUMENT_TEMPLATE("comDocumentTemplate","Template Dokumen"),
	COM_DOCUMENT_TEMPLATE_ADD("comDocumentTemplateAdd","Add Template documen"),
	COM_DOCUMENT_TEMPLATE_EDIT("comDocumentTemplateEdit","Edit Template documen"),
	COM_DOCUMENT_TEMPLATE_DELETE("comDocumentTemplateDelete","Delete Template documen"),

	COM_TAHAPAN("comTahapan","Akses Master Data Tahapan"),
	COM_TAHAPAN_ADD("comTahapanAdd","Akses Tambah Tahapan"),
	COM_TAHAPAN_EDIT("comTahapanEdit","Akses Edit Tahapan"),
	COM_TAHAPAN_DELETE("comTahapanDelete","Akses Hapus Tahapan"),

	COM_BIDANG("comBidang","Akses Master Data Bidang"),
	COM_BIDANG_ADD("comBidangAdd","Akses Tambah Bidang"),
	COM_BIDANG_EDIT("comBidangEdit","Akses Edit Bidang"),
	COM_BIDANG_DELETE("comBidangDelete","Akses Hapus Bidang"),

	COM_PARAMETER_APLIKASI("comParameterAplikasi","Akses Manajemen Parameter Aplikasi"),
	COM_PARAMETER_APLIKASI_EDIT("comParameterAplikasiEdit","Akses Edit Parameter Aplikasi"),

	COM_WILAYAH("comWilayah","Akses Master Data Wilayah"),
	COM_WILAYAH_ADD("comWilayahAdd","Tambah wilayah"),
	COM_WILAYAH_EDIT("comWilayahEdit","Edit wilayah"),
	COM_WILAYAH_DELETE("comWilayahDelete","Hapus wilayah"),

	COM_RUP("comRUP","Akses Master Data RUP"),

	COM_BLACKLIST("comBLACKLIST","Akses Master Data Blacklist"),

	COM_PROFIL_PENYEDIA("comProfilPenyedia", "Akses profil penyedia"),
	COM_PROFIL_PENYEDIA_DISTRIBUTOR("comProfilPenyediaDistributor","Akses profil distributor penyedia"),
	COM_PROFIL_SATUAN_KERJA("comProfilSatuanKerja","Akses profil satuan kerja"),
	COM_PROFIL_SATUAN_KERJA_EDIT("comProfilSatuanKerjaEdit","Edit profil satuan kerja"),

//  -- END MASTER DATA --

//	-- START PRODUK --

	COM_PRODUK("comProduk","Akses produk"),
	COM_PRODUK_ADD("comProdukAdd","Tambah produk"),
	COM_PRODUK_EDIT("comProdukEdit","Edit produk"),
	COM_PRODUK_DELETE("comProdukDelete","Hapus produk"),
	COM_PRODUK_GAMBAR_KELOLA("comProdukGambarKelola","Kelola gambar produk"),
	COM_PRODUK_LAMPIRAN_KELOLA("comProdukLampiranKelola","Kelola lampiran produk"),
	COM_PRODUK_HARGA_KELOLA("comProdukHargaKelola","Kelola harga produk"),

	COM_PRODUK_WILAYAH_JUAL_KELOLA("comProdukWilayahJualKelola","Kelola wilayah jual produk"),
	COM_PRODUK_RIWAYAT_HARGA_LIHAT("comProdukRiwayatHargaLihat","Lihat riwayat harga produk"),

	COM_PRODUK_KATALOG_LOKAL("comProdukKatalogLokal","Kelola Admin dan Penyedia Katalog Lokal"),
	COM_PRODUK_LIHAT_PERBANDINGAN("comProdukLihatPerbandingan","Melihat Perbandingan Produk"),
	COM_PRODUK_LIHAT_RIWAYAT("comProdukLihatRiwayat","Melihat Riwayat Produk"),

	COM_PRODUK_KELOLA_STOK_INDEN("comProdukKelolaStokInden","Kelola stok inden produk"),

	COM_PRODUK_TUNGGU_SETUJU("comProdukTungguSetuju","Produk yang dibuat/diedit akan dipending dulu untuk persetujuan"),
	COM_PRODUK_MEMBERI_PERSETUJUAN("comProdukMemberiPersetujuan","Menolak atau menyetujui produk"),

	COM_PRODUK_MINTA_PERSUTUJUAN_TAYANG("comProdukMintaPersetujuanTayang","Meminta persetujuan tayang produk"),

	COM_PRODUK_TUNGGU_SETUJU_LOKAL("comProdukTungguSetujuLokal","Produk katalog lokal yang menunggu persetujuan"),
	COM_PRODUK_TUNGGU_SETUJU_SEKTORAL("comProdukTungguSetujuSektoral","Produk katalog sektoral yang menunggu persetujuan"),

	COM_PRODUK_KELOLA_STOK("comProdukKelolaStok","Kelola stok produk"),
	COM_PRODUK_KELOLA_STOK_BOLEH_MENGURANGI("comProdukKelolaStokBolehMengurangi","Diperbolehkan untuk mengurangi stok produk"),

	COM_PRODUK_BUKA_EDIT_FITUR("comProdukBukaEditFitur", "Buka fitur ubah produk"),
	COM_PRODUK_BELI_BTNBUY("comProdukBelibtnBuy", "Beli Produk button Buy di dashboard"),
    COM_PRODUK_EDIT_DAPAT_DIBELI("comProdukEditDapatDibeli","Edit Produk Dapat Dibeli"),
    COM_PRODUK_EDIT_DAPAT_DITAYANGKAN("comProdukEditDapatDitayangkan","Edit Produk Dapat Ditayangkan"),

	COM_PRODUK_REPORT_MANAGE("comProdukReportManage","Kelola Laporan Produk"),
	COM_PERMOHONAN_PEMBARUAN("comPermohonanPembaruan","Permohonan Pembaruan"),
	COM_PERMOHONAN_PEMBARUAN_ADD("comPermohonanPembaruanAdd","Tambah Permohonan Pembaruan"),
	COM_PERMOHONAN_PEMBARUAN_EDIT("comPermohonanPembaruanEdit","Edit Permohonan Pembaruan"),
	COM_PERMOHONAN_PEMBARUAN_DELETE("comPermohonanPembaruanDelete","Delete Permohonan Pembaruan"),
	COM_PERMOHONAN_PEMBARUAN_DETAIL("comPermohonanPembaruanDetail","Detail Permohonan Pembaruan"),
	COM_PERMOHONAN_PEMBARUAN_PROFIL("comPermohonanPembaruanProfil","Permohonan Pembaruan Profil"),
	COM_PERMOHONAN_PEMBARUAN_PROFIL_ADD("comPermohonanPembaruanProfilAdd","Permohonan Pembaruan Tambah Profil"),
	COM_PERMOHONAN_PEMBARUAN_PROFIL_EDIT("comPermohonanPembaruanProfilEdit","Permohonan Pembaruan Edit Profil"),
	COM_PERMOHONAN_PEMBARUAN_PRODUK("comPermohonanPembaruanProduk","Permohonan Pembaruan Produk"),
	COM_PERMOHONAN_PEMBARUAN_PRODUK_ADD("comPermohonanPembaruanProdukAdd","Permohonan Pembaruan Tambah Produk"),
	COM_PERMOHONAN_PEMBARUAN_PRODUK_EDIT("comPermohonanPembaruanProdukEdit","Permohonan Pembaruan Edit Produk"),
	COM_PERMOHONAN_PEMBARUAN_PRODUK_DELETE("comPermohonanPembaruanProdukDelete","Permohonan Pembaruan Delete Produk"),
	COM_PERMOHONAN_PEMBARUAN_DISTRIBUTOR("comPermohonanPembaruanDistributor","Permohonan Pembaruan Distributor"),
	COM_PERMOHONAN_PEMBARUAN_DISTRIBUTOR_ADD("comPermohonanPembaruanDistributorAdd","Permohonan Pembaruan Tambah Distributor"),
	COM_PERMOHONAN_PEMBARUAN_DISTRIBUTOR_EDIT("comPermohonanPembaruanDistributorEdit","Permohonan Pembaruan Edit Distributor"),
	COM_PERMOHONAN_PEMBARUAN_DISTRIBUTOR_DELETE("comPermohonanPembaruanDistributorDelete","Permohonan Pembaruan Delete Distributor"),
	COM_PERMOHONAN_PEMBARUAN_DISPOSISI("comPermohonanPembaruanDisposisi","Permohonan Pembaruan Disposisi"),
	COM_PERMOHONAN_PEMBARUAN_DISPOSISI_ADD("comPermohonanPembaruanDisposisiAdd","Permohonan Pembaruan Tambah Disposisi"),
	COM_PERMOHONAN_PEMBARUAN_DISPOSISI_EDIT("comPermohonanPembaruanDisposisiEdit","Permohonan Pembaruan Edit Disposisi"),
	COM_PERMOHONAN_PEMBARUAN_KOORDINASI("comPermohonanPembaruanKoordinasi","Permohonan Pembaruan Koordinasi"),
	COM_PERMOHONAN_PEMBARUAN_KOORDINASI_ADD("comPermohonanPembaruanKoordinasiAdd","Permohonan Pembaruan Tambah Koordinasi"),
	COM_PERMOHONAN_PEMBARUAN_KOORDINASI_EDIT("comPermohonanPembaruanKoordinasiEdit","Permohonan Pembaruan Edit Koordinasi"),
	COM_PERMOHONAN_PEMBARUAN_KLARIFIKASI("comPermohonanPembaruanKlarifikasi","Permohonan Pembaruan Klarifikasi"),
	COM_PERMOHONAN_PEMBARUAN_KLARIFIKASI_ADD("comPermohonanPembaruanKlarifikasiAdd","Permohonan Pembaruan Tambah Klarifikasi"),
	COM_PERMOHONAN_PEMBARUAN_KLARIFIKASI_EDIT("comPermohonanPembaruanKlarifikasiEdit","Permohonan Pembaruan Edit Klarifikasi"),
	COM_PERMOHONAN_PEMBARUAN_BA_KLARIFIKASI("comPermohonanPembaruanBaKlarifikasi","Permohonan Pembaruan BA Klarifikasi"),
	COM_PERMOHONAN_PEMBARUAN_BA_KLARIFIKASI_ADD("comPermohonanPembaruanBaKlarifikasiAdd","Permohonan Pembaruan Tambah BA Klarifikasi"),
	COM_PERMOHONAN_PEMBARUAN_HASILKLARIFIKASI("comPermohonanPembaruanHasilKlarifikasi","Permohonan Pembaruan Hasil Klarifikasi"),
	COM_PERMOHONAN_PEMBARUAN_HASILKLARIFIKASI_ADD("comPermohonanPembaruanHasilKlarifikasiAdd","Permohonan Pembaruan Tambah Hasil Klarifikasi"),
	COM_PERMOHONAN_PEMBARUAN_HASILKLARIFIKASI_EDIT("comPermohonanPembaruanHasilKlarifikasiEdit","Permohonan Pembaruan Edit Hasil Klarifikasi"),
	COM_PERMOHONAN_PEMBARUAN_KONTRAK("comPermohonanPembaruanKontrak","Permohonan Pembaruan Kontrak"),
	COM_PERMOHONAN_PEMBARUAN_KONTRAK_ADD("comPermohonanPembaruanKontrakAdd","Permohonan Pembaruan Tambah Kontrak"),
	COM_PERMOHONAN_PEMBARUAN_KONTRAK_EDIT("comPermohonanPembaruanKontrakEdit","Permohonan Pembaruan Edit Kontrak"),

//	-- END CHAT --
//	-- START PAKET --

	COM_PAKET("comPaket","Akses paket"),
	COM_PAKET_ADD("comPaketAdd","Tambah paket"),
	COM_PAKET_EDIT("comPaketEdit","Edit paket"),
	COM_PAKET_DELETE("comPaketDelete","Delete paket"),

	COM_PAKET_DELETE_PRODUK_DETAIL_PAKET("comPaketDeleteProdukDetailPaket","Hapus produk pada dialog detail paket"),
	COM_PAKET_SHOPPING_CART("comPaketShoppingCart","Keranjang pembelian produk"),
	COM_PAKET_NEGOSIASI("comPaketNegosiasi","Negosiasi harga produk (apabila diperlukan)"),
	COM_PAKET_CETAK_PESANAN("comPaketCetakPesanan","Cetak pesanan paket"),
	COM_PAKET_DOWNLOAD_SURAT_PEMESANAN("comPaketDownloadSuratPemesanan","Download surat pesanan"),
	COM_PAKET_WORKFLOW("comPaketWorkflow","Workflow paket (panitia <-> penyedia <-> PPK)"),
	COM_PAKET_RIWAYAT("comPaketRiwayat","Lihat riwayat paket"),

	COM_PAKET_RIWAYAT_NEGOSIASI("comPaketRiwayatNegosiasi","Lihat riwayat negosiasi"),
	COM_PAKET_RIWAYAT_PEMBAYARAN("comPaketRiwayatPembayaran","Lihat riwayat pembayaran"),
	COM_PAKET_RIWAYAT_KONTRAK("comPaketRiwayatKontrak","Lihat kontrak pada paket"),
	COM_PAKET_RIWAYAT_PEMBAYARAN_INPUT("comPaketRiwayatPembayaranInput","Input form pembayaran pada paket"),
	COM_PAKET_RIWAYAT_KONTRAK_INPUT("comPaketRiwayatKontrakInput","Input form kontrak pada paket"),
	COM_PAKET_RIWAYAT_KONTRAK_DELETE("comPaketRiwayatKontrakDelete","Hapus kontrak pada paket"),

	COM_PAKET_RIWAYAT_PENGIRIMAN("comPaketRiwayatPengiriman","Lihat riwayat pengiriman"),
	COM_PAKET_RIWAYAT_PENGIRIMAN_INPUT("comPaketRiwayatPengirimanInput","Input riwayat pengiriman"),
	COM_PAKET_RIWAYAT_PENERIMAAN("comPaketRiwayatPenerimaan","Lihat riwayat penerimaan"),
	COM_PAKET_RIWAYAT_PENERIMAAN_INPUT("comPaketRiwayatPenerimaanInput","Input riwayat penerimaan"),

	COM_PAKET_FEEDBACK_MANAGE("comPaketFeedbackManage","Kelola Feedback Paket"),
	COM_PAKET_FEEDBACK_ADD("comPaketFeedbackAdd","Input Feedback Paket"),

	COM_PAKET_PENERIMAAN_LAMPIRAN_KELOLA("comPaketPenerimaanLampiranKelola","Kelola lampiran penerimaan paket"),
	COM_PAKET_RATING_LIHAT("comPaketRatingLihat","Lihat rating"),
	COM_PAKET_RATING_INPUT("comPaketRatingInput","Input rating"),

	COM_PAKET_BEST_PRICE("comPaketBestPrice","Lihat harga terbaik sebelumnya"),

//	-- END PAKET --

//	-- START SERVICE --

	COM_SERVICE_API_RUP("comServiceApiRup","Akses Service API RUP"),
	COM_SERVICE_API_ADP("comServiceApiAdp","Akses Service API ADP"),
	COM_SERVICE_API_PRODUK("comServiceApiProduk","Akses Service API Produk (Agregator)"),
	COM_SERVICE_MARGIN_HARGA("comServiceMarginHarga","Akses Service Margin Harga"),

//	-- END SERVICE --

//	-- START USULAN --

	COM_USULAN("comUsulan","Akses usulan"),
	COM_USULAN_ADD("comUsulanAdd","Tambah Usulan"),
	COM_USULAN_EDIT("comUsulanEdit","Edit Usulan"),
	COM_USULAN_DELETE("comUsulanDelete","Delete Usulan"),

	COM_USULAN_DOKUMEN_ADD("comUsulanDokumenAdd","Tambah Usulan Dokumen"),
	COM_USULAN_DOKUMEN_EDIT("comUsulanDokumenEdit","Edit Usulan Dokumen"),

	COM_USULAN_FILE_DOKUMEN_KELOLA("comUsulanFileDokumenKelola","Kelola file Dokumen Usulan (Upload, Edit, Delete)"),
	COM_USULAN_FILE_DOKUMEN_DOWNLOAD("comUsulanFileDokumenDownload","Download file dokumen usulan"),

	COM_USULAN_TAHAPAN_KELOLA("comUsulanTahapanKelola","Kelola proses tahapan usulan"),

	COM_USULAN_UBAH_JADWAL("comUsulanUbahJadwal","Ubah Jadwal Usulan"),

	COM_USULAN_AGENDA_KEGIATAN_ADD("comUsulanAgendaKegiatanAdd","Tambah Agenda kegiatan"),
	COM_USULAN_AGENDA_KEGIATAN_EDIT("comUsulanAgendaKegiatEdit","Edit Agenda kegiatan"),

	COM_USULAN_REVIEW("comUsulanReview","Review Usulan"),

//	-- END USULAN --

//	-- START PENAWARAN --

	COM_PENAWARAN("comPenawaran","Akses Penawaran"),
	COM_PENAWARAN_ADD("comPenawaranAdd","Tambah Penawaran"),
	COM_PENAWARAN_EDIT("comPenawaranEdit","Edit Penawaran"),
	COM_PENAWARAN_DELETE("comPenawaranDelete","Delete Penawaran"),

	COM_PENAWARAN_BUKA("comPenawaranBuka", "Buka penawaran"),
	COM_PENAWARAN_DAFTAR("comPenawaranDaftar", "Akses Daftar Penawaran"),

	COM_VERIFIKASI_KUALIFIKASI_PENYEDIA("comVerifikasiKualifikasiPenyedia", "verifikasi kualifikasi penyedia"),

	COM_PENAWARAN_LIST_TEMPLATE_JADWAL("comPenawaranListTemplateJadwal", "List template jadwal"),
	COM_PENAWARAN_TEMPLATE_JADWAL_ADD ("comPenawaranTemplateJadwalAdd", "Add template jadwal penawaran"),
	COM_PENAWARAN_TEMPLATE_JADWAL_EDIT ("comPenawaranTemplateJadwalEdit", "Edit template jadwal penawaran"),
	COM_PENAWARAN_TEMPLATE_JADWAL_DELETE ("comPenawaranTemplateJadwalDelete", "Delete template jadwal penawaran"),

	COM_PENAWARAN_PRODUK_LIST("comPenawaranProdukList", "Produk list penawaran"),
	COM_PENAWARAN_PRODUK_ADD("comPenawaranProdukAdd", "add produk penawaran"),
	COM_PENAWARAN_APPROVAL("comPenawaranApproval", "approval penawaran"),
	COM_PENAWARAN_PERSETUJUAN_PRODUK("comPenawaranPersetujuanProduk","Menyetujui Produk dari Penawaran Penyedia"),

	COM_PENAWARAN_FILE_DOKUMEN_DOWNLOAD("comPenawaranFileDokumenDownload","Download file dokumen penawaran"),
	COM_PENAWARAN_FILE_DOKUMEN_KELOLA("comPenawaranFileDokumenKelola","Kelola file Dokumen Penawaran (Upload, Edit, Delete)"),

	COM_PENAWARAN_TAHAPAN_KELOLA("comPenawaranTahapanKelola","Kelola proses tahapan penawaran"),

	COM_PENAWARAN_CETAK_BA_HASIL_EVALUASI("comPenawaranCetakBaHasilEvaluasi","Mencetak Berita Acara Hasil Evaluasi"),
	COM_PENAWARAN_ADD_MANUFAKTUR_PRODUK("comPenawaranAddManufakturProduk","Mencetak Berita Acara Hasil Evaluasi"),
	COM_PENAWARAN_DELETE_MANUFAKTUR_PRODUK("comPenawaranDeleteManufakturProduk","Mencetak Berita Acara Hasil Evaluasi"),


//	-- END PENAWARAN --

//	-- START CETAK DOKUMEN --

	COM_CETAK_DOKUMEN_BA_NEGOSIASI("comCetakDokumenBaNegosiasi", "Cetak BA negosiasi"),
	COM_CETAK_DOKUMEN_BA_EVALUASI("comCetakDokumenBaEvaluasi", "Cetak BA evaluasi"),
	COM_CETAK_DOKUMEN_SK_PENETAPAN_PRODUK("comCetakDokumenSKPenetapanProduk", "Cetak SK penetapan produk"),

//	-- END CETAK DOKUMEN --

//	-- START POINT RATING --
	COM_MASTER_POINT_RATING_INDEX("comMenuPointRating","List Point RAting"),
	COM_MASTER_POINT_RATING_ADD("comAddPointRating","Tambah Point Rating"),
	COM_MASTER_POINT_RATING_DELETE("comDeletePointRating","Hapus Point Rating"),
	COM_RATING_INPUT("comInputRating","Input Rating"),
	COM_RATING_VIEW("comViewRating","Tampil Rating"),
// END POINT RATING

//	-- START ROLE ITEM --
	COM_MASTER_ROLE_ITEM_INDEX("comMenuRoleItem","List Role Item"),
	COM_MASTER_ROLE_ITEM_ADD("comAddRoleItem","Tambah Role Item"),
	COM_MASTER_ROLE_ITEM_DELETE("comDeleteRoleItem","Delete Role Item"),
// END ROLE ITEM

//	-- OTHERS --
// ---Notifikasi---

	COM_NOTIFIKASI_ADD("comNotifikasiAdd","Tambah Notifikasi"),
	COM_NOTIFIKASI_EDIT("comNotifikasiEdit","Ubah Notifikasi"),

//	---END Notifikasi---
	COM_SET_WILAYAH("comSetWilayah","Set Wilayah User"),

	COM_ADD_ADMIN_LOKAL("comAddAdminLokal","Add admin lokal"),
	COM_USER_INPUT_PBJ("comUserInputPbj","Input Pbj"),
	COM_KELOLA_ADMIN_PENYEDIA_LOKAL("comKelolaAdminPenyediaLokal","Kelola Admin dan Penyedia Lokal"),

//EXEC API
	COM_EXEC_PRODUK_ELASTICS("comExecProdukElastics","Jalankan Elastic Dump Manual"),

//INPUT DATA KONTRAK
	COM_INPUT_DATA_KONTRAK("comInputDataKontrak", "Input data kontrak penawaran"),

//AANWIJZING
    COM_AANWIJZING("comAanwijzing", "Halaman aanwijzing"),
    COM_INPUT_QUESTION_AANWIJZING("comInputQuestionAanwijzing", "Input pertanyaan halaman aanwijzing"),
    COM_EDIT_QUESTION_AANWIJZING("comEditQuestionAanwijzing", "Edit pertanyaan halaman aanwijzing"),
    COM_DELETE_QUESTION_AANWIJZING("comDeleteQuestionAanwijzing", "Delete pertanyaan halaman aanwijzing"),
    COM_AANWIJZING_EXPORT_PDF("comAanwijzingExportPdf", "Eksport PDF pertanyaan dan response aanwijzing"),
    COM_AANWIJZING_DETAIL("comAanwijzingDetail", "Halaman detail aanwijzing"),
    COM_INPUT_COMMENT_AANWIJZING_DETAIL("comInputCommentAanwijzingDetail", "Input komentar halaman aanwijzing detail"),
    COM_EDIT_COMMENT_AANWIJZING_DETAIL("comEditCommentAanwijzingDetail", "Edit komentar halaman aanwijzing"),
    COM_DELETE_COMMENT_AANWIJZING_DETAIL("comDeleteCommentAanwijzingDetail", "Delete pertanyaan halaman aanwijzing"),
//	-- OTHERS --


	/**Ini mewakili semua user yang berhasil login.
	 * Beberapa halaman boleh diakses oleh semua user yang login, misalnya LogAccess */
	ALL_AUTHORIZED_USERS("public", "All registered users");

	public final String namaRole;
	public final String deskripsi;

	private Acl(String namaRole, String deskripsi){
		this.namaRole = namaRole;
		this.deskripsi = deskripsi;
	}
}
