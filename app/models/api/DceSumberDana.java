package models.api;

import models.jcommon.util.CommonUtil;
import models.masterdata.SumberDana;
import play.Logger;
import utils.KatalogUtils;

import java.util.Date;
import java.util.List;

/**
 * Created by reza on 27/08/18
 */
public class DceSumberDana {
	public Long id;
	public String nama;


	public SumberDana getAsSumberDana(){
		try{
			SumberDana sumberDana = new SumberDana();
			sumberDana.id=this.id;
			sumberDana.nama_sumber_dana=this.nama;
			
			sumberDana.active=true;
			return sumberDana;
		}catch (Exception e){
			Logger.error("error parse DceSumberDana to sumberDana"+ e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

}
