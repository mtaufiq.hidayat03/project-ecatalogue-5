package models.katalog;

import models.BaseKatalogTable;
import models.masterdata.Kabupaten;
import models.masterdata.Provinsi;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by dadang on 8/27/17.
 */

@Table(name = "produk_wilayah_jual_kabupaten")
public class ProdukWilayahJualKabupaten extends BaseKatalogTable {

	@Id
	public Long id;
	public Long kabupaten_id;
	public Long produk_id;
	public BigDecimal harga_utama;
	public BigDecimal harga_ongkir;
	public Date harga_tanggal;
	public Long harga_kurs_id;
	public Double margin_harga;
	public boolean active;
	public boolean approved;
	@Transient
	public Double harga_pemerintah;
	@Transient
	public String nama_lokasi;
	@Transient
	public String nama_produk;
	@Transient
	public String nama_manufaktur;
	@Transient
	public String nama_unit_pengukuran;
	@Transient
	public String nama_kabupaten;
	@Transient
	public String nama_provinsi;

	public static int deleteByProdukId(long produk_id){
		String query = "delete from produk_wilayah_jual_kabupaten where produk_id = ?";

		return Query.update(query, produk_id);
	}

	public static List<Kabupaten> findWilayahJual(Long produk_id){
		String query = "select k.* from produk_wilayah_jual_kabupaten pwj join kabupaten k on k.id = pwj.kabupaten_id " +
				"where pwj.produk_id = ?";

		return Query.find(query, Kabupaten.class, produk_id).fetch();
	}

	public static ProdukWilayahJualKabupaten getHargaByKabupatenId(Long produk_id, Long kabupaten_id){

		return find("produk_id = ? and kabupaten_id = ? and active = 1", produk_id, kabupaten_id).first();
	}

	public static ProdukWilayahJualKabupaten getHargaByKabupatenIdNonApproved(Long produk_id, Long kabupaten_id){

		return find("produk_id = ? AND kabupaten_id = ? AND active = 1", produk_id, kabupaten_id).first();
	}

	public double getMainPrice() {
		if (this.harga_utama != null) {
			return this.harga_utama.doubleValue();
		}
		return 0;
	}

	public static List<ProdukWilayahJualKabupaten> findProdukByPenawaran(long penawaran_id){
		String query = "select " +
				"p.id" +
				",p.komoditas_id" +
				",k.nama_komoditas" +
				",p.produk_kategori_id" +
				",pk.nama_kategori" +
				",p.penyedia_id" +
				",py.nama_penyedia" +
				",p.manufaktur_id" +
				",m.nama_manufaktur" +
				",p.unspsc_id" +
				",p.unit_pengukuran_id" +
				",up.nama_unit_pengukuran" +
				",p.no_produk" +
				",p.no_produk_penyedia" +
				",p.nama_produk" +
				",p.jenis_produk" +
				",p.setuju_tolak" +
				",p.setuju_tolak_tanggal" +
				",p.setuju_tolak_oleh" +
				",p.setuju_tolak_alasan" +
				",pwjk.kabupaten_id" +
				",prv.nama_provinsi" +
				",kab.nama_kabupaten" +
				",pwjk.harga_utama" +
				",pwjk.harga_ongkir" +
				",pwjk.harga_tanggal" +
				",pwjk.harga_kurs_id" +
				",pwjk.margin_harga" +
				",p.berlaku_sampai" +
				",p.kontrak_id" +
				",p.ongkir " +
				"from produk_wilayah_jual_kabupaten pwjk " +
				"left join produk p on pwjk.produk_id = p.id " +
				"left join kabupaten kab on pwjk.kabupaten_id = kab.id " +
				"left join provinsi prv ON prv.id = kab.provinsi_id " +
				"left join penyedia py on p.penyedia_id = py.id " +
				"left join komoditas k on p.komoditas_id = k.id " +
				"left join produk_kategori pk on p.produk_kategori_id = pk.id " +
				"left join manufaktur m on p.manufaktur_id = m.id " +
				"left join unit_pengukuran up on p.unit_pengukuran_id = up.id " +
				"where pwjk.active = " + AKTIF + " and p.active = " + AKTIF + " and p.status in ('" + Produk.STATUS_TERIMA + "','" + Produk.STATUS_TAYANG + "') " +
				"and penawaran_id = ?";

		return Query.find(query, ProdukWilayahJualKabupaten.class, penawaran_id).fetch();
	}

}
