package models.cms;

import ext.DateBinder;
import models.BaseKatalogTable;
import play.data.binding.As;
import play.db.jdbc.Id;

import play.db.jdbc.Table;

import java.util.Date;
import java.util.List;


@Table(name = "banner")
public class Banner extends BaseKatalogTable {

    @Id
    public Long id;

    public String subjek;

    @As(binder = DateBinder.class)
    public Date tanggal_mulai_publikasi;
    @As(binder = DateBinder.class)
    public Date tanggal_akhir_publikasi;

    public String file_name;
    public long file_size;
    public String file_sub_location;
    public String original_file_name;
    public Boolean active;

    public static List<Banner> findAllBannerActive(){
        return Banner.find("active = 1").fetch();
    }

    public void setAktif() {
        this.active = true;
        prePersist();
        save();
    }

    public void setNonAktif() {
        this.active = false;
        prePersist();
        save();
    }

}
