INSERT INTO role_grup (nama_grup, kategori, active, created_date) VALUES
("Dokumen Template", "cms", 1, CURDATE());

INSERT INTO role_item (role_grup_id, nama_role, deskripsi, active, created_date) VALUES
  ((SELECT id FROM role_grup WHERE nama_grup = "Dokumen Template"), "comDocumentTemplate", "Akses Template Dokumen", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Dokumen Template"), "comDocumentTemplateAdd", "Akses Tambah Template Dokumen", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Dokumen Template"), "comDocumentTemplateEdit", "Akses Edit Template Dokumen", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Dokumen Template"), "comDocumentTemplateDelete", "Akses Hapus Template Dokumen", 1, CURDATE());
