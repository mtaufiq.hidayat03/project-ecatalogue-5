package models.permohonan;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import models.BaseKatalogTable;
import models.katalog.ProdukAtributValue;
import models.penyedia.Penyedia;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Table(name = "permohonan_pembaruan_staging")
public class PembaruanStaging extends BaseKatalogTable {
    @Id
    public Long id;
    public Long permohonan_pembaruan_id;
    public String class_name;
    // 0 = menunggu persetujuan
    // 1 = disetujui
    // -1 ditolak
    public int apakah_disetujui = 0;
    public Timestamp tgl_disetujui;
    public boolean isList;
    public String object_json;
    public boolean active;

    @Transient
    public  Long produk_id;

    public PermohonanPembaruan getPermohonanPembaruan() {
        return PermohonanPembaruan.findById(permohonan_pembaruan_id);
    }

    public static List<PembaruanStaging> getListByPermohonanId(Long id) {// ini maksute adalah get by permohonan_id
        return find("permohonan_pembaruan_id = ? and active=1", id).fetch();
    }
    public static PembaruanStaging getById(Long id) {// ini maksute adalah get by permohonan_id
        return find("permohonan_pembaruan_id = ? and active=1", id).first();
    }

    public static List<PembaruanStaging> getProdukStaggingByPmbIdAndPrdId(Long pmb, Long prd_id) {
        String query = "SELECT pps.* FROM permohonan_pembaruan_staging pps WHERE pps.permohonan_pembaruan_id = " + pmb + " AND\n" +
                "pps.class_name = 'models.permohonan.ProdukStagging' AND JSON_EXTRACT(pps.object_json, \"$.produk_id\") = " + prd_id;
        return Query.find(query, PembaruanStaging.class).fetch();
    }

    public static List<PembaruanStaging> getDistributorStaggingByPmbIdAndDistId(Long pmb, Long distId) {
        String query = "SELECT pps.* FROM permohonan_pembaruan_staging pps WHERE pps.permohonan_pembaruan_id = " + pmb + " AND\n" +
                "pps.class_name = 'models.permohonan.DistributorStagging' AND JSON_EXTRACT(pps.object_json, \"$.distributor.id\") = " + distId;
        return Query.find(query, PembaruanStaging.class).fetch();
    }

    public static List<PembaruanStaging> getByPermohonanPembaruanId(Long pmb) {
        return find("permohonan_pembaruan_id = ? and active=1", pmb).fetch();
    }

    public static List<PembaruanStaging> getByPermohonanPembaruanIdApproved(Long pmb, int approved) {
        return find("permohonan_pembaruan_id = ? and active=1 AND apakah_disetujui=?", pmb, approved).fetch();
    }

    public static List<PembaruanStaging> getProdukDistinct(Long perm_id){
        String query = "SELECT p.* FROM permohonan_pembaruan_staging p WHERE p.permohonan_pembaruan_id = ? GROUP BY JSON_EXTRACT(object_json, '$.produk_id')";
        return Query.find(query,PembaruanStaging.class,perm_id).fetch();
    }

    public static PembaruanStaging getIfProdByPermohonanPembaruanId_PENYEDIA(Long pmb) {
        return find("permohonan_pembaruan_id = ? AND JSON_EXTRACT(object_json, '$.active') = true AND active=1 AND apakah_disetujui = 1", pmb).first();
    }

    public static List<PembaruanStaging> getIfProdByPermohonanPembaruanId_INFO(Long pmb) {
        return find("permohonan_pembaruan_id = ? AND JSON_EXTRACT(object_json, '$.info') = true AND active=1 AND apakah_disetujui = 1", pmb).fetch();
    }

    public static List<PembaruanStaging> getIfProdByPermohonanPembaruanId_NOTINFO(Long pmb) {
        return find("permohonan_pembaruan_id = ? AND JSON_EXTRACT(object_json, '$.info') = false AND active=1 AND apakah_disetujui = 1", pmb).fetch();
    }

    public static List<PembaruanStaging> getIdProdukByPermohonanId(Long pmb) {
        String sql = "SELECT pps.*, JSON_EXTRACT(pps.object_json,'$.produk_id') as produk_id " +
                "FROM permohonan_pembaruan_staging pps WHERE pps.permohonan_pembaruan_id = ? and pps.active = 1 and pps.apakah_disetujui = 1";
        return Query.find(sql, PembaruanStaging.class, pmb).fetch();
    }

    public static List<PembaruanStaging> getDistributorApprovedByPermohonanPembaruanId(Long pmb) {
        //return find("permohonan_pembaruan_id = ? and active=1 and class_name LIKE '%Distributor%'", pmb).fetch();
        String sql = "SELECT stg.* FROM permohonan_pembaruan_staging stg \n" +
                "JOIN permohonan_pembaruan pmb ON pmb.id = stg.permohonan_pembaruan_id \n" +
                "AND pmb.tipe_permohonan = 'pembaruan_distributor' AND stg.active =1 AND pmb.active =1 \n" +
                "WHERE pmb.id = ? AND stg.apakah_disetujui = 1";
        return Query.find(sql, PembaruanStaging.class, pmb).fetch();
    }

    public static List<PembaruanStaging> getAllDistributorByPermohonanPembaruanId(Long pmb) {
        //return find("permohonan_pembaruan_id = ? and active=1 and class_name LIKE '%Distributor%'", pmb).fetch();
        String sql = "SELECT stg.* FROM permohonan_pembaruan_staging stg \n" +
                "JOIN permohonan_pembaruan pmb ON pmb.id = stg.permohonan_pembaruan_id \n" +
                "AND pmb.tipe_permohonan = 'pembaruan_distributor' AND stg.active =1 AND pmb.active =1 \n" +
                "WHERE pmb.id = ?";
        return Query.find(sql, PembaruanStaging.class, pmb).fetch();
    }

    public static void deleteById(long id) {
        Query.update("DELETE from permohonan_pembaruan_staging where id = ?", id);
    }

    public static int deleteStagingByPermId(long permId) {
        String query = "update permohonan_pembaruan_staging set active = 0 where permohonan_pembaruan_id = ?";
        return Query.update(query, permId);
    }

    public static int cancelApproveStagingByPermId(long permId) {
        String query = "update permohonan_pembaruan_staging set apakah_disetujui = 0 , tgl_disetujui = null where permohonan_pembaruan_id = ?";
        return Query.update(query, permId);
    }

    public String getProdukIdIfTypeProduk() {
        PermohonanPembaruan pmb = PermohonanPembaruan.findById(permohonan_pembaruan_id);
        if (pmb.tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_PRODUK)) {
            ProdukStagging pstg = new Gson().fromJson(object_json, ProdukStagging.class);
            return pstg.produk_id.toString();
        }
        return "";
    }

    public String getDistributorIdIfTypeDistributor() {
        PermohonanPembaruan pmb = PermohonanPembaruan.findById(permohonan_pembaruan_id);
//        List<PembaruanStaging> pmst = PembaruanStaging.getDistributorByPermohonanPembaruanId(permohonan_pembaruan_id);

//        dstg = DistributorStagging.findByPermohonanPembaruanId(pmb.id);

//        for(PembaruanStaging p: pmst){
        if (pmb.tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_DISTRIBUTOR)) {
            DistributorStagging dstg = new Gson().fromJson(object_json, DistributorStagging.class);

            return dstg.distributor.id.toString();
        }
//        }

        return "";
    }

    public Penyedia getPenyediaIfPenyedia() {
        Penyedia penyedia = null;
        Gson gson = new Gson();
        if (Penyedia.class.getCanonicalName().equalsIgnoreCase(class_name)) {
            try {
                Type type = (Type) Class.forName(class_name);
                penyedia = gson.fromJson(object_json, type);
            } catch (ClassNotFoundException e) {
                Logger.error("Tidak bisa parsing data penyedia, dari json:" + e.getMessage());
            }
        }
        return penyedia;
    }

//    public int savePenyediaToJson(Penyedia penyedia){
//        this.object_json = new Gson().toJson(penyedia);
//        return save();
//    }

    private Object getObject() {
        Object o = new Object();
        Gson gson = new Gson();
        try {
            Type type = (Type) Class.forName(class_name);
            o = gson.fromJson(object_json, type);
        } catch (ClassNotFoundException e) {
            Logger.error("Tidak bisa parsing data Object, dari json:" + e.getMessage());
        }
        return o;
    }

    public ProdukStagging getIfProdukStagging() {
        return (ProdukStagging) this.getObject();
    }

    public DistributorStagging getIfDistributorStagging() {
        return (DistributorStagging) this.getObject();
    }

    private List<Object> getListObject() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            java.util.List<Object> pp3 = mapper.readValue(this.object_json, new TypeReference<ArrayList>() {
            });
            return pp3;
        } catch (Exception e) {
            Logger.info("getListObject error:" + e.getMessage());
        }
        return new ArrayList<>();
    }

    public List<ProdukAtributValue> getListProdukAtributValue() {
        return (List<ProdukAtributValue>) (Object) this.getListObject();

    }

//    private List<Object> getListObject(){
//        ObjectMapper mapper = new ObjectMapper();
//        try {
//            java.util.List<Object> pp3 = mapper.readValue(this.object_json, new TypeReference<ArrayList>() {});
//            return pp3;
//        }catch (Exception e){
//            Logger.info("getListObject error:"+e.getMessage());
//        }
//        return new ArrayList<>();
//    }
//    public List<ProdukAtributValue> getListProdukAtributValue(){
//        return  (List<ProdukAtributValue>) (Object) this.getListObject();
//    }

//    public void getListPembProdukIfTipeProduk(Long pmb, Long produk_id){
//       List<PembaruanStaging> list =  find("permohonan_pembaruan_id=? and active=1").fetch();
//        list = list.stream().filter(f-> getIfProdukStagging().produk_id.equals(produk_id)).collect(Collectors.toList());
//        for ( PembaruanStaging p : list) {
//            Logger.info(p.getIfProdukStagging().produk_id.toString()+" "+ p.getIfProdukStagging().getType());
//        }
//    }

    public void getListPembProdukIfTipeProduk(Long produk_id) {
        List<PembaruanStaging> list = find("permohonan_pembaruan_id=? and active=1", permohonan_pembaruan_id).fetch();
        list = list.stream().filter(f -> getIfProdukStagging().produk_id.equals(produk_id)).collect(Collectors.toList());
        for (PembaruanStaging p : list) {
            Logger.info(p.getIfProdukStagging().produk_id.toString() + " " + p.getIfProdukStagging().getType());
        }
    }

    public static List<PembaruanStaging> getIfDistributorStagingByIdProduk(long[] idsDist, Long idPembaruan) {
        List<PembaruanStaging> result = new ArrayList<>();
        if (null != idsDist) {
            List<Long> lstLong = Arrays.asList(ArrayUtils.toObject(idsDist));
            String ids = StringUtils.join(lstLong.toArray(), ",");
            String query = "SELECT stg.* FROM permohonan_pembaruan_staging stg\n" +
                    "JOIN permohonan_pembaruan pmb on pmb.id = stg.permohonan_pembaruan_id and pmb.tipe_permohonan = 'pembaruan_distributor'\n" +
                    "WHERE JSON_EXTRACT(object_json, '$.distributor.id') in (" + ids + ") AND pmb.id=?";
            result = Query.find(query, PembaruanStaging.class, idPembaruan).fetch();
        }
        return result;
    }

    public static List<PembaruanStaging> getIfProdukStagingByIdProduk(long[] idproduk, Long pmbid) {
        List<PembaruanStaging> result = new ArrayList<>();
        if (null != idproduk) {
            List<Long> lstLong = Arrays.asList(ArrayUtils.toObject(idproduk));
            String ids = StringUtils.join(lstLong.toArray(), ",");
            String query = "SELECT stg.* FROM permohonan_pembaruan_staging stg\n" +
                    "JOIN permohonan_pembaruan pmb on pmb.id = stg.permohonan_pembaruan_id and pmb.tipe_permohonan = 'pembaruan_produk'\n" +
                    "WHERE JSON_EXTRACT(object_json, '$.produk_id') in (" + ids + ") AND pmb.id=?";
            result = Query.find(query, PembaruanStaging.class, pmbid).fetch();
        }
        return result;
    }

    public static PembaruanStaging getIfProdukStagingINFOByIdProduk(long idproduk, Long pmbid) {
        String query = "SELECT stg.* FROM permohonan_pembaruan_staging stg\n" +
                "JOIN permohonan_pembaruan pmb on pmb.id = stg.permohonan_pembaruan_id and pmb.tipe_permohonan = 'pembaruan_produk'\n" +
                "WHERE JSON_EXTRACT(object_json, '$.produk_id') =? AND JSON_EXTRACT(object_json, '$.info') = true AND pmb.id=? ";
        return Query.find(query, PembaruanStaging.class, idproduk, pmbid).first();
    }

    public static int UpdateHargaTglIfStagHArga(Long id, Date tgl){
        PembaruanStaging pmb = PembaruanStaging.findById(id);
        ProdukStagging psg = pmb.getIfProdukStagging();
        ProdukHargaStagging phsg = psg.getIfProdukHargaStagging();
        phsg.harga_tanggal = tgl;
        psg.object_json = psg.createGson().toJson(phsg);
        pmb.object_json = new Gson().toJson(psg);
        int res = pmb.save();
        return res;
    }

}
