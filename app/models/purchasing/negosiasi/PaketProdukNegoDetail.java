package models.purchasing.negosiasi;

import ext.FormatUtils;
import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by dadang on 11/9/17.
 */
@Table(name = "paket_produk_nego_detail")
public class PaketProdukNegoDetail extends BaseKatalogTable {

	@Id
	public Long id;
	public Long nego_header_id;
	public Long paket_produk_id;
	public Long produk_id;
	public BigDecimal kuantitas;
	public Double harga_satuan;
	public Double harga_ongkir;
	public Double harga_total;
	public Double konversi_harga_satuan = new Double(0);
	public Double konversi_harga_ongkir = new Double(0);
	public Double konversi_harga_total = new Double(0);
	public String catatan;
	public boolean active = true;
	public Date batas_pengiriman;

	public static PaketProdukNegoDetail getByHeaderId(Long id) {
		return find("nego_header_id =? AND active > 0", id).first();
	}

	public static void deleteByNegoHeaderId(long nego_header_id){
		Query.update("DELETE from paket_produk_nego_detail where nego_header_id = ?", nego_header_id);
	}

	public static void inactiveByPaketProdukId(long paket_produk_id){
		Query.update("UPDATE paket_produk_nego_detail set active = 0 where paket_produk_id = ?", paket_produk_id);
	}

	public String getFormattedTotalPrice() {
		return FormatUtils.formatCurrencyRupiah(harga_total);
	}

	public String getFormattedConversionTotalPrice() {
		return FormatUtils.formatCurrencyRupiah(konversi_harga_total);
	}

	public static Double getHargaAwal(long paketProdukId){
        final String query = "SELECT pnd.harga_satuan " +
				"FROM paket_produk_nego_detail pnd " +
				"LEFT JOIN paket_produk_nego_header pnh on pnh.id = pnd.nego_header_id " +
				"LEFT JOIN paket p on p.id = pnh.paket_id " +
				"WHERE pnd.paket_produk_id = ? AND pnh.revisi = 0";

        PaketProdukNegoDetail pnd = Query.find(query, PaketProdukNegoDetail.class, paketProdukId).first();
        return  pnd.harga_satuan;
	}

	public static PaketProdukNegoDetail getPaketProdukId(Long paketProdukId) {
		final String query = "SELECT * FROM paket_produk_nego_detail WHERE paket_produk_id = ?";
		PaketProdukNegoDetail pnd = Query.find(query, PaketProdukNegoDetail.class, paketProdukId).first();
		return  pnd;
	}
	public static void updatePaketProdukId(long paketProdukIdBaru, long paketProdukIdLama){
		Query.update("UPDATE paket_produk_nego_detail set paket_produk_id = ? where paket_produk_id = ?", paketProdukIdBaru, paketProdukIdLama);
	}

	public static void updateDataPaketProdukId(
			Long paketProdukIdBaru, BigDecimal kuantitas, Double harga_ongkir, Double harga_total,
			Double konversi_harga_satuan,  Double konversi_harga_ongkir, Double konversi_harga_total,
			Long paketProdukIdLama
	){
		Query.update("UPDATE paket_produk_nego_detail set paket_produk_id = ?, kuantitas = ?, harga_ongkir = ?," +
						" harga_total = ?, konversi_harga_satuan = ? , konversi_harga_ongkir = ?," +
						" konversi_harga_total = ? where paket_produk_id = ?",
				paketProdukIdBaru, kuantitas, harga_ongkir, harga_total, konversi_harga_satuan, konversi_harga_ongkir,
				konversi_harga_total, paketProdukIdLama);
	}

}
