package jobs.blacklist;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import jobs.JobTrail.JobBlackList1;
import jobs.TrackableJob;
import models.api.DceKab;
import models.api.DcePelanggaran;
import models.api.DceBlacklist;
import models.api.DceBlacklistContainer;
import models.jcommon.config.Configuration;
import models.jcommon.http.SimpleHttpRequest;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.blacklist.Blacklist;
import models.blacklist.Pelanggaran;
import models.util.Config;
import play.Logger;
import play.jobs.Job;
import play.libs.URLs;
import utils.KatalogUtils;

public class ExtBlacklistData {
	private Properties propJob;
	public static final String DEFAULT_WAIT_TIME = "job.sirup.waittime";
	public static final String DEFAULT_WAIT_TIME_NEXT = "job.sirup.waittime.next";
	private String jsResult = "[]";
	private Boolean resultGet = false;
	public List<DceBlacklist> blacklistList = new ArrayList<>();
	private int defaultWaitTime = 0;
	private String blacklistUrl = Config.getInstance().blacklist_api;
	private int nextWaitTime = 0;
	private int delayTaskJob = 5;// second
	private int delayJob = 10;// second
	//
	public ExtBlacklistData() {
		propJob = new Properties();
		propJob.put(DEFAULT_WAIT_TIME,
				Configuration.getConfigurationValue(DEFAULT_WAIT_TIME, ""));
		propJob.put(DEFAULT_WAIT_TIME_NEXT, Configuration
				.getConfigurationValue(DEFAULT_WAIT_TIME_NEXT, ""));

		defaultWaitTime = Integer
				.parseInt(propJob.getProperty(DEFAULT_WAIT_TIME));
		nextWaitTime = Integer
				.parseInt(propJob.getProperty(DEFAULT_WAIT_TIME_NEXT));

	}

	public List<DceBlacklist> getBlacklistData() {
		return getBlacklistData(defaultWaitTime);
	}
	public List<DceBlacklist> getBlacklistData(int minuteTimeOut) {
		if (fetchBlacklistData(minuteTimeOut)) {
			this.blacklistList = parseBlacklistData();
		}
		return this.blacklistList;
	}

	private Boolean fetchBlacklistData(Integer minuteTimeout) {
		resultGet = false;
		jsResult = "[]";
		Date tgl_update = new Date();
		String date = KatalogUtils.toDateyyyyMMdd(tgl_update);
		Integer seconds = 0;
		if (minuteTimeout > 0) {
			seconds = (minuteTimeout * 60);
		}
		Logger.info("run with minuteTimeout: " + minuteTimeout + ", and date "
				+ tgl_update.toLocaleString());
		try (SimpleHttpRequest connect = new SimpleHttpRequest(seconds)) {
			String params = "";
			connect.get(blacklistUrl + params);
			jsResult = connect.getString();
			resultGet = true;
		} catch (InterruptedException e) {
			Logger.error("Interupted Exception:" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Logger.error("IO Exception:" + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		if (resultGet == false) {
			defaultWaitTime++;
			nextWaitTime = defaultWaitTime;
			// Configuration.updateConfigurationValue(DEFAULT_WAIT_TIME_NEXT,
			// String.valueOf(newWait));
			// propJob.replace(DEFAULT_WAIT_TIME_NEXT, String.valueOf(newWait));

			// newJob(tgl_update);
		}
		return resultGet;
	}

	private List<DceBlacklist> parseBlacklistData() {
		try {
			DceBlacklistContainer c = new Gson().fromJson(jsResult,
					new TypeToken<DceBlacklistContainer>() {
					}.getType());
			blacklistList = c.data;
			Logger.info("total:" + blacklistList.size());
		} catch (Exception e) {
			Logger.error("error parse data blacklist:" + e.getMessage());
			e.printStackTrace();
		}
		return blacklistList;
	}

	public void saveDataBlacklistToDb() {
		List<Blacklist> failed = new ArrayList<>();
		List<Blacklist> success = new ArrayList<>();
		Logger.info("save data blacklist:");
		final utils.BlacklistHandler blacklistHandler = new utils.BlacklistHandler();
		for (DceBlacklist s : blacklistList) {
			Blacklist obj = Blacklist.find(" blacklist_source_id=?",s.id).first();
			try {
				if (obj != null) {
					Long id = obj.id;
					obj = s.getAsBlacklist();
					obj.id = id;
					int result = obj.save();
					handleNewBlacklist(obj);
					Logger.info("update:" + result);
				} else {
					obj = s.getAsBlacklist();
					int result = obj.save();
					obj.id = new Long(result);
					blacklistHandler.turunkanProdukBlacklistDariDaftarTayang(obj);
					Logger.info("save:" + result);
				}
				if (obj != null) {
					success.add(obj);
					savePelanggaran(obj.id, s.pelanggaran);
				}
			} catch (Exception e) {
				failed.add(obj);
				Logger.info("error save or update blacklist:" + e.getMessage());
				e.printStackTrace();
			}
		}

	}

	private void handleNewBlacklist(Blacklist obj) {
		if(obj==null)
			return;
		final utils.BlacklistHandler blacklistHandler = new utils.BlacklistHandler();
		blacklistHandler.turunkanProdukBlacklistDariDaftarTayang(obj);
		blacklistHandler.laporkanKeAdmin(obj);

	}

	private void savePelanggaran(Long blacklistId, List<DcePelanggaran> pelanggaran) {
		for(DcePelanggaran p:pelanggaran) {
			Logger.info("DcePelanggaran sk = %s", p.sk);
			Pelanggaran p1 = null;
			try{
				p1 = Pelanggaran.find("blacklist_id = ? and sk = ?", blacklistId, p.sk).first();
			}catch (Exception e){

			}
			if(p1==null) {
				p1=p.getAsPelanggaran();
				p1.blacklist_id=blacklistId;
				p1.save();
			}
			else {
				Long id = p1.id;
				p1=p.getAsPelanggaran();
				p1.id=id;
				p1.blacklist_id=blacklistId;
				p1.save();
			}
		}
	}

	private void newJob() {
		new JobBlackList1(this,nextWaitTime).in(delayJob);

//		new Job() {
//			String uniqueID = UUID.randomUUID().toString();
//			@Override
//			public void before() {
//				Logger.info("job id:" + uniqueID);
//				super.before();
//			}
//
//			public void doJob() {
//				Logger.info("on job execute, waittime:" + nextWaitTime);
//				getBlacklistData(nextWaitTime);
//				Logger.info("%%%%done getDate, saving now%%%");
//				saveDataBlacklistToDb();
//				Logger.info("done job:" + blacklistList.size());
//			}
//		}.in(delayJob);
	}

	public void run() {
		newJob();
	}
}
