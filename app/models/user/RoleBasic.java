package models.user;

import java.util.List;

import models.common.AktifUser;
import play.db.jdbc.BaseTable;
import play.db.jdbc.CacheMerdeka;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.management.relation.Role;

@Table(name="role_basic")
public class RoleBasic extends BaseTable {

	@Id
	public Long id;
	public String nama_role;
	public String nama_role_alias;
	public boolean active;

//  konversi dari spse ke katalog
//	panitia -> pokja
//	pp -> Pejabat Pengadaan
//	ppk -> ppk
//	rekanan -> penyedia

	public static String convRoleSpse(String rolespse){
		if("PANITIA".equalsIgnoreCase(rolespse)){
			return RoleBasic.POKJA_NAME;
		}else if("PP".equalsIgnoreCase(rolespse)){
			return RoleBasic.PP_NAME;
		}else if("PPK".equalsIgnoreCase(rolespse)){
			return RoleBasic.PPK_NAME;
		}else{
			return RoleBasic.PENYEDIA_NAME;
		}
	}
	public static final String ADMIN_NAME  = "Admin";
	public static final String ADMIN_LOKAL_SEKTORAL_NAME  = "Admin Lokal Sektoral";
	public static final String POKJA_NAME  = "Pokja";
	public static final String PPK_NAME  = "PPK";
	public static final String PANITIA_NAME  = "Panitia";
	public static final String PENYEDIA_NAME  = "Penyedia";
	public static final String CMS_NAME  = "CMS";
	public static final String DISTRIBUTOR_NAME  = "Distributor";
	public static final String PP_NAME  = "Pejabat Pengadaan";
	public static final String AUDITOR_NAME  = "Auditor";
	public static final String PENYEDIA_DISTRIBUTOR_NAME= "Penyedia Distributor";

	/*
		-berikut adalah role basic yang digunakan secara tetap dalam aplikasi, dan pastinya berhubungan dengan
		 status apakah penyedia_dan_distributor pada tabel user,
		- role basic penyedia_distributor pada role basic belum pernah di gunakan pada user real, baru ada pt test,
		setelah dicek pada tabel user;
		-  status user penyedia_dan_distributor juga banyak yang belum menentukan statusnya pada tabel user
	 */
	public static final Long PENYEDIA = 4L;
	public static final Long DISTRIBUTOR = 6L;
	public static final Long PENYEDIA_DISTRIBUTOR = 10L;

	@CacheMerdeka
	public static List<RoleBasic> findAllActive(){
		String query = "Select id, nama_role, nama_role_alias from role_basic where active = 1";
		
		return Query.find(query, RoleBasic.class).fetch();
	}

	public static RoleBasic findByName(String nama_role){
		nama_role = nama_role.replace("_", " ");

		return find("nama_role=? limit 1", nama_role).first();
	}

	public static boolean checkRoleId(AktifUser aktifUser, String roleName){
		RoleBasic role = findByName(roleName);
		return aktifUser.role_basic_id.equals(role.id);
	}
}
