package repositories.elasticsearch;

import com.google.gson.GsonBuilder;
import jobs.JobTrail.JobPopularProduk;
import models.common.AktifUser;
import models.elasticsearch.SearchQuery;
import models.elasticsearch.SearchResult;
import models.elasticsearch.query.*;
import models.elasticsearch.query.aggregation.InnerTermAggregation;
import models.elasticsearch.query.aggregation.TermAggregation;
import models.elasticsearch.query.aggregation.TopHitAggregation;
import models.elasticsearch.request.RequestCount;
import models.elasticsearch.request.RequestSearch;
import models.elasticsearch.response.MetaProduct;
import models.elasticsearch.response.ResponseCount;
import models.elasticsearch.response.ResponseInputElastic;
import models.elasticsearch.response.ResponseProduct;
import models.katalog.Produk;
import models.util.Config;
import okhttp3.RequestBody;
import play.Logger;
import play.cache.Cache;
import retrofit2.Call;
import retrofit2.Response;
import services.elasticsearch.ElasticsearchConnection;
import utils.LogUtil;

import java.util.ArrayList;
import java.util.List;

import static constants.models.ElasticProductConstant.*;

//tidak digunakan lagi, atas saran spse
//import models.jcommon.cache.JcommonEhCache;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class ElasticsearchRepository {
    public static String modelUrl = "/produk";
    public static String searchlUrl = "/_search";
    public static String countlUrl = "/_count";
    public static String elasticurls = Config.getInstance().getElasticurl();
    public static final String TAG = "ElasticRepository";
    private static final int TOTAL_PER_PAGE = 20;
    private static final String[] FIELDS = new String[]{
            PRODUK_ID, NAMA_PRODUK, NAMA_PENYEDIA, NAMA_MANUFAKTUR,
            STATUS, NAMA_KOMODITAS, CATEGORIES, PRICE_LIST, IMAGE_URL,
            JENIS_PRODUK, ID_PENYEDIA, IS_UMKM};
    public static final String POPULAR_CACHE_KEY = "popular_cache";
    public static final String RECOMMENDATION_CACHE_KEY = "recommendation_cache_";

    public ElasticsearchRepository(){
        //Cache.cacheImpl = JcommonEhCache.getInstance();
    }
    public static ElasticsearchRepository open() {
        return new ElasticsearchRepository();
    }

    public SearchResult searchProduct(SearchQuery model) {
        RequestSearch request = generateSearchQuery(model);
        SearchResult searchResult = new SearchResult(model);
        if (isAllowToDoQuery(model, request)) {
            try {
                //Logger.debug(new GsonBuilder().setPrettyPrinting().create().toJson(request));
                Call<ResponseProduct> call = ElasticsearchConnection.open().getProduct(request, elasticurls+modelUrl+searchlUrl);
//                call.enqueue(new Callback<ResponseProduct>(){
//                    @Override
//                    public void onResponse(Call<ResponseProduct> call, Response<ResponseProduct> response) {
//                        ResponseBody body =  response.raw().body();
//                    }
//                    @Override
//                    public void onFailure(Call<ResponseProduct> call, Throwable throwable) {
//                        Logger.info("error");
//                    }
//                });
                Response<ResponseProduct> responseCall = call.execute();
                if (ElasticsearchConnection.isSucceed(responseCall.code())) {
                    ResponseProduct response = responseCall.body();
                    if (response != null) {
                        searchResult = response.getPopularResult();
                        searchResult.paramAble = model;
                    }
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }finally {

            }
            if (!searchResult.products.isEmpty()) {
                List<MetaProduct> mtp = new ArrayList<>();
                try{
                    mtp = getRecommendation(model);
                }catch (Exception e){
                    Logger.error("error get recommendation:"+e.getMessage());
                }
                searchResult.recommendations.addAll(mtp);
            }
        }
        return searchResult;
    }

    private boolean isAllowToDoQuery(SearchQuery model, RequestSearch requestSearch) {
        LogUtil.d(TAG, model);
        int total = getTotalDataByQuery(requestSearch).count;
        LogUtil.d(TAG, "total from query: " + total);
        requestSearch.from = model.getCurrentPage() * TOTAL_PER_PAGE;
        model.from = requestSearch.from;
        LogUtil.d(TAG, " current page: " + requestSearch.from);
        return total > 0 && (model.getCurrentPage() <= Math.ceil((double) total / TOTAL_PER_PAGE));
    }

    public ResponseCount getTotalDataByQuery(RequestSearch request) {
        LogUtil.d(TAG, "get total by query");
        ResponseCount response = new ResponseCount();
        try {
            RequestCount requestCount = new RequestCount(request);
            LogUtil.d(TAG, requestCount);
            Call<ResponseCount> call = ElasticsearchConnection.open().getTotalProduct(requestCount, elasticurls+modelUrl+countlUrl);
            Response<ResponseCount> responseCall = call.execute();
            LogUtil.d(TAG, "code: " + responseCall.code() + " message: " + responseCall.message());
            if (ElasticsearchConnection.isSucceed(responseCall.code())) {
                response = responseCall.body();
                if (response != null) {
                    LogUtil.d(TAG," total data: " + response.count);
                }
            }
        } catch (Throwable t) {
            LogUtil.e(TAG, t);
        }
        return response;
    }

    public SearchResult searchAll(SearchQuery model) {
        model.isGlobal = true;
        return searchProduct(model);
    }

    private RequestSearch generateSearchQuery(SearchQuery model) {
        ElasticsearchUtil.QueryUtil util = new ElasticsearchUtil.QueryUtil();
        if (model.isKeywordExists()) {
            LogUtil.d(TAG, "include keyword");
            util.setQueryString(new QueryStringParam()
                    .setKeyword(model.getProcessedKeyword())
                    .setFuzzyLength(5)
                    .defaultOperator(QueryStringParam.OPERATOR_AND)
                    .setFieldsToSearch(new String[]{
                            NAMA_PRODUK+"^6",
                            NAMA_PENYEDIA,
                            NO_PRODUK,
                            NO_PRODUK_PENYEDIA,
                            NAMA_KOMODITAS,
                            CATEGORY_NAME
                    }));
            /*util.boolShould(new MultiMatchQuery()
                    .boost(1.1d)
                    .operator(AND_OPERATOR)
                    .query(model.keyword)
                    .type(MultiMatchQuery.TYPE_BEST_FIELD)
                    .fields(new String[]{
                            NAMA_PENYEDIA,
                            NO_PRODUK,
                            NO_PRODUK_PENYEDIA,
                            NAMA_KOMODITAS,
                            CATEGORY_NAME}));*/
        }

        if (model.isProductNameExists()) {
            LogUtil.d(TAG, "include product name");
            util.setQueryString(new QueryStringParam()
                    .setKeyword(model.getProcessedProductName())
                    .setFuzzyLength(5)
                    .defaultOperator(QueryStringParam.OPERATOR_AND)
                    .setFieldsToSearch(new String[]{
                            NAMA_PRODUK+"^6",
                    }));
        }

        if (model.isGlobal) {
            LogUtil.d(TAG, "global search");
            util.setAggregation(new TermAggregation()
                                .setAlias("commodity")
                                .setField(ID_KOMODITAS)
                                .setSize(30)
                                .setInnerAggregation(new InnerTermAggregation()
                                        .topHitAggregasion(new TopHitAggregation()
                                                .setAlias("hit_list")
                                                .size(1)
                                                .includeSource(new String[]{NAMA_KOMODITAS, ID_KOMODITAS}))));
        }

        if (model.isCategoryExists()) {
            LogUtil.d(TAG, "category exists");
            util.setNestedFilter(new NestedQuery()
                    .setQuery(new CategoryFilter()
                            .setCategoryId(model.categoryId)
                            .setField(ID)
                            .setPath(CATEGORIES))
                    .setInnerHit(ID));
        }

        if (model.isMinPriceExists() || model.isMaxPriceExists()) {
            LogUtil.d(TAG, "min or max price is exists");
            PriceRange priceRange = new PriceRange()
                    .setPath(PRICE_LIST)
                    .setField(PRICE_HARGA_UTAMA);
            if (model.isMaxPriceExists()) {
                priceRange.setMaxPrice(model.maxPrice);
            }
            if (model.isMinPriceExists()) {
                priceRange.setMinPrice(model.minPrice);
            }
            util.setNestedFilter(new NestedQuery()
                    .setQuery(priceRange));
        }

        if (model.isLocationExists()) {
            LogUtil.d(TAG, "price location exists");
            NestedQuery nestedQuery = new NestedQuery()
                    .setQuery(new PriceLocation()
                            .setField(ID)
                            .setPath(PRICE_LIST)
                            .setValue(model.locationId))
                    .setInnerHit(PRICE_LIST);
            if (model.isLocationTypeExists()) {
                nestedQuery.setQuery(new PriceLocation().setPath(PRICE_LIST).setField("type").setValue(model.type));
            }
            util.setNestedFilter(nestedQuery);
        }

        if (model.isCommodityExist()) {
            LogUtil.d(TAG, "commodity exists");
            util.setFilter(new BoolFilter().setField(ID_KOMODITAS).setValue(model.commodityId));
        }

        if (model.isUmkmExist()) {
            LogUtil.d(TAG, "umkm exists");
            util.setFilter(new BoolFilter().setField(IS_UMKM).setValue(model.isUmkm == 1));
        }

        if (model.isManufactureExists()) {
            LogUtil.d(TAG, "manufacture exists");
            util.setFilter(new BoolFilter().setField(MANUFAKTUR_ID).setValue(model.manufactureId));
        }

        if (model.isProviderExists()) {
            LogUtil.d(TAG, "provider exists");
            util.setFilter(new BoolFilter().setField(ID_PENYEDIA).setValue(model.providerId));
        }

        util.setFilter(new BoolFilter().setField(STATUS).setValue((long) 1));
        if (model.getSort() != null) {
            util.setSorting(model.getSort());
        }
        else {
            LogUtil.d(TAG, "Sort by score DESC");
            util.setSorting(new Sort("_score", Sort.DESC));
        }

        util.setLimit(TOTAL_PER_PAGE);
        util.setReturnFields(FIELDS);
        return util.buildRequest();
    }

    public SearchResult getPopularList() {
        SearchResult result = new SearchResult();
        try{
            SearchResult cacheResult = (SearchResult) Cache.get(POPULAR_CACHE_KEY);
            if (cacheResult != null) {
                Logger.debug(TAG + " get popular from cache");
                return cacheResult;
            }
        }catch (Throwable t){
            Logger.error(TAG + " ERROR: get popular from cache");
            //KlassInfoTablet.getMessage();
        }
        JobPopularProduk job1  = new JobPopularProduk(elasticurls,modelUrl,searchlUrl);
        job1.in(3);
//        new Job(){
//            public void doJob(){
//                try {
//                    SearchResult result = new SearchResult();
//                    InnerTermAggregation innerTermAggregation = new InnerTermAggregation();
//                    innerTermAggregation.setTerm(new MetricAggregation()
//                            .setMethod(MetricAggregation.METHOD_MAX)
//                            .setAlias("total_purchased")
//                            .setField("total_pembelian"))
//                            .topHitAggregasion(new TopHitAggregation()
//                                    .setAlias("hit_list")
//                                    .size(1)
//                                    .sort(new Sort("total_pembelian", "desc")));
//                    ElasticsearchUtil.QueryUtil builder = new ElasticsearchUtil.QueryUtil()
//                            .setAggregation(new TermAggregation()
//                                    .setAlias("commodity")
//                                    .setField(ID_KOMODITAS)
//                                    .setSize(12)
//                                    .setSort(new Sort("total_purchased", "desc"))
//                                    .setInnerAggregation(innerTermAggregation))
//                            .setLimit(0);
//                    Logger.debug(new GsonBuilder().setPrettyPrinting().create().toJson(builder.buildRequest()));
//                    Call<ResponseProduct> call = ElasticsearchConnection.open().getProduct(builder.buildRequest(),elasticurls+modelUrl+searchlUrl);
//                    Response<ResponseProduct> responseCall = call.execute();
//                    if (ElasticsearchConnection.isSucceed(responseCall.code())) {
//                        ResponseProduct response = responseCall.body();
//                        if (response != null) {
//                            result = response.getPopularResult();
//                            try{
//                                Cache.safeDelete(POPULAR_CACHE_KEY);
//                            }catch (Exception e){}
//                            Cache.set(POPULAR_CACHE_KEY, result, "10min");
//                        }
//                    }
//                } catch (Throwable t) {
//                    t.printStackTrace();
//                    Logger.error("error elastic get popular list: from server");
//                }
//            }
//        }.in(3);
        return result;
    }

    public List<MetaProduct> getRecommendation(SearchQuery model) {
        Logger.debug(TAG + " get recommendation products");
        SearchResult searchResult = new SearchResult();
        if (model.isCommodityExist()) {
            final String cacheKey = RECOMMENDATION_CACHE_KEY + model.commodityId;
            SearchResult cached  = new SearchResult();
            try{
                cached = (SearchResult) Cache.get(cacheKey);
            }catch (Throwable t){
                Logger.error(TAG + " ERROR: getRecommendation from cache");
                //t.getMessage();
            }
            if (cached != null) {
                searchResult = cached;
            } else {
                ElasticsearchUtil.QueryUtil builder = new ElasticsearchUtil.QueryUtil()
                        .setFilter(new BoolFilter()
                                .setField(ID_KOMODITAS)
                                .setValue(model.commodityId))
                        .setSorting(new Sort(TOTAL_PEMBELIAN, Sort.DESC))
                        .setReturnFields(FIELDS)
                        .setLimit(4);
                if (model.isKeywordExists()) {
                    builder.setQueryString(new QueryStringParam()
                            .setKeyword(model.keyword)
                            .setFuzzyLength(10)
                            .setFieldsToSearch(new String[]{
                                    NAMA_PRODUK + "^6",
                                    NAMA_PENYEDIA,
                                    NO_PRODUK,
                                    NO_PRODUK_PENYEDIA,
                                    NAMA_KOMODITAS,
                                    CATEGORY_NAME
                            }));
                }
                try {
                    Logger.debug(new GsonBuilder().setPrettyPrinting().create().toJson(builder.buildRequest()));
                    Call<ResponseProduct> call = ElasticsearchConnection.open().getProduct(builder.buildRequest(),elasticurls+modelUrl+searchlUrl);
                    Response<ResponseProduct> responseCall = call.execute();
                    if (ElasticsearchConnection.isSucceed(responseCall.code())) {
                        ResponseProduct response = responseCall.body();
                        if (response != null) {
                            searchResult.setRecommendations(response);
                            Logger.debug(TAG + " total recommendation: " + searchResult.recommendations.size());
                            try{
                                Cache.safeDelete(cacheKey);
                            }catch (Exception e){}
                            Cache.set(cacheKey, searchResult, "5min");
                        }
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        }
        return searchResult.recommendations;
    }

    public void inputViewCounter(Produk model, AktifUser activeUser) {
        inputViewCounter(new ElasticsearchUtil.InputUtil()
                .generateBulk(model, activeUser != null
                                        ? activeUser.user_id
                                        : 0));
    }

    public void inputViewCounter(List<Produk> produks) {
        inputViewCounter(new ElasticsearchUtil.InputUtil().generateBulk(produks));
    }

    public void inputViewCounterProduct(List<MetaProduct> productElastics) {
        inputViewCounter(new ElasticsearchUtil.InputUtil().generateProductElasticBulk(productElastics));
    }

    private void inputViewCounter(RequestBody requestBody) {
        try {
            Call<ResponseInputElastic> call = ElasticsearchConnection.open().bulkInputViewCounter(requestBody);
            Response<ResponseInputElastic> responseCall = call.execute();
            if (ElasticsearchConnection.isSucceed(responseCall.code())) {
                ResponseInputElastic response = responseCall.body();
                Logger.debug(TAG + " " + new GsonBuilder().setPrettyPrinting().create().toJson(response));
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /*================================== search suggestion =======================================*/

    private RequestSearch generateSearchSugestionQuery(SearchQuery model) {
        ElasticsearchUtil.QueryUtil util = new ElasticsearchUtil.QueryUtil();
        if (model.isKeywordExists()) {
            LogUtil.d(TAG, "include keyword");
            util.setQueryString(new QueryStringParam()
                    .setKeyword(model.getProcessedKeyword())
                    .setFuzzyLength(6)
                    .defaultOperator(QueryStringParam.OPERATOR_AND)
                    .setFieldsToSearch(new String[]{
                            NAMA_PRODUK+"^4",
                            NAMA_PENYEDIA+"^2",
                            NO_PRODUK+"^1",
                            NO_PRODUK_PENYEDIA,
                            NAMA_KOMODITAS,
                            CATEGORY_NAME
                    }));
        }

        LogUtil.d(TAG, "global search");
        util.setAggregation(new TermAggregation()
                .setAlias("commodity")
                .setField(ID_KOMODITAS)
                .setSize(5)
                .setInnerAggregation(new InnerTermAggregation()
                        .topHitAggregasion(new TopHitAggregation()
                                .setAlias("hit_list")
                                .size(1)
                                .includeSource(new String[]{NAMA_KOMODITAS, ID_KOMODITAS}))));

        util.setSorting(new Sort("_score", Sort.DESC));
        util.setLimit(0);
        return util.buildRequest();
    }

    public SearchResult searchSuggestion(SearchQuery model) {
        RequestSearch request = generateSearchSugestionQuery(model);
        SearchResult searchResult = new SearchResult();
        try {
            Logger.info(new GsonBuilder().setPrettyPrinting().create().toJson(request));
            Call<ResponseProduct> call = ElasticsearchConnection.open().getProduct(request,elasticurls+modelUrl+searchlUrl);
            Response<ResponseProduct> responseCall = call.execute();
            if (ElasticsearchConnection.isSucceed(responseCall.code())) {
                ResponseProduct response = responseCall.body();
                if (response != null) {
                    searchResult = response.getPopularResult();
                }
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return searchResult;
    }

}
