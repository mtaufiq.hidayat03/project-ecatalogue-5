ALTER TABLE `produk_tunggu_setuju`
ADD COLUMN `deleted_date` DateTime NULL;

ALTER TABLE `produk_tunggu_setuju`
ADD COLUMN `deleted_by` Int( 5 ) NULL;
