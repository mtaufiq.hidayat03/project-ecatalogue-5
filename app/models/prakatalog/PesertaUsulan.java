package models.prakatalog;

import models.BaseKatalogTable;
import models.user.User;
import play.Logger;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

/**
 * Created by rezaprima on 21/12/18
 */
@Table(name = "peserta_usulan")
public class PesertaUsulan extends BaseKatalogTable {

	@Id
	public Long id;
	public Long usulan_id;
	public Long user_id;

	public boolean active;

	public static Boolean isUserRegistedUsulan(Long user_id, Long usulan_id){
		boolean result = false;
		try{
			List<PesertaUsulan> pu = Query.find("select * from peserta_usulan where usulan_id=? AND user_id=? ",PesertaUsulan.class, usulan_id, user_id).fetch();
			result = (pu.size() > 0 );
		}catch (Exception e){
		}
		return  result;
	}

	public static long getJumlahPendaftar(long usulan_id){
		return PesertaUsulan.count("usulan_id = ? and active = ?", usulan_id, AKTIF);
	}
}
