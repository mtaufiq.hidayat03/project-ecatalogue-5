package controllers.prakatalog;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.common.AktifUser;
import models.common.DokumenHasilEvaluasi;
import models.common.DokumenInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.util.CommonUtil;
import models.katalog.Komoditas;
import models.katalog.Produk;
import models.katalog.ProdukHarga;
import models.katalog.ProdukKategori;
import models.katalog.komoditasmodel.PenawaranManufaktur;
import models.masterdata.*;
import models.penyedia.Penyedia;
import models.prakatalog.*;
import models.prakatalog.search.PenawaranSearch;
import models.prakatalog.search.PenawaranSearchResult;
import models.secman.Acl;
import models.user.User;
import models.util.Config;
import models.util.produk.ProdukHargaJson;
import models.util.sikap.*;
import org.json.JSONException;
import org.json.JSONObject;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.i18n.Messages;
import repositories.penyedia.ProviderRepository;
import repositories.prakatalog.PenawaranRepository;
import services.prakatalog.BeritaAcaraService;
import services.prakatalog.PenawaranService;
import services.prakatalog.UsulanPokjaService;
import services.prakatalog.UsulanService;
import utils.DateTimeUtils;
import utils.HtmlUtil;
import utils.SikapUtils;
import utils.UrlUtil;

import java.io.*;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class PenawaranCtr extends BaseController {

	public static final String TAG = "PenawaranCtr";

	@AllowAccess({Acl.COM_PENAWARAN})
	public static void index(){
		PenawaranSearch query = new PenawaranSearch(params);

        User user = User.findById(AktifUser.getActiveUserId());
        Map<String,String> statusMaps = Penawaran.statusMaps();
		List<Komoditas> komoditas = new ArrayList<>();

		if(getAktifUser().isAdminLokalSektoral()){
			komoditas = Komoditas.findByKldi(getAktifUser().kldi_id);
		}else if(getAktifUser().isProvider()){
			komoditas = Komoditas.findByPenyedia(Penyedia.getProviderByUserId(AktifUser.getActiveUserId().longValue()).id);
		}else{
			komoditas = Komoditas.getForDropDown();
		}

		PenawaranSearchResult model = new PenawaranSearchResult(query);

		if(query.urutkan == ""){
			query.urutkan = "8";
		}
		query.uid = user.id;
		model = PenawaranRepository.searchPackage(query);

		renderArgs.put("paramQuery", query);
        render("prakatalog/PenawaranCtr/index.html", model, user, statusMaps, komoditas);
	}

	@AllowAccess({Acl.COM_PENAWARAN})
	public static void penawaranPenyedia(){
		AktifUser aktifUser = getAktifUser();
		User user = User.findById(aktifUser.user_id);
		Map<String,String> statusMaps = Penawaran.statusMaps();
		render(user, statusMaps);
	}

	public static void pendaftarUsulan(long usulan_id){
		render(usulan_id);
	}

	public static void penawaranUsulan(Long usulan_id){
		Usulan usulan = new Usulan();
		Komoditas komoditas = new Komoditas();
		if (usulan_id!=null) {
			usulan = Usulan.findById(usulan_id);
			komoditas = Komoditas.findById(usulan.komoditas_id);


		}
		Map<String, String> statusMaps = Penawaran.statusMaps();

		PenawaranSearch query = new PenawaranSearch(params);

		User user = User.findById(AktifUser.getActiveUserId());

		PenawaranSearchResult model = new PenawaranSearchResult(query);

		if(query.urutkan == ""){
			query.urutkan = "8";
		}
		query.uid = user.id;
		model = PenawaranRepository.searchPackage(query);

		renderArgs.put("paramQuery", query);

		render(usulan,komoditas, statusMaps);
	}


	private static void renderCreate(Penawaran penawaran, long usulan_id, long rkn_id, Identitas identitas, String[] keterangan){
		String page = "create";

		//Data SIKaP
		List<UsulanKualifikasi> kualifikasiUsulanList = UsulanKualifikasi.find("usulan_id = ?",usulan_id).fetch();
		List<UsulanDokumenPenawaran> dokumenPenawaranList = UsulanDokumenPenawaran.find("usulan_id = ?",usulan_id).fetch();

		int datafeed = Komoditas.KOMODITAS_DATAFEED;
		Komoditas komoditas = Komoditas.findByUsulan(usulan_id);

		render("prakatalog/PenawaranCtr/create.html", penawaran, usulan_id, rkn_id, identitas,
				kualifikasiUsulanList, dokumenPenawaranList, keterangan, page, datafeed, komoditas);
	}

	private static void renderEdit(Penawaran penawaran, long rkn_id, Identitas identitas, Sikap dataSikap){
		String page = "edit";

		//Data SIKaP
		List<UsulanKualifikasi> kualifikasiUsulanList = UsulanKualifikasi.find("usulan_id = ?",penawaran.usulan_id).fetch();

		List<DokumenInfo> dokumenPenawaranList = DokumenInfo.listDokumenPenawaran(penawaran.id);


		render("prakatalog/PenawaranCtr/edit.html", penawaran, rkn_id, identitas,
				kualifikasiUsulanList, dokumenPenawaranList, page, dataSikap);
	}

	@AllowAccess({Acl.COM_PENAWARAN_ADD})
	public static void daftar(long usulan_id) {
		AktifUser aktifUser = getAktifUser();
		if(aktifUser!=null) {
			try{
				Boolean isRegistered = PesertaUsulan.isUserRegistedUsulan(aktifUser.user_id, usulan_id);
				if(!isRegistered){
					PesertaUsulan pu = new PesertaUsulan();
					pu.user_id = aktifUser.user_id;
					pu.usulan_id = usulan_id;
					pu.active = true;
					pu.save();
				}
			}catch (Exception e){
				Logger.info("Gagal daftar ke usulan:"+e.getMessage());
			}
		}
		redirect(UrlUtil.builder()
				.setUrl("admin.UsulanCtr.detailPengumuman")
				.setParam("id", usulan_id)
				.build());
	}

	@AllowAccess({Acl.COM_PENAWARAN_ADD})
	public static void create(long usulan_id) throws ParseException {
		AktifUser aktifUser = getAktifUser();
		Logger.info(" create: rkn_id : ");
		long rkn_id = 0;
		try {
			rkn_id = aktifUser.rkn_id;
		} catch (Exception e) {
		}

		if (Penawaran.sudahMengajukanPenawaran(aktifUser.user_id, usulan_id) ||
				Penawaran.sudahMengajukanPenawaranDanDitolak(aktifUser.user_id, usulan_id)) {
			params.flash();
			flash.error(Messages.get("prakatalog.penawaran.pengajuan.sudah_mengajukan.error"));
			redirect(UrlUtil.builder()
					.setUrl("admin.UsulanCtr.detailPengumuman")
					.setParam("id", usulan_id)
					.build());
		} else {
			Penawaran penawaran = new Penawaran();

			//Data SIKaP
			String cacheKey = String.valueOf(rkn_id);
			Identitas identitas = SikapUtils.getRekananIdentitas(rkn_id);
			validation.valid(identitas);
			if (validation.hasErrors() && identitas == null) {
				params.flash();
				flash.error(Messages.get("prakatalog.penawaran.sikap.error"));
			}

			Sikap sikap = new Sikap();
			if (SikapUtils.isExistCachedData(cacheKey)) {
				sikap = Cache.get(cacheKey, Sikap.class);
			}
			Cache.set(cacheKey, sikap);

			renderCreate(penawaran, usulan_id, rkn_id, identitas, null);
		}
	}

	@AllowAccess({Acl.COM_PENAWARAN_EDIT})
	public static void edit(long id){
		AktifUser aktifUser = getAktifUser();
		Long rkn_id = aktifUser.rkn_id;
		if (rkn_id == null){
			rkn_id = 0L;
		}

		Penawaran penawaran = Penawaran.findById(id);
		notFoundIfNull(penawaran);

//		Sikap dataSikap = penawaran.getDataSikap();
//		Identitas identitas = dataSikap.identitas;

		String cacheKey = String.valueOf(rkn_id);
		Identitas identitas = SikapUtils.getRekananIdentitas(rkn_id);
		validation.valid(identitas);
		if(validation.hasErrors() && identitas == null){
			params.flash();
			flash.error(Messages.get("prakatalog.penawaran.sikap.error"));
		}

		Sikap dataSikap = new Sikap();
		if(SikapUtils.isExistCachedData(cacheKey)){
			dataSikap = Cache.get(cacheKey,Sikap.class);
		}
		Cache.set(cacheKey,dataSikap);

		penawaran.revisi_sudah_kadaluarsa = Penawaran.isPenawaranKadaluarsa(penawaran);

		renderEdit(penawaran, rkn_id, identitas, dataSikap);
	}

	@AllowAccess({Acl.COM_PENAWARAN_EDIT})
	public static void editSubmit(Penawaran penawaran, File[] dokumen, String[] keterangan, String[] nama_dokumen, Long[] dok_id){
		Penawaran dbPenawaran = Penawaran.findById(penawaran.id);
		AktifUser aktifUser = getAktifUser();
		long rkn_id = 0;
		try{
			rkn_id = aktifUser.rkn_id;
		}catch (Exception e){Logger.info("editsubmit penawaran rknid=0, user:"+aktifUser.user_id);}

		Sikap cachedData = null;
		//handling
		try{
			cachedData = Cache.get(String.valueOf(rkn_id),Sikap.class);
			//pada mode prod
			if(cachedData == null){
				//ambil lagi data SIKaP
				String key = String.valueOf(rkn_id);
				cachedData = SikapUtils.getDataSikap(key,"all",rkn_id);
			}
		}catch (Exception e){
			Logger.error("error data sikap:"+e.getMessage());
			e.printStackTrace();
		}

		for(int i = 0;i<dokumen.length;i++){
			DokumenPenawaran.editDokPenawaran(dbPenawaran.id, dokumen[i], keterangan[i], nama_dokumen[i], dok_id[i]);
		}

		//pada mode prod cache akan cepat null, check dipindah keatas
		if(cachedData == null){
			//ambil lagi data SIKaP
			String key = String.valueOf(rkn_id);
			cachedData = SikapUtils.getDataSikap(key,"all",rkn_id);
		}

		dbPenawaran.data_sikap = new Gson().toJson(cachedData);
		dbPenawaran.active = true;
		dbPenawaran.status = Penawaran.STATUS_MENUNGGU_VERIFIKASI;
		dbPenawaran.save();

		index();
		//penawaranPenyedia();
	}

	@AllowAccess({Acl.COM_PENAWARAN_ADD})
	public static void createSubmit(Penawaran penawaran, File[] dokumen, String[] keterangan, String[] nama_dokumen, Long komoditas_id){
		AktifUser aktifUser = getAktifUser();
		Logger.info("createsubmit: rkn_id:"+aktifUser.rkn_id);
		long rkn_id = 0;
		boolean dokumenValid = true;
		boolean dokumenPenawaranSuksesDisimpan = true;

		try{
			rkn_id = aktifUser.rkn_id;
		}catch (Exception e){Logger.info("createsubmit penawaran rknid=0, user:"+aktifUser.user_id);}

		Sikap cachedData = null;
		//handling
		try{
			cachedData = Cache.get(String.valueOf(rkn_id),Sikap.class);
			//pada mode prod
			if(cachedData == null){
				//ambil lagi data SIKaP
				String key = String.valueOf(rkn_id);
				cachedData = SikapUtils.getDataSikap(key,"all",rkn_id);
			}
		}catch (Exception e){
			Logger.error("error data sikap:"+e.getMessage());
			e.printStackTrace();
		}

		validation.valid(penawaran);
		if (validation.hasErrors()) {
			params.flash();
			flash.error("Data SIKaP Anda belum lengkap(penawaran). Silahkan melengkapi data SIKaP Anda di <a target='_blank' href='http://sikap.lkpp.go.id'>sikap.lkpp.go.id</a>");
			renderCreate(penawaran, penawaran.usulan_id, rkn_id, cachedData.identitas, keterangan);
		}

		//validasi dokumen penawaran
		for(int i = 0;i<dokumen.length;i++){
			if (keterangan[i].toString().length() > 255){
				dokumenValid = false;
			}
		}

		if (!dokumenValid){ //jika ada keterangan dokumen yang panjangnya lebih dari 255
			params.flash();
			flash.error("Ada informasi Keterangan yang panjangnya lebih dari 255 karakter di dokumen penawaran yang anda unggah. Silahkan diperbaikai terlebih dahulu!");
			renderCreate(penawaran, penawaran.usulan_id, rkn_id, cachedData.identitas, keterangan);
		}

		if(Play.mode.isProd()){
			//if prodDev=false
//			if(!Config.getInstance().prodDev) {
//				validation.valid(penawaran);
//				if (validation.hasErrors()) {
//					params.flash();
//					flash.error("Data SIKaP Anda belum lengkap(penawaran). Silakan melengkapi data SIKaP Anda di <a target='_blank' href='http://sikap.lkpp.go.id'>sikap.lkpp.go.id</a>");
//					renderCreate(penawaran, penawaran.usulan_id, rkn_id, cachedData.identitas, keterangan);
//
//				}
//			}
			//if prodDev=false
			if(!Config.getInstance().prodDev){
				if(!isDataSikapValidate(cachedData)){
					Logger.error("datasikap notvalid :"+cachedData);
					params.flash();
					flash.error("Data SIKaP Anda belum lengkap. Silhakan melengkapi data SIKaP Anda di <a target='_blank' href='http://sikap.lkpp.go.id'>sikap.lkpp.go.id</a>");
					renderCreate(penawaran, penawaran.usulan_id,rkn_id,cachedData.identitas, keterangan);
				}
			}
		}

		if(Penawaran.sudahMengajukanPenawaran(aktifUser.user_id,penawaran.usulan_id)){
			Penawaran oldPenawaran = Penawaran.getPenawaranByPenyediaAndUsulan(aktifUser.user_id,penawaran.usulan_id);
			oldPenawaran.active = false;
			oldPenawaran.save();
		}

		//pada mode prod cache akan cepat null, check dipindah keatas
		if(cachedData == null){
			//ambil lagi data SIKaP
			String key = String.valueOf(rkn_id);
			cachedData = SikapUtils.getDataSikap(key,"all",rkn_id);
		}

		penawaran.data_sikap = new Gson().toJson(cachedData);
		Logger.debug("user id aktif: "+aktifUser.user_id);
		penawaran.user_id = aktifUser.user_id;
		penawaran.komoditas_id = komoditas_id;
		penawaran.status = Penawaran.STATUS_MENUNGGU_VERIFIKASI;
		penawaran.active = true;

		//Cek Apakah sudah insert ke table Penyedia
		Penyedia penyedia = ProviderRepository.findByUserId(aktifUser.user_id);
		if(penyedia == null){
			Long penyedia_id = ProviderRepository.saveNewByRknId(rkn_id, aktifUser.user_id);
			if(penyedia_id != null){
				penawaran.penyedia_id = penyedia_id;
			}
		}else{
			penawaran.penyedia_id = penyedia.id;
		}

		long penawaran_id = penawaran.save();
		for(int i = 0;i<dokumen.length;i++){
			try {
				DokumenPenawaran.simpanDokPenawaran(penawaran_id, dokumen[i], keterangan[i], nama_dokumen[i]);
			}catch (Exception ex){
				ex.printStackTrace();
				dokumenPenawaranSuksesDisimpan = false;
			}
		}

		if (!dokumenPenawaranSuksesDisimpan){ //jika dokumen penawaran ada yang gagal tersimpan maka rollback penawarannya dan buat ulang
			Penawaran penawaranBaru = Penawaran.findById(penawaran_id);
			penawaranBaru.active = false;
			penawaranBaru.save();

			List<DokumenPenawaran> dokumenPenawaranList = DokumenPenawaran.listDokumenPenawaran(penawaran_id);

			if (dokumenPenawaranList.size() > 0){
				for (DokumenPenawaran dp : dokumenPenawaranList){
					dp.active = false;
					dp.save();
				}
			}

			params.flash();
			flash.error("Gagal menyimpan dokumen penawaran. Silahkan ulangi lagi!");
			renderCreate(penawaran, penawaran.usulan_id, rkn_id, cachedData.identitas, keterangan);
		}

		index();
//		penawaranPenyedia();

	}

	@AllowAccess({Acl.COM_PENAWARAN, Acl.COM_PENAWARAN_PRODUK_LIST})
	public static void daftarProduk(long id) throws ParseException {
		Penawaran penawaran = Penawaran.findById(id);
		Usulan usulan = Usulan.findById(penawaran.usulan_id);
		Komoditas komoditas = Komoditas.findById(penawaran.komoditas_id);

		if(komoditas.isDatafeed()){
			notFound();
		}else{
			AktifUser aktifUser = getAktifUser();
			boolean isPenyedia = penawaran.user_id.equals(aktifUser.user_id);
			boolean ableToCreateAndEdit = isPenyedia && penawaran.allowToAddProduct();
			boolean beforeNegotiationEnd = isPenyedia && UsulanService.isBeforeOrSameNegotiationDate(usulan);
			boolean isMenungguVerifikasiOrLolosVerifikasiOrNegosiasi = Penawaran.isMenungguVerifikasiOrLolosVerifikasiOrNegosiasi(penawaran);
			render(id, ableToCreateAndEdit, beforeNegotiationEnd, isMenungguVerifikasiOrLolosVerifikasiOrNegosiasi);
		}
	}

	@AllowAccess({Acl.COM_PENAWARAN})
	public static void detail(long id){
		AktifUser aktifUser = getAktifUser();
		Penawaran penawaran = Penawaran.findById(id);
		if( aktifUser.isPenyedia() && !penawaran.user_id.equals(aktifUser.user_id)) {
			forbidden(Messages.get("not-authorized"));
		}

		Sikap dataSikap = new Gson().fromJson(penawaran.data_sikap, Sikap.class);
		Identitas identitas = dataSikap.identitas;

		List<UsulanKualifikasi> kualifikasiUsulanList = UsulanKualifikasi.find("usulan_id = ?",penawaran.usulan_id).fetch();
		List<DokumenInfo> dokumenPenawaranList = DokumenInfo.listDokumenPenawaran(id);
		List<DokumenTemplate> baList = DokumenTemplate.findAllBAActive();
		List<BeritaAcara> bas = BeritaAcara.findByPenawaranId(penawaran.id);

		String page = "detail";
		render(penawaran,identitas,dataSikap,kualifikasiUsulanList,dokumenPenawaranList,page,aktifUser,baList,bas);
	}

	@AllowAccess({Acl.COM_PENAWARAN})
	public static void verification(long id) throws ParseException {
		String page = "verification";

		Penawaran penawaran = Penawaran.findById(id);
		List<UsulanKualifikasi> kualifikasiUsulanList = UsulanKualifikasi.find("usulan_id = ?",penawaran.usulan_id).fetch();
		Sikap dataSikap = new Gson().fromJson(penawaran.data_sikap,Sikap.class);
		Identitas identitas = dataSikap.identitas;

		penawaran.tanggal_batas_akhir_boleh_revisi = Penawaran.tetapkanBatasAwalRevisi(penawaran.id);

		List<DokumenInfo> dokumenPenawaranList = DokumenInfo.listDokumenPenawaran(id);
		Usulan usulan = Usulan.findById(penawaran.usulan_id);
		if (usulan != null) {
			final Boolean isRevision = UsulanService.isRevision(usulan);
			renderArgs.put("canDoVerification", UsulanService.isAfterPenawaran(usulan));
			renderArgs.put("isRevision", isRevision);
			renderArgs.put("isAfterRevision", UsulanService.isAfterRevision(penawaran));
			renderArgs.put("isAfterPokjaForRevision", UsulanService.isAfterPokjaAskedForRevision(penawaran));
			Tahapan tahapan = Tahapan.getFlaggingTahapanRevisi();
			UsulanJadwal usulanJadwal = UsulanJadwal.getBy(tahapan.id, usulan.id);
			renderArgs.put("revisionEndDate", DateTimeUtils.convertToDdMmYyyy(usulanJadwal.tanggal_selesai));
		}
		render(penawaran,dataSikap,identitas,kualifikasiUsulanList, dokumenPenawaranList, page);
	}

	@AllowAccess({Acl.COM_PENAWARAN})
	public static void verificationSikapSubmit(long penawaran_id, VerifikasiSikap verifikasi, UnVerifikasiSikap unverifikasi) throws ParseException {
		checkAuthenticity();
		if (verifikasi != null) {


			Penawaran penawaran = Penawaran.findById(penawaran_id);

			User userPenyedia = User.findById(penawaran.user_id);

			AktifUser aktifUser = getAktifUser();
			User userVerifikator = User.findById(aktifUser.user_id);

			if(userVerifikator.isValidPokjaProperti()){
				Sikap dataSikap = penawaran.getDataSikap();

				dataSikap = SikapUtils.updateVerifikasiDataSikap(dataSikap,
						userVerifikator, userPenyedia.rkn_id, verifikasi, unverifikasi);

				penawaran.data_sikap = CommonUtil.toJson(dataSikap);
				penawaran.active = true;
				penawaran.save();
			}else {
				flash.error("Pastikan profile data anda lengkap, sebelum memverifikasi SIKAP");
			}
		} else {
			flash.error("Periksa kembali, belum ada data untuk diverifikasi!");
		}
		verification(penawaran_id);

	}

	@AllowAccess({Acl.COM_PENAWARAN})
	public static void verificationAgree(long penawaran_id){
		Penawaran penawaran = Penawaran.findById(penawaran_id);
		notFoundIfNull(penawaran);
		User user = User.findById(penawaran.user_id);

		Penyedia penyedia = ProviderRepository.findByUserId(user.id);
		if(penyedia == null){
			Long penyedia_id = ProviderRepository.saveNewByRknId(user.rkn_id, user.id);
			if(penyedia_id != null){
				penawaran.penyedia_id = penyedia_id;
			}
		}else{
			penawaran.penyedia_id = penyedia.id;
		}

		if(Komoditas.isDatafeed(penawaran.komoditas_id)){
			penawaran.status = Penawaran.STATUS_PERSIAPAN_KONTRAK;
		}else{
			List<Produk> produkList = Produk.findByPenawaranAndMenungguPersetujuan(penawaran_id);
			if(produkList.size() > 0){
				penawaran.status = Penawaran.STATUS_NEGOSIASI;
			}else{
				penawaran.status = Penawaran.STATUS_LOLOS_KUALIFIKASI;
			}
		}

		penawaran.active = true;
		penawaran.save();

		params.flash();
		flash.success(Messages.get("prakatalog.penawaran.verifikasi.success"));

		index();
//		penawaranUsulan(penawaran.usulan_id);

	}

	@AllowAccess({Acl.COM_PENAWARAN})
	public static void verificationReject(long penawaran_id, String alasan_tolak){
		checkAuthenticity();
		Penawaran penawaran = Penawaran.findById(penawaran_id);
		notFoundIfNull(penawaran);
		penawaran.status = Penawaran.STATUS_GAGAL_KUALIFIKASI;
		penawaran.alasan_tolak = alasan_tolak;
		penawaran.active = true;
		penawaran.save();

		index();
//		penawaranUsulan(penawaran.usulan_id);

	}

	@AllowAccess({Acl.COM_PENAWARAN})
	public static void verificationCorrection(long penawaran_id, String alasan_revisi, String batas_akhir_revisi) throws Exception {
		checkAuthenticity();
		Penawaran penawaran = Penawaran.findById(penawaran_id);
		notFoundIfNull(penawaran);
		penawaran.status = Penawaran.STATUS_REVISI;
		penawaran.alasan_revisi = alasan_revisi;
		penawaran.batas_akhir_revisi = DateTimeUtils.formatStringToDate(batas_akhir_revisi);
		penawaran.active = true;
		penawaran.save();

		index();
//		penawaranUsulan(penawaran.usulan_id);

	}

	@AllowAccess({Acl.COM_PENAWARAN})
	public static void getDataSikap(String jenis_kualifikasi, long rkn_id){
		String key = String.valueOf(rkn_id);
		Sikap dataSikap = SikapUtils.getDataSikap(key,jenis_kualifikasi,rkn_id);
		if(dataSikap == null){
			dataSikap = new Sikap();
		}
		renderJSON(dataSikap);

	}

	@AllowAccess({Acl.COM_PENAWARAN_CETAK_BA_HASIL_EVALUASI})
	public static void cetakHasilEvaluasiForm(Long id) throws JSONException {
		Penawaran penawaran = Penawaran.getPenawaranUserKomoditas(id);
		Penyedia penyedia = Penyedia.getProviderByUserId(penawaran.user_id);
		Penawaran penawaranSikap = Penawaran.findById(id);
		Boolean isJsonBAEvaluasi = false;
		Boolean isJsonBAPembuktian = false;
		DokumenUpload dataBAEvaluasi = DokumenUpload.getDataActive(id,1);
		DokumenUpload dataBAPembuktian = DokumenUpload.getDataActive(id,2);


		String jsonBAEvaluasi = "false";
		String jsonBAPembuktian = "false";


		if(dataBAEvaluasi != null){
			isJsonBAEvaluasi = true;
			isJsonBAPembuktian = true;
			jsonBAEvaluasi = dataBAEvaluasi.data;
		}

		if (dataBAPembuktian != null){
			isJsonBAPembuktian = true;
			jsonBAPembuktian = dataBAPembuktian.data;
		}else{
			jsonBAPembuktian = jsonBAEvaluasi;
		}

		Logger.debug(penawaran.usulan_id+"");
		List<UsulanKualifikasi> usulanKualifikasiList = UsulanKualifikasi.find("usulan_id = ?", penawaran.usulan_id).fetch();
		List<DokumenInfo> dokumenPenawaranList = DokumenInfo.listDokumenPenawaran(id);
		List<User> pokjaList = User.findPokjaListOrderByName();
		Sikap dataSikap = new Gson().fromJson(penawaranSikap.data_sikap,Sikap.class);
		Usulan usulan = Usulan.getDetail(penawaran.usulan_id);
		usulan.withPokja();

		List<DokumenInfo>dokumenAdministrasi = new ArrayList();
		List<DokumenInfo>dokumenTeknis = new ArrayList();
		List<DokumenInfo>dokumenHarga = new ArrayList();
		List<DokumenInfo>dokumenKualifikasi = new ArrayList();


		//proses pemecahan variable dokumenPenawaranList kedalam masing masing List diatas
		for(DokumenInfo di: dokumenPenawaranList){
			di.name_alias = di.name.replace(" ","_").toLowerCase();
			if(di.jenis_evaluasi != null && di.jenis_evaluasi.equalsIgnoreCase("administrasi")){
				dokumenAdministrasi.add(di);
			}else if(di.jenis_evaluasi != null && di.jenis_evaluasi.equalsIgnoreCase("harga")){
				dokumenHarga.add(di);
			}else if(di.jenis_evaluasi != null && di.jenis_evaluasi.equalsIgnoreCase("kualifikasi")){
				dokumenKualifikasi.add(di);
			}else{
				dokumenTeknis.add(di);
			}
		}

		render(penawaran, usulanKualifikasiList, dokumenPenawaranList, dokumenAdministrasi,dokumenHarga,dokumenTeknis,
				dokumenKualifikasi, usulan, dataSikap, pokjaList, isJsonBAEvaluasi,isJsonBAPembuktian,jsonBAEvaluasi,
				jsonBAPembuktian,dataBAEvaluasi,dataBAPembuktian,penyedia);
	}

	@AllowAccess({Acl.COM_PENAWARAN_CETAK_BA_HASIL_EVALUASI})
	public static void beritaAcara(Long id) {
		List<DokumenTemplate> baList = DokumenTemplate.findAllBAActive();
		List<BeritaAcara> bas = BeritaAcara.findByPenawaranId(id);
		render(id, bas,baList);
	}

	@AllowAccess({Acl.COM_PENAWARAN_CETAK_BA_HASIL_EVALUASI})
	public static void createBeritaAcara(Long penawaranId) {
		Penawaran model = Penawaran.getDataBA(penawaranId);
		DokumenTemplate doc = DokumenTemplate.findById(13);

		render("prakatalog/PenawaranCtr/createBeritaAcara.html",penawaranId, model, doc);
	}

	public static void storeBeritaAcara(BeritaAcara model, Long dokumen_id) {
		try {
			String decodeText = HtmlUtil.decodeFromTinymce(model.dokumen_ba);

			BeritaAcara bas = new BeritaAcara();
			bas.penawaran_id = model.penawaran_id;
			bas.dokumen_template_id = dokumen_id;
			bas.no_ba = model.no_ba;
			bas.dokumen_ba = decodeText;
			bas.tanggal_ba = DateTimeUtils.parseDdMmYyyy(model.tanggal_berita_acara);
			bas.deskripsi = model.deskripsi;
			bas.active = true;
			bas.save();

			flash.success(Messages.get("prakatalog.penawaran.ba.success"));
			detail(model.penawaran_id);
		}catch (Exception e){
			flash.error(Messages.get("prakatalog.penawaran.ba.error"));
			createBeritaAcara(model.penawaran_id);
		}
	}

	public static void loadBeritaAcara(Long templateId, Long penawaranId, String noBa){
		DokumenTemplate baTemplate = DokumenTemplate.findById(templateId);
		Penawaran dataPenawaran = Penawaran.getDataBA(penawaranId);
		LocalDate localDate = LocalDate.now();

		List<Produk> listProdukPenawaran = Produk.findAllProdukByPenawaran(penawaranId);
		List<UsulanPokja> listPokja = UsulanPokja.findByPenawaranId(penawaranId);
		Logger.info(localDate.getDayOfWeek().name());
		Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-LL-yyyy");

		Map<String, Object> params = new HashMap<>();
		params.put("ba",dataPenawaran);
		params.put("no_ba",noBa);
		params.put("tanggal",String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
		params.put("hari",DateTimeUtils.getReadableDay(cal.getTime().getDay()));
		params.put("bulan",DateTimeUtils.getReadableMonth(cal.get(Calendar.MONTH)));
		params.put("tahun",String.valueOf(cal.get(Calendar.YEAR)));
		params.put("formatTanggal",localDate.format(formatter));
		params.put("listPokja", UsulanPokjaService.listPokjaUsulan(listPokja));

		if(!listProdukPenawaran.isEmpty()){
			List<ListHargaBeritaAcara> listDataProduk = new ArrayList<ListHargaBeritaAcara>();
			List<ProdukHarga> listHarga = ProdukHarga.getHargaByPenawaran(penawaranId);
			List<Provinsi> listProvinsi = Provinsi.findAllActive();
			List<Kabupaten>listKabupaten = Kabupaten.findAll();
			List<ProdukHargaJson> aa = new ArrayList<>();

			params.put("produk_penawaran",BeritaAcaraService.generateListProdukBAEvaluasiKlarifikasiTeknisSertaHarga(listProdukPenawaran));

			if(dataPenawaran.kelas_harga.equals("nasional")){
				listHarga.forEach(h->{
					ProdukHargaJson hargaProdukJson = new Gson().fromJson(h.harga, ProdukHargaJson.class);
					hargaProdukJson.nama_produk = h.nama_produk;
					hargaProdukJson.nama_manufaktur = h.nama_manufaktur;
					hargaProdukJson.unit_pengukuran = h.nama_unit_pengukuran;

					aa.add(hargaProdukJson);
				});
				params.put("produk",BeritaAcaraService.generateListProdukHargaNasional(aa));
			}else if(dataPenawaran.kelas_harga.equals("provinsi")){
				listHarga.forEach( x -> {
							List<ProdukHargaJson> phj = new Gson().fromJson(x.harga, new TypeToken<List<ProdukHargaJson>>() {
							}.getType());
							phj.forEach(h -> {
								h.produkId = x.produk_id;
							});
							aa.addAll(phj);
						}
				);
				aa.forEach(h->
						listDataProduk.add(new ListHargaBeritaAcara(h.produkId, h.provinsiId, (long)1, h.harga,
								listProvinsi,
								listKabupaten,
								listProdukPenawaran
						))
				);
				params.put("produk",BeritaAcaraService.generateListProdukHargaProvinsi(listDataProduk));
			}else{
				listHarga.forEach( x -> {
					List<ProdukHargaJson> phj = new Gson().fromJson(x.harga, new TypeToken<List<ProdukHargaJson>>() {
					}.getType());
					phj.forEach(h -> {
						h.produkId = x.produk_id;
					});
							aa.addAll(phj);
						}
				);

				aa.forEach(h->
					listDataProduk.add(new ListHargaBeritaAcara(h.produkId, h.provinsiId, h.kabupatenId, h.harga,
							listProvinsi,
							listKabupaten,
							listProdukPenawaran
					))
				);
				params.put("produk",BeritaAcaraService.generateListProdukHargaKabupaten(listDataProduk));
			}
		}

		baTemplate.template = HtmlUtil.renderHtml(baTemplate.template,params);

		renderJSON(baTemplate);
	}

	public static void simpanBA(Long penawaranId, Long dokumenId, Long baId, String mytextarea, String noBa){

		List<DokumenTemplate> baList = DokumenTemplate.findAllBAActive();
		List<BeritaAcara> bas = BeritaAcara.findByPenawaranId(penawaranId);

		BeritaAcara ba  = new BeritaAcara();

		params.flash();
		//encode decode html submit
		String decodedText = HtmlUtil.decodeFromTinymce(mytextarea);
		mytextarea = decodedText;

		if(baId != 0){
			ba.updateBA(baId,mytextarea);
			flash.success(Messages.get("berhasil terupdate"));
		}else{
			ba.simpanBA(penawaranId,dokumenId,noBa,mytextarea);
			flash.success(Messages.get("berhasil terinput"));
		}

		detail(penawaranId);
	}


	public static void getBeritaAcara(Long baId) throws JSONException{
		BeritaAcara ba = BeritaAcara.findById(baId);
		renderJSON(ba);
	}

	public static void hapusBeritaAcara(Long baId){
		BeritaAcara ba = BeritaAcara.findById(baId);

		if(ba.blb_id!=null){
			hapusFileBA(ba.blb_id);
		}

		ba.deleteBa(baId);
//		renderJSON(1);
		detail(ba.penawaran_id);
	}

	public static void downloadDocBA(long baId) {
		BeritaAcara ba = BeritaAcara.findById(baId);
		renderArgs.put("isi_text",ba.dokumen_ba);
		renderArgs.put("nama_file", "dokumen_berita_acara_"+ba.no_ba);
		render("MediaManager/create.html");
	}

	public static void uploadBA(File file, Long berita_acara_id){
		BeritaAcara ba = BeritaAcara.findById(berita_acara_id);
		if(ba != null){
			try {
				BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, ba.id, BeritaAcara.class.getName());
				blob.blb_nama_file = file.getName();
				ba.nama_file = blob.blb_nama_file;
				ba.blb_id = blob.id;
				Logger.info("data ba "+ba);
				ba.save();
				flash.success("Berita Acara berhasil diunggah");
			} catch (Exception e) {
				e.printStackTrace();
				flash.error("Berita Acara gagal diunggah");
			}
		}

		detail(ba.penawaran_id);
	}

//	public static void createBeritaAcara(BeritaAcara ba, File file) throws FileNotFoundException, JSONException {
		//ba.simpanBeritaAcara(file);
		//beritaAcara(ba.usulan_id);
//	}

	@AllowAccess({Acl.COM_PENAWARAN_CETAK_BA_HASIL_EVALUASI})
	public static void hapusFileBA(Long idFile){
		BlobTable blobTable = BlobTable.findByBlobId(idFile); //.findById(fileId, 0);
		BeritaAcara pl = BeritaAcara.find("blb_id = ?",blobTable.id).first();
		blobTable.preDelete();
		blobTable.delete();
		//int result = pl.delete();
		pl.deleteFileBA(pl.id);
	}

	@AllowAccess({Acl.COM_PENAWARAN_CETAK_BA_HASIL_EVALUASI})
	public static void cetakHasilEvaluasi(Long penawaran_id, DokumenHasilEvaluasi dok) throws FileNotFoundException, JSONException {
		Penawaran penawaran = Penawaran.findById(penawaran_id);
		Penyedia penyedia = Penyedia.getProviderByUserId(penawaran.user_id);
		DokumenTemplate dokumenTemplate = DokumenTemplate.findById(6); //Hardcode
		DokumenTemplate dokumenTemplatePembuktian = DokumenTemplate.findById(8); //Hardcode
		if (dokumenTemplate == null ||dokumenTemplatePembuktian == null ) {
			flash.error("template not found!");
			index();
//			penawaranUsulan(penawaran.usulan_id);
		}else{

			JSONObject dataEvaluasi = new JSONObject();
			dataEvaluasi.put("data_evaluasi",PenawaranService.createJsonData(dok));

			long dokumenkategori_id = 0;
			if(dok.hasilPembuktianKualifikasi[0].contains("Sesuai")){
				dokumenkategori_id = 1;
			}else if(dok.hasilPembuktianKualifikasi[0].contains("Lulus")){
				dokumenkategori_id = 2;
			}

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("dok", PenawaranService.populateDataForDocument(dok,penawaran));


			Map<String,InputStream> map = new HashMap<>();
			InputStream is = HtmlUtil.generatePDF(dokumenTemplate.template, params);

			map.put("BA Evaluasi "+dok.namaPenyedia+".pdf",is);

			OutputStream outputStream = null;
			LocalDate localDate = LocalDate.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddLLyyyy");
			String formattedString = localDate.format(formatter);
			try{
				String filePath = "/public/files/dokumen/";
				String fileName = penyedia.id.toString()+penawaran_id+formattedString+"_"+getRandom()+".pdf";
				outputStream = new FileOutputStream(new File(Play.applicationPath+filePath+fileName));
				int read = 0;
				byte[] bytes = new byte[1024];

				while ((read = is.read(bytes)) != -1) {
					outputStream.write(bytes, 0, read);
				}

				DokumenUpload previousData = DokumenUpload.getPreviousData(penawaran_id,penyedia.id,dokumenkategori_id);

				if(previousData != null){
					DokumenUpload.updateToNonActive(previousData.id);
				}
				DokumenUpload dokumenUpload = new DokumenUpload();
				dokumenUpload.dokumen_kategori_id = dokumenkategori_id;
				dokumenUpload.penyedia_id = penyedia.id;
				dokumenUpload.penawaran_id = penawaran_id;
				dokumenUpload.nama_dokumen = fileName;
				dokumenUpload.file_path = filePath;
				dokumenUpload.data = dataEvaluasi.toString();
				dokumenUpload.active = true;
				dokumenUpload.save();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {

			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}
			cetakHasilEvaluasiForm(penawaran_id);
		}
	}

	public static String getRandom(){
		String SALTCHARS = "abcdefghijklmnopqrstuvwxyz1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 5) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;
	}

	private static boolean isDataSikapValidate(Sikap sikap){

		//Izin Usaha
		if(sikap.izin_usaha != null){
			for (IzinUsaha part : sikap.izin_usaha){
				validation.valid(part);
				if(validation.hasErrors()){
					Logger.info("izin_usaha not valid");
					return false;
				}
			}
		}

		//Akta
		if(sikap.akta != null){
			//Pendirian
			for (Akta pendirian : sikap.akta.akta_pendirian){
				validation.valid(pendirian);
				if(validation.hasErrors()){
					Logger.info("akta_pendirian not valid");
					return false;
				}
			}

			//Perubahan
			for (Akta perubahan : sikap.akta.akta_perubahan){
				validation.valid(perubahan);
				if(validation.hasErrors()){
					Logger.info("akta_perubahan not valid");
					return false;
				}
			}
		}

		//Pemilik
		if(sikap.pemilik != null){
			for (Pemilik part : sikap.pemilik){
				validation.valid(part);
				if(validation.hasErrors()){
					Logger.info("pemilik not valid");
					return false;
				}
			}
		}

		//Pengurus
		if(sikap.pengurus != null){
			for (Pengurus part : sikap.pengurus){
				validation.valid(part);
				if(validation.hasErrors()){
					Logger.info("pengurus not valid");
					return false;
				}
			}
		}

		//Tenaga Ahli
		if(sikap.tenaga_ahli != null){
			for (TenagaAhli part : sikap.tenaga_ahli){
				validation.valid(part);
				if(validation.hasErrors()){
					Logger.info("tenaga_ahli not valid");
					return false;
				}
			}
		}

		//Peralatan
		if(sikap.peralatan != null){
			for (Peralatan part : sikap.peralatan){
				validation.valid(part);
				if(validation.hasErrors()){
					Logger.info("peralatan not valid");
					return false;
				}
			}
		}

		//Pengalaman
		if(sikap.pengalaman != null){
			for (Pengalaman part : sikap.pengalaman){
				validation.valid(part);
				if(validation.hasErrors()){
					Logger.info("pengalaman not valid");
					return false;
				}
			}
		}

		//Pajak
		if(sikap.pajak != null){
			for (Pajak part : sikap.pajak){
				validation.valid(part);
				if(validation.hasErrors()){
					Logger.info("pajak not valid");
					return false;
				}
			}
		}

		return true;
	}

	@AllowAccess({Acl.COM_PENAWARAN_ADD_MANUFAKTUR_PRODUK})
	public static void inputMerekKategori(Long id, Long penawaran_id){

		Komoditas komo = Komoditas.findById(id);
		List<Manufaktur> manu = Manufaktur.findManufakturByKomoditas(id);
		List<ProdukKategori> dt = ProdukKategori.getByKomoditasId(id);

		List<PenawaranManufaktur> pm = PenawaranManufaktur.findByPenawaranDanKomoditasId(penawaran_id,id);

		renderArgs.put("pm",pm);
		renderArgs.put("ktg",dt);
		renderArgs.put("manu",manu);
		renderArgs.put("komoditas",komo);
		renderArgs.put("penawaran_id",penawaran_id);
		render();
	}

	public static void approveMerekKategory(Long id, Long penawaran_id){

		Komoditas komo = Komoditas.findById(id);
		List<Manufaktur> manu = Manufaktur.findManufakturByKomoditas(id);
		List<ProdukKategori> dt = ProdukKategori.getByKomoditasId(id);

		List<PenawaranManufaktur> pm = PenawaranManufaktur.findByPenawaranDanKomoditasId(penawaran_id,id);

		renderArgs.put("pm",pm);
		renderArgs.put("ktg",dt);
		renderArgs.put("manu",manu);
		renderArgs.put("komoditas",komo);
		renderArgs.put("penawaran_id",penawaran_id);
		render();
	}

	@AllowAccess({Acl.COM_PENAWARAN_ADD_MANUFAKTUR_PRODUK})
	public static void submitInputMerekKategori(Long komoditas_id, Long manufaktur_id, Long produk_kategori_id, Long penawaran_id){

		PenawaranManufaktur dt = PenawaranManufaktur.exist(produk_kategori_id, manufaktur_id, penawaran_id, komoditas_id);
		if( null != dt && dt.active==true){
			flash.error(Messages.get("duplikat_data"));
		}else if( null != dt && dt.active==false) {
			dt.active = true;
			dt.save();
		}else{
			PenawaranManufaktur pm = new PenawaranManufaktur();
			pm.komoditas_id = komoditas_id;
			pm.manufaktur_id = manufaktur_id;
			pm.produk_kategori_id = produk_kategori_id;
			pm.penawaran_id = penawaran_id;
			pm.active = true;
			pm.save();
		}
		inputMerekKategori(komoditas_id, penawaran_id);
	}

	@AllowAccess({Acl.COM_PENAWARAN_DELETE_MANUFAKTUR_PRODUK})
	public static void hapusPenawaranManufaktur(Long id){
		PenawaranManufaktur pm = PenawaranManufaktur.findById(id);
		pm.active =false;
		pm.save();
		inputMerekKategori(pm.komoditas_id, pm.penawaran_id);
	}
}
