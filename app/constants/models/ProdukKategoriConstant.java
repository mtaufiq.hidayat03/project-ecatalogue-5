package constants.models;

import constants.models.BaseConstant;

/**
 * this class contains static field related to the fields inside produk_kategori table*/
public class ProdukKategoriConstant extends BaseConstant {

	public static final String TABLE_NAME = "produk_kategori";

	public static final String ID = "id";
	public static final String KOMODITAS_ID = "komoditas_id";
	public static final String PARENT_ID = "parent_id";
	public static final String NAMA_KATEGORI = "nama_kategori";
	public static final String POSISI_ITEM = "posisi_item";
	public static final String LEVEL_KATEGORI = "level_kategori";

}
