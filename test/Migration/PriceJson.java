package Migration;

import java.math.BigDecimal;

public class PriceJson {
    public String Provinsi;
    public String Kabupaten;
    public BigDecimal HargaUtama;
    public BigDecimal HargaRetail;
    public BigDecimal OngkosKirim;
}
