package services.prakatalog;

import ext.FormatUtils;
import models.common.DokumenPenetapanProduk;
import models.katalog.Produk;
import models.katalog.ProdukHarga;
import models.prakatalog.ListHargaBeritaAcara;
import models.util.produk.ProdukHargaJson;

import java.util.List;

public class BeritaAcaraService {

    public static DokumenPenetapanProduk populateDataForDocument(DokumenPenetapanProduk dok, List<ProdukHarga> listDataSK){
        StringBuilder sb = new StringBuilder();
        String tableProdukHarga = generateListProdukHarga(listDataSK);
        sb.append(tableProdukHarga);
        dok.tblProdukHarga = sb.toString();
        return dok;
    }

    public static String generateListProdukHarga(List<ProdukHarga> listTable){
        StringBuilder sb = new StringBuilder();
        sb.append("<table class='table table-bordered' align='center' border='1' cellspacing='0' style='width:100%'>" +
                "<thead><tr>" +
                    "<th rowspan='2' style='width:5%;text-align:center'><b>No</b></th>" +
                    "<th rowspan='2' style='width:30%;text-align:center'><b>Nama<br/>Produk</b></th>" +
                    "<th rowspan='2' style='width:10%;text-align:center'><b>Wilayah Jual</b></th>" +
                    "<th rowspan='2' style='width:15%;text-align:center'><b>Harga Penawaran<br/>(Rp)</b></th>" +
                    "<th rowspan='2' style='width:15%;text-align:center'><b>Harga Negosiasi<br/>(Rp)</b></th>" +
                    "<th rowspan='2' style='width:20%;text-align:center'><b>Keterangan<br/>(Sepakat/Tidak Sepakat)</b></th>" +
                    "<th rowspan='2' style='width:20%;text-align:center'><b>Persentase Penurunan<br/>Harga(%)</b></th>" +
                "</tr></thead><tbody>");
        for(int i=0; i<listTable.size(); i++){
            sb.append("<tr>" +
                        "<td style='width:5%'><p style='text-align:center;margin:0;'>" + (i+1) +"</p></td>" +
                        "<td style='width:30%'><p style='text-align:left;margin:0;'>" + listTable.get(i).nama_produk + "<br/><br/>Merek : "+listTable.get(i).nama_manufaktur +"</p></td>" +
                            "<td style='width:10%'><p style='text-align:left;margin:0;'>" + listTable.get(i).wilayah_jual + "</p></td>" +
                            "<td style='width:15%'><p style='text-align:left;margin:0;'> </p></td>" +
                            "<td style='width:15%'><p style='text-align:right;margin:0;'>" + listTable.get(i).harga + "</p></td>" +
                            "<td style='width:20%'><p style='text-align:right;margin:0;'> </p></td>" +
                            "<td style='width:20%'><p style='text-align:right;margin:0;'> </p></td>" +
                    "</tr>");
        }
        sb.append("</tbody></table>");
        return sb.toString();
    }

    public static String generateListProdukHargaNasional(List<ProdukHargaJson> listTable){
        StringBuilder sb = new StringBuilder();
                sb.append("<table class='table table-bordered' align='center' border='1' cellspacing='0' style='width:100%'>" +
                        "<thead><tr>" +
                        "<th style='width:5%;text-align:center'><b>No</b></th>" +
                        "<th style='width:20%;text-align:center'><b>Nama Produk</b></th>" +
                        "<th style='width:10%;text-align:center'><b>Merk</b></th>" +
                        "<th style='width:10%;text-align:center'><b>Satuan</b></th>" +
                        "<td style='width:25%;text-align:center'><b>Harga Pemerintah (Rp)</b></th>" +
                        "</tr></thead><tbody>");
        for(int i=0; i<listTable.size(); i++){
            sb.append("<tr style='height:20px'>" +
                            "<td style='width:5%'><p style='text-align:center;margin:0;'>" + (i+1) +"</p></td>" +
                            "<td style='width:20%'><p style='text-align:left;margin:0;'>" + listTable.get(i).nama_produk + "</p></td>" +
                            "<td style='width:10%'><p style='text-align:left;margin:0;'>" + listTable.get(i).nama_manufaktur + "</p></td>" +
                            "<td style='width:10%'><p style='text-align:center;margin:0;'>" + listTable.get(i).unit_pengukuran + "</p></td>" +
                            "<td style='width:25%'><p style='text-align:right;margin:0;'>" + FormatUtils.formatDesimal2(listTable.get(i).harga) + "</p></td>" +
                            "</tr>");
        }
        sb.append("</tbody></table>");
        return sb.toString();
    }

    public static String generateListProdukHargaProvinsi(List<ListHargaBeritaAcara> listTable){
        StringBuilder sb = new StringBuilder();
        sb.append("<table class='table table-bordered' align='center' border='1' cellspacing='0' style='width:100%'>" +
                        "<thead><tr>" +
                        "<th style='width:5%;text-align:center'><b>No</b></th>" +
                        "<th style='width:20%;text-align:center'><b>Nama Produk</b></th>" +
                        "<th style='width:10%;text-align:center'><b>Merk</b></th>" +
                        "<th style='width:10%;text-align:center'><b>Satuan</b></th>" +
                        "<td style='width:25%;text-align:center'><b>Wilayah Jual</b></th>" +
                        "<td style='width:25%;text-align:center'><b>Harga Pemerintah (Rp)</b></th>" +
                        "</tr></thead><tbody>");
        for(int i=0; i<listTable.size(); i++){
            sb.append("<tr style='height:20px'>" +
                            "<td style='width:5%'><p style='text-align:center;margin:0;'>" + (i+1) +"</p></td>" +
                            "<td style='width:20%'><p style='text-align:left;margin:0;'>" + listTable.get(i).namaProduk + "</p></td>" +
                            "<td style='width:10%'><p style='text-align:left;margin:0;'>" + listTable.get(i).namaManufaktur + "</p></td>" +
                            "<td style='width:10%'><p style='text-align:center;margin:0;'>" + listTable.get(i).namaUnitPengukuran + "</p></td>" +
                            "<td style='width:15%'><p style='text-align:left;margin:0;'>" + listTable.get(i).namaProvinsi + "</p></td>" +
                            "<td style='width:25%'><p style='text-align:right;margin:0;'>" + FormatUtils.formatDesimal2(listTable.get(i).harga) + "</p></td>" +
                            "</tr>");
        }
        sb.append("</tbody></table>");
        return sb.toString();
    }

    public static String generateListProdukHargaKabupaten(List<ListHargaBeritaAcara> listTable){
        StringBuilder sb = new StringBuilder();
        sb.append("<table class='table table-bordered' align='center' border='1' cellspacing='0' style='width:100%'>" +
                        "<thead><tr>" +
                        "<th style='width:5%;text-align:center'><b>No</b></th>" +
                        "<th style='width:20%;text-align:center'><b>Nama Produk</b></th>" +
                        "<th style='width:10%;text-align:center'><b>Merk</b></th>" +
                        "<th style='width:10%;text-align:center'><b>Satuan</b></th>" +
                        "<td style='width:15%;text-align:center'><b>Wilayah Provinsi</b></th>" +
                        "<td style='width:25%;text-align:center'><b>Wilayah Jual</b></th>" +
                        "<td style='width:25%;text-align:center'><b>Harga Pemerintah (Rp)</b></th>" +
                        "</tr></thead><tbody>");
        for(int i=0; i<listTable.size(); i++){
            sb.append("<tr style='height:20px'>" +
                            "<td style='width:5%'><p style='text-align:center;margin:0;'>" + (i+1) +"</p></td>" +
                            "<td style='width:20%'><p style='text-align:left;margin:0;'>" + listTable.get(i).namaProduk + "</p></td>" +
                            "<td style='width:10%'><p style='text-align:left;margin:0;'>" + listTable.get(i).namaManufaktur + "</p></td>" +
                            "<td style='width:10%'><p style='text-align:center;margin:0;'>" + listTable.get(i).namaUnitPengukuran + "</p></td>" +
                            "<td style='width:15%'><p style='text-align:left;margin:0;'>" + listTable.get(i).namaProvinsi + "</p></td>" +
                            "<td style='width:25%'><p style='text-align:left;margin:0;'>" + listTable.get(i).namaKabupaten + "</p></td>" +
                            "<td style='width:25%'><p style='text-align:right;margin:0;'>" + FormatUtils.formatDesimal2(listTable.get(i).harga) + "</p></td>" +
                            "</tr>");
        }
        sb.append("</tbody></table>");
        return sb.toString();
    }

    public static String generateListProdukBAEvaluasiKlarifikasiTeknisSertaHarga(List<Produk> listTable){
        StringBuilder sb = new StringBuilder();
        sb.append("<table class='table table-bordered' align='center' border='1' cellspacing='0' style='width:100%'>" +
                "<thead><tr>" +
                "<th style='width:5%;text-align:center'><b>No</b></th>" +
                "<th style='width:20%;text-align:center'><b>Nama Produk</b></th>" +
                "<th style='width:15%;text-align:center'><b>Harga untuk pemerintah tidak lebih mahal atau sama dengan harga penawaran kepada non Pemerintah (Sesuai/Tidak Sesuai)</b> </th>" +
                "<th style='width:10%;text-align:center'><b>Analisa harga satuan (Ada/Tidak Ada)</b></th>" +
                "<td style='width:10%;text-align:center'><b>PIB (Ada/Tidak Ada)</b></th>" +
                "<td style='width:10%;text-align:center'><b>Invoice pembelian Bahan Baku (Ada/Tidak Ada)</b></th>" +
                "<td style='width:10%;text-align:center'><b>Invoice Pembelian Barang (Ada/Tidak Ada)</b></th>" +
                "<td style='width:10%;text-align:center'><b>Invoice Penjualan Barang (ApabilaAda) (Ada/Tidak Ada)</b></th>" +
                "<td style='width:10%;text-align:center'><b>Ket.(Lulus/Tidak Lulus)</b></th>" +
                "</tr></thead><tbody>");
        for(int i=0; i<listTable.size(); i++){
            sb.append("<tr style='height:20px'>" +
                            "<td style='width:5%'><p style='text-align:center;margin:0;'>" + (i+1) +"</p></td>" +
                            "<td style='width:20%'><p style='text-align:left;margin:0;'>" + listTable.get(i).nama_produk + "</p></td>" +
                            "<td style='width:15%'><p style='text-align:left;margin:0;'> </p></td>" +
                            "<td style='width:10%'><p style='text-align:center;margin:0;'> </p></td>" +
                            "<td style='width:10%'><p style='text-align:center;margin:0;'> </p></td>" +
                            "<td style='width:10%'><p style='text-align:center;margin:0;'> </p></td>" +
                            "<td style='width:10%'><p style='text-align:center;margin:0;'> </p></td>" +
                            "<td style='width:10%'><p style='text-align:center;margin:0;'> </p></td>" +
                            "<td style='width:10%'><p style='text-align:center;margin:0;'> </p></td>" +
                            "</tr>");
            }
            sb.append("</tbody></table>");
        return sb.toString();
    }

}
