package models.purchasing.pembayaran.form;

import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 11/1/2017
 */
public class SelectedItem {

    @SerializedName("id")
    public Long id;
    @SerializedName("total")
    public Double total;
    @SerializedName("packageId")
    public Long packageId;
    @SerializedName("invoiceDate")
    public String invoiceDate;
    @SerializedName("description")
    public String description;
    @SerializedName("acceptanceDate")
    public String acceptanceDate;

}
