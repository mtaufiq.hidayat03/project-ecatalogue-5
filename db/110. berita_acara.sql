/*
 Navicat Premium Data Transfer

 Source Server         : Database 151
 Source Server Type    : MySQL
 Source Server Version : 50723
 Source Host           : 10.1.30.151
 Source Database       : lkpp_katalog_portal_30052017

 Target Server Type    : MySQL
 Target Server Version : 50723
 File Encoding         : utf-8

 Date: 11/09/2018 17:14:18 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `berita_acara`
-- ----------------------------
DROP TABLE IF EXISTS `berita_acara`;
CREATE TABLE `berita_acara` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penawaran_id` int(11) DEFAULT NULL,
  `dokumen_template_id` int(11) DEFAULT NULL,
  `no_ba` varchar(100) DEFAULT NULL,
  `dokumen_ba` longtext,
  `nama_file` varchar(255) DEFAULT NULL,
  `blb_id` int(11) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `deleted_date` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `audittype` varchar(255) DEFAULT NULL,
  `auditupdate` timestamp NULL DEFAULT NULL,
  `audituser` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
