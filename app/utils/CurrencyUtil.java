package utils;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * Created by dadang on 11/15/17.
 */
public class CurrencyUtil {

	public static double formattedToDouble(String str) throws ParseException {
		NumberFormat format = NumberFormat.getInstance();
		Number number = format.parse(str);

		return number.doubleValue();
	}
}
