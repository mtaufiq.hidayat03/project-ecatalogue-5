package models.prakatalog;

import models.BaseKatalogTable;
import models.common.DokumenInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.io.File;

/**
 * Created by dadang on 7/21/17.
 */
@Table(name = "dokumen_usulan_pokja")
public class DokumenUsulanPokja extends BaseKatalogTable {

	@Id
	public Long id;

	public Long usulan_id;

	public String dok_judul;

	public String dok_label;

	public String dok_hash;

	public String dok_signature;

	public Long dok_id_attachment;

	public static DokumenInfo simpanDokUsulan(Long usulan_id, File file, String dok_label) throws Exception {

		DokumenUsulanPokja dokumenUsulanPokja = new DokumenUsulanPokja();
		//BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, dokumenUsulanPokja.dok_id_attachment);
		BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, dokumenUsulanPokja.dok_id_attachment, DokumenUsulanPokja.class.getSimpleName());
		dokumenUsulanPokja.dok_id_attachment = blob.id;
		dokumenUsulanPokja.dok_judul = blob.blb_nama_file;
		dokumenUsulanPokja.dok_hash = blob.blb_hash;
		dokumenUsulanPokja.dok_label = dok_label;
		if(usulan_id != null){
			dokumenUsulanPokja.usulan_id = usulan_id;
		}
		Long ids = new Long(dokumenUsulanPokja.save());

		DokumenInfo doki = new DokumenInfo(blob);
		doki.file_label = dokumenUsulanPokja.dok_label;
		if(null != dokumenUsulanPokja.id)
			doki.id = dokumenUsulanPokja.id;
		else
			doki.id = ids;
		return doki;
		//return DokumenInfo.findforDokumenUsulanPokja(blob);
	}

	//harap cek dari sumbernya, yang lama ngirim dok_id_attachment, kali yang baru kirim id
	public static DokumenUsulanPokja findByDokId(long dok_id){
		//dok_id_attachment
		return DokumenUsulanPokja.findById(dok_id);
		//return DokumenUsulanPokja.find("dok_id_attachment = ?", dok_id_attachment).first();
	}

	public static void updateIdLelangDokUsulan(long id, long usulan_id){
		//String sql = "update dokumen_usulan set usulan_id = :usulan_id where dok_id_attachment = :id";
		String sql = "update dokumen_usulan_pokja set usulan_id = :usulan_id where id = :id";
		Query.update(sql, usulan_id, id);
	}

	public DokumenInfo getAsdocInfo(){
		DokumenInfo info = new DokumenInfo();
		BlobTable blob = BlobTable.find("id = ?",dok_id_attachment).first();
		if(blob!=null){
			info.id = blob.id;
			info.blb_id_content = blob.blb_id_content;
			info.file_label = dok_label;
			info.name = dok_judul;
			info.download_url = blob.getDownloadUrl(null);
		}

		return info;
	}
}
