package models.api.produk;

import java.util.List;

/**
 * Created by dadang on 1/5/18.
 */
public class ProdukApiInfo {

	public Integer total;
	public Integer current_page;
	public Integer per_page;
	public Integer total_page;
	public List<Produk> produk;
	
}
