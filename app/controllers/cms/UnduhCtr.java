package controllers.cms;

import controllers.BaseController;
import controllers.security.AllowAccess;
import ext.DatetimeBinder;
import models.cms.KategoriKonten;
import models.cms.Unduh;
import models.secman.Acl;
import org.apache.commons.io.FilenameUtils;
import play.Play;
import play.data.binding.As;
import play.i18n.Messages;
import play.libs.Files;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static models.cms.KategoriKonten.*;

public class UnduhCtr extends BaseController {

    @AllowAccess({Acl.COM_PUSTAKA_FILE})
    public static void index(){
        List<KategoriKonten> kategoriKontenList = findAllByKonten(4);
        render(kategoriKontenList);
    }

    @AllowAccess({Acl.COM_PUSTAKA_FILE_ADD})
    public static void create(){
        List<KategoriKonten> KategoriUnduhList = findAllByKonten(4);
        render("cms/UnduhCtr/create.html",KategoriUnduhList);
    }

    @AllowAccess({Acl.COM_PUSTAKA_FILE_DELETE})
    public static void delete(long id) {
        Unduh.deleteById(id);
        flash.success(Messages.get("notif.success.delete"));
        index();
    }

    public static String getRandom(){
        String SALTCHARS = "1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 14) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    @AllowAccess({Acl.COM_PUSTAKA_FILE_ADD})
    public static void createSubmit(String deskripsi, long kategori,File file,
                                    @As(binder= DatetimeBinder.class) Date publish_date_from,
                                    @As(binder=DatetimeBinder.class) Date publish_date_to) {

        Unduh unduh = new Unduh();
        if(file != null) {
            String generalPath = Play.applicationPath + "/public/files/pustaka_file/";
            File directory = new File(generalPath+kategori);
            if (!directory.exists()) {
                directory.mkdir();
            }
            String ext = FilenameUtils.getExtension(file.getName());
            String nama_file = getRandom()+"."+ext;
            File pathFile = new File(generalPath + kategori +"/"+nama_file);
            Files.copy(file,pathFile);

            unduh.file_name = nama_file;
            unduh.original_file_name = file.getName();
            unduh.file_size = file.length();
            unduh.file_sub_location = "/"+kategori;
        }
        unduh.konten_kategori_id = kategori;
        unduh.deskripsi = deskripsi;
        unduh.publish_date_from = publish_date_from;
        unduh.publish_date_to = publish_date_to;
        unduh.posisi_file = 1;
        unduh.status = "draft";
        unduh.active = true;
        long id = unduh.save();
        flash.success(Messages.get("notif.success.draft"));
        edit(id);
    }

    @AllowAccess({Acl.COM_PUSTAKA_FILE_EDIT})
    public static void edit(long id) {
        Unduh viewunduh = Unduh.findById(id);
        String date_from;
        String date_to;

        SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        List<KategoriKonten> KategoriUnduhList = findAllByKonten(4);
        date_from = dateformat.format(viewunduh.publish_date_from);

        if(viewunduh.publish_date_to!=null) {
            date_to = dateformat.format(viewunduh.publish_date_to);
        }else{
            date_to = null;
        }

        render("cms/UnduhCtr/edit.html",viewunduh,date_from,date_to,KategoriUnduhList);
    }

    @AllowAccess({Acl.COM_PUSTAKA_FILE_EDIT})
    public static void editSubmit(long id, long kategori, String status,
                                  File file, String deskripsi,
                                  @As(binder= DatetimeBinder.class) Date publish_date_from,
                                  @As(binder= DatetimeBinder.class) Date publish_date_to) {

        Unduh unduh = Unduh.findById(id);

        String generalPath = Play.applicationPath + "/public/files/pustaka_file";
        File directory = new File(generalPath+"/"+kategori);
        if (!directory.exists()) {
            directory.mkdir();
        }

        if(file != null) {
            String ext = FilenameUtils.getExtension(file.getName());
            String nama_file = getRandom()+"."+ext;
            File pathFile = new File(generalPath+"/" + kategori +"/"+nama_file);
            Files.delete(new File(generalPath+unduh.file_sub_location+"/"+unduh.file_name));
            Files.copy(file,pathFile);

            unduh.file_name = nama_file;
            unduh.original_file_name = file.getName();
            unduh.file_size = file.length();
        }
        unduh.file_sub_location = "/"+kategori;
        unduh.konten_kategori_id = kategori;
        unduh.deskripsi = deskripsi;
        unduh.publish_date_from = publish_date_from;
        unduh.publish_date_to = publish_date_to;
        unduh.posisi_file = 1;
        unduh.active = true;
        unduh.status = status;
        unduh.save();
        flash.success(Messages.get("notif.success.update"));
        edit(id);
    }
}
