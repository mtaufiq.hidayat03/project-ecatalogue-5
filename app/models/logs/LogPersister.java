package models.logs;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.lang.time.StopWatch;

import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;

@NoTransaction
public class LogPersister extends Job {

	private static Queue<ActivityLog> queue=new LinkedList<>();
	private static AtomicBoolean isRunning=new AtomicBoolean(false);
	
	
	public static void add(ActivityLog log) {
		//simpan log ini secara asynchronous, tunggu beberapa saat
		queue.add(log);
		if(!isRunning.getAndSet(true))
		{
			new LogPersister().in(5);
		}
	}
	
	public void doJob()
	{
		int countProcessed=0;
		ActivityLog log;
		while((log=queue.poll())!=null)
		{
			log.save();
			countProcessed++;
		}
		isRunning.set(false);
		Logger.debug("[LogPerster], counts: %,d", countProcessed);
	}

}
