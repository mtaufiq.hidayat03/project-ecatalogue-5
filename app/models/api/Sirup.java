package models.api;

import models.api.sirup.PaketAnggaran;
import models.jcommon.util.CommonUtil;
import play.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by dadang on 12/12/17.
 */
public class Sirup {
	public Long id;
	public String nama;
	public String kegiatan;
	public Integer jenisPengadaan;
	public Integer jenisBelanja;
	public String volume;
	public Integer metodePengadaan;
	public String tanggalAwalPengadaan;
	public String tanggalAkhirPengadaan;
	public String tanggalAwalPekerjaan;
	public String tanggalAkhirPekerjaan;
	public String lokasi;
	public String keterangan;
	public Integer tahunAnggaran;
	public String kodeAnggaran;
	public Long idSatker;
	public String kodeKldi;
	public boolean aktif;
	public boolean is_delete;
	public Date tanggalPengumuman;
	public Date auditupdate;
	public String sumberDanaString;
	public Double jumlahPagu;
	public String spesifikasi;
	public Long idProgram;
	public Long idKegiatan;

	public List<PaketAnggaran> listPaketAnggaranM;

	public Date getTanggalAwalPengadaan(){
		if(!CommonUtil.isEmpty(tanggalAwalPengadaan)){
			return convertDate(this.tanggalAwalPengadaan);
		}
		return null;
	}
	public Date getTanggalAkhirPengadaan(){
		if(!CommonUtil.isEmpty(tanggalAkhirPengadaan)){
			return convertDate(this.tanggalAkhirPengadaan);
		}
		return null;
	}
	public Date getTanggalAwalPekerjaan(){
		if(!CommonUtil.isEmpty(tanggalAwalPekerjaan)){
			return convertDate(this.tanggalAwalPekerjaan);
		}
		return null;
	}
	public Date getTanggalAkhirPekerjaan(){
		if(!CommonUtil.isEmpty(tanggalAkhirPekerjaan)){
			return convertDate(this.tanggalAkhirPekerjaan);
		}
		return null;
	}

	public Rup getAsRup(){
		try{
			Rup rup = new Rup();

			rup.setRupFromSirup(this);
			return rup;
		}catch (Exception e){
			Logger.error("error parse Sirup to rup"+ e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public static java.util.Date convertDate(String date){
		SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");
		try {
			if(date != null){
				return sdf.parse(date);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

}
