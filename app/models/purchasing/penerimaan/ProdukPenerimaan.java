package models.purchasing.penerimaan;

import models.purchasing.PaketProduk;
import models.purchasing.base.BaseProductForm;
import models.purchasing.base.BaseProduk;
import play.db.jdbc.Table;

/**
 * @author HanusaCloud on 10/23/2017
 */
@Table(name = "paket_penerimaan_produk")
public class ProdukPenerimaan extends BaseProduk {

    public Long paket_penerimaan_id;

    public ProdukPenerimaan() {
        super();
    }

    public ProdukPenerimaan dataForm(BaseProductForm model) {
        this.produk_id = model.productId;
        this.kuantitas = model.productQuantity;
        this.deskripsi = model.productNote;
        return this;
    }

    public ProdukPenerimaan penerimaan(PaketRiwayatPenerimaan model) {
        this.paket_penerimaan_id = model.id;
        this.paket_id = model.paket_id;
        return this;
    }

    public ProdukPenerimaan paketProduk(PaketProduk model) {
        this.paket_produk_id = model.id;
        return this;
    }

    public ProdukPenerimaan saveProduct() {
        final long result = (long) save();
        if (this.id == null) {
            this.id = result;
        }
        return this;
    }

}
