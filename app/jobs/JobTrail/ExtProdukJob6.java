package jobs.JobTrail;

import jobs.ExtProduct.ExtProduct;
import jobs.ExtProduct.PenyediaKomoditasApi;
import models.api.produk.HistoryDatafeed;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;

import java.util.UUID;

@NoTransaction
public class ExtProdukJob6 extends Job {
    UUID id = UUID.randomUUID();
    int _defaultWaitime = 0;
    PenyediaKomoditasApi _pk = new PenyediaKomoditasApi();
    int _nextWaitime = 0;
    HistoryDatafeed _hdf = new HistoryDatafeed();
    public ExtProdukJob6 (int defaultWaitime, PenyediaKomoditasApi pk, int nextWaitime, HistoryDatafeed hdf){
        _defaultWaitime = defaultWaitime;
        _pk = pk;
        _nextWaitime = nextWaitime;
        _hdf = hdf;
    }

    @Override
    public void doJob() {

        ExtProduct ext = new ExtProduct();
        ext.penyediaKomoditasApi = _pk;
        ext.defaultWaitime = _defaultWaitime;
        ext.nextWaitime = _nextWaitime;
        ext.historyDatafeed = _hdf;
        //ext.urlForExecutejob = _pk.produk_api_url;
        ext.getDatas(ext.defaultWaitime,_pk.produk_api_url, ext.historyDatafeed);
    }

    public void before(){
        Logger.info("running job id:"+id+", 1st page, penyedia:"+_pk.nama_penyedia);

    }
    @Override
    public void after() {
        //after finish send mail
        Logger.info(_pk.nama_penyedia+" job is done, reporting job id:"+id);

    }
}
