package StressWs;

import java.util.ArrayList;
import java.util.List;

public class StresTest2 {
String nama;
public StresTest2(String _nama){
    nama = _nama;
}
    public void run(){
        List<Thread> lt = new ArrayList<>();

        for (int i = 0; i <= 1000; i++){
            lt.add(new Thread(new ThreadJava("Thread "+ nama+" ws ke "+i, 1000)));
            lt.get(i).start();
        }

        System.out.println("TOTAL Thread execute for :"+nama+" is:"+lt.size());
        lt = null;
    }
}
