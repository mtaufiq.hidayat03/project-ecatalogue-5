package GrabData;

import java.util.List;

import org.junit.Test;

import jobs.wilayah.ExtKabupatenData;
import jobs.wilayah.ExtPropinsiData;
import models.api.DceKab;
import models.api.DcePropinsi;
import play.test.UnitTest;

public class FetchWilayahTest extends UnitTest {
	//@Test
	public void testFetchPropinsi() {
		final ExtPropinsiData extPropinsiData = new ExtPropinsiData();
		final List<DcePropinsi> result = extPropinsiData.getDceProvinsiData();
		assertFalse(result.isEmpty());
	}

	//@Test
	public void testFetchKabupaten() {
		final ExtKabupatenData extKabupatenData = new ExtKabupatenData(12L);
		final List<DceKab> result = extKabupatenData.getKabupatenData();
		assertFalse(result.isEmpty());
	}
}
