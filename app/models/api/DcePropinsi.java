package models.api;

import models.jcommon.util.CommonUtil;
import models.masterdata.DceProvinsi;
import models.masterdata.Provinsi;
import play.Logger;
import utils.KatalogUtils;

import java.util.Date;
import java.util.List;

/**
 * Created by reza on 27/08/18
 */
public class DcePropinsi {
	public Long prp_id;
	public String prp_nama;
	public List<DceKab> kabupatenList;


	public Provinsi getAsProvinsi(){
		try{
			Provinsi provinsi = new Provinsi();
			provinsi.id=this.prp_id;
			provinsi.nama_provinsi=this.prp_nama;
			provinsi.active=true;
			return provinsi;
		}catch (Exception e){
			Logger.error("error parse DcePropinsi to provinsi"+ e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public DceProvinsi getAsDceProvinsi(){
		try{
			DceProvinsi provinsi = new DceProvinsi();
			provinsi.id=this.prp_id;
			provinsi.nama=this.prp_nama;
			return provinsi;
		}catch (Exception e){
			Logger.error("error parse DcePropinsi to provinsi"+ e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

}
