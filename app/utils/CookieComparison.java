package utils;

import models.common.Comparison;
import play.Logger;
import play.mvc.Http;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 10/14/2017
 */
public class CookieComparison {

    public static List<Comparison> get() {
        List<Comparison> comparisons = new ArrayList<>();
        try {
            Http.Cookie cookie = Http.Request.current().cookies.get("comparisons");
            if(cookie != null){
                String cookieValue = URLDecoder.decode(cookie.value, "UTF-8");
                String[] cookiesList = cookieValue.split(";");
                for(String cl : cookiesList){
                    String[] obj = cl.split("\\|");
                    if(obj.length == 3){
                        Comparison comparison = new Comparison();
                        comparison.id = new Long(obj[0]);
                        comparison.location_id = new Long(obj[1]);
                        comparison.name = obj[2];
                        comparisons.add(comparison);
                    }
                }
            }
        } catch (UnsupportedEncodingException e) {
            Logger.error(CookieComparison.class.getName()+" error:"+e.getMessage());
            e.printStackTrace();
        }
        return comparisons;
    }

    public static Set<Long> getKeys() {
        List<Comparison> comparisons = get();
        if (!comparisons.isEmpty()) {
            return Collections.emptySet();
        }
        return get().stream().map(Comparison::getId).collect(Collectors.toSet());
    }

}
