package jobs.JobTrail;

import jobs.extdata.SirupData;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import java.util.Date;

@NoTransaction
public class JobSirupData4 extends Job {
    int minuteWait = 0;
    Date currentDate = new Date();
    SirupData sd = new SirupData();
    public JobSirupData4(int _minuteWait, Date _currentDate, SirupData _sd){
        minuteWait = _minuteWait;
        currentDate = _currentDate;
        sd = _sd;
    }
    public void doJob(){
        int newWait = minuteWait++;
        sd.getData(newWait,currentDate);
    }
}
