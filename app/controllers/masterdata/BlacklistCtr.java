package controllers.masterdata;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import controllers.BaseController;
import controllers.security.AllowAccess;
import jobs.blacklist.JobBlacklist;
import models.jcommon.util.CommonUtil;
import models.blacklist.Blacklist;
import models.secman.Acl;
import play.i18n.Messages;

import java.util.*;

public class BlacklistCtr extends BaseController {

	@AllowAccess({Acl.COM_BLACKLIST})
	public static void index() {
		render();
	}

	@AllowAccess({Acl.COM_BLACKLIST})
	public static void manualFetch() {
		JobBlacklist job = new JobBlacklist();
		job.doJob();
		index();
	}

}
