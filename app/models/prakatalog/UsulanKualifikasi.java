package models.prakatalog;

import models.BaseKatalogTable;
import models.prakatalog.form.FormBukaPenawaran;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.LogUtil;

import java.util.List;

/**
 * Created by raihaniqbal on 8/3/17.
 */
@Table(name = "usulan_kualifikasi")
public class UsulanKualifikasi extends BaseKatalogTable {

	public static final String TAG = "UsulanKualifikasi";

	@Id
	public Long id;
	public Long usulan_id;
	public String jenis_kualifikasi;
	public String jenis_kualifikasi_alias;
	public boolean active;

	public static void simpanUsulanKualifikasi(FormBukaPenawaran form) {
		UsulanKualifikasi.delete("usulan_id = ?", form.usulan_id);
		if (form.kualifikasi_sikap == null || form.kualifikasi_sikap.length == 0) {
			LogUtil.debug(TAG, "kualifikasi_sikap is empty");
			return;
		}
		for (int i = 0; i < form.kualifikasi_sikap.length; i++) {

			UsulanKualifikasi uk = new UsulanKualifikasi();
			uk.usulan_id = form.usulan_id;
			uk.jenis_kualifikasi = UsulanKualifikasi.generateFromAlias(form.kualifikasi_sikap[i]);
			uk.jenis_kualifikasi_alias = form.kualifikasi_sikap[i];
			uk.active = true;

			uk.save();
		}
	}

	public static List<UsulanKualifikasi> findBy(Long usulanId) {
		return UsulanKualifikasi.find("usulan_id = ? AND active = 1", usulanId).fetch();
	}

	public static String generateFromAlias(String jenis_kualifikasi){
		return jenis_kualifikasi.toLowerCase().replace(" ","_");
	}

	public static List<UsulanKualifikasi> findJenisKualifikasi(){
		String query = "SELECT DISTINCT jenis_kualifikasi, jenis_kualifikasi_alias FROM usulan_kualifikasi";
		return Query.find(query,UsulanKualifikasi.class).fetch();
	}
}
