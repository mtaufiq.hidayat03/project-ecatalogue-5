create PROCEDURE updateJumlahProdukTerjualKomoditas()
    BEGIN
        update komoditas k set k.agr_jumlah_produk_terjual = (select sum(p.agr_total_dibeli) from produk p where komoditas_id = k.id);
    END;


create PROCEDURE updateJumlahProdukTerjual()
    BEGIN
        update produk prod set prod.agr_total_dibeli = (
            select sum(pp.kuantitas) from paket_produk pp join paket pak on pp.paket_id = pak.id
            where pp.produk_id = prod.id and pak.apakah_selesai = 1 and pak.apakah_batal = 0
        ) where (prod.harga_utama != null or prod.harga_utama != 0) and prod.active = 1 and prod.apakah_ditayangkan = 1 and prod.minta_disetujui = 0
          and berlaku_sampai < curdate() and prod.setuju_tolak = 'setuju';
    END;

create EVENT updateProdukAndKomoditas ON SCHEDULE EVERY 1 DAY STARTS '2017-09-20 23:59.00'
    DO BEGIN
        CALL updateJumlahProdukTerjual();
        CALL updateJumlahProdukTerjualKomoditas();
    END;