package models.masterdata;

import models.BaseKatalogTable;
import play.Logger;
import play.cache.Cache;
import play.cache.CacheFor;
import play.db.jdbc.CacheMerdeka;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by dadang on 8/7/17.
 */

@Table(name = "provinsi")
public class Provinsi extends BaseKatalogTable {

	@Id
	public Long id;

	public String nama_provinsi;

	public Long kldi_id;
	public Long dce_id;

	public boolean active;

	@Transient
	public List<Kabupaten> kabupatens;


	public Long getId() {return id;}
	public String getNama() {return nama_provinsi;}

	@Transient
	private static String key1 = "list-git -findAllActive";
	public static List<Provinsi> findAllActive(){

		List<Provinsi> provinsiList = Cache.get(key1, List.class);
		if(null == provinsiList){
			provinsiList = Provinsi.find("active = 1 order by nama_provinsi").fetch();
			if (!provinsiList.isEmpty()) {
				for (Provinsi p : provinsiList) {
					p.kabupatens = Kabupaten.findByProvId(p.id);
				}
			}
			Logger.info("setCACHE PROVINSI");
			Cache.set(key1,provinsiList,"30mn");
		}

		return provinsiList;
	}

	@CacheMerdeka
	public static Map<Long, String> findAllActiveMap(){
		List<Provinsi> provinsiList = Provinsi.find("active = 1").fetch();
		Map<Long, String> provinsis = provinsiList.stream()
				.collect(Collectors.toMap(obj -> obj.id, obj -> obj.nama_provinsi));

		return provinsis;
	}

	public static Provinsi findByName(String name){
		return find("nama_provinsi = ? and active = ?",name, AKTIF).first();
	}

	public static List<Provinsi> findByIds(List<String> ids){
		return Provinsi.find("id in (" + String.join(",", ids) + ")").fetch();
	}

	public void setAktif() {
		this.active = true;
		prePersist();
		save();
	}

	public void setNonAktif() {
		this.active = false;
		prePersist();
		save();
	}

	public static void setCache(String key, Provinsi obj, String duration){
		Cache.set(key,obj,duration);
	}

}
