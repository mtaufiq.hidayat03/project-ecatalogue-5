package models.purchasing.pengiriman.form;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import models.purchasing.pengiriman.PaketRiwayatPengiriman;

/**
 * @author HanusaCloud on 11/3/2017
 */
public class DeliveryItemJson {

    @SerializedName("id")
    public Long id;
    @SerializedName("documentDate")
    public String documentDate;
    @SerializedName("systemDocumentNumber")
    public String systemDocumentNumber;
    @SerializedName("documentNumber")
    public String documentNumber;
    @SerializedName("detailUrl")
    public String detailUrl;
    @SerializedName("total")
    public Long total;
    @SerializedName("deskripsi")
    public String deskripsi;

    public DeliveryItemJson(PaketRiwayatPengiriman model) {
        this.id = model.id;
        this.detailUrl = model.getDetailUrl();
        this.total = model.agr_paket_pengiriman_produk;
        this.systemDocumentNumber = model.getFormattedSystemDocNumber();
        this.documentDate = model.getReadableDocDate();
        this.documentNumber = model.getDocNumber();
        this.deskripsi = model.deskripsi;
    }

    public JsonObject getJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", this.id);
        jsonObject.addProperty("documentDate", this.documentDate);
        jsonObject.addProperty("systemDocumentNumber", this.systemDocumentNumber);
        jsonObject.addProperty("documentNumber", this.documentNumber);
        jsonObject.addProperty("detailUrl", this.detailUrl);
        jsonObject.addProperty("total", this.total);
        jsonObject.addProperty("deskripsi", this.deskripsi);
        return jsonObject;
    }

    public String getModelId() {
        return String.valueOf(id);
    }

}
