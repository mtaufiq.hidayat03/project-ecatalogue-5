package models.purchasing.contracts;

import models.katalog.Komoditas;
import models.penyedia.Penyedia;
import models.penyedia.PenyediaDistributor;
import models.purchasing.status.PaketStatus;
import models.user.User;
import repositories.paket.PackageContractRepository;

/**
 * @author HanusaCloud on 11/2/2017
 */
public interface BaseContract {

    public Long getId();

    Long getProviderId();

    Long getDistributorId();

    Long getPpId();

    Long getPpkId();

    User getUserPp();

    Integer getCommodityId();

    PaketStatus getStatus();

    Komoditas getCommodity();

    Penyedia getProvider();

    Integer getTotalFullyAccepted();

    Integer getTotalContractUploaded();

    Long getBuyerId();

    PenyediaDistributor getDistributor();

    User getBuyer();

    User getCommittee();

    String getRejectedReason();

    void setDistributor(PenyediaDistributor model);

    boolean isProductFullyAccepted();

    void setCommodity(Komoditas model);

    void setLatestStatus(PaketStatus model);

    void setProvider(Penyedia model);

    void setBuyer(User model);

    void setPp(User model);

    void setTotalContractUploaded(Integer value);

    default void withCommodity() {
        if (getCommodityId() != null) {
            setCommodity(Komoditas.findById(getCommodityId()));
        }
    }

    default void withLatestStatus() {
        if (getId() != null) {
            PaketStatus model = PaketStatus.getLatestStatusByPackageId(getId());
            if (model != null) {
                model.withLatestNegotiation();
            }
            setLatestStatus(model);
        }
    }

    default void withProvider(boolean includeDistributor) {
        if (getProviderId() != null) {
            Penyedia model = Penyedia.getUserProviderById(getProviderId());
            setProvider(model);
            if (model != null && includeDistributor) {
                model.withDistributors();
            }
        }
    }

    default void withDistributor() {
        if (getDistributorId() != null) {
            setDistributor(PenyediaDistributor.findById(getDistributorId()));
        }
    }

    default void withPpk() {
        if (getBuyerId() != null) {
            setBuyer(User.findById(getBuyerId()));
        }
    }

    default void withPp() {
        if (getPpId() != null) {
            setPp(User.findById(getPpId()));
        }
    }


    default void withTotalContractUploaded() {
        if (getId() != null) {
            setTotalContractUploaded(PackageContractRepository.getTotalContract(getId()));
        }
    }

}
