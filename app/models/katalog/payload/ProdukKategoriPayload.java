package models.katalog.payload;

import models.katalog.ProdukKategori;

public class ProdukKategoriPayload {

	public long id;
	public long kid;
	public String name;
	public String description;
	public long parent;

	public ProdukKategoriPayload() {}

	public ProdukKategoriPayload(long kid) {
		this.kid = kid;
	}

	public ProdukKategoriPayload(ProdukKategori model) {
		this.id = model.id;
		this.kid = model.komoditas_id;
		this.name = model.nama_kategori;
		this.description = model.deskripsi;
		this.parent = model.parent_id;
	}

}
