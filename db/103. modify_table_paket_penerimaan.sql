describe paket_penerimaan;
ALTER TABLE paket_penerimaan
  MODIFY COLUMN agr_total_harga_diterima_dan_ongkir
  decimal(20, 5) NULL DEFAULT NULL ;