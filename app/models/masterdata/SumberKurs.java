package models.masterdata;

import models.BaseKatalogTable;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.sql.Timestamp;
import java.util.List;

@Table(name = "kurs_sumber")
public class SumberKurs extends BaseKatalogTable {

    @Id
    public Long id;
    public String sumber;
    public String url;
    public String deskripsi;
    public boolean active;

    public static final long sumber_utama = 1;

    public static void deleteById(long id){
        SumberKurs sumberKurs = SumberKurs.findById(id);
        sumberKurs.active = false;
        sumberKurs.save();
    }

    public static SumberKurs findByNamaSumberKurs(String namaSumberKurs){
        return find("sumber = ?", namaSumberKurs).first();
    }

    public static SumberKurs findSumberKursUtama(){
        return find("id = ?", sumber_utama).first();
    }

    public void softDelete() {
        this.active = false;
        setDeleted();
        save();
    }

	public void setAktif() {
		this.active = true;
		prePersist();
		save();
	}

	public void setNonAktif() {
		this.active = false;
		prePersist();
		save();
	}



}