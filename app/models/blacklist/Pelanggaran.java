package models.blacklist;

import java.util.Date;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

/**
 * @author Reza
 */
@Table(name = "blacklist_pelanggaran")
public class Pelanggaran extends BaseTable{

	@Id
	public Long id;
	public Long blacklist_id;
	public String sk;
	public String jenis_pelanggaran;
	public String deskripsi_pelanggaran;
	
	public Date started_at;
	public Date expired_at;
	public Date created_at;
	public Date published_at;

}
