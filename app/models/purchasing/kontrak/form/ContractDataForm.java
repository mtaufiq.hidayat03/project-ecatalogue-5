package models.purchasing.kontrak.form;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.util.TextUtils;
import utils.DateTimeUtils;

import java.io.File;
import java.util.Date;

/**
 * @author HanusaCloud on 11/6/2017
 */
public class ContractDataForm {

    public Long packageId;
    public String contractDate;
    public String contractNumber;
    public String contractValue;
    public String note;
    public File attachment;
    public String dokumen_kontrak;

    public String getContractNumber() {
        return StringEscapeUtils.escapeSql(contractNumber);
    }

    public String getNote() {
        return StringEscapeUtils.escapeSql(note);
    }

    public Date parseContractDate() {
        return DateTimeUtils.parseDdMmYyyy(contractDate);
    }

    public Double getContractValueDouble() {
        if (!TextUtils.isEmpty(contractValue)) {
            return Double.parseDouble(contractValue.replaceAll("[^0-9]",""));
        }
        return 0D;
    }
}
