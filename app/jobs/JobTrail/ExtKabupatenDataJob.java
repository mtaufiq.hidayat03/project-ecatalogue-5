package jobs.JobTrail;

import jobs.wilayah.ExtKabupatenData;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;

import java.util.UUID;

@NoTransaction
public class ExtKabupatenDataJob extends Job {

    String uniqueID = UUID.randomUUID().toString();
    int nextWaitTime;
    ExtKabupatenData ext;
    public ExtKabupatenDataJob(ExtKabupatenData _ext, int _nextWaitTime){
        ext = _ext;
        nextWaitTime = _nextWaitTime;
    }
    @Override
    public void before() {
        Logger.info("job id:" + uniqueID);
        super.before();
    }

    public void doJob() {
        Logger.info("on job execute, waittime:" + nextWaitTime);
        ext.getKabupatenData(nextWaitTime);
        Logger.info("%%%%done getDate, saving now%%%");
        ext.saveDataKabupatenToDb();
        Logger.info("done job:" + ext.kabupatenList.size());
    }
}
