package controllers.prakatalog;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import controllers.BaseController;
import controllers.security.AllowAccess;
import jobs.elasticsearch.ElasticsearchLog;
import models.common.AktifUser;
import models.katalog.Komoditas;
import models.katalog.Produk;
import models.katalog.ProdukHarga;
import models.katalog.ProdukTungguSetuju;
import models.penyedia.Penyedia;
import models.prakatalog.Penawaran;
import models.prakatalog.Usulan;
import models.product.search.ProductApprovalSearch;
import models.product.search.ProductApprovalSearchResult;
import models.secman.Acl;
import org.apache.http.util.TextUtils;
import org.sql2o.Sql2oException;
import repositories.produk.ProductApprovalRepository;
import services.prakatalog.UsulanService;
import services.produk.ProdukService;
import utils.LogUtil;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class PersetujuanCtr extends BaseController {

	public static final String TAG = "PersetujuanCtr";

	@AllowAccess({Acl.COM_PENAWARAN_PERSETUJUAN_PRODUK})
	public static void persetujuanProduk(Long id) throws ParseException {
		ProductApprovalSearch query = new ProductApprovalSearch(params);
		Penawaran penawaran = Penawaran.findById(id);
		List<Komoditas> komoditas = new ArrayList<>();
		List<Penyedia> penyedia = new ArrayList<>();

		AktifUser aktifUser = getAktifUser();
		query.uid = aktifUser.user_id;

		komoditas = Komoditas.getAllKomoditasPersetujuanProdukByPokjaId(query.uid);

		if (id == null) {
			query.pid = 0L;
		} else {
			query.pid = id;
			query.komoditas = penawaran.komoditas_id;
		}

		if (query.komoditas != 0) {
			penyedia = Penyedia.getAllPenyediaPersetujuanProdukByPokjaId(query.uid, query.komoditas);
			if (query.pid != 0) {
				query.penyedia = penawaran.penyedia_id.toString();
			}
		}

		if (query.urutkan == "") {
			query.urutkan = "7";
		}

		ProductApprovalSearchResult model = new ProductApprovalSearchResult(query);
		if (params.all().size() > 1) {
			model = ProductApprovalRepository.searchProductApproval(query);
		}
		if (penawaran != null && penawaran.usulan_id != null) {
			Usulan usulan = Usulan.findById(penawaran.usulan_id);
			LogUtil.debug(TAG, "usulan id: " + usulan.id);
			if (usulan != null && penawaran.isNegotiation()) {
				renderArgs.put("allowApproval", UsulanService.isNegotiation(usulan));
			}
		} else {
			renderArgs.put("allowApproval", true);
		}
		Boolean isNegotationOrLolosKualifikasi = false;
		if (penawaran != null && penawaran.usulan_id != null) {
			isNegotationOrLolosKualifikasi = !TextUtils.isEmpty(penawaran.status)
					&& (penawaran.status.equals(Penawaran.STATUS_NEGOSIASI) || penawaran.status.equals(Penawaran.STATUS_LOLOS_KUALIFIKASI));
		}
		renderArgs.put("isNegotationOrLolosKualifikasi", isNegotationOrLolosKualifikasi);
		renderArgs.put("paramQuery", query);
		render("prakatalog/PersetujuanCtr/persetujuanProduk.html", model, komoditas, penyedia);
	}

	@AllowAccess({Acl.COM_PENAWARAN_PERSETUJUAN_PRODUK})
	public static void persetujuanProdukSubmit(String alasan, long[] idProduk, String setujuTolak, Long penawaran_id) throws ParseException {
		//TODO : Pengecekan yang berwenang
		LogUtil.d("Approval", idProduk);
		AktifUser aktifUser = getAktifUser();
		String aksi = "produk_riwayat_permintaan_tayang";
		// update status harga
		for(long id : idProduk){
			LogUtil.d("idProduk",id);
			try{
				Produk produk = Produk.findById(id);

				if(produk.penawaran_id != null){

					Penawaran penawaran = Penawaran.findById(produk.penawaran_id);

					if (penawaran.status.equalsIgnoreCase(Penawaran.STATUS_NEGOSIASI) || penawaran.status.equalsIgnoreCase(Penawaran.STATUS_LOLOS_KUALIFIKASI) || penawaran.status.equalsIgnoreCase(Penawaran.STATUS_SELESAI)){


						if(setujuTolak.equalsIgnoreCase("tolak") && alasan != null){
							ProdukHarga.tolakHargaProduk(id, alasan);
							produk.rejectProduct(id, alasan);
							if (!penawaran.status.equalsIgnoreCase(Penawaran.STATUS_NEGOSIASI)){
								if(!penawaran.status.equalsIgnoreCase(Penawaran.STATUS_SELESAI)){
									penawaran.changeStatus(Penawaran.STATUS_NEGOSIASI);
								}
							}
						} else if(setujuTolak.equalsIgnoreCase("setuju")){
							ProdukHarga.setujuHargaProduk(id);
							produk.approveProduct(id, alasan);
							if (!penawaran.status.equalsIgnoreCase(Penawaran.STATUS_NEGOSIASI)){
								if(!penawaran.status.equalsIgnoreCase(Penawaran.STATUS_SELESAI)){
									penawaran.changeStatus(Penawaran.STATUS_NEGOSIASI);
								}
							}

							try {
								//create job elastic dan, crawl data dan update ke elastic
								ElasticsearchLog obj = new ElasticsearchLog();
								obj.createJobUpdateElastic(id);
							} catch (Exception e) {
								e.printStackTrace();
							}

						}

						// delete produk tunggu setuju
						ProdukTungguSetuju.deleteProdTunggu(id);
						produk.historyProduct(aksi + "_" + setujuTolak, alasan, id);
					}else {
						flash.error("Approval Produk " + produk.nama_produk + " gagal karena Status Penawarannya " + penawaran.status.toUpperCase());
					}

					// insert produk riwayat

				}

			}catch (Sql2oException e){
				e.printStackTrace();
				throw new UnsupportedOperationException();
			}
		}

		persetujuanProduk(null);
	}

	@AllowAccess({Acl.COM_PENAWARAN_PERSETUJUAN_PRODUK})
	public static void selesaiNegosiasi(long id){
		AktifUser aktifUser = getAktifUser();
		try {
			Penawaran penawaran = Penawaran.findById(id);
			penawaran.changeStatus(Penawaran.STATUS_NEGOSIASI_SELESAI);
			ProdukTungguSetuju.deleteAllByPenawaran(id);
			ProdukService.setRejectedAllUnapprovedProductByPenawaran(id,aktifUser);
			PenawaranCtr.index();

		}catch (Exception e){
			e.printStackTrace();
			throw new UnsupportedOperationException(e);
		}

	}

	@AllowAccess({Acl.COM_PENAWARAN_PERSETUJUAN_PRODUK})
	public static void getPenyedia(String komoditas_id, long pokja_id){
		List<Penyedia> penyediaList = new ArrayList<>();
		JsonArray jsonArray = new JsonArray();
		if (!TextUtils.isEmpty(komoditas_id)) {
			penyediaList.addAll(Penyedia.getAllPenyediaPersetujuanProdukByPokjaId(pokja_id, Long.parseLong(komoditas_id)));
			for (Penyedia model : penyediaList) {
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("id", model.id);
				jsonObject.addProperty("nama_penyedia", model.nama_penyedia);
				jsonArray.add(jsonObject);
			}
		}
		renderJSON(jsonArray);
	}
}
