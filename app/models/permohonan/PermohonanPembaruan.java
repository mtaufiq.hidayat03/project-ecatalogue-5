package models.permohonan;

import com.google.gson.Gson;
import controllers.BaseController;
import models.BaseKatalogTable;
import models.common.AktifUser;
import models.common.DokumenInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.config.Configuration;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.masterdata.Kldi;
import models.penyedia.Penyedia;
import models.penyedia.PenyediaDistributor;
import models.penyedia.PenyediaDistributorRepresentatif;
import models.penyedia.PenyediaKontrak;
import models.prakatalog.Penawaran;
import models.user.User;
import play.Logger;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;
import play.mvc.Router;
import utils.DateTimeUtils;
import utils.KatalogUtils;
import utils.LogUtil;

import javax.persistence.Transient;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Table(name = "permohonan_pembaruan")
public class PermohonanPembaruan extends BaseKatalogTable {

    @Id
    public Long id;
    public String tipe_permohonan;
    public String nomor_permohonan;
    public Long penyedia_id;
    public String deskripsi;
    public int minta_disetujui;
    public Long minta_disetujui_oleh;
    public Date minta_disetujui_tanggal;
    public String setuju_tolak;
    public Date setuju_tolak_tanggal;
    public Long setuju_tolak_oleh;
    public String setuju_tolak_alasan;
    public Long dispo_user_id;
    public String klarif_terima_tolak;
    public Date klarif_terima_tolak_tanggal;
    public Long klarif_terima_tolak_oleh;
    public String klarif_terima_tolak_alasan;
    public Long klarif_user_id;
    public String status;
    public String file_hash;
    public String nama_file;
    public String kldi_jenis;
    public String kldi_id;
    public String jenis_komoditas;
    public Long blb_id_content;
    public int active;
    public int butuh_konfirmasi_kehadiran;
    public int butuh_klarifikasi_ulang; //0 belum diputuskan, -1 tidak butuh, 1 butuh
    public String klarifikasi_ulang_alasan;
    public int klarifikasi_ulang_ke;
    @Transient
    public String nama_penyedia;
    @Transient
    public String status_permohonan;
    @Transient
    public String from_status;
    @Transient
    public String to_status;
    @Transient
    public String kldi_nama;

    public static final String TIPE_PERMOHONAN_PROFIL = "pembaruan_profil";
    public static final String TIPE_PERMOHONAN_DISTRIBUTOR = "pembaruan_distributor";
    public static final String TIPE_PERMOHONAN_PRODUK = "pembaruan_produk";

    public static final String JENIS_KOMODITAS_NASIONAL = "nasional";
    public static final String JENIS_KOMODITAS_LOKAL = "lokal";
    public static final String JENIS_KOMODITAS_SEKTORAL = "sektoral";

    /*Status permohonan yang digunakan sebanyak 15 status*/
    public static final String STATUS_DRAFT = "draft";
    public static final String STATUS_PERSIAPAN = "persiapan";
    public static final String STATUS_MENUNGGU_PERSETUJUAN = "menunggu_persetujuan";
    public static final String STATUS_VERIFIKASI_SETUJU = "verifikasi_disetujui";
    public static final String STATUS_VERIFIKASI_TOLAK = "verifikasi_ditolak";
    public static final String STATUS_VERIFIKASI_PIHAK_KETIGA = "verifikasi_pihak_ketiga";
    public static final String STATUS_PERMOHONAN_DITOLAK = "permohonan_ditolak";
    public static final String STATUS_PERMOHONAN_DIPROSES = "permohonan_diproses";
    public static final String STATUS_KLARIFIKASI = "klarifikasi";
    public static final String STATUS_HASIL = "hasil_klarifikasi";
    public static final String STATUS_ADENDUM_KONTRAK = "adendum_kontrak";
    public static final String STATUS_SELESAI_SETUJU = "selesai_dan_disetujui";
    public static final String STATUS_SELESAI_TOLAK = "selesai_dan_ditolak";
    public static final String STATUS_BATAL = "batal"; //jika penyedia yang membatalkan permohonan sebelum permohonan dikirim ke lkpp
    public static final String STATUS_DIBATALKAN = "dibatalkan"; //jika admin/penyedia yang membatalkan permohonan setelah permohonan dikirim ke lkpp karna tidak bisa hadir klarifikasi

    /*status pendukung tambahan*/
    public static final String STATUS_TOLAK_DATA = "tolak_data";
    public static final String STATUS_TOLAK_PROSES = "tolak_proses";
    public static final String STATUS_SETUJU_DATA = "setuju_data";
    public static final String STATUS_SETUJU_PROSES = "setuju_proses";
    public static final String STATUS_SELESAI = "selesai";
    public static final String STATUS_KLARIFIKASI_TERIMA = "diterima";
    public static final String STATUS_KLARIFIKASI_TOLAK = "ditolak";
    public static final String STATUS_BERITA_ACARA = "berita_acara";
    public static final String STATUS_DIREKTUR = "direktur";
    public static final String STATUS_KASUBDIT = "kasubdit";
    public static final String STATUS_KASI = "kasi";
    public static final String STATUS_ADMIN = "admin";
    public static final String STATUS_PENYEDIA = "penyedia";
    public static final String STATUS_SETUJU = "setuju";
    public static final String STATUS_TOLAK = "tolak";
    public static final String STATUS_YA = "ya";
    public static final String STATUS_TIDAK = "tidak";

    public String getNomor_permohonan() {
        if (nomor_permohonan != null) {
            String[] idsp = nomor_permohonan.split("-");
            if (idsp[2].trim().equalsIgnoreCase("0")) {
                nomor_permohonan = idsp[0] + "-" + idsp[1] + "-" + id;
                //Paket.updateNopaketJob(id);
            }

            if (null != id && id > 0 && nomor_permohonan.contains("null")) {
                nomor_permohonan = nomor_permohonan.replaceAll("-null", "-" + id);
                updateNomorPermohonan(nomor_permohonan, id);
            }
        }

        return nomor_permohonan;
    }

    public void setNomor_permohonan(String nomor_permohonan) {
        this.nomor_permohonan = nomor_permohonan;
    }

    public static int updateNomorPermohonan(String nomor_permohonan, Long id){
        //update produk set jumlah_stok = (jumlah_stok-10) where id = 6905;
        String query = "update permohonan_pembaruan " +
                " set nomor_permohonan = '"+nomor_permohonan+"'" +
                " where id = "+id;

        return Query.update(query);
    }

    public static Map<String,String> jenisKomoditasMaps(){
        Map<String,String> map = new HashMap<String,String>();
        map.put(JENIS_KOMODITAS_NASIONAL, KatalogUtils.getStatusLabel(JENIS_KOMODITAS_NASIONAL));
        map.put(JENIS_KOMODITAS_LOKAL,KatalogUtils.getStatusLabel(JENIS_KOMODITAS_LOKAL));
        map.put(JENIS_KOMODITAS_SEKTORAL,KatalogUtils.getStatusLabel(JENIS_KOMODITAS_SEKTORAL));
        return map;
    }

    public static Map<String,String> tipePermohonanMaps(){
        Map<String,String> map = new HashMap<String,String>();
        map.put(TIPE_PERMOHONAN_PROFIL, KatalogUtils.getStatusLabel(TIPE_PERMOHONAN_PROFIL));
        map.put(TIPE_PERMOHONAN_DISTRIBUTOR,KatalogUtils.getStatusLabel(TIPE_PERMOHONAN_DISTRIBUTOR));
        map.put(TIPE_PERMOHONAN_PRODUK,KatalogUtils.getStatusLabel(TIPE_PERMOHONAN_PRODUK));
        return map;
    }

    public static Map<String,String> posisiPermohonanMaps(){
        Map<String,String> map = new HashMap<String,String>();
        map.put(STATUS_DIREKTUR, KatalogUtils.getStatusLabel(STATUS_DIREKTUR));
        map.put(STATUS_KASUBDIT, KatalogUtils.getStatusLabel(STATUS_KASUBDIT));
        map.put(STATUS_KASI,KatalogUtils.getStatusLabel(STATUS_KASI));
        map.put(STATUS_ADMIN,KatalogUtils.getStatusLabel(STATUS_ADMIN));
        map.put(STATUS_PENYEDIA, KatalogUtils.getStatusLabel(STATUS_PENYEDIA));
        return map;
    }

    public static List<User> findUserToSend(String posisi, String tipe, String NasLokSekTipe, String kldi_id){
        /*select user untuk nasional, lokal, sektoral*/
        String query = "SELECT * FROM user ";
        String whereQuery = "";
        String toSql = "";
        String status = "";

        if (NasLokSekTipe.equalsIgnoreCase(JENIS_KOMODITAS_NASIONAL)){
            whereQuery = "(kldi_jenis = 'LEMBAGA' OR kldi_jenis is null) AND role_basic_id not in (4,6) ";
        } else { //lokal sektoral
            whereQuery = "kldi_id = '"+kldi_id+"' ";
        }

        if (tipe.equalsIgnoreCase(STATUS_VERIFIKASI_SETUJU) || tipe.equalsIgnoreCase(STATUS_PERMOHONAN_DIPROSES)){ //untuk disposisi
            if (posisi.equalsIgnoreCase(STATUS_ADMIN)){
                status = STATUS_DIREKTUR;
            }

            if (posisi.equalsIgnoreCase(STATUS_DIREKTUR)){
                status = STATUS_KASUBDIT;
            }

            if (posisi.equalsIgnoreCase(STATUS_KASUBDIT)){
                status = STATUS_KASI;
            }
        } else { //untuk hasil klarif
            if (posisi.equalsIgnoreCase(STATUS_ADMIN)){
                status = STATUS_KASI;
            }

            if (posisi.equalsIgnoreCase(STATUS_KASI)){
                status = STATUS_KASUBDIT;
            }

            if (posisi.equalsIgnoreCase(STATUS_KASUBDIT)){
                status = STATUS_DIREKTUR;
            }
        }

        if (!whereQuery.isEmpty()){
            whereQuery = whereQuery + " AND jabatan = '"+status+"' ";
            toSql = query + " where " + whereQuery;
        }

        List<User> userList = Query.find(toSql,User.class).fetch();

        return userList;
    }

    public static Map<String,String> statusPermohonanMaps(){
        Map<String,String> map = new HashMap<String,String>();
        map.put(STATUS_DRAFT, KatalogUtils.getStatusLabel(STATUS_DRAFT));
        map.put(STATUS_PERSIAPAN, KatalogUtils.getStatusLabel(STATUS_PERSIAPAN));
        map.put(STATUS_MENUNGGU_PERSETUJUAN, KatalogUtils.getStatusLabel(STATUS_MENUNGGU_PERSETUJUAN));
        map.put(STATUS_VERIFIKASI_SETUJU, KatalogUtils.getStatusLabel(STATUS_VERIFIKASI_SETUJU));
        map.put(STATUS_VERIFIKASI_TOLAK, KatalogUtils.getStatusLabel(STATUS_VERIFIKASI_TOLAK));
        map.put(STATUS_VERIFIKASI_PIHAK_KETIGA, KatalogUtils.getStatusLabel(STATUS_VERIFIKASI_PIHAK_KETIGA));
        map.put(STATUS_PERMOHONAN_DITOLAK, KatalogUtils.getStatusLabel(STATUS_PERMOHONAN_DITOLAK));
        map.put(STATUS_PERMOHONAN_DIPROSES, KatalogUtils.getStatusLabel(STATUS_PERMOHONAN_DIPROSES));
        map.put(STATUS_KLARIFIKASI, KatalogUtils.getStatusLabel(STATUS_KLARIFIKASI));
        map.put(STATUS_HASIL, KatalogUtils.getStatusLabel(STATUS_HASIL));
        map.put(STATUS_ADENDUM_KONTRAK, KatalogUtils.getStatusLabel(STATUS_ADENDUM_KONTRAK));
        map.put(STATUS_SELESAI_SETUJU, KatalogUtils.getStatusLabel(STATUS_SELESAI_SETUJU));
        map.put(STATUS_SELESAI_TOLAK, KatalogUtils.getStatusLabel(STATUS_SELESAI_TOLAK));
        map.put(STATUS_BATAL, KatalogUtils.getStatusLabel(STATUS_BATAL));
        map.put(STATUS_DIBATALKAN, KatalogUtils.getStatusLabel(STATUS_DIBATALKAN));
        return map;
    }

    public String getReadableCreatedDate() {
        return DateTimeUtils.parseToReadableDate(created_date);
    }

    public String getReadableRequestApprovedDate() {
        String requestDate = DateTimeUtils.parseToReadableDate(minta_disetujui_tanggal);
        if(requestDate != null && !requestDate.equalsIgnoreCase("")){
            return requestDate;
        }else{
            return "N/A";
        }
    }

    public String getTipeLabel(){
        return KatalogUtils.getLabel(tipe_permohonan);
    }

    public String getStatusLabel(){
        return KatalogUtils.getLabel(status != null ? status : "");
    }

    public String getPosisiLabel(){
        String kldiName = "";

        if(to_status != null){
            if(AktifUser.getAktifUser().isProvider() && (to_status.equalsIgnoreCase(STATUS_DIREKTUR) || to_status.equalsIgnoreCase(STATUS_KASUBDIT) || to_status.equalsIgnoreCase(STATUS_KASI) || to_status.equalsIgnoreCase(STATUS_ADMIN))){
                if(kldi_id != null && !kldi_id.equalsIgnoreCase("") && !jenis_komoditas.equalsIgnoreCase(JENIS_KOMODITAS_NASIONAL)){
                    Kldi kldi = Kldi.findbyIdKldi(kldi_id);

                    if(kldi.nama != null){
                        kldiName = kldi.nama;
                    }
                }else{
                    kldiName = "LKPP";
                }

                return kldiName;
            }else{
                return to_status;
            }
        }else{
            return STATUS_PENYEDIA;
        }
    }

    public String getFileUrl() {
        String url = "";

        if(blb_id_content != null && blb_id_content.toString() != ""){
            BlobTable blobTable = BlobTable.findByBlobId(blb_id_content);
            if(blobTable != null){
                DokumenInfo dokumenInfo = DokumenInfo.findDokumenKontrak(blobTable);
                url = dokumenInfo.download_url;
            }
        }

        return url;
    }

    public boolean permohonanSubmit(PermohonanPembaruan pmb, File file){
        try {
            BlobTable blob = null;
            if(file != null) {
//                if(file.length() <= 5242880) {
                    blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, this.blb_id_content, PermohonanPembaruan.class.getSimpleName());
                    this.nama_file = blob.blb_nama_file;
                    this.file_hash = blob.blb_hash;
                    this.blb_id_content = blob.id;
//                }else{
//                    return false;
//                }
            }
            this.tipe_permohonan = pmb.tipe_permohonan;
            this.penyedia_id = pmb.penyedia_id;
            this.deskripsi = pmb.deskripsi;
            this.status = pmb.status;
            this.active = pmb.active;

            Long idr = new Long(this.savePermAndRiwayat());
            if(null != blob){
                blob.blb_id_content = idr;
                blob.save();
            }

            return true;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            LogUtil.d("***************** error ***************", e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public int savePermAndRiwayat(){
        PembaruanRiwayat rw = new PembaruanRiwayat();
        int ret = this.save();
        if(null == this.id){
            rw.addPermohonanPembaruan(new Long(ret),this.deskripsi);
        }else{
            rw.editPermohonanPembaruan(id,this.deskripsi);
        }
        return ret;
    }

    public StringBuilder getMakeButtonGroups(){
        try{
            AktifUser aktifUser = BaseController.getAktifUser();
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("id", id);

            String urlEdit = Router.reverse("katalog.PermohonanPembaruanCtr.edit", params).url;
            String urlForm = Router.reverse("katalog.PermohonanPembaruanCtr.formPermohonan", params).url;
            String urlDetailPenyedia = Router.reverse("katalog.PermohonanPembaruanCtr.infoDetailPenyedia", params).url;
            String urlListProduk = Router.reverse("katalog.PermohonanPembaruanCtr.listProdukPermohonanPembaruanStaging", params).url;
            String urlDispo = Router.reverse("katalog.PermohonanPembaruanCtr.listDisposisi", params).url;
            String urlListDistributor = Router.reverse("katalog.PermohonanPembaruanCtr.listDistributorPermohonanPembaruanStaging", params).url;
            String urlKlarifikasi = Router.reverse("katalog.PermohonanPembaruanCtr.listKlarifikasi", params).url;
            String urlBeritaAcaraKlarifikasi = Router.reverse("katalog.PermohonanPembaruanCtr.beritaAcaraKlarifikasi", params).url;
            String urlHasilKlarifikasi = Router.reverse("katalog.PermohonanPembaruanCtr.listHasilKlarifikasi", params).url;
            String urlKoordinasi = Router.reverse("katalog.PermohonanPembaruanCtr.listKoordinasi", params).url;
            String urlListPenawaranKontrak = Router.reverse("katalog.PermohonanPembaruanCtr.listPenawaranKontrak", params).url;

            StringBuilder htmlBuilder = new StringBuilder("<ul class='dropdown-menu' aria-labelledby='dropdownMenu4'>");

            if(dispo_user_id == null){
                dispo_user_id = 0l;
            }

            PembaruanStaging pembStaging = PembaruanStaging.getById(id);

            if(aktifUser != null){
                if(aktifUser.isProvider()) {
                    if (status.equalsIgnoreCase(PermohonanPembaruan.STATUS_DRAFT)
                        || status.equalsIgnoreCase(PermohonanPembaruan.STATUS_PERSIAPAN)
                        || status.equalsIgnoreCase(PermohonanPembaruan.STATUS_VERIFIKASI_TOLAK)){
                        htmlBuilder.append("<li><a href='").append(urlEdit).append("'>").append(Messages.get("ubah.label")).append("</a></li>");
                        htmlBuilder.append("<li><a href='").append(urlForm).append("'>").append(Messages.get("form_permohonan.label")).append("</a></li>");
                        if (!status.equalsIgnoreCase(PermohonanPembaruan.STATUS_DRAFT) || null != pembStaging) {
                            if (tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_PROFIL)) {
                                htmlBuilder.append("<li><a href='").append(urlDetailPenyedia).append("'>").append(Messages.get("detail_permohonan.label")).append("</a></li>");
                            } else if (tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_PRODUK)) {
                                htmlBuilder.append("<li><a href='").append(urlListProduk).append("'>").append(Messages.get("detail_permohonan.label")).append("</a></li>");
                            } else if (tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_DISTRIBUTOR)) {
                                htmlBuilder.append("<li><a href='").append(urlListDistributor).append("'>").append(Messages.get("detail_permohonan.label")).append("</a></li>");
                            }
                        }
                        htmlBuilder.append("<li><a href='#' data_id='"+id+"' id='batalPermohonan' value='"+id+"'>").append(Messages.get("batal.label")).append("</a></li>");
                    }else{
                        if(tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_PROFIL)){
                            htmlBuilder.append("<li><a href='").append(urlDetailPenyedia).append("'>").append(Messages.get("detail_permohonan.label")).append("</a></li>");
                        } else if(tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_PRODUK)){
                            htmlBuilder.append("<li><a href='").append(urlListProduk).append("'>").append(Messages.get("detail_permohonan.label")).append("</a></li>");
                        } else if(tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_DISTRIBUTOR)){
                            htmlBuilder.append("<li><a href='").append(urlListDistributor).append("'>").append(Messages.get("detail_permohonan.label")).append("</a></li>");
                        }
                        if (status.equalsIgnoreCase(PermohonanPembaruan.STATUS_DIBATALKAN)){
                            htmlBuilder.append("<li><a href='").append(urlKlarifikasi).append("'>").append(Messages.get("jadwal_klarifikasi.label")).append("</a></li>");
                        }
                    }
                }else{
                    if(!status.equalsIgnoreCase(PermohonanPembaruan.STATUS_DRAFT)) {
                        if (tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_PROFIL)) {
                            htmlBuilder.append("<li><a href='").append(urlDetailPenyedia).append("'>").append(Messages.get("detail_permohonan.label")).append("</a></li>");
                        } else if (tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_PRODUK)) {
                            htmlBuilder.append("<li><a href='").append(urlListProduk).append("'>").append(Messages.get("detail_permohonan.label")).append("</a></li>");
                        } else if (tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_DISTRIBUTOR)) {
                            htmlBuilder.append("<li><a href='").append(urlListDistributor).append("'>").append(Messages.get("detail_permohonan.label")).append("</a></li>");
                        }
                    }
                }

                if(setuju_tolak != null){
                    if(setuju_tolak.equalsIgnoreCase(PermohonanPembaruan.STATUS_SETUJU_PROSES)) {
                        if(status.equalsIgnoreCase(PermohonanPembaruan.STATUS_PERMOHONAN_DIPROSES)){
                            if(aktifUser.isAdmin() || aktifUser.isAdminLokalSektoral() || aktifUser.user_id.equals(new Long(dispo_user_id))){
                                htmlBuilder.append("<li><a href='").append(urlDispo).append("'>").append(Messages.get("form_disposisi.label")).append("</a></li>");
                            }
                        }

                        if(status.equalsIgnoreCase(PermohonanPembaruan.STATUS_VERIFIKASI_PIHAK_KETIGA)){
                            htmlBuilder.append("<li><a href='").append(urlKoordinasi).append("'>").append(Messages.get("hasil_koordinasi.label")).append("</a></li>");
                        }

                        if(status.equalsIgnoreCase(PermohonanPembaruan.STATUS_KLARIFIKASI)) {
                            if (butuh_konfirmasi_kehadiran > 0) {
                                htmlBuilder.append("<li style='background-color: #f3d17a !important;' ><a href='").append(urlKlarifikasi).append("'>").append(Messages.get("jadwal_klarifikasi.label")).append("</a></li>");
                            } else {
                                htmlBuilder.append("<li><a href='").append(urlKlarifikasi).append("'>").append(Messages.get("jadwal_klarifikasi.label")).append("</a></li>");
                            }
                            htmlBuilder.append("<li><a href='").append(urlBeritaAcaraKlarifikasi).append("'>").append(Messages.get("ba_klarifikasi.label")).append("</a></li>");
                        }

                        if(status.equalsIgnoreCase(PermohonanPembaruan.STATUS_HASIL) || klarif_terima_tolak != null) {
                            htmlBuilder.append("<li><a href='").append(urlBeritaAcaraKlarifikasi).append("'>").append(Messages.get("ba_klarifikasi.label")).append("</a></li>");
                            if(aktifUser.isAdmin() || aktifUser.isAdminLokalSektoral()) {
                                htmlBuilder.append("<li><a href='").append(urlHasilKlarifikasi).append("'>").append(Messages.get("hasil_klarifikasi.label")).append("</a></li>");
                            }
                        }

                        if(status.equalsIgnoreCase(PermohonanPembaruan.STATUS_ADENDUM_KONTRAK)){
                            htmlBuilder.append("<li><a href='").append(urlListPenawaranKontrak).append("'>").append(Messages.get("unggah_kontrak.label")).append("</a></li>");
                        }
                    }
                }
            }

            htmlBuilder.append("</ul>");
            return htmlBuilder;
        }catch (Exception e){
            Logger.error("Error permohonantableservice buttongroup:",e);
            e.printStackTrace();
            throw new UnsupportedOperationException(e);
        }
    }

    public static int removeDocument(Long permohonanId){
        String query = "update permohonan_pembaruan set file_hash = null, nama_file = null, blb_id_content = null where id = ?";
        return Query.update(query, permohonanId);
    }

    public void applyStagging(){
        if(tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_PRODUK)){
            applyProdukStagging();
            ApplyKontrak();
        } else if(tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_PROFIL)){
            applyPenyediaStagging();
        } else if(tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_DISTRIBUTOR)){
            applyDistributorStagging();
        }
    }

    public void ApplyKontrak(){
        List<PembaruanKontrak> list = PembaruanKontrak.getByPermohonanId(this.id);
        for ( PembaruanKontrak k: list) {
            Logger.info("pembaruan kontrak:"+k.id);
            List<PenyediaKontrak> lstKontrak = PenyediaKontrak.getByPenawaranId(k.penawaran_id);
            //nonaktifkan yang lama
            for ( PenyediaKontrak pk : lstKontrak) {
                pk.apakah_berlaku = false;
                pk.save();
                Logger.info("old kontrak:"+pk.id);
            }
            //save yang baru
            Long idkontrak = k.saveAsPenyediaKontrak(k.penawaran_id);
            Logger.info("new kontrak:"+idkontrak);
            Penawaran.updateKontrak(idkontrak, k.penawaran_id);
            Logger.info("update done:"+k.id);
        }


    }

    private void applyProdukStagging(){
        List<PembaruanStaging> infos = PembaruanStaging.getIfProdByPermohonanPembaruanId_INFO(this.id);
        Logger.info("APPLY INFO");
        for ( PembaruanStaging pstg: infos) {
            pstg.getIfProdukStagging().applyStaggingProdukInfo();
        }
        List<PembaruanStaging> notinfos = PembaruanStaging.getIfProdByPermohonanPembaruanId_NOTINFO(this.id);
        for ( PembaruanStaging pstg: notinfos) {
            if(pstg.getIfProdukStagging().harga){
                //Logger.info("HARGA");
                //active 1, apakah disetuui 0 untuk yang lama
                pstg.getIfProdukStagging().applyStaggingProdukHarga();
            }else if(pstg.getIfProdukStagging().spec){
                //Logger.info("SPEC");
                pstg.getIfProdukStagging().applyStaggingSpec();
            } else if(pstg.getIfProdukStagging().lamp){
                //Logger.info("LAMP");
                pstg.getIfProdukStagging().applyStaggingLampiran();
            }else if(pstg.getIfProdukStagging().gambar){
                //Logger.info("GAMBAR");
                pstg.getIfProdukStagging().applyStaggingGambar();
            }
        }
    }

    private void applyPenyediaStagging(){
        PembaruanStaging penyedias = PembaruanStaging.getIfProdByPermohonanPembaruanId_PENYEDIA(this.id);
        Logger.info("APPLY PENYEDIA");
        Penyedia p = new Gson().fromJson(penyedias.object_json, Penyedia.class);
        p.save();
    }

    private void applyDistributorStagging(){
        List<PembaruanStaging> dist = PembaruanStaging.getDistributorApprovedByPermohonanPembaruanId(this.id);
        Logger.info("APPLY DISTRIBUTOR");
        for (PembaruanStaging ps : dist){
            DistributorStagging ds = new Gson().fromJson(ps.object_json, DistributorStagging.class);
            PenyediaDistributor pds =  ds.distributor;
            Boolean success = false;
            Boolean newdata = false;
            //handling new data dengan id 0
            if(null != pds.id && pds.id == 0l){
                pds.id = null;
            }
           if(null == pds.id){
               pds.penyedia_id = penyedia_id;
               pds.active = true;
               pds.minta_disetujui = 0l;
               pds.setuju_tolak = "Setuju";
               pds.setuju_tolak_oleh = -99l;
               pds.setuju_tolak_tanggal = new Date();
               pds.setuju_tolak_alasan = "Approved by Permohonan Pembaruan";
               pds.active = true;
               int ids = pds.save();
               pds.id = Long.valueOf(ids);
               success = true;
               newdata = true;
           }else{
//               pds.setFromEditStaging();
//               pds.minta_disetujui = 0l;
//               pds.active = true;
//               pds.save();
               PenyediaDistributor pdd = PenyediaDistributor.findById(pds.id);
               // dibedain konsepnya karena khawatir pada saat editnya tidak menyimpan data2 sebelumnya,
               // khawatir data kotor, karena salah penerapan
               pdd.setFromEditStaging(pds);
               pdd.save();
               success = true;
           }

           //Logger.info("dist:"+pds.nama_distributor);
           if(success){
               List<PenyediaDistributorRepresentatif> reps = ds.listRepresentatif;
               for ( PenyediaDistributorRepresentatif pr: reps) {
                   //Logger.info("reps:"+pr.nama);
                   //handling new representatif
                   if(null != pr.id){
                       if(pr.id.equals(0l)){
                           pr.id = null;
                       }
                   }
                   pr.active = true;
                   pr.penyedia_distributor_id = pds.id;
                   pr.save();
               }
           }
        }
    }

    public static void sendMail(String email, PembaruanKlarifikasi pembaruanKlarifikasi, PermohonanPembaruan pmb, Penyedia penyedia){
        final String PAKET_EMAIL_BODY = "mail.body.jadwal_klarifikasi";
        final String PAKET_EMAIL_SUBJECT ="mail.subject.jadwal_klarifikasi";

        String subjectEmail = Configuration.getConfigurationValue(PAKET_EMAIL_SUBJECT);
        subjectEmail = subjectEmail.replaceAll("nomor_permohonan",pmb.nomor_permohonan);
        String waktu = new SimpleDateFormat("HH:mm").format(pembaruanKlarifikasi.waktu_kegiatan);

        String bodyEmail = Configuration.getConfigurationValue(PAKET_EMAIL_BODY);
        bodyEmail = bodyEmail.replaceAll("no_permohonan",pmb.nomor_permohonan);
        bodyEmail = bodyEmail.replaceAll("nama_penyedia",penyedia.nama_penyedia);
        bodyEmail = bodyEmail.replaceAll("perihal_permohonan",pmb.deskripsi);
        bodyEmail = bodyEmail.replaceAll("tanggal_klarifikasi",pembaruanKlarifikasi.getReadableDate());
        bodyEmail = bodyEmail.replaceAll("waktu_klarifikasi",""+waktu+" WIB");
        bodyEmail = bodyEmail.replaceAll("lokasi_klarifikasi",pembaruanKlarifikasi.lokasi);
        bodyEmail = bodyEmail.replaceAll("judul_klarifikasi",pembaruanKlarifikasi.judul_kegiatan);
        bodyEmail = bodyEmail.replaceAll("keterangan_klarifikasi",pembaruanKlarifikasi.keterangan);

        String reportText = bodyEmail;
//        Logger.info("Permohonan Pembaruan - create mail subject: " + subjectEmail);
//        Logger.info("Permohonan Pembaruan - create mail body: " + reportText);
//        Logger.info("Permohonan Pembaruan - create mail send to: " + email);
        try {
            String report = Configuration.getConfigurationValue(MailQueue.SMTP_MAIL_ADMIN, "");
            Logger.info("create mail prop job report to: " + email);
            MailQueue mq = EmailManager.createEmail(email, reportText, subjectEmail);
            mq.save();
        } catch (Exception e) {
            e.printStackTrace();
            Logger.error("error create mail:" + e.getMessage());
        }
    }
}
