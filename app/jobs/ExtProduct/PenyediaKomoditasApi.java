package jobs.ExtProduct;

import java.sql.Timestamp;

public class PenyediaKomoditasApi {
    public Long penyediaid;
    public String nama_penyedia;
    public String email;
    public Long komoditasid;
    public String kode_komoditas;
    public String produk_api_url;
    public Timestamp start_date;
    public Long harga_retail;
    public Long harga_pemerintah;
    public Long harga_ongkir;

}
