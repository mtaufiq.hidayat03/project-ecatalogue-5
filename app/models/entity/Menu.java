package models.entity;

import models.secman.Acl;
import play.i18n.Messages;
import play.mvc.Router;

public enum Menu {
	
	HOME("beranda.label", "PublikCtr.home",""),
	HOMEPUBLIC("beranda.label", "HomeCtr.Home",""),
	SDK("syarat_ketentuan.label", "PublikCtr.syaratKetentuan",""),
	FAQ("faq.label", "PublikCtr.faq",""),
	UNDUH("unduh.label","PublikCtr.listUnduh",""),
	BERITA("berita.label", "PublikCtr.listBerita", ""),
	PENGUMUMAN("pengumuman.label", "PublikCtr.listUsulan", ""),
	DEV("Development Guide", "PublikCtr.index",""),
	HUB("hubungi_kami.label", "PublikCtr.hubungiKami",""),
	KATALOG_LOKAL_SEKTORAL("katalog_lokal_sektoral.label", "PublikCtr.katalogLokalSektoral", ""),
	LOGIN("masuk.label", "LoginCtr.index",""),
	LOGOUT("keluar.label", "admin.UserCtr.logout",""),
	
	MASTER_USER_ROLE("peran_pengguna.label", "masterdata.GrupRoleCtr.index",Acl.COM_USER_GRUP_ROLE.namaRole),
	MASTER_USER("pengguna.label", "admin.UserCtr.index", Acl.COM_USER.namaRole),
	MASTER_USER_LOKAL("pengguna_lokal_sektoral.label", "admin.UserLokalCtr.index", Acl.COM_USER_LOKAL.namaRole),
//	MASTER_PENYEDIA_LOKAL("penyedia_lokal_sektoral.label", "admin.PenyediaLokalCtr.index", Acl.COM_PENYEDIA_LOKAL_ADD.namaRole),
	MASTER_PENYEDIA("penyedia.label", "masterdata.PenyediaCtr.index", Acl.COM_PENYEDIA.namaRole),
	MASTER_KATEGORI_KOMODITAS("kategori_komoditas.label", "admin.KategoriKomoditasCtr.index",Acl.COM_KOMODITAS_KATEGORI.namaRole),
	MASTER_KOMODITAS("komoditas.label", "admin.KomoditasCtr.index",Acl.COM_KOMODITAS.namaRole),
	MASTER_MANUFAKTUR("merk.label", "masterdata.ManufakturCtr.index", Acl.COM_MANUFAKTUR.namaRole),
	MASTER_SUMBER_DANA("sumber_dana.label", "masterdata.SumberDanaCtr.index", Acl.COM_SUMBER_DANA.namaRole),
	MASTER_BIDANG("bidang.label", "masterdata.BidangCtr.index", Acl.COM_BIDANG.namaRole),
	MASTER_SUMBER_KURS("sumber_kurs.label", "masterdata.SumberKursCtr.index",Acl.COM_SUMBER_KURS.namaRole),
	MASTER_KURS("kurs.label", "masterdata.KursCtr.index",Acl.COM_KURS.namaRole),
	MASTER_UNIT_PENGUKURAN("unit_pengukuran.label", "masterdata.UnitPengukuranCtr.index",Acl.COM_UNIT_PENGUKURAN.namaRole),
	MASTER_TAHAPAN("tahapan.label", "masterdata.TahapanCtr.index",Acl.COM_TAHAPAN.namaRole),
	MASTER_BERITA("berita.label", "cms.BeritaCtr.index",Acl.COM_BERITA.namaRole),
	MASTER_UNDUH("unduh.label","cms.UnduhCtr.index",Acl.COM_PUSTAKA_FILE.namaRole),
	MASTER_DOKUMEN_TEMPLATE("template_dokumen.label","masterdata.DokumenTemplateCtr.index",Acl.COM_DOCUMENT_TEMPLATE.namaRole),
	MASTER_FAQ("faq.label", "cms.FaqCtr.index", Acl.COM_FAQ.namaRole),
	MASTER_KONTEN_STATIS("konten_statis.label", "cms.KontenStatisCtr.index", Acl.COM_KONTEN_STATIS.namaRole),
	MASTER_KATEGORI_KONTEN("kategori_konten.label", "cms.KategoriKontenCtr.index", Acl.COM_KATEGORI_KONTEN.namaRole),
	MASTER_POLLING("polling.label", "cms.PollingCtr.index", Acl.COM_POLLING.namaRole),
	MASTER_NOTIFIKASI_EDIT("kelola_pemberitahuan.label","cms.NotifikasiCtr.edit",Acl.COM_NOTIFIKASI_EDIT.namaRole),
	MASTER_INSTANSI("instansi_satker.label", "masterdata.InstansiSatkerCtr.index",Acl.COM_INSTANSI.namaRole),
	MASTER_WILAYAH("wilayah.label", "masterdata.WilayahCtr.index",Acl.COM_WILAYAH.namaRole),
	MASTER_RUP("rup.label", "masterdata.RupCtr.index",Acl.COM_RUP.namaRole),
	MASTER_BLACKLIST("blacklist.label", "masterdata.BlacklistCtr.index",Acl.COM_BLACKLIST.namaRole),
	MASTER_PROFIL_PENYEDIA("pengaturan.label", "masterdata.ProfilPenyediaCtr.index", Acl.COM_PROFIL_PENYEDIA.namaRole),
	MASTER_PROFIL_SATUAN_KERJA("pengaturan.label", "masterdata.ProfilSatkerCtr.index", Acl.COM_PROFIL_SATUAN_KERJA.namaRole),
	MASTER_DISTRIBUTOR("daftar_distributor.label", "penyedia.PenyediaDistributorCtr.index", Acl.COM_PROFIL_PENYEDIA_DISTRIBUTOR.namaRole),
	MASTER_PARAMETER_APLIKASI("parameter_aplikasi.label", "masterdata.ParameterAplikasiCtr.index", Acl.COM_PARAMETER_APLIKASI.namaRole),
	MASTER_KOMODITAS_NASIONAL("komoditas_nasional.label", "admin.KomoditasCtr.nasional",""),
	MASTER_KOMODITAS_LOKAL("komoditas_lokal.label", "admin.KomoditasCtr.lokal",""),
	MASTER_KOMODITAS_SEKTORAL("komoditas_sektoral.label", "admin.KomoditasCtr.sektoral",""),
	MASTER_BANNER("banner.label", "cms.BannerCtr.index", Acl.COM_BANNER.namaRole),

	PRAKATALOG_PERSETUJUAN_PRODUK("persetujuan_produk.label", "prakatalog.PersetujuanCtr.persetujuanProduk",Acl.COM_PENAWARAN_PERSETUJUAN_PRODUK.namaRole),
	PRAKATALOG_PENAWARAN("penawaran.label", "prakatalog.PenawaranCtr.index",Acl.COM_PENAWARAN.namaRole),
	PRAKATALOG_USULAN("usulan.label", "admin.UsulanCtr.index", Acl.COM_USULAN.namaRole),
	
	KATALOG_PRODUK("daftar_produk.label", "katalog.ProdukCtr.index", Acl.COM_PRODUK.namaRole),
	KATALOG_LAPORAN_PRODUK("kelola_laporan.label", "katalog.LaporanProdukCtr.index", Acl.COM_PRODUK_REPORT_MANAGE.namaRole),
	KATALOG_PERMOHONAN_PEMBARUAN("permohonan_pembaruan.label", "katalog.PermohonanPembaruanCtr.index", Acl.COM_PERMOHONAN_PEMBARUAN.namaRole),
	
	PAKET_KONSOLIDASI("paket_konsolidasi.label", "purchasing.PaketKonsolidasiCtr.index",""),
	PAKET("paket.label", "purchasing.PaketCtr.index", Acl.COM_PAKET.namaRole),

	MASTER_POINT_RATING("point_rating.label","masterdata.PointRatingCtr.index", Acl.COM_MASTER_POINT_RATING_INDEX.namaRole),
	MASTER_ROLE_ITEM("item_peran.label","admin.RoleItemCtr.index",Acl.COM_MASTER_ROLE_ITEM_INDEX.namaRole),

	SYSTEM_JOBS("Jobs", "SystemCtr.jobs",""),
	SYSTEM_INFORMATION("informasi.label", "SystemCtr.information",""),
	SYSTEM_PERFORMANCE("kinerja.label", "SystemCtr.performance",""),

	EXEC_PRODUK_ELASTIC("execapi.produkelastic.label", "api.KatalogApiCtr.formProduk", Acl.COM_EXEC_PRODUK_ELASTICS.namaRole);


	public static Menu[] MENU_USER_MANAGEMENT = new Menu[]{
			MASTER_USER_ROLE, 
			MASTER_USER,
			MASTER_USER_LOKAL,
//			MASTER_PENYEDIA_LOKAL,
			MASTER_PENYEDIA,
			MASTER_ROLE_ITEM
	};

	public static String[] USER_MANAGEMENT_ACL = new String[]{
			Acl.COM_USER_GRUP_ROLE.namaRole,
			Acl.COM_USER.namaRole,
			Acl.COM_USER_LOKAL.namaRole,
//			Acl.COM_PENYEDIA_LOKAL.namaRole,
			Acl.COM_PENYEDIA.namaRole,
			Acl.COM_MASTER_ROLE_ITEM_INDEX.namaRole
	};

	public static Menu[] MENU_KOMODITAS = new Menu[] {
			MASTER_KOMODITAS_NASIONAL,
			MASTER_KOMODITAS_LOKAL,
			MASTER_KOMODITAS_SEKTORAL,
	};

	public static Menu[] MENU_MANAJEMEN_KOMODITAS = new Menu[] {
			MASTER_KATEGORI_KOMODITAS,
			MASTER_KOMODITAS,
			MASTER_MANUFAKTUR,
	};

	public static String[] MANAJEMEN_KOMODITAS_ACL = new String[]{
			Acl.COM_KOMODITAS_KATEGORI.namaRole,
			Acl.COM_KOMODITAS.namaRole,
			Acl.COM_MANUFAKTUR.namaRole
	};

	public static Menu[] MENU_PARAMETER = new Menu[] {
			MASTER_BIDANG,
			MASTER_SUMBER_DANA,
			MASTER_SUMBER_KURS,
			MASTER_KURS,
			MASTER_UNIT_PENGUKURAN,
			MASTER_TAHAPAN,
			MASTER_PARAMETER_APLIKASI,
			MASTER_INSTANSI,
			MASTER_WILAYAH,
			MASTER_RUP,
			MASTER_BLACKLIST
	};

	public static String[] PARAMETER_ACL = new String[]{
			Acl.COM_BIDANG.namaRole,
			Acl.COM_SUMBER_DANA.namaRole,
			Acl.COM_SUMBER_KURS.namaRole,
			Acl.COM_KURS.namaRole,
			Acl.COM_UNIT_PENGUKURAN.namaRole,
			Acl.COM_TAHAPAN.namaRole,
			Acl.COM_PARAMETER_APLIKASI.namaRole,
			Acl.COM_INSTANSI.namaRole,
			Acl.COM_WILAYAH.namaRole,
			Acl.COM_RUP.namaRole
	};

	public static Menu[] MENU_CMS = new Menu[]{
			MASTER_BANNER,
			MASTER_BERITA,
			MASTER_FAQ,
			MASTER_DOKUMEN_TEMPLATE,
			MASTER_KONTEN_STATIS,
			MASTER_KATEGORI_KONTEN,
			MASTER_POLLING,
			MASTER_NOTIFIKASI_EDIT,
			MASTER_UNDUH,
			MASTER_POINT_RATING
	};

	public static String[] CMS_ACL = new String[]{
			Acl.COM_BANNER.namaRole,
			Acl.COM_BERITA.namaRole,
			Acl.COM_DOCUMENT_TEMPLATE.namaRole,
			Acl.COM_FAQ.namaRole,
			Acl.COM_KONTEN_STATIS.namaRole,
			Acl.COM_KATEGORI_KONTEN.namaRole,
			Acl.COM_SYARAT_KETENTUAN.namaRole,
			Acl.COM_NOTIFIKASI_EDIT.namaRole
	};

	public static Menu[] MENU_KATALOG = new Menu[] {
			KATALOG_PRODUK,
			KATALOG_LAPORAN_PRODUK
//			,KATALOG_PERMOHONAN_PEMBARUAN
	};

	public static String[] KATALOG_ACL = new String[] {
			Acl.COM_PRODUK.namaRole,
			Acl.COM_PRODUK_REPORT_MANAGE.namaRole
//			,Acl.COM_PERMOHONAN_PEMBARUAN.namaRole
	};

	public static Menu[] MENU_PRAKATALOG = new Menu[] {
			PRAKATALOG_PENAWARAN,
			PRAKATALOG_PERSETUJUAN_PRODUK,
			PRAKATALOG_USULAN
	};

	public static String[] PRAKATALOG_ACL = new String[]{
			Acl.COM_PENAWARAN.namaRole,
			Acl.COM_PENAWARAN_PERSETUJUAN_PRODUK.namaRole,
			Acl.COM_USULAN.namaRole
	};

	public static Menu[] MENU_PERMOHONAN = new Menu[] {
			KATALOG_PERMOHONAN_PEMBARUAN
	};
	
	public static Menu[] MENU_PAKET = new Menu[] {
			PAKET,
			PAKET_KONSOLIDASI
	};

	public static Menu[] MENU_PROFIL = new Menu[] {
			MASTER_PROFIL_PENYEDIA,
			MASTER_PROFIL_SATUAN_KERJA,
			MASTER_DISTRIBUTOR
	};

	public static String[] PROFIL_ACL = new String[]{
			Acl.COM_PROFIL_PENYEDIA.namaRole,
			Acl.COM_PROFIL_SATUAN_KERJA.namaRole,
			Acl.COM_PROFIL_PENYEDIA_DISTRIBUTOR.namaRole
	};

	public static String[] Exec_API_ACL = new String[]{
			Acl.COM_EXEC_PRODUK_ELASTICS.namaRole
	};

	public static Menu[] MENU_API = new Menu[] {
			EXEC_PRODUK_ELASTIC
	};

	public final String caption;
	public final String captionUpperCase;
	
	public final String action;
	public final String acl;

	public final String page;
	
	Menu(String caption, String action, String acl) {
		this.caption = caption;
		this.captionUpperCase = caption.toUpperCase();
		this.action = action;
		this.acl = acl;
		this.page = Router.reverse(action).url;
	}

}
