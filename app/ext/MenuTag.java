package ext;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

//import com.mysql.fabric.xmlrpc.base.Array;

import controllers.BaseController;
import groovy.lang.Closure;
import models.common.AktifUser;
import models.entity.Menu;
import play.i18n.Messages;
import play.mvc.Router;
import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;

public class MenuTag extends FastTags {
	
	public static void _desktopmenu(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
//		Request request = Request.current();
		StringBuilder content = new StringBuilder();
		AktifUser aktifUser = BaseController.getAktifUser();

		String openUl = "<ul class=\"nav navbar-nav\">";

		String openMenuList = "<li class=\"dropdown\">";

		String openAnchorMenu = "<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
		String closeAnchorMenu = " <span class=\"caret\"></span></a>";

		String menuDropDown = "<ul class=\"dropdown-menu\">" +
				"<li><div class=\"container-fluid\"><div class=\"row\">";

		String closeMenuList = "</div></div></li>" +
				"</ul></li>";

		content.append(openUl);
		
		//HOME
		if(aktifUser != null) {
			content.append("<li><a href=\"").append(Menu.HOME.page).append("\">").append(Messages.get(Menu.HOME.caption)).append("</a></li>");
		}else{
			content.append("<li><a href=\"").append(Menu.HOMEPUBLIC.page).append("\">").append(Messages.get(Menu.HOME.caption)).append("</a></li>");
		}
		//Pengumuman
//		if(aktifUser != null) {
			content.append("<li><a href=\"").append(Menu.PENGUMUMAN.page).append("\">").append(Messages.get(Menu.PENGUMUMAN.caption)).append("</a></li>");
//		}
		if(aktifUser != null) {
			//Komoditas
			List<String> komoditasAcls = Arrays.asList(Menu.MANAJEMEN_KOMODITAS_ACL);
			List<String> intersectKomoditasAcls = aktifUser.roleItems.stream().filter(komoditasAcls::contains).collect(Collectors.toList());

			if(!intersectKomoditasAcls.isEmpty()){
				content.append(openMenuList);

				content.append(openAnchorMenu);
				content.append(Messages.get("manajemen_komoditas.label"));
				content.append(closeAnchorMenu);

				content.append(menuDropDown);
				for (Menu menu_manajemen_komoditas : Menu.MENU_MANAJEMEN_KOMODITAS) {
					if(aktifUser.roleItems.contains(menu_manajemen_komoditas.acl)){
						content.append("<a href=\"").append(menu_manajemen_komoditas.page.replace("_", "-")).append("\">")
								.append(Messages.get(menu_manajemen_komoditas.caption)).append("</a>");
					}
				}
				content.append(closeMenuList);
			}
			
			//Prakatalog
			List<String> prakatalogAcls = Arrays.asList(Menu.PRAKATALOG_ACL);
			List<String> intersectPrakatalogAcls = aktifUser.roleItems.stream().filter(prakatalogAcls::contains).collect(Collectors.toList());

			if(!intersectPrakatalogAcls.isEmpty()) {
				content.append(openMenuList);

				content.append(openAnchorMenu);
				content.append(Messages.get("prakatalog.label"));
				content.append(closeAnchorMenu);

				content.append(menuDropDown);

				for (Menu menu_parameter : Menu.MENU_PRAKATALOG) {
					if(aktifUser.roleItems.contains(menu_parameter.acl)){
						content.append("<a href=\"").append(menu_parameter.page).append("\">")
								.append(Messages.get(menu_parameter.caption)).append("</a>");
					}
				}

				content.append(closeMenuList);
			}
			
			//Katalog
			//content.append("<li><a href=\"").append(Menu.KATALOG_PRODUK.page).append("\">").append(Messages.get(Menu.KATALOG_PRODUK.caption)).append("</a></li>");

			List<String> katalogAcls = Arrays.asList(Menu.KATALOG_ACL);
			List<String> intersectKatalogAcls = aktifUser.roleItems.stream().filter(katalogAcls::contains).collect(Collectors.toList());

			if(!intersectKatalogAcls.isEmpty()) {
				content.append(openMenuList);

				content.append(openAnchorMenu);
				content.append(Messages.get("produk.label"));
				content.append(closeAnchorMenu);

				content.append(menuDropDown);

				for (Menu menu_katalog : Menu.MENU_KATALOG) {
					if(aktifUser.roleItems.contains(menu_katalog.acl)){
						content.append("<a href=\"").append(menu_katalog.page).append("\">")
								.append(Messages.get(menu_katalog.caption)).append("</a>");
					}
				}

				content.append(closeMenuList);
			}

			//Permohonan Pembaruan
			if(aktifUser.roleItems.contains(Menu.KATALOG_PERMOHONAN_PEMBARUAN.acl)){
				content.append("<li><a href=\"").append(Menu.KATALOG_PERMOHONAN_PEMBARUAN.page).append("\">").append(Messages.get(Menu.KATALOG_PERMOHONAN_PEMBARUAN.caption)).append("</a></li>");
			}

			//Paket
			if(aktifUser.roleItems.contains(Menu.PAKET.acl)){
				content.append("<li><a href=\"").append(Menu.PAKET.page).append("\">").append(Messages.get(Menu.PAKET.caption)).append("</a></li>");
			}

			//User Management
			List<String> userManAcls = Arrays.asList(Menu.USER_MANAGEMENT_ACL);
			List<String> intersectUserManAcls = aktifUser.roleItems.stream().filter(userManAcls::contains).collect(Collectors.toList());

			if(!intersectUserManAcls.isEmpty()){
				content.append(openMenuList);

				content.append(openAnchorMenu);
				content.append(Messages.get("manajemen_pengguna.label"));
				content.append(closeAnchorMenu);

				content.append(menuDropDown);
				for (Menu menu_usermanagement :  Menu.MENU_USER_MANAGEMENT) {
					if(aktifUser.roleItems.contains(menu_usermanagement.acl)){
						content.append("<a href=\"").append(menu_usermanagement.page).append("\">")
								.append(Messages.get(menu_usermanagement.caption)).append("</a>");
					}
				}
				content.append(closeMenuList);
			}

			//Master Data
			List<String> parameterAcls = Arrays.asList(Menu.PARAMETER_ACL);
			List<String> intersectParameterAcls = aktifUser.roleItems.stream().filter(parameterAcls::contains).collect(Collectors.toList());

			if(!intersectParameterAcls.isEmpty()){
				content.append(openMenuList);

				content.append(openAnchorMenu);
				content.append(Messages.get("master_data.label"));
				content.append(closeAnchorMenu);

				content.append(menuDropDown);
				for (Menu menu_parameter : Menu.MENU_PARAMETER) {
					if(aktifUser.roleItems.contains(menu_parameter.acl)){
						content.append("<a href=\"").append(menu_parameter.page).append("\">")
								.append(Messages.get(menu_parameter.caption)).append("</a>");
					}
				}
				content.append(closeMenuList);
			}

			//CMS
			List<String> cmsAcls = Arrays.asList(Menu.CMS_ACL);
			List<String> intersectCmsAlcs = aktifUser.roleItems.stream().filter(cmsAcls::contains).collect(Collectors.toList());

			if(!intersectCmsAlcs.isEmpty()) {
				content.append(openMenuList);

				content.append(openAnchorMenu);
				content.append(Messages.get("cms.label"));
				content.append(closeAnchorMenu);

				content.append(menuDropDown);
				for (Menu menu_cms : Menu.MENU_CMS) {
					if(aktifUser.roleItems.contains(menu_cms.acl)){
						content.append("<a href=\"").append(menu_cms.page).append("\">")
								.append(Messages.get(menu_cms.caption)).append("</a>");
					}
				}
				content.append(closeMenuList);
			}

			//Execute API
			List<String> apiAcls = Arrays.asList(Menu.Exec_API_ACL);
			List<String> intersectApiAcls = aktifUser.roleItems.stream().filter(apiAcls::contains).collect(Collectors.toList());

			if(!intersectApiAcls.isEmpty()){
				content.append(openMenuList);

				content.append(openAnchorMenu);
				content.append(Messages.get("exec_api_top.label"));
				content.append(closeAnchorMenu);

				content.append(menuDropDown);
				for (Menu menu_api : Menu.MENU_API) {
					if(aktifUser.roleItems.contains(menu_api.acl)){
						content.append("<a href=\"").append(menu_api.page).append("\">")
								.append(Messages.get(menu_api.caption)).append("</a>");
					}
				}
				content.append(closeMenuList);
			}

			//Profile
			List<String> profilAcls = Arrays.asList(Menu.PROFIL_ACL);
			List<String> intersectProfilAcls = aktifUser.roleItems.stream().filter(profilAcls::contains).collect(Collectors.toList());

			if(!intersectProfilAcls.isEmpty()){
				content.append(openMenuList);

				content.append(openAnchorMenu);
				content.append(Messages.get("profil.label"));
				content.append(closeAnchorMenu);

				content.append(menuDropDown);
				for (Menu menu_profil :  Menu.MENU_PROFIL) {
					if(aktifUser.roleItems.contains(menu_profil.acl)){
						content.append("<a href=\"").append(menu_profil.page).append("\">")
								.append(Messages.get(menu_profil.caption)).append("</a>");
					}
				}
				content.append(closeMenuList);
			}
		}

		//Berita
		content.append("<li><a href=\"").append(Menu.BERITA.page).append("\">").append(Messages.get(Menu.BERITA.caption)).append("</a></li>");
				
		//Unduh
		content.append("<li><a href=\"").append(Menu.UNDUH.page).append("\">").append(Messages.get(Menu.UNDUH.caption)).append("</a></li>");

		//Faq
		content.append("<li><a href=\"").append(Menu.FAQ.page).append("\">").append(Messages.get(Menu.FAQ.caption)).append("</a></li>");

		//Syarat Ketentuan
		if(aktifUser != null) {
			content.append("<li><a href=\"").append(Menu.SDK.page).append("\">").append(Messages.get(Menu.SDK.caption)).append("</a></li>");
		}

		//Hubungi Kami
		content.append("<li><a href=\"").append(Menu.HUB.page).append("\">").append(Messages.get(Menu.HUB.caption)).append("</a></li>");

		//Katalog Lokal/Sektoral
		content.append("<li><a href=\"").append(Menu.KATALOG_LOKAL_SEKTORAL.page).append("\">").append(Messages.get(Menu.KATALOG_LOKAL_SEKTORAL.caption)).append("</a></li>");

		//Developer
		//content.append("<li><a href=\"").append(Menu.DEV.page).append("\">").append(Menu.DEV.caption).append("</a></li>");
		content.append("</ul>");

		//String changeLangUrl = Router.getFullUrl("PublikCtr.changeLang");
		//content.append("<div class=\"language-wrapper\">");
		//content.append("<div class=\"pull-right\">");
		//content.append("<a href=\"" + changeLangUrl + "?lang=id" + "\">Bahasa Indonesia</a> | <a href=\"" + changeLangUrl + "?lang=en" + "\">English</a>");
		//content.append("</div></div>");

		out.print(content.toString());
	}

	public static void _mobilemenu(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {

		StringBuilder content = new StringBuilder();
		AktifUser aktifUser = BaseController.getAktifUser();

		String komoditasMenu = "komoditasMenu";
		String komoditasManagementMenu = "komoditasManagementMenu";
		String prakatalogMenu = "prakatalogMenu";
		String katalogMenu = "katalogMenu";
		String userManagementMenu = "userManagementMenu";
		String masterDataMenu = "masterDataMenu";
		String cmsMenu = "cmsMenu";
		String apiMenu = "apiMenu";
		String profilMenu = "profilMenu";

		String changeLangUrl = Router.reverse("PublikCtr.changeLang").url;

		content.append("<ul>");

		//Beranda
		if (aktifUser != null) {
			content.append("<li><a href=\"").append(Menu.HOME.page).append("\">").append(Messages.get(Menu.HOME.caption)).append("</a></li>");
		} else {
			content.append("<li><a href=\"").append(Menu.HOMEPUBLIC.page).append("\">").append(Messages.get(Menu.HOME.caption)).append("</a></li>");
		}
		content.append("<li>");
		content.append("<a href=\"#" + komoditasMenu + "\" class=\"collapsed\" data-toggle=\"collapse\" aria-expanded=\"false\" aria-controls=\"" + komoditasMenu + "\">");
		content.append(Messages.get("komoditas.label"));
		content.append("</a>");

		content.append("<ul class=\"collapse\" id=\"" + komoditasMenu + "\">");
		for (Menu menu_komoditas : Menu.MENU_KOMODITAS) {
			content.append("<li>");
			content.append("<a href=\"").append(menu_komoditas.page).append("\"><i class=\"fa fa-angle-double-right\"></i> ")
					.append(Messages.get(menu_komoditas.caption)).append("</a>");
		}
		content.append("</ul></li>");

		//Pengumuman
//		if(aktifUser != null) {
			content.append("<li><a href=\"").append(Menu.PENGUMUMAN.page).append("\">").append(Messages.get(Menu.PENGUMUMAN.caption)).append("</a></li>");
//		}

		if(aktifUser != null) {
			//Manajemen Komoditas
			List<String> komoditasAcls = Arrays.asList(Menu.MANAJEMEN_KOMODITAS_ACL);
			List<String> intersectKomoditasAcls = aktifUser.roleItems.stream().filter(komoditasAcls::contains).collect(Collectors.toList());

			if(!intersectKomoditasAcls.isEmpty()) {
				content.append("<li>");
				content.append("<a href=\"#"+komoditasManagementMenu+"\" class=\"collapsed\" data-toggle=\"collapse\" aria-expanded=\"false\" aria-controls=\""+komoditasManagementMenu+"\">");
				content.append(Messages.get("manajemen_komoditas.label"));
				content.append("</a>");

				content.append("<ul class=\"collapse\" id=\""+komoditasManagementMenu+"\">");
				for(Menu menu_manajemen_komoditas : Menu.MENU_MANAJEMEN_KOMODITAS) {
					if(aktifUser.roleItems.contains(menu_manajemen_komoditas.acl)) {
						content.append("<li>");
						content.append("<a href=\"").append(menu_manajemen_komoditas.page).append("\"><i class=\"fa fa-angle-double-right\"></i> ")
								.append(Messages.get(menu_manajemen_komoditas.caption)).append("</a>");
					}
				}
				content.append("</ul></li>");
			}

			//Prakatalog
			List<String> prakatalogAcls = Arrays.asList(Menu.PRAKATALOG_ACL);
			List<String> intersectPrakatalogAcls = aktifUser.roleItems.stream().filter(prakatalogAcls::contains).collect(Collectors.toList());

			if(!intersectPrakatalogAcls.isEmpty()) {
				content.append("<li>");
				content.append("<a href=\"#"+prakatalogMenu+"\" class=\"collapsed\" data-toggle=\"collapse\" aria-expanded=\"false\" aria-controls=\""+prakatalogMenu+"\">");
				content.append(Messages.get("prakatalog.label"));
				content.append("</a>");

				content.append("<ul class=\"collapse\" id=\""+prakatalogMenu+"\">");
				for(Menu menu_prakatalog : Menu.MENU_PRAKATALOG) {
					if(aktifUser.roleItems.contains(menu_prakatalog.acl)) {
						content.append("<li>");
						content.append("<a href=\"").append(menu_prakatalog.page).append("\"><i class=\"fa fa-angle-double-right\"></i> ")
								.append(Messages.get(menu_prakatalog.caption)).append("</a>");
					}
				}
				content.append("</ul></li>");
			}

			//Katalog
			List<String> katalogAcls = Arrays.asList(Menu.KATALOG_ACL);
			List<String> intersectKatalogAcls = aktifUser.roleItems.stream().filter(katalogAcls::contains).collect(Collectors.toList());

			if(!intersectKatalogAcls.isEmpty()) {
				content.append("<li>");
				content.append("<a href=\"#"+katalogMenu+"\" class=\"collapsed\" data-toggle=\"collapse\" aria-expanded=\"false\" aria-controls=\""+katalogMenu+"\">");
				content.append(Messages.get("produk.label"));
				content.append("</a>");

				content.append("<ul class=\"collapse\" id=\""+katalogMenu+"\">");
				for(Menu menu_katalog : Menu.MENU_KATALOG) {
					if(aktifUser.roleItems.contains(menu_katalog.acl)) {
						content.append("<li>");
						content.append("<a href=\"").append(menu_katalog.page).append("\"><i class=\"fa fa-angle-double-right\"></i> ")
								.append(Messages.get(menu_katalog.caption)).append("</a>");
					}
				}
				content.append("</ul></li>");
			}

			if(aktifUser.roleItems.contains(Menu.KATALOG_PERMOHONAN_PEMBARUAN.acl)){
				content.append("<li><a href=\"").append(Menu.KATALOG_PERMOHONAN_PEMBARUAN.page).append("\">").append(Messages.get(Menu.KATALOG_PERMOHONAN_PEMBARUAN.caption)).append("</a></li>");
			}

			//Paket
			if(aktifUser.roleItems.contains(Menu.PAKET.acl)){
				content.append("<li><a href=\"").append(Menu.PAKET.page).append("\">").append(Messages.get(Menu.PAKET.caption)).append("</a></li>");
			}

			//User Management
			List<String> userManAcls = Arrays.asList(Menu.USER_MANAGEMENT_ACL);
			List<String> intersectUserManAcls = aktifUser.roleItems.stream().filter(userManAcls::contains).collect(Collectors.toList());

			if(!intersectUserManAcls.isEmpty()) {
				content.append("<li>");
				content.append("<a href=\"#"+userManagementMenu+"\" class=\"collapsed\" data-toggle=\"collapse\" aria-expanded=\"false\" aria-controls=\""+userManagementMenu+"\">");
				content.append(Messages.get("manajemen_pengguna.label"));
				content.append("</a>");

				content.append("<ul class=\"collapse\" id=\""+userManagementMenu+"\">");
				for(Menu menu_usermanagement : Menu.MENU_USER_MANAGEMENT) {
					if(aktifUser.roleItems.contains(menu_usermanagement.acl)) {
						content.append("<li>");
						content.append("<a href=\"").append(menu_usermanagement.page).append("\"><i class=\"fa fa-angle-double-right\"></i> ")
								.append(Messages.get(menu_usermanagement.caption)).append("</a>");
					}
				}
				content.append("</ul></li>");
			}

			//Master Data
			List<String> parameterAcls = Arrays.asList(Menu.PARAMETER_ACL);
			List<String> intersectParameterAcls = aktifUser.roleItems.stream().filter(parameterAcls::contains).collect(Collectors.toList());

			if(!intersectParameterAcls.isEmpty()) {
				content.append("<li>");
				content.append("<a href=\"#"+masterDataMenu+"\" class=\"collapsed\" data-toggle=\"collapse\" aria-expanded=\"false\" aria-controls=\""+masterDataMenu+"\">");
				content.append(Messages.get("master_data.label"));
				content.append("</a>");

				content.append("<ul class=\"collapse\" id=\""+masterDataMenu+"\">");
				for(Menu menu_parameter : Menu.MENU_PARAMETER) {
					if(aktifUser.roleItems.contains(menu_parameter.acl)) {
						content.append("<li>");
						content.append("<a href=\"").append(menu_parameter.page).append("\"><i class=\"fa fa-angle-double-right\"></i> ")
								.append(Messages.get(menu_parameter.caption)).append("</a>");
					}
				}
				content.append("</ul></li>");
			}

			//CMS
			List<String> cmsAcls = Arrays.asList(Menu.CMS_ACL);
			List<String> intersectCmsAcls = aktifUser.roleItems.stream().filter(cmsAcls::contains).collect(Collectors.toList());

			if(!intersectCmsAcls.isEmpty()) {
				content.append("<li>");
				content.append("<a href=\"#"+cmsMenu+"\" class=\"collapsed\" data-toggle=\"collapse\" aria-expanded=\"false\" aria-controls=\""+cmsMenu+"\">");
				content.append(Messages.get("cms.label"));
				content.append("</a>");

				content.append("<ul class=\"collapse\" id=\""+cmsMenu+"\">");
				for(Menu menu_cms : Menu.MENU_CMS) {
					if(aktifUser.roleItems.contains(menu_cms.acl)) {
						content.append("<li>");
						content.append("<a href=\"").append(menu_cms.page).append("\"><i class=\"fa fa-angle-double-right\"></i> ")
								.append(Messages.get(menu_cms.caption)).append("</a>");
					}
				}
				content.append("</ul></li>");
			}

			//Execute API
			List<String> apiAcls = Arrays.asList(Menu.Exec_API_ACL);
			List<String> intersectApiAcls = aktifUser.roleItems.stream().filter(apiAcls::contains).collect(Collectors.toList());

			if(!intersectApiAcls.isEmpty()) {
				content.append("<li>");
				content.append("<a href=\"#"+apiMenu+"\" class=\"collapsed\" data-toggle=\"collapse\" aria-expanded=\"false\" aria-controls=\""+apiMenu+"\">");
				content.append(Messages.get("exec_api_top.label"));
				content.append("</a>");

				content.append("<ul class=\"collapse\" id=\""+apiMenu+"\">");
				for(Menu menu_api : Menu.MENU_API) {
					if(aktifUser.roleItems.contains(menu_api.acl)) {
						content.append("<li>");
						content.append("<a href=\"").append(menu_api.page).append("\"><i class=\"fa fa-angle-double-right\"></i> ")
								.append(Messages.get(menu_api.caption)).append("</a>");
					}
				}
				content.append("</ul></li>");
			}

			//Profile
			List<String> profilAcls = Arrays.asList(Menu.PROFIL_ACL);
			List<String> intersectProfilAcls = aktifUser.roleItems.stream().filter(profilAcls::contains).collect(Collectors.toList());

			if(!intersectProfilAcls.isEmpty()) {
				content.append("<li>");
				content.append("<a href=\"#"+profilMenu+"\" class=\"collapsed\" data-toggle=\"collapse\" aria-expanded=\"false\" aria-controls=\""+profilMenu+"\">");
				content.append(Messages.get("profil.label"));
				content.append("</a>");

				content.append("<ul class=\"collapse\" id=\""+profilMenu+"\">");
				for(Menu menu_profil : Menu.MENU_PROFIL) {
					if(aktifUser.roleItems.contains(menu_profil.acl)) {
						content.append("<li>");
						content.append("<a href=\"").append(menu_profil.page).append("\"><i class=\"fa fa-angle-double-right\"></i> ")
								.append(Messages.get(menu_profil.caption)).append("</a>");
					}
				}
				content.append("</ul></li>");
			}
		}

		//Berita
		content.append("<li><a href=\"").append(Menu.BERITA.page).append("\">").append(Messages.get(Menu.BERITA.caption)).append("</a></li>");

		//Unduh
		content.append("<li><a href=\"").append(Menu.UNDUH.page).append("\">").append(Messages.get(Menu.UNDUH.caption)).append("</a></li>");

		//Faq
		content.append("<li><a href=\"").append(Menu.FAQ.page).append("\">").append(Messages.get(Menu.FAQ.caption)).append("</a></li>");

		//Syarat Ketentuan
		if(aktifUser != null) {
			content.append("<li><a href=\"").append(Menu.SDK.page).append("\">").append(Messages.get(Menu.SDK.caption)).append("</a></li>");
		}

		//Hubungi Kami
		content.append("<li><a href=\"").append(Menu.HUB.page).append("\">").append(Messages.get(Menu.HUB.caption)).append("</a></li>");

		//Katalog Lokal/Sektoral
		content.append("<li><a href=\"").append(Menu.KATALOG_LOKAL_SEKTORAL.page).append("\">").append(Messages.get(Menu.KATALOG_LOKAL_SEKTORAL.caption)).append("</a></li>");

//		Bahasa/English
//		content.append("<li><a href=\"" + changeLangUrl + "?lang=id\">Bahasa Indonesia</a></li>");
//		content.append("<li><a href=\"" + changeLangUrl + "?lang=en\">English</a></li>");

		if(aktifUser != null) {
			//Logout
			content.append("<li><a href=\"").append(Menu.LOGOUT.page).append("\">").append(Messages.get(Menu.LOGOUT.caption)).append("</a></li>");
		} else {
			//Login
			content.append("<li><a href=\"").append(Menu.LOGIN.page).append("\">").append(Messages.get(Menu.LOGIN.caption)).append("</a></li>");
		}

		content.append("</ul>");
		out.print(content.toString());
	}

}
