package models.purchasing.form;

import org.apache.http.util.TextUtils;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by dadang on 11/6/17.
 */
public class PaketForm {

	// tab 2
	public String isEdit;
	public String kode_paket;
	public Integer komoditas_id;
	public String nama_paket;
	public String tahun_anggaran;
	public Long jenis_instansi_id;
	public Long instansi_id;
	public String alamat_satuan_kerja;
	public Long satuan_kerja_kabupaten;
	public String pengiriman_alamat;
	public Long pengiriman_kabupaten;
	public String npwp_satuan_kerja;
	public Long rup_id;
	public String satker_id;
	public String kldi_jenis;
	public String kldi_id;
	public String nama_satuan_kerja;
	public Long id_sumber_dana[];
	public String kode_anggaran[];
	public Long prp_id;
	public Long kbp_id;

	public String instansi;

	public Integer gunakan_alamat_satker;

	// tab 3
	public Long id_pemesan;
	public String nama_pemesan;
	public String nip_pemesan;
	public String email_pemesan;
	public String no_telp_pemesan;
	public String no_sert_pbj_pemesan;
	public Long ulp_anggota_id[];
	public String ulp_anggota_nip[];
	public String jabatan_panitia;

	// tab 4
	public String nip_ppk;
	public Long id_ppk;
	public String nama_ppk;
	public String jabatan_ppk;
	public String email_ppk;
	public String no_telp_ppk;
	public String no_sert_pbj_ppk;

	// tab 5
	public Long produk_id[];
	public Long produk_daerah_id[];
	public String produk_kelas_harga[];
	public BigDecimal produk_kuantitas[];
	public Double produk_harga_satuan[];
	public Double produk_harga_satuan_konversi[];
	public String produk_catatan[];
	public Long paket_produk_id[]; // kebutuhan edit
	public BigDecimal harga_ongkir[];

	//others
	public Long penyedia_id;
	public Long kurs_id;
	public Long paket_id;

	public Date created_date;

	public String status;
	public String page;

	public boolean isDefaultDraft() {
		return !TextUtils.isEmpty(status) && status.equalsIgnoreCase("draft");
	}

	public boolean isFromEditPage() {
		return !TextUtils.isEmpty(page) && page.equalsIgnoreCase("edit");
	}

}
