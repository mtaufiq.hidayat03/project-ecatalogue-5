package models.permohonan;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;

public class ISO8601DateAdapter implements JsonSerializer<Date>, JsonDeserializer<Date> {
    public ISO8601DateAdapter() {
    }

    @Override
    public Date deserialize(JsonElement json, Type type, JsonDeserializationContext jsonDeserializationContext) {
        Date date = deserializeToDate(json);
        if (type == Date.class) {
            return date;
        } else if (type == Timestamp.class) {
            return new Timestamp(date.getTime());
        } else if (type == java.sql.Date.class) {
            return new java.sql.Date(date.getTime());
        } else {
            throw new IllegalArgumentException(getClass() + " cannot deserialize to " + type);
        }
    }

    private Date deserializeToDate(JsonElement json) {
        try {
            return ISO8601DateParser.parse(json.getAsString());
        } catch (ParseException e) {
            throw new JsonSyntaxException(json.getAsString(), e);
        }
    }

    @Override
    public JsonElement serialize(Date date, Type type, JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive(ISO8601DateParser.format(date));
    }
}
