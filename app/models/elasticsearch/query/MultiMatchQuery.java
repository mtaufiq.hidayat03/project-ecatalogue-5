package models.elasticsearch.query;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author HanusaCloud on 10/11/2017
 */
public class MultiMatchQuery extends BoolShould {

    private static final String MULTI_MATCH = "multi_match";
    public static final String TYPE_BEST_FIELD = "best_fields";

    private String type = "most_fields";
    private String query;
    private String operator = ElasticsearchUtil.AND_OPERATOR;
    private Double boost;
    private Double fuzziness;
    private List<String> fields = new ArrayList<>();

    public MultiMatchQuery query(String query) {
        this.query = query;
        return this;
    }

    public MultiMatchQuery type(String type) {
        this.type = type;
        return this;
    }

    public MultiMatchQuery fields(String[] strings) {
        this.fields.addAll(Arrays.asList(strings));
        return this;
    }

    public MultiMatchQuery fuziness(Double fuzziness) {
        this.fuzziness = fuzziness;
        return this;
    }

    public MultiMatchQuery boost(Double boost) {
        this.boost = boost;
        return this;
    }

    public MultiMatchQuery operator(String operator) {
        this.operator = operator;
        return this;
    }

    @Override
    public JsonObject build() {
        JsonObject result = new JsonObject();
        JsonObject multiMatch = new JsonObject();
        multiMatch.addProperty("query", query);
        multiMatch.addProperty("type", type);
        multiMatch.addProperty("operator", operator);
        if (boost != null) {
            multiMatch.addProperty("boost", boost);
        }
        if (fuzziness != null) {
            multiMatch.addProperty("fuzziness", fuzziness);
        }
        if (!fields.isEmpty()) {
            JsonArray jsonArray = new JsonArray();
            for (String s : fields) {
                jsonArray.add(s);
            }
            multiMatch.add("fields", jsonArray);
        }
        result.add(MULTI_MATCH, multiMatch);
        return result;
    }

}
