package models.prakatalog;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

@Table(name = "usulan_pokja")
public class UsulanPokja extends BaseKatalogTable {
    @Id
    public Long id;
    public Long usulan_id;
    public Long penawaran_id;
    public Long pokja_id;
    public Long active;

    @Transient
    public String nama_usulan;

    @Transient
    public String nama_pokja;

    @Transient
    public Long user_id;

    public static List<UsulanPokja> findByPenawaranId(Long penawaranId){
                String query="SELECT usl.id usulan_id, pnw.user_id, pnw.id penawaran_id, up.pokja_id, u.nama_lengkap nama_pokja FROM penawaran pnw " +
                "JOIN usulan usl ON pnw.usulan_id = usl.id " +
                "JOIN usulan_pokja up ON usl.id = up.usulan_id " +
                "JOIN user u ON up.pokja_id = u.id " +
                "where pnw.id = ? " +
                "ORDER BY u.nama_lengkap";
        return Query.find(query,UsulanPokja.class,penawaranId).fetch();
    }

    public static UsulanPokja getByUsulanPokja(Long usulanId, Long pokjaId){
        String query = "select up.* from usulan_pokja up where up.usulan_id = ? and up.pokja_id = ? ";
        return Query.find(query, UsulanPokja.class, usulanId, pokjaId).first();
    }

    public static UsulanPokja getAssignPokjaByUsulanId(Long usulanId){
        String query = "select * from usulan_pokja where usulan_id = ? ";
        return Query.find(query, UsulanPokja.class, usulanId).first();
    }
}
