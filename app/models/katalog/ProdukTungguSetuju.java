package models.katalog;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.LogUtil;

import java.util.List;

/**
 * Created by dadang on 8/31/17.
 */
@Table(name = "produk_tunggu_setuju")
public class ProdukTungguSetuju extends BaseKatalogTable {

	public static final String NEW = "new";
	public static final String WAITING_FOR_APPROVAL = "minta_persetujuan";

	@Id
	public Long id;
	public Long produk_id;
	public String area;
	public String deskripsi;
	public String deskripsi_param;
	public String jenis_aksi;
	public boolean active;

	public ProdukTungguSetuju() {}

	public ProdukTungguSetuju setNew(long produk_id) {
		this.produk_id = produk_id;
		this.area = "produk";
		this.deskripsi = "Pengajuan Produk";
		this.jenis_aksi = NEW;
		this.active = true;
		return this;
	}

	public ProdukTungguSetuju setApproval(long produk_id) {
		this.produk_id = produk_id;
		this.area = "produk";
		this.deskripsi = "Pengajuan Produk";
		this.jenis_aksi = WAITING_FOR_APPROVAL;
		this.active = true;
		return this;
	}

	public static int deleteProdTunggu(long produk_id){
		String query = "delete from produk_tunggu_setuju where produk_id = ?";

		return Query.update(query, produk_id);
	}

	public static long countByProdukId(long produk_id){
		return ProdukTungguSetuju.count("produk_id = ?",produk_id);
	}

	public static void deleteAllByPenawaran(long penawaran_id){
		List<Produk> produkList = Produk.findByPenawaranAndMenungguPersetujuan(penawaran_id);
		if (!produkList.isEmpty()) {
			String ids = getIds(produkList);
			StringBuilder sb = new StringBuilder("DELETE FROM produk_tunggu_setuju WHERE produk_id IN (").append(ids).append(")");
			LogUtil.d("query", sb.toString());
			Query.update(sb.toString());
		} else {
			LogUtil.d("ProdukTungguSetuju", "no need to remove products");
		}
	}

	private static String getIds(List<Produk> produkList){
		StringBuilder sb = new StringBuilder();
		if (!produkList.isEmpty()) {
			String glue = "";
			for (Produk model : produkList) {
				sb.append(glue).append("'").append(model.id).append("'");
				glue = ",";
			}
		}
		return sb.toString();
	}

}
