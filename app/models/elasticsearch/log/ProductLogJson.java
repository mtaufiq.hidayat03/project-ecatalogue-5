package models.elasticsearch.log;

import com.google.gson.annotations.SerializedName;
import models.elasticsearch.product.ProductCategoryElastic;
import models.elasticsearch.product.ProductPriceElastic;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

import static models.katalog.Komoditas.*;

/**
 * @author HanusaCloud on 10/2/2017
 */

public class ProductLogJson {

    @SerializedName("produk_id")
    public Long id;
    public String nama_produk;
    public String no_produk;
    public String no_produk_penyedia;
    public Long komoditas_id;
    public String kelas_harga;
    public String nama_komoditas;
    public transient Long produk_kategori_id;
    public String nama_kategori;
    public Long manufaktur_id;
    public String nama_manufaktur;
    public Long penyedia_id;
    public String nama_penyedia;
    public Long total_pembelian = 0L;
    public transient Long harga_utama;
    public transient Long harga_pemerintah;
    public double stock = 0;
    public String expired;
    public String image_url;
    @SerializedName("@timestamp")
    public String timestamp;
    public long status = 1;
    public String created_at;
    public String updated_at;
    public String jenis_produk;
    public long view_page_count = 0;
    public String produk_api_url;
    public Boolean is_umkm = false;
    @Transient
    public List<ProductPriceElastic> priceList = new ArrayList<>();
    @Transient
    public List<ProductCategoryElastic> categories = new ArrayList<>();

    public ProductLogJson() {}

    public boolean isNational() {
        return this.kelas_harga.equalsIgnoreCase(NASIONAL);
    }

    public boolean isProvince() {
        return this.kelas_harga.equalsIgnoreCase(PROVINSI);
    }

    public boolean isRegency() {
        return this.kelas_harga.equalsIgnoreCase(KABUPATEN);
    }
    public boolean isAllowedToSetGeneralPrice() {
        //return harga_utama > 0 && harga_pemerintah > 0;
        //perbaikan logic
        return isNational();
    }

    public void setDefaultCategoryPosition() {
        if (categories != null && !categories.isEmpty()) {
            for (ProductCategoryElastic model : categories) {
                if (model.id == produk_kategori_id) {
                    model.current = "1";
                    break;
                }
            }
        }
    }

    public String getIdToString(){
        return this.id.toString();
    }

}
