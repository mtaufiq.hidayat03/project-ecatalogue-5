package controllers.admin;

import java.util.*;

import com.google.gson.Gson;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.katalog.ProdukKategoriAtribut;

import models.katalog.komoditasmodel.KomoditasProdukAtributTipe;
import models.katalog.payload.ProdukKategoriAtributPayload;
import models.secman.Acl;
import play.Logger;
import repositories.produkkategori.ProdukKategoriAtributRepository;
import services.produkkategori.ProdukKategoriAtributeService;

public class ProdukKategoriAtributCtr extends BaseController {

    public static final String TAG = "ProdukKategoriAtributCtr";

    @AllowAccess({Acl.COM_KOMODITAS_HARGA_ATRIBUT})
    public static void create(long kid, long pkid) {
        ProdukKategoriAtributPayload model = new ProdukKategoriAtributPayload();
        model.kid = kid;
        model.pkid = pkid;
        List<ProdukKategoriAtribut> results = ProdukKategoriAtributRepository.findAllActive(pkid, "");
        Map<Long, KomoditasProdukAtributTipe> types = KomoditasProdukAtributTipe.getAllByCommodityMap(kid);
        Logger.debug(TAG + " total: " + results.size());
        render("admin/ProdukKategoriAtributCtr/create.html", model, results, types);
    }

    @AllowAccess({Acl.COM_KOMODITAS_HARGA_ATRIBUT})
    public static void createSubmit(ProdukKategoriAtributPayload model) {
        Logger.debug(TAG + " " + new Gson().toJson(model));
        try {
            ProdukKategoriAtributeService.save(model);
            flash.success("Data berhasil disimpan.");
        } catch (Exception e) {
            e.printStackTrace();
            flash.error("Data gagal disimpan, periksa kembali!");
        }
        create(model.kid, model.pkid);

    }

    @AllowAccess({Acl.COM_KOMODITAS_HARGA_ATRIBUT})
    public static void createPosisi(long kid, long pkid) {
        List<ProdukKategoriAtribut> results = ProdukKategoriAtributRepository.getNestedList(pkid);
        Logger.debug(TAG + " total: " + results.size());
        render("admin/ProdukKategoriAtributCtr/createPosisi.html", results, pkid, kid);
    }

    @AllowAccess({Acl.COM_KOMODITAS_HARGA_ATRIBUT})
    public static void createPosisiSubmit(long pkid, long kid, String positions) {
        try {
            ProdukKategoriAtributeService.savePosisi(positions, pkid);
            flash.success("Data berhasil disimpan.");
        } catch (Exception e) {
            e.printStackTrace();
            flash.error("Data gagal disimpan, periksa kembali!");
        }
        createPosisi(kid, pkid);
    }

    @AllowAccess({Acl.COM_KOMODITAS_HARGA_ATRIBUT})
    public static void delete(long pkid) {
        Logger.debug(TAG +" delete by id: " + pkid);
        boolean status = ProdukKategoriAtributeService.delete(pkid);
        Map<String, Object> result = new HashMap<>();
        result.put("id", pkid);
        result.put("status", status);
        renderJSON(result);
    }

}
