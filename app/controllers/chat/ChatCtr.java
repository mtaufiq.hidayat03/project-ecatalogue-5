package controllers.chat;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.chat.Broadcast;
import models.chat.BroadcastDetail;
import models.chat.Chat;
import models.common.AktifUser;
import models.katalog.Komoditas;
import models.katalog.Produk;
import models.katalog.ProdukDiskusi;
import models.penyedia.Penyedia;
import models.penyedia.PenyediaKomoditas;
import models.purchasing.Paket;
import models.secman.Acl;
import models.user.User;
import models.user.UserRoleGrup;
import models.util.Config;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.RandomStringUtils;
import play.Play;
import play.data.binding.As;
import play.i18n.Messages;
import utils.UrlUtil;

import java.io.*;
import java.time.LocalDateTime;
import java.util.*;

public class ChatCtr extends BaseController {
    private static final String TAG = "ChatCtr";
    static String rootPath = Play.applicationPath.getAbsolutePath();
    static String chatStorage = Config.getInstance().storageChat;

    protected static String fileChat = rootPath+chatStorage;
	public static void fetchNewChat(Long paket_id_post, Integer count){
        JsonObject jsonChat = new JsonObject();
        if (getAktifUser() != null) {
            AktifUser aktifUser = getAktifUser();
            Chat chat;
            if (aktifUser.isPpk()){
                chat = Chat.findByIdPaketPpk(aktifUser.user_id, paket_id_post);
            }else {
                chat = Chat.findByIdPengirimAndIdPaket(aktifUser.user_id, paket_id_post);
                if (chat == null){
                    chat = Chat.findByIdPenerimaAndIdPaket(aktifUser.user_id, paket_id_post);
                }
            }

            List<String> listArrayChatFile = new ArrayList<String>(20);
            List<String> noticeNavbar = new ArrayList<String>(20);
            if (chat != null) {
                File file = new File(fileChat + chat.file);
                if (file.isFile()){
                    try {
                        Scanner scannerCountRows = new Scanner(file);
                        Scanner scannerGetData = new Scanner(file);
                        int i = 0;
                        int lineNum = 0;
                        while (scannerCountRows.hasNextLine()) {
                            String line = scannerCountRows.nextLine();
                            Scanner lineScanner = new Scanner(line);
                            lineScanner.useDelimiter(";");

                            //menghitung jumlah baris
                            while (lineScanner.hasNext()) {
                                lineScanner.next();
                                lineNum += 1;
                            }
                        }
                        String[] splitFile = new String[lineNum];
                        //memasukan data kedalam array
                        while (scannerGetData.hasNextLine()) {
                            String lineGetData = scannerGetData.nextLine();
                            Scanner lineScannerGetData = new Scanner(lineGetData);
                            lineScannerGetData.useDelimiter(";");
                            while (lineScannerGetData.hasNext()) {
                                String part = lineScannerGetData.next();
                                splitFile[i] = part;
                                i++;
                            }
                        }
                        int num = 0;
                        while (num < lineNum) {
                            String[] splitRows = splitFile[num].split("@@@");
                            String idPenerima = "" + aktifUser.user_id;
                            if (splitRows[1].equals(idPenerima)) {
                                User user = User.findById(splitRows[1]);
                                listArrayChatFile.add("<div class=\"row msg_container base_sent\">\n" + "    <div class=\"col-md-10 col-xs-10\">\n" + "        <div class=\"messages msg_sent\">\n" + " <b> "+user.nama_lengkap+" </b><hr>           <p class=\"isi_pesan\" id=\"isi_pesan\">" + splitRows[2] + "</p>\n" + "            <time datetime=\"2009-11-13T20:00\">" + splitRows[0] + "</time>\n" + "        </div>\n" + "    </div>\n" + "    <div class=\"col-md-2 col-xs-2 avatar\">\n" + "        <img src=\"/public/images/icon-chat.jpg\" class=\" img-responsive \">\n" + "    </div>\n" + "</div>");
                            } else {
                                User user = User.findById(splitRows[1]);
                                listArrayChatFile.add("<div class=\"row msg_container base_receive\">\n" + "                        <div class=\"col-md-2 col-xs-2 avatar\">\n" + "                            <img src=\"/public/images/icon-chat.jpg\" class=\" img-responsive \">\n" + "                        </div>\n" + "                        <div class=\"col-md-10 col-xs-10\">\n" + "                            <div class=\"messages msg_receive\">\n" + " <b> "+user.nama_lengkap+"</b><hr>                                <p>" + splitRows[2] + "</p>\n" + "                                <time datetime=\"2009-11-13T20:00\">" + splitRows[0] + "</time>\n" + "                            </div>\n" + "                        </div>\n" + "                    </div>");
                            }
                            noticeNavbar.add("<li><a href=\"#\"> Pesan " + num + " </a></li>");
                            num++;
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }else {
                    listArrayChatFile.add("");
                }
            }

            String joined = String.join(" ", listArrayChatFile);
            jsonChat.addProperty("dataChat", joined);
            jsonChat.addProperty("statusUser", "1");
        }else {
            jsonChat.addProperty("dataChat", "");
            jsonChat.addProperty("statusUser", "");
        }
		renderJSON(jsonChat);
	}

	public static void fetchNewChatProduk(Long produk_id_post, String vUser){
        JsonObject jsonChat = new JsonObject();
        if (vUser == null){
            vUser = "0";
        }
        if (getAktifUser() != null) {
            AktifUser aktifUser = getAktifUser();
            Chat chat;
            if (vUser.equals("0")) {
                chat = Chat.findByIdPengirimAndIdProduk(aktifUser.user_id, produk_id_post);
                if (chat == null) {
                    chat = Chat.findByIdPenerimaAndIdProduk(aktifUser.user_id, produk_id_post);
                }
            }else {
                chat = Chat.findByIdPengirimAndIdProdukForLink(aktifUser.user_id, produk_id_post, vUser);
                if (chat == null) {
                    chat = Chat.findByIdPenerimaAndIdProdukForLink(aktifUser.user_id, produk_id_post, vUser);
                }
            }


            List<String> listArrayChatFile = new ArrayList<String>(20);
            List<String> noticeNavbar = new ArrayList<String>(20);
            if (chat != null) {
                File file = new File(fileChat + chat.file);
                if (file.isFile()){
                    try {
                        Scanner scannerCountRows = new Scanner(file);
                        Scanner scannerGetData = new Scanner(file);
                        int i = 0;
                        int lineNum = 0;
                        while (scannerCountRows.hasNextLine()) {
                            String line = scannerCountRows.nextLine();
                            Scanner lineScanner = new Scanner(line);
                            lineScanner.useDelimiter(";");

                            //menghitung jumlah baris
                            while (lineScanner.hasNext()) {
                                lineScanner.next();
                                lineNum += 1;
                            }
                        }
                        String[] splitFile = new String[lineNum];
                        //memasukan data kedalam array
                        while (scannerGetData.hasNextLine()) {
                            String lineGetData = scannerGetData.nextLine();
                            Scanner lineScannerGetData = new Scanner(lineGetData);
                            lineScannerGetData.useDelimiter(";");
                            while (lineScannerGetData.hasNext()) {
                                String part = lineScannerGetData.next();
                                splitFile[i] = part;
                                i++;
                            }
                        }
                        int num = 0;
                        while (num < lineNum) {
                            String[] splitRows = splitFile[num].split("@@@");
                            String idPenerima = "" + aktifUser.user_id;
                            if (splitRows[1].equals(idPenerima)) {
                                User user = User.findById(splitRows[1]);
                                listArrayChatFile.add("<div class=\"row msg_container base_sent\">\n" + "    <div class=\"col-md-10 col-xs-10\">\n" + "        <div class=\"messages msg_sent\">\n" + " <b> "+user.nama_lengkap+" </b><hr>            <p class=\"isi_pesan\" id=\"isi_pesan\">" + splitRows[2] + "</p>\n" + "            <time datetime=\"2009-11-13T20:00\">" + splitRows[0] + "</time>\n" + "        </div>\n" + "    </div>\n" + "    <div class=\"col-md-2 col-xs-2 avatar\">\n" + "        <img src=\"/public/images/icon-chat.jpg\" class=\" img-responsive \">\n" + "    </div>\n" + "</div>");
                            } else {
                                User user = User.findById(splitRows[1]);
                                listArrayChatFile.add("<div class=\"row msg_container base_receive\">\n" + "                        <div class=\"col-md-2 col-xs-2 avatar\">\n" + "                            <img src=\"/public/images/icon-chat.jpg\" class=\" img-responsive \">\n" + "                        </div>\n" + "                        <div class=\"col-md-10 col-xs-10\">\n" + "                            <div class=\"messages msg_receive\">\n" + "<b> "+user.nama_lengkap+" </b><hr>                                <p>" + splitRows[2] + "</p>\n" + "                                <time datetime=\"2009-11-13T20:00\">" + splitRows[0] + "</time>\n" + "                            </div>\n" + "                        </div>\n" + "                    </div>");
                            }
                            noticeNavbar.add("<li><a href=\"#\"> Pesan " + num + " </a></li>");
                            num++;
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }else {
                    listArrayChatFile.add("");
                }
            }

            String joined = String.join(" ", listArrayChatFile);

//        get data for navbar
            List<String> listArrayChatBacaPenerima = new ArrayList<String>(1);

            List<Chat> chatForNavbarFromPenerima = Chat.FindDataNavbarCountListProduk(aktifUser.user_id);
            List<Chat> chatForNavbarFromPengirim = Chat.FinDataNavbarCountListProdukFromPengirim(aktifUser.user_id);
            for (Chat loopChatForNavbar : chatForNavbarFromPenerima) {
                String urlDetail = UrlUtil.builder().setUrl("katalog.ProdukCtr.detail").setParam("id", loopChatForNavbar.produk_id).build();
                listArrayChatBacaPenerima.add("<li><a href=\"" + urlDetail + "\"> " + loopChatForNavbar.pesan_akhir + " </a></li>");
            }
            for (Chat loopChatForNavbarPengirim : chatForNavbarFromPengirim) {
                String urlDetail = UrlUtil.builder().setUrl("katalog.ProdukCtr.detail").setParam("id", loopChatForNavbarPengirim.produk_id).build();
                listArrayChatBacaPenerima.add("<li><a href=\"" + urlDetail + "\"> " + loopChatForNavbarPengirim.pesan_akhir + " </a></li>");
            }
            String urlDetail = UrlUtil.builder().setUrl("chat.ChatCtr.allMessage").build();
            listArrayChatBacaPenerima.add("<li style=\"background:#E5E5E5; font-weight: bold;\"><a href=\"" + urlDetail + "\"> Lihat Semua</a></li>");

            String listCountBacaPenerimaToString = String.join(" ", listArrayChatBacaPenerima);

            jsonChat.addProperty("dataChat", joined);
            jsonChat.addProperty("countNotifPesan", listArrayChatBacaPenerima.size() - 1);
            jsonChat.addProperty("navbarChat", listCountBacaPenerimaToString);
            jsonChat.addProperty("statusUser", "1");
        }else {

            jsonChat.addProperty("dataChat", "");
            jsonChat.addProperty("countNotifPesan", "");
            jsonChat.addProperty("navbarChat", "");
            jsonChat.addProperty("statusUser", "");
        }
		renderJSON(jsonChat);
	}

	public static void fetchNewChatForBorderNavbar(){
        JsonObject jsonChat = new JsonObject();
        jsonChat.addProperty("countNotifPesan", 0);
//        jsonChat.addProperty("navbarChat",0);
		AktifUser aktifUser = getAktifUser();
		if(aktifUser!=null){
            jsonChat = new JsonObject();
//            List<String> noticeNavbar = new ArrayList<String>(20);
//        get data for navbar
//            List<String> listArrayChatBacaPenerima = new ArrayList<String>(1);

//            List<Chat> chatForNavbarFromPenerima = Chat.FindDataNavbarCountListProduk(aktifUser.user_id);
//            List<Chat> chatForNavbarFromPengirim = Chat.FinDataNavbarCountListProdukFromPengirim(aktifUser.user_id);

//            for (Chat loopChatForNavbar : chatForNavbarFromPenerima){
//                String urlDetailPenerima;
//                if (loopChatForNavbar.produk_id != null) {
//                    urlDetailPenerima = UrlUtil.builder().setUrl("katalog.ProdukCtr.detail").setParam("id", loopChatForNavbar.produk_id).build();
//                    listArrayChatBacaPenerima.add("<li><a href=\""+urlDetailPenerima+"?vUser="+loopChatForNavbar.id_pengirim+"\"> "+loopChatForNavbar.pesan_akhir+" </a></li>");
//                }else {
//                    urlDetailPenerima = UrlUtil.builder().setUrl("purchasing.PaketCtr.detail").setParam("paket_id", loopChatForNavbar.paket_id).build();
//                    listArrayChatBacaPenerima.add("<li><a href=\""+urlDetailPenerima+"\"> "+loopChatForNavbar.pesan_akhir+" </a></li>");
//                }
//
//            }
//            for (Chat loopChatForNavbarPengirim : chatForNavbarFromPengirim){
//                String urlDetail;
//                if (loopChatForNavbarPengirim.produk_id != null) {
//                    urlDetail = UrlUtil.builder().setUrl("katalog.ProdukCtr.detail").setParam("id", loopChatForNavbarPengirim.produk_id).build();
//                    listArrayChatBacaPenerima.add("<li><a href=\""+urlDetail+"?vUser="+loopChatForNavbarPengirim.id_penerima+"\"> "+loopChatForNavbarPengirim.pesan_akhir+" </a></li>");
//                }else {
//                    urlDetail = UrlUtil.builder().setUrl("purchasing.PaketCtr.detail").setParam("paket_id", loopChatForNavbarPengirim.paket_id).build();
//                    listArrayChatBacaPenerima.add("<li><a href=\""+urlDetail+"\"> "+loopChatForNavbarPengirim.pesan_akhir+" </a></li>");
//                }
//            }
//            String urlDetail = UrlUtil.builder().setUrl("chat.ChatCtr.allMessage")
//                    .build();
//            listArrayChatBacaPenerima.add("<li style=\"background:#E5E5E5; font-weight: bold;\"><a href=\""+urlDetail+"\"> Lihat Semua</a></li>");

//            String listCountBacaPenerimaToString = String.join(" ", listArrayChatBacaPenerima);

            long checkChatFromPenerima = Chat.checkChatFromPenerima(aktifUser.user_id);
            long checkChatFromPengirim = Chat.checkChatFromPengirim(aktifUser.user_id);

            jsonChat.addProperty("countNotifPesan", checkChatFromPenerima+checkChatFromPengirim);
//            jsonChat.addProperty("navbarChat",listCountBacaPenerimaToString);

        }

		renderJSON(jsonChat);
	}

    public static void createSubmitChatPaket(Long id_chat, String pesan, Long paket_id, Boolean checkChat, String file) throws IOException {

        String ext = "chat-";
        long admin = 2;
        AktifUser aktifUser = getAktifUser();
        LocalDateTime localDate = LocalDateTime.now();
        java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String formattedString = localDate.format(formatter);

        if (checkChat == true){

            Writer output = new BufferedWriter(new FileWriter(fileChat+file, true));
            output.append(formattedString+"@@@"+aktifUser.user_id+"@@@"+pesan+" ;"+"\n");
            output.close();
            Chat chat = Chat.findById(id_chat);
            String userLogin = ""+aktifUser.user_id;
            String userPenerima = ""+chat.id_penerima;
                if (userPenerima.equals(userLogin)){
                    chat.baca_pengirim = 0;
                    chat.baca_penerima = 1;
                    chat.pesan_akhir = pesan;
                    chat.save();
                }else {
                    chat.active = true;
                    chat.baca_pengirim = 1;
                    chat.baca_penerima = 0;
                    chat.pesan_akhir = pesan;
                    chat.save();
                }
            chat.save();

        }else {
            String name = String.format(ext+RandomStringUtils.randomAlphanumeric(8));
            Paket paket = Paket.findById(paket_id);
            Penyedia penyedia = Penyedia.findById(paket.penyedia_id);
            String stringUserPanitiaPaket = ""+paket.panitia_user_id;
            String userLogin = ""+aktifUser.user_id;

            PrintWriter writer = new PrintWriter(fileChat+name+".txt", "UTF-8");
            writer.println(formattedString+"@@@"+aktifUser.user_id+"@@@"+pesan+" ;"+"\n");
            writer.close();
            if (stringUserPanitiaPaket.equals(userLogin)){
                Chat chat = new Chat();
                chat.id_pengirim = aktifUser.user_id;
                chat.id_penerima = penyedia.user_id;
                chat.ppk_user_id = paket.ppk_user_id;
                chat.paket_id = paket_id;
                chat.active = true;
                chat.baca_pengirim = 1;
                chat.baca_penerima = 0;
                chat.file = name+".txt";
                chat.pesan_akhir = pesan;
                chat.save();
            }else {
                Chat chat = new Chat();
                chat.id_pengirim = aktifUser.user_id;
                chat.id_penerima = paket.panitia_user_id;
                chat.ppk_user_id = paket.ppk_user_id;
                chat.paket_id = paket_id;
                chat.active = true;
                chat.baca_pengirim = 1;
                chat.baca_penerima = 0;
                chat.file = name+".txt";
                chat.pesan_akhir = pesan;
                chat.save();
            }
        }
    }

    public static void createSubmitChatProduk(Long user_produk_id, Long id_chat, String pesan, Long produk_id, Boolean checkChat, String file) throws IOException {
        String ext = "chat-";
        AktifUser aktifUser = getAktifUser();
        LocalDateTime localDate = LocalDateTime.now();
        java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String formattedString = localDate.format(formatter);
        if (checkChat == true){

            Writer output = new BufferedWriter(new FileWriter(fileChat+file, true));
            output.append(formattedString+"@@@"+aktifUser.user_id+"@@@"+pesan+" ;"+"\n");
            output.close();
            Chat chat = Chat.findById(id_chat);
            String idPengirim = ""+chat.id_pengirim;
            String userLogin = ""+aktifUser.user_id;

            if (idPengirim.equals(userLogin)){
                chat.baca_penerima = 0;
                chat.baca_pengirim = 1;
                chat.active = true;
                chat.pesan_akhir = pesan;
            }else {
                chat.baca_penerima = 1;
                chat.baca_pengirim = 0;
                chat.active = true;
                chat.pesan_akhir = pesan;
            }
            chat.save();

        }else {
            String name = String.format(ext+RandomStringUtils.randomAlphanumeric(8));

            PrintWriter writer = new PrintWriter(fileChat+name+".txt", "UTF-8");
            writer.println(formattedString+"@@@"+aktifUser.user_id+"@@@"+pesan+" ;"+"\n");
            writer.close();

            Chat chat = new Chat();
            if (aktifUser != null){
                chat.id_pengirim = aktifUser.user_id;
            }
            chat.id_penerima = user_produk_id;
            chat.produk_id = produk_id;
            chat.pesan_akhir = pesan;
            chat.active = true;
            chat.baca_pengirim = 1;
            chat.baca_penerima = 0;
            chat.file = name+".txt";
            chat.save();
        }
    }

    public static void allMessage(){
        AktifUser aktifUser = getAktifUser();
        List<ProdukDiskusi> listProdukDiskusiByUserLogin = ProdukDiskusi.findProdukDiskusiByUserIdLogin(aktifUser.user_id);
        boolean aksesBtnAddBroadcast = aktifUser.isAuthorized("comBroadcastAdd");
        Penyedia penyedia = Penyedia.getProviderByUserId(aktifUser.user_id);
        if (penyedia != null){
            List<PenyediaKomoditas> penyediaKomoditasId = PenyediaKomoditas.findIdsByPenyediaId(penyedia.id);
            List<BroadcastDetail> listBroadcastDetail = BroadcastDetail.findBroadcastDetailByUserIdLoginUserRoleIdKomoditasId(aktifUser.user_id, aktifUser.user_role_grup_id, penyediaKomoditasId);
            int countLoop = 0;
            boolean dotAkses = false;
            for (BroadcastDetail listBroadcastDetails : listBroadcastDetail){
                if (listBroadcastDetails.is_read == null || !listBroadcastDetails.is_read){
                    countLoop++;
                }
            }
            if (countLoop > 0){
                dotAkses = true;
            }
//            for (BroadcastDetail listBroadcastDetails1 : listBroadcastDetail){
//                System.out.println(listBroadcastDetails1.broadcast_judul);
//                System.out.println(listBroadcastDetails1.is_read);
//                System.out.println("-------------------------");
//            }

            render(listBroadcastDetail, aksesBtnAddBroadcast, listProdukDiskusiByUserLogin, dotAkses);
        }else{
            List<BroadcastDetail> listBroadcastDetail = BroadcastDetail.findBroadcastDetailByUserIdLoginUserRoleIdKomoditasId(aktifUser.user_id, aktifUser.user_role_grup_id,null);
            int countLoop = 0;
            boolean dotAkses = false;
            for (BroadcastDetail listBroadcastDetails : listBroadcastDetail){
                if (listBroadcastDetails.is_read == null || !listBroadcastDetails.is_read){
                    countLoop++;
                }
            }
            if (countLoop > 0){
                dotAkses = true;
            }

//            for (BroadcastDetail listBroadcastDetails1 : listBroadcastDetail){
//                System.out.println(listBroadcastDetails1.broadcast_judul);
//                System.out.println(listBroadcastDetails1.is_read);
//                System.out.println(listBroadcastDetails1.user_id);
//                System.out.println("-------------------------");
//            }

            render(listBroadcastDetail, aksesBtnAddBroadcast, listProdukDiskusiByUserLogin, dotAkses);
        }
    }

    public static void getDataAllMessage(){
        JsonObject jsonChat = new JsonObject();
        if (getAktifUser() != null) {
            List<String> listArrayChatBacaPenerima = new ArrayList<String>(1);
            if (getAktifUser().isPpk()) {
                AktifUser aktifUser = getAktifUser();
                List<Chat> listAllMessagePpkPaket = Chat.listAllMessagePpkPaket(aktifUser.user_id);
                List<Chat> listAllMessagePpkProduk = Chat.listAllMessagePpkProduk(aktifUser.user_id);
                for (Chat loopChatForNavbar : listAllMessagePpkPaket) {
                    Date dateCreateOrUpdate;
                    if (loopChatForNavbar.modified_date != null) {
                        dateCreateOrUpdate = loopChatForNavbar.modified_date;
                    } else {
                        dateCreateOrUpdate = loopChatForNavbar.created_date;
                    }
                    if (loopChatForNavbar.produk_id != null) {
                        Produk produk = Produk.findById(loopChatForNavbar.produk_id);
                        String urlDetail = UrlUtil.builder().setUrl("katalog.ProdukCtr.detail").setParam("id", loopChatForNavbar.produk_id).build();
                        listArrayChatBacaPenerima.add("<div class=\"chat_list\">\n" + "    <div class=\"chat_people\">\n" + "        <div class=\"chat_img\"> <img src=\"/public/images/icon-chat-list.png\" alt=\"sunil\"> </div>\n" + "        <div class=\"chat_ib\">\n" + "            <h5><a href=\"" + urlDetail + "?vUser=" + loopChatForNavbar.id_pengirim + "\">" + produk.nama_produk + "</a><span class=\"chat_date\">" + dateCreateOrUpdate + "</span></h5>\n" + "            <p onclick=\"viewGetDataOneMessage(" + loopChatForNavbar.id + ", " + getAktifUser().user_id + ")\" >" + loopChatForNavbar.pesan_akhir + "</p>\n" + "        </div>\n" + "    </div>\n" + "</div>");
                    }

                    if (loopChatForNavbar.paket_id != null) {
                        Paket paket = Paket.findById(loopChatForNavbar.paket_id);
                        String urlDetail = UrlUtil.builder().setUrl("purchasing.PaketCtr.detail").setParam("paket_id", loopChatForNavbar.paket_id).build();
                        listArrayChatBacaPenerima.add("<div class=\"chat_list\">\n" + "    <div class=\"chat_people\">\n" + "        <div class=\"chat_img\"> <img src=\"/public/images/icon-chat-list.png\" alt=\"sunil\"> </div>\n" + "        <div class=\"chat_ib\">\n" + "            <h5><a href=\"" + urlDetail + "\">" + paket.nama_paket + "</a><span class=\"chat_date\">" + dateCreateOrUpdate + "</span></h5>\n" + "            <p onclick=\"viewGetDataOneMessage(" + loopChatForNavbar.id + ", " + getAktifUser().user_id + ")\" >" + loopChatForNavbar.pesan_akhir + "</p>\n" + "        </div>\n" + "    </div>\n" + "</div>");
                    }
                }
                for (Chat loopListAllMessagePpkProduk: listAllMessagePpkProduk) {
                    Date dateCreateOrUpdate;
                    if (loopListAllMessagePpkProduk.modified_date != null) {
                        dateCreateOrUpdate = loopListAllMessagePpkProduk.modified_date;
                    } else {
                        dateCreateOrUpdate = loopListAllMessagePpkProduk.created_date;
                    }
                    if (loopListAllMessagePpkProduk.produk_id != null) {
                        Produk produk = Produk.findById(loopListAllMessagePpkProduk.produk_id);
                        String urlDetail = UrlUtil.builder().setUrl("katalog.ProdukCtr.detail").setParam("id", loopListAllMessagePpkProduk.produk_id).build();
                        listArrayChatBacaPenerima.add("<div class=\"chat_list\">\n" + "    <div class=\"chat_people\">\n" + "        <div class=\"chat_img\"> <img src=\"/public/images/icon-chat-list.png\" alt=\"sunil\"> </div>\n" + "        <div class=\"chat_ib\">\n" + "            <h5><a href=\"" + urlDetail + "?vUser=" + loopListAllMessagePpkProduk.id_pengirim + "\">" + produk.nama_produk + "</a><span class=\"chat_date\">" + dateCreateOrUpdate + "</span></h5>\n" + "            <p onclick=\"viewGetDataOneMessage(" + loopListAllMessagePpkProduk.id + ", " + getAktifUser().user_id + ")\" >" + loopListAllMessagePpkProduk.pesan_akhir + "</p>\n" + "        </div>\n" + "    </div>\n" + "</div>");
                    }

                    if (loopListAllMessagePpkProduk.paket_id != null) {
                        Paket paket = Paket.findById(loopListAllMessagePpkProduk.paket_id);
                        String urlDetail = UrlUtil.builder().setUrl("purchasing.PaketCtr.detail").setParam("paket_id", loopListAllMessagePpkProduk.paket_id).build();
                        listArrayChatBacaPenerima.add("<div class=\"chat_list\">\n" + "    <div class=\"chat_people\">\n" + "        <div class=\"chat_img\"> <img src=\"/public/images/icon-chat-list.png\" alt=\"sunil\"> </div>\n" + "        <div class=\"chat_ib\">\n" + "            <h5><a href=\"" + urlDetail + "\">" + paket.nama_paket + "</a><span class=\"chat_date\">" + dateCreateOrUpdate + "</span></h5>\n" + "            <p onclick=\"viewGetDataOneMessage(" + loopListAllMessagePpkProduk.id + ", " + getAktifUser().user_id + ")\" >" + loopListAllMessagePpkProduk.pesan_akhir + "</p>\n" + "        </div>\n" + "    </div>\n" + "</div>");
                    }
                }
            }else {
                AktifUser aktifUser = getAktifUser();
                List<String> noticeNavbar = new ArrayList<String>(20);

                List<Chat> chatForNavbarFromPenerima = Chat.listAllMessagePenerima(aktifUser.user_id);
                List<Chat> chatForNavbarFromPengirim = Chat.listAllMessagePengirim(aktifUser.user_id);

                for (Chat loopChatForNavbar : chatForNavbarFromPenerima) {
                    Date dateCreateOrUpdate;
                    if (loopChatForNavbar.modified_date != null) {
                        dateCreateOrUpdate = loopChatForNavbar.modified_date;
                    } else {
                        dateCreateOrUpdate = loopChatForNavbar.created_date;
                    }
                    if (loopChatForNavbar.produk_id != null) {
                        Produk produk = Produk.findById(loopChatForNavbar.produk_id);
                        String urlDetail = UrlUtil.builder().setUrl("katalog.ProdukCtr.detail").setParam("id", loopChatForNavbar.produk_id).build();
                        listArrayChatBacaPenerima.add("<div class=\"chat_list\">\n" + "    <div class=\"chat_people\">\n" + "        <div class=\"chat_img\"> <img src=\"/public/images/icon-chat-list.png\" alt=\"sunil\"> </div>\n" + "        <div class=\"chat_ib\">\n" + "            <h5><a href=\"" + urlDetail + "?vUser=" + loopChatForNavbar.id_pengirim + "\">" + produk.nama_produk + "</a><span class=\"chat_date\">" + dateCreateOrUpdate + "</span></h5>\n" + "            <p onclick=\"viewGetDataOneMessage(" + loopChatForNavbar.id + ", " + getAktifUser().user_id + ")\" >" + loopChatForNavbar.pesan_akhir + "</p>\n" + "        </div>\n" + "    </div>\n" + "</div>");
                    }

                    if (loopChatForNavbar.paket_id != null) {
                        Paket paket = Paket.findById(loopChatForNavbar.paket_id);
                        String urlDetail = UrlUtil.builder().setUrl("purchasing.PaketCtr.detail").setParam("paket_id", loopChatForNavbar.paket_id).build();
                        listArrayChatBacaPenerima.add("<div class=\"chat_list\">\n" + "    <div class=\"chat_people\">\n" + "        <div class=\"chat_img\"> <img src=\"/public/images/icon-chat-list.png\" alt=\"sunil\"> </div>\n" + "        <div class=\"chat_ib\">\n" + "            <h5><a href=\"" + urlDetail + "\">" + paket.nama_paket + "</a><span class=\"chat_date\">" + dateCreateOrUpdate + "</span></h5>\n" + "            <p onclick=\"viewGetDataOneMessage(" + loopChatForNavbar.id + ", " + getAktifUser().user_id + ")\" >" + loopChatForNavbar.pesan_akhir + "</p>\n" + "        </div>\n" + "    </div>\n" + "</div>");
                    }
                }
                for (Chat loopChatForNavbarPengirim : chatForNavbarFromPengirim) {
                    Date dateCreateOrUpdate;
                    if (loopChatForNavbarPengirim.modified_date != null) {
                        dateCreateOrUpdate = loopChatForNavbarPengirim.modified_date;
                    } else {
                        dateCreateOrUpdate = loopChatForNavbarPengirim.created_date;
                    }
                    if (loopChatForNavbarPengirim.produk_id != null) {
                        Produk produk = Produk.findById(loopChatForNavbarPengirim.produk_id);
                        String urlDetail = UrlUtil.builder().setUrl("katalog.ProdukCtr.detail").setParam("id", loopChatForNavbarPengirim.produk_id).build();
                        listArrayChatBacaPenerima.add("<div class=\"chat_list\">\n" + "    <div class=\"chat_people\">\n" + "        <div class=\"chat_img\"> <img src=\"/public/images/icon-chat-list.png\" alt=\"sunil\"> </div>\n" + "        <div class=\"chat_ib\">\n" + "            <h5><a href=\"" + urlDetail + "?vUser=" + loopChatForNavbarPengirim.id_penerima + "\">" + produk.nama_produk + "</a><span class=\"chat_date\">" + dateCreateOrUpdate + "</span></h5>\n" + "            <p onclick=\"viewGetDataOneMessage(" + loopChatForNavbarPengirim.id + ", " + getAktifUser().user_id + ")\" >" + loopChatForNavbarPengirim.pesan_akhir + "</p>\n" + "        </div>\n" + "    </div>\n" + "</div>");
                    }
                    if (loopChatForNavbarPengirim.paket_id != null) {
                        Paket paket = Paket.findById(loopChatForNavbarPengirim.paket_id);
                        String urlDetail = UrlUtil.builder().setUrl("purchasing.PaketCtr.detail").setParam("paket_id", loopChatForNavbarPengirim.paket_id).build();
                        listArrayChatBacaPenerima.add("<div class=\"chat_list\">\n" + "    <div class=\"chat_people\">\n" + "        <div class=\"chat_img\"> <img src=\"/public/images/icon-chat-list.png\" alt=\"sunil\"> </div>\n" + "        <div class=\"chat_ib\">\n" + "            <h5><a href=\"" + urlDetail + "\">" + paket.nama_paket + "</a><span class=\"chat_date\">" + dateCreateOrUpdate + "</span></h5>\n" + "            <p onclick=\"viewGetDataOneMessage(" + loopChatForNavbarPengirim.id + ", " + getAktifUser().user_id + ")\" >" + loopChatForNavbarPengirim.pesan_akhir + "</p>\n" + "        </div>\n" + "    </div>\n" + "</div>");
                    }
                }
            }
            String listDataChatLeft = String.join(" ", listArrayChatBacaPenerima);
            jsonChat.addProperty("dataChatLeft", listDataChatLeft);
            jsonChat.addProperty("statusUser", "1");

        }else {
            jsonChat.addProperty("statusUser", "");
        }

        renderJSON(jsonChat);
    }

    public static void getDataOneMessage(Long chat_id, Long user_id){
        Chat chat =Chat.findById(chat_id);

        File file = new File(fileChat + chat.file);
        List<String> listArrayChatOneMessage = new ArrayList<String>(20);
        try {
            Scanner countRowsOneMessage = new Scanner(file);
            Scanner scannerGetDataOneMessage = new Scanner(file);
            int i = 0;
            int lineNum = 0;
            while (countRowsOneMessage.hasNextLine()) {
                String line = countRowsOneMessage.nextLine();
                Scanner lineScanner = new Scanner(line);
                lineScanner.useDelimiter(";");
                //menghitung jumlah baris
                while (lineScanner.hasNext()) {
                    lineScanner.next();
                    lineNum += 1;
                }
            }
            String[] splitFile = new String[lineNum];
            //memasukan data kedalam array
            while (scannerGetDataOneMessage.hasNextLine()) {
                String lineGetData = scannerGetDataOneMessage.nextLine();
                Scanner lineScannerGetData = new Scanner(lineGetData);
                lineScannerGetData.useDelimiter(";");
                while (lineScannerGetData.hasNext()) {
                    String part = lineScannerGetData.next();
                    splitFile[i] = part;
                    i++;
                }
            }
            int num = 0;
            while (num < lineNum) {
                String[] splitRows = splitFile[num].split("@@@");

                String idPenerima = ""+user_id;
                if (splitRows[1].equals(idPenerima)){
                    listArrayChatOneMessage.add("<div class=\"outgoing_msg\">\n" + "                        <input type=\"hidden\" name=\"chat_id_one\" class=\"chat_id_one\" value="+chat_id+"><div class=\"sent_msg\">\n" + "                            <p>"+splitRows[2]+"</p>\n" + "                            <span class=\"time_date\"> "+splitRows[0]+"</span> </div>\n" + "                    </div>");
                }else{
                   // listArrayChatOneMessage.add("<div class=\"incoming_msg\">\n" + "                        <input type=\"hidden\" name=\"chat_id_one\" class=\"chat_id_one\" value="+chat_id+"><div class=\"incoming_msg_img\"> <img src=\"/public/images/icon-chat-list.png\" alt=\"sunil\"> </div>\n" + "                        <div class=\"received_msg\">\n" + "                            <div class=\"received_withd_msg\">\n" + "                                <p>"+splitRows[2]+"</p>\n" + "                                <span class=\"time_date\"> "+splitRows[0]+"</span></div>\n" + "                        </div>\n" + "                    </div>");
                    User namaPengirimPesan = User.findById(splitRows[1]);
                    listArrayChatOneMessage.add("<div class=\"incoming_msg\">\n" +
                                                "    <input type=\"hidden\" name=\"chat_id_one\" class=\"chat_id_one\" value=\""+chat_id+"\">\n" +
                                                "    <div class=\"incoming_msg_img\"> \n" +
                                                "    \t<img src=\"/public/images/icon-chat-list.png\" alt=\"sunil\"> \n" +
                                                "    </div>\n" +
                                                "    <div class=\"received_msg\">\n" +
                                                "        <div class=\"received_withd_msg\">\n" +
                                                "            <p>\n" +
                                                "\t\t\t\t<b>\n" +
                                                "\t\t\t\t\t"+ namaPengirimPesan.nama_lengkap +"\n" +
                                                "\t\t\t\t</b>\n" +
                                                "\t\t\t\t<br>\n" +
                                                "\t\t\t\t"+splitRows[2]+" \n" +
                                                "\t\t\t</p>\n" +
                                                "            <span class=\"time_date\"> "+splitRows[0]+"</span>\n" +
                                                "        </div>\n" +
                                                "    </div>\n" +
                                                "</div>");
                }
                num++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String listArrayChatOneMessageToString = String.join(" ", listArrayChatOneMessage);

        JsonObject jsonChat = new JsonObject();
        jsonChat.addProperty("dataChatOneMessage", listArrayChatOneMessageToString);
        renderJSON(jsonChat);
    }

    public static void submitDataOneMessage(String pesan, Long chat_id) throws IOException {
        Chat chat = Chat.findById(chat_id);
        AktifUser aktifUser = getAktifUser();
        LocalDateTime localDate = LocalDateTime.now();
        java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String formattedString = localDate.format(formatter);
        Writer output = new BufferedWriter(new FileWriter(fileChat+chat.file, true));
        output.append(formattedString+"@@@"+aktifUser.user_id+"@@@"+pesan+" ;"+"\n");
        output.close();

        String idPengirim = ""+chat.id_pengirim;
        String userLogin = ""+aktifUser.user_id;

        if (idPengirim.equals(userLogin)){
            chat.baca_penerima = 0;
            chat.baca_pengirim = 1;
            chat.active = true;
            chat.pesan_akhir = pesan;
        }else {
            chat.baca_penerima = 1;
            chat.baca_pengirim = 0;
            chat.active = true;
            chat.pesan_akhir = pesan;
        }
        chat.save();

//        reload data untuk di tampilkan kembali

        File file = new File(fileChat + chat.file);
        List<String> listArrayChatOneMessage = new ArrayList<String>(20);
        try {
            Scanner countRowsOneMessage = new Scanner(file);
            Scanner scannerGetDataOneMessage = new Scanner(file);
            int i = 0;
            int lineNum = 0;
            while (countRowsOneMessage.hasNextLine()) {
                String line = countRowsOneMessage.nextLine();
                Scanner lineScanner = new Scanner(line);
                lineScanner.useDelimiter(";");
                //menghitung jumlah baris
                while (lineScanner.hasNext()) {
                    lineScanner.next();
                    lineNum += 1;
                }
            }
            String[] splitFile = new String[lineNum];
            //memasukan data kedalam array
            while (scannerGetDataOneMessage.hasNextLine()) {
                String lineGetData = scannerGetDataOneMessage.nextLine();
                Scanner lineScannerGetData = new Scanner(lineGetData);
                lineScannerGetData.useDelimiter(";");
                while (lineScannerGetData.hasNext()) {
                    String part = lineScannerGetData.next();
                    splitFile[i] = part;
                    i++;
                }
            }
            int num = 0;
            while (num < lineNum) {
                String[] splitRows = splitFile[num].split("@@@");

                String idPenerima = ""+aktifUser.user_id;
                if (splitRows[1].equals(idPenerima)){
                    listArrayChatOneMessage.add("<div class=\"outgoing_msg\">\n" + "                        <input type=\"hidden\" name=\"chat_id_one\" class=\"chat_id_one\" value="+chat_id+"><div class=\"sent_msg\">\n" + "                            <p>"+splitRows[2]+"</p>\n" + "                            <span class=\"time_date\"> "+splitRows[0]+"</span> </div>\n" + "                    </div>");
                }else{
                    listArrayChatOneMessage.add("<div class=\"incoming_msg\">\n" + "                        <input type=\"hidden\" name=\"chat_id_one\" class=\"chat_id_one\" value="+chat_id+"><div class=\"incoming_msg_img\"> <img src=\"/public/images/icon-chat-list.png\" alt=\"sunil\"> </div>\n" + "                        <div class=\"received_msg\">\n" + "                            <div class=\"received_withd_msg\">\n" + "                                <p>"+splitRows[2]+"</p>\n" + "                                <span class=\"time_date\"> "+splitRows[0]+"</span></div>\n" + "                        </div>\n" + "                    </div>");
                }
                num++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String listArrayChatOneMessageToString = String.join(" ", listArrayChatOneMessage);

        JsonObject jsonChat = new JsonObject();
        jsonChat.addProperty("dataChatOneMessage", listArrayChatOneMessageToString);
        renderJSON(jsonChat);
    }

    @AllowAccess({Acl.COM_BROADCAST_ADD})
    public static void createPengumuman(){
        List<UserRoleGrup> userRoleGrups = UserRoleGrup.findAllActive();
        List<Komoditas> komoditasList = Komoditas.findAllActive();
        List<Komoditas> komoditasListNasional = Komoditas.getKomoditasNasional();
        List<Komoditas> komoditasListLokal = Komoditas.getKomoditasLokal();
        List<Komoditas> komoditasListSektoral = Komoditas.getKomoditasSektoral();

        render("chat/ChatCtr/createPengumuman.html", userRoleGrups, komoditasList, komoditasListNasional, komoditasListLokal, komoditasListSektoral);
    }

    public static String searchUser(@As("keyword:")String keyword) throws NumberFormatException{
        List<User> userList = User.findByUserName(keyword);

        JsonArray jsonArray = new JsonArray();
        JsonObject result = new JsonObject();

        if(userList.isEmpty()){
            result.addProperty("message","User tidak ditemukan");
            result.addProperty("status",false);
        }else{
            for (User user : userList) {
                JsonObject obj = new JsonObject();
                obj.addProperty("id",user.id);
                obj.addProperty("user_name",user.user_name);
                obj.addProperty("nama_lengkap",user.nama_lengkap);
                obj.addProperty("pps_id",user.pps_id);

                jsonArray.add(obj);
            }

            result.addProperty("status",true);
            result.add("data",jsonArray);
        }

        return result.toString();
    }

    public static String searchPenyedia(@As("keyword:")String keyword) throws NumberFormatException{
        List<User> userList = User.findByUserNameJoinPenyedia(keyword);

        JsonArray jsonArray = new JsonArray();
        JsonObject result = new JsonObject();

        if(userList.isEmpty()){
            result.addProperty("message","User Penyedia tidak ditemukan");
            result.addProperty("status",false);
        }else{
            for (User user : userList) {
                JsonObject obj = new JsonObject();
                obj.addProperty("id",user.id);
                obj.addProperty("user_name",user.user_name);
                obj.addProperty("nama_lengkap",user.nama_lengkap);
                obj.addProperty("rkn_id",user.rkn_id);

                jsonArray.add(obj);
            }

            result.addProperty("status",true);
            result.add("data",jsonArray);
        }

        return result.toString();
    }

    public static String UserPenyedia() throws NumberFormatException{
        List<User> userList = User.getAllPenyedia();
        JsonArray jsonArray = new JsonArray();
        JsonObject result = new JsonObject();

        if(userList.isEmpty()){
            result.addProperty("message","User Penyedia tidak ditemukan");
            result.addProperty("status",false);
        }else{
            for (User user : userList) {
                JsonObject obj = new JsonObject();
                obj.addProperty("id",user.id);
                obj.addProperty("user_name",user.user_name);
                jsonArray.add(obj);
            }
            result.addProperty("status",true);
            result.add("data",jsonArray);
        }

        return result.toString();
    }

    public static void createSubmitPengumuman(String judul, String user_penerima, String isi_konten, Long user_ids[], Long user_role_grup_id, Long[] komoditas_ids){
        int filterId = 0;
        if (user_penerima.equals("user")){
            filterId = 1;

            Broadcast broadcast = new Broadcast();
            broadcast.judul = judul;
            broadcast.filter_id = filterId;
            broadcast.deskripsi = isi_konten;
            broadcast.user_role_grup_id = user_role_grup_id;
            broadcast.active = true;
            long broadcast_id = broadcast.save();

            if (!ArrayUtils.isEmpty(user_ids)){
                for (Long user_id : user_ids){
                    BroadcastDetail broadcastDetail = new BroadcastDetail();
                    broadcastDetail.user_id = user_id;
                    broadcastDetail.broadcast_id = broadcast_id;
                    broadcastDetail.active = true;
                    broadcastDetail.is_read = false;
                    broadcastDetail.save();
                }
            }

        }else if (user_penerima.equals("role")){
            filterId = 2;

            Broadcast broadcast = new Broadcast();
            broadcast.judul = judul;
            broadcast.filter_id = filterId;
            broadcast.deskripsi = isi_konten;
            broadcast.user_role_grup_id = user_role_grup_id;
            broadcast.active = true;
            broadcast.save();

        }else if (user_penerima.equals("penyedia_komoditas")) {
            filterId = 3;

            if (!ArrayUtils.isEmpty(komoditas_ids)){
                for (Long komoditas_id : komoditas_ids){
                    Broadcast broadcast = new Broadcast();
                    broadcast.judul = judul;
                    broadcast.filter_id = filterId;
                    broadcast.deskripsi = isi_konten;
                    broadcast.komoditas_id = komoditas_id;
                    broadcast.active = true;
                    broadcast.save();
                }
            }

        }
        flash.success(Messages.get("notif.success.submit"));
        createPengumuman();
    }

    public static void getDataBroadcast(Long broadcast_id){
        Broadcast broadcast = Broadcast.findById(broadcast_id);
        BroadcastDetail checkBroadcastDetail = BroadcastDetail.findByBroadcastIdAndUserId(broadcast_id, getAktifUser().user_id);

        if (checkBroadcastDetail != null){
            checkBroadcastDetail.active = true;
            checkBroadcastDetail.is_read = true;
            checkBroadcastDetail.save();
        }else {
            BroadcastDetail broadcastDetail = new BroadcastDetail();
            broadcastDetail.user_id = getAktifUser().user_id;
            broadcastDetail.broadcast_id = broadcast_id;
            broadcastDetail.active = true;
            broadcastDetail.is_read = true;
            broadcastDetail.save();
        }

        List<String> listArrayChatOneMessage = new ArrayList<String>(20);
                    listArrayChatOneMessage.add("<div class=\"incoming_msg\">\n" +
                            "    <input type=\"hidden\" name=\"chat_id_one\" class=\"chat_id_one\" value=\""+broadcast.id+"\">\n" +
                            "    <div class=\"incoming_msg_img\"> \n" +
                            "    \t<img src=\"/public/images/icon-chat-list.png\" alt=\"sunil\"> \n" +
                            "    </div>\n" +
                            "    <div class=\"received_msg\">\n" +
                            "        <div class=\"received_withd_msg\">\n" +
                            "            <p>\n" +
                            "\t\t\t\t<b>\n" +
                            "\t\t\t\t\t "+broadcast.judul+" \n" +
//                            "\t\t\t\t\t"+ namaPengirimPesan.nama_lengkap +"\n" +
                            "\t\t\t\t</b>\n" +
                            "\t\t\t\t<br>\n" +
                            "\t\t\t\t"+broadcast.deskripsi+" \n" +
                            "\t\t\t</p>\n" +
                            "            <span class=\"time_date\"> "+broadcast.created_date+"</span>\n" +
                            "        </div>\n" +
                            "    </div>\n" +
                            "</div>");
        String listArrayChatOneMessageToString = String.join(" ", listArrayChatOneMessage);
        JsonObject jsonChat = new JsonObject();
        jsonChat.addProperty("dataChatOneMessage", listArrayChatOneMessageToString);
        renderJSON(jsonChat);
    }

}
