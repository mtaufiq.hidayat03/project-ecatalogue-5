package models.katalog.komoditasmodel;

import models.BaseKatalogTable;
import models.chat.Broadcast;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;

@Table(name = "komoditas_template_kontrak")
public class KomoditasTemplateKontrak extends BaseKatalogTable {

	@Id
	public Long id;
	public long komoditas_id;
	public String jenis_dokumen;
	public String template;
	public boolean active;

	public static final String KONTRAK_KATALOG  = "KONTRAK KATALOG";
	public static final String SSUK  = "SYARAT SYARAT UMUM KONTRAK KATALOG";
	public static final String SSKK  = "SYARAT SYARAT KHUSUS KONTRAK KATALOG";

	public static List<KomoditasTemplateKontrak> findByKomoditas(long komoditas_id){
		return KomoditasTemplateKontrak.find("komoditas_id = ? and active = ?",komoditas_id, AKTIF).fetch();
	}

	public static KomoditasTemplateKontrak findByKomoditasAndJenisDokumen(long komoditas_id, String jenis_dokumen){
		return KomoditasTemplateKontrak.find("komoditas_id = ? and jenis_dokumen = ? and active = ?",komoditas_id,jenis_dokumen, AKTIF).first();
	}

	public static KomoditasTemplateKontrak getOneIdKomoditas(){
//		return KomoditasTemplateKontrak.find("komoditas_id = ? and jenis_dokumen = ? and active = ?", AKTIF).first();
		String query = "SELECT komoditas_id FROM `komoditas_template_kontrak` where active = 1 GROUP BY komoditas_id ORDER BY id asc limit 1";
		return Query.find(query, KomoditasTemplateKontrak.class).first();
	}
//	SELECT komoditas_id FROM `komoditas_template_kontrak` GROUP BY komoditas_id ORDER BY id asc limit 1;

}
