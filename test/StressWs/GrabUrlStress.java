package StressWs;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Scanner;

public class GrabUrlStress {


    public static void main(String[] args){
//        for(int i = 0; i <=100; i++ ) {
//            new ThreadJavaRoot().run();
//        }
        new StresTest2("Stress 1 ").run();
        new StresTest2("Stress 2").run();
        new StresTest2("Stress 3").run();

//
        try {
            Thread.sleep(1000000);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("<----------------thread For wait stacktrace ------------>");
    }


    public static void getWs(int id){
        String charset = java.nio.charset.StandardCharsets.UTF_8.name();
        String url = "http://10.1.30.151:2212/en/katalog/produk/alat-berat/"+id;
        //String url = "http://localhost:2209/en/katalog/produk/alat-berat/"+id;
        String query = "";
        try {
            URLConnection connection = new URL(url + "?" + query).openConnection();
            connection.setRequestProperty("Accept-Charset", charset);
            InputStream response = connection.getInputStream();
            try (Scanner scanner = new Scanner(response)) {
                String responseBody = scanner.useDelimiter("\\A").next();
                System.out.println(!responseBody.isEmpty()+" ->>>>>>>>>>>> response oke");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static void runWs(){
        String url = "http://localhost:2209/en/katalog/produk/alat-berat/41";
        String charset = "UTF-8";  // Or in Java 7 and later, use the constant: java.nio.charset.StandardCharsets.UTF_8.name()
        String param1 = "value1";
        String param2 = "value2";

        String query = "";
//        try {
//            query = String.format("param1=%s&param2=%s",
//                    URLEncoder.encode(param1, charset),
//                    URLEncoder.encode(param2, charset));
//
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
        URLConnection connection = null;
        try {
            connection = new URL(url).openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        connection.setDoOutput(true); // Triggers POST.
        connection.setRequestProperty("Accept-Charset", charset);
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);

        try (OutputStream output = connection.getOutputStream()) {
            output.write(query.getBytes(charset));
            InputStream response = connection.getInputStream();
            System.out.println(response);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
