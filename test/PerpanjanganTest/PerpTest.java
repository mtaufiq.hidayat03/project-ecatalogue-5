package PerpanjanganTest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import controllers.katalog.PermohonanPembaruanCtr;
import models.common.DokumenInfo;
import models.katalog.Produk;
import models.permohonan.PembaruanStaging;
import models.permohonan.ProdukHargaStagging;
import models.permohonan.ProdukStagging;
import models.prakatalog.Penawaran;
import models.prakatalog.PerpanjanganStaging;
import models.prakatalog.Usulan;
import models.prakatalog.UsulanKualifikasi;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.parser.Parser;
import org.junit.Test;
import play.Logger;
import play.test.UnitTest;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class PerpTest extends UnitTest {

    ////@Test
    public void runTest(){
        Gson gson = new Gson();
        SipleClassTest object = new SipleClassTest();
        object.nama_produk = "test produk apa saja";
        Class c = object.getClass();
        String cn = c.toString();//ke db
        String datajs = gson.toJson(object);
        System.out.println(cn);
        Object target = new Object();
        Class c2 = null;
        try {
            c2 = Class.forName(new SipleClassTest().getClass().getName());
             target = gson.fromJson(datajs,c2);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if(target.getClass() == c2){//SipleClassTest
            System.out.println("TAGET MATCH");
            SipleClassTest a=  (SipleClassTest)target;
            System.out.println(a.nama_produk);
        }else {
            System.out.println("TARGET NOT MATCH");
        }
    }

    ////@Test
    public void nextTest(){
        SipleClassTest a = new SipleClassTest();
        a.nama_produk = "sebuah nama";
        PerpanjanganStaging perp = new PerpanjanganStaging();
        perp.class_name = a.getClass().getName();
        perp.object_json = new Gson().toJson(a);

        Object real = perp.getRealObjec();
        if(null != real){
            System.out.println("OKE");
            SipleClassTest target = (SipleClassTest)real;
            System.out.println(target.nama_produk);
        }
    }

    //@Test
    public void testExistingData(){
//        PerpanjanganStaging ps = PerpanjanganStaging.findById(1);
//        Object obj =(Usulan) ps.getRealObjec();

//        PerpanjanganStaging staging = PerpanjanganStaging.find("class_name=? and perpanjangan_id=?",Usulan.class.getName(),2).first();
//        Usulan usul = (Usulan) staging.getRealObjec();
//        System.out.println(usul.nama_usulan);
        List<UsulanKualifikasi> kualifikasiUsulanList = new ArrayList<>();
        List<DokumenInfo> dokumenPenawaranList = new ArrayList<>();
        Penawaran pen = Penawaran.findById(1033);
        kualifikasiUsulanList = UsulanKualifikasi.find("usulan_id = ?",pen.usulan_id).fetch();
        dokumenPenawaranList = DokumenInfo.listDokumenPenawaran(pen.id);

        PerpanjanganStaging pens = new PerpanjanganStaging();
        pens.perpanjangan_id = new Long(2);
        pens.class_name = UsulanKualifikasi.class.getName();
        pens.isList = true;
        pens.object_json = new Gson().toJson(kualifikasiUsulanList);

        Type listType = new TypeToken<ArrayList<UsulanKualifikasi>>(){}.getType();

        List<UsulanKualifikasi> data = (List<UsulanKualifikasi>)(Object) pens.getListData(listType);



    }

    ////@Test
    public void getStaging(){
        PerpanjanganStaging obj = PerpanjanganStaging.find("class_name=? and isList=1 and perpanjangan_id=?", new UsulanKualifikasi().getClass().getName(), new Long(1)).first();
    }

    //@Test
    public void testConvertJson(){
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX")
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        Produk produk = Produk.findById(new Long(540194));
        String string = gson.toJson(produk);
        Logger.info(string);
        Produk balik = gson.fromJson(string, Produk.class);
        Logger.debug(balik.id+" oke");
    }

    @Test
    public void testUpdate(){
        PembaruanStaging pmb = PembaruanStaging.findById(886l);
        ProdukStagging psg = pmb.getIfProdukStagging();
        ProdukHargaStagging phsg = psg.getIfProdukHargaStagging();
        phsg.harga_tanggal = new Date();
        psg.object_json = psg.createGson().toJson(phsg);
        pmb.object_json = new Gson().toJson(psg);
        int res = pmb.save();
        Logger.info("res:"+res);
        PembaruanStaging pmbs = PembaruanStaging.findById(886l);
        Logger.info("info:"+pmbs.getIfProdukStagging().getIfProdukHargaStagging().harga_tanggal);

    }
}
