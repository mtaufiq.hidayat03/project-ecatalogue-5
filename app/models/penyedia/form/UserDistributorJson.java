package models.penyedia.form;

public class UserDistributorJson {
//    SELECT p.id as penyedia_id, p.nama_penyedia nama, u.user_name as username, p.email as email," +
//            " u.user_role_grup_id as role_id, ug.nama_grup as role_name, u.status_penyedia_distributor, u.id as user_id, u.rkn_id
    public Long penyedia_id;
    //public String nama_penyedia;
    public String nama;
    public String username;
    public String email;
    public Integer role_id;
    public String role_name;
    public String status_penyedia_distributor;
    public Long user_id;
    public Long rkn_id;

    public String rkn_nama;
    public String rkn_website;
    public String rkn_alamat;
    public String rkn_telepon;
    public String rkn_fax;
    public String rkn_npwp;
    public String rkn_email;
    public String rkn_mobile_phone;
    public String rkn_pkp;
    public String rkn_namauser;
    public String rkn_kodepos;
    public String passw;

}
