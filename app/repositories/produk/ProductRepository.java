package repositories.produk;

import models.product.Produk;
import models.product.search.ProductSearch;
import models.product.search.ProductSearchResult;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.util.TextUtils;
import play.db.jdbc.Query;
import utils.LogUtil;

public class ProductRepository {

    public static final String TAG = "ProductRepository";

    public static ProductSearchResult searchProduct(ProductSearch search) {
        ProductSearchResult model = new ProductSearchResult(search);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT DISTINCT p.id, " +
                "p.produk_gambar_file_sub_location, " +
                "p.produk_gambar_file_name, " +
                "(case when (concat(p.produk_gambar_file_sub_location,p.produk_gambar_file_name) is null) then concat(pg.file_sub_location,pg.file_name) else concat(p.produk_gambar_file_sub_location,p.produk_gambar_file_name) end) as file_url, " +
                "m.nama_manufaktur, " +
                "pen.nama_penyedia, " +
                "p.no_produk_penyedia, " +
                "p.minta_disetujui, " +
                "p.apakah_dapat_dibeli, " +
                "p.apakah_ditayangkan, " +
                "p.apakah_dapat_diedit, " +
                "p.status, " +
                "p.komoditas_id, " +
                "k.nama_komoditas, " +
                "p.no_produk, " +
                "p.setuju_tolak, " +
                "p.jumlah_stok, " +
                "p.jumlah_stok_inden, " +
                "pk.nama_kategori, " +
                "p.nama_produk, " +
                "p.is_umkm, " +
                "pnw.status as penawaran_status, " +
                "k.kelas_harga, "+
                "( CASE WHEN (p.active = 1 AND p.apakah_ditayangkan = 1 AND k.active = 1 AND p.setuju_tolak = 'setuju' " +
                "AND (( p.berlaku_sampai >= CURDATE( ) AND pnyk.tgl_masa_berlaku_mulai <= curdate( ) AND pnyk.tgl_masa_berlaku_selesai >= curdate( ) AND pnyk.active = 1 AND pnyk.apakah_berlaku = 1) " +
                " OR " +
                " (p.berlaku_sampai IS NULL AND pnyk.tgl_masa_berlaku_mulai <= curdate( ) AND pnyk.tgl_masa_berlaku_selesai >= curdate( ) AND pnyk.active = 1 AND pnyk.apakah_berlaku = 1) ) " +
                "AND ( k.apakah_stok_unlimited = 1 OR ( k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0 ) )) THEN 'Tayang' ELSE 'Tidak Tayang' END ) as sudah_tayang ")
            .append("FROM produk p ")
            .append("LEFT JOIN penyedia pen on p.penyedia_id = pen.id ")
            .append("LEFT JOIN komoditas k on k.id = p.komoditas_id ")
            .append("LEFT JOIN penawaran pnw on p.penawaran_id = pnw.id  ")
            .append("LEFT JOIN manufaktur m on m.id = p.manufaktur_id ")
            .append("LEFT JOIN produk_kategori pk on pk.id = p.produk_kategori_id ")
            .append("LEFT JOIN produk_gambar pg on pg.produk_id = p.id and pg.active=1 and pg.produk_id > 0 ")
            .append("LEFT JOIN penyedia_kontrak pnyk on pnyk.id = pnw.kontrak_id ");

        sb.append(" WHERE p.active = 1 ");
        if (!TextUtils.isEmpty(search.keyword)) {
            sb.append(" AND (p.nama_produk LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%'");
            sb.append(" OR p.no_produk LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%'");
            sb.append(" OR m.nama_manufaktur LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%')");
        }

        if (search.komoditas > 0) {
            sb.append(" AND p.komoditas_id = ").append(search.komoditas);
        }

        if(!TextUtils.isEmpty(search.jenis_produk)){
            sb.append(" AND p.jenis_produk = '").append(search.jenis_produk).append("'");
        }

        if(!TextUtils.isEmpty(search.penyedia)){
            sb.append(" AND p.penyedia_id = ").append(search.penyedia);
        } else {
            sb.append(" AND p.status NOT IN ('draft')");
        }

        if(!TextUtils.isEmpty(search.status)){
            if(search.status.equalsIgnoreCase("setuju") || search.status.equalsIgnoreCase("tolak")){
                sb.append(" AND p.setuju_tolak = '").append(search.status).append("'");
            }else if(search.status.equalsIgnoreCase("0") || search.status.equalsIgnoreCase("1")){
                sb.append(" AND p.minta_disetujui = ").append(search.status);
                sb.append(" AND (p.setuju_tolak is null OR p.setuju_tolak = '')");
            }
        }

        if(!TextUtils.isEmpty(search.apakah_dibeli)){
            sb.append(" AND p.apakah_dapat_dibeli = ").append(search.apakah_dibeli);
        }

        if(!TextUtils.isEmpty(search.apakah_ditayangkan)){
            sb.append(" AND p.apakah_ditayangkan = ").append(search.apakah_ditayangkan);
        }
        //search where id produk in
        String idsIn = StringUtils.join(search.listProdukIdsIn, ',');
        if(!idsIn.isEmpty()){
            sb.append(" AND p.id in ( ").append(idsIn).append(") ");
        }
        //search where id produk not in
        String idsNotIn = StringUtils.join(search.listProdukIdsNotIn, ',');
        if(!idsNotIn.isEmpty()){
            sb.append(" AND p.id NOT IN ( ").append(idsNotIn).append(") ");
        }
        sb.append(" GROUP BY p.id ORDER BY p.id DESC ");
        Long total = ProductRepository.generateTotal(sb.toString());
        model.setTotal(total);
        LogUtil.debug(TAG, sb.toString());
        LogUtil.debug(TAG, "total items: " + model.getTotal());
        if (!model.isEmpty()) {
            sb.append(" LIMIT ").append(search.per_page).append(" OFFSET ").append(model.getFrom());
            model.items = Query.find(sb.toString(), Produk.class).fetch();
            LogUtil.debug(TAG, model.items);
        }
        return model;
    }

    private static Long generateTotal(String query) {
        Long total = Query.find("SELECT COUNT(id) FROM (" + query + ") p", Long.class).first();
        return total;
    }

    public static ProductSearchResult searchProductTayang(ProductSearch search) {
        ProductSearchResult model = new ProductSearchResult(search);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT\n" +
                "\tp.id AS id,\n" +
                "\tp.nama_produk AS nama_produk,\n" +
                "\tp.no_produk AS no_produk,\n" +
                "\t( CASE WHEN p.harga_utama IS NULL THEN 0 ELSE p.harga_utama END ) AS harga_utama_produk,\n" +
                "\t(\tCASE WHEN p.margin_harga > 0 THEN ROUND( ( p.harga_utama + ( p.harga_utama * p.margin_harga / 100 ) ), 2 ) \n" +
                "\t\tWHEN p.harga_utama IS NULL THEN 0 ELSE p.harga_utama \n" +
                "\t\tEND \n" +
                "\t) AS harga_pemerintah_produk,\n" +
                "\tp.active AS produk_aktif,\n" +
                "\tp.apakah_ditayangkan AS produk_apakah_ditayangkan,\n" +
                "\tp.setuju_tolak AS produk_setuju_tolak,\n" +
                "\tCURDATE( ) AS tanggal_sekarang,\n" +
                "\tp.berlaku_sampai AS produk_tanggal_berlaku,\n" +
                "\t(CASE WHEN p.berlaku_sampai >= CURDATE( ) OR p.berlaku_sampai IS NULL THEN 1 ELSE 0 END) AS produk_tanggal_berlaku_status,\n"+
                "\tp.jumlah_stok AS produk_jumlah_stok,\n" +
                "\tk.nama_komoditas AS komoditas_nama,\n" +
                "\tk.active AS komoditas_aktif,\n" +
                "\tk.apakah_stok_unlimited AS komoditas_apakah_stok_unlimited,\n" +
                "\tp.penawaran_id as id_penawaran,\n" +
                "\tpk.id AS id_kontrak,\n" +
                "\t(CASE WHEN pk.tgl_masa_berlaku_mulai <= curdate( ) THEN 1 ELSE 0 END) AS kontrak_tgl_masa_berlaku_mulai_status,\n" +
                "\t(CASE WHEN pk.tgl_masa_berlaku_selesai >= curdate( ) THEN 1 ELSE 0 END) AS kontrak_tgl_masa_berlaku_selesai_status,\n"+
                "\tpk.tgl_masa_berlaku_mulai AS kontrak_tgl_masa_berlaku_mulai,\n" +
                "\tpk.tgl_masa_berlaku_selesai AS kontrak_tgl_masa_berlaku_selesai,\n" +
                "\tpk.active AS kontrak_aktif,\n" +
                "\t( CASE WHEN (p.active = 1 \n" +
                "\tAND p.apakah_ditayangkan = 1 \n" +
                "\tAND k.active = 1 \n" +
                "\tAND p.setuju_tolak = 'setuju' \n" +
                "\tAND (\n" +
                "\t\t\t( p.berlaku_sampai >= CURDATE( ) \n" +
                "\t\t\t\tAND pk.tgl_masa_berlaku_mulai <= curdate( ) \n" +
                "\t\t\t\tAND pk.tgl_masa_berlaku_selesai >= curdate( ) \n" +
                "\t\t\t\tAND pk.active = 1 AND pk.apakah_berlaku = 1 \n" +
                "\t\t\t) \n" +
                "\t\t\tOR \n" +
                "\t\t\t(\n" +
                "\t\t\t\tp.berlaku_sampai IS NULL \n" +
                "\t\t\t\tAND pk.tgl_masa_berlaku_mulai <= curdate( ) \n" +
                "\t\t\t\tAND pk.tgl_masa_berlaku_selesai >= curdate( ) \n" +
                "\t\t\t\tAND pk.active = 1 AND pk.apakah_berlaku = 1 \n" +
                "\t\t\t) \n" +
                "\t\t) \n" +
                "\tAND ( k.apakah_stok_unlimited = 1 OR ( k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0 ) )) THEN 'Tayang' ELSE 'Tidak Tayang' END ) as sudah_tayang\n" +
                "FROM\n" +
                "\tproduk p\n" +
                "\tLEFT JOIN produk_kategori cat ON cat.id = p.produk_kategori_id\n" +
                "\tLEFT JOIN penawaran pw ON pw.id = p.penawaran_id\n" +
                "\tLEFT JOIN penyedia_kontrak pk ON pk.id = pw.kontrak_id\n" +
                "\tLEFT JOIN penyedia pn ON pn.id = pk.penyedia_id\n" +
                "\tLEFT JOIN komoditas k ON p.komoditas_id = k.id\n" +
                "\tLEFT JOIN manufaktur m ON m.id = p.manufaktur_id ");

        sb.append(" WHERE p.active = 1 ");
        if (!TextUtils.isEmpty(search.keyword)) {
            sb.append(" AND (p.nama_produk LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%'");
            sb.append(" OR p.no_produk LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%'");
            sb.append(" OR m.nama_manufaktur LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%')");
        }

        if (search.komoditas > 0) {
            sb.append(" AND p.komoditas_id = ").append(search.komoditas);
        }

        if(!TextUtils.isEmpty(search.jenis_produk)){
            sb.append(" AND p.jenis_produk = '").append(search.jenis_produk).append("'");
        }

        if(!TextUtils.isEmpty(search.penyedia)){
            sb.append(" AND p.penyedia_id = ").append(search.penyedia);
        }

        if(!TextUtils.isEmpty(search.status)){
            if(search.status.equalsIgnoreCase("setuju") || search.status.equalsIgnoreCase("tolak")){
                sb.append(" AND p.setuju_tolak = '").append(search.status).append("'");
            }else if(search.status.equalsIgnoreCase("0") || search.status.equalsIgnoreCase("1")){
                sb.append(" AND p.minta_disetujui = ").append(search.status);
                sb.append(" AND (p.setuju_tolak is null OR p.setuju_tolak = '')");
            }
        }

        if(!TextUtils.isEmpty(search.apakah_dibeli)){
            sb.append(" AND p.apakah_dapat_dibeli = ").append(search.apakah_dibeli);
        }

        if(!TextUtils.isEmpty(search.apakah_ditayangkan)){
            sb.append(" AND p.apakah_ditayangkan = ").append(search.apakah_ditayangkan);
        }

        sb.append(" GROUP BY p.id ORDER BY p.id DESC ");
        Long total = ProductRepository.generateTotal(sb.toString());
        model.setTotal(total);

        if (!model.isEmpty()) {
            sb.append(" LIMIT ").append(model.getMax()).append(" OFFSET ").append(model.getFrom());
            model.items = Query.find(sb.toString(), Produk.class).fetch();
        }
        return model;
    }

}
