package ext.contracts;

import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import play.mvc.Scope;

import java.util.List;

/**
 * @author HanusaCloud on 12/18/2017
 */
public interface ParamAble {

    List<BasicNameValuePair> getParams();

    int getPage();

    default String getQueryString() {
        if (getParams() != null) {
            return "?" + URLEncodedUtils.format(getParams(), "UTF-8");
        }
        return "";
    }

    default Integer getCurrentPage(Scope.Params params) {
       return !isNull(params, "offset") ? params.get("offset", Integer.class) : 1;
    }

    default boolean isNull(Scope.Params params, String key) {
        return params.get(key) == null;
    }

}
