package jobs.JobTrail;

import jobs.ExtProduct.ExtProduct;
import jobs.ExtProduct.PenyediaKomoditasApi;
import models.api.produk.HistoryDatafeed;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@NoTransaction
public class ExtProdukJob9 extends Job {
    UUID id = UUID.randomUUID();
    PenyediaKomoditasApi pk = new PenyediaKomoditasApi();
    int defaultWaitime = 0;
    int nextWaitime = 0;
    public ExtProdukJob9(PenyediaKomoditasApi _pk, int _defaultWaitime, int _nextWaitime){
        pk =_pk;
        defaultWaitime = _defaultWaitime;
        defaultWaitime = _nextWaitime;
    }
    public void doJob() {
        try {
            URL urls = new URL(pk.produk_api_url);
            Logger.info("get from url: "+pk.produk_api_url+" "+pk.start_date);
            //create new object for independence object
            ExtProduct exts = new ExtProduct();
            exts.penyediaKomoditasApi = pk;
            exts.defaultWaitime = defaultWaitime;
            exts.nextWaitime = nextWaitime;
            exts.urlForExecutejob = pk.produk_api_url;

            HistoryDatafeed hdf = new HistoryDatafeed();
            hdf.id_job = id.toString();
            hdf.penyedia_id = pk.penyediaid;
            hdf.penyedia_name = pk.nama_penyedia;
            hdf.start_time = new Timestamp(new Date().getTime()).toString();

            exts.getData(exts.defaultWaitime,exts.urlForExecutejob, hdf);

        }catch (MalformedURLException e) {
            Logger.error("not valid url penyedia:"+ pk.produk_api_url+" "+pk.start_date);
        }
    }
}
