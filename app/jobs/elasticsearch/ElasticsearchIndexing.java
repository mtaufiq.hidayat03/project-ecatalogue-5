package jobs.elasticsearch;

import play.Play;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * @author HanusaCloud on 11/28/2017
 */
public class ElasticsearchIndexing {

    public static void main(String[] args) throws Exception {
        Play.configuration = new Properties();
        Play.applicationPath = new File(System.getProperty("application.path", "."));
        Play.configuration.load(new FileInputStream(Play.applicationPath+"/conf/application.conf"));
        final String mode = Play.configuration.getProperty("application.mode");
        final boolean isLogged = mode.equalsIgnoreCase("dev") || mode.equalsIgnoreCase("debug");
        final String indexFilePath = Play.applicationPath + "/logstash/index.json";
        final String indexAuditFilePath = Play.applicationPath + "/logstash/index-audit.json";
        ElasticConnection elasticConnection = new ElasticConnection(isLogged);
        elasticConnection.delete();
        elasticConnection.deleteAuditTrail();
        elasticConnection.indexing(new File(indexFilePath));
        elasticConnection.indexingAuditTrail(new File(indexAuditFilePath));
    }

}
