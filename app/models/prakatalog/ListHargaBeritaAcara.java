package models.prakatalog;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import models.jcommon.util.CommonUtil;
import models.katalog.Produk;
import models.masterdata.Kabupaten;
import models.masterdata.Provinsi;
import models.util.produk.ProdukHargaJson;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import play.Logger;

import java.util.List;

public class ListHargaBeritaAcara {

    public Long provId;
    public Long kabId;
    public String namaProvinsi;
    public String namaKabupaten;
    public double harga;
    public String namaProduk;
    public String namaManufaktur;
    public String namaUnitPengukuran;

    public ListHargaBeritaAcara(){

    }

    public ListHargaBeritaAcara(Long _produkId, Long _provId, Long _kabId, Double _harga,
                                List<Provinsi> provinsiList,
                                List<Kabupaten> kabupatenList,
                                List<Produk> listProdukPenawaran){
        this.kabId = _kabId;
        this.provId = _provId;
        this.harga = _harga;

        this.namaKabupaten = kabupatenList.stream()
                .filter(f->f.id.equals(_kabId)).findAny().orElse(new Kabupaten()).nama_kabupaten;

        this.namaProvinsi = provinsiList.stream()
                .filter(f->f.id.equals(_provId)).findAny().orElse(new Provinsi() ).nama_provinsi;

        this.namaProduk = listProdukPenawaran.stream()
                .filter(f->f.id.equals(_produkId)).findAny().orElse(new Produk() ).nama_produk;

        this.namaManufaktur = listProdukPenawaran.stream()
                .filter(f->f.id.equals(_produkId)).findAny().orElse(new Produk() ).nama_manufaktur;

        this.namaUnitPengukuran = listProdukPenawaran.stream()
                .filter(f->f.id.equals(_produkId)).findAny().orElse(new Produk() ).nama_unit_pengukuran;

    }


}
