package controllers.cms;

import controllers.BaseController;
import controllers.security.AllowAccess;
import ext.DatetimeBinder;
import models.cms.Banner;
import models.common.AktifUser;
import models.secman.Acl;
import play.Play;
import play.data.binding.As;
import play.i18n.Messages;
import play.libs.Files;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Random;

public class BannerCtr extends BaseController {

    @AllowAccess({Acl.COM_BANNER})
    public static void index(){
        render();
    }

    @AllowAccess({Acl.COM_BANNER_ADD})
    public static void create(){
        render("cms/BannerCtr/create.html");
    }

    public static String getRandom(){
        String SALTCHARS = "abcdefghijklmnopqrstuvwxyz1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    @AllowAccess({Acl.COM_BANNER_ADD})
    public static void createSubmit(String subjek, File gambar){

        Banner banner = new Banner();

        if (gambar != null) {
            banner.subjek = subjek;
            banner.active = true;
            banner.created_by = AktifUser.getActiveUserId();

            String nama_file = getRandom() + "-" + gambar.getName().replace(" ", "-");
            File dokumenroot = Play.applicationPath;
            File destination = new File(dokumenroot + "/public/files/image/banner/" + nama_file);
            Files.copy(gambar, destination);
            banner.file_name = nama_file;
            banner.original_file_name = gambar.getName();
            banner.file_size = destination.length();
            banner.save();
        }

//        banner.save();
        index();
    }

    @AllowAccess({Acl.COM_BANNER_ADD, Acl.COM_BANNER_EDIT, Acl.COM_BANNER_DELETE})
    public static void setAktif(long id) {
        Banner banner = Banner.findById(id);
        notFoundIfNull(banner);
        banner.setAktif();
        flash.success(
                Messages.get("record_update_aktifnonaktif_relation_success"));
        index();
    }

    @AllowAccess({Acl.COM_BANNER_ADD, Acl.COM_BANNER_EDIT, Acl.COM_BANNER_DELETE})
    public static void setNonAktif(long id) {
        Banner banner = Banner.findById(id);
        notFoundIfNull(banner);
        banner.setNonAktif();
        flash.success(
                Messages.get("record_update_aktifnonaktif_relation_success"));
        index();
    }

    @AllowAccess({Acl.COM_BANNER_EDIT})
    public static void edit(long id) {
        Banner banner = Banner.findById(id);

        render("cms/BannerCtr/edit.html",banner);
    }

    @AllowAccess({Acl.COM_BANNER_EDIT})
    public static void editSubmit(long bnr_id, String subjek, File gambar){
        Banner banner = Banner.findById(bnr_id);

        banner.subjek = subjek;
        banner.modified_by = AktifUser.getActiveUserId();
        banner.modified_date =new Timestamp(new java.util.Date().getTime());

        if(!gambar.getName().equalsIgnoreCase(banner.original_file_name)){
            String nama_file = getRandom() + "-" + gambar.getName().replace(" ", "-");
            File imgRoot = Play.applicationPath;
            File destination = new File(imgRoot + "/public/files/image/banner/" + nama_file);
            Files.copy(gambar, destination);
            banner.file_name = nama_file;
            banner.original_file_name = gambar.getName();
            banner.file_size = destination.length();
        }
        banner.save();
        flash.success(Messages.get("notif.success.update"));

        index();
    }

    @AllowAccess({Acl.COM_BANNER_DELETE})
    public static void delete(long id){
        Banner banner = Banner.findById(id);
        if(banner != null){
            File imgRoot = Play.applicationPath;
            File target = new File(imgRoot + "/public/files/image/banner/" + banner.file_name);
            target.delete();
            banner.delete();
            flash.success(Messages.get("notif.success.delete"));
        }

        index();
    }
}
