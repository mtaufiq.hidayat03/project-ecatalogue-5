package jobs;

import java.util.Date;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.apache.commons.lang.time.StopWatch;

import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;

/**Extend dari Job ini supaya bisa dilakukan monitoring!
 * @author Andik Yulianto<andikyulianto@yahoo.com>
 * @param <V>
 */
@NoTransaction
public class TrackableJob<V> extends Job<V> {

	
	//simpan 100 job terakhir
	public static Queue<TrackableJob> lastJobs=new CircularFifoQueue<>(100);
	
	public String status;//digunakan untuk informasi status job dalam rangka monitoring
	public String className;
	public Date startTime;
	public String duration="Running...";
	
	private StopWatch sw=new StopWatch();
	
	public void before()
	{
		if(!lastJobs.contains(this))
			lastJobs.add(this);
		startTime=new Date();
		sw.start();
		className=getClass().getName();
	}
	
	public void after()
	{
		sw.stop();
		duration=sw.toString();
		Logger.debug("[%s] DONE, duration: %s", className, sw);
		sw.reset();
	}
	
}
