package controllers.admin.komoditas;

/**
 * @author HanusaCloud on 11/30/2017
 */
public class CommoditySearch {

    public String keyword = "";
    public int category = 0;
    public int negotiation = -1;
    public int adds = -1;
    public int deliveryCost = -1;
    public int approval = -1;
    public int isDuplicate = -1;

}
