package models.cms;

import ext.DatetimeBinder;
import models.BaseKatalogTable;
import models.common.AktifUser;
import play.Logger;
import play.data.binding.As;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by user on 10/12/2017.
 */
@Table(name = "polling")
public class Polling extends BaseKatalogTable {
    @Id
    public Long id;
    public String subjek;
    public String slug;
    public String deskripsi_singkat;
    public String status;
    public Boolean terkunci;
    public Boolean apakah_utama;
    public String param;
    public Boolean active;

    @As(binder = DatetimeBinder.class)
    public Date publish_date_from;
    @As(binder = DatetimeBinder.class)
    public Date publish_date_to;

    public static List<Polling> findAllActive(){
        return Polling.find("active = 1").fetch();
    }

    public static List<Polling> findAllPublished(){
        List<Polling> result = new ArrayList<Polling>();
        try{
            String query = "SELECT * FROM polling WHERE active = 1 AND apakah_utama = 1 AND status = 'published' " +
                    "AND publish_date_from <= NOW() AND (publish_date_to >= NOW() OR publish_date_to is null)";
            result = Query.find(query,Polling.class).fetch();
        }catch (Exception e){
            Logger.info(e.getMessage());
        }
        return result;
    }

    private static String multipleReturns(){
        String separator = "!@#";

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String dateNow = dateFormat.format(date);

        String result = AktifUser.getActiveUserId() + separator + dateNow + separator;
        return (result);
    }

    public static void updateByParam(Long id, String param, Long val){
        String res = multipleReturns();
        int userId = Integer.parseInt(res.split("!@#")[0]);
        String dateNow = res.split("!@#")[1];

        String query = "update polling set " + param + " = " + val + ", modified_date = '" + dateNow + "', modified_by = " + userId + " where id = ?";
        Query.update(query,id);
    }

    public static void deleteById(Long id){
        String res = multipleReturns();
        int userId = Integer.parseInt(res.split("!@#")[0]);
        String dateNow = res.split("!@#")[1];

        String query = "update polling set active = 0, deleted_by = " + userId + ", deleted_date = '" + dateNow + "' where id = ?";
        Query.update(query,id);
    }
}
