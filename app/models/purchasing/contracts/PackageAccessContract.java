package models.purchasing.contracts;

import models.Feedback;
import models.common.AktifUser;
import models.jcommon.util.CommonUtil;
import models.purchasing.PaketRating;
import models.purchasing.status.ProgressStatus;
import models.purchasing.status.StatusMessage;
import play.i18n.Messages;
import repositories.paket.PaymentRepository;
import utils.LogUtil;

import java.util.Collections;
import java.util.List;

import static constants.paket.PaketStatusConstant.*;
import static controllers.BaseController.getAktifUser;

/**
 * @author HanusaCloud on 11/14/2017
 */
public interface PackageAccessContract extends BaseContract {

    String TAG = "PackageAccessContract";

    void setStatusMessage(StatusMessage statusMessage);

    StatusMessage getAccessMessage();

    default ProgressStatus[] getProgress() {
        LogUtil.d(TAG," generate progress");
        ProgressStatus[] progressStatuses = new ProgressStatus[4];
        progressStatuses[0] = new ProgressStatus(Messages.get("draft_paket.label"), "1", true);
        progressStatuses[1] = new ProgressStatus(Messages.get("proses.label"), "2", false);
        progressStatuses[2] = new ProgressStatus(Messages.get("pengiriman.label"), "3", false);
        progressStatuses[3] = new ProgressStatus(Messages.get("selesai.label"), "4", false);
        if (getStatus() != null) {
            if (getStatus().isDelivery()) {
                progressStatuses[1].isActive = true;
                progressStatuses[2].isActive = true;
            } else if (getStatus().isFinish()) {
                progressStatuses[1].isActive = true;
                progressStatuses[2].isActive = true;
                progressStatuses[3].isActive = true;
            } else if (getStatus().isNegotiation()
                    || !getCommodity().isNegotiationRequired()
                    || getStatus().isPpkApproved()
                    || getStatus().isNegotiationDone()) {
                progressStatuses[1].isActive = true;
            }
        }
        return progressStatuses;
    }

    default StatusMessage generateNonNegotiation(AktifUser aktifUser) {
        StatusMessage result = new StatusMessage();

        // modified by sudi untuk handling data existing kat 4 untuk ppk yang membuat paket negosiasi agar form persutujuan paket tampil
        if (null != getStatus().status_paket
                && null != getStatus().status_negosiasi
                && getStatus().status_negosiasi.equalsIgnoreCase("sepakat")
                && getStatus().status_paket.equalsIgnoreCase("ppk_setuju_baru")){
            getStatus().status_paket = "persiapan";
        }
        // end modified

        if (getStatus().isPreparation()) {
            LogUtil.d(TAG, "on preparation state waiting committee");
            result.setPreparationNonNegotiation();
            result.setApproval(allowPp(aktifUser));
        } else if (!allowPpkAsCommittee() && getStatus().isToPp() && getDistributorId() != null) {
            // send to pp again
            LogUtil.d(TAG,"send to pp again");
            result.setActionToPpkNonNegotiation();
            result.setSendToPpk(allowPp(aktifUser));
        } else if (!allowPpkAsCommittee() && getStatus().isToPpk() && getDistributorId() != null) {
            // set send ppk for approval
            LogUtil.d(TAG,"send to ppk for approval");
            result.setPpkActionApproval();
            result.setApproval(allowPpk(aktifUser));
        } else if (getStatus().isBuyerApproved()) {
            // send to provider for approval
            LogUtil.d(TAG,"send to provider for approval");
            result.setToProviderApproval();
            result.setApproval(allowProvider(aktifUser));
        } else if (getStatus().isProviderApproved()) {
            // send to committee for approval
            if (!allowPpkAsCommittee()) {
                LogUtil.d(TAG, "send to committee for approval");
                result.setBuyerActionNonNegotiation();
            }
            if (allowPpkAsCommittee()) {
                LogUtil.d(TAG, "ppk as committee no need for approval");
                result.position = getStatus().to_status;
            }
            if (getStatus().isToPp() && getDistributorId() == null) {
                // send to provider to set distributor
                LogUtil.d(TAG,"send to provider to set distributor");
                result.setDistributorUrl(allowProvider(aktifUser));
                result.setDistributorAction();
            }
            if (getStatus().status_paket.equalsIgnoreCase(STATUS_PROVIDER_APPROVED) && getStatus().fromProvider() && getStatus().isToProvider() && getDistributorId() == null){
                // send to provider to set distributor ketika ppk yang buat paket
                LogUtil.d(TAG,"send to provider to set distributor");
                result.setDistributorUrl(allowProvider(aktifUser));
                result.setDistributorAction();
            }
        }
        return result;
    }

    default boolean allowProvider(AktifUser aktifUser) {
        if (getProvider() == null) {
            withProvider(false);
        }
        return (aktifUser.isProvider() && getProvider().isUserIdEqual(aktifUser.user_id));
    }

    default boolean allowPp(AktifUser aktifUser) {
        return (aktifUser.isPp() || aktifUser.isUlp() || aktifUser.isPpk()) && getPpId().equals(aktifUser.user_id);
    }

    default boolean allowPpAsPpk(AktifUser aktifUser) {
        return aktifUser.isPpk() && getPpId().equals(aktifUser.user_id);
    }

    default boolean allowPpkAsCommittee() {
        return getUserPp().isPpk();
    }

    default boolean allowPpk(AktifUser aktifUser) {
        final boolean isAllowPpk = aktifUser.isPpk() && getPpkId().equals(aktifUser.user_id);
        LogUtil.d(TAG," is allow PPK: " + isAllowPpk);
        return isAllowPpk;
    }

    default boolean allowDistributor(AktifUser aktifUser) {
        if (getDistributor() == null) {
            withDistributor();
        }
        return aktifUser.isDistributor()
                && getDistributor() != null
                && getDistributor().isUserIdEqual(aktifUser.user_id);
    }

    default boolean allowProviderOrDistributor(AktifUser aktifUser) {
        if (getDistributor() == null) {
            withDistributor();
        }
        return getDistributor() != null
                && aktifUser.allowProviderOrDistributor()
                && getDistributor().isUserIdEqual(aktifUser.user_id);
    }

    default StatusMessage generateNegotiation(AktifUser aktifUser) {
        StatusMessage result = new StatusMessage();
        result.setPreparation();
        if (getStatus().allApproved()) {
            result = generateNonNegotiation(aktifUser);
        } else if (getStatus().isNegotiationBuyerApproved()) {
            result.setProviderPosition();
        } else if (getStatus().isNegotiationProviderApproved()) {
            result.setBuyerPosition();
        } else {
            result.position = getStatus().to_status;
        }
        return result;
    }

    default void preparation(AktifUser aktifUser) {
        LogUtil.d(TAG, "preparation");
        if (aktifUser == null) {
            LogUtil.d(TAG, "active user null");
            setStatusMessage(new StatusMessage());
            return;
        }

        if (getCommodity() == null) {
            withCommodity();
        }

        if (getStatus() == null) {
            withLatestStatus();
        }

        if (getProvider() == null) {
            withProvider(false);
        }

        if (getDistributor() == null) {
            withDistributor();
        }

        if (getBuyer() == null) {
            withPp();
        }

        if (getCommittee() == null) {
            withPp();
        }

        if (getTotalContractUploaded() == null || getTotalContractUploaded() == 0) {
            withTotalContractUploaded();
        }

    }

    default String[] getUserData() {
        String[] results = {"", ""};
        if (getStatus().fromProvider()) {
            results[0] = getProvider().nama_penyedia;
            results[1] = getProvider().user_name;
        } else if (getStatus().fromCommittee()) {
            results[0] = getCommittee().nama_lengkap;
            results[1] = getCommittee().user_name;
        } else if (getStatus().fromBuyer()) {
            results[0] = null != getBuyer() ? getBuyer().nama_lengkap : "" ;
            results[1] = null != getBuyer() ? getBuyer().user_name : "" ;
        }
        return results;
    }

    default void withStatusMessage(AktifUser aktifUser) {
        preparation(aktifUser);
        StatusMessage result = new StatusMessage();

        if (getStatus().isPackageRejected()) {
            LogUtil.debug(TAG,"on rejected state");
            final String[] userData = getUserData();
            result.setRejected(userData[0],
                    userData[1],
                    getStatus().getReadableModifiedDate(),
                    getStatus().deskripsi);
                    //getRejectedReason() ini alasan milik batal bukan reject
        } else if (getStatus().isPpkApproved()) {
            LogUtil.debug(TAG,"send to distributor for delivery");
            result.position = USER_DISTRIBUTOR;
        } else if (getStatus().isDelivery()) {
            LogUtil.debug(TAG,"on delivery state");
            result.position = USER_DISTRIBUTOR;
            result.setFinishedButton(isProductFullyAccepted()
                    && allowPpk(aktifUser)
                    && getTotalContractUploaded() > 0
                    && isPaymentCompleted());
        } else if (getStatus().isFinish()) {
            LogUtil.debug(TAG,"on finished state");
            result.position = USER_PPK;
            result.allowed = 0;
            result.rating = new PaketRating().fromArray(getFeedback());
        } else if (!getCommodity().isNegotiationRequired()) {
            LogUtil.debug(TAG,"package does not require negotiation");
            result = generateNonNegotiation(aktifUser);
        } else if (getCommodity().isNegotiationRequired()
                && getStatus().isNegotiationDone()
                && getStatus().getLatestHeader() != null
                && getStatus().allApproved()) {
            LogUtil.debug(TAG,"on negotiation state");
            result = generateNegotiation(aktifUser);
        } else if (getStatus().isPreparation()) {
            result.setPreparation();
        }
        result.packageId = getId();
        result.build();
        setButtonAllowance(aktifUser, result);
        showButtonAfterNegotiation(result);
        showButtonPilihDistributor(result);
        showButtonBatalPaket(result);
        showMoreOptionItem(result);
        setStatusMessage(result);
        LogUtil.debug(TAG,"status message: " + CommonUtil.toJson(result));
    }

    default boolean isUserAllowApproval(AktifUser activeUser) {
        if (getProvider() == null) {
            withProvider(false);
        }
        final boolean isAllowForApproval = (getProvider() != null && getProvider().isUserIdEqual(activeUser.user_id))
                || getPpId().equals(activeUser.user_id)
                || getPpkId().equals(activeUser.user_id);
        LogUtil.debug(TAG,"is allow for approval " + activeUser.user_id + " " + isAllowForApproval);
        return isAllowForApproval;
    }

    default String getNegotiationPosition() {
        if (getStatus() != null && getStatus().getLatestHeader() != null) {
            return getStatus().getLatestHeader().getNoteLabel().equalsIgnoreCase("penyedia")
                    ? getProvider() != null ? getProvider().nama_penyedia : ""
                    : getUserPp() != null ? getUserPp().nama_lengkap : "";
        }
        return "";
    }

    default void setButtonAllowance(AktifUser aktifUser, StatusMessage statusMessage) {
        if (aktifUser != null && statusMessage != null) {
            LogUtil.debug(TAG," set button allowance");
            statusMessage.allowAddDelivery = allowProviderOrDistributor(aktifUser)
                    && getStatus().isAllowed();
            statusMessage.allowAddAcceptance = allowPpk(aktifUser)
                    || allowProviderOrDistributor(aktifUser)
                    || allowDistributor(aktifUser)
//            statusMessage.allowAddAcceptance = allowPpk(aktifUser)
                    && getStatus().isAllowed();
            statusMessage.allowAddPayment = allowPpk(aktifUser) || allowProvider(aktifUser)
                    && getStatus().isAllowed();
            statusMessage.allowAddContract = allowPpk(aktifUser)
                    && getStatus().isAllowed();
            statusMessage.allowNegotiation = getCommodity().isNegotiationRequired()
                    && !(getStatus().isDrafted() || getStatus().isCanceld());
            LogUtil.debug(TAG, "allow negotiation: " + statusMessage.allowNegotiation);
            statusMessage.allowAddVerification = allowProvider(aktifUser) && getStatus().isAllowed();
            statusMessage.showContractMessage = getTotalContractUploaded() == 0
                    && getStatus().isDelivery()
                    && allowPpk(aktifUser);
        }
    }

    default void showButtonAfterNegotiation(StatusMessage statusMessage) {
        if (statusMessage != null) {
            statusMessage.showButtonsAfterNegotiation = !getStatus().isPackageRejected()
                    && (getStatus().isPpkApproved()
                    || getStatus().isDelivery()
                    || getStatus().isFinish()
                    || (userPpkProviderApproved()));
        }
    }

    default  void showButtonPilihDistributor(StatusMessage statusMessage){
        AktifUser aktifUser = getAktifUser();

            if (statusMessage != null && aktifUser.is_penyedia){
                statusMessage.showButtonPilihDistributor =  !getStatus().isDelivery()
                        && !getStatus().isFinish();
            }

    }

    default  void showButtonBatalPaket(StatusMessage statusMessage){
        AktifUser aktifUser = getAktifUser();

        if (statusMessage != null && (aktifUser.isPp() || aktifUser.isPpk())){
            statusMessage.showButtonBatalPaket =  !getStatus().isDelivery()
                    && !getStatus().isFinish();
        }

    }

    default boolean userPpkProviderApproved() {
        return getUserPp().isPpk() && getStatus().isProviderApproved();
    }

    default void showMoreOptionItem(StatusMessage statusMessage) {
        if (statusMessage != null) {
            LogUtil.d(TAG," set more option allowance");
            statusMessage.showMoreOptionItem = getStatus().isAllowed();
        }
    }

    default boolean isPaymentCompleted() {
        return PaymentRepository.isPaymentCompleted(getId());
    }

    default List<Feedback> getFeedback() {
        if (getId() != null) {
            return Feedback.getPackageFeedbacks(getId());
        }
        return Collections.emptyList();
    }

    default void isCommitteeAnUlp(StatusMessage model) {
        if (getProvider() != null && model != null) {
            getProvider().withUser();
            model.showUlpUrl = getProvider().user.isUlp();
        }
    }

    default boolean validate(String from, String to, boolean isApproved) {
        final boolean allowed = isApproved
                ? getAccessMessage().position.equalsIgnoreCase(from) && getAccessMessage().to.equalsIgnoreCase(to)
                : getAccessMessage().position.equalsIgnoreCase(from) && to.equalsIgnoreCase(USER_PANITIA);
        LogUtil.d(TAG, "is valid from: " + from + " to: " + to);
        return allowed;
    }

    default boolean allowAddDelivery() {
        return getStatus().isDelivery() || getStatus().isPpkApproved() || userPpkProviderApproved();
    }

    default boolean accessDelivery(AktifUser aktifUser) {
        return allowDistributor(aktifUser) || allowProviderOrDistributor(aktifUser);
    }

    default boolean accessAcceptance(AktifUser aktifUser) {
        return allowPpk(aktifUser) || allowPpAsPpk(aktifUser) || allowProviderOrDistributor(aktifUser) || allowDistributor(aktifUser);
//        return allowPpk(aktifUser) || allowPpAsPpk(aktifUser);
    }

    default boolean accessPayment(AktifUser aktifUser) {
//        return allowPpk(aktifUser) || allowPpAsPpk(aktifUser);
        return allowPpk(aktifUser) || allowPpAsPpk(aktifUser) || allowProvider(aktifUser);
    }

    default boolean accessPaymentVerification(AktifUser aktifUser) {
        return allowProvider(aktifUser) || allowProviderOrDistributor(aktifUser);
    }

}
