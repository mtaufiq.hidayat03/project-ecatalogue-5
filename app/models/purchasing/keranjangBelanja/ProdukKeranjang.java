package models.purchasing.keranjangBelanja;

import models.masterdata.KursNilai;
import play.Logger;

/**
 * Created by dadang on 11/1/17.
 */
public class ProdukKeranjang {

	public Long produkId;
	public String namaProduk;
	public String urlImage;
	public Long kuantitas;
	public Double hargaSatuan;
	public Double totalHarga;
	public Long kursId;
	public String namaKurs;
	public Long komoditasId;
	public String namaKomoditas;

	public Double getHargaSatuanRupiah() {
		if(kursId.intValue()==1){
			Logger.debug("hs %s", hargaSatuan);
			return hargaSatuan;
		}

		final KursNilai kn = KursNilai.getNewestKursByKursId(kursId);
		Logger.debug("kurs nilai beli: %s %s, %s",kn.nilai_beli, hargaSatuan, hargaSatuan * kn.nilai_beli);
		return hargaSatuan * kn.nilai_beli;
	}

}
