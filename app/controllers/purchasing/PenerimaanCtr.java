package controllers.purchasing;

import controllers.BaseController;
import controllers.security.AllowAccess;
import models.jcommon.blob.BlobTable;
import models.purchasing.Paket;
import models.purchasing.base.ProductJson;
import models.purchasing.penerimaan.LampiranPenerimaan;
import models.purchasing.penerimaan.PaketRiwayatPenerimaan;
import models.purchasing.penerimaan.form.AcceptanceDataForm;
import models.secman.Acl;
import play.i18n.Messages;
import services.paket.AcceptanceService;
import utils.LogUtil;

import java.util.List;

/**
 * @author HanusaCloud on 10/24/2017
 */
public class PenerimaanCtr extends BaseController {

    public static final String TAG = "PenerimaanCtr";

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PENERIMAAN})
    public static void riwayatPenerimaan(String language, Long paket_id) {
        Paket model = Paket.findById(paket_id);
        notFoundIfNull(model);
        if (model != null) {
            model.withAcceptances();
            model.withStatusMessage(getAktifUser());
        }
        renderArgs.put("paket", model);
        renderArgs.put("activeUser", getAktifUser());
        render("purchasing/PaketCtr/penerimaan/riwayatPenerimaan.html");
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PENERIMAAN_INPUT})
    public static void createPenerimaan(String language, Long paket_id) {
        Paket model = Paket.findById(paket_id);
        notFoundIfNull(model);
        if (!model.accessAcceptance(getAktifUser())) {
            LogUtil.d(TAG, "not allow to add acceptance");
            notFound();
        }
        if (model != null) {
            model.acceptanceHistory = new PaketRiwayatPenerimaan();
        }
        renderArgs.put("paket", model);
        renderArgs.put("action", "create");
        render("purchasing/PaketCtr/penerimaan/createPenerimaan.html");
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PENERIMAAN_INPUT})
    public static void updatePenerimaan(Long paket_id, Long id) {
        Paket model = Paket.findById(paket_id);
        notFoundIfNull(model);
        if (!model.accessAcceptance(getAktifUser())) {
            notFound();
        }
        if (model != null) {
            model.acceptanceHistory = PaketRiwayatPenerimaan.findById(id);
            notFoundIfNull(model.acceptanceHistory);
            if (model.acceptanceHistory != null) {
                model.acceptanceHistory.withDelivery();
                model.acceptanceHistory.withProducts();
            }
        }
        renderArgs.put("paket", model);
        renderArgs.put("action", "update");
        render("purchasing/PaketCtr/penerimaan/createPenerimaan.html");
    }

    public static void storePenerimaan(Long paket_id, AcceptanceDataForm model) {
        checkAuthenticity();
        try {
            model.packageId = paket_id;
            AcceptanceService.instance().store(paket_id, model);
            flash.success(Messages.get("notif.success.submit"));
        } catch (Exception e) {
            e.printStackTrace();
            flash.error(Messages.get("notif.failed.submit"));
        }
        redirect(new Paket(paket_id).getReportAcceptanceUrl());
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PENERIMAAN_INPUT})
    public static void deletePenerimaan(Long paket_id, Long id) {
        final boolean isSucceed = AcceptanceService.instance().deleteAcceptance(id, getAktifUser());
        if (!isSucceed) {
            flash.error(Messages.get("notif.failed.delete"));
        }
        redirect(new Paket(paket_id).getReportAcceptanceUrl());
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PENERIMAAN_INPUT})
    public static void searchProduct(Long paket_id, String keyword, String exception) {
        checkAuthenticity();
        List<ProductJson> results = AcceptanceService.instance().getProducts(paket_id, keyword, exception);
        renderJSON(results);
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PENERIMAAN})
    public static void downloadLampiran(Long id) {
        LampiranPenerimaan model = LampiranPenerimaan.findById(id);
        if (model != null) {
            BlobTable blobTable = model.withBlobContent();
            if (blobTable != null) {
                renderBinary(blobTable.getFile(), model.original_file_name);
            } else if (model.getFile().exists()) {
                renderBinary(model.getFile(), model.original_file_name);
            } else {
                error(Messages.get("notif.files_not_found"));
            }
        } else {
            error(Messages.get("notif.files_not_found"));
        }
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PENERIMAAN})
    public static void detailRiwayatPenerimaan(Long paket_id, Long id) {
        Paket model = Paket.findById(paket_id);
        notFoundIfNull(model);
        if (model != null) {
            model.acceptanceHistory = PaketRiwayatPenerimaan.findById(id);
            notFoundIfNull(model.acceptanceHistory);
            if (model.acceptanceHistory != null) {
                model.acceptanceHistory.withProducts();
                model.acceptanceHistory.withAttachment();
                model.acceptanceHistory.withDelivery();
            }
        }
        renderArgs.put("paket", model);
        render("purchasing/PaketCtr/penerimaan/detailRiwayatPenerimaan.html");
    }

}
