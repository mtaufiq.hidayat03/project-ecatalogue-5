package controllers.cms;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

import com.google.gson.Gson;
import controllers.BaseController;
import controllers.security.AllowAccess;
import ext.DatetimeBinder;
import models.cms.Berita;
import models.cms.KategoriKonten;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.secman.Acl;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import play.Logger;
import play.data.binding.As;
import play.i18n.Messages;
import play.libs.Files;
import play.Play;
import utils.HtmlUtil;
import utils.LogUtil;


import static models.cms.KategoriKonten.*;
public class BeritaCtr extends BaseController {

    @AllowAccess({Acl.COM_BERITA})
    public static void index(){
        List<KategoriKonten> kategoriKontenList = findAllByKonten(1);
        render(kategoriKontenList);
    }

    @AllowAccess({Acl.COM_BERITA_ADD})
    public static void create(){
        List<KategoriKonten> KategoriKontenList = findAllByKonten(1);
        render("cms/BeritaCtr/create.html",KategoriKontenList);
    }

    @AllowAccess({Acl.COM_BERITA_DELETE})
    public static void delete(long id) {
        Berita.deleteById(id);
        flash.success(Messages.get("notif.success.delete"));
        index();
    }

    public static String getRandom(){
        String SALTCHARS = "abcdefghijklmnopqrstuvwxyz1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    @AllowAccess({Acl.COM_BERITA_ADD})
    public static void createSubmit(String judul_konten, long kategori,String isi_konten,
                                    String slug, File gambar, String tags,
                                    @As(binder= DatetimeBinder.class) Date tanggal_konten,
                                    @As(binder= DatetimeBinder.class) Date publish_date_from,
                                    @As(binder=DatetimeBinder.class) Date publish_date_to) {
        try{
            Berita berita = new Berita();
            berita.default_konten_kategori_id = kategori;
            berita.judul_konten = judul_konten;
            berita.tags = tags.replace(",","-");
            berita.slug = judul_konten.replaceAll("\\s","0").replaceAll("\\W","").replace("0","-");
            berita.tanggal_konten = tanggal_konten;

            String decodedText = HtmlUtil.decodeFromTinymce(isi_konten);
            berita.isi_konten = decodedText;

            berita.status = "draft";
            berita.publish_date_from = publish_date_from;
            berita.publish_date_to = publish_date_to;
            berita.active = true;

//        if(gambar!= null){
//            String nama_file = getRandom()+"-"+gambar.getName().replace(" ","-");
//            File dokumenroot = Play.applicationPath;
//            File destination = new File(dokumenroot+"/public/files/image/berita/"+nama_file);
//            Files.copy(gambar,destination);
//            Map<String, String> result = new HashMap<>();
//            result.put("fotoPendamping",nama_file);
//            berita.param = new Gson().toJson(result);
//        }
            BlobTable blob = null;
            if(gambar != null){
                blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, gambar, berita.blb_id_content, Berita.class.getSimpleName());
                berita.nama_file = blob.blb_nama_file;
                berita.file_hash = blob.blb_hash;
                berita.blb_id_content = blob.id;
            }

            long id = berita.save();
            if(null != blob){
                blob.blb_id_content = id;
                blob.save();
            }

            flash.success(Messages.get("notif.success.draft"));
            edit(id);
        }catch (Exception e) {
            // TODO Auto-generated catch block
            LogUtil.d("***************** error ***************", e.getMessage());
            e.printStackTrace();
        }
    }

    @AllowAccess({Acl.COM_BERITA_EDIT})
    public static void detail(long id){
        Berita berita = Berita.findById(id);
        List<String> tags = new ArrayList<>();

        if(berita.tags != null && berita.tags != "")
            tags = Arrays.asList(berita.tags.split("-"));

        render("cms/BeritaCtr/detail.html",berita,tags);
    }

    @AllowAccess({Acl.COM_BERITA_EDIT})
    public static void edit(long id) {
        Berita viewberita = Berita.findById(id);
//        JSONParser parser = new JSONParser();
//        try {
//            if(null != viewberita.param) {
//                JSONObject json = (JSONObject) parser.parse(viewberita.param);
//                String pict = (String) json.get("fotoPendamping");
//                String[] arpath = pict.split("/");
//                if(arpath.length > 0){
//                    viewberita.param = arpath[(arpath.length - 1)];
//                }else{
//                    viewberita.param = pict;
//                }
//                Logger.info(viewberita.param);
//            }
//        } catch (ParseException e) {}

        String date_from;
        String date_to;
        String tags;

        SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        List<KategoriKonten> KategoriKontenList = findAllByKonten(1);
        String tanggal_berita = dateformat.format(viewberita.tanggal_konten);
        date_from = dateformat.format(viewberita.publish_date_from);

        if(viewberita.publish_date_to!=null) {
            date_to = dateformat.format(viewberita.publish_date_to);
        }else{
            date_to = null;
        }

        if(viewberita.tags!=null) {
            tags = viewberita.tags.replace("-", ",");
        }else{
            tags = "";
        }
        render("cms/BeritaCtr/edit.html",viewberita,tanggal_berita,date_from,date_to,KategoriKontenList,tags);
    }

    @AllowAccess({Acl.COM_BERITA_EDIT})
    public static void editSubmit(long id, long kategori, String judul_konten, String slug, String status,
                                  String isi_konten, File gambar, String tags,
                                  @As(binder= DatetimeBinder.class) Date tanggal_konten,
                                  @As(binder= DatetimeBinder.class) Date publish_date_from,
                                  @As(binder= DatetimeBinder.class) Date publish_date_to) {

        try{
            Berita berita = Berita.findById(id);
            berita.judul_konten = judul_konten;
            berita.default_konten_kategori_id = kategori;
            berita.slug = judul_konten.replaceAll("\\s","0").replaceAll("\\W","").replace("0","-");
            berita.tags = tags.replace(",","-");
            berita.status = status;

            String decodedText = HtmlUtil.decodeFromTinymce(isi_konten);
            berita.isi_konten = decodedText;

            berita.tanggal_konten = tanggal_konten;
            berita.publish_date_from = publish_date_from;
            berita.publish_date_to = publish_date_to;
            berita.active = true;

    //        if(gambar!= null){
    //            String nama_file = getRandom()+"-"+gambar.getName().replace(" ","-");
    //            File dokumenroot = Play.applicationPath;
    //            File destination = new File(dokumenroot+"/public/files/image/berita/"+nama_file);
    //            Files.delete(new File(dokumenroot+"/public/files/image/berita/"+berita.param));
    //            Files.copy(gambar,destination);
    //            Map<String, String> result = new HashMap<>();
    //            result.put("fotoPendamping",nama_file);
    //            berita.param = new Gson().toJson(result);
    //        }

            BlobTable blob = null;
            if(gambar != null){
                blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, gambar, berita.blb_id_content, Berita.class.getSimpleName());
                berita.nama_file = blob.blb_nama_file;
                berita.file_hash = blob.blb_hash;
                berita.blb_id_content = blob.id;
            }

            berita.save();
            if(null != blob){
                blob.blb_id_content = id;
                blob.save();
            }

            flash.success(Messages.get("notif.success.update"));
            edit(id);
        }catch (Exception e) {
            // TODO Auto-generated catch block
            LogUtil.d("***************** error ***************", e.getMessage());
            e.printStackTrace();
        }
    }

    public static void removePhoto(Long beritaId, Long btId){
        boolean rmBlob = removeFile(btId);
        if(rmBlob) {
            int rmDocument = Berita.removeFile(beritaId);
            renderJSON(rmDocument);
        }
    }

    public static boolean removeFile(Long blob_id) {
        try{
            BlobTable blobTable = BlobTable.findByBlobId(blob_id);
            if(blobTable != null){
                blobTable.preDelete();
                blobTable.delete();
            }
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
