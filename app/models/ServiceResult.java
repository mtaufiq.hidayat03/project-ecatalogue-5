package models;

/**
 * @author HanusaCloud on 3/26/2020 4:14 PM
 */
public class ServiceResult<T> {

    private final String message;
    private final Boolean status;
    private final T payload;

    public ServiceResult(String message) {
        this.message = message;
        this.status = false;
        this.payload = null;
    }

    public ServiceResult(Boolean status, String message) {
        this.message = message;
        this.status = status;
        this.payload = null;
    }

    public ServiceResult(Boolean status, String message, T payload) {
        this.message = message;
        this.status = status;
        this.payload = payload;
    }

    public Boolean isSuccess() {
        return status;
    }

    public T getPayload() {
        return payload;
    }

    public String getMessage() {
        return message;
    }

}
