package models.elasticsearch.response;

import com.google.gson.annotations.SerializedName;
import ext.FormatUtils;
import models.elasticsearch.product.InnerHit;
import models.elasticsearch.product.ProductElastic;
import models.elasticsearch.product.ProductPriceElastic;
import models.katalog.Produk;
import play.i18n.Lang;
import utils.UrlUtil;

import java.io.Serializable;

import static models.katalog.Produk.LIMIT_CHAR_TITLE;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class MetaProduct implements Serializable {

    @SerializedName("_index")
    public String index;
    @SerializedName("_type")
    public String type;
    @SerializedName("_id")
    public String id;
    @SerializedName("_version")
    public int version;
    @SerializedName("_score")
    public float score;
    @SerializedName("_source")
    public ProductElastic product = new ProductElastic();
    @SerializedName("inner_hits")
    public InnerHit innerHit = new InnerHit();
    public transient boolean isChecked = false;


    public String getTrimmedTitle() {
        final String productName = this.product.getManufactureName() + " " + this.product.getProductName();
        if(productName.length() < LIMIT_CHAR_TITLE) {
            return productName;
        }
        return productName.substring(0, LIMIT_CHAR_TITLE) + "...";
    }

    public long getId() {
        return product.id;
    }

    public String getPrice() {
        ProductPriceElastic innerPrice = innerHit.getPrice();
        if (innerPrice != null) {
            return FormatUtils.formatCurrencyRupiah(innerPrice.hargaUtama);
        }
        return product.getPrice();
    }
    public String isRegencyOrProvince(){
        ProductPriceElastic innerPrice = innerHit.getPrice();
        if (innerPrice != null) {
            //return innerPrice.type+" "+innerPrice.id+" "+ innerPrice.locationName;
            return innerPrice.locationName;
        }
        return "";
        //return innerPrice.type+" "+innerPrice.id+" "+ innerPrice.locationName;


        //return "oke";
//        if(product.productType.equalsIgnoreCase("province") ^ product.productType.equalsIgnoreCase("regency"))
//            return true;
//        else
//            return false;
    }

    public String getPriceByLocation(long location_id) {
        double price = new Double(0);
        try{
            price = product.priceList.stream()
                    .filter(it-> it.id == location_id).findFirst()
                    .orElse(new ProductPriceElastic()).hargaUtama;
        }catch (Exception e){}
        return FormatUtils.formatCurrencyRupiah(price);
    }

    public ProductPriceElastic getCurrentPrice() {
        ProductPriceElastic price = innerHit.getPrice();
        if (price != null) {
            return price;
        }
        return new ProductPriceElastic("general", -999, "");
    }

    public String getProductType() {
        return product.productType;
    }

    public String getImageUrl() {
        String pathImage = new String();
        if (product != null){
            pathImage = product.imageUrl;
//            if (product.imageUrl != null){
//                pathImage = product.imageUrl.replace("//","/");
//            }else {
//                pathImage = "/public/images/image-not-found.png";
//            }
            //Logger.info("produk image url:"+pathImage);
        }
//        if(Play.mode.isProd()){
//            if(!Play.configuration.getProperty("application.baseUrl").isEmpty()){
//                String urlhttps = Play.configuration.getProperty("application.baseUrl").replace("http","https");
//                String newPathImage = pathImage.replace(urlhttps,"");
//                Logger.info("new path replace baseurls:"+newPathImage);
//                newPathImage = newPathImage.replace("//","/");
//                Logger.info("new path replace //:"+newPathImage);
//                if(!KatalogUtils.fileExistInPathApps(newPathImage) && newPathImage.contains("https")){
//                    pathImage = newPathImage.replace("/public/files/image/","/public/files/upload/");
//                    Logger.info("into :"+pathImage);
//                }else{
//                    Logger.info("else :"+pathImage);
//                }
//            }
//        }
        return pathImage;
    }
    public String getProviderName() {
        return product.providerName;
    }

    public boolean getApakahIklan() {
        return product.apakahIklan;
    }

    public String isChecked() {
        return isChecked ? "checked" : "";
    }

    public boolean isAllowedToCompare() {
        ProductPriceElastic innerPrice = innerHit.getPrice();
        return innerPrice != null || product.priceList.size() == 1;
    }

    public String getDetailUrl(Long locationId) {

        UrlUtil.Builder builder = UrlUtil.builder();
        builder.setUrl("katalog.ProdukCtr.detail")
                .setParam("lang", Lang.get())
                .setParam("id", getId())
                .setParam("location_id",locationId);
        if (isAllowedToCompare()) {
            ProductPriceElastic model = product.priceList.get(0);
            if (model == null) {
                model = innerHit.getPrice();
            }
            if (model != null) {
                builder.setParam("type", model.type);
                if(locationId != null){
                    builder.setParam("location_id", locationId);
                }
            }
        } else {
            ProductPriceElastic model = null;
            if (product.priceList != null && !product.priceList.isEmpty()) {
                final int total = product.priceList.size();
                model = product.priceList.get(total == 1 ? 0 : total - 1);
            }
            if (model != null) {
                builder.setParam("type", model.type);
            }
        }
        return builder.build();
    }

    public boolean cekProdukAllowedBuy(){
        boolean result = false;
        try{
            Produk produk = Produk.findById(this.product.id);
            result = produk.apakah_dapat_dibeli;
        }catch (Exception e){}
        return result;
    }

    public boolean isUmkm() {
        return product != null && product.isUmkm;
    }

}
