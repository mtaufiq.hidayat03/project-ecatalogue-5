package models.elasticsearch.request;

import com.google.gson.JsonObject;

/**
 * @author HanusaCloud on 1/2/2018
 */
public class RequestCount {

    public JsonObject query;

    public RequestCount(RequestSearch model) {
        this.query = model.query;
    }

}
