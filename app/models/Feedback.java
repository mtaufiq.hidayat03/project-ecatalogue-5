package models;

import models.common.AktifUser;
import models.penyedia.Penyedia;
import models.purchasing.PaketRating;
import play.Logger;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * @author HanusaCloud on 11/21/2017
 */
@Table(name = "feedback")
public class Feedback extends BaseTable {

    public static final String TAG = "Feedback";

    public static final String TYPE_PACKAGE_ACCURACY   = "paket_accuracy";
    public static final String TYPE_PACKAGE_SPEED      = "paket_speed";
    public static final String TYPE_PACKAGE_SERVICE    = "paket_service";
    public static final String TYPE_PACKAGE_ADDITIONAL = "paket_additional";
    public static final String TYPE_PROVIDER           = "provider";

    @Id
    public Long id;
    public Long item_id;
    public String item_type;
    public Integer reporter_id;
    public String message;
    public Integer rating;
    public Timestamp created_date;

    @Override
    protected void prePersist() {
        super.prePersist();
        this.reporter_id = AktifUser.getActiveUserId();
        this.created_date = new Timestamp(new Date().getTime());
    }

    public boolean isPackageAccuracy() {
        return item_type.equalsIgnoreCase(TYPE_PACKAGE_ACCURACY);
    }

    public boolean isPackageSpeed() {
        return item_type.equalsIgnoreCase(TYPE_PACKAGE_SPEED);
    }

    public boolean isPackageService() {
        return item_type.equalsIgnoreCase(TYPE_PACKAGE_SERVICE);
    }

    public boolean isPackageAdditionalMessage() {
        return item_type.equalsIgnoreCase(TYPE_PACKAGE_ADDITIONAL);
    }

    public Feedback setFromAccuracyRating(PaketRating model) {
        this.item_id = model.packageId;
        this.message = model.accuracyMessage;
        this.rating = model.accuracy;
        this.item_type = TYPE_PACKAGE_ACCURACY;
        return this;
    }

    public Feedback setFromSpeedRating(PaketRating model) {
        this.item_id = model.packageId;
        this.message = model.speedMessage;
        this.rating = model.speed;
        this.item_type = TYPE_PACKAGE_SPEED;
        return this;
    }

    public Feedback setFromServiceRating(PaketRating model) {
        this.item_id = model.packageId;
        this.message = model.serviceMessage;
        this.rating = model.service;
        this.item_type = TYPE_PACKAGE_SERVICE;
        return this;
    }

    public Feedback setAdditionalMessage(PaketRating model) {
        this.item_id = model.packageId;
        this.message = model.additionalMessage;
        this.rating = -1;
        this.item_type = TYPE_PACKAGE_ADDITIONAL;
        return this;
    }

    public static List<Feedback> getPackageFeedbacks(Long id) {
        final String query = "SELECT * FROM feedback" +
                " WHERE item_id =?" +
                " AND item_type IN ('" + TYPE_PACKAGE_ACCURACY +
                "','" + TYPE_PACKAGE_ADDITIONAL +
                "','" + TYPE_PACKAGE_SERVICE + "','" + TYPE_PACKAGE_SPEED + "')";
        return Query.find(query, Feedback.class, id).fetch();
    }

    public Feedback setFromProvider(Penyedia model, int avgPackage) {
        if (model != null) {
            this.item_id = model.id;
            this.item_type = TYPE_PROVIDER;
            this.rating = avgPackage;
            this.message = "";
        } else {
            Logger.debug(TAG + " provider is null");
        }
        return this;
    }

}
