package models.masterdata;

import models.purchasing.negosiasi.PaketProdukNegoDetail;
import play.cache.Cache;
import play.cache.CacheFor;
import play.db.jdbc.*;
import java.util.ArrayList;
import java.util.List;

import models.BaseKatalogTable;

import javax.persistence.Transient;

@CacheMerdeka
@Table(name = "manufaktur")
public class Manufaktur extends BaseKatalogTable {

    @Id
    public Long id;
    public String nama_manufaktur;
    public String deskripsi;
    public boolean active;

    public static void deleteById(long id){
        Manufaktur man = Manufaktur.findById(id);
        man.active = false;
        man.save();
    }

    public static List<Manufaktur> findAllActive(){

        return Manufaktur.find("active = 1").fetch();
    }

    public static Manufaktur findManufakturByNamaManufaktur(String nama_manufaktur){
        String query = "select m.nama_manufaktur, m.id \n" +
                "from manufaktur m \n" +
                "where m.nama_manufaktur = ? and m.active = ? ";
        return Query.find(query, Manufaktur.class, nama_manufaktur, AKTIF).first();
    }

    @Transient
    private static String key1 = "findManufakturByKomoditas";
    public static List<Manufaktur> findManufakturByKomoditas(long komoditas_id){
        List<Manufaktur> results = Cache.get(key1+komoditas_id,List.class);
        if(null == results) {
            String query = "select m.nama_manufaktur, m.id \n" +
                    "from manufaktur m \n" +
                    "join komoditas_manufaktur km on m.id = km.manufaktur_id \n" +
                    "where km.komoditas_id = ? and m.active = ? ";
            results = Query.find(query, Manufaktur.class, komoditas_id, AKTIF).fetch();
            Cache.set(key1+komoditas_id,results, "30mn");
        }
        return results;
    }

    @Transient
    public static String key2 ="getManufacturesActive";
    //@CacheMerdeka
    public static List<Manufaktur> getManufactures() {
        List<Manufaktur> result = Cache.get(key2,List.class);
        if(null == result) {
            String query = "select m.nama_manufaktur, m.id " +
                    "from manufaktur m " +
                    "left join komoditas_manufaktur km on m.id = km.manufaktur_id " +
                    "left join komoditas k on km.komoditas_id = k.id " +
                    "where m.active = ? and km.active = ? and k.active = ? " +
                    "order by m.nama_manufaktur";
            try{
                result = Query.find(query, Manufaktur.class, AKTIF, AKTIF, AKTIF).fetch();
                Cache.set(key2,result,"30mn");
            }catch (Exception e){
                result = new ArrayList<>();
            }
        }
        return result;
    }

    @Transient
    private static String key3 = "findManufakturTayangByKomoditas";
    public static List<Manufaktur> findManufakturTayangByKomoditas(long komoditas_id){
        List<Manufaktur> results = Cache.get(key3+komoditas_id,List.class);
        if(null == results) {
            String query = "SELECT m.nama_manufaktur, m.id " +
                    "FROM produk p " +
                    "JOIN penawaran pw ON pw.id = p.penawaran_id " +
                    "JOIN penyedia_kontrak pk ON pk.id = pw.kontrak_id " +
                    "JOIN penyedia pn ON pn.id = pk.penyedia_id " +
                    "JOIN komoditas k ON p.komoditas_id = k.id " +
                    "JOIN manufaktur m ON p.manufaktur_id = m.id " +
                    "WHERE p.komoditas_id = ? " +
                    "AND m.active = 1 " +
                    "AND p.active = 1 " +
                    "AND p.apakah_ditayangkan = 1 " +
                    "AND k.active = 1 " +
                    "AND p.setuju_tolak = 'setuju' " +
                    "AND ((p.berlaku_sampai >= CURDATE() AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1) " +
                    "OR (p.berlaku_sampai is null AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1)) " +
                    "AND (k.apakah_stok_unlimited = 1 OR (k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0)) " +
                    "GROUP BY m.id ORDER BY m.nama_manufaktur ";
            results = Query.find(query, Manufaktur.class, komoditas_id).fetch();
            Cache.set(key3+komoditas_id,results, "30mn");
        }
        return results;
    }

    public void softDelete() {
        this.active = false;
        setDeleted();
        save();
    }

	public void setAktif() {
		this.active = true;
		prePersist();
		save();
	}

	public void setNonAktif() {
		this.active = false;
		prePersist();
		save();
	}

}