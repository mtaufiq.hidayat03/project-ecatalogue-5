package models.api.sirup;

import java.util.Date;

/**
 * Created by dadang on 12/12/17.
 */
public class PaketAnggaran {
	public Long id;
	public Long pktId;
	public Integer jenis;
	public Integer sumberDana;
	public String asalDana;
	public Long asalDanaSatker;
	public String mak;
	public Double pagu;
	public Date auditupdate;

}
