package models;

import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.MappedSuperclass;
import models.common.AktifUser;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Query;

/**Semua Model yang punya field ini harus extend dari sini
 * @author Andik Yulianto<andikyulianto@yahoo.com>
 */
@MappedSuperclass
public class BaseKatalogTable extends BaseTable {

    public Timestamp created_date = new Timestamp(new Date().getTime());
    public Integer created_by;
    public Timestamp modified_date;
    public Integer modified_by;
    public Timestamp deleted_date;
    public Integer deleted_by;

    public static final int AKTIF = 1;
    public static final int NOT_AKTIF = 0;

    protected void prePersist() {
        if(created_by == null){
            created_by = AktifUser.getActiveUserId();
        }else{
            modified_by = AktifUser.getActiveUserId();
            modified_date = new Timestamp(System.currentTimeMillis());
        }
    }

    protected void setDeleted() {
        deleted_by = AktifUser.getActiveUserId();
        deleted_date = new Timestamp(System.currentTimeMillis());
    }

    public <T extends BaseKatalogTable> T findByIdActive(Long id, Class<T> type) {
        return Query.find("SELECT * FROM " + getTableName() + " WHERE id =? AND active =?", type, id, 1).first();
    }

}
