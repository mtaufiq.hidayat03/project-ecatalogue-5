package services.produkkategori;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import models.common.AllLevelProdukKategori;
import models.katalog.Produk;
import models.katalog.ProdukKategori;
import models.katalog.ProdukKategoriAtribut;
import models.katalog.ProdukPosisi;
import models.katalog.komoditasmodel.KomoditasProdukAtributTipe;
import models.prakatalog.UsulanKategoriProduk;
import org.apache.http.util.TextUtils;
import play.Logger;
import repositories.produkkategori.ProdukKategoriAtributRepository;
import repositories.produkkategori.ProdukKategoriRepository;
import utils.LogUtil;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProdukKategoriService {

	public static final String TAG = "ProdukKategoriService";

	/**
	 * @param positions this is json list in string
	 * @param id komoditas id*/
	public static void savePosition(String positions, long id) {
		Logger.debug(TAG + " save position");
		Logger.debug(TAG + " positions " + positions);
		if (TextUtils.isEmpty(positions)) {
			positions = "[]";
		}
		Type collectionType = new TypeToken<Collection<ProdukPosisi>>() {
		}.getType();
		List<ProdukPosisi> products = new Gson().fromJson(positions, collectionType);
		if (products != null && !products.isEmpty()) {
			for (ProdukPosisi model : products) {
				if (model.id > 0) {
					ProdukKategoriRepository.savePosisi(new ProdukKategori(model));
				}
			}
		}
		Logger.debug(TAG + " - " + new Gson().toJson(products));
		ProdukKategoriRepository.clearCache(id);
	}

	/**
	 * @param pkid produk kategori id
	 * @return boolean if it has been deleted or not*/
	public static boolean delete(long pkid) {
		ProdukKategori model = ProdukKategori.findById(pkid);
		if (model != null) {
			ProdukKategori child = ProdukKategoriRepository.getChild(pkid);
			Produk produk = Produk.getProdukByKategori(pkid);
			ProdukKategoriAtribut produkKategoriAtribut = ProdukKategoriAtributRepository.getAtibutByKategori(model.id);
			if (child == null && produkKategoriAtribut == null && produk == null) {
				model.softDelete();
				ProdukKategoriRepository
						.clearCache(model.komoditas_id);
				return true;
			}
		}
		return false;
	}

	/**
	 * @return List of AllLevelProdukKategori
	 */
	public static List<AllLevelProdukKategori> generateSingleRowAllLevelKategori(List<ProdukKategori> list){
		List<AllLevelProdukKategori> allLevelList = new ArrayList<AllLevelProdukKategori>();

		for(ProdukKategori pk : list){

			// Level 1
			StringBuilder parentNamaKategori = new StringBuilder();
			parentNamaKategori.append(pk.nama_kategori);

			Logger.debug(pk.nama_kategori+": "+pk.children.size()+"");

			if(!pk.children.isEmpty()){
				Logger.debug("child1");
				parentNamaKategori.append(" > ");
				for(ProdukKategori child1 : pk.children){

					// Level 2
					Logger.debug(parentNamaKategori.toString());

					StringBuilder child1NamaKategori = new StringBuilder();
					child1NamaKategori.append(parentNamaKategori);
					child1NamaKategori.append(child1.nama_kategori);

					Logger.debug(child1NamaKategori.toString());

					if(!child1.children.isEmpty()){
						Logger.debug("child2");
						child1NamaKategori.append(" > ");
						for(ProdukKategori child2 : child1.children){

							// Level 3
							StringBuilder child2NamaKategori = new StringBuilder();
							child2NamaKategori.append(child1NamaKategori);
							child2NamaKategori.append(child2.nama_kategori);

							if(!child2.children.isEmpty()){
								Logger.debug("child3");
								child2NamaKategori.append(" > ");
								for(ProdukKategori child3 : child2.children){

									// Level 4
									StringBuilder child3NamaKategori = new StringBuilder();
									child3NamaKategori.append(child2NamaKategori);
									child3NamaKategori.append(child3.nama_kategori);

									if(!child3.children.isEmpty()){
										Logger.debug("child4");
										child3NamaKategori.append(" > ");
										for(ProdukKategori child4 : child3.children){

											// Level 5
											StringBuilder child4NamaKategori = new StringBuilder();
											child4NamaKategori.append(child3NamaKategori);
											child4NamaKategori.append(child4.nama_kategori);

											AllLevelProdukKategori allLevels = new AllLevelProdukKategori();
											allLevels.kategori = child4NamaKategori.toString();
											allLevels.id = child4.id;

											allLevelList.add(allLevels);

										}
									}else{
										AllLevelProdukKategori allLevels = new AllLevelProdukKategori();
										allLevels.kategori = child3NamaKategori.toString();
										allLevels.id = child3.id;

										allLevelList.add(allLevels);
									}
								}
							}else{
								AllLevelProdukKategori allLevels = new AllLevelProdukKategori();
								allLevels.kategori = child2NamaKategori.toString();
								allLevels.id = child2.id;

								allLevelList.add(allLevels);
							}
						}
					}else{
						AllLevelProdukKategori allLevels = new AllLevelProdukKategori();
						allLevels.kategori = child1NamaKategori.toString();
						allLevels.id = child1.id;

						allLevelList.add(allLevels);
					}
				}

			}else{
				AllLevelProdukKategori allLevels = new AllLevelProdukKategori();
				allLevels.kategori = parentNamaKategori.toString();
				allLevels.id = pk.id;

				allLevelList.add(allLevels);
			}

		}

		return allLevelList;
	}


	public static List<ProdukKategori> getTemporaryListFromPositionJson(String json){
		List<ProdukKategori> result = new ArrayList<ProdukKategori>();

		Type type = new TypeToken<List<ProdukPosisi>>(){}.getType();
		List<ProdukPosisi> positions = new Gson().fromJson(json,type);

		for(ProdukPosisi posisi : positions){
			ProdukKategori kategori = new ProdukKategori();
			kategori.id  = posisi.id;
			kategori.nama_kategori = posisi.name;
			kategori.level_kategori = posisi.depth;
		}

		return result;
	}

	public static List<AllLevelProdukKategori> generateSingleRowKategoriByUsulan(List<ProdukKategori> list, List<UsulanKategoriProduk> ukp){
		List<AllLevelProdukKategori> allProdukKategoriList = generateSingleRowAllLevelKategori(list);
		List<AllLevelProdukKategori> result = new ArrayList<AllLevelProdukKategori>();
		if (ukp != null) {
			for (AllLevelProdukKategori alpk : allProdukKategoriList) {
				for (UsulanKategoriProduk u : ukp) {
					if (u.produk_kategori_id == alpk.id) {
						result.add(alpk);
					}
				}
			}
			return result;
		} else {
			return allProdukKategoriList;
		}
	}

	public static List<Long> getAllLastChild(List<ProdukKategori> list){
		List<Long> result = new ArrayList<>();
		List<AllLevelProdukKategori> allProdukKategoriList = generateSingleRowAllLevelKategori(list);

		LogUtil.d("alkp",new Gson().toJson(allProdukKategoriList));

		for(AllLevelProdukKategori alpk : allProdukKategoriList){
			result.add(alpk.id);
		}

		return result;
	}

	public static void saveUsulanKategori(long usulan_id, long komoditas_id){
		List<ProdukKategori> kategoriList = ProdukKategoriRepository.getProdukKategoriList(komoditas_id);
		List<Long> lastChildIds = getAllLastChild(kategoriList);
		for(Long lastChildId : lastChildIds){
			UsulanKategoriProduk ukp = new UsulanKategoriProduk();
			ukp.usulan_id = usulan_id;
			ukp.produk_kategori_id = lastChildId;
			ukp.active = true;
			ukp.save();
		}
	}

}
