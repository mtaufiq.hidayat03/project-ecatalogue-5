package controllers.katalog.form;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import models.jcommon.util.CommonUtil;
import models.katalog.ProdukAtributValue;
import models.masterdata.Kabupaten;
import models.masterdata.Provinsi;
import org.apache.commons.lang.ArrayUtils;
import org.apache.http.util.TextUtils;
import utils.DateTimeUtils;
import utils.LogUtil;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 12/10/2017
 */
public class ProductForm {

    public String action = "create";
    public String status = "";
    public String kelas_harga = "";
    public String kode_komoditas = "";
    public String infoJson = "";
    //akan disimpan sebagai ongkir dan harga, lihat produkservice.saveProduk, tab-UploadHargaProduk.html
    public String hargaJson = "";
    public String ongkirJson = "";
    public String harga_utama = "";
    public String checkedLocations = "";
    public String reason = "";
    public String tanggal_harga = "";
    public int[] file_id;
    public int[] pict_id;
    public List<ProductPriceForm> price;
    public String xlsJson = "";

    public boolean isCreatedMode() {
        return action.equalsIgnoreCase("create");
    }

    public BigDecimal getMainPrice() {
        BigDecimal mainPrice = new BigDecimal(0);
        LogUtil.d("test", CommonUtil.toJson(price));
        if (this.price != null && !this.price.isEmpty()) {
            for(ProductPriceForm p : price){
                if(p.isMain){
                    mainPrice = p.getValue();
                }
            }
        }
        return mainPrice;
    }

    public JsonArray getHargaJsonArray() {
        return !TextUtils.isEmpty(hargaJson)
                ? new Gson().fromJson(hargaJson, JsonArray.class)
                : null;
    }

    public JsonArray getOngkirJsonArray() {
        return !TextUtils.isEmpty(ongkirJson)
                ? new Gson().fromJson(ongkirJson, JsonArray.class)
                : null;
    }

    public String getOngkirJsonArrayFromPrice(){
        JsonArray result = new JsonArray();

        if (!TextUtils.isEmpty(hargaJson)) {

			JsonArray hargaJsonArray = getHargaJsonArray();
			if (hargaJsonArray != null && hargaJsonArray.size() > 0) {

				for (int i = 0; i < hargaJsonArray.size(); i++) {
                    JsonObject item = new JsonObject();
					JsonObject obj = hargaJsonArray.get(i).getAsJsonObject();

					String namaProvinsiFromJson = obj.get("Provinsi").getAsString();
					Provinsi provinsi = Provinsi.findByName(namaProvinsiFromJson);
					item.addProperty("provinsiId", provinsi.id);

					String namaKabupatenFromJson = obj.get("Kabupaten").getAsString();
					Kabupaten kabupaten = Kabupaten.findByName(namaKabupatenFromJson);
					item.addProperty("kabupatenId", kabupaten.id);

					item.addProperty("ongkir", obj.get("Referensi Ongkos Kirim").getAsBigDecimal());

                    result.add(item);
				}

			}

		}
		return CommonUtil.toJson(result);
    }

    public int getAgrWilayahJual() {
        JsonArray priceJsonArray = getHargaJsonArray();
        return kelas_harga.equalsIgnoreCase("nasional")
                ? 0
                : priceJsonArray != null ? priceJsonArray.size() : 0;
    }

    public String getXlsJson() {
        return !TextUtils.isEmpty(xlsJson) ? xlsJson : "{\"price\":[],\"cost\":[]}";
    }

    public int getTotalPicture() {
        return !ArrayUtils.isEmpty(pict_id) ? pict_id.length : 0;
    }

    public int getTotalAttachment() {
        return !ArrayUtils.isEmpty(file_id) ? file_id.length : 0;
    }

    public Date parseKursDate() {
        return DateTimeUtils.parseDdMmYyyy(this.tanggal_harga);
    }

    public Map<Long,BigDecimal> createMapFromProductPriceForm(){
        Map<Long,BigDecimal> result = null;

        if(price != null && !price.isEmpty()){
            result = new HashMap<>();
            for(ProductPriceForm p : price){
                result.put(p.atribut_id,p.getValue());
            }
        }

        return result;
    }

    public boolean apakahValidProdukAtributValue(List<ProdukAtributValue> list){
        return false;
    }

    public boolean isDefaultDraft() {
        return !TextUtils.isEmpty(status) && status.equalsIgnoreCase("draft");
    }

}
