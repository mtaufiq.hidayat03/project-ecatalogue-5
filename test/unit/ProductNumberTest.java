package unit;

import models.katalog.Produk;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author HanusaCloud on 5/6/2020 9:14 AM
 */
@RunWith(Parameterized.class)
public class ProductNumberTest {

    Long currentUnspscId;
    String currentNoProduct;
    String commodityCode;
    Long unspscId;
    String sequence;
    String expected;

    public ProductNumberTest(
            Long currentUnspscId,
            String currentNoProduct,
            String commodityCode,
            Long unspscId,
            String sequence,
            String expected
    ) {
        this.currentUnspscId = currentUnspscId;
        this.currentNoProduct = currentNoProduct;
        this.commodityCode = commodityCode;
        this.unspscId = unspscId;
        this.sequence = sequence;
        this.expected = expected;
    }

    @Test
    public void shouldBeAbleToGroupByCategory() {
        Assert.assertEquals(
                expected,
                Produk.generateProductNumber(
                        currentNoProduct,
                        commodityCode,
                        unspscId,
                        sequence
                )
        );
    }


    @Parameterized.Parameters
    public static Collection categorySampleData() {
        return Arrays.asList(
                new Object[][] {
                        {null, "", "commodity", 1L, "00001", "1-commodity-00001"},
                        {null, null, "commodity", 1L, "00001", "1-commodity-00001"},
                        {1L, null, "commodity", 1L, "00001", "1-commodity-00001"},
                        {1L, "1-commodity-00001", "commodity", 2L, "00001", "2-commodity-00001"},
                        {1L, "0-commodity-00001", "commodity", 2L, "00001", "2-commodity-00001"},
                        {1L, "0-commodity-00001", "commodity", 1L, "00001", "1-commodity-00001"},
                        {2L, "2-commodity-00001", "commodity", null, "00001", "2-commodity-00001"},
                        {1L, "1-commodity-00001", "commodity", 1L, "00001", "1-commodity-00001"},
                }
        );
    }

}
