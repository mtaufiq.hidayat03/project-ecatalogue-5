package models.api;

import models.blacklist.Pelanggaran;
import models.jcommon.util.CommonUtil;
import play.Logger;
import utils.KatalogUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by reza on 27/08/18
 */
public class DcePelanggaran {
	public String sk;
	public String started;
	public String expired;
	public String published_date;
	
	public DceDetailPelanggaran detil_pelanggaran;
	public static final SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");


	public Pelanggaran getAsPelanggaran(){
		try{
			Pelanggaran pelanggaran = new Pelanggaran();
			pelanggaran.jenis_pelanggaran = detil_pelanggaran.jenis_pelanggaran;
			pelanggaran.deskripsi_pelanggaran = detil_pelanggaran.deskripsi;
			pelanggaran.created_at = tryParse(detil_pelanggaran.dibuat);
			pelanggaran.sk = sk;
			pelanggaran.started_at = tryParse(started);
			pelanggaran.expired_at = tryParse(expired);
			pelanggaran.published_at = tryParse(published_date);

			return pelanggaran;
		}catch (Exception e){
			Logger.error("error parse DcePelanggaran to pelanggaran"+ e.getMessage());
			e.printStackTrace();
		}
		return null;
	}


	private Date tryParse(String s) {
		if(s==null || s.isEmpty())
			return null;
		try {
			return df.parse(s);
		} catch (ParseException e) {
			return null;
		}
	}

}
