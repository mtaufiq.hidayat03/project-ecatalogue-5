CREATE DATABASE  IF NOT EXISTS `ekatalog5` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ekatalog5`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: ekatalog5
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `configuration`
--

DROP TABLE IF EXISTS `configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuration` (
  `cfg_category` varchar(80) NOT NULL,
  `cfg_sub_category` varchar(80) NOT NULL,
  `audittype` char(1) NOT NULL DEFAULT 'C',
  `audituser` varchar(100) NOT NULL DEFAULT 'ADMIN',
  `auditupdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `cfg_value` varchar(2000) DEFAULT NULL,
  `cfg_comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`cfg_category`,`cfg_sub_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuration`
--

LOCK TABLES `configuration` WRITE;
/*!40000 ALTER TABLE `configuration` DISABLE KEYS */;
INSERT INTO `configuration` VALUES ('CONFIG','file.storage-dir','C','ADMIN','2017-05-07 11:49:02','C:\\Temp\\ekatalog5',NULL),('CONFIG','mail.retry','C','ADMIN','2017-05-07 14:20:12','5',NULL),('CONFIG','mail.sender','C','ADMIN','2017-05-07 11:49:02','e-katalog@lkpp.go.d',NULL),('CONFIG','mail.smtp.host','C','ADMIN','2017-05-07 11:49:02','localhost',NULL),('CONFIG','mail.smtp.port','C','ADMIN','2017-05-07 11:49:02','25',NULL);
/*!40000 ALTER TABLE `configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_queue`
--

DROP TABLE IF EXISTS `mail_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exception` text,
  `to_addresses` varchar(1000) DEFAULT NULL,
  `cc_addresses` varchar(1000) DEFAULT NULL,
  `bcc_addresses` varchar(1000) DEFAULT NULL,
  `subject` varchar(1000) DEFAULT NULL,
  `from_address` varchar(255) DEFAULT NULL,
  `body` text,
  `mime` decimal(1,0) DEFAULT '0',
  `retry` decimal(2,0) DEFAULT NULL,
  `enqueue_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `send_date` datetime DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `noemail` text,
  `lls_id` decimal(19,0) NOT NULL DEFAULT '0',
  `jenis` int(11) NOT NULL DEFAULT '0',
  `rkn_id` decimal(19,0) DEFAULT '0',
  `bcstat` decimal(1,0) NOT NULL DEFAULT '1',
  `prioritas` int(11) DEFAULT '2',
  `audittype` varchar(1) DEFAULT NULL,
  `auditupdate` datetime DEFAULT NULL,
  `audituser` varchar(100) DEFAULT NULL,
  `engine_version` varchar(50) DEFAULT NULL,
  `ref_1` bigint(20) DEFAULT NULL,
  `ref_2` bigint(20) DEFAULT NULL,
  `ref_3` bigint(20) DEFAULT NULL,
  `ref_a` varchar(100) DEFAULT NULL,
  `ref_b` varchar(100) DEFAULT NULL,
  `ref_c` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_queue`
--

LOCK TABLES `mail_queue` WRITE;
/*!40000 ALTER TABLE `mail_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'ekatalog5'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-08  7:50:01
