package controllers.masterdata;

import controllers.BaseController;
import controllers.security.AllowAccess;
import models.common.AktifUser;
import models.masterdata.Kldi;
import models.masterdata.KldiSatker;
import models.secman.Acl;
import models.user.User;
import play.i18n.Messages;

import java.util.ArrayList;
import java.util.List;

public class ProfilSatkerCtr extends BaseController {

    @AllowAccess({Acl.COM_PROFIL_SATUAN_KERJA})
    public static void index() {
        User user = User.findById(AktifUser.getAktifUser().user_id);
        List<Kldi> jenisKldiList = Kldi.findJenis();
        List<Kldi> kldiList = new ArrayList<>();
        List<KldiSatker> kldiSatkerList = new ArrayList<>();
        try{
            if(null != user.kldi_id) {
                kldiList = Kldi.findIdKldi(user.kldi_id);
            }
            if(null != user.satker_id) {
                kldiSatkerList = KldiSatker.findIdSatker(user.satker_id);
            }
        }catch (Exception e) {

        }
        render(jenisKldiList, kldiList, kldiSatkerList, user);
    }

    @AllowAccess({Acl.COM_PROFIL_SATUAN_KERJA_EDIT})
    public static void editSubmit(User user){
        KldiSatker kldiSatker = KldiSatker.findById(user.satker_id);
        User aktifUser = User.findById(user.id);
        aktifUser.user_email = user.user_email;
        aktifUser.satuan_kerja_nama = kldiSatker.nama;
        aktifUser.satuan_kerja_alamat = user.satuan_kerja_alamat;
        aktifUser.satuan_kerja_npwp = user.satuan_kerja_npwp;
        aktifUser.no_telp = user.no_telp;
        aktifUser.nip = user.nip;
        aktifUser.jabatan = user.jabatan;
        aktifUser.nomor_sertifikat_pbj = user.nomor_sertifikat_pbj;
        aktifUser.satker_id = user.satker_id;
        aktifUser.kldi_jenis = user.kldi_jenis;
        aktifUser.kldi_id = user.kldi_id;
        aktifUser.save();
        flash.success(Messages.get("notif.success.update"));
        index();
    }

    @AllowAccess({Acl.COM_PROFIL_SATUAN_KERJA_EDIT})
    public static void getInstansi(String jenisInstansi){
        renderJSON(Kldi.findByJenis(jenisInstansi));
    }

    @AllowAccess({Acl.COM_PROFIL_SATUAN_KERJA_EDIT})
    public static void getSatker(String idKldi){
        renderJSON(KldiSatker.findByKldi(idKldi));
    }
}
