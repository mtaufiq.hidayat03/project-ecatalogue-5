package models.elasticsearch;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class MetaData {

    public String index;
    public String type;
    public long version;
    public long score;

}
