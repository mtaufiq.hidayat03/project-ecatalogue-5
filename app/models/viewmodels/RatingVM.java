package models.viewmodels;

import play.data.validation.Required;

public class RatingVM {

    @Required
    public Long pointRating_id;
    @Required
    public String nama_point;
    @Required
    public String deskripsi;
    @Required
    public Long jumlah;
    @Required
    public Long banyak;
    @Required
    public Long rating;

}
