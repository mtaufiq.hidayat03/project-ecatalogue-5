package models.masterdata;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;

@Table(name = "kldi_satker")
public class KldiSatker extends BaseTable {

    @Id
    public Long id;
    public String nama;
    public String idSatker;
    public String idKldi;
    public boolean isDeleted;
    public String statusPerubahan;

    public static List<KldiSatker> findIdSatker(long id){
        return Query.find("SELECT * FROM kldi_satker WHERE id = " + id,KldiSatker.class).fetch();
    }

    public static List<Kldi> findByKldi(String idKldi){
        return find("idKldi = ?", idKldi).fetch();
    }
}
