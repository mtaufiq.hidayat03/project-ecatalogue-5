package models.penyedia;

import com.google.gson.Gson;
import models.BaseKatalogTable;
import models.masterdata.Kldi;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import utils.LogUtil;

import java.util.List;

//base on kldi wilayah, mengakomodir kebutuhan katalog lokal, dan sektoral
@Table(name = "penyedia_sektoral")
public class PenyediaSektoral extends BaseKatalogTable {

    @Id
    public Long id;
    public Long penyedia_id;
    public String kldi_id;

    public PenyediaSektoral(){}

    public static  boolean saveNew(String kldiSektoral, Long penyedia_id){
        try{
            Kldi kldi = Kldi.findById(kldiSektoral);
            PenyediaSektoral kw = new PenyediaSektoral();
            kw.kldi_id = kldi.id;
            kw.penyedia_id = penyedia_id;
            kw.save();
        }catch (Exception e){
            return false;
        }
        return true;
    }

    public static void deleteNotIn(Penyedia penyedia, List<String> selected){
        List<PenyediaSektoral> list = penyedia.getPenyediaSektoral();
        for (PenyediaSektoral kw: list) {
            if(!selected.contains(kw.kldi_id)){
                kw.delete();
            }
        }
    }

    public PenyediaSektoral(String kldiwilayah, long penyedia_id){
        this.kldi_id = kldiwilayah;
        this.penyedia_id = penyedia_id;
    }

    public static void addNew(String kldiwilayah, long penyedia_id){
        PenyediaSektoral ps = find("kldi_id = ? AND penyedia_id = ?",kldiwilayah, penyedia_id).first();
        LogUtil.d("ps new ",new Gson().toJson(ps));
        if(ps == null){
            ps = new PenyediaSektoral(kldiwilayah, penyedia_id);
            int ids = ps.save();
        }
    }

    public static void deleteByPenyediaSektoral(String kldiwilayah, long penyedia_id){
        delete("penyedia_id = ? AND kldi_id = ?", penyedia_id, kldiwilayah);
    }
}
