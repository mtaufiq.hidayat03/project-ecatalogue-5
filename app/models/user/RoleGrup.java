package models.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.sql2o.ResultSetHandler;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;

@Table(name="role_grup")
public class RoleGrup extends BaseTable {

	@Id
	public Long id;
	public String nama_grup;
	public String kategori;
	public boolean active;
	
/*
 1	Admin
2	PPK
3	Panitia
4	Penyedia
5	CMS
6	Distributor
7	Pejabat Pengadaan
8	Auditor
 */
	//TODO perlu komunikasi dulu dg Syahrul
	//Role yg login lewat LPSE
	public static final String LOGIN_FROM_LPSE="2,4,6,3,7";
	//Role yg login dari katalog langsung
	public static final String LOGIN_FROM_KATALOG="1,5,8,9,2,4,6,3,7";
	//Role penyedia (default)
	public static final long PENYEDIA = 2;

	public static final long PANITIA_ULP = 3;

	public static final long PANITIA_PP = 11;

	@Transient
	public List<RoleItem> items = new ArrayList<>();

	public static List<RoleGrup> findAllActive(){
		String query = "Select id, nama_grup, kategori from role_grup where active = 1";
		
		return Query.find(query, RoleGrup.class).fetch();
	}
	
	public static List<RoleGrup> findAllByKategori(String kategori){
		String query = "Select id, nama_grup, kategori from role_grup where active = 1 and kategori = ?";
		
		return Query.find(query, RoleGrup.class, kategori).fetch();
	}
	
	public static List<RoleGrup> findAllKategori(){
		String query = "Select DISTINCT(kategori) kategori from role_grup where active = 1";
		
		return Query.find(query, RoleGrup.class).fetch();
	}
	
	public static final ResultSetHandler<String[]> resultsetGrupRoleItem = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet arg0) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}
		
	};

	public String getTrimmedKategori() {
		return this.kategori.replace(" ", "");
	}
}
