package models.cms;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.*;

/**
 * Created by user on 11/3/2017.
 */
@Table(name = "polling_hasil")
public class PollingHasil extends BaseKatalogTable{
    @Id
    public Long id;
    public Long polling_id;
    public Long polling_pilihan_id;
    public String client_ip;
    public Boolean active;

    public static List<PollingHasil> findAllActive(){return PollingHasil.find("active = 1").fetch();}

    public static List<PollingHasil> findAllByExist(Long id, Long polId){
        String query = "SELECT * FROM polling_hasil WHERE active = 1 and created_by = " + id + " and polling_id = " + polId;
        return Query.find(query,PollingHasil.class).fetch();
    }
}
