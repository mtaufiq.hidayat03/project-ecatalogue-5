package models.elasticsearch.query;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import models.elasticsearch.contract.ElasticsearchRequestContract;
import models.elasticsearch.product.ViewHitCounter;
import models.elasticsearch.query.aggregation.TermAggregation;
import models.elasticsearch.request.RequestSearch;
import models.elasticsearch.response.MetaProduct;
import models.katalog.Produk;
import models.logging.Logging;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import play.Logger;

import java.util.*;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class ElasticsearchUtil {

    public static final String TYPE_COUNTER = "product_counter";


    public static final String QUERY = "query";
    public static final String BOOL = "bool";
    public static final String MUST = "must";
    public static final String FILTER = "filter";

    public static final String AND_OPERATOR = "and";

    public static class QueryUtil {

        private int limit = 10;
        private int offset = 0;
        private QueryStringParam queryStringParam;
        private List<NestedQuery> nestedFilters = new ArrayList<>();
        private List<Sort> sorts = new ArrayList<>();
        private List<BoolFilter> filters = new ArrayList<>();
        private List<String> fields = new ArrayList<>();
        private Map<String, TermAggregation> aggs = new HashMap<>();
        private List<BoolShould> boolShould = new ArrayList<>();

        public QueryUtil setQueryString(QueryStringParam queryString) {
            this.queryStringParam = queryString;
            return this;
        }

        public QueryUtil setNestedFilter(NestedQuery query) {
            if (query != null) {
                this.nestedFilters.add(query);
            }
            return this;
        }

        public QueryUtil setAggregation(TermAggregation aggregation) {
            this.aggs.put(aggregation.getAlias(), aggregation);
            return this;
        }

        public QueryUtil setFilter(BoolFilter boolFilter) {
            if (boolFilter != null) {
                this.filters.add(boolFilter);
            }
            return this;
        }

        public QueryUtil setReturnFields(String[] fields) {
            this.fields.addAll(Arrays.asList(fields));
            return this;
        }

        public QueryUtil boolShould(BoolShould model) {
            this.boolShould.add(model);
            return this;
        }

        public QueryUtil setLimit(int limit) {
            this.limit = limit;
            return this;
        }

        public QueryUtil setOffset(int offset) {
            this.offset = offset;
            return this;
        }

        public QueryUtil setSorting(Sort sort) {
            this.sorts.add(sort);
            return this;
        }

        private JsonObject generateQueryElement() {
            JsonArray jsonArray = new JsonArray();
            JsonObject jsonObject = new JsonObject();
            JsonObject must = new JsonObject();
            if (queryStringParam != null) {
                jsonArray.add(queryStringParam.buildQueryString());
            }
            if (!nestedFilters.isEmpty()) {
                for (NestedQuery filter : nestedFilters) {
                    jsonArray.add(filter.build());
                }
            }

            if (!nestedFilters.isEmpty() || queryStringParam != null) {
                must.add(MUST, jsonArray);
            }

            if (!boolShould.isEmpty()) {
                JsonArray multiMatches = new JsonArray();
                for (BoolShould model : boolShould) {
                    multiMatches.add(model.build());
                }
                must.add("should", multiMatches);
            }

            if (!filters.isEmpty()) {
                JsonArray filterArray = new JsonArray();
                for (BoolFilter j : filters) {
                    filterArray.add(j.build());
                }
                must.add(FILTER, filterArray);
            }

            jsonObject.add(BOOL, must);
            return jsonObject;
        }

        private JsonObject generateAggregation() {
            JsonObject result = new JsonObject();
            if (!aggs.isEmpty()) {
                for (Map.Entry<String, TermAggregation> entry : aggs.entrySet()) {
                    TermAggregation value = entry.getValue();
                    result.add(value.getAlias(), value.build());
                }
            }
            return result;
        }

        public RequestSearch buildRequest() {
            RequestSearch request = new RequestSearch();
            request.query = generateQueryElement();
            request.aggs = generateAggregation();
            request.from = offset;
            request.size = limit;
            if (!sorts.isEmpty()) {
                request.sort = new ArrayList<>();
                for (Sort sort : sorts) {
                    request.sort.add(sort.build());
                }
            }
            if (!fields.isEmpty()) {
                request.fields = this.fields;
            }
            return request;
        }

    }

    public static class InputUtil implements ElasticsearchRequestContract {

        public RequestBody generateBulk(Produk model, Long userId) {
            return getRequestBody(new ViewHitCounter(model).setVisitedBy(userId).generatePayload());
        }

        public RequestBody generateBulk(List<Produk> produks) {
            StringBuilder sb = new StringBuilder();
            for (Produk model : produks) {
                sb.append(new ViewHitCounter(model).generatePayload());
            }
            return getRequestBody(sb.toString());
        }

        public RequestBody generateProductElasticBulk(List<MetaProduct> produks) {
            StringBuilder sb = new StringBuilder();
            for (MetaProduct model : produks) {
                sb.append(new ViewHitCounter(model.product).generatePayload());
            }
            Logger.debug(" json result: " + new GsonBuilder().setPrettyPrinting().create().toJson(sb.toString()));
            return getRequestBody(sb.toString());
        }

    }

}
