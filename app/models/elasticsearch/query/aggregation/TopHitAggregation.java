package models.elasticsearch.query.aggregation;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import models.elasticsearch.query.Sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author HanusaCloud on 10/10/2017
 */
public class TopHitAggregation {

    private String alias;
    private Sort sort;
    private int size = 1;
    private List<String> sources = new ArrayList<>();

    public TopHitAggregation setAlias(String value) {
        this.alias = value;
        return this;
    }

    public TopHitAggregation sort(Sort sort) {
        this.sort = sort;
        return this;
    }

    public TopHitAggregation size(int size) {
        this.size = size;
        return this;
    }

    public TopHitAggregation includeSource(String[] sources) {
        this.sources.addAll(Arrays.asList(sources));
        return this;
    }

    public String getAlias() {
        return alias;
    }

    public JsonObject build() {
        JsonObject result = new JsonObject();
        JsonObject model = new JsonObject();
        if (sort != null) {
            model.add("sort", sort.build());
        }
        if (!sources.isEmpty()) {
            JsonArray jsonArray = new JsonArray();
            for (String s : sources) {
                jsonArray.add(s);
            }
            model.add("_source", jsonArray);
        }
        model.addProperty("size", size);
        result.add("top_hits", model);
        return result;
    }

}
