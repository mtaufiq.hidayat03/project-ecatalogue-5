package models.penyedia;

import com.google.gson.Gson;
import models.BaseKatalogTable;
import models.user.User;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.LogUtil;

import javax.persistence.Transient;
import java.util.List;

@Table(name = "penyedia_komoditas")
public class PenyediaKomoditas extends BaseKatalogTable {

	@Id
	public Long id;
	public Long penyedia_id;
	public Long komoditas_id;
	public boolean active;

	@Transient
	public String nama_komoditas;

	public PenyediaKomoditas(){}

	public PenyediaKomoditas(long penyedia_id, long komoditas_id){
		this.penyedia_id = penyedia_id;
		this.komoditas_id = komoditas_id;
		this.active = true;
	}

	public static void deleteByPenyedia(long id){
		delete("penyedia_id = ?",id);
	}

	public static void deleteByPenyediaKomoditas(long penyedia_id, long komoditas_id){
		delete("penyedia_id = ? AND komoditas_id = ?", penyedia_id, komoditas_id);
	}

	public static List<Long> findIdsByPenyedia(long penyedia_id){
		String query = "SELECT komoditas_id " +
				"FROM penyedia_komoditas " +
				"WHERE penyedia_id = ? ";

		return Query.find(query,Long.class,penyedia_id).fetch();
	}

	public static List<PenyediaKomoditas> findIdsByPenyediaId(long penyedia_id){
		String query = "SELECT komoditas_id " +
				"FROM penyedia_komoditas " +
				"WHERE penyedia_id = ? ";

		return Query.find(query,Long.class,penyedia_id).fetch();
	}

	public static void addNew(long penyedia_id, long komoditas_id){
		PenyediaKomoditas pk = find("penyedia_id = ? AND komoditas_id = ? AND active = ?",penyedia_id, komoditas_id, AKTIF).first();
		LogUtil.d("pk new ",new Gson().toJson(pk));
		if(pk == null){
			pk = new PenyediaKomoditas(penyedia_id,komoditas_id);
			pk.save();
		}else {
			if(!pk.active){
				pk.active = true;
				pk.save();
			}
		}
	}

	public static List<PenyediaKomoditas> getIdPenyediaByUserKomoditasId(Long id){
		String query = "SELECT penyedia_id FROM penyedia_komoditas " +
				"WHERE komoditas_id = ?";
		return Query.find(query,PenyediaKomoditas.class,id).fetch();
	}
}
