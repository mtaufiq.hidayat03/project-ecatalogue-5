package models.cms;

import ext.DatetimeBinder;
import models.common.AktifUser;
import play.cache.CacheFor;
import play.data.binding.As;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import models.BaseKatalogTable;
@Table(name = "notifikasi")
public class Notifikasi extends BaseKatalogTable{
    @Id
    public Long id;
    public String deskripsi;
    public String status;
    public String param;
    public boolean active;
    @As(binder = DatetimeBinder.class)
    public Date publish_date_from;

    @As(binder = DatetimeBinder.class)
    public Date publish_date_to;

    public static void deleteById (long id){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String date_now = dateFormat.format(date);

        String query = "update notifikasi set active = 0, deleted_by = " + AktifUser.getActiveUserId() + ", deleted_date = '"+date_now+"' where id = ?";
        Query.update(query, id);
    }

    @CacheFor("5mn")
    public static List<Notifikasi> findAllActive(){
        return Notifikasi.find("active = 1").fetch();
    }

    @CacheFor("5mn")
    public static Notifikasi findActive (){
        String query = "select deskripsi,param from notifikasi where active = 1 and status = 'published' and publish_date_from <= now() and (publish_date_to is NULL OR publish_date_to >= now())";
        return Query.find(query,Notifikasi.class).first();
    }
}
