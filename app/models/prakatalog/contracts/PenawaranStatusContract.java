package models.prakatalog.contracts;

import models.prakatalog.Penawaran;
import org.apache.http.util.TextUtils;

import java.util.Arrays;
import java.util.List;

/**
 * @author HanusaCloud on 12/22/2017
 */
public interface PenawaranStatusContract {

    String getStatusPenawaran();

    default boolean allowToAddProduct() {
        List<String> strings = Arrays.asList(Penawaran.NOT_ALLOWED_ADD_PRODUCT);
        return !TextUtils.isEmpty(getStatusPenawaran()) && !strings.contains(getStatusPenawaran());
    }

    default boolean allowToProduct() {
        List<String> strings = Arrays.asList(Penawaran.NOT_ALLOWED_ADD_PRODUCT);
        return !TextUtils.isEmpty(getStatusPenawaran());
    }
}
