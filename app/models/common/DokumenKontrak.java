package models.common;

import ext.DateBinder;
import ext.FormatUtils;
import models.jcommon.blob.BlobTable;
import models.prakatalog.DokumenPenawaran;
import models.prakatalog.DokumenUsulan;
import play.Logger;
import play.data.binding.As;
import play.data.binding.types.DateTimeBinder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * model untuk informasi saat generate dokumen kontrak
 * @author lkpp
 *
 */
public class DokumenKontrak implements Serializable {

	public Long komoditas;

	public String namaKomoditas;
	
	public String nomorSurat;

	public String tanggalTerbilang;

	public String hari;

	public String tanggal;

	public String bulan;

	public String tahun;

	public String namaPihakPertama;
	
	public String jabatanPihakPertama;

	public String landasan;

	public String perihalLandasan;

	public String namaPihakKedua;

	public String jabatanPihakKedua;

	public String namaBadanUsaha;

	public String alamatBadanUsaha;

	public String nomorAkta;

	public String tanggalAkta;

	public String nomorAktaPerubahan;

	public String tanggalAktaPerubahan;

	public String tanggalBerlakuKontrak;

	public String jumlahPenyesuaianHarga;

	//Korespondensi
	public String namaLKPP;
	public String alamatLKPP;
	public String teleponLKPP;
	public String websiteLKPP;
	public String faksLKPP;
	public String wakilSahLKPP;

	public String namaPenyedia;
	public String alamatPenyedia;
	public String teleponPenyedia;
	public String websitePenyedia;
	public String faksPenyedia;
	public String emailPenyedia;
	public String wakilSahPenyedia;


}
