package controllers.masterdata;

import controllers.BaseController;
import controllers.security.AllowAccess;
import models.jcommon.util.CommonUtil;
import models.masterdata.SumberKurs;
import models.masterdata.Kurs;
import models.masterdata.KursNilai;
import models.purchasing.Paket;
import models.secman.Acl;
import play.Logger;
import play.i18n.Messages;
import services.produk.ProdukService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

public class SumberKursCtr extends BaseController{

    @AllowAccess({Acl.COM_SUMBER_KURS})
    public static void index(){
        render();
    }

    @AllowAccess({Acl.COM_SUMBER_KURS_ADD})
    public static void create(){
        render();
    }

    @AllowAccess({Acl.COM_SUMBER_KURS_ADD})
    public static void createSubmit(String[] nama_sumber_arr) {
        checkAuthenticity();
        if(nama_sumber_arr != null){
            //validasi sumberKurs
            boolean validate = validateSumberKurs(nama_sumber_arr);
            if(!validate){
                if (validation.hasErrors())
                    flash.error("Gagal Simpan Sumber Kurs, periksa kembali inputan anda.");
                List<String> sumberKursList = Arrays.asList(nama_sumber_arr);
                render("masterdata/SumberKursCtr/create.html", sumberKursList);
            }
            for(int i = 0; i < nama_sumber_arr.length; i++){
                SumberKurs sd = new SumberKurs();
                sd.sumber = nama_sumber_arr[i];
                sd.active = true;
                sd.save();
            }

            flash.success("Data berhasil disimpan");
            index();
        }
    }

    public static void detail(long id) {
        SumberKurs model = SumberKurs.findById(id);
        notFoundIfNull(model);
        render("masterdata/SumberKursCtr/detail.html", model);
    }

    @AllowAccess({Acl.COM_SUMBER_KURS_EDIT})
    public static void editSubmit(long id_sumber, String nama_sumber) {
        checkAuthenticity();
        if (CommonUtil.isEmpty(nama_sumber)) {
            flash.error("Gagal Ubah sumberKurs, nama sumberKurs tidak boleh kosong");
        } else {
            SumberKurs sumberKurs = SumberKurs.findById(id_sumber);
            sumberKurs.sumber = nama_sumber;
            sumberKurs.active = true;
            sumberKurs.save();
        }
        flash.success("Data berhasil disimpan");
        index();
    }

    @AllowAccess({Acl.COM_SUMBER_KURS_DELETE})
    public static void delete(long id) {
        SumberKurs model = SumberKurs.findById(id);
        notFoundIfNull(model);
        if (!sumberKursIsInUse(id)) {
            model.delete();
            flash.success(Messages.get("notif.success.delete"));
        } else {
            flash.error(Messages.get("notif.failed.delete.relation"));
        }
        index();
    }

    private static boolean sumberKursIsInUse(long id) {
        final KursNilai aKursNilai = KursNilai.find("kurs_sumber_id=? and active=?", id, 1).first();
		return aKursNilai != null;
    }

    private static boolean validateSumberKurs(String[] nama_sumber_arr){
        boolean validate = true;
        for(int i = 0; i < nama_sumber_arr.length; i++){
            if(CommonUtil.isEmpty(nama_sumber_arr[i])) {
                validate = false;
                validation.addError("sumberKurs_"+(i+1), "Nama SumberKurs Harus Diisi", "var");
            }
        }
        return validate;
    }

    @AllowAccess({Acl.COM_SUMBER_KURS_EDIT})
    public static void setAktif(long id) {
        SumberKurs sumberKurs = SumberKurs.findById(id);
        notFoundIfNull(sumberKurs);
        sumberKurs.setAktif();
        flash.success(Messages.get("record_update_aktifnonaktif_relation_success"));
        index();
    }

    @AllowAccess({Acl.COM_SUMBER_KURS_EDIT})
    public static void setNonAktif(long id) {
        SumberKurs sumberKurs = SumberKurs.findById(id);
        notFoundIfNull(sumberKurs);
        if(!sumberKursIsInUse(id)){
            sumberKurs.setNonAktif();
            flash.success(Messages.get("record_update_aktifnonaktif_relation_success"));
        }else{
            flash.error(Messages.get("record_update_aktifnonaktif_relation_failed"));
        }
        index();
    }

}
