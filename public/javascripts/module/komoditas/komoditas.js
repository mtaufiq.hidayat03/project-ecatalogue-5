
//Template Jadwal
CKEDITOR.replace( 'description' );

$("body").on("click","#removeInput",function(){

    $(this).parent().parent().remove();
    counterTemplateJadwal -= 1;
});

function generateSelectTahapan(obj,jadwalIndex,nego,penawaran){
    var tahapanValue = null;
    var result = "<tr>" +
        "<td>" +
        "<select index='"+jadwalIndex+"' name='form.tahapan_arr' id='tahapan_"+jadwalIndex+"' class='form-control tahapan_drop_down'>" +
        //Pilih tahapan value='' karena untuk validasi, kalo 0 dianggap ada value
        "<option value=''>"+pleaseSelctLabel+"</option>";

    for(var i = 0; i < tahapanArray.length;i++){
        var selected = "";
        if(obj[jadwalIndex] == tahapanArray[i].id){
            selected = "selected";
            tahapanValue = obj[jadwalIndex];
        }
        result += "<option value='"+tahapanArray[i].id+"' "+selected+">"+tahapanArray[i].nama+"</option>";
    }
    result += "</select></td>";

    var checkedPenawaran = penawaran != null && penawaran == tahapanValue ? "checked" : "";
    var checkedNego = nego != null && nego == tahapanValue ? "checked" : "";

    result +=
        "<td>" +
            "<div class='radio-"+jadwalIndex+"'>" +
                "<input type='radio' id='penawaran"+jadwalIndex+"' onclick='radioPenawaran("+jadwalIndex+")' name='form.penawaran' "+checkedPenawaran+" value='"+tahapanValue+"'/>" +
            "</div>" +
        "</td>";

    if(!isLelangOrDatafeed){
        result +=
            "<td>" +
                "<div class='radio-"+jadwalIndex+"'>" +
                    "<input type='radio' id='negosiasi"+jadwalIndex+"' onclick='radioNego("+jadwalIndex+")' name='form.negosiasi' "+checkedNego+" value='"+tahapanValue+"'/>" +
                "</div>" +
            "</td>";
    }
    result += "<td><a id='removeInput' index='"+jadwalIndex+"' class='btn btn-danger'><i class='fa fa-trash'></i></a></td>" +
        "</tr>";

    return result;

}

function radioNego(jadwalIndex) {
    document.getElementById("penawaran"+jadwalIndex).checked = false;
}

function radioPenawaran(jadwalIndex) {
    document.getElementById("negosiasi"+jadwalIndex).checked = false;
}


$("#tambah_tahapan").on("click",function(){

    var str = generateSelectTahapan(tahapanObj,jadwalIndex,selectedNego,selectedPenawaran);
    $("#tahapan_jadwal").append(str);
    jadwalIndex += 1;
    counterTemplateJadwal += 1;
});

$("body").on("change",".tahapan_drop_down",function(){
    var index = $(this).attr("index");
    var this_value = $(this).val();

    var penawaran = "#penawaran"+index;
    if(!isLelangOrDatafeed){
        var negosiasi = "#negosiasi"+index;
        $(negosiasi).val(this_value);
    }

    $(penawaran).val(this_value);
    tahapanObj[index] = this_value;

});

$("body").on("change","input[name='form.penawaran']",function(){
    var this_value = $(this).val();
    selectedPenawaran = this_value;

});

$("body").on("change","input[name='form.negosiasi']",function(){
    var this_value = $(this).val();
    selectedNego = this_value;

});

$("#tambah-kategori").on("click",function(){
    isKategoriChange = true;
    var kategori = "<li id=\"list_"+kategori_index+"\" class=\"mjs-nestedSortable-branch mjs-nestedSortable-expended\" style=\"display: list-item;\">";
    kategori += "<div>"+
        "<button type='button' class='remove-kategori btn btn-danger'><i class='fa fa-trash'></i></button>" +
        "<span class=\"disclose\"><span></span></span>" +
        "<input id=\"input-name-"+kategori_index+"\" name=\"namaKategori[]\" size=\"30\" type=\"text\">" +
        "</div>";
    kategori += "</li>";


    $("#list-kategori").append(kategori);
    kategori_index++;

});

$("#removeAllkategories").on("click",function(){
    $("#list-kategori").empty();
});

$("body").on("click",".remove-kategori",function () {
    var button = $(this);
    button.parent().parent().remove();
    isKategoriChange = true;

});

$("#remove-all-kategori").on("click",function () {
    $("#list-kategori ol").remove();
    isKategoriChange = true;
});

$("#addManufaktur").on("click",function(){
    var str = "<tr><td><a id='removeManufaktur' class='btn btn-danger'><i class='fa fa-trash'></i></a></td><td><input class='form-control' name='form.manufaktur'/></td></tr>";
    counterManufaktur += 1;
    $("#alert-manufaktur").hide();
    $("#manufakturForm").append(str);
});

$("body").on("click","#removeManufaktur",function(){
    $(this).parent().parent().remove();
    counterManufaktur = counterManufaktur - 1;
});

$("#removeAllManufaktur").on("click",function(){
    $("#manufakturForm tr").remove();
    counterManufaktur = 0;
});

function  generateInsertAtributProduk(items){
    $("#accordion").html("");
    var arrayName = [];
    var counter = 0;

    for(var i in items){
        var item = items[i];
        var nextDepth = null;
        var nextIndex = parseInt(i)+1;
        var nextItem = items[nextIndex];

        if(nextItem != undefined || nextItem != null){
            nextDepth = nextItem.depth;
        }

        if(nextDepth == null || nextDepth == item.depth || nextDepth < item.depth){
            var resultHtml = generateFormAtributProduk(counter,item,arrayName);

            $("#accordion").append(resultHtml);
            $("#content-"+counter).html(formHtmlAtributKategori);
            atributKategoriObj["content-"+counter] = 0;
            counter++;

            //
            for(var a = arrayName.length; a >= nextDepth; a--){
                arrayName.splice(a,1);
            }

        }else{
            arrayName[item.depth] = item.name;
        }
    }

    var html = $("#accordion").html();
    $("#atributKategoriHtml").val(html);


}

function generateFormAtributProduk(counter, item, arrayName){
    var $panel = $("<div>", {"class": "panel panel-default"});

    //panel heading
    var $panel_head = $("<div>", {"class": "panel-heading","role":"tab",id:"heading"+counter});
    var $panel_title = $("<h4>", {"class": "panel-title"});
    var $a = $("<a>", {"role": "button","data-toggle":"collapse", "data-parent":"#accordion", "href":"#collapse"+counter,"aria-expanded":"true",
        "aria-controls":"collapse"+counter});
    var $i = $("<i>",{"class":"more-less glyphicon glyphicon-plus"});

    arrayName[item.depth] = item.name;
    var kategori = arrayName.join(" > ");

    $a.append($i);
    $a.append(" ");
    $a.append(kategori);
    $panel_title.append($a);
    $panel_head.append($panel_title);
    //end of panel heading

    var $collapse = $("<div>",{id:"collapse"+counter,"class": "panel-collapse collapse","role":"tabpanel","aria-labelledby":"heading"+counter, "style":"overflow-x: auto"});
    var $panel_body = $("<div>",{id:"content-"+counter,"class":"panel-body","kategori-id":item.id,"atribut-counter":0,"panel-count":counter});

    $collapse.append($panel_body);

    $panel.append($panel_head);
    $panel.append($collapse);

    return $panel;
}

var temporaryId = 0;
$("body").on("click",".addAtribut",function () {
    var panel_counter = $(this).parent().parent().attr("panel-count");
    var panel = $("#content-"+panel_counter);
    var kategori_id = panel.attr('kategori-id');
    var atribut_counter = panel.attr('atribut-counter');
    var str = '<tr data-value="">' +
        '<td><a class="btn btn-danger remove-atribut ladda-button trash-' + temporaryId + '" data-value="'+temporaryId+'" data-style="zoom-out"><i class="fa fa-trash"></i></a></td>' +
        '<td><input class="form-control input-attr" name="formKategoriAtribut['+panel_counter+'].kategoriAtributList['+atribut_counter+'].label_atribut" type="text" required>' +
        '<input name="formKategoriAtribut['+panel_counter+'].kategoriId" type="hidden" value="'+kategori_id+'"></td>' +
        '<td><input class="form-control input-attr-desc" name="formKategoriAtribut['+panel_counter+'].kategoriAtributList['+atribut_counter+'].deskripsi" type="text" value="-"/></td>' +
        '<td>' +
        '<select id="tipeInput" onchange="disableCheckbox()" class="form-control dropdown-input-type valid" name="formKategoriAtribut['+panel_counter+'].kategoriAtributList['+atribut_counter+'].tipe_input">' +
        '<option value="text" selected="selected">Text</option>' +
        '<option value="textarea">Textarea</option>' +
        '<option value="label">Label</option>' +
        '</select>' +
        '</td>' +
        '<td>' +
        generateSelect(panel_counter,atribut_counter) +
        '</td>'+
        '<td class="header-center"><input id="checkboxs" class="input-is-required" value="1" name="formKategoriAtribut['+panel_counter+'].kategoriAtributList['+atribut_counter+'].wajib_diisi" type="checkbox"></td>' +
        '</tr>';
    temporaryId++;
    atribut_counter++;
    panel.attr("atribut-counter",atribut_counter);
    var tbody = $("#content-"+panel_counter).find('tbody');
    tbody.append(str);
    atributKategoriObj["content-"+panel_counter] += 1;
});

$("body").on("change",".input-attr-desc",function () {
    var value = $(this).val();
    if(value == null || value == ""){
        $(this).val("-");
    }
});

$("body").on("click",".remove-atribut",function(){
    var panel_counter = $(this).parent().parent().attr("panel-count");
    atributKategoriObj["content-"+panel_counter] -= 1;
    $(this).parent().parent().remove();
    // console.log(atributKategoriObj);
});

$("body").on("click",".removeAllAtribut",function(){
    var panel_counter = $(this).parent().parent().attr("panel-count");
    $("#content-"+panel_counter+" #inputForm tr").remove();
    atributKategoriObj["content-"+panel_counter] = 0;
    // console.log(atributKategoriObj);
});

function generateSelect(panel_counter,atribut_counter) {
    var types = JSON.parse(tipeAtributJson);
    var template = '<select required class="form-control dropdown-information-type valid" name="formKategoriAtribut['+panel_counter+'].kategoriAtributList['+atribut_counter+'].tipe_atribut">';
    template += '' ;
    for (var i in types) {
        template += '<option value="' + types[i] + '">' + types[i] + '</option>';
    }
    template += '</select>';
    return template;
}

$("#addTipeAtribut").on("click",function(){
    var str = "<tr><td><a id='removeTipeAtribut' class='btn btn-danger'><i class='fa fa-trash'></i></a></td><td><input class='form-control' name='form.tipeAtribut'/></td><td><textarea class='form-control' name='form.deskripsiTipeAtribut' ></textarea></td></tr>";
    counterTipeAtribut += 1;
    $("#alert-tipe-atribut-kosong").hide();
    $("#tipeAtributForm").append(str);
});

$("body").on("click","#removeTipeAtribut",function(){
    $(this).parent().parent().remove();
    counterTipeAtribut = counterTipeAtribut - 1;
});

$("#removeAllTipeAtribut").on("click",function(){
    $("#tipeAtributForm tr").remove();
    counterTipeAtribut = 0;
});

function copyRowData(src, dest){

    var attrLabelValue = src.children(".input-attr").val();
    var attrLabelDescription = src.children(".input-attr-desc").val();
    var wajibDiisiChecked = src.children(".input-is-required").prop('checked');
    
    var tipeInput = src.children(".dropdown-input-type").val();
    var tipeInformasi = src.children(".dropdown-information-type").val();

    dest.children(".input-attr").val(attrLabelValue);
    dest.children(".input-attr-desc").val(attrLabelDescription);
    dest.children(".input-is-required").prop('checked',wajibDiisiChecked);

    dest.children(".dropdown-input-type").val(tipeInput).change();
    dest.children(".dropdown-information-type").val(tipeInformasi).change();

    // console.log(" kategoriRow input-attr value = "+dest.children(".input-attr").val() +" should be "+attrLabelValue);
}

function duplicateValues() {
    var addButtons = $(".addAtribut");
    var removeButtons = $(".removeAllAtribut");
    var kategoriDivs = addButtons.parent().parent();

    var firstKategoriDiv = $(kategoriDivs[0]);
    var firstKategoriRows = firstKategoriDiv.children(".table-condensed").children("#inputForm").children();

    var rowCount = firstKategoriRows.length;

    var restRemoveButton = removeButtons.slice(1);
    for (let removeButton of restRemoveButton){
        removeButton.click();
    }
    var restAddButton = addButtons.slice(1);
    for(let addButton of restAddButton){
        for (var j = 0; j < rowCount; j++) {
            addButton.click();
        }
    }

    var restKategoriDivs = kategoriDivs.slice(1);

    for (let kategoriDiv of restKategoriDivs){
        var kategoriRows = $(kategoriDiv).children(".table-condensed").children("#inputForm").children();
        for (var i = 0 ; i < rowCount ; i++){
            var row = $(firstKategoriRows[i]).children();
            var kategoriRow = $(kategoriRows[i]).children();

            copyRowData(row, kategoriRow);

        }
    }
}
$("#copyEntries").on("click",function(){
    duplicateValues();
});

function disableCheckbox() {
    var tipeInput = $("#tipeInput").val();

    if (tipeInput == "label"){
        $("#checkboxs").prop("disabled",true);
    }
}