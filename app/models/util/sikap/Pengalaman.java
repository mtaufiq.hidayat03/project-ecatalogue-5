package models.util.sikap;

import play.data.validation.Required;

/**
 * Created by dadang on 7/11/17.
 */
public class Pengalaman {

	@Required
	public long rkn_id;
	@Required
	public long pgl_id;
	@Required
	public String pgl_kegiatan;
	@Required
	public String pgl_keterangan;
	@Required
	public String pgl_lokasi;
	@Required
	public long pgl_nilai;
	@Required
	public String pgl_nokontrak;
	@Required
	public String pgl_persenprogress;
	@Required
	public String pgl_tglkontrak;
	@Required
	public String pgl_slskontrak;
	@Required
	public String pgl_pembtgs;
	@Required
	public String pgl_almtpembtgs;
	@Required
	public String pgl_telppembtgs;
	@Required
	public String pgl_tglprogress;
	@Required
	public String kategori;
	@Required
	public String kategori_keterangan;

	public String status_verifikasi;
	public boolean is_verified = false;

	public String auditupdate;
	public String source_data;
	public String createdat;

	public boolean deleted = false;
	public String alasan_pembatalan = "";
	public String keterangan_pembuktian = "";

}
