package models.logging;


import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;
import groovy.util.logging.Log4j;
import models.common.AktifUser;
import models.elasticsearch.contract.ElasticsearchRequestContract;
import models.jcommon.util.CommonUtil;
import okhttp3.RequestBody;
import org.sql2o.Sql2oException;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.mvc.Http;
import retrofit2.Call;
import retrofit2.Response;
import services.elasticsearch.ElasticsearchConnection;
import utils.LogUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * @author HanusaCloud on 11/28/2017
 */
@Table(name = "audit_trail")
public class Logging extends BaseTable implements ElasticsearchRequestContract {

    public static final String TAG = "Logging";
    public static final boolean TODB = false;

    @Id
    public transient Long id;
    @SerializedName("action")
    public String action = "";
    @SerializedName("path")
    public String path = "";
    @SerializedName("payload")
    public String payload = "{}";
    @SerializedName("remoteAddress")
    public String remoteAddress = "";
    @SerializedName("method")
    public String method = "";
    @SerializedName("session")
    public String session = "";
    @SerializedName("created_by")
    public Integer created_by = 0;
    @SerializedName("created_at")
    public String created_at = "";
    @SerializedName("user_agent")
    public String user_agent = "";
    @SerializedName("query_string")
    public String query_string = "";

    public Logging(Http.Request request, String session, AktifUser aktifUser) {
        this.action = request.action;
        this.path = request.path;
        this.remoteAddress = request.remoteAddress;
        this.method = request.method;
        if (request.params != null && request.params.data != null) {
            String[] strings = request.params.data.get("body");
            this.payload = strings != null && strings.length > 0 ? strings[0] : "";
        }
        this.query_string = request.querystring;
        this.session = session;
        if (request.headers != null && request.headers.get("user-agent") != null) {
            List<String> list = request.headers.get("user-agent").values;
            this.user_agent = !list.isEmpty() ? list.get(0) : "";
        }
        this.created_by = aktifUser != null ? aktifUser.user_id.intValue() : 0;
        this.created_at = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZ")
                .format(Calendar.getInstance().getTime());
    }

    private String getPayload() {
        final String index = "{ \"index\":  { \"_index\": \"ecatalogue-audit-trail\", \"_type\": \"logging\" }}\n";
        final String result = CommonUtil.toJson(this);
        return index + result + "\n";
    }

    private void saveToDb() {
        try {
            LogUtil.d(TAG, "log to db start...");
            save();
            LogUtil.d(TAG, "log to db done");
        } catch (Sql2oException e) {
            LogUtil.e(TAG, e);
        }
    }

    public void saveLog() {
        if (TODB) {
            saveToDb();
        } else {
            saveToElastic();
        }
    }

    private void saveToElastic() {
        try {
            LogUtil.d(TAG, "start logging..");
            RequestBody request = getRequestBody(getPayload());
            Call<JsonElement> call = ElasticsearchConnection.open().bulkAuditTrail(request);
            Response<JsonElement> responseCall = call.execute();
            if (responseCall != null && responseCall.isSuccessful()) {
                LogUtil.d(TAG, responseCall.code() + " " + responseCall.message());
                JsonElement response = responseCall.body();
                if (response != null) {
                    LogUtil.d(TAG, response);
                }
            }
        } catch (Throwable t) {
            LogUtil.e(TAG, t);
        }
    }

    private void saveToFile() {

    }

}
