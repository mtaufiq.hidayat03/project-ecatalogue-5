CREATE FUNCTION nextval(seq_name VARCHAR(100))
  RETURNS BIGINT
  BEGIN
  DECLARE cur_value BIGINT(20);
  SELECT LAST_INSERT_ID(null) INTO cur_value;
  UPDATE sequence_data
  SET sequence_cur_value = LAST_INSERT_ID(
      IF (
        (sequence_cur_value + sequence_increment) > sequence_max_value,
        IF (
          sequence_cycle = TRUE,
          sequence_min_value,
          NULL
        ),
      sequence_cur_value + sequence_increment
      )
  )
  WHERE sequence_name = seq_name;
  SELECT LAST_INSERT_ID() INTO cur_value;
  RETURN cur_value;
END;