package jobs;

import models.jcommon.config.Configuration;
import models.jcommon.util.CommonUtil;
import models.masterdata.Kurs;
import models.masterdata.KursNilai;
import models.prakatalog.Penawaran;
import models.prakatalog.UsulanJadwal;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import play.jobs.On;
import utils.KatalogUtils;
import utils.LogUtil;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/** job di eksekusi setiap jam 00:00 pagi **/
//@On("0 0 0 * * ? *")
@NoTransaction
public class UpdateStatusPenawaranJob extends Job {


    public void doJob() {

        List<Penawaran> allPenawaranActive = Penawaran.findAllPenawaranForScheduler();
        Calendar currentCal = Calendar.getInstance();
        currentCal.setTime(new Date());

        for(Penawaran penawaran : allPenawaranActive){
            Penawaran objPenawaran = Penawaran.findById(penawaran.id);

            Date jadwalSelesai = UsulanJadwal.findLastDateSchedule(penawaran.usulan_id);
            Calendar calJadwalSelesai = getCalendarTanggalSelesai(jadwalSelesai);

            if(currentCal.after(calJadwalSelesai) && !penawaran.status.equals(Penawaran.STATUS_SELESAI)){
                objPenawaran.changeStatus(Penawaran.STATUS_SELESAI);
            }else{

                if(penawaran.tahapan_negosiasi != null){
                    Date negosiasiSelesai = UsulanJadwal.getTanggalAkhir(penawaran.usulan_id,penawaran.tahapan_negosiasi);
                    Calendar calNegosiasiSelesai = getCalendarTanggalSelesai(negosiasiSelesai);

                    if(currentCal.after(calNegosiasiSelesai) && penawaran.status.equals(Penawaran.STATUS_NEGOSIASI)){
                        objPenawaran.changeStatus(Penawaran.STATUS_NEGOSIASI_SELESAI);
                    }else{
                        checkPenawaran(penawaran,currentCal,objPenawaran);
                    }

                }else{
                    checkPenawaran(penawaran,currentCal,objPenawaran);
                }

            }

        }

    }

    private Calendar getCalendarTanggalSelesai(Date tanggal){
            Calendar cal = Calendar.getInstance();
            cal.setTime(tanggal);

            return cal;
    }

    private void checkPenawaran(Penawaran penawaran, Calendar currentCal, Penawaran objPenawaran){
        Date penawaranSelesai = UsulanJadwal.getTanggalAkhir(penawaran.usulan_id,penawaran.tahapan_penawaran);
        Calendar calPenawaranSelesai = getCalendarTanggalSelesai(penawaranSelesai);

        if(currentCal.after(calPenawaranSelesai) && penawaran.status.equals(Penawaran.STATUS_LOLOS_KUALIFIKASI)){
            objPenawaran.changeStatus(Penawaran.STATUS_NEGOSIASI);

        }
    }


}
