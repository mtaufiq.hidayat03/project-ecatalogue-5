package models.katalog;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.Date;

/**
 * Created by dadang on 11/27/17.
 */
@Table(name = "produk_harga_kabupaten")
public class ProdukHargaKabupaten extends BaseKatalogTable {

	@Id
	public Long id;
	public Long produk_id;
	public Long provinsi_id;
	public Long kabupaten_id;
	public Long kurs_id;
	public Double harga;
	public boolean active;
	public boolean approved;
	public Date harga_tanggal;
	public Long komoditas_harga_atribut_id;
	public Date approved_date;
	public Long approved_by;

	public void getGroupTanggal(){
		String query = "select * from produk_harga_kabupaten where produk_id=347 group by (harga_tanggal);";
	}
}
