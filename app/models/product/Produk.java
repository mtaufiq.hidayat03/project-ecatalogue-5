package models.product;

import models.BaseKatalogTable;
import models.elasticsearch.product.ProductPriceElastic;
import models.permohonan.PembaruanStaging;
import models.permohonan.PermohonanPembaruan;
import models.util.Config;
import org.apache.commons.lang.StringUtils;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;
import play.mvc.Router;
import utils.KatalogUtils;
import utils.UrlUtil;

import javax.persistence.Transient;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static models.katalog.Produk.STATUS_DRAFT;

@Table
public class Produk extends BaseKatalogTable {

    @Id
    public Long id;

    @Required
    public Integer komoditas_id;
    @Transient
    public String nama_komoditas;
    @Transient
    public String kelas_harga;
    @Transient
    public String nama_kldi;
    @Transient
    public String nama_usulan;

    @Required
    public Integer produk_kategori_id;
    @Transient
    public String nama_kategori;

    public Integer penyedia_id;
    @Transient
    public String nama_penyedia;

    @Required
    public Integer manufaktur_id;

    @Transient
    public String nama_manufaktur;

    @Required
    public Integer unspsc_id;
    @Required
    public Integer unit_pengukuran_id;
    @Required
    public String no_produk;

    public String no_produk_penyedia;

    @Required
    public String nama_produk;

    public String jenis_produk;
    public String produk_gambar_file_name;
    public String produk_gambar_file_sub_location;
    public String produk_gambar_file_url;

    @Required
    public Integer jumlah_stok;
    @Required
    public Integer jumlah_stok_inden;

    public String setuju_tolak;

    @Required
    public Integer minta_disetujui;
    @Required
    public Integer apakah_dapat_dibeli;
    @Required
    public Integer apakah_ditayangkan;

    public Integer apakah_dapat_diedit;

    @Transient
    public String file_name;
    @Transient
    public String file_sub_location;
    @Transient
    public String file_url;

    @Transient
    public Long penawaran_id;

    @Transient
    public String penawaran_status;

    public String status;

    public static final String _imageFoldersub = "/produk_gambar/";

    /*Transient untuk keperluan syarat produk tayang*/
    @Transient
    public Double harga_pemerintah_produk;
    @Transient
    public Double harga_utama_produk;
    @Transient
    public Integer produk_aktif;
    @Transient
    public Integer produk_apakah_ditayangkan;
    @Transient
    public String produk_setuju_tolak;
    @Transient
    public Date tanggal_sekarang;
    @Transient
    public Date produk_tanggal_berlaku;
    @Transient
    public Integer produk_tanggal_berlaku_status;
    @Transient
    public Long produk_jumlah_stok;
    @Transient
    public String komoditas_nama;
    @Transient
    public Integer komoditas_aktif;
    @Transient
    public Integer komoditas_apakah_stok_unlimited;
    @Transient
    public Long id_penawaran;
    @Transient
    public Long id_kontrak;
    @Transient
    public Date kontrak_tgl_masa_berlaku_mulai;
    @Transient
    public Date kontrak_tgl_masa_berlaku_selesai;
    @Transient
    public Integer kontrak_tgl_masa_berlaku_mulai_status;
    @Transient
    public Integer kontrak_tgl_masa_berlaku_selesai_status;
    @Transient
    public Integer kontrak_aktif;
    @Transient
    public Integer status_stok;
    @Transient
    public String sudah_tayang;
    @Transient
    public Integer kontrak_berlaku;

    public Boolean is_umkm = false;

    public String getDetailUrl() {
        return UrlUtil.builder()
                .setUrl("katalog.produkctr.detail")
                .setParam("id", this.id)
                .setParam("type", ProductPriceElastic.getElasticPriceType(kelas_harga))
                .build();
    }

    public String getEditUrl() {
        return UrlUtil.builder()
                .setUrl("katalog.produkctr.edit")
                .setParam("produk_id", this.id)
                .build();
    }

    public String getEditProductUrl() {
        return UrlUtil.builder()
                .setUrl("katalog.produkctr.editProduk")
                .setParam("produk_id", this.id)
                .build();
    }

    public String getImageUrl(){
        Config conf=Config.getInstance();
        String localPath = "";
        if(StringUtils.isEmpty(this.file_url)){
            localPath = "";
        }else{
            localPath =  Config.getInstance().fileImageGallery + _imageFoldersub + this.file_url;
            if(!KatalogUtils.fileExistInPathApps(localPath)){
                localPath = conf.cdnServerImage100 + _imageFoldersub + this.file_url;
            }
        }

        return localPath;
    }

    public String getDetailCompareUrl(Long pmbid) {

        String type = kelas_harga;
        if (type.equalsIgnoreCase("kabupaten")){
            type = "regency";
        }else if(type.equalsIgnoreCase("provinsi")){
            type = "province";
        }else {
            type = "general";
        }

        return UrlUtil.builder()
                .setUrl("katalog.PermohonanPembaruanCtr.detailProdukCompare")
                .setParam("id", this.id)
                .setParam("type", type)
                .setParam("pmbid", pmbid)
                .build();
    }
    public String getProductUrl(){

        String type = kelas_harga;
        if (type.equalsIgnoreCase("kabupaten")){
            type = "regency";
        }else if(type.equalsIgnoreCase("provinsi")){
            type = "province";
        }else {
            type = "general";
        }

        return UrlUtil.builder()
                .setUrl("katalog.ProdukCtr.detail")
                .setParam("id", this.id)
                .setParam("type", type)
                .build();
    }

    public StringBuilder getMakeButtonGroups(){
        Map<String, Object> ids = new HashMap<>();
        ids.put("id", this.id);
        String urlPerbandinganSpek = Router.reverse("katalog.ProdukCtr.spesifikasi", ids).url;
        String urlPerbandinganHarga = Router.reverse("katalog.ProdukCtr.harga", ids).url;

        StringBuilder htmlBuilder = new StringBuilder("<ul class='dropdown-menu' aria-labelledby='dropdownMenu4'>");
        htmlBuilder.append("<li><a target='_blank' href='").append(urlPerbandinganSpek).append("'>").append(Messages.get("perubahan_spesifikasi.label")).append("</a></li>");
        htmlBuilder.append("<li><a target='_blank' href='").append(urlPerbandinganHarga).append("'>").append(Messages.get("perubahan_harga.label")).append("</a></li>");
        htmlBuilder.append("</ul>");
        return htmlBuilder;
    }

    public static Produk getProdukInfoSyaratTayang(long produk_id){
        String query = "SELECT \n" +
                "  p.id AS id, \n" +
                "  p.nama_produk AS nama_produk, \n" +
                "  p.no_produk AS no_produk, \n" +
                "  ( CASE WHEN p.harga_utama IS NULL THEN 0 ELSE p.harga_utama END ) AS harga_utama_produk, \n" +
                "  ( CASE WHEN p.margin_harga > 0 THEN ROUND( ( p.harga_utama + ( p.harga_utama * p.margin_harga / 100 ) ), 2 )  \n" +
                "   WHEN p.harga_utama IS NULL THEN 0 ELSE p.harga_utama  \n" +
                "   END  \n" +
                "  ) AS harga_pemerintah_produk, \n" +
                "  p.active AS produk_aktif, \n" +
                "  p.apakah_ditayangkan AS produk_apakah_ditayangkan, \n" +
                "  p.setuju_tolak AS produk_setuju_tolak, \n" +
                "  CURDATE( ) AS tanggal_sekarang, \n" +
                "  p.berlaku_sampai AS produk_tanggal_berlaku, \n" +
                "  (CASE WHEN p.berlaku_sampai >= CURDATE( ) OR p.berlaku_sampai IS NULL THEN 1 ELSE 0 END) AS produk_tanggal_berlaku_status, \n" +
                "  p.jumlah_stok AS produk_jumlah_stok, \n" +
                "  k.nama_komoditas AS komoditas_nama, \n" +
                "  k.active AS komoditas_aktif, \n" +
                "  k.apakah_stok_unlimited AS komoditas_apakah_stok_unlimited, \n" +
                "  p.penawaran_id as id_penawaran, \n" +
                "  pk.id AS id_kontrak, \n" +
                "  (CASE WHEN k.apakah_stok_unlimited = 1 OR p.jumlah_stok > 0  THEN 1 ELSE 0 END) AS status_stok, \n" +
                "  (CASE WHEN pk.tgl_masa_berlaku_mulai <= curdate( ) THEN 1 ELSE 0 END) AS kontrak_tgl_masa_berlaku_mulai_status, \n" +
                "  (CASE WHEN pk.tgl_masa_berlaku_selesai >= curdate( ) THEN 1 ELSE 0 END) AS kontrak_tgl_masa_berlaku_selesai_status, \n" +
                "  pk.tgl_masa_berlaku_mulai AS kontrak_tgl_masa_berlaku_mulai, \n" +
                "  pk.tgl_masa_berlaku_selesai AS kontrak_tgl_masa_berlaku_selesai, \n" +
                "  pk.active AS kontrak_aktif, \n" +
                "  pk.apakah_berlaku AS kontrak_berlaku, \n" +
                "  ( CASE WHEN (p.active = 1  \n" +
                "  AND p.apakah_ditayangkan = 1  \n" +
                "  AND k.active = 1  \n" +
                "  AND p.setuju_tolak = 'setuju'  \n" +
                "  AND ( \n" +
                "    ( p.berlaku_sampai >= CURDATE( )  \n" +
                "     AND pk.tgl_masa_berlaku_mulai <= curdate( )  \n" +
                "     AND pk.tgl_masa_berlaku_selesai >= curdate( )  \n" +
                "     AND pk.active = 1 AND pk.apakah_berlaku = 1 \n" +
                "    )  \n" +
                "    OR  \n" +
                "    ( \n" +
                "     p.berlaku_sampai IS NULL  \n" +
                "     AND pk.tgl_masa_berlaku_mulai <= curdate( )  \n" +
                "     AND pk.tgl_masa_berlaku_selesai >= curdate( )  \n" +
                "     AND pk.active = 1 AND pk.apakah_berlaku = 1 \n" +
                "    )  \n" +
                "   )  \n" +
                "  AND ( k.apakah_stok_unlimited = 1 OR ( k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0 ) )) THEN 'Tayang' ELSE 'Tidak Tayang' END ) as sudah_tayang \n" +
                " FROM \n" +
                "  produk p \n" +
                "  LEFT JOIN produk_kategori cat ON cat.id = p.produk_kategori_id \n" +
                "  LEFT JOIN penawaran pw ON pw.id = p.penawaran_id \n" +
                "  LEFT JOIN penyedia_kontrak pk ON pk.id = pw.kontrak_id \n" +
                "  LEFT JOIN penyedia pn ON pn.id = pk.penyedia_id \n" +
                "  LEFT JOIN komoditas k ON p.komoditas_id = k.id \n" +
                "  LEFT JOIN manufaktur m ON m.id = p.manufaktur_id\n" +
                " WHERE p.id = ?";

        return Query.find(query, Produk.class, produk_id).first();
    }

    public List<PembaruanStaging> getProdukStagging(Long pmb_id){
        List<PembaruanStaging> pmbs = PembaruanStaging.getProdukStaggingByPmbIdAndPrdId(pmb_id, id);
        return pmbs;
    }

    public String createButtonEditStaging(Long pmb_id) {
        List<PembaruanStaging> list = getProdukStagging(pmb_id);

        StringBuilder sb = new StringBuilder("<ul class='dropdown-menu' aria-labelledby='dropdownMenu4'>");
        String label = Messages.get("edit.label");
        String info = Messages.get("st_informasi_produk.label");
        String spec = Messages.get("informasi_spesifikasi_produk.label");
        String lamp = Messages.get("lampiran.label");
        String gambar = Messages.get("gambar.label");
        String harga = Messages.get("harga_produk.label");
        String hapus = Messages.get("hapus.label");

        String urlDelete = "";

        for (PembaruanStaging stg : list) {
            String url = "";
            Map<String, Object> ids = new HashMap<>();
            ids.put("id", stg.id);
            ids.put("pembaruan_id", pmb_id);
            ids.put("produk_id", stg.getIfProdukStagging().produk_id);
            if (stg.getIfProdukStagging().info) {
                url = Router.reverse("controllers.katalog.PermohonanPembaruanCtr.editProdukStagingInfo", ids).url;
                sb.append("<li><a href='"+url+"'>"+label+" " +info+" </a></li>");
            } else if (stg.getIfProdukStagging().spec) {
                url = Router.reverse("controllers.katalog.PermohonanPembaruanCtr.editProdukStagingSpec", ids).url;
                sb.append("<li><a href='"+url+"'>"+label+" " +spec+" </a></li>");
            } else if (stg.getIfProdukStagging().lamp) {
                url = Router.reverse("controllers.katalog.PermohonanPembaruanCtr.editProdukStagingLamp", ids).url;
                sb.append("<li><a href='"+url+"'>"+label+" " +lamp+" </a></li>");
            } else if (stg.getIfProdukStagging().gambar) {
                url = Router.reverse("controllers.katalog.PermohonanPembaruanCtr.editProdukStagingGambar", ids).url;
                sb.append("<li><a href='"+url+"'>"+label+" " +gambar+" </a></li>");
            } else if (stg.getIfProdukStagging().harga) {
                url = Router.reverse("controllers.katalog.PermohonanPembaruanCtr.editProdukStagingWilyahJual", ids).url;
                sb.append("<li><a href='"+url+"'>"+label+" " +harga+" </a></li>");
            }

            urlDelete = Router.reverse("controllers.katalog.PermohonanPembaruanCtr.hapusProdukPermohonanPembaruan", ids).url;
        }


        sb.append("<li><a href='"+urlDelete+"'>"+hapus+" </a></li>");
        sb.append("</ul>");
        return sb.toString();
    }

    public String isApprovedPermohonan(List<PembaruanStaging> list, Long produk_id){
        // 0 = menunggu persetujuan
        // 1 = disetujui
        // -1 ditolak
        PembaruanStaging stg = list.stream().filter(f-> f.getIfProdukStagging().produk_id.equals(produk_id)).findFirst().orElse(null);
        if(null != stg){
            if(stg.apakah_disetujui == 0){
               return PermohonanPembaruan.STATUS_MENUNGGU_PERSETUJUAN.replaceAll("_"," ").toUpperCase();
            }else if(stg.apakah_disetujui == 1){
                return PermohonanPembaruan.STATUS_SETUJU.replaceAll("_"," ").toUpperCase();
            }else{
                return PermohonanPembaruan.STATUS_TOLAK.replaceAll("_"," ").toUpperCase();
            }
        }
        return PermohonanPembaruan.STATUS_MENUNGGU_PERSETUJUAN.replaceAll("_"," ").toUpperCase();
    }

    public boolean isDraft() {
        return !StringUtils.isEmpty(status) && status.equalsIgnoreCase(STATUS_DRAFT);
    }

}
