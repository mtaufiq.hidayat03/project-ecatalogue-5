package models.purchasing.search;

import ext.contracts.PaginateAble;
import models.purchasing.Paket;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 12/26/2017
 */
public class PurchasingSearchResult implements PaginateAble {

    public long total = 0;
    public List<Paket> items = new ArrayList<>();
    public final PurchasingSearch contract;

    public PurchasingSearchResult(
            long total,
            List<Paket> items,
            PurchasingSearch paramAble
    ) {
        this.total = total;
        this.items = items;
        this.contract = paramAble;
    }

    public PurchasingSearchResult(PurchasingSearch paramAble) {
        this.contract = paramAble;
    }

    @Override
    public long getTotal() {
        return total;
    }

    @Override
    public long getTotalCurrentPage() {
        return items.size();
    }

    @Override
    public int getMax() {
        return 20;
    }

    @Override
    public int getPage() {
        return contract.getPage();
    }

    @Override
    public List<BasicNameValuePair> getParams() {
        return contract.getParams();
    }

    @Override
    public void setTotal(Long total) {
        this.total = total;
    }

//    @Override
//    public void setTotal(Long totalRevisi) {
//        this.totalRevisi = totalRevisi;
//    }

}
