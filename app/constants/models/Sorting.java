package constants.models;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.i18n.Messages;
import play.mvc.Http;

import java.net.URI;
import java.util.List;

/**
 * @author HanusaCloud on 10/11/2017
 */
public class Sorting {

    private static final String ORDER = "order=";
    public static final String MIN_PRICE = "minPrice";
    public static final String MAX_PRICE = "maxPrice";
    public static final String NAME_ASC = "nameAsc";
    public static final String NAME_DESC = "nameDesc";
    public static final String NO_PRODUCT_LKPP_ASC = "productLkppAsc";
    public static final String NO_PRODUCT_LKPP_DESC = "productLkppDesc";
    public static final String NO_PRODUCT_PROVIDER_ASC = "productProviderAsc";
    public static final String NO_PRODUCT_PROVIDER_DESC = "productProviderDesc";

    public static Sort[] SORTS = new Sort[]{
            new Sort(Messages.get("sort.min.price"), MIN_PRICE),
            new Sort(Messages.get("sort.max.price"), MAX_PRICE),
            new Sort(Messages.get("sort.product.name.asc"), NAME_ASC),
            new Sort(Messages.get("sort.product.name.desc"), NAME_DESC),
            new Sort(Messages.get("sort.product.number.lkpp.asc"), NO_PRODUCT_LKPP_ASC),
            new Sort(Messages.get("sort.product.number.lkpp.desc"), NO_PRODUCT_LKPP_DESC),
            new Sort(Messages.get("sort.product.number.provider.asc"), NO_PRODUCT_PROVIDER_ASC),
            new Sort(Messages.get("sort.product.number.provider.desc"), NO_PRODUCT_PROVIDER_DESC)
    };

    public static class Sort {
        public String name;
        public String value;

        public Sort(String name, String value) {
            this.name = name;
            this.value = value;
        }
        public String getUrl() {
            final String currentUrl = Http.Request.current().url;
            final String order = Http.Request.current().params.get("order");
            if (currentUrl.contains(ORDER)) {
                final String replacement = ORDER + value;
                if (!TextUtils.isEmpty(order)) {
                    return currentUrl.replaceAll("order=[^&]+", replacement);
                } else  {
                    return currentUrl.replace(ORDER, replacement);
                }
            } else {
                final String addQuestion = !currentUrl.contains("?") ? "?" : "&";
                return currentUrl + addQuestion + ORDER + value;
            }
        }

    }

}
