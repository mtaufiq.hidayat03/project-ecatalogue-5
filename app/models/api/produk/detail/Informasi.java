package models.api.produk.detail;

import play.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;


/**
 * Created by dadang on 1/5/18.
 */
public class Informasi {

	public Long unspsc;
	public Long id_kategori_produk_lkpp;
	public Long id_penawaran_lkpp;
	public String nama_produk;
	public String no_produk_penyedia;
	public Long id_manufaktur;
	public Long id_unit_pengukuran_lkpp;
	public String deskripsi_singkat;
	public String deskripsi_lengkap;
	public Double kuantitas_stok;

	public String tanggal_update;

	public Integer produk_aktif;
	public Integer apakah_produk_lokal;


	public String berlaku_sampai;//distringkan karena yyyy-mm-dd

	public String url_produk;
	public String tkdn_produk;
	public String image_50x50;
	public String image_100x100;
	public String image_300x300;
	public String image_800x800;

	private Date parseAsDate(String dateStr){
		try{
			DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime dateTime = LocalDateTime.parse(dateStr, format);
//			Date in = new Date();
//			LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
			Date result = Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
			return result;
		}catch (Exception e){
			Logger.error(Image.class.getName()+"error parse date" +e.getMessage());
		}
		return null;
	}

	public Date getTglUpdateAsDate(){
		return parseAsDate(tanggal_update);
	}

	public Date getTanggalUpdateAsDate(){
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			format.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date date = format.parse ( this.tanggal_update );
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Date getBerlakuSampaiAsDate(){

		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			format.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date date = format.parse ( this.berlaku_sampai );
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
}
