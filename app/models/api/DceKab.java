package models.api;

import models.jcommon.util.CommonUtil;
import models.masterdata.DceKabupaten;
import models.masterdata.Kabupaten;
import play.Logger;
import utils.KatalogUtils;

import java.util.Date;
import java.util.List;

/**
 * Created by reza on 27/08/18
 */
public class DceKab {
	public Long prp_id;
	public Long kbp_id;
	public String kbp_nama;


	public Kabupaten getAsKabupaten(){
		try{
			Kabupaten kabupaten = new Kabupaten();
			kabupaten.id=this.kbp_id;
			kabupaten.provinsi_id=this.prp_id;
			kabupaten.nama_kabupaten=this.kbp_nama;
			kabupaten.active=true;
			return kabupaten;
		}catch (Exception e){
			Logger.error("error parse DceKabupaten to kabupaten"+ e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public DceKabupaten getAsDceKabupaten(){
		try{
			DceKabupaten kabupaten = new DceKabupaten();
			kabupaten.id=this.kbp_id;
			kabupaten.dce_provinsi_id=this.prp_id;
			kabupaten.nama=this.kbp_nama;
			return kabupaten;
		}catch (Exception e){
			Logger.error("error parse DceKabupaten to kabupaten"+ e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

}
