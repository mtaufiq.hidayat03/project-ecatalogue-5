package models.katalog.komoditasmodel.form;

import models.katalog.komoditasmodel.KomoditasProdukAtributTipe;
import org.hibernate.validator.constraints.Length;
import play.data.validation.Required;

import java.io.Serializable;

/**
 * @author HanusaCloud on 12/5/2017
 */
public class AttributeForm implements Serializable {

    public Long id;
    @Required
    @Length(max = 50)
    public String name;
    public Long commodityId;
    public String description;
    public String action = "create";
    public int position = 0;

    public AttributeForm(KomoditasProdukAtributTipe model) {
        this.id = model.id;
        this.name = model.tipe_atribut;
        this.description = model.deskripsi;
    }

    public boolean isCreateMode() {
        return action.equalsIgnoreCase("create");
    }

}
