-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------

-- CREATE TABLE "komoditas" --------------------------------
-- CREATE TABLE "komoditas" ------------------------------------
CREATE TABLE `komoditas` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`komoditas_kategori_id` Int( 11 ) NOT NULL,
	`terkunci` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
	`nama_komoditas` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`kode_komoditas` Char( 3 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`external_url` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`jenis_produk` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`jenis_produk_override` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`perlu_negosiasi_harga` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
	`perlu_ongkir` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
	`perlu_approval_produk` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
	`apakah_iklan` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
	`apakah_paket_produk_duplikat` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
	`apakah_external_url` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
	`apakah_stok_limited` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
	`kelas_harga` VarChar( 10 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`margin_harga` Decimal( 5, 2 ) NOT NULL,
	`posisi_item` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`agr_produk` Int( 5 ) NOT NULL,
	`agr_paket` Int( 5 ) NOT NULL,
	`agr_penyedia_komoditas` Int( 5 ) NOT NULL,
	`created_date` DateTime NOT NULL,
	`created_by` Int( 5 ) NOT NULL,
	`modified_date` DateTime NOT NULL,
	`modified_by` Int( 5 ) NOT NULL,
	`deleted_date` DateTime NOT NULL,
	`deleted_by` Int( 5 ) NOT NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`auditdate` Timestamp NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`active` INT( 1 ) NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "role_basic" -------------------------------
-- CREATE TABLE "role_basic" -----------------------------------
CREATE TABLE `role_basic` ( 
	`id` Int( 5 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`nama_role` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`nama_role_alias` Char( 3 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`active` INT( 1 ) NULL DEFAULT '0',
	`created_date` Timestamp NULL,
	`modified_date` Timestamp NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "role_grup" --------------------------------
-- CREATE TABLE "role_grup" ------------------------------------
CREATE TABLE `role_grup` ( 
	`id` Int( 5 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`nama_grup` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`kategori` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`active` INT( 1 ) NULL DEFAULT '0',
	`created_date` Timestamp NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "role_item" --------------------------------
-- CREATE TABLE "role_item" ------------------------------------
CREATE TABLE `role_item` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`role_grup_id` Int( 5 ) NULL,
	`nama_role` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`deskripsi` VarChar( 150 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`active` INT( 1 ) NULL DEFAULT '0',
	`created_date` Timestamp NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "user" -------------------------------------
-- CREATE TABLE "user" -----------------------------------------
CREATE TABLE `user` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`user_role_grup_id` Int( 11 ) UNSIGNED NULL,
	`nama_lengkap` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`user_name` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`user_password` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`user_email` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`bahasa` Char( 2 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`role_basic_id` Int( 5 ) NULL,
	`override_role_group` INT( 1 ) NULL DEFAULT '0',
	`override_role_komoditas` INT( 1 ) NULL DEFAULT '0',
	`override_role_paket` INT( 1 ) NULL DEFAULT '0',
	`status_penyedia_distributor` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`satker_id` Int( 5 ) NULL,
	`no_telp` VarChar( 12 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`nip` VarChar( 30 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`jabatan` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`pps_id` Int( 5 ) NULL,
	`nomor_sertifikat_pbj` VarChar( 30 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`kldi_id` Int( 5 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`active` INT( 1 ) NULL DEFAULT '0',
	`wilayah_provinsi_id` Int( 2 ) NULL,
	`wilayah_kabupaten_id` Int( 5 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "user_role_grup" ---------------------------
-- CREATE TABLE "user_role_grup" -------------------------------
CREATE TABLE `user_role_grup` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`role_basic_id` Int( 5 ) NULL,
	`role_basic_default` INT( 1 ) NULL DEFAULT '0',
	`nama_grup` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`akses_seluruh_komoditas` INT( 1 ) NULL DEFAULT '0',
	`akses_seluruh_paket` INT( 1 ) NULL DEFAULT '0',
	`batasi_pembelian` INT( 1 ) NULL DEFAULT '0',
	`nilai_batas_pembelian` Int( 11 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`active` INT( 1 ) NULL DEFAULT '0',
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "user_role_item" ---------------------------
-- CREATE TABLE "user_role_item" -------------------------------
CREATE TABLE `user_role_item` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`user_role_grup_id` Int( 11 ) NULL,
	`role_item_id` Int( 11 ) NULL,
	`active` INT( 1 ) NULL DEFAULT '0',
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "user_role_item_override" ------------------
-- CREATE TABLE "user_role_item_override" ----------------------
CREATE TABLE `user_role_item_override` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`user_id` Int( 11 ) NULL,
	`role_item_id` Int( 11 ) NULL,
	`active` INT( 1 ) NULL DEFAULT '0',
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "user_role_komoditas" ----------------------
-- CREATE TABLE "user_role_komoditas" --------------------------
CREATE TABLE `user_role_komoditas` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`user_role_grup_id` Int( 11 ) NULL,
	`komoditas_id` Int( 11 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`active` INT( 1 ) NULL DEFAULT '0',
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "user_role_komoditas_override" -------------
-- CREATE TABLE "user_role_komoditas_override" -----------------
CREATE TABLE `user_role_komoditas_override` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`user_id` Int( 11 ) NULL,
	`komoditas_id` Int( 11 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`active` INT( 1 ) NULL DEFAULT '0',
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "user_role_paket" --------------------------
-- CREATE TABLE "user_role_paket" ------------------------------
CREATE TABLE `user_role_paket` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`user_role_grup_id` Int( 11 ) NULL,
	`paket_id` Int( 11 ) NULL,
	`active` INT( 1 ) NULL DEFAULT '0',
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "user_role_paket_override" -----------------
-- CREATE TABLE "user_role_paket_override" ---------------------
CREATE TABLE `user_role_paket_override` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`user_id` Int( 11 ) NULL,
	`paket_id` Int( 11 ) NULL,
	`active` INT( 1 ) NULL DEFAULT '0',
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "komoditas_ikon" ---------------------------
-- CREATE TABLE "komoditas_ikon" -------------------------------
CREATE TABLE `komoditas_ikon` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`komoditas_id` Int( 11 ) NULL,
	`file_name` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`original_file_name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`file_size` BigInt( 20 ) NULL,
	`file_sub_location` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`active` INT( 1 ) NULL DEFAULT '0',
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "komoditas_kategori" -----------------------
-- CREATE TABLE "komoditas_kategori" ---------------------------
CREATE TABLE `komoditas_kategori` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`nama_kategori` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`posisi_item` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`active` INT( 1 ) NULL,
	`created_by` Int( 11 ) NULL,
	`created_date` Timestamp NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "komoditas_kategori_ikon" ------------------
-- CREATE TABLE "komoditas_kategori_ikon" ----------------------
CREATE TABLE `komoditas_kategori_ikon` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`komoditas_kategori_id` Int( 11 ) NULL,
	`file_name` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`original_file_name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`file_size` BigInt( 20 ) NULL,
	`file_sub_location` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`active` INT( 1 ) NULL DEFAULT '0',
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "komoditas_manufaktur" ---------------------
-- CREATE TABLE "komoditas_manufaktur" -------------------------
CREATE TABLE `komoditas_manufaktur` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`manufaktur_id` Int( 11 ) NULL,
	`komoditas_id` Int( 11 ) NULL,
	`active` INT( 1 ) NULL DEFAULT '0',
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "komoditas_produk_atribut" -----------------
-- CREATE TABLE "komoditas_produk_atribut" ---------------------
CREATE TABLE `komoditas_produk_atribut` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`komoditas_id` Int( 11 ) NULL,
	`tipe_atribut` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`posisi_item` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`active` INT( 1 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "paket" ------------------------------------
-- CREATE TABLE "paket" ----------------------------------------
CREATE TABLE `paket` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`komoditas_id` Int( 11 ) NULL,
	`penyedia_id` Int( 11 ) NULL,
	`penyedia_distributor_id` Int( 11 ) NULL,
	`rup_id` Int( 11 ) NULL,
	`no_paket` VarChar( 30 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`provinsi_id` Int( 2 ) NULL,
	`kabupaten_id` Int( 3 ) NULL,
	`nama_paket` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`sumber_dana` JSON NULL,
	`panitia_user_id` Int( 11 ) NULL,
	`ppk_user_id` Int( 11 ) NULL,
	`status_paket` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`alasan_batal` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`tanggal_batal` Date NULL,
	`dibatalkan_oleh` Int( 11 ) NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`kurs_id_from` Int( 5 ) NULL,
	`kurs_nilai` Decimal( 20, 2 ) NULL,
	`kurs_tanggal` Date NULL,
	`tanggal_selesai` Date NULL,
	`diselesaikan_oleh` Int( 11 ) NULL,
	`sudah_dirating` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`rating_nilai_avg` Decimal( 5, 2 ) NULL,
	`agr_paket_produk` Int( 11 ) NULL,
	`agr_paket_kontrak` Int( 11 ) NULL,
	`agr_paket_pembayaran` Int( 11 ) NULL,
	`agr_paket_pembayaran_total_invoice` Decimal( 20, 2 ) NULL,
	`agr_konversi_harga_total` Decimal( 20, 2 ) NULL,
	`agr_paket_produk_sudah_diterima_semua` Int( 5 ) NULL,
	`satker_id` Int( 11 ) NULL,
	`kldi_id` Int( 11 ) NULL,
	`tahun_anggaran` Int( 4 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`active` INT( 1 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "paket_anggota_ulp" ------------------------
-- CREATE TABLE "paket_anggota_ulp" ----------------------------
CREATE TABLE `paket_anggota_ulp` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`paket_id` Int( 11 ) NULL,
	`anggota_user_id` Int( 11 ) NULL,
	`anggota_nip` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`active` INT( 1 ) NULL DEFAULT '1',
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "penyedia" ---------------------------------
-- CREATE TABLE "penyedia" -------------------------------------
CREATE TABLE `penyedia` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`user_id` Int( 11 ) NULL,
	`nama_penyedia` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`alamat` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`website` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`email` VarChar( 40 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`no_telp` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`no_fax` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`no_hp` VarChar( 12 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`npwp` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`pkp` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`kode_pos` Char( 5 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`produk_api_url` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`default_kurs_id` Int( 3 ) NULL,
	`agr_penyedia_distributor` Int( 5 ) NULL,
	`agr_penyedia_kontrak` Int( 5 ) NULL,
	`active` INT( 1 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "penyedia_distributor" ---------------------
-- CREATE TABLE "penyedia_distributor" -------------------------
CREATE TABLE `penyedia_distributor` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`penyedia_id` Int( 11 ) NULL,
	`user_id` Int( 11 ) NULL,
	`nama_distributor` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`alamat` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`website` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`email` VarChar( 40 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`no_telp` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`no_fax` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`no_hp` VarChar( 12 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`npwp` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`pkp` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`kode_pos` Char( 5 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`rating_nilai_avg` Decimal( 5, 2 ) NULL,
	`active` INT( 1 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "penyedia_distributor_representatif" -------
-- CREATE TABLE "penyedia_distributor_representatif" -----------
CREATE TABLE `penyedia_distributor_representatif` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`penyedia_distributor_id` Int( 11 ) NULL,
	`nama` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`email` VarChar( 40 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`no_telp` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`active` INT( 1 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "penyedia_komoditas" -----------------------
-- CREATE TABLE "penyedia_komoditas" ---------------------------
CREATE TABLE `penyedia_komoditas` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`penyedia_id` Int( 11 ) NULL,
	`komoditas_id` Int( 11 ) NULL,
	`active` INT( 1 ) NULL DEFAULT '0',
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "penyedia_komoditas_kontrak" ---------------
-- CREATE TABLE "penyedia_komoditas_kontrak" -------------------
CREATE TABLE `penyedia_komoditas_kontrak` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`penyedia_komoditas_id` Int( 11 ) NULL,
	`no_kontrak` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`file_name` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`original_file_name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`file_size` BigInt( 20 ) NULL,
	`file_sub_location` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`file_url` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`posisi_file` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`apakah_berlaku` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`tgl_masa_berlaku_mulai` Date NULL,
	`tgl_masa_berlaku_selesai` Date NULL,
	`item_param` JSON NULL,
	`active` INT( 1 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "penyedia_representatif" -------------------
-- CREATE TABLE "penyedia_representatif" -----------------------
CREATE TABLE `penyedia_representatif` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`penyedia_id` Int( 11 ) NULL,
	`nama` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`no_telp` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`email` VarChar( 30 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`active` INT( 1 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "produk" -----------------------------------
-- CREATE TABLE "produk" ---------------------------------------
CREATE TABLE `produk` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`komoditas_id` Int( 11 ) NULL,
	`produk_kategori_id` Int( 11 ) NULL,
	`penyedia_id` Int( 11 ) NULL,
	`manufaktur_id` Int( 11 ) NULL,
	`unspscc_id` Int( 11 ) NULL,
	`unit_pengukuran_id` Int( 3 ) NULL,
	`no_produk` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`no_produk_penyedia` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`nama_produk` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`jenis_produk` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '1 : lokal, 2: impor',
	`url_produk` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`jumlah_stok` Decimal( 5, 2 ) NULL,
	`jumlah_stok_inden` Int( 5 ) NULL,
	`status` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`minta_disetujui_oleh` Int( 11 ) NULL,
	`minta_disetujui_tanggal` DateTime NULL,
	`minta_disetujui_alasan` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`setuju_tolak_tanggal` DateTime NULL,
	`setuju_tolak_oleh` Int( 11 ) NULL,
	`setuju_tolak_alasan` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`harga_utama` Decimal( 15, 2 ) NULL,
	`harga_ongkir` JSON NULL,
	`harga_tanggal` Date NULL,
	`harga_kurs_id` Int( 5 ) NULL,
	`margin_harga` Decimal( 15, 2 ) NULL,
	`agr_total_gambar` Int( 2 ) NULL,
	`agr_total_lampiran` Int( 3 ) NULL,
	`agr_total_wilayah_jual` Int( 3 ) NULL,
	`apakah_dapat_dibeli` INT( 1 ) NULL DEFAULT '0',
	`berlaku_sampai` Date NULL,
	`spesifikasi` JSON NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "produk_atribut_value" ---------------------
-- CREATE TABLE "produk_atribut_value" -------------------------
CREATE TABLE `produk_atribut_value` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`produk_id` Int( 11 ) NULL,
	`produk_kategori_atribut_id` Int( 11 ) NULL,
	`atribut_value` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`active` INT( 1 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "produk_gambar" ----------------------------
-- CREATE TABLE "produk_gambar" --------------------------------
CREATE TABLE `produk_gambar` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`produk_id` Int( 11 ) NULL,
	`file_name` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`original_file_name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`file_size` BigInt( 20 ) NULL,
	`file_sub_location` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`file_url` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`thumb_url` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`thumb_width` Int( 4 ) NULL,
	`thumb_height` Int( 4 ) NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`posisi_file` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`item_param` JSON NULL,
	`processed` Char( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`has_100` INT( 1 ) NULL,
	`has_1024` INT( 1 ) NULL,
	`active` INT( 2 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "produk_harga" -----------------------------
-- CREATE TABLE "produk_harga" ---------------------------------
CREATE TABLE `produk_harga` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`produk_id` Int( 11 ) NULL,
	`kurs_id` Int( 5 ) NULL,
	`harga` JSON NULL,
	`ongkir` JSON NULL,
	`tanggal_dibuat` Timestamp NULL,
	`apakah_disetujui` INT( 1 ) NULL,
	`tanggal_disetujui` Timestamp NULL,
	`tanggal_ditolak` Timestamp NULL,
	`alasan_ditolak` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`setuju_tolak_oleh` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "produk_harga_lokal" -----------------------
-- CREATE TABLE "produk_harga_lokal" ---------------------------
CREATE TABLE `produk_harga_lokal` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`produk_id` Int( 11 ) NULL,
	`provinsi_id` Int( 2 ) NULL,
	`kabupaten_id` Int( 3 ) NULL,
	`kurs_id` Int( 5 ) NULL,
	`harga` JSON NULL,
	`ongkir` JSON NULL,
	`tanggal_dibuat` Timestamp NULL,
	`apakah_disetujui` INT( 1 ) NULL,
	`tanggal_disetujui` Timestamp NULL,
	`tanggal_ditolak` Timestamp NULL,
	`alasan_ditolak` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`setuju_tolak_oleh` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "produk_kategori" --------------------------
-- CREATE TABLE "produk_kategori" ------------------------------
CREATE TABLE `produk_kategori` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`komoditas_id` Int( 11 ) NULL,
	`parent_id` Int( 11 ) NULL,
	`posisi_item` Int( 3 ) NULL,
	`nama_kategori` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`active` INT( 1 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "produk_kategori_atribut" ------------------
-- CREATE TABLE "produk_kategori_atribut" ----------------------
CREATE TABLE `produk_kategori_atribut` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`komoditas_id` Int( 11 ) NULL,
	`komoditas_produk_atribut_tipe_id` Int( 11 ) NULL,
	`komoditas_produk_atribut_tipe_label` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`produk_kategori_id` Int( 11 ) NULL,
	`parent_id` Int( 11 ) NULL,
	`posisi_item` Int( 3 ) NULL,
	`label_atribut` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`tipe_input` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`wajib_diisi` INT( 1 ) NULL,
	`active` INT( 1 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "produk_lampiran" --------------------------
-- CREATE TABLE "produk_lampiran" ------------------------------
CREATE TABLE `produk_lampiran` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`produk_id` Int( 11 ) NULL,
	`file_name` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`original_file_name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`file_size` BigInt( 20 ) NULL,
	`file_sub_location` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`file_url` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`posisi_file` Int( 3 ) NULL,
	`item_param` JSON NULL,
	`active` INT( 1 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "produk_riwayat" ---------------------------
-- CREATE TABLE "produk_riwayat" -------------------------------
CREATE TABLE `produk_riwayat` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`produk_id` Int( 11 ) NULL,
	`aksi` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`data` JSON NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "produk_tunggu_setuju" ---------------------
-- CREATE TABLE "produk_tunggu_setuju" -------------------------
CREATE TABLE `produk_tunggu_setuju` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`produk_id` Int( 11 ) NULL,
	`area` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`deskripsi_param` JSON NULL,
	`jenis_aksi` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`active` INT( 1 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "produk_wilayah_jual_kabupaten" ------------
-- CREATE TABLE "produk_wilayah_jual_kabupaten" ----------------
CREATE TABLE `produk_wilayah_jual_kabupaten` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`produk_id` Int( 11 ) NULL,
	`kabupaten_id` Int( 2 ) NULL,
	`harga_utama` Decimal( 20, 2 ) NULL,
	`harga_ongkir` Decimal( 20, 2 ) NULL,
	`harga_tanggal` Date NULL,
	`harga_kurs_id` Int( 3 ) NULL,
	`margin_harga` Decimal( 20, 2 ) NULL,
	`active` INT( 1 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "produk_wilayah_jual_provinsi" -------------
-- CREATE TABLE "produk_wilayah_jual_provinsi" -----------------
CREATE TABLE `produk_wilayah_jual_provinsi` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`produk_id` Int( 11 ) NULL,
	`provinsi_id` Int( 2 ) NULL,
	`harga_utama` Decimal( 20, 2 ) NULL,
	`harga_ongkir` Decimal( 20, 2 ) NULL,
	`harga_tanggal` Date NULL,
	`harga_kurs_id` Int( 3 ) NULL,
	`margin_harga` Decimal( 20, 2 ) NULL,
	`active` INT( 1 ) NULL,
	`created_date` Timestamp NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "app_configuration" ------------------------
-- CREATE TABLE "app_configuration" ----------------------------
CREATE TABLE `app_configuration` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`config_for` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`key` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`value` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`last_modified_by` Int( 11 ) NULL,
	`last_modified_date` Date NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 10;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "app_param" --------------------------------
-- CREATE TABLE "app_param" ------------------------------------
CREATE TABLE `app_param` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`param_type` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`param_key` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`param_text` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`param_date` Timestamp NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 19;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "baper_host" -------------------------------
-- CREATE TABLE "baper_host" -----------------------------------
CREATE TABLE `baper_host` ( 
	`penyedia_id` Int( 11 ) NOT NULL,
	`host_url` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`host_name` VarChar( 45 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`last_pull` Date NULL,
	`path_all_produk` VarChar( 45 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`path_updated_produk` VarChar( 45 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`path_produk` VarChar( 45 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`secret_key` VarChar( 45 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`kategori` VarChar( 45 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`aktif` Int( 1 ) NOT NULL DEFAULT '1',
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `penyedia_id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "baper_pull_host" --------------------------
-- CREATE TABLE "baper_pull_host" ------------------------------
CREATE TABLE `baper_pull_host` ( 
	`pull_id` Char( 9 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`penyedia_id` Int( 11 ) NOT NULL,
	`status` VarChar( 45 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`produk_update` Int( 11 ) NOT NULL DEFAULT '0',
	`produk_count` Int( 11 ) NOT NULL DEFAULT '0',
	`produk_insert` Int( 11 ) NOT NULL DEFAULT '0',
	`produk_no_change` Int( 11 ) NULL,
	`start_time` DateTime NULL,
	`duration` VarChar( 45 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `pull_id`, `penyedia_id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "dokumen" ----------------------------------
-- CREATE TABLE "dokumen" --------------------------------------
CREATE TABLE `dokumen` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`dokumen_grup_id` Int( 11 ) NULL,
	`nama_dokumen` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`mandatory` Enum( '0', '1' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`active` Int( 2 ) NULL DEFAULT '1',
	`created_date` DateTime NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` DateTime NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` DateTime NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 51;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "dokumen_grup" -----------------------------
-- CREATE TABLE "dokumen_grup" ---------------------------------
CREATE TABLE `dokumen_grup` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`nama_dokumen_grup` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`parent` Int( 11 ) NULL,
	`active` Enum( '0', '1' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
	`created_date` DateTime NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` DateTime NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` DateTime NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 18;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "faq" --------------------------------------
-- CREATE TABLE "faq" ------------------------------------------
CREATE TABLE `faq` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`konten_kategori_id` Int( 11 ) NOT NULL,
	`judul_konten` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`slug` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`isi_konten` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`posisi_konten` Int( 11 ) NOT NULL DEFAULT '1',
	`status` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'published',
	`publish_date_from` Timestamp NULL,
	`publish_date_to` Timestamp NULL,
	`active` Int( 2 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 4;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "feedback" ---------------------------------
-- CREATE TABLE "feedback" -------------------------------------
CREATE TABLE `feedback` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`item_id` Int( 11 ) NOT NULL,
	`item_type` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`reporter_id` Int( 11 ) NULL,
	`reporter_nik` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`message` Text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`rating` Int( 11 ) NOT NULL,
	`created_date` DateTime NOT NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 19;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "instansi" ---------------------------------
-- CREATE TABLE "instansi" -------------------------------------
CREATE TABLE `instansi` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`instansi_jenis_id` Int( 11 ) NULL,
	`nama` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`provinsi_id` Int( 11 ) NULL,
	`active` Int( 2 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`kldi_id` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`prp_id` Int( 11 ) NOT NULL,
	`kbp_id` Int( 11 ) NOT NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 455539;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "instansi_jenis" ---------------------------
-- CREATE TABLE "instansi_jenis" -------------------------------
CREATE TABLE `instansi_jenis` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`nama` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`butuh_provinsi` Int( 2 ) NOT NULL DEFAULT '0',
	`posisi_item` Int( 11 ) NOT NULL DEFAULT '1',
	`active` Int( 2 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 24;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "item_counter" -----------------------------
-- CREATE TABLE "item_counter" ---------------------------------
CREATE TABLE `item_counter` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`item_type` VarChar( 30 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`item_action` VarChar( 30 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`item_id` Int( 11 ) NOT NULL,
	`item_counter` Int( 20 ) NOT NULL DEFAULT '1',
	`active` Int( 2 ) NOT NULL DEFAULT '1',
	`since_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `uq_type_action_id` UNIQUE( `item_type`, `item_action`, `item_id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 43393;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "kabupaten" --------------------------------
-- CREATE TABLE "kabupaten" ------------------------------------
CREATE TABLE `kabupaten` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`provinsi_id` Int( 11 ) NOT NULL,
	`nama_kabupaten` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`active` Int( 2 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`kldi_id` VarChar( 6 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 569;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "katalog_lokal_admin" ----------------------
-- CREATE TABLE "katalog_lokal_admin" --------------------------
CREATE TABLE `katalog_lokal_admin` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`provinsi_id` Int( 11 ) NOT NULL,
	`kabupaten_id` Int( 11 ) NULL,
	`user_id` Int( 11 ) NOT NULL,
	`created_by` Int( 11 ) NULL,
	`created_date` DateTime NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 51;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "katalog_lokal_penyedia" -------------------
-- CREATE TABLE "katalog_lokal_penyedia" -----------------------
CREATE TABLE `katalog_lokal_penyedia` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`provinsi_id` Int( 11 ) NOT NULL,
	`kabupaten_id` Int( 11 ) NULL,
	`penyedia_id` Int( 11 ) NOT NULL,
	`created_by` Int( 11 ) NULL,
	`created_date` DateTime NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 19;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "katalog_sektoral_admin" -------------------
-- CREATE TABLE "katalog_sektoral_admin" -----------------------
CREATE TABLE `katalog_sektoral_admin` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`satker_id` Int( 11 ) NOT NULL,
	`user_id` Int( 11 ) NOT NULL,
	`created_by` Int( 11 ) NULL,
	`created_date` DateTime NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "katalog_sektoral_penyedia" ----------------
-- CREATE TABLE "katalog_sektoral_penyedia" --------------------
CREATE TABLE `katalog_sektoral_penyedia` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`satker_id` Int( 11 ) NOT NULL,
	`penyedia_id` Int( 11 ) NOT NULL,
	`created_by` Int( 11 ) NULL,
	`created_date` DateTime NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "kldi" -------------------------------------
-- CREATE TABLE "kldi" -----------------------------------------
CREATE TABLE `kldi` ( 
	`id` VarChar( 6 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`prp_id` Int( 11 ) NULL,
	`kbp_id` Int( 11 ) NULL,
	`nama` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`jenis` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`website` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`is2014` INT( 1 ) NOT NULL DEFAULT '0',
	`is2015` INT( 1 ) NOT NULL DEFAULT '0',
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	CONSTRAINT `id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "kldi_satker" ------------------------------
-- CREATE TABLE "kldi_satker" ----------------------------------
CREATE TABLE `kldi_satker` ( 
	`id` Int( 11 ) NOT NULL,
	`nama` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`alamat` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`fax` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`kodepos` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`telepon` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`idSatker` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`idSatkerInduk` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`idKldi` VarChar( 6 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`aktif` INT( 1 ) NULL,
	`isDeleted` INT( 1 ) NULL,
	`createdBy` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`aktifDari` DateTime NULL,
	`aktifSampai` DateTime NULL,
	`createdOn` DateTime NULL,
	`auditupdate` DateTime NULL,
	`statusPerubahan` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	CONSTRAINT `id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "kurs" -------------------------------------
-- CREATE TABLE "kurs" -----------------------------------------
CREATE TABLE `kurs` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`nama_kurs` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`active` Int( 2 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 12;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "kurs_nilai" -------------------------------
-- CREATE TABLE "kurs_nilai" -----------------------------------
CREATE TABLE `kurs_nilai` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`kurs_sumber_id` Int( 11 ) NOT NULL,
	`tanggal_kurs` Date NOT NULL,
	`kurs_id_from` Int( 11 ) NOT NULL,
	`nilai_beli` Decimal( 20, 2 ) NOT NULL DEFAULT '0.00',
	`nilai_jual` Decimal( 20, 2 ) NOT NULL DEFAULT '0.00',
	`nilai_tengah` Decimal( 20, 2 ) NOT NULL DEFAULT '0.00',
	`active` Int( 2 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 6;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "kurs_sumber" ------------------------------
-- CREATE TABLE "kurs_sumber" ----------------------------------
CREATE TABLE `kurs_sumber` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`sumber` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`url` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`deskripsi` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`active` Int( 2 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 4;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "manufaktur" -------------------------------
-- CREATE TABLE "manufaktur" -----------------------------------
CREATE TABLE `manufaktur` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`nama_manufaktur` VarChar( 300 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`active` Int( 2 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 12243;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "metode" -----------------------------------
-- CREATE TABLE "metode" ---------------------------------------
CREATE TABLE `metode` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`nama_metode` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`active` Enum( '0', '1' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
	`created_date` DateTime NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` DateTime NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` DateTime NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 4;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "notifikasi" -------------------------------
-- CREATE TABLE "notifikasi" -----------------------------------
CREATE TABLE `notifikasi` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`status` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'published',
	`publish_date_from` Timestamp NULL,
	`publish_date_to` Timestamp NULL,
	`param` VarChar( 1000 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`active` Int( 2 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 2;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "paket_download" ---------------------------
-- CREATE TABLE "paket_download" -------------------------------
CREATE TABLE `paket_download` ( 
	`id` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`kategori` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`sub_kategori` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`paket_id` Int( 11 ) NOT NULL,
	`http_user_agent` VarChar( 300 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`remote_addr` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`description` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "paket_kontrak" ----------------------------
-- CREATE TABLE "paket_kontrak" --------------------------------
CREATE TABLE `paket_kontrak` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`paket_id` Int( 11 ) NOT NULL,
	`no_kontrak` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`nilai_kontrak` Decimal( 20, 2 ) NOT NULL DEFAULT '0.00',
	`tanggal_kontrak` Timestamp NULL,
	`file_name` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`original_file_name` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`file_size` BigInt( 20 ) NOT NULL DEFAULT '0',
	`file_sub_location` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`file_url` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`posisi_file` Int( 11 ) NOT NULL DEFAULT '1',
	`deskripsi` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`active` INT( 1 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 4487;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "paket_pembayaran" -------------------------
-- CREATE TABLE "paket_pembayaran" -----------------------------
CREATE TABLE `paket_pembayaran` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`paket_id` Int( 11 ) NOT NULL,
	`no_invoice` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`total_invoice` Decimal( 20, 2 ) NOT NULL DEFAULT '0.00',
	`tanggal_invoice` Timestamp NULL,
	`tanggal_pembayaran` Timestamp NULL,
	`tanggal_penerimaan_produk` Timestamp NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`active` INT( 1 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1648;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "paket_penerimaan" -------------------------
-- CREATE TABLE "paket_penerimaan" -----------------------------
CREATE TABLE `paket_penerimaan` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`paket_id` Int( 11 ) NOT NULL,
	`no_dokumen` VarChar( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`tanggal_dokumen` Timestamp NULL,
	`tanggal_terima` Timestamp NULL,
	`deskripsi` Text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`paket_pengiriman_no_dokumen_sistem` VarChar( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`sudah_diterima_semua` Int( 11 ) NOT NULL DEFAULT '0',
	`agr_paket_penerimaan_produk` Int( 11 ) NOT NULL DEFAULT '0',
	`agr_paket_penerimaan_lampiran` Int( 11 ) NOT NULL DEFAULT '0',
	`active` INT( 1 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 2805;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "paket_penerimaan_produk" ------------------
-- CREATE TABLE "paket_penerimaan_produk" ----------------------
CREATE TABLE `paket_penerimaan_produk` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`paket_penerimaan_id` Int( 11 ) NOT NULL,
	`paket_id` Int( 11 ) NOT NULL,
	`paket_produk_id` Int( 11 ) NOT NULL,
	`produk_id` Int( 11 ) NOT NULL,
	`kuantitas` Decimal( 20, 5 ) NOT NULL DEFAULT '0.00000',
	`deskripsi` VarChar( 1000 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`active` INT( 1 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 594;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "paket_pengiriman" -------------------------
-- CREATE TABLE "paket_pengiriman" -----------------------------
CREATE TABLE `paket_pengiriman` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`paket_id` Int( 11 ) NOT NULL,
	`no_dokumen_sistem` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`no_dokumen` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`tanggal_dokumen` Timestamp NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`agr_paket_pengiriman_produk` Int( 11 ) NOT NULL DEFAULT '0',
	`agr_paket_pengiriman_lampiran` Int( 11 ) NOT NULL DEFAULT '0',
	`active` INT( 1 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 28980;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "paket_pengiriman_lampiran" ----------------
-- CREATE TABLE "paket_pengiriman_lampiran" --------------------
CREATE TABLE `paket_pengiriman_lampiran` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`paket_pengiriman_id` Int( 11 ) NOT NULL,
	`paket_id` Int( 11 ) NOT NULL,
	`file_name` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`original_file_name` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`file_size` BigInt( 20 ) NOT NULL DEFAULT '0',
	`file_sub_location` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`file_url` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`deskripsi` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`posisi_file` Int( 11 ) NOT NULL DEFAULT '1',
	`item_param` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`active` INT( 1 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 416;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "paket_pengiriman_produk" ------------------
-- CREATE TABLE "paket_pengiriman_produk" ----------------------
CREATE TABLE `paket_pengiriman_produk` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`paket_pengiriman_id` Int( 11 ) NOT NULL,
	`paket_id` Int( 11 ) NOT NULL,
	`paket_produk_id` Int( 11 ) NOT NULL,
	`produk_id` Int( 11 ) NOT NULL,
	`kuantitas` Decimal( 20, 5 ) NOT NULL DEFAULT '0.00000',
	`deskripsi` VarChar( 1000 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`active` INT( 1 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 53242;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "paket_pengiriman_status" ------------------
-- CREATE TABLE "paket_pengiriman_status" ----------------------
CREATE TABLE `paket_pengiriman_status` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`paket_pengiriman_id` Int( 11 ) NOT NULL,
	`paket_id` Int( 11 ) NOT NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`active` INT( 1 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1615;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "paket_produk" -----------------------------
-- CREATE TABLE "paket_produk" ---------------------------------
CREATE TABLE `paket_produk` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`paket_id` Int( 11 ) NOT NULL,
	`produk_id` Int( 11 ) NOT NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`kuantitas` Decimal( 20, 5 ) NOT NULL DEFAULT '0.00000',
	`harga_satuan` Decimal( 20, 2 ) NOT NULL DEFAULT '0.00',
	`harga_ongkir` Decimal( 20, 2 ) NOT NULL DEFAULT '0.00',
	`harga_total` Decimal( 20, 2 ) NOT NULL DEFAULT '0.00',
	`konversi_harga_satuan` Decimal( 20, 2 ) NOT NULL DEFAULT '0.00',
	`konversi_harga_ongkir` Decimal( 20, 2 ) NOT NULL DEFAULT '0.00',
	`konversi_harga_total` Decimal( 20, 2 ) NOT NULL DEFAULT '0.00',
	`catatan` VarChar( 1000 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`sudah_diterima_semua` INT( 1 ) NOT NULL DEFAULT '0',
	`agr_kuantitas_paket_penerimaan_produk` Int( 5 ) NOT NULL DEFAULT '0',
	`active` INT( 1 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `idx_pp_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 926824;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "paket_produk_nego_detail" -----------------
-- CREATE TABLE "paket_produk_nego_detail" ---------------------
CREATE TABLE `paket_produk_nego_detail` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`nego_header_id` Int( 11 ) NOT NULL,
	`paket_produk_id` Int( 11 ) NOT NULL,
	`produk_id` Int( 11 ) NOT NULL,
	`kuantitas` Decimal( 20, 5 ) NOT NULL DEFAULT '0.00000',
	`harga_satuan` Decimal( 20, 2 ) NOT NULL DEFAULT '0.00',
	`harga_ongkir` Decimal( 20, 2 ) NOT NULL DEFAULT '0.00',
	`harga_total` Decimal( 20, 2 ) NOT NULL DEFAULT '0.00',
	`konversi_harga_satuan` Decimal( 20, 2 ) NOT NULL DEFAULT '0.00',
	`konversi_harga_ongkir` Decimal( 20, 2 ) NOT NULL DEFAULT '0.00',
	`konversi_harga_total` Decimal( 20, 2 ) NOT NULL DEFAULT '0.00',
	`catatan` VarChar( 1000 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`active` INT( 1 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1967719;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "paket_produk_nego_header" -----------------
-- CREATE TABLE "paket_produk_nego_header" ---------------------
CREATE TABLE `paket_produk_nego_header` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`paket_id` Int( 11 ) NOT NULL,
	`revisi` Int( 11 ) NOT NULL DEFAULT '0',
	`kurs_id_from` Int( 11 ) NULL,
	`kurs_nilai` Decimal( 20, 2 ) NULL,
	`kurs_tanggal` Timestamp NULL,
	`panitia_setuju` Int( 11 ) NOT NULL DEFAULT '0',
	`panitia_setuju_user_id` Int( 11 ) NULL,
	`panitia_setuju_tanggal` Timestamp NULL,
	`penyedia_setuju` Int( 11 ) NOT NULL DEFAULT '0',
	`penyedia_setuju_user_id` Int( 11 ) NULL,
	`penyedia_setuju_tanggal` Timestamp NULL,
	`active` INT( 1 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `uq_paket_id_revisi` UNIQUE( `paket_id`, `revisi` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 386518;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "paket_riwayat" ----------------------------
-- CREATE TABLE "paket_riwayat" --------------------------------
CREATE TABLE `paket_riwayat` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`paket_id` Int( 11 ) NOT NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`status_paket` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`active` INT( 1 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `idx_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1916105;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "paket_status" -----------------------------
-- CREATE TABLE "paket_status" ---------------------------------
CREATE TABLE `paket_status` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`paket_id` Int( 11 ) NOT NULL,
	`nego_header_id` Int( 11 ) NULL,
	`status_negosiasi` VarChar( 30 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`status_paket` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`from_status` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`to_status` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`di_tolak` Int( 2 ) NOT NULL DEFAULT '0',
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`active` INT( 1 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `uq_paket_id` UNIQUE( `paket_id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 229255;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "penawaran" --------------------------------
-- CREATE TABLE "penawaran" ------------------------------------
CREATE TABLE `penawaran` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`usulan_id` Int( 11 ) NULL,
	`komoditas_id` Int( 11 ) NULL,
	`usulan_komoditas_id` Int( 11 ) NULL DEFAULT '0',
	`user_id` Int( 11 ) NULL,
	`metode_id` Int( 11 ) NULL,
	`penawaran_jenis_id` Int( 11 ) NULL,
	`status` VarChar( 20 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'sedang_diproses',
	`no_agenda` VarChar( 25 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`nama_penawaran` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`pic_nama` VarChar( 200 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`pic_email` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`pic_no_telp` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`pic_alamat` VarChar( 1000 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`active` INT( 1 ) NULL DEFAULT '1',
	`created_date` DateTime NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` DateTime NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` DateTime NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "penawaran_dokumen" ------------------------
-- CREATE TABLE "penawaran_dokumen" ----------------------------
CREATE TABLE `penawaran_dokumen` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`penawaran_id` Int( 11 ) NOT NULL,
	`dokumen_id` Int( 11 ) NOT NULL DEFAULT '0',
	`penawaran_status_id` Int( 11 ) NOT NULL DEFAULT '0',
	`no_dokumen` VarChar( 200 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`file_name` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`original_file_name` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`file_size` BigInt( 20 ) NOT NULL DEFAULT '0',
	`file_sub_location` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`file_url` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`deskripsi` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`posisi_file` Int( 11 ) NOT NULL DEFAULT '1',
	`item_param` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`active` INT( 1 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "penawaran_jenis" --------------------------
-- CREATE TABLE "penawaran_jenis" ------------------------------
CREATE TABLE `penawaran_jenis` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`nama_penawaran_jenis` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`active` INT( 1 ) NULL DEFAULT '1',
	`created_date` DateTime NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` DateTime NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` DateTime NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 4;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "penawaran_status" -------------------------
-- CREATE TABLE "penawaran_status" -----------------------------
CREATE TABLE `penawaran_status` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`penawaran_id` Int( 11 ) NULL,
	`tahapan_status_id` Int( 11 ) NULL,
	`pesan` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`active` INT( 1 ) NULL DEFAULT '1',
	`created_date` DateTime NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` DateTime NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` DateTime NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "ppe_site" ---------------------------------
-- CREATE TABLE "ppe_site" -------------------------------------
CREATE TABLE `ppe_site` ( 
	`pps_id` VarChar( 128 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`tag_id` VarChar( 128 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`stg_id` VarChar( 128 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`audittype` VarChar( 1 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`audituser` VarChar( 128 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`auditupdate` Date NOT NULL,
	`pps_nama` VarChar( 128 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`pps_alamat` VarChar( 256 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`pps_display` VarChar( 128 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`pps_kontak` VarChar( 128 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`pps_proxy` VarChar( 128 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`pps_public_url` VarChar( 128 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`pps_private_url` VarChar( 128 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`pps_tanggal_pendaftaran` Date NOT NULL,
	`pps_sk` VarChar( 128 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	PRIMARY KEY ( `pps_id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "provinsi" ---------------------------------
-- CREATE TABLE "provinsi" -------------------------------------
CREATE TABLE `provinsi` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`nama_provinsi` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`active` Int( 2 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`kldi_id` VarChar( 6 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 36;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "report" -----------------------------------
-- CREATE TABLE "report" ---------------------------------------
CREATE TABLE `report` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`item_id` Int( 11 ) NOT NULL,
	`item_type` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`status` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'proses',
	`reporter_id` Int( 11 ) NULL,
	`reporter_name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`reporter_email` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`reporter_nik` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`message` Text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`created_date` DateTime NOT NULL,
	`reply` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`reply_by` Int( 11 ) NULL,
	`reply_date` DateTime NULL,
	`rating` Int( 11 ) NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 11;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "rup" --------------------------------------
-- CREATE TABLE "rup" ------------------------------------------
CREATE TABLE `rup` ( 
	`id` Int( 11 ) NOT NULL,
	`nama` VarChar( 2000 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`kegiatan` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`jenis_belanja` Int( 11 ) NULL,
	`jenis_pengadaan` Int( 11 ) NULL,
	`volume` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`metode_pengadaan` Int( 11 ) NULL,
	`tanggal_awal_pengadaan` Date NULL,
	`tanggal_akhir_pengadaan` Date NULL,
	`tanggal_awal_pekerjaan` Date NULL,
	`tanggal_akhir_pekerjaan` Date NULL,
	`lokasi` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`keterangan` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`tahun_anggaran` Int( 10 ) NULL,
	`id_sat_ker` Int( 11 ) NULL,
	`kode_kldi` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`aktif` Int( 11 ) NULL,
	`is_delete` Int( 11 ) NULL,
	`audit_update` Timestamp NULL,
	`kode_anggaran` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`sumber_dana` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `idx_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "satker" -----------------------------------
-- CREATE TABLE "satker" ---------------------------------------
CREATE TABLE `satker` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`nama` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`alamat` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`instansi_id` Int( 11 ) NOT NULL,
	`active` INT( 1 ) NOT NULL,
	`created_date` DateTime NULL,
	`idSatker` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`modified_date` DateTime NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 34306706;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "sumber_dana" ------------------------------
-- CREATE TABLE "sumber_dana" ----------------------------------
CREATE TABLE `sumber_dana` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`nama_sumber_dana` VarChar( 300 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`active` Int( 2 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 12;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "sumber_dana_paket" ------------------------
-- CREATE TABLE "sumber_dana_paket" ----------------------------
CREATE TABLE `sumber_dana_paket` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`sumber_dana_id` Int( 11 ) NOT NULL,
	`paket_id` Int( 11 ) NOT NULL,
	`kode_anggaran` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 69912;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "tag" --------------------------------------
-- CREATE TABLE "tag" ------------------------------------------
CREATE TABLE `tag` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`nama_tag` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`kategori` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`active` Int( 2 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `UQ_NAMA_TAG` UNIQUE( `nama_tag` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 46;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "tahapan" ----------------------------------
-- CREATE TABLE "tahapan" --------------------------------------
CREATE TABLE `tahapan` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`nama_tahapan` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`nama_tahapan_alias` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`active` Enum( '0', '1' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
	`created_date` DateTime NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` DateTime NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` DateTime NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 17;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "tahapan_dokumen" --------------------------
-- CREATE TABLE "tahapan_dokumen" ------------------------------
CREATE TABLE `tahapan_dokumen` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`tahapan_id` Int( 11 ) NULL,
	`dokumen_id` Int( 11 ) NULL,
	`active` Int( 11 ) NULL DEFAULT '1',
	`created_date` DateTime NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` DateTime NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` DateTime NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 53;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "tahapan_status" ---------------------------
-- CREATE TABLE "tahapan_status" -------------------------------
CREATE TABLE `tahapan_status` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`tahapan_id` Int( 11 ) NULL,
	`metode_id` Int( 11 ) NULL,
	`penawaran_jenis_id` Int( 11 ) NULL,
	`parent` Int( 11 ) NULL,
	`stop` INT( 1 ) NULL,
	`durasi` Int( 2 ) NULL,
	`active` Int( 2 ) NULL DEFAULT '1',
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 40;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "temp_data" --------------------------------
-- CREATE TABLE "temp_data" ------------------------------------
CREATE TABLE `temp_data` ( 
	`id` BigInt( 20 ) AUTO_INCREMENT NOT NULL,
	`temp_id` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`data_type` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`data_id` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`data_value` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`created_date` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 37663;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "temp_data_fixed" --------------------------
-- CREATE TABLE "temp_data_fixed" ------------------------------
CREATE TABLE `temp_data_fixed` ( 
	`c01` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`c02` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`c03` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`c04` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`c05` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`c06` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`c07` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`c08` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`c09` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`c10` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "temp_rekanan" -----------------------------
-- CREATE TABLE "temp_rekanan" ---------------------------------
CREATE TABLE `temp_rekanan` ( 
	`rkn_id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`rkn_nama` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`rkn_daftar` DateTime NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `rkn_id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
COMMENT 'temp untuk rekanan'
ENGINE = InnoDB
AUTO_INCREMENT = 1215;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "unit_pengukuran" --------------------------
-- CREATE TABLE "unit_pengukuran" ------------------------------
CREATE TABLE `unit_pengukuran` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`nama_unit_pengukuran` VarChar( 300 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`active` Int( 2 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 84;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "unspsc" -----------------------------------
-- CREATE TABLE "unspsc" ---------------------------------------
CREATE TABLE `unspsc` ( 
	`segment_title` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`family_title` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`class_title` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`commodity_title` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`commodity_id` Int( 11 ) NOT NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `commodity_id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "usulan" -----------------------------------
-- CREATE TABLE "usulan" ---------------------------------------
CREATE TABLE `usulan` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`metode_id` Int( 11 ) NULL,
	`status` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'sedang_diproses',
	`tgl_masa_berlaku_mulai` Date NULL,
	`tgl_masa_berlaku_selesai` Date NULL,
	`nama_usulan` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`jenis_usulan` Enum( 'K', 'D' ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`pengusul_nama` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`pengusul_email` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`pengusul_no_telp` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`pengusul_alamat` VarChar( 1000 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`active` Int( 2 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 6;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "usulan_dokumen" ---------------------------
-- CREATE TABLE "usulan_dokumen" -------------------------------
CREATE TABLE `usulan_dokumen` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`usulan_id` Int( 11 ) NOT NULL,
	`dokumen_id` Int( 11 ) NOT NULL DEFAULT '0',
	`usulan_status_id` Int( 11 ) NOT NULL DEFAULT '0',
	`no_dokumen` VarChar( 200 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`file_name` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`original_file_name` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`file_size` BigInt( 20 ) NOT NULL DEFAULT '0',
	`file_sub_location` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`file_url` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`deskripsi` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`posisi_file` Int( 11 ) NOT NULL DEFAULT '1',
	`item_param` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`active` Int( 2 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 7;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "usulan_komoditas" -------------------------
-- CREATE TABLE "usulan_komoditas" -----------------------------
CREATE TABLE `usulan_komoditas` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`usulan_id` Int( 11 ) NOT NULL,
	`komoditas_id` Int( 11 ) NOT NULL,
	`active` Int( 2 ) NOT NULL DEFAULT '1',
	`created_date` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` Int( 11 ) NOT NULL,
	`modified_date` Timestamp NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` Timestamp NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 8;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "usulan_status" ----------------------------
-- CREATE TABLE "usulan_status" --------------------------------
CREATE TABLE `usulan_status` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`usulan_id` Int( 11 ) NULL,
	`tahapan_status_id` Int( 11 ) NULL,
	`pesan` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`active` Int( 2 ) NULL DEFAULT '1',
	`created_date` DateTime NULL,
	`created_by` Int( 11 ) NULL,
	`modified_date` DateTime NULL,
	`modified_by` Int( 11 ) NULL,
	`deleted_date` DateTime NULL,
	`deleted_by` Int( 11 ) NULL,
	`auditdate` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`audittype` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`audituser` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 7;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "fk_user_role_grup" ------------------------
-- CREATE INDEX "fk_user_role_grup" ----------------------------
CREATE INDEX `fk_user_role_grup` USING BTREE ON `user`( `user_role_grup_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "item_id" ----------------------------------
-- CREATE INDEX "item_id" --------------------------------------
CREATE INDEX `item_id` USING BTREE ON `feedback`( `item_id`, `item_type` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "reporter_id" ------------------------------
-- CREATE INDEX "reporter_id" ----------------------------------
CREATE INDEX `reporter_id` USING BTREE ON `feedback`( `reporter_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "active" -----------------------------------
-- CREATE INDEX "active" ---------------------------------------
CREATE INDEX `active` USING BTREE ON `instansi`( `active` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "kldi_id" ----------------------------------
-- CREATE INDEX "kldi_id" --------------------------------------
CREATE INDEX `kldi_id` USING BTREE ON `instansi`( `kldi_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "kldi_id" ----------------------------------
-- CREATE INDEX "kldi_id" --------------------------------------
CREATE INDEX `kldi_id` USING BTREE ON `kabupaten`( `kldi_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_pktkon_pi" ----------------------------
-- CREATE INDEX "idx_pktkon_pi" --------------------------------
CREATE INDEX `idx_pktkon_pi` USING BTREE ON `paket_kontrak`( `paket_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_ppem_pi" ------------------------------
-- CREATE INDEX "idx_ppem_pi" ----------------------------------
CREATE INDEX `idx_ppem_pi` USING BTREE ON `paket_pembayaran`( `paket_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_ppen_pi" ------------------------------
-- CREATE INDEX "idx_ppen_pi" ----------------------------------
CREATE INDEX `idx_ppen_pi` USING BTREE ON `paket_penerimaan`( `paket_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_ppnds" --------------------------------
-- CREATE INDEX "idx_ppnds" ------------------------------------
CREATE INDEX `idx_ppnds` USING BTREE ON `paket_penerimaan`( `paket_pengiriman_no_dokumen_sistem` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_ppenp_ppi" ----------------------------
-- CREATE INDEX "idx_ppenp_ppi" --------------------------------
CREATE INDEX `idx_ppenp_ppi` USING BTREE ON `paket_penerimaan_produk`( `paket_penerimaan_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_ppeng_pi" -----------------------------
-- CREATE INDEX "idx_ppeng_pi" ---------------------------------
CREATE INDEX `idx_ppeng_pi` USING BTREE ON `paket_pengiriman`( `paket_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_ppenglamp_ppi" ------------------------
-- CREATE INDEX "idx_ppenglamp_ppi" ----------------------------
CREATE INDEX `idx_ppenglamp_ppi` USING BTREE ON `paket_pengiriman_lampiran`( `paket_pengiriman_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_ppengp_ppi" ---------------------------
-- CREATE INDEX "idx_ppengp_ppi" -------------------------------
CREATE INDEX `idx_ppengp_ppi` USING BTREE ON `paket_pengiriman_produk`( `paket_pengiriman_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_ppengs_ppi" ---------------------------
-- CREATE INDEX "idx_ppengs_ppi" -------------------------------
CREATE INDEX `idx_ppengs_ppi` USING BTREE ON `paket_pengiriman_status`( `paket_pengiriman_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_paket_produk_id" ----------------------
-- CREATE INDEX "idx_paket_produk_id" --------------------------
CREATE INDEX `idx_paket_produk_id` USING BTREE ON `paket_produk`( `paket_id`, `produk_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_pp_paket_id_pp_active" ----------------
-- CREATE INDEX "idx_pp_paket_id_pp_active" --------------------
CREATE INDEX `idx_pp_paket_id_pp_active` USING BTREE ON `paket_produk`( `paket_id`, `active` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_det_active" ---------------------------
-- CREATE INDEX "idx_det_active" -------------------------------
CREATE INDEX `idx_det_active` USING BTREE ON `paket_produk_nego_detail`( `active` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_det_nego_header_id" -------------------
-- CREATE INDEX "idx_det_nego_header_id" -----------------------
CREATE INDEX `idx_det_nego_header_id` USING BTREE ON `paket_produk_nego_detail`( `nego_header_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_det_paket_produk_id" ------------------
-- CREATE INDEX "idx_det_paket_produk_id" ----------------------
CREATE INDEX `idx_det_paket_produk_id` USING BTREE ON `paket_produk_nego_detail`( `paket_produk_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_det_produk_id" ------------------------
-- CREATE INDEX "idx_det_produk_id" ----------------------------
CREATE INDEX `idx_det_produk_id` USING BTREE ON `paket_produk_nego_detail`( `produk_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_active" -------------------------------
-- CREATE INDEX "idx_active" -----------------------------------
CREATE INDEX `idx_active` USING BTREE ON `paket_riwayat`( `active` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_paket_id_active" ----------------------
-- CREATE INDEX "idx_paket_id_active" --------------------------
CREATE INDEX `idx_paket_id_active` USING BTREE ON `paket_riwayat`( `paket_id`, `active` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_nego_header_id" -----------------------
-- CREATE INDEX "idx_nego_header_id" ---------------------------
CREATE INDEX `idx_nego_header_id` USING BTREE ON `paket_status`( `nego_header_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "kldi_id" ----------------------------------
-- CREATE INDEX "kldi_id" --------------------------------------
CREATE INDEX `kldi_id` USING BTREE ON `provinsi`( `kldi_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "item_id" ----------------------------------
-- CREATE INDEX "item_id" --------------------------------------
CREATE INDEX `item_id` USING BTREE ON `report`( `item_id`, `item_type` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "reporter_id" ------------------------------
-- CREATE INDEX "reporter_id" ----------------------------------
CREATE INDEX `reporter_id` USING BTREE ON `report`( `reporter_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "status" -----------------------------------
-- CREATE INDEX "status" ---------------------------------------
CREATE INDEX `status` USING BTREE ON `report`( `status` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_aktif" --------------------------------
-- CREATE INDEX "idx_aktif" ------------------------------------
CREATE INDEX `idx_aktif` USING BTREE ON `rup`( `aktif` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_id_aktif" -----------------------------
-- CREATE INDEX "idx_id_aktif" ---------------------------------
CREATE INDEX `idx_id_aktif` USING BTREE ON `rup`( `id`, `aktif` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_nama_aktif" ---------------------------
-- CREATE INDEX "idx_nama_aktif" -------------------------------
CREATE INDEX `idx_nama_aktif` USING BTREE ON `rup`( `nama`( 255 ), `aktif` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "active" -----------------------------------
-- CREATE INDEX "active" ---------------------------------------
CREATE INDEX `active` USING BTREE ON `satker`( `active` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idSatker" ---------------------------------
-- CREATE INDEX "idSatker" -------------------------------------
CREATE INDEX `idSatker` USING BTREE ON `satker`( `idSatker` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "instansiId_active" ------------------------
-- CREATE INDEX "instansiId_active" ----------------------------
CREATE INDEX `instansiId_active` USING BTREE ON `satker`( `instansi_id`, `active` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "nama_active" ------------------------------
-- CREATE INDEX "nama_active" ----------------------------------
CREATE INDEX `nama_active` USING BTREE ON `satker`( `nama`, `active` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_td_ttiv" ------------------------------
-- CREATE INDEX "idx_td_ttiv" ----------------------------------
CREATE INDEX `idx_td_ttiv` USING BTREE ON `temp_data`( `temp_id`, `data_type`, `data_id`, `data_value`( 255 ) );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "td_di" ------------------------------------
-- CREATE INDEX "td_di" ----------------------------------------
CREATE INDEX `td_di` USING BTREE ON `temp_data`( `data_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "td_dv" ------------------------------------
-- CREATE INDEX "td_dv" ----------------------------------------
CREATE INDEX `td_dv` USING BTREE ON `temp_data`( `data_value`( 255 ) );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "td_dy" ------------------------------------
-- CREATE INDEX "td_dy" ----------------------------------------
CREATE INDEX `td_dy` USING BTREE ON `temp_data`( `data_type` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "idx_tdf_c0102" ----------------------------
-- CREATE INDEX "idx_tdf_c0102" --------------------------------
CREATE INDEX `idx_tdf_c0102` USING BTREE ON `temp_data_fixed`( `c01`, `c02` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE LINK "lnk_user_user_role_grup" -------------------
-- CREATE LINK "lnk_user_user_role_grup" -----------------------
ALTER TABLE `user_role_grup`
	ADD CONSTRAINT `lnk_user_user_role_grup` FOREIGN KEY ( `id` )
	REFERENCES `user`( `user_role_grup_id` )
	ON DELETE Restrict
	ON UPDATE Restrict;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


