package models.katalog;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 4/8/2020 2:12 PM
 */
public class XlsPrice {

    @SerializedName("price")
    List<Price> prices;
    @SerializedName("cost")
    List<Map<String, Object>> costs;

    public static class Price {

        @SerializedName("Provinsi")
        public String province;
        @SerializedName("Kabupaten")
        public String regency;
        @SerializedName("Harga Pemerintah")
        public Double governmentPrice;
        @SerializedName("Harga Non Pemerintah")
        public Double governmentNonPrice;

    }

}
