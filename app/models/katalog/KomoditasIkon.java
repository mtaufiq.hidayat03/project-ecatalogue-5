package models.katalog;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

@Table(name = "komoditas_ikon")
public class KomoditasIkon extends BaseKatalogTable {
    @Id
    public Long id;

    public Long komoditas_id;

    public String file_name;

    public String original_file_name;

    public long file_size;

    public String file_sub_location;

    public static KomoditasIkon findByKomoditasId(long komoditas_id){
        String query = "select * from komoditas_ikon where komoditas_id = ?";

        return Query.find(query, KomoditasIkon.class, komoditas_id).first();
    }
}
