package controllers;

import ext.Pageable;
import features.komoditas.KomoditasService;
import models.cms.Banner;
import models.cms.Berita;
import models.cms.Notifikasi;
import models.elasticsearch.SearchResult;
import models.katalog.Komoditas;
import models.prakatalog.Usulan;
import play.cache.CacheFor;
import play.i18n.Lang;
import repositories.UsulanRepository;

import java.util.List;

public class HomeCtr extends BaseController {

    public static int max_konten = 12;
    public static int max_faq = 20;

    @CacheFor("10mn")
    public  static void Home(){
        String lang = params.get("lang");
        if(null == lang){
            Lang.change("id");
        }else if(!lang.equalsIgnoreCase("id")){
            Lang.change("en");
        }else{
            Lang.change("id");
        }
        List<Usulan> usulanList = UsulanRepository.homePage();
        /* Notifikasi */
        Notifikasi notif = Notifikasi.findActive();
        /* Berita */
        List<Berita> beritaList = Berita.findLastesBerita();

        if (getAktifUser() != null) {
            notif = null;
        }

        List<Komoditas> komoditasList = Komoditas.findAllActive();

        List<Banner> bannerList = Banner.findAllBannerActive();

        SearchResult popular = new SearchResult();
        renderArgs.put("popularCommodities", KomoditasService.getInstance().groupByCategory(komoditasList));//popular.commodities
        renderArgs.put("popularProducts", popular.aggregateProducts);
        renderArgs.put("bannerList", bannerList);
        render("HomeCtr/Home.html", usulanList, notif, beritaList);
    }

    public static void listBerita() {
        int page= null != params.get("offset") ? params.get("offset", Integer.class) : 1;
        int start = page*max_konten-max_konten;

        List<Berita> beritaAll = Berita.findAllPublished();
        Pageable p = new Pageable(page, max_konten, beritaAll.size(), "berita");
        List<Berita> beritaList = Berita.findJoinKategoriAllActiveByLimit(Long.valueOf(start), Long.valueOf(max_konten));
        List<Berita> kategoriKontenList = Berita.findAllCountBerita();
        List<Berita> beritaLatestList = Berita.findLastesBerita();
        List<Berita> beritaPopularList = Berita.findPopularBerita();
        render("HomeCtr/listBerita.html",p,beritaList,kategoriKontenList,beritaLatestList,beritaPopularList);
    }
}
