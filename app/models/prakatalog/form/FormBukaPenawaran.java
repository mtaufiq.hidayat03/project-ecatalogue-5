package models.prakatalog.form;

import models.common.JadwalUsulan;

import java.io.Serializable;
import java.util.List;

/**
 * Created by raihaniqbal on 9/7/17.
 */
public class FormBukaPenawaran implements Serializable {

    public long usulan_id;
    public String judul;
    public List<JadwalUsulan> jadwalUsulanList;
    public Long akhir_penawaran;
    public String[] dokumen_arr;
    public String[] jenis_evaluasi;
    public String[] kualifikasi_sikap;
    public boolean isLelang = false;
    public boolean isBukaPenawaran = false;
    public boolean allowToAddPenawaran = false;
    public boolean isAjukanPenawaran = false;
    public boolean isTerdaftar = false;
    public boolean isPenawaranExist = false;
    public boolean isAkhirPenawaran = false;
    public boolean isRegistration = false;
    public boolean isExplanation = false;
    public boolean isPenawaran = false;

}
