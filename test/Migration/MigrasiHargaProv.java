package Migration;

import models.katalog.Produk;
import play.Play;
import play.db.jdbc.Query;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MigrasiHargaProv {
    String filename = "reportProdukHargaProv_"+ UUID.randomUUID().toString().substring(0,5)+".txt";
    static String rootPath = Play.applicationPath.getAbsolutePath();
    String msg = "";
    public String sqlHeaderProduk = "";
    public String sqlBody = "";
    int pageLength = 50;
    List<Produk> listProduk = new ArrayList<>();

    public MigrasiHargaProv(){
        sqlBody = "from produk p\n" +
                "join produk_harga_provinsi pp on p.id = pp.produk_id and pp.active = 1\n" +
                "join provinsi pr on pp.provinsi_id = pr.id";
    }

    public void startMigration(){
        msg="Starting Migration ONGKIR";writeReportToFile(msg);

        int start = 0;
        int end = pageLength;
        long totalProduk =  getTotal();//new Long(4493);

        msg = "total produk: "+totalProduk; writeReportToFile(msg);

        int totalPage = new Long(totalProduk/pageLength).intValue();
        int page = 1;
        List<Produk> listProduk = new ArrayList<>();

        while (totalProduk > start){

            end+=pageLength;start+=pageLength;page++;
        }
    }

    public long getTotal(){
        msg = "Get TOTAL produk";writeReportToFile(msg);
        Long total = 0L;
        try {
            String sql = "select count(distinct(p.id)) " + sqlBody;
            total = Query.count(sql);
        }catch (Exception e){
            msg = "error get total: "+e.getMessage(); writeReportToFile(msg);
        }
        return total;
    }

    public void writeReportToFile(String text) {
        System.out.println(text);
//        text += " \n";
//        if (!Files.exists(Paths.get(rootPath + "/"+filename))) {
//            PrintWriter writer = null;
//            try {
//                System.out.println("new file name report:"+filename);
//                writer = new PrintWriter(rootPath + "/"+filename, "UTF-8");
//            } catch (FileNotFoundException e) {
//
//            } catch (UnsupportedEncodingException e) {
//
//            }
//            writer.println("----<The first line >--- \n");
//            writer.close();
//        }
//        try {
//            Files.write(Paths.get(filename), text.getBytes(), StandardOpenOption.APPEND);
//        } catch (IOException e) {
//            //exception handling left as an exercise for the reader
//            System.out.println("Eror write filereport:" + e.getMessage());
//            e.printStackTrace();
//        }
        msg = "";
    }

    protected List<Produk> getProdukPerPage(int start, int end){
        String headerSql = "";
        String sql = sqlHeaderProduk+ sqlBody+" limit ?, ?";
        listProduk = new ArrayList<>();
        try{
            listProduk =  Query.find(sql, Produk.class, start,end).fetch();
        }catch (Exception e){
            msg = "error getProdukPerpage:"+e.getMessage();
            writeReportToFile(msg);
        }
        return listProduk;
    }
}
