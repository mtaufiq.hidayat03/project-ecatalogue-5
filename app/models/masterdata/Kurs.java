package models.masterdata;

import models.BaseKatalogTable;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.sql.Timestamp;
import java.util.List;

@Table(name = "kurs")
public class Kurs extends BaseKatalogTable {

    @Id
    public Long id;
    public String nama_kurs;
    public boolean active;

    public static final long kurs_utama = 1;

    public static void deleteById(long id){
        Kurs kurs = Kurs.findById(id);
        kurs.active = false;
        kurs.save();
    }

    public static Kurs findByNamaKurs(String namaKurs){
        return find("nama_kurs = ?", namaKurs).first();
    }

    public static Kurs findKursUtama(){
        return find("id = ?", kurs_utama).first();
    }

    public void softDelete() {
        this.active = false;
        setDeleted();
        save();
    }

	public void setAktif() {
		this.active = true;
		prePersist();
		save();
	}

	public void setNonAktif() {
		this.active = false;
		prePersist();
		save();
	}

	public static Kurs getUsd() {
		return findByNamaKurs("USD");
	}



}