package models.purchasing.keranjangBelanja;

import java.util.List;

/**
 * Created by dadang on 11/1/17.
 */
public class PenyediaKeranjang {

	public Long penyediaId;
	public String namaPenyedia;
	public Double totalKeseluruhan;
	public List<ProdukKeranjang> produkKeranjangList;
	public Long kursId;
	public String namaKurs;
	public Long wilayahId;
	public String namaWilayah;
	public Long komoditasId;
	public String namaKomoditas;
}
