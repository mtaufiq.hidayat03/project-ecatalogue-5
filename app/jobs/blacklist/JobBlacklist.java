package jobs.blacklist;

import java.util.List;
import java.util.UUID;

import jobs.TrackableJob;
import models.jcommon.config.Configuration;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.blacklist.Blacklist;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import play.jobs.On;

/** job di eksekusi setiap jam 01:30 **/
// @On("0 30 1 * * ?") // daily at 08 : 39 @On("0 39 08 * * ?")
@NoTransaction
public class JobBlacklist extends TrackableJob {

	public void doJob() {
		pullData();
//		new Job() {
//			String uniqueID = UUID.randomUUID().toString();
//			@Override
//			public void before() {
//				Logger.info("job id:" + uniqueID);
//				super.before();
//			}
//
//			public void doJob() {
//				pullData();
//			}
//		}.in(1);
	}

	private void pullData() {
		Logger.info("Job Blacklist Start");
		ExtBlacklistData ext = new ExtBlacklistData();
		ext.getBlacklistData();
		ext.run();

		String reportText = "Import data blacklist selesai";
		try {
			String report = Configuration
					.getConfigurationValue(MailQueue.SMTP_MAIL_ADMIN, "");
			String[] ary = report.split(";");
			for (String to : ary) {
				Logger.info("create mail prop job report to: " + to);
				MailQueue mq = EmailManager.createEmail(to, reportText,
						"Report Job Dce Blacklist");
				mq.save();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error("error create mail:" + e.getMessage());
		}
	}
}
