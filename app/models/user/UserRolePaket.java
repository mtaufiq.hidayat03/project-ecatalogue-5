package models.user;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Table(name="user_role_paket")
public class UserRolePaket extends BaseKatalogTable {

	@Id
	public Long id;
	public boolean active;
	public Long paket_id;
	public Long user_role_grup_id;

	@Transient
	public String nama_paket;

	public UserRolePaket() {}

	public UserRolePaket(Long packageId, Long groupId) {
		this.paket_id = packageId;
		this.user_role_grup_id = groupId;
	}

	public static List<UserRolePaket> findByUserRoleGrupId(long id){
		String query = "select uri.id, uri.paket_id, uri.user_role_grup_id, ri.nama_paket as nama_paket from user_role_paket uri " +
				"inner join paket ri on uri.paket_id = ri.id where uri.user_role_grup_id = ?";

		return Query.find(query, UserRolePaket.class, id).fetch();
	}

	public static Map<Long, String> findByUserRoleGrupIdMap(long id){
		List<UserRolePaket> userRolePakets = findByUserRoleGrupId(id);

		Map<Long, String> result = userRolePakets.stream().collect(
				Collectors.toMap(x -> x.paket_id, x -> x.nama_paket));

		return result;
	}

	public static void deleteByUserRoleGrupId(long id){
		String query = "update user_role_paket set active = ? where user_role_grup_id = ?";
		Query.update(query, NOT_AKTIF, id);
	}

	public static Integer getTotalPackageByUserRoleGrupId(Long id) {
		return Query.find("SELECT COUNT(*) FROM user_role_paket " +
				"WHERE user_role_grup_id =? AND active =?", Integer.class, id, AKTIF).first();
	}

	public void saveRole() {
		final int result = save();
		if (this.id == null) {
			this.id = (long) result;
		}
	}

}
