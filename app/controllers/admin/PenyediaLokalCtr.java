package controllers.admin;

import controllers.BaseController;
import controllers.security.AllowAccess;
import models.katalog.Komoditas;
import models.masterdata.Kldi;
import models.penyedia.Penyedia;
import models.penyedia.PenyediaKomoditas;
import models.penyedia.PenyediaSektoral;
import models.penyedia.PenyediaWilayah;
import models.secman.Acl;
import models.user.User;
import org.apache.commons.lang.ArrayUtils;
import play.i18n.Messages;

public class PenyediaLokalCtr extends BaseController {

    @AllowAccess(Acl.COM_PENYEDIA_LOKAL)
    public static void index() {
        User user = User.findById(getAktifUser().user_id);
        Kldi kldi = Kldi.findById(user.kldi_id);
        render("admin/PenyediaLokalCtr/index.html", kldi);
    }

    @AllowAccess(Acl.COM_PENYEDIA_LOKAL_ADD)
    public static void createPenyediaLokal(Long komoditas_id){
        Komoditas komoditas = Komoditas.findById(komoditas_id);
        render("admin/PenyediaLokalCtr/createPenyediaLokal.html", komoditas);
    }

    @AllowAccess(Acl.COM_PENYEDIA_LOKAL_ADD)
    public static void createSubmitPenyediaLokal(Long komoditas_id, Long user_ids[]){
        try {
            Komoditas komoditas = Komoditas.findById(komoditas_id);

            if (!ArrayUtils.isEmpty(user_ids)) {
                for (Long idUser : user_ids) {
                    Penyedia penyedia = Penyedia.getProviderByUserId(idUser);
                    PenyediaKomoditas.deleteByPenyediaKomoditas(penyedia.id, komoditas_id);
                    PenyediaKomoditas.addNew(penyedia.id, komoditas_id);

                    if (komoditas.komoditas_kategori_id == 5) { //Tipe Komoditas Lokal
                        PenyediaWilayah.addNew(komoditas.kldi_id, penyedia.id);
                    } else if (komoditas.komoditas_kategori_id == 6) { //Tipe Komoditas Sektoral
                        PenyediaSektoral.addNew(komoditas.kldi_id, penyedia.id);
                    }
                }
                flash.success(Messages.get("notif.success.submit"));
            }
        }catch (Exception e){
            flash.error(Messages.get("notif.failed.submit"));
        }
        createPenyediaLokal(komoditas_id);
    }

    @AllowAccess(Acl.COM_PENYEDIA_LOKAL_DELETE)
    public static void deleteUserKomoditas(Long id){
        PenyediaKomoditas pk = PenyediaKomoditas.findById(id);
        Komoditas kom = Komoditas.findById(pk.komoditas_id);
        PenyediaKomoditas.deleteByPenyediaKomoditas(pk.penyedia_id, pk.komoditas_id);
        if (kom.komoditas_kategori_id == 5) { //Tipe Komoditas Lokal
            PenyediaWilayah.deleteByPenyediaWilayah(kom.kldi_id, pk.penyedia_id);
        } else if (kom.komoditas_kategori_id == 6) { //Tipe Komoditas Sektoral
            PenyediaSektoral.deleteByPenyediaSektoral(kom.kldi_id, pk.penyedia_id);
        }

        flash.success(Messages.get("notif.success.delete"));
        createPenyediaLokal(pk.komoditas_id);
    }
}