package Migration;

import com.google.gson.Gson;
import jobs.ExtProduct.ExtCrawlVendor;
import jobs.ExtProduct.ExtProduct;
import jobs.ExtProduct.PenyediaKomoditasApi;
import models.api.produk.HistoryDatafeed;
import models.api.produk.ProdukApiInfo;
import models.api.produk.detail.image.Item;
import models.jcommon.config.Configuration;
import models.jcommon.http.SimpleHttpRequest;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.katalog.*;
import models.katalog.komoditasmodel.KomoditasAtributHarga;
import models.penyedia.Penyedia;
import models.util.Config;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import play.Logger;
import play.db.jdbc.Query;
import play.jobs.Job;
import play.test.UnitTest;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TestGrabUrl extends UnitTest {



    @Test
    public void CekUrl(){
        ExtProduct ext = new ExtProduct();
        List<PenyediaKomoditasApi> list = ext.getUrlPenyedia(77l,1l);
        int delaySecond = 1;
        for (PenyediaKomoditasApi pk: list) {
            Logger.debug(">>>>>>>>>:"+pk.nama_penyedia);
            //new Job(){
                PenyediaKomoditasApi _pk = pk;
                //public void doJob(){
                    ExtProduct ext2 = new ExtProduct();
                    //ext2.executeOne(_pk);
                //}
            //}.in(delaySecond++);
        }

        try {

            Thread.sleep(480000);//
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void doJob(){

        ExtProduct exts = new ExtProduct();
        List<PenyediaKomoditasApi> list = exts.getUrlPenyedia(77l, 1l);
        HistoryDatafeed hdf = new HistoryDatafeed();
        String ids = "";
        int delaySecond = 1;
        for (PenyediaKomoditasApi pk: list) {
            Logger.debug(">>>>>>>>>:"+pk.nama_penyedia);
            //hdf.start_time = new Timestamp(new Date().getTime()).toString();
            new Job(){
                UUID id = UUID.randomUUID();
                PenyediaKomoditasApi _pk = pk;
                public void doJob(){
                    ExtProduct ext = new ExtProduct();

                    hdf.id_job = id.toString();
                    hdf.penyedia_id = _pk.penyediaid;
                    hdf.penyedia_name = _pk.nama_penyedia;
                    hdf.start_time = new Timestamp(new Date().getTime()).toString();
                    hdf.save();
                    ext.executeOne(_pk,hdf);
                }

            }.in(delaySecond++);
        }
        try {

            Thread.sleep(500000);//
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void crawlVendor(){
        ExtCrawlVendor varc = new ExtCrawlVendor();
        List<Penyedia> listPenyedia = Penyedia.getVendorAPI();
        for (Penyedia py: listPenyedia) {
            varc.CrawlVendor(py.komoditas_id, py.id);
        }
    }

    public Map<String, List<String>> splitQuery(URL url) throws UnsupportedEncodingException {
        Map<String, List<String>> query_pairs = new LinkedHashMap<String, List<String>>();
        if(url.getQuery() != null){
            String[] pairs = url.getQuery().split("&");
            for (String pair : pairs) {
                int idx = pair.indexOf("=");
                String key = idx > 0 ? URLDecoder.decode(pair.substring(0, idx), "UTF-8") : pair;
                if (!query_pairs.containsKey(key)) {
                    query_pairs.put(key, new LinkedList<String>());
                }
                String value = idx > 0 && pair.length() > idx + 1 ? URLDecoder.decode(pair.substring(idx + 1), "UTF-8") : null;
                query_pairs.get(key).add(value);
            }
        }
        return query_pairs;
    }

}
