package controllers.admin;

import com.google.gson.JsonObject;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.datatable.ResultsetHandlers;
import models.jcommon.datatable.DatatableQuery;
import models.katalog.Komoditas;
import models.masterdata.Kldi;
import models.secman.Acl;
import models.user.User;
import models.user.UserRoleKomoditasOverride;
import org.apache.commons.lang.ArrayUtils;
import play.Logger;
import play.i18n.Messages;

public class UserLokalCtr extends BaseController {

    @AllowAccess(Acl.COM_USER_LOKAL)
    public static void index() {
        User user = User.findById(getAktifUser().user_id);
        Kldi kldi = Kldi.findById(user.kldi_id);
        render("admin/UserLokalCtr/index.html", kldi);
    }

    @AllowAccess(Acl.COM_USER_LOKAL_ADD)
    public static void createUserKomoditas(Long komoditas_id){
        Komoditas komoditas = Komoditas.findById(komoditas_id);
        render("admin/UserLokalCtr/createUserKomoditas.html", komoditas);
    }

    @AllowAccess(Acl.COM_USER_LOKAL_ADD)
    public static void createSubmitUserKomoditas(Long komoditas_id, Long user_ids[]){
        if (!ArrayUtils.isEmpty(user_ids)){
            for (Long idUser : user_ids){
                int getRoleKomoditas = UserRoleKomoditasOverride.getTotalByUserIdKomId(idUser, komoditas_id);
                if(getRoleKomoditas == 0) {
                    int getTotalUserRoleKomoditas = UserRoleKomoditasOverride.getTotalByUserId(idUser);
                    if(getTotalUserRoleKomoditas == 0){
                        User user = User.findById(idUser);
                        user.override_role_komoditas = true;
                        user.save();
                    }
                    new UserRoleKomoditasOverride(komoditas_id, idUser).save();
                }
            }
            flash.success(Messages.get("notif.success.submit"));
            createUserKomoditas(komoditas_id);
        }
    }

    @AllowAccess(Acl.COM_USER_LOKAL_DELETE)
    public static void deleteUserKomoditas(Long id, Long idKom){
        UserRoleKomoditasOverride userRoleKomoditasOverride = UserRoleKomoditasOverride.findById(id);
        Long user_id = userRoleKomoditasOverride.user_id;
        UserRoleKomoditasOverride.deleteById(id);
        int getTotalUserRoleKomoditas = UserRoleKomoditasOverride.getTotalByUserId(user_id);
        if(getTotalUserRoleKomoditas == 0){
            User user = User.findById(user_id);
            user.override_role_komoditas = false;
            user.save();
        }
        flash.success(Messages.get("notif.success.delete"));
        createUserKomoditas(idKom);
    }
}
