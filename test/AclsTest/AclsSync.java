package AclsTest;

import models.secman.Acl;
import models.user.RoleItem;
import org.junit.Test;
import play.Logger;
import play.test.UnitTest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AclsSync extends UnitTest {

    //@Test
    public void doTest(){

        List<String> strAcl = new ArrayList<>();
        List<String> dbAcl = new ArrayList<>();
        for (Acl acl :Acl.values()){
            strAcl.add(acl.namaRole);
        }
        Logger.debug("stracl:"+strAcl.size());
        List<RoleItem> roles =  RoleItem.findAllActive();

        for (RoleItem ri: roles) {
            dbAcl.add(ri.nama_role);
        }

        List<String> sumber1 = strAcl;
        List<String> sumber2 = strAcl;

        List<String> notin1 = dbAcl;
        List<String> notin2 = dbAcl;

        sumber1.removeAll(notin1);

        for (String acs: sumber1) {

        }


        notin2.removeAll(sumber2);
        System.out.println("yang ada enum acl tidak ada di db");
        System.out.println(sumber1);
        System.out.println("yang ada di db tidak ada di enum acl");
        System.out.println(notin2);

    }

    public void doTest2(){
        //List<String> strAcl = new ArrayList<>();
        String snot = "(";
        boolean first = true;
        for (Acl acl :Acl.values()){
            if(first==false){
                snot += ","+"'"+acl.namaRole+"'";
            }else{
                snot += "'"+acl.namaRole+"'";
                first=false;
            }

        }
        snot += ")";
        System.out.println(snot);


    }
}
