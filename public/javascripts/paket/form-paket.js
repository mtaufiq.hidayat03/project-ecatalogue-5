var sumDanaId = $("#tambahSumDana").data("jumlah");
sumDanaId = parseInt(sumDanaId);
var selectRup = $('input[name="paket.paket_id"]').val() != "";
/* not working in chrome
window.onbeforeunload = function() {
    var UrlReload = $('#url_reload').val();
    window.setTimeout(function () {
        window.location.href = UrlReload
    }, 0);
    window.onbeforeunload = null;
}; */

$(document).ready(function() {
    var isEdit = $('#isEdit').val();
    function functionToRun () {
        window.setTimeout(function () {
            window.location.href = $('#url_reload').val()
        }, 0);
       // this.removeEventListener("beforeunload", arguments.callee);
    }
    window.addEventListener("beforeunload", functionToRun);
    $("#ppk_username").hide();
    $('#rootwizard').bootstrapWizard({
        onTabShow : function(tab, navigation, index) {
            $('.next').show();
            $('.simpan').hide();
            var $total = navigation.find('li').length;
            var $current = index + 1;
            var $percent = ($current / $total) * 100;
            $('#rootwizard .progress-bar').css({
                width : $percent + '%'
            });
            //console.log("current = "+$current);
            //console.log("total = "+$total);
            if($current === $total) {
                $('.next').hide();
                $('.simpan').css('float', 'right');
                $('.simpan').show();
            }
            if (!(index === 0)) {
                automatedSaving();
            }
            /*if($current === $total) {
                window.onbeforeunload = null;
            } */
        },
        onTabClick: function(tab, navigation, index) {
            // alert('on tab click disabled');
            return false;
        },
        onNext: function (tab, navigation, index) {
            return validatePage(index);
        }
    });

    function automatedSaving() {
        let element = $('#auto-save')
        if (typeof element === 'undefined' || element == null || element.val() !== 'yes') {
            return
        }
        var form = new FormData($('.form-paket')[0]);
        if (isEdit === "no") {
            //start for cleaning data currency
            //delete already data exists
            form.delete('paket.produk_kuantitas');
            form.delete('paket.harga_ongkir');
            form.delete('paket.produk_harga_satuan');
            form.delete('paket.harga_total');
            //append new data with cleaning currency
            $('input[name^="paket.produk_kuantitas"]').each(function() {
                form.append('paket.produk_kuantitas', myParseFloat($(this).val()));
            });
            $('input[name^="paket.harga_ongkir"]').each(function() {
                form.append('paket.harga_ongkir', myParseFloat($(this).val()));
            });
            $('input[name^="paket.produk_harga_satuan"]').each(function() {
                form.append('paket.produk_harga_satuan', myParseFloat($(this).val()));
            });
            $('input[name^="paket.harga_total"]').each(function() {
                form.append('paket.harga_total', myParseFloat($(this).val()));
            });
            //end of cleaning
        }
        if (isEdit === "yes") {
            //set this value for updating ongkos kirim, because select2 not updating value after change
            form.set("paket.pengiriman_kabupaten", $("#peng-kabupaten").val());
        }
        let currentStatus = $('#paket-status').val();
        if (typeof currentStatus === 'undefined' || currentStatus == null || currentStatus === '') {
            form.set('paket.status', 'draft')
        }
        let productId = $('#paket-id').val()
        if (typeof productId !== 'undefined' && productId != null && productId !== '') {
            form.set('paket.action', 'update')
        }
        // console log of formdata
        /*for (var pair of form.entries()) {
            console.log(pair[0]+ '=' + pair[1]);
        } */
        $.ajax({
            type: 'POST',
            url: params.submitPaket,
            data: form,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            success: function(result){
                if (result.status && result.payload != null) {
                    $('#paket-id').val(result.payload.id)
                    let checkPaketIdExist = $('#paket-id').val();
                    let urlReload = params.reloadPaket + checkPaketIdExist;
                    $('#url_reload').val(urlReload);
                    $('#form-action').val('update')
                }
            },
            error: function(data) {
            }
        })
    }

    function buttonValue(element) {
        if (typeof element === 'undefined' || element == null) {
              return null
        }
        return $(element).data('value')
    }

    $('.button-save').click(function(e) {
        e.preventDefault()
        window.removeEventListener("beforeunload", functionToRun);
        let elementValue = buttonValue($(this))
        if (elementValue === 'draft') {
            $('#paket-status').val('draft')
        } else {
            $('#paket-status').val('persiapan')
        }
        $(".currency").removeClass('currency').each(function(){
            $(this).val(myParseFloat($(this).val()));
        });
        $('.form-paket').submit()
    })

    // currency format
    $(".currency").convertThousand();
    $("#npwpSatker").mask("99.999.999.9–999.999");

    if(params["kelasHarga"] == "kabupaten"){
        $("#peng-provinsi").val(params["provinsiId"]);
        $("#peng-provinsi").change();
        $("#peng-provinsi").attr("disabled",true);
        $("#peng-kabupaten").attr("disabled",true);
    }else if(params["kelasHarga"] == "provinsi"){
        $("#peng-provinsi").val(params["wilayahId"]);
        $("#peng-provinsi").change();
        $("#peng-provinsi").attr("disabled",true);
    }
});

var validatePage = function(index){
    var result = false;
    switch(index){
        case 1:
            if(!selectRup){
                $.notify(
                {
                    title : '<strong><h3>Peringatan</h3></strong>',
                    message : 'RUP harus dipilih.'
                }, 
                {
                    offset : { x : 70, y : 115},
                    type: 'danger'
                });
                //selectRup = false;
            }
            // $('input[name="paket\\.kode_anggaran"]').keyup(function(e)
            // {
            //     if (/[^0-9-]+/g.test(this.value))
            //     {
            //         // Filter non-digits from input value.
            //         this.value = this.value.replace(/[^0-9-]+/g, '');
            //     }
            // });
            $('input[name="paket\\.kode_anggaran"]').on("input", function(evt) {
                var self = $(this);
                self.val(self.val().replace(/[^a-zA-Z0-9./ -]/, ""));

            });
            result = selectRup;
        break;
        case 2:
            var sumberDana = $("#tblSumberDana").find("tr").length > 0;
            if(!sumberDana){
                $.notify(
                    {
                        title : '<strong><h3>Peringatan</h3></strong>',
                        message : 'Sumber Dana harus di isi.'
                    }, 
                    {
                        offset : { x : 70, y : 115},
                        type: 'danger'
                    });
            }
            result = $('#data-kldi [required]').valid() && sumberDana;
        break;
        case 3:
            result = $('#data-pp [required]').valid();
        break;
        case 4:
            var selectPPK = $('input[name="paket.id_ppk"]').val() != "";
            // console.log($('input[name="paket.id_ppk"]').val());
            if(selectPPK == false){
                $.notify(
                    {
                        title : '<strong><h3>Peringatan</h3></strong>',
                        message : 'PPK harus dicek dulu kemudian dipilih. Setelah itu bisa dilengkapi data yang kurang!'
                    },
                    {
                        offset : { x : 70, y : 115},
                        type: 'danger'
                    });
                result = false;
            }else{
                result = $('#data-ppk [required]').valid();
            }
        break;
        default:
            result = true;
        break;
    }
    return result;
}

$("body").on('click', '#btnCariRupPaket', function () {
    if( $.fn.dataTable.isDataTable('#tblRupPaket')){
        $("#tblRupPaket").dataTable().fnDestroy();
    }
    var parameter = "?year="+$("#rupYear").val()+"&keyword="+$("#rupQuerySearch").val()+"&option="+$("#rupOptionSearch").val();
    $("#tblRupPaket").dataTable({
        ajax: {
            url:$(this).data("action") + parameter,
            cache: true},
        order: [[ 1, "asc" ], [ 1, "asc" ]],
        columns: [
            {"searchable": true },
            {"searchable": true},
            {"searchable": false },
            {"searchable": false },
            {"searchable": false},
            {"searchable": false },
            {"searchable": false }
        ]
    });
});

$("body").on('click', '.rup-select-button', function (e) {
    selectRup = true;
    e.preventDefault();
    var btn = $(this);
    $(".rup-select-button").prop("class", "rup-select-button btn btn-primary btn-xs");
    btn.prop("class", "rup-select-button btn btn-warning btn-xs");
    $("#tahunAnggaran").val(btn.data("tahunanggaran"));
    $("#jenisInstansiNama").val(btn.data("jenisinstansi"));
    $("#jenisInstansiId").val(btn.data("jenisinstansiid"));
    $("#instansiId").val(btn.data("instansiid"));
    $("#instansiNama").val(btn.data("instansi"));
    $("#alamatSatker").val(btn.data("alamatsatker"));
    $("#namaSatker").val(btn.data("namasatker"));
    $("#idSatker").val(btn.data("idsatker"));
    $("#rupId").val(btn.data("id"));
    $("#rupIdh").val(btn.data("id"));
    $("#rupId2").val(btn.data("id"));
    $("#prpId").val(btn.data("prpid"));
    $("#kbpId").val(btn.data("kbpid"));
    $("textarea#nama-paket").val(btn.data("namarup"));
    var sumberDana = btn.data("sumberdana") != null ? btn.data("sumberdana").replace(/\s/g, '').split(",") : null;
    if(sumberDana){
        $("#tblSumberDana").html("");
        $.ajax({
            url: $("#tambahSumDana").data("action"),
            dataType: 'json',
            success: function (res) {
                for(var i=0; i < sumberDana.length; i++){
                    sumDanaId += 1;
                    var trSumber = $("<tr>").attr("id", "sum-dana-"+sumDanaId);
                    var tdInput = $("<td>")
                        .append($("<input>").attr("name", "paket.kode_anggaran").attr("class", "form-control").attr("required", "required"));
                    var tdSelect = $("<td>");
                    var tdDelete = $("<td>")
                        .append($("<button>").attr("class", "btn btn-xs btn-danger btn-delete-sumdana").attr("data-id", sumDanaId).attr("type", "button")
                            .append($("<span>").attr("class", "fa fa-trash")));
                    var select = $("<select></select>").attr("name", "paket.id_sumber_dana").attr("class", "form-control");

                    var selectedOne = false;
                    $.each(res,function(index,json){
                        var selected = json.nama_sumber_dana === sumberDana[i];
                        if(selected){
                            selectedOne = true;
                        }
                        select.append($("<option></option>").attr("value", json.id).attr("selected", selected).text(json.nama_sumber_dana));
                    });

                    tdSelect.append(select);
                    trSumber.append(tdSelect).append(tdInput).append(tdDelete);
                    if(selectedOne){
                        $("#tblSumberDana").append(trSumber);
                    }
                }

            }
        });
    }
    $.notify(
        {
            title : '<strong><h3>Info</h3></strong>',
            message : 'RUP (' + btn.data("namarup") + ') telah dipilih'
        }, 
        {
            offset : { x : 70, y : 115}
        });
});

$("body").on('click', '#btn-cari-ppk', function () {
    if( $.fn.dataTable.isDataTable('#tbl-ppk')){
        $("#tbl-ppk").dataTable().fnDestroy();
    }
    var parameter = "?keyword="+$("#keyword-ppk").val()+"&option="+$("#option-ppk").val();
    $("#tbl-ppk").dataTable({
        pageLength: 10,
        bFilter: false,
        ajax: {
            url: $(this).data("action") + parameter,
            cache: true},
        order: [[ 1, "asc" ], [ 1, "asc" ]],
        columns: [
            {"searchable": true },
            {"searchable": true},
            {"searchable": true },
            {"searchable": false }
        ]
    });
});

$("body").on('click', '.ppk-select-button', function (e) {
    $("#ppk_username").show();
    selectPpk = true;
    e.preventDefault();
    var btn = $(this);
    $(".ppk-select-button").prop("class", "ppk-select-button btn btn-primary btn-xs");
    btn.prop("class", "ppk-select-button btn btn-warning btn-xs")
    $("#id-ppk").val(btn.data("id"));

    $("#nip-ppk").val(btn.data("nip"));
    var vl = $("#nip-ppk").val().replace(/[^0-9-]+/g, '');
    $("#nip-ppk").val(vl);
    $("#username-ppk").val(btn.data("username"));
    $("#nama-ppk").val(btn.data("nama"));
    $("#nama-ppk-update").val(btn.data("nama"));
    $("#jabatan-ppk").val(btn.data("jabatan"));
    $("#email-ppk").val(btn.data("email"));
    $("#notelp-ppk").val(btn.data("notelp"));
    $("#pbj-ppk").val(btn.data("sertifikatpbj"));
});

$("body").on('click', '#btn-cari-ulp', function () {
    if( $.fn.dataTable.isDataTable('#tbl-ulp')){
        $("#tbl-ulp").dataTable().fnDestroy();
    }
    var parameter = "?keyword="+$("#keyword-ulp").val()+"&option="+$("#option-ulp").val();
    $("#tbl-ulp").dataTable({
        pageLength: 10,
        bFilter: false,
        ajax: {
            url:'@{DataTableCtr.cariUlp()}' + parameter,
            cache: true},
        order: [[ 1, "asc" ], [ 1, "asc" ]],
        columns: [
            {"searchable": true },
            {"searchable": true},
            {"searchable": true },
            {"searchable": false }
        ]
    });
});
var idx = 0;
$("body").on('click', '.ulp-select-button', function (e) {
    e.preventDefault();
    var btn = $(this);
    btn.prop("class", "ulp-select-button btn btn-warning btn-xs");
    var tblUlp = $("#tbl-anggota-ulp");
    var trAnggota = $("<tr>").attr("id", "anggota_ulp_" + idx)
        .append($("<td>").text($(this).data("nama"))
            .append($("<input>").attr("type", "hidden").attr("name", "paket.ulp_anggota_id").attr("value", $(this).data("id"))))
        .append($("<td>").text($(this).data("nip"))
            .append($("<input>").attr("type", "hidden").attr("name", "paket.ulp_anggota_nip").attr("value", $(this).data("nip"))))
        .append($("<td>")
            .append($("<button>").attr("type", "button").attr("class", "btn btn-sm btn-danger del-anggota-ulp").attr("data-id", idx)
                .append($("<i>").attr("class", "fa fa-trash"))));
    tblUlp.append(trAnggota);
    idx ++;
});

$("body").on('click', '.del-anggota-ulp', function (e) {
    e.preventDefault();
    var btn = $(this);
    var trId = btn.data("id");
    $("#anggota_ulp_" + trId).remove();
});

$("#tambahSumDana").on('click', function (e) {
    e.preventDefault();
    var loader = Ladda.create(document.querySelector('#tambahSumDana'));
    loader.start();
    $.ajax({
        url: $(this).data("action"),
        dataType: 'json',
        success: function (res) {
            sumDanaId += 1;
            var trSumber = $("<tr>").attr("id", "sum-dana-"+sumDanaId);
            var tdInput = $("<td>")
                .append($("<input>").attr("name", "paket.kode_anggaran").attr("class", "form-control kodesumberdana").attr("required", "required"));
            var tdSelect = $("<td>");
            var tdDelete = $("<td>")
                .append($("<button>").attr("class", "btn btn-xs btn-danger btn-delete-sumdana").attr("data-id", sumDanaId).attr("type", "button")
                    .append($("<span>").attr("class", "fa fa-trash")));
            var select = $("<select></select>").attr("name", "paket.id_sumber_dana").attr("class", "form-control");

            $.each(res,function(index,json){
                select.append($("<option></option>").attr("value", json.id).text(json.nama_sumber_dana));
            });

            tdSelect.append(select);
            trSumber.append(tdSelect).append(tdInput).append(tdDelete);
            $("#tblSumberDana").append(trSumber);
            loader.stop();

            /*contoh input yang hanya boleh angka, huruf, spasi, garis miring, strip, dan titik*/
            $('input[name="paket\\.kode_anggaran"]').on("input", function(evt) {
                var self = $(this);
                self.val(self.val().replace(/[^a-zA-Z0-9./ -]/, ""));

            });

            // $('input[name="paket\\.kode_anggaran"]').keyup(function(e)
            // {
            //     if (/^[0-9a-zA-Z/ -]+/g.test(this.value))
            //     {
            //         // Filter non-digits from input value.
            //         this.value = this.value.replace(/^[0-9a-zA-Z/ -]+/g, '');
            //     }
            // });

        }
    });

});

$("body").on('click', '.btn-delete-sumdana', function (e) {
    e.preventDefault();
    var id = $(this).data("id");
    $("#sum-dana-"+id).remove()
});

$("body").on('change', '.kuantitas', function () {
    var idx = $(this).data("index");
    var kuantitas = parseFloat($(this).val());
    var hargaSatuan = myParseFloat($(".harga-satuan_" + idx).val());
    var hargaOngkir = myParseFloat($("#harga-ongkir_" + idx).val());
    var totalHarga = (kuantitas * hargaSatuan) + hargaOngkir;

    $(".total-harga_" + idx).val(totalHarga);
    $(".kuantitas_" + idx).val(kuantitas);
    if($(".total-harga-konversi_" + idx)){
        var kursNilai = myParseFloat($(".kurs-nilai-tengah").val());
        var hargaTotalKonversi = totalHarga * kursNilai;
        $(".total-harga-konversi_" + idx).val(hargaTotalKonversi);
    }

    var produk_jumlah = parseInt($("#produk_jumlah").val());
    var grandTotal = 0;
    for (let index = 1; index <= produk_jumlah; index++) {

    let kuantitas = parseFloat($("#kuantitas_"+index).val());
    let hargaSatuan = myParseFloat($(".harga-satuan_" + index).val());
    let hargaOngkir = myParseFloat($("#harga-ongkir_" + index).val());
    let totalHarga = (kuantitas * hargaSatuan) + hargaOngkir ;
    if($(".total-harga-konversi_" + index).length>0){
        var kursNilai = myParseFloat($(".kurs-nilai-tengah").val());
        var hargaTotalKonversi = totalHarga * kursNilai;
        grandTotal+=hargaTotalKonversi;
    }
    else{
        grandTotal+=totalHarga;
    }
    }
    $("#total-harga-keseluruhan").html("<b>"+grandTotal+"</b>");
    $(".currency").convertThousand();
});

$("body").on('change', '.ongkos-kirim', function () {
    var idx = $(this).data("index");
    var hargaOngkir = myParseFloat($(this).val());
    var hargaSatuan = myParseFloat($(".harga-satuan_" + idx).val());
    var kuantitas = myParseFloat($("#kuantitas_"+ idx).val());
    var totalHarga = (kuantitas * hargaSatuan) + hargaOngkir;

    $(".total-harga_" + idx).val(totalHarga);
    $(".kuantitas_" + idx).val(kuantitas);
    if($(".total-harga-konversi_" + idx)){
        var kursNilai = myParseFloat($(".kurs-nilai-tengah").val());
        var hargaTotalKonversi = totalHarga * kursNilai;
        $(".total-harga-konversi_" + idx).val(hargaTotalKonversi);
    }

    var produk_jumlah = parseInt($("#produk_jumlah").val());
    var grandTotal = 0;
    for (let index = 1; index <= produk_jumlah; index++) {

    let kuantitas = parseFloat($("#kuantitas_"+index).val());
    let hargaSatuan = myParseFloat($(".harga-satuan_" + index).val());
    let hargaOngkir = myParseFloat($("#harga-ongkir_" + index).val());
    let totalHarga = (kuantitas * hargaSatuan) + hargaOngkir ;
    if($(".total-harga-konversi_" + index).length>0){
        var kursNilai = myParseFloat($(".kurs-nilai-tengah").val());
        var hargaTotalKonversi = totalHarga * kursNilai;
        grandTotal+=hargaTotalKonversi;
    }
    else{
        grandTotal+=totalHarga;
    }
    }

    $("#total-harga-keseluruhan").html("<b>"+grandTotal+"</b>");
    $(".currency").convertThousand();
});


$("body").on("change", "#sk-provinsi", function(){
    var skProv = $(this).val();
    $("#sk-kabupaten").empty();        
    $("#sk-kabupaten").append("<option value=''>Pilih</option>");
    if(skProv != ""){
        $.ajax({
            url: params.urlGetKabupaten,
            data: { provinsiId: skProv },
            dataType: 'json',
            success: function (results) {
                $(results).each(function(idx, record){
                    $("#sk-kabupaten").append("<option value='" + record.id + "'>" + record.nama_kabupaten + "</option>");
                });
            }
        });
    }
});

$("body").on("change", "#peng-provinsi", function(){
    var pengProv = $(this).val();
    $("#peng-kabupaten").empty();        
    $("#peng-kabupaten").append("<option value=''>Pilih</option>");
    if(pengProv != ""){
        $.ajax({
            url: params.urlGetKabupaten,
            data: { provinsiId: pengProv },
            dataType: 'json',
            success: function (results) {
                $(results).each(function(idx, record){
                    $("#peng-kabupaten").append("<option value='" + record.id + "' "+ ((params["kelasHarga"] == "kabupaten" && params["wilayahId"] != "") ? " selected " : "") +
                        ">" + record.nama_kabupaten + "</option>");
                });
                if(params["kelasHarga"] != "nasional") $(".select2-container").width("100%");
            }
        });
    }
});

$("body").on("change", "#peng-kabupaten", function(){
    var totalKeseluruhan = 0;
    if(params.perluOngkir){
        var pIds = $(".cart-list input[name='paket.produk_id']").map(function(){ return parseInt(this.value);});
        var wId = parseInt($(this).val());
        $('#data-peng-kabupaten').val(wId);
        var subTotal = 0;
        $(".cart-list").each(function(){
            dis = $(this);
            // console.log("dis = "+dis);
            // console.log("ongkir = "+$(this).children("input[name='ongkir']").val());
            var ongkirInput = $(this).children("input[name='ongkir']").val();
            if(ongkirInput === undefined){
            	    return;
            }
            var ongkir = JSON.parse($(this).children("input[name='ongkir']").val());
            //console.log(ongkir);
            var ok = ongkir.find(function(data){return data.kabupatenId == wId;});
            if (ok != undefined && ok.ongkir != undefined) {
                    $(this).find("input[name='paket.harga_ongkir']").val(ok.ongkir);
                    $(this).find("input[name='paket.harga_ongkir']").convertThousand();
                    var qty = parseFloat($(this).find("input[name='paket.produk_kuantitas']").val().replace(/[ ]*,[ ]*|[ ]+/g, ""));
                    var harga = parseFloat($(this).find("input[name='paket.produk_harga_satuan']").val().replace(/[ ]*,[ ]*|[ ]+/g, ""));
                    $(this).find("input[name='paket.harga_total']").val((qty * harga) + ok.ongkir);
                    $(this).find("input[name='paket.harga_total']").convertThousand();
                    subTotal = parseFloat($(this).find("input[name='paket.harga_total']").val().replace(/[ ]*,[ ]*|[ ]+/g, ""));
                    //console.log("subtotal dengan ongkir = "+subTotal);
            } else {
                subTotal = parseFloat($(this).find("input[name='paket.harga_total']").val().replace(/[ ]*,[ ]*|[ ]+/g, ""));
                //console.log("subtotal tanpa ongkir = "+subTotal);
            }
            //console.log("subtotal = "+subTotal);
            //console.log("total sebelum = "+totalKeseluruhan);
            totalKeseluruhan += subTotal;
            //console.log("total sesudah ditambah = "+totalKeseluruhan);
            subTotal = 0;
        });
    }
    $("#total-harga-keseluruhan").text(totalKeseluruhan);
    $("#total-harga-keseluruhan").convertThousand();
});

var thisYear = (new Date()).getFullYear();
var lastYear = thisYear-1;
var nextYear = thisYear+1;
$('#rupYear').append(new Option(lastYear, lastYear, false, false));
$('#rupYear').append(new Option(thisYear, thisYear, true , true ));
$('#rupYear').append(new Option(nextYear, nextYear, false, false));


$('input[name="paket\\.nip_pemesan"]').on("input keydown keyup mousedown mouseup select contextmenu drop change", function()
{
    if (/[^0-9-]+/g.test(this.value))
    {
        // Filter non-digits from input value.
        this.value = this.value.replace(/[^0-9-]+/g, '');
    }
});
$('input[name="paket\\.nip_ppk"]').on("input keydown keyup mousedown mouseup select contextmenu drop change", function()
{
    if (/[^0-9-]+/g.test(this.value))
    {
        // Filter non-digits from input value.
        this.value = this.value.replace(/[^0-9-]+/g, '');
    }
});
