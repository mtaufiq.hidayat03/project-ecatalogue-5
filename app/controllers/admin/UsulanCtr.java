package controllers.admin;

import controllers.BaseController;
import controllers.prakatalog.PenawaranCtr;
import controllers.security.AllowAccess;
import ext.Pageable;
import models.ServiceResult;
import models.aanwijzing.AanwijzingAnswer;
import models.aanwijzing.AanwijzingQuestion;
import models.common.AktifUser;
import models.common.AllLevelProdukKategori;
import models.common.DokumenInfo;
import models.common.JadwalUsulan;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.katalog.Komoditas;
import models.katalog.ProdukKategori;
import models.katalog.komoditasmodel.KomoditasTemplateJadwal;
import models.masterdata.Kldi;
import models.masterdata.Tahapan;
import models.penyedia.Penyedia;
import models.prakatalog.*;
import models.prakatalog.form.FormBukaPenawaran;
import models.prakatalog.search.UsulanSearch;
import models.prakatalog.search.UsulanSearchResult;
import models.secman.Acl;
import models.user.User;
import org.apache.commons.io.IOUtils;
import play.Logger;
import play.Play;
import play.i18n.Messages;
import repositories.AanwijzingRepository;
import repositories.UsulanRepository;
import repositories.produkkategori.ProdukKategoriRepository;
import services.prakatalog.UsulanService;
import services.produkkategori.ProdukKategoriService;
import utils.DateTimeUtils;
import utils.HtmlUtil;
import utils.LogUtil;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class UsulanCtr extends BaseController {

    public static final String TAG = "UsulanCtr";

    private static final String createUrl = "admin/UsulanCtr/create.html";
    private static final String detailUsulanUrl = "admin/UsulanCtr/detail.html";
    private static final String detailPengumumanUrl = "admin/UsulanCtr/detailPengumuman.html";

    private static final List<String> SIKAP_LABELS = Arrays.asList(
            Messages.get("izin_usaha.label"),
            Messages.get("akta.label"),
            Messages.get("pemilik.label"),
            Messages.get("pengurus.label"),
            Messages.get("tenaga_ahli.label"),
            Messages.get("peralatan.label"),
            Messages.get("pengalaman.label"),
            Messages.get("pajak.label")
    );

    @AllowAccess({Acl.COM_USULAN})
    public static void index() {
        UsulanSearch query = new UsulanSearch(params);

        User user = User.findById(AktifUser.getActiveUserId());
        Map<String, String> statusMaps = Usulan.statusMaps();
        List<Komoditas> commodities = new ArrayList<>();

        if (getAktifUser().isAdminLokalSektoral()) {
            commodities = Komoditas.findByKldi(getAktifUser().kldi_id);
        } else {
            commodities = Komoditas.getForDropDown();
        }

        UsulanSearchResult model = new UsulanSearchResult(query);

        if (query.urutkan == "") {
            query.urutkan = "8";
        }
        query.uid = user.id;
        model = UsulanRepository.searchDataUsulan(query);

        renderArgs.put("paramQuery", query);
        render(model, user, statusMaps, commodities);
    }

    //@AllowAccess({Acl.COM_USULAN})
    public static void listPendaftar(Long id, Long kid) {
        Usulan usulan = Usulan.findById(id);
        notFoundIfNull(usulan);
        //List<User> pesertaList = User.getAllPesertaUsulan(id);
        render(usulan, kid);
    }

    @AllowAccess({Acl.COM_USULAN_ADD})
    public static void create(Long komoditas_id) {
        Komoditas komoditas = Komoditas.findById(komoditas_id);
        String halaman = "Tambah Usulan";
        List<Kldi> kldiList = Kldi.findAll();
        List<KomoditasTemplateJadwal> templateJadwal = KomoditasTemplateJadwal.findByKomoditasId(komoditas_id);
        List<User> pokjaList = User.findPokjaList();
        boolean isCreateKomoditas = false;

        List<ProdukKategori> produkKategoriList = ProdukKategoriRepository.getProdukKategoriList(komoditas_id);
        List<AllLevelProdukKategori> kategoriList = ProdukKategoriService.generateSingleRowAllLevelKategori(produkKategoriList);
        List<Long> pokjaIdList = new ArrayList<Long>();
        Usulan usulan = new Usulan();
        List<UsulanPokja> listPokja = new ArrayList<>();
        usulan.pokja_ids = listPokja.stream().map(x -> x.pokja_id.longValue()).mapToLong(l -> l).toArray();
        render(usulan, komoditas, kldiList, pokjaIdList, templateJadwal, pokjaList, isCreateKomoditas, kategoriList, halaman);
    }

    @AllowAccess({Acl.COM_USULAN_ADD})
    public static void createSubmit(
            Usulan usulan,
            File file,
            File pokjafile,
            int[] file_id,
            int[] file_pokja_id,
            long[] kategori,
            long[] pokja
    ) throws Exception {
        Logger.info(">>> id usulan saat edit submit " + usulan.id);
        Logger.info(">>> kategori saat edit submit " + kategori);
        Logger.info(">>> pokja " + pokja.toString());
        Logger.info(">>> pokja " + kategori.toString());
        checkAuthenticity();
        validation.valid(usulan);
        Long idUsulan = usulan.id;
        if (validation.errors().size() > 0) {
            validation.errors().forEach(e -> {
                Logger.debug("error = %s", e);
            });
            params.flash();
            validation.keep();
            flash.error("Periksa kembali data yang anda inputkan!");
            create(usulan.komoditas_id);
        }
        usulan.pokja_ids = pokja;
        usulan.kategori_ids = kategori;
        usulan.status = Usulan.STATUS_BARU;
        usulan.active = true;
        KomoditasTemplateJadwal komoditasTemplateJadwal = KomoditasTemplateJadwal.findById(usulan.template_jadwal_id);
        komoditasTemplateJadwal.apakah_dipakai = true;
        komoditasTemplateJadwal.save();
        long usulan_id = new Long(0);
        if (usulan.id != null) {
            usulan_id = idUsulan;
            usulan.id = usulan_id;
            usulan.save();
        } else {
            usulan_id = usulan.save();
            usulan.id = usulan_id;
        }
        Logger.info("HHHHHHHHH usulan id HHHHHH " + usulan_id);
        usulan.savePokja();
        if (file_id != null && file_id.length > 0) {
            for (int id : file_id) {
                DokumenUsulan.updateIdLelangDokUsulan(id, usulan_id);
            }
        }

        if (file_pokja_id != null && file_pokja_id.length > 0) {
            for (int id : file_pokja_id) {
                DokumenUsulanPokja.updateIdLelangDokUsulan(id, usulan_id);
            }
        }

        if (kategori != null) {

            usulan.saveKategori();


        } else {
            for (long kategoriId : kategori) {
                UsulanKategoriProduk ukp = new UsulanKategoriProduk(usulan_id, kategoriId);
                ukp.save();
            }
        }
        flash.success("Usulan berhasil ditambahkan");
        KomoditasCtr.detail(usulan.komoditas_id);
    }

    @AllowAccess({Acl.COM_USULAN_EDIT})
    public static void edit(Long id, Long kid) {
        Logger.info("=============== usulan edit id ============ " + id);
        Usulan usulan = Usulan.findById(id);
        notFoundIfNull(usulan);
        usulan.withPokja();
        List<Long> pokjaIdList = Arrays.stream(usulan.pokja_ids).boxed().collect(Collectors.toList());
        usulan.withKategori();
        List<Long> kategoriIdList = Arrays.stream(usulan.kategori_ids).boxed().collect(Collectors.toList());
//		Set<Long> selectedCategories = UsulanKategoriProduk.getCategoryIdByUsulan(usulan.id);
        Komoditas komoditas = Komoditas.findById(kid);
        List<Kldi> kldiList = Kldi.findAll();
        List<KomoditasTemplateJadwal> templateJadwal = KomoditasTemplateJadwal.findByKomoditasId(kid);
        List<User> pokjaList = User.findPokjaList();
        String halaman = "Edit Usulan";

        List<ProdukKategori> produkKategoriList = ProdukKategoriRepository.getProdukKategoriList(kid);

        List<AllLevelProdukKategori> kategoriList = ProdukKategoriService.generateSingleRowAllLevelKategori(produkKategoriList);

        List<DokumenUsulan> dokumenUsulanList = DokumenUsulan.find("usulan_id = ?", id).fetch();

        List<DokumenUsulanPokja> dokumenPokjaList = DokumenUsulanPokja.find("usulan_id = ?", id).fetch();

        List<DokumenInfo> dokumenInfoList = DokumenInfo.getUploadInfoList(dokumenUsulanList);
        List<DokumenInfo> dokumenInfoPokjaList = DokumenInfo.getUploadUsulanPokjaList(dokumenPokjaList);
        render(createUrl, usulan, komoditas, kldiList, pokjaIdList, dokumenInfoList, dokumenInfoPokjaList, pokjaList, templateJadwal, kategoriList, halaman, kategoriIdList);
    }

    @AllowAccess({Acl.COM_USULAN_EDIT})
    public static void editSubmit(Long usulan_id, Usulan usulan, File file, int[] file_id, long[] pokja) {
        checkAuthenticity();
        validation.valid(usulan);
        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
            flash.error("Periksa kembali data yang anda inputkan!");
            edit(usulan.id, usulan.komoditas_id);
        } else {
            Usulan usulanDb = Usulan.findById(usulan_id);
            usulanDb.pokja_ids = pokja;
            usulanDb.setDataForm(usulan);
            usulanDb.save();
            usulan.savePokja();

            if (file_id != null && file_id.length > 0) {
                for (int id : file_id) {
                    DokumenUsulan.updateIdLelangDokUsulan(id, usulanDb.id);
                }
            }
        }

        flash.success("Usulan berhasil disimpan");
        KomoditasCtr.detail(usulan.komoditas_id);
    }

    @AllowAccess({Acl.COM_USULAN_DELETE})
    public static void delete(Long id, Long komoditas_id) {
        Usulan model = Usulan.findById(id);
        notFoundIfNull(model);
        if (model.isAllowedToBeDeleted()) {
            model.softDelete();
            flash.success(Messages.get("notif.success.delete"));
        } else {
            flash.error(Messages.get("notif.failed.delete"));
        }
        KomoditasCtr.detail(model.komoditas_id);
    }

    @AllowAccess({Acl.COM_USULAN})
    public static void tambahJadwal() {
        render("pl/jadwal-usulan.html");
    }

    @AllowAccess({Acl.COM_USULAN, Acl.COM_USULAN_ADD, Acl.COM_USULAN_EDIT})
    public static void upload(File file, Long usulan_id, String dok_label) {
        LogUtil.d(TAG, "usulan id: " + usulan_id);
        checkAuthenticity();
        Map<String, Object> result = new HashMap<String, Object>(1);
        try {
            DokumenInfo dokumenInfo = DokumenUsulan.simpanDokUsulan(usulan_id, file, dok_label);
            List<DokumenInfo> files = new ArrayList<DokumenInfo>();
            files.add(dokumenInfo);
            result.put("files", files);
        } catch (Exception e) {
            result.put("errorx", "upload failed");
            e.printStackTrace();

        }
        if (result.get("files") == null) {
            result.clear();
            result.put("errorx", "upload failed");
        }

        renderJSON(result);
    }

    @AllowAccess({Acl.COM_USULAN, Acl.COM_USULAN_ADD, Acl.COM_USULAN_EDIT})
    public static void uploadPokja(File pokjafile, Long usulan_id, String dok_label) {
        LogUtil.d(TAG, "usulan id: " + usulan_id);
        LogUtil.d(TAG, "file = " + pokjafile);
        checkAuthenticity();
        Map<String, Object> result = new HashMap<String, Object>(1);
        try {
            DokumenInfo dokumenInfo = DokumenUsulanPokja.simpanDokUsulan(usulan_id, pokjafile, dok_label);
            List<DokumenInfo> files = new ArrayList<DokumenInfo>();
            files.add(dokumenInfo);
            result.put("files", files);
        } catch (Exception e) {
            result.put("errorx", "upload failed");
            e.printStackTrace();

        }
        if (result.get("files") == null) {
            result.clear();
            result.put("errorx", "upload failed");
        }

        renderJSON(result);
    }

//	@AllowAccess({Acl.COM_USULAN, Acl.COM_USULAN_ADD, Acl.COM_USULAN_EDIT})
//	public static void hapusDoc(Long fileId, Integer versi) throws Exception{
//		checkAuthenticity();
//		BlobTable blobTable = BlobTable.findById(fileId, versi);
//		if(blobTable != null){
//			blobTable.delete();
//		}
//	}

    @AllowAccess({Acl.COM_USULAN, Acl.COM_USULAN_ADD, Acl.COM_USULAN_EDIT})
    public static void hapusDoc(Long docId) throws Exception {
        checkAuthenticity();

        DokumenUsulan docU = DokumenUsulan.findByDokId(docId);
        BlobTable blobTable = BlobTable.findByBlobId(docU.dok_id_attachment);//BlobTable.findById(docU.dok_id_attachment, versi);
        if (blobTable != null) {
            blobTable.delete();
        }
        if (docU != null) {
            docU.delete();
        }
    }

    @AllowAccess({Acl.COM_USULAN})
    public static void detail(long id) throws ParseException {
        allowToAccessMenuUsulan(id);
        Usulan usulan = Usulan.getDetail(id);
        usulan.withPokja();
        List<DokumenInfo> dokumenUsulanList = new ArrayList<>();
        FormBukaPenawaran form = new FormBukaPenawaran();
        List<UsulanDokumenPenawaran> dokumenList = new ArrayList<UsulanDokumenPenawaran>();
        List<UsulanKualifikasi> kualifikasiList = new ArrayList<UsulanKualifikasi>();
        AktifUser aktifUser = getAktifUser();
        List<AgendaKegiatan> agendaList = new ArrayList<>();
        List<User> listPokja = new ArrayList<>();
        long jumlahPenawaran = 0L;
        long jumlahPendaftar = 0L;
        if (usulan != null) {
            dokumenUsulanList = DokumenInfo.listDokumenUsulan(usulan.id);
            Komoditas komoditas = Komoditas.findById(usulan.komoditas_id);
            agendaList = AgendaKegiatan.findByUsulan(id);

            form = UsulanService.getButtonPenawaran(aktifUser, usulan, komoditas);
            showInvalidPenawaran(form, aktifUser, usulan);

            if (!usulan.status.equals(Usulan.STATUS_BARU)) {
                dokumenList = UsulanDokumenPenawaran.find("usulan_id = ?", usulan.id).fetch();
                kualifikasiList = UsulanKualifikasi.find("usulan_id = ?", usulan.id).fetch();
                jumlahPenawaran = Penawaran.getJumlahPenawaran(usulan.id);
                jumlahPendaftar = PesertaUsulan.getJumlahPendaftar(usulan.id);
            }

            if (null != usulan.pokja_ids && usulan.pokja_ids.length > 0) {
                String[] idx = new String[usulan.pokja_ids.length];
                for (int a = 0; a < idx.length; a++) {
                    idx[a] = String.valueOf(usulan.pokja_ids[a]);
                }
                String pokja_id = (String) String.join(", ", idx);
                listPokja = User.findPokjaListByIdPokja(pokja_id);
            }
        }

        render(usulan, form, dokumenUsulanList, dokumenList, kualifikasiList, jumlahPenawaran, jumlahPendaftar, aktifUser, agendaList, listPokja);
    }

    public static void detailPengumuman(long id) throws ParseException {
        Usulan usulan = Usulan.findById(id);
        List<DokumenInfo> pengadaanList = new ArrayList<>();
        FormBukaPenawaran form = new FormBukaPenawaran();
        long jumlahPenawaran = 0L;
        AktifUser aktifUser = getAktifUser();
        boolean isAllowedToRegister = getAktifUser() != null && getAktifUser().isAuthorized("comPenawaranDaftar");
        boolean isAssignPokja = false;
        if (usulan != null) {
            pengadaanList = DokumenInfo.listDokumenPengadaan(usulan.id);

            Komoditas komoditas = Komoditas.findById(usulan.komoditas_id);
            usulan.komoditas = komoditas.nama_komoditas;

            form = UsulanService.getButtonPenawaran(aktifUser, usulan, komoditas);
            showInvalidPenawaran(form, aktifUser, usulan);

            if (!usulan.status.equals(Usulan.STATUS_BARU)) {
                jumlahPenawaran = Penawaran.getJumlahPenawaran(usulan.id);
            }
            UsulanPokja usulanPokja = UsulanPokja.getAssignPokjaByUsulanId(usulan.id);
            if (usulanPokja != null) {
                if (aktifUser.user_id.equals(usulanPokja.pokja_id)) {
                    isAssignPokja = true;
                }
            }
        }
        render(usulan, form, pengadaanList, jumlahPenawaran, aktifUser, isAllowedToRegister, isAssignPokja);
    }

    private static boolean showInvalidPenawaran(FormBukaPenawaran form, AktifUser aktifUser, Usulan usulan) {
        if (!usulan.isPenawaranDibuka()
                || !form.allowToAddPenawaran
                || aktifUser == null
                || !aktifUser.allowProviderOrDistributor()) {
            return false;
        }
        params.flash();
        if (!form.isAjukanPenawaran) {
            LogUtil.debug(TAG, "show invalid penawaran date");
            Tahapan tahapan = Tahapan.getFlaggingTahapanPenawaran();
            UsulanJadwal usulanJadwal = UsulanJadwal.getBy(tahapan.id, usulan.id);
            flash.error(Messages.get("jadwal_pemasukan_penawaran.label") + " : "
                    + DateTimeUtils.formatDateToString(usulanJadwal.tanggal_mulai,"dd-MM-yyyy") + " "
                    + Messages.get("sampai_dengan.label") + " "
                    + DateTimeUtils.formatDateToString(usulanJadwal.tanggal_selesai,"dd-MM-yyyy"));
            return true;
        }
        if (form.isLelang && !form.isAjukanPenawaran) {
            // ada request notif selalu muncul, hanya di provide ketika penyedia dan atau distributor
            flash.error(Messages.get("button.penawaran.penyedia.gagalbuka.message"));
            return true;
        }
        if (form.isPenawaranExist && form.isTerdaftar) {
            LogUtil.debug(TAG, "already submit offering");
            flash.error(Messages.get("button.penawaran.penawaran.exist.message"));
        }
        return true;
    }

    @AllowAccess({Acl.COM_PENAWARAN_BUKA})
    public static void bukaPenawaran(long id) {
        allowToAccessMenuUsulan(id);
        Usulan usulan = Usulan.findById(id);
        notFoundIfNull(usulan);
        if (usulan.isOpen()) {
            notFound();
        }
        Komoditas komoditas = Komoditas.findById(usulan.komoditas_id);
        String informasi = Messages.get("informasi.pilih_penyedia.buka_penawaran");

        FormBukaPenawaran form = new FormBukaPenawaran();
        form.jadwalUsulanList = JadwalUsulan.getJadwalUsulan(usulan);
        int komoditasTipeDatafeed = Komoditas.KOMODITAS_DATAFEED;
        List<UsulanDokumenPengadaan> usulanDokumenPengadaans = UsulanDokumenPengadaan.findByUsulan(usulan.id);
        List<UsulanDokumenPenawaran> usulanDokumenPenawarans = UsulanDokumenPenawaran.findBy(usulan.id);
        Set<String> usulanKualifikasis = UsulanKualifikasi.findBy(usulan.id)
                .stream()
                .map(u -> u.jenis_kualifikasi_alias)
                .collect(Collectors.toSet());
        LogUtil.debug(TAG, usulanDokumenPengadaans);
        LogUtil.debug(TAG, usulanDokumenPenawarans);
        if (komoditas.komoditas_kategori_id != komoditasTipeDatafeed && komoditas.prakatalog == 0) {
            form.isLelang = true;
        }

        Logger.info("is lelang nya " + form.isLelang);
        renderArgs.put("sikapLabels", SIKAP_LABELS);
        render(form, usulan, komoditas,
                komoditasTipeDatafeed, informasi, usulanDokumenPengadaans,
                usulanDokumenPenawarans, usulanKualifikasis);
    }

    @AllowAccess({Acl.COM_PENAWARAN, Acl.COM_PENAWARAN_BUKA})
    public static void bukaPenawaranSubmit(FormBukaPenawaran form, Long[] penyedia, Long[] pengadaan) throws ParseException {
        checkAuthenticity();
        List<String> dokumenList = Arrays.asList(form.dokumen_arr);

        if (UsulanCtr.bukaPenawaranValidation(form, penyedia, pengadaan)) {

            JadwalUsulan.simpanJadwal(form);
            UsulanDokumenPenawaran.simpanUsulanDokumenPenawaran(form);
            UsulanKualifikasi.simpanUsulanKualifikasi(form);
            UsulanDokumenPengadaan.activateDokumenPengadaan(form.usulan_id, pengadaan);

            Usulan usulan = Usulan.findById(form.usulan_id);
            usulan.status = Usulan.STATUS_PENAWARAN_DIBUKA;
            usulan.judul_pengumuman = form.judul;
            usulan.active = true;
            usulan.save();

            if (form.isLelang) {
                for (Long providerId : penyedia) {
                    Penyedia provider = Penyedia.findById(providerId);
                    UsulanPenyedia up = new UsulanPenyedia(form.usulan_id, providerId, provider.user_id);
                    up.save();
                }
            }

            detail(form.usulan_id);

        }

        Usulan usulan = Usulan.getDetail(form.usulan_id);
        Komoditas komoditas = Komoditas.findById(usulan.komoditas_id);
        String informasi = Messages.get("informasi.pilih_penyedia.buka_penawaran");
        int komoditasTipeDatafeed = Komoditas.KOMODITAS_DATAFEED;

        render("admin/UsulanCtr/bukaPenawaran.html",
                form, usulan, komoditas, komoditasTipeDatafeed,
                informasi, dokumenList, pengadaan);

    }

    @AllowAccess({Acl.COM_PENAWARAN, Acl.COM_PENAWARAN_BUKA})
    public static void automatedSavingUsulan(FormBukaPenawaran form, Long[] penyedia, Long[] pengadaan) {
        LogUtil.debug(TAG, pengadaan);
        LogUtil.debug(TAG, penyedia);
        LogUtil.debug(TAG, form);
        JadwalUsulan.simpanJadwal(form);
        UsulanDokumenPenawaran.simpanUsulanDokumenPenawaran(form);
        UsulanKualifikasi.simpanUsulanKualifikasi(form);
        UsulanDokumenPengadaan.activateDokumenPengadaan(form.usulan_id, pengadaan);

        Usulan usulan = Usulan.findById(form.usulan_id);
        usulan.status = Usulan.STATUS_DRAFT;
        usulan.judul_pengumuman = form.judul;
        usulan.active = true;
        usulan.save();

        if (form.isLelang) {
            for (Long providerId : penyedia) {
                Penyedia provider = Penyedia.findById(providerId);
                UsulanPenyedia up = new UsulanPenyedia(form.usulan_id, providerId, provider.user_id);
                up.save();
            }
        }

        renderJSON(new ServiceResult<>(true, "success", usulan));

    }

    private static boolean bukaPenawaranValidation(FormBukaPenawaran form, Long[] penyedia, Long[] pengadaan) {
        Usulan usulan = Usulan.findById(form.usulan_id);
        if (usulan.status.equalsIgnoreCase(Usulan.STATUS_PENAWARAN_DIBUKA)) {
            flash.error("Buat Pengumuman Gagal Dilakukan. Usulan telah Diumumkan.");
            redirect("admin.UsulanCtr.index");
        }

        if (form.judul.isEmpty()) {
            flash.error("Buat Pengumuman Gagal Dilakukan. Mohon isi Judul Pengumuman.");
            return false;
        }

        if (form.kualifikasi_sikap == null) {
            flash.error("Buat Pengumuman Gagal Dilakukan. Pilih minimal satu data SIKaP yang perlu diverifikasi.");
            return false;
        }

        if (form.dokumen_arr == null || form.dokumen_arr.length < 1) {
            flash.error("Buat Pengumuman Gagal Dilakukan. Tambahkan minimal satu dokumen yang harus di-upload oleh Penyedia.");
            return false;
        }

        if (pengadaan.length < 1) {
            flash.error("Buat Pengumuman Gagal Dilakukan. Mohon unggah Dokumen Pengadaan terlebih dahulu.");
            return false;
        }

        if (form.isLelang && (penyedia == null || penyedia.length < 1)) {
            flash.error("Anda belum memilih penyedia yang menang di lelang SPSE.");
            return false;
        }

        return validateJadwalUsulan(form);

    }

    private static Boolean validateJadwalUsulan(FormBukaPenawaran form) {
        for (JadwalUsulan jadwalUsulan : form.jadwalUsulanList) {

            UsulanJadwal usulanJadwal = new UsulanJadwal();
            usulanJadwal.tahapan_id = jadwalUsulan.tahapan_id;
            usulanJadwal.usulan_id = form.usulan_id;
            usulanJadwal.tanggal_mulai = JadwalUsulan.convertStringToDate(jadwalUsulan.tanggal_mulai);
            usulanJadwal.tanggal_selesai = JadwalUsulan.convertStringToDate(jadwalUsulan.tanggal_selesai);

            validation.valid(usulanJadwal);
            if (validation.hasErrors()) {
                validation.keep();
                params.flash();
                flash.error("Buat Pengumuman Gagal Dilakukan. Silakan isi informasi jadwal dengan lengkap.");
                return false;
            }
        }
        return true;
    }

    @AllowAccess({Acl.COM_USULAN_UBAH_JADWAL})
    public static void editJadwal(long uId, long tId) {
        Usulan usulan = Usulan.findById(uId);
        Tahapan tahapan = Tahapan.findById(tId);
        JadwalUsulan jadwal = JadwalUsulan.getSingleJadwalUsulan(usulan, tahapan);

        render(usulan, tahapan, jadwal);
    }

    @AllowAccess({Acl.COM_USULAN_UBAH_JADWAL})
    public static void editAllJadwal(long uId) {
        Usulan usulan = Usulan.findById(uId);
        List<UsulanJadwal> listJadwal = UsulanJadwal.listJadwal(uId);

        render(usulan, listJadwal);
    }

    @AllowAccess({Acl.COM_USULAN_UBAH_JADWAL})
    public static void editAllJadwalSubmit(FormBukaPenawaran form) throws ParseException {

        JadwalUsulan.updateAllJadwal(form);

        detail(form.usulan_id);
    }

    @AllowAccess({Acl.COM_USULAN_UBAH_JADWAL})
    public static void editJadwalSubmit(long jadwalId, String tanggalMulai, String tanggalSelesai, String keteranganPerubahanJadwal) throws ParseException {
        checkAuthenticity();
        UsulanJadwal usulanJadwal = UsulanJadwal.findById(jadwalId);

        if (keteranganPerubahanJadwal.isEmpty()) {
            params.flash();
            flash.error(Messages.get("katalog.usulan.ubah_jadwal.keterangan_kosong.error"));

            Usulan usulan = Usulan.findById(usulanJadwal.usulan_id);
            Tahapan tahapan = Tahapan.findById(usulanJadwal.tahapan_id);
            JadwalUsulan jadwal = new JadwalUsulan();
            jadwal.id = jadwalId;
            jadwal.tanggal_mulai = tanggalMulai;
            jadwal.tanggal_selesai = tanggalSelesai;

            render("admin/usulanCtr/editJadwal.html", usulan, tahapan, jadwal);
        } else {
            try {

                //save to riwayat
                UsulanJadwalRiwayat ujr = new UsulanJadwalRiwayat()
                        .setUser(getAktifUser().user_id)
                        .setFromUsulanJadwal(usulanJadwal)
                        .setNote(keteranganPerubahanJadwal);

                ujr.save();

                //update jadwal
                usulanJadwal.tanggal_mulai = DateTimeUtils.formatStringToDate(tanggalMulai);
                usulanJadwal.tanggal_selesai = DateTimeUtils.formatStringToDate(tanggalSelesai);

                usulanJadwal.save();


            } catch (Exception e) {
                e.printStackTrace();
            }

            detail(usulanJadwal.usulan_id);
        }

    }

    public static void riwayatPerubahanJadwal(long uId, long tId) {
        List<UsulanJadwalRiwayat> riwayatList = UsulanJadwalRiwayat.findByUsulanAndTahapan(uId, tId);
        Tahapan tahapan = Tahapan.findById(tId);
        String namaTahapan = "";
        if (tahapan != null) {
            namaTahapan = tahapan.nama_tahapan_alias;
        }
        render(riwayatList, uId, namaTahapan);
    }

    @AllowAccess({Acl.COM_PENAWARAN, Acl.COM_PENAWARAN_BUKA})
    public static void uploadDokumenPengadaan(File file, Long usulanId) {
        Map<String, Object> result = new HashMap<>(1);
        try {
            DokumenInfo dokumenInfo = UsulanDokumenPengadaan.simpanPengadaan(file, usulanId);
            List<DokumenInfo> files = new ArrayList<>();
            files.add(dokumenInfo);
            result.put("files", files);
        } catch (Exception e) {
            result.put("errorx", "upload failed");
            e.printStackTrace();

        }
        if (result.get("files") == null) {
            result.clear();
            result.put("errorx", "upload failed");
        }

        renderJSON(result);
    }

    @AllowAccess({Acl.COM_PENAWARAN, Acl.COM_PENAWARAN_BUKA})
    public static void removeDokumenPengadaan(Long btId) {
        BlobTable blobTable = BlobTable.findByBlobId(btId);
        UsulanDokumenPengadaan usulanDok = UsulanDokumenPengadaan.findById(blobTable.blb_id_content);
//		UsulanDokumenPengadaan udp = UsulanDokumenPengadaan.find("dok_id_attachment = ?",blobTable.blb_id_content).first();
        int result = 0;
        if (usulanDok != null) {
            result = usulanDok.delete();
        }
        if (blobTable != null) {
            blobTable.preDelete();
            blobTable.delete();
        }
//		int result = udp.delete();
        renderJSON(result);
    }

    @AllowAccess({Acl.COM_USULAN_AGENDA_KEGIATAN_ADD})
    public static void addAgenda(Long usulanId) {
        Usulan usulan = Usulan.findById(usulanId);
        render("admin/UsulanCtr/createAgenda.html", usulan);
    }

    @AllowAccess({Acl.COM_USULAN_AGENDA_KEGIATAN_EDIT})
    public static void editAgenda(Long agendaId) {
        AgendaKegiatan agendaKegiatan = AgendaKegiatan.findById(agendaId);
        Usulan usulan = Usulan.findById(agendaKegiatan.usulan_id);
        DateFormat format_tanggal = new SimpleDateFormat("dd-MM-yyyy");
        String tanggal_kegiatan = format_tanggal.format(agendaKegiatan.tanggal_kegiatan);
        render("admin/UsulanCtr/editAgenda.html", agendaKegiatan, usulan, tanggal_kegiatan);
    }

    @AllowAccess({Acl.COM_USULAN_AGENDA_KEGIATAN_EDIT})
    public static void removeDokumenAgenda(Long agId, Long btId) {
        BlobTable blobTable = BlobTable.findByBlobId(btId);
        if (blobTable != null) {
            blobTable.preDelete();
            blobTable.delete();
        }
        int rmDocument = AgendaKegiatan.removeDocument(agId);
        renderJSON(rmDocument);
    }

    @AllowAccess({Acl.COM_USULAN_AGENDA_KEGIATAN_ADD, Acl.COM_USULAN_AGENDA_KEGIATAN_EDIT})
    public static void saveAgenda(File file_lampiran, Long usulan_id, String tanggal, String jam, String judul,
                                  String lokasi, String isi_konten, Long agenda_id) {
        try {

            Date tanggal_format = new SimpleDateFormat("dd-MM-yyyy").parse(tanggal);
            String jam2 = jam.replace(".", ":");
            Date jam_format = new SimpleDateFormat("kk:mm").parse(jam2);

            AgendaKegiatan agendaKegiatan = new AgendaKegiatan();

            if (agenda_id != null) {
                agendaKegiatan = AgendaKegiatan.findById(agenda_id);
            }

            if (file_lampiran != null) {
                BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file_lampiran, agendaKegiatan.blb_id_content, AgendaKegiatan.class.getCanonicalName());
                agendaKegiatan.blb_id_content = blob.id;
                agendaKegiatan.nama_file = file_lampiran.getName();
            }

            agendaKegiatan.usulan_id = usulan_id;
            agendaKegiatan.tanggal_kegiatan = tanggal_format;
            agendaKegiatan.waktu_kegiatan = jam_format;
            agendaKegiatan.judul_kegiatan = judul;
            agendaKegiatan.lokasi = lokasi;
            agendaKegiatan.keterangan = isi_konten;
            agendaKegiatan.active = true;
            agendaKegiatan.save();
            flash.success(Messages.get("notif.success.submit"));
            detail(usulan_id);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AllowAccess({Acl.COM_USULAN_REVIEW})
    public static void reviewUsulan(Long usulan_id) {
        allowToAccessMenuUsulan(usulan_id);
        Usulan usulan = Usulan.findById(usulan_id);
        render(usulan);
    }

    @AllowAccess({Acl.COM_USULAN_REVIEW})
    public static void submitReviewUsulan(Long usulan_id, Long file_id, String dok_label, String from_page) {
        BlobTable blob = BlobTable.findByBlobId(file_id);//file_id blob id cek uploadDokReview
        blob.postLoad();
        Usulan usulan = Usulan.findById(usulan_id);
        usulan.doc_review_id = file_id;
        usulan.doc_review = dok_label + " " + blob.blb_nama_file;
        usulan.save();

        //edit by sudi
        if (from_page.contains("penawaran")) { //jika akses usulan dari penawaran
            PenawaranCtr.index();
        } else { //jika akses usulan dari usulan
            index();
        }
    }

    @AllowAccess({Acl.COM_USULAN_REVIEW})
    public static void DetailReviewUsulan() {
        render();
    }

    @AllowAccess({Acl.COM_USULAN_REVIEW})
    public static void uploadDokReview(File file, Long usulan_id) {
        LogUtil.d(TAG, "usulan id: " + usulan_id);
        checkAuthenticity();
        Map<String, Object> result = new HashMap<String, Object>(1);
        try {
            BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, usulan_id, Usulan.class.getSimpleName());
            DokumenInfo dokumenInfo = new DokumenInfo(blob);
            List<DokumenInfo> files = new ArrayList<DokumenInfo>();
            files.add(dokumenInfo);
            result.put("files", files);
        } catch (Exception e) {
            result.put("errorx", "upload failed");
            e.printStackTrace();

        }
        if (result.get("files") == null) {
            result.clear();
            result.put("errorx", "upload failed");
        }

        renderJSON(result);
    }

    @AllowAccess({Acl.COM_USULAN_REVIEW})
    public static void hapusDocReview(Long id) throws Exception {

        BlobTable blobTable = BlobTable.findByBlobId(id);
        if (blobTable != null) {
            blobTable.delete();
        }
    }

    public static void dataSrc(Long usulan_id) {
        renderJSON(UsulanDokumenPerpanjangan.dataSrc(usulan_id));
    }

    public static void goToList(Long usulan_id) {
        render("admin/UsulanCtr/listDokPerpanjangan.html", usulan_id);
    }

    public static void createUsulanDokPerp() {
        UsulanDokumenPerpanjangan udp = new UsulanDokumenPerpanjangan();
        List<Usulan> usulan = Usulan.findAll();
        udp.active = true;
        render("admin/UsulanCtr/createDokPerpanjangan.html", udp, usulan);
    }

    public static void saveUsulanDokPerp(UsulanDokumenPerpanjangan udp) {
        params.flash();
        try {
            if (udp.id != null) {
                UsulanDokumenPerpanjangan prObj = UsulanDokumenPerpanjangan.findById(udp.id);
                prObj.usulan_id = udp.usulan_id;
                prObj.dok_label = udp.dok_label;
                prObj.deskripsi = udp.deskripsi;
                prObj.active = udp.active;
                prObj.save();
                flash.success(Messages.get("notif.success.submit"));
            } else {
                udp.save();
                flash.success(Messages.get("notif.success.submit"));
            }
        } catch (Exception e) {
            flash.error(Messages.get("notif.failed.submit"));
        }
        goToList(udp.usulan_id);
    }

    public static void editUsulanDokPerp(Long id) {
        List<Usulan> usulan = Usulan.findAll();
        UsulanDokumenPerpanjangan udp = UsulanDokumenPerpanjangan.findById(id);
        render("admin/UsulanCtr/createDokPerpanjangan.html", udp, usulan);
    }

    public static void deleteUsulanDokPerp(Long id) {
        List<Usulan> usulan = Usulan.findAll();
        UsulanDokumenPerpanjangan udp = UsulanDokumenPerpanjangan.findById(id);
        udp.active = false;//softdelete
        udp.save();
        render("admin/UsulanCtr/listDokPerpanjangan.html");
    }

    public static boolean allowToAccessMenuUsulan(long id) {
        Usulan usulan = Usulan.findById(id);
        Boolean status = false;

        if (usulan != null) {
            if (getAktifUser().isAdminLokalSektoral()) {
                if (getAktifUser().kldi_id.equalsIgnoreCase(usulan.kldi_id)) {
                    status = true;
                }
            } else if (AktifUser.getAktifUser().isPokja()) {
                UsulanPokja usulanPokja = UsulanPokja.getByUsulanPokja(id, getAktifUser().user_id);
                if (usulanPokja != null) {
                    status = true;
                }
            } else {
                status = true;
            }
        }

        if (!status) {
            index();
        }

        return status;
    }

    @AllowAccess({Acl.COM_AANWIJZING})
    public static void aanwijzing(Integer produk_id) throws ParseException {
        Usulan usulan = Usulan.findById(produk_id);
        AktifUser aktifUser = getAktifUser();
        Boolean isTerdaftar = PesertaUsulan.isUserRegistedUsulan(aktifUser.user_id, usulan.id);
        String search = "";
        String searchUrl = "";
        if (params.get("search") != null && !params.get("search").equals("")) {
            searchUrl = "&search="+params.get("search");
            search = params.get("search");
        }
        Tahapan tahapanPenjelasan = Tahapan.getFlaggingTahapanPenjelasan();
        UsulanJadwal usulanJadwalPenjelasan = UsulanJadwal.getDateFlaggingTahapanPenjelasan(tahapanPenjelasan.id, (long) produk_id);
        if (usulanJadwalPenjelasan  != null) {
            if (UsulanService.isExplanation(usulan) && isTerdaftar || getAktifUser().isAdmin() || UsulanService.isExplanation(usulan) && getAktifUser().isPokja()) {
                User user = User.findById(AktifUser.getActiveUserId());
                int max_konten = 10;
                int page = null != params.get("offset") ? params.get("offset", Integer.class) : 1;
                int start = page * max_konten - max_konten;
                List<AanwijzingQuestion> questionAll = AanwijzingRepository.findAllWithSearch(produk_id, search);
                Pageable p = new Pageable(page, max_konten, questionAll.size(), ""+ "?produk_id=" + produk_id + searchUrl);
                renderArgs.put("user", user);
                renderArgs.put("produk_id", produk_id);
                renderArgs.put("p", p);
                renderArgs.put("usulan", usulan);
                renderArgs.put("search", search);
                renderArgs.put("aanwijzingLatest", AanwijzingRepository.findLastesAanwijzing(produk_id));
                renderArgs.put("aanwijzingQuestion", AanwijzingRepository.getDataAanwijzingQuestion(start, max_konten, produk_id, search));
                render("admin/UsulanCtr/aanwijzing/index.html");
            }
        }
        detailPengumuman((long) produk_id);
    }

    @AllowAccess({Acl.COM_INPUT_QUESTION_AANWIJZING})
    public static void createQuestion(String judul_pertanyaan, Integer produk_id, String pertanyaan, File file) throws ParseException {
        try {
            if(file != null) {
                if(file.length() <= 1048576) {
                    AanwijzingQuestion.createAanwijzingQuestion(judul_pertanyaan, produk_id, pertanyaan, file);
                    flash.success(Messages.get("notif.success.submit"));
                } else {
                    flash.error(Messages.get("ukuran_file_upload_image.label")+" 1 MB");
                }
            } else {
                AanwijzingQuestion.createAanwijzingQuestion(judul_pertanyaan, produk_id, pertanyaan, file);
                flash.success(Messages.get("notif.success.submit"));
            }
        } catch (Exception e) {
            flash.error(Messages.get("notif.failed.submit"));
            LogUtil.d("Error : ", e.getMessage());
            e.printStackTrace();
        }
        redirect("/admin.usulanctr/aanwijzing?produk_id="+produk_id);
       // aanwijzing(produk_id);
    }

    @AllowAccess({Acl.COM_EDIT_QUESTION_AANWIJZING})
    public static void editQuestion(String editjudul_pertanyaan, Integer editproduk_id,
                                    String editpertanyaan, Integer editid) throws ParseException {
        try {
            AanwijzingQuestion.editAanwijzingQuestion(editjudul_pertanyaan, editpertanyaan, editid);
            flash.success(Messages.get("notif.success.update"));
        } catch (Exception e) {
            flash.error(Messages.get("notif.failed.update"));
            LogUtil.d("Error : ", e.getMessage());
            e.printStackTrace();
        }
        aanwijzing(editproduk_id);
    }

    @AllowAccess({Acl.COM_DELETE_QUESTION_AANWIJZING})
    public static void deleteQuestion(Long id, Integer produk_id) throws ParseException {
        try {
            AanwijzingQuestion.deleteAanwijzingQuestion(id);
            flash.success(Messages.get("notif.success.delete"));
        } catch (Exception e) {
            flash.error(Messages.get("notif.failed.delete"));
        }
        aanwijzing(produk_id);
    }

    public static String getRandom() {
        String SALTCHARS = "abcdefghijklmnopqrstuvwxyz1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 5) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    @AllowAccess({Acl.COM_AANWIJZING_EXPORT_PDF})
    public static void cetakPdfAanwijzing(Integer produk_id) {
        List<AanwijzingQuestion> aanwijzingQuestion = AanwijzingRepository.findAll(produk_id);
        Usulan usulan = Usulan.findById(produk_id);
        LocalDate localDate = LocalDate.now();
        DateTimeFormatter formatterPdf = DateTimeFormatter.ofPattern("dd MMMM yyyy");
        Map<String, Object> params = new HashMap<>();
        params.put("aanwijzingQuestion", aanwijzingQuestion);
        params.put("usulan", usulan);
        params.put("now", localDate.format(formatterPdf));
        String templateHtml = "admin/UsulanCtr/aanwijzing/cetak.html";
        InputStream inp = HtmlUtil.generatePDFByHtml(params, templateHtml);
        String filePath = "/public/files/dokumen/";
        String fileName = "cetak_aanwijzing_" + produk_id + "_" +
                localDate.format(formatterPdf).replaceAll(" ","_") + "_" + getRandom() + ".pdf";
        File file = new File(Play.applicationPath + filePath + fileName);
        try {
            OutputStream os = new FileOutputStream(file);
            IOUtils.copy(inp, os);
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        renderBinary(file);
    }

    @AllowAccess({Acl.COM_AANWIJZING_DETAIL})
    public static void aanwijzingDetail(Long id, Integer produk_id) throws ParseException {
        Usulan usulan = Usulan.findById(produk_id);
        AktifUser aktifUser = getAktifUser();
        Boolean isTerdaftar = PesertaUsulan.isUserRegistedUsulan(aktifUser.user_id, usulan.id);
        String search = "";
        String searchUrl = "";
        if (params.get("search") != null && !params.get("search").equals("")) {
            searchUrl = "&search="+params.get("search");
            search = params.get("search");
        }
        Tahapan tahapanPenjelasan = Tahapan.getFlaggingTahapanPenjelasan();
        UsulanJadwal usulanJadwalPenjelasan = UsulanJadwal.getDateFlaggingTahapanPenjelasan(tahapanPenjelasan.id, (long) produk_id);
        if (usulanJadwalPenjelasan  != null) {
            if (UsulanService.isExplanation(usulan) && isTerdaftar || getAktifUser().isAdmin() || UsulanService.isExplanation(usulan) && getAktifUser().isPokja()) {
                User user = User.findById(AktifUser.getActiveUserId());
                int max_konten = 10;
                int page = null != params.get("offset") ? params.get("offset", Integer.class) : 1;
                int start = page * max_konten - max_konten;
                List<AanwijzingAnswer> answerAll = AanwijzingRepository.findAllAnswerWithSearch(produk_id, id, search);
                Pageable p = new Pageable(page, max_konten, answerAll.size(), "" + "?produk_id=" + produk_id + "&id=" + id + searchUrl);
                renderArgs.put("user", user);
                renderArgs.put("id", id);
                renderArgs.put("produk_id", produk_id);
                renderArgs.put("p", p);
                renderArgs.put("usulan", usulan);
                renderArgs.put("search", search);
                renderArgs.put("aq", AanwijzingRepository.getDataAanwijzingQuestionDetail(produk_id, id));
                renderArgs.put("aanwijzingAnswerLatest", AanwijzingRepository.findLastesAanwijzingAnswer(produk_id, id));
                renderArgs.put("aanwijzingAnswer", AanwijzingRepository.getDataAanwijzingAnswer(start, max_konten, produk_id, id, search));
                render("admin/UsulanCtr/aanwijzing/detail.html");
            }
        }
        detailPengumuman((long) produk_id);
    }

    @AllowAccess({Acl.COM_INPUT_COMMENT_AANWIJZING_DETAIL})
    public static void createAnswer(Long pertanyaan_id, Integer produk_id, String answer, File file) throws ParseException {
        try {
            if(file != null) {
                if(file.length() <= 1048576) {
                    AanwijzingAnswer.createAanwijzingAnswer(Math.toIntExact(pertanyaan_id), produk_id, answer, file);
                    flash.success(Messages.get("notif.success.submit"));
                } else {
                    flash.error(Messages.get("ukuran_file_upload_image.label")+" 1 MB");
                }
            } else {
                AanwijzingAnswer.createAanwijzingAnswer(Math.toIntExact(pertanyaan_id), produk_id, answer, file);
                flash.success(Messages.get("notif.success.submit"));
            }
        } catch (Exception e) {
            flash.error(Messages.get("notif.failed.submit"));
            LogUtil.d("Error : ", e.getMessage());
            e.printStackTrace();
        }
        redirect("/admin.usulanctr/aanwijzingdetail?produk_id="+produk_id+"&id="+pertanyaan_id);
        //aanwijzingDetail(pertanyaan_id, produk_id);
    }

    @AllowAccess({Acl.COM_EDIT_COMMENT_AANWIJZING_DETAIL})
    public static void editAnswer(String editanswer, Long editpertanyaan_id,
                                  Integer editproduk_id, Integer editid) throws ParseException {
        try {
            AanwijzingAnswer.editAanwijzingAnswer(editanswer, editid);
            flash.success(Messages.get("notif.success.update"));
        } catch (Exception e) {
            flash.error(Messages.get("notif.failed.update"));
            LogUtil.d("Error : ", e.getMessage());
            e.printStackTrace();
        }
        aanwijzingDetail(editpertanyaan_id, editproduk_id);
    }

    @AllowAccess({Acl.COM_DELETE_COMMENT_AANWIJZING_DETAIL})
    public static void deleteAnswer(Long pertanyaan_id, Integer produk_id, Long id) throws ParseException {
        try {
            AanwijzingAnswer.deleteAanwijzingAnswer(id);
            flash.success(Messages.get("notif.success.delete"));
        } catch (Exception e) {
            flash.error(Messages.get("notif.failed.delete"));
        }
        aanwijzingDetail(pertanyaan_id, produk_id);
    }
}
