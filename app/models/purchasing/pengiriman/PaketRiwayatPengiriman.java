package models.purchasing.pengiriman;

import models.BaseKatalogTable;
import models.common.AktifUser;
import models.purchasing.contracts.DeliveryContract;
import models.purchasing.pengiriman.form.DeliveryDataForm;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import utils.DateTimeUtils;
import utils.KatalogUtils;

import javax.persistence.Transient;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author HanusaCloud on 10/17/2017
 */
@Table(name = "paket_pengiriman")
public class PaketRiwayatPengiriman extends BaseKatalogTable implements DeliveryContract {

    @Id
    public Long id;
    public Long paket_id;
    public String deskripsi;
    public String no_dokumen_sistem;
    public String no_dokumen;
    public Double ongkir = 0.0;
    public Date tanggal_dokumen;
    public long active = 1;
    public long agr_paket_pengiriman_produk;
    public Integer agr_paket_pengiriman_lampiran;
    public Long paket_penerimaan_pembayaran_id;

    @Transient
    public Integer agr_paket_penerimaan_produk;

    @Transient
    public Long file_id;
    @Transient
    public String file_sub_location;
    @Transient
    public String file_name;
    @Transient
    public String original_file_name;

    @Transient
    public List<ProdukPengiriman> products = new ArrayList<>();

    @Transient
    public LampiranPengiriman attachment;

    public PaketRiwayatPengiriman() {}

    public PaketRiwayatPengiriman(DeliveryDataForm model) {
        dataForm(model);
    }

    public PaketRiwayatPengiriman dataForm(DeliveryDataForm model) {
        if (!model.isCreateMode()) {
            this.id = model.model;
        }
        this.paket_id = model.packageId;
        this.deskripsi = model.note;
        if (this.no_dokumen_sistem == null) {
            this.no_dokumen_sistem = model.generateSystemNumber();
        }
        this.no_dokumen = model.docNumber;
        this.tanggal_dokumen = model.getDocumentDate();
        this.active = 1;
        this.agr_paket_pengiriman_produk = model.getSavedProducts().size();
        this.agr_paket_pengiriman_lampiran = model.attachment != null ? 1 : 0;
        this.ongkir = model.getOngkosKirimDouble();
        return this;
    }

    @Override
    public String getDetailUrl() {
        return generateUrl("purchasing.PengirimanCtr.detailRiwayatPengiriman");
    }

    @Override
    public String getDeleteUrl() {
        return generateUrl("purchasing.PengirimanCtr.deletePengiriman");
    }

    public String getUpdateUrl() {
        return generateUrl("purchasing.PengirimanCtr.updatePengiriman");
    }

    @Override
    public String getAttachmentDownloadUrl() {
        if (attachment != null) {
            return attachment.getDownloadUrl();
        }
        return "#";
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Long getPackageId() {
        return paket_id;
    }

    @Override
    public boolean isUserAllowed(AktifUser model) {
        return this.created_by == Math.toIntExact(model.user_id);
    }

    @Override
    public void saveHistory() {
        final long result = (long) save();
        if (this.id == null) {
            this.id = result;
        }
    }

    public String getFormattedCreatedDate() {
        return DateTimeUtils.parseToReadableDate(created_date);
    }

    public String getDocumentDate() {
        return DateTimeUtils.parseToReadableDate(tanggal_dokumen);
    }

    /*2243-2671-86*/
    public String getFormattedSystemDocNumber() {
        return KatalogUtils.getFormattedDocNumber(no_dokumen_sistem);
    }

    public String getDocNumber() {
        return no_dokumen;
    }

    public String getDescription() {
        return deskripsi;
    }

    public Boolean getStatusDelivery() {
        if(agr_paket_penerimaan_produk == null || agr_paket_penerimaan_produk == 0){
            return true;
        }
        return false;
    }

    public String getReadableDocDate() {
        return DateTimeUtils.parseToReadableDate(tanggal_dokumen);
    }

    public String getOngkirString() {
        DecimalFormat df = new DecimalFormat("#.#####");
        String formatted = df.format(ongkir);
        return formatted;
    }


    @Override
    public void setProducts(List<ProdukPengiriman> list) {
        this.products = list;
    }

    @Override
    public void setAttachment(LampiranPengiriman model) {
        this.attachment = model;
    }

    @Override
    public List<ProdukPengiriman> getProducts() {
        return this.products;
    }

    public void softDelete() {
        this.active = 0;
        setDeleted();
        save();
    }

}
