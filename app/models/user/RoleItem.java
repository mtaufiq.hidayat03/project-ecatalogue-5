package models.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

import com.google.gson.JsonObject;
import models.BaseKatalogTable;
import models.common.ButtonGroup;
import models.jcommon.datatable.DatatableQuery;
import models.jcommon.datatable.DatatableResultsetHandler;
import play.Logger;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;
import play.mvc.Router;
import services.datatable.DataTableService;

import javax.persistence.Transient;

@Table(name="role_item")
public class RoleItem extends BaseTable {

	@Id
	public Long id;
	public String nama_role;
	public String deskripsi;
	public Long role_grup_id;
	public boolean active;
	public Timestamp created_date = new Timestamp(new Date().getTime());

	@Transient
	public String kategori;
	@Transient
	public String nama_grup;
	
	public static List<RoleItem> findAllActive(){
		String query = "select id, nama_role, deskripsi, role_grup_id from role_item where active = 1";
		
		return Query.find(query, RoleItem.class).fetch();
	}
	
	public static List<RoleItem> findByRoleGrup(Long role_grup_id){
		String query = "select id, nama_role, deskripsi, role_grup_id from role_item where active = 1 and role_grup_id = ?";
		
		return Query.find(query, RoleItem.class, role_grup_id).fetch();
	}

	public static Map<Long, List<RoleItem>> getRoleItemMapGroupByRoleGroup() {
		final String query = "SELECT r.id, r.nama_role, r.deskripsi, r.role_grup_id, rg.kategori, rg.nama_grup " +
				"FROM role_item r " +
				"JOIN role_grup rg ON rg.id = r.role_grup_id " +
				"WHERE r.active = 1 ORDER BY r.role_grup_id";
		List<RoleItem> items = Query.find(query, RoleItem.class).fetch();
		Map<Long, List<RoleItem>> results = new HashMap<>();
		if (!items.isEmpty()) {
			for (RoleItem model : items) {
				if (results.containsKey(model.role_grup_id)) {
					results.get(model.role_grup_id).add(model);
				} else {
					List<RoleItem> roleItems = new ArrayList<>();
					roleItems.add(model);
					results.put(model.role_grup_id, roleItems);
				}
			}
		}
		return results;
	}

	@Transient
	private static String qField = " pr.id,pr.role_grup_id,pr.nama_role,pr.deskripsi, pr.active, rg.nama_grup, rg.id as role_grup_id";
	@Transient
	private static String qFrom = " role_item pr left join role_grup rg on rg.id = pr.role_grup_id ";

	public static JsonObject dataSrc(){
		JsonObject result = new JsonObject();
		try {

			DatatableQuery query =
					new DatatableQuery(dataResult)
							.select(qField)
							.from(qFrom);
			result = query.executeQuery();
		}catch (Exception e){
			Logger.error("Error getDataSrc datatable PointRating:"+ e.getMessage());
		}
		return result;
	}
	private static DatatableResultsetHandler<String[]> dataResult =
			new DatatableResultsetHandler<String[]>(qField) {
				@Override
				public String[] handle(ResultSet rs) throws SQLException {
					Map<String, Object> params = new HashMap<String, Object>();
					final String id = rs.getString("pr.id");
					params.put("id", id);
					String urlEdit =Router.reverse("controllers.admin.roleItemCtr.edit",params).url;
					String urlDetail =Router.reverse("controllers.admin.roleItemCtr.detail",params).url;
					String urlHapus =Router.reverse("controllers.admin.roleItemCtr.delete",params).url;

					List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();
					buttonGroups.add(new ButtonGroup(Messages.get("ubah.label"),urlEdit));
					//buttonGroups.add(new ButtonGroup(Messages.get("detail.label"),urlDetail));
//					buttonGroups.add(new ButtonGroup(Messages.get("hapus.label"),urlHapus));
					String[] results=new String[columns.length+1];
					results[0] = rs.getString("pr.id");
					results[1] = rs.getString("rg.nama_grup");
					results[2] = rs.getString("pr.nama_role");
					results[3] = rs.getString("pr.deskripsi");
					boolean active = rs.getString("pr.active").equals("1");
					String classButton = active ? "class='btn btn-success btn-xs disabled'":"class='btn btn-danger btn-xs disabled'";
					results[4] = String.format("<a href='#' %s style='width:50px'>%s</a>", classButton, active ? Messages.get("ya.label"):Messages.get("tidak.label"));
					results[5] = DataTableService.generateButtonGroup(buttonGroups);

					return results;
				}
			};
}
