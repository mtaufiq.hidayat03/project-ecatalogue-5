package repositories;


import java.util.List;
import models.util.produk.ProvKabJson;
import play.db.jdbc.Query;

/**
 * @author Ipeng on 21/03/2018
 */
public class WilayahRepository {

    public static WilayahRepository instance = new WilayahRepository();

    public List<ProvKabJson> getAllWilayah(){
        StringBuilder sb = new StringBuilder("select p.id provId, p.nama_provinsi provName, k.id kabId, k.nama_kabupaten kabName from kabupaten k join provinsi p on p.id = k.provinsi_id where k.active = 1 and p.active = 1");
        return Query.find(sb.toString(), ProvKabJson.class ).fetch();
    }
}