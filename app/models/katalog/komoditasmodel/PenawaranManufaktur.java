package models.katalog.komoditasmodel;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

@Table(name = "penawaran_manufaktur")
public class PenawaranManufaktur extends BaseKatalogTable {

    @Id
    public Long id;
    public Long penawaran_id;
    public Long manufaktur_id;
    public Long komoditas_id;
    public Long produk_kategori_id;
    public boolean active;

    @Transient
    public String nama_manufaktur;
    @Transient
    public String nama_komoditas;
    @Transient
    public String nama_kategori;

    public static List<PenawaranManufaktur> findByPenawaranDanKomoditasId(Long penawaran_id, Long komoditas_id){
        String sql = "select pm.*, p.nama_penawaran, m.nama_manufaktur, pk.nama_kategori, k.nama_komoditas from penawaran_manufaktur pm\n" +
                "join komoditas k on k.id=pm.komoditas_id\n" +
                "join penawaran p on p.id=pm.penawaran_id\n" +
                "join manufaktur m on m.id = pm.manufaktur_id\n" +
                "join produk_kategori pk on pk.id = pm.produk_kategori_id\n" +
                "where pm.penawaran_id=? and pm.komoditas_id=? and pm.active=1";
        return Query.find(sql,PenawaranManufaktur.class, penawaran_id, komoditas_id).fetch();
    }

    public static PenawaranManufaktur exist(Long produk_kategori_id, Long manufaktur_id, Long penawaran_id, Long komoditas_id){
        String qry = "select * from penawaran_manufaktur where \n" +
                "produk_kategori_id=? and manufaktur_id=? and \n" +
                "penawaran_id=? and komoditas_id=?";
        return Query.find(qry,PenawaranManufaktur.class, produk_kategori_id, manufaktur_id, penawaran_id, komoditas_id).first();
    }
}
