package models.penyedia;

import models.BaseKatalogTable;
import models.common.DokumenInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.io.File;

/**
 * Created by fajar on 29/04/20.
 */
@Table(name = "dokumen_ukm")
public class DokumenUkm extends BaseKatalogTable {

    @Id
    public Long id;

    public Long penyedia_id;

    public String dok_judul;

    public String dok_label;

    public String dok_hash;

    public String dok_signature;

    public Long dok_id_attachment;

    public static DokumenInfo simpanDokUkm(Long penyedia_id, File file, String dok_label) throws Exception {
        DokumenUkm dokumenUkm = new DokumenUkm();
        BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, dokumenUkm.dok_id_attachment, DokumenUkm.class.getSimpleName());
        dokumenUkm.dok_id_attachment = blob.id;
        dokumenUkm.dok_judul = blob.blb_nama_file;
        dokumenUkm.dok_hash = blob.blb_hash;
        dokumenUkm.dok_label = dok_label;
        if(penyedia_id != null){
            dokumenUkm.penyedia_id = penyedia_id;
        }
        Long ids = new Long(dokumenUkm.save());

        DokumenInfo dokInf = new DokumenInfo(blob);
        dokInf.file_label = dokumenUkm.dok_label;
        dokInf.blb_id_content = dokumenUkm.dok_id_attachment;
        if(null != dokumenUkm.id)
            dokInf.id = dokumenUkm.id;
        else
            dokInf.id = ids;

        return dokInf;
    }

    public static Long countDokumen(Long penyediaId) {
        Long total = Query.find("select count(id) total_dok from dokumen_ukm where penyedia_id = ? ", Long.class, penyediaId).first();
        return total;
    }

    public static DokumenUkm findByDokIdAttach(long dokId){
        return find("dok_id_attachment = ? limit 1", dokId).first();
    }

    public DokumenInfo getAsdocInfo(){
        DokumenInfo info = new DokumenInfo();
        BlobTable blob = BlobTable.find("id = ?",dok_id_attachment).first();
        if(blob!=null){
            info.id = blob.id;
            info.blb_id_content = blob.blb_id_content;
            info.file_label = dok_label;
            info.name = dok_judul;
            info.download_url = blob.getDownloadUrl(null);
        }

        return info;
    }
}
