package controllers.cms;

import controllers.BaseController;
import controllers.security.AllowAccess;
import ext.DatetimeBinder;

import models.cms.KategoriKonten;
import static models.cms.KategoriKonten.*;
import models.cms.KontenStatis;
import models.secman.Acl;
import org.apache.commons.codec.binary.Base64;
import org.jsoup.Jsoup;
import play.data.binding.As;
import play.i18n.Messages;
import utils.HtmlUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * Created by user on 10/10/2017.
 */
public class KontenStatisCtr extends BaseController {
    @AllowAccess({Acl.COM_KONTEN_STATIS})
    public static void index(){
        List<KategoriKonten> kategoriKontenList = findAllByKonten(2);
        render(kategoriKontenList);
    }

    @AllowAccess({Acl.COM_KONTEN_STATIS_ADD})
    public static void create(){
        List<KategoriKonten> kategoriKontenList = findAllByKonten(2);
        render("cms/KontenStatisCtr/create.html", kategoriKontenList);
    }

    @AllowAccess({Acl.COM_KONTEN_STATIS_ADD})
    public static void createSubmit(String judul, Long kategori,
                                    @As(binder = DatetimeBinder.class)Date publish_date_from,
                                    @As(binder = DatetimeBinder.class)Date publish_date_end,
                                    String isi_konten){



        KontenStatis kontenStatis = new KontenStatis();
        kontenStatis.judul_konten = judul;
        kontenStatis.konten_kategori_id = kategori;
        kontenStatis.slug = judul.replaceAll("\\s","0").replaceAll("\\W","").replace("0","-");

        String decodedText = HtmlUtil.decodeFromTinymce(isi_konten);

        kontenStatis.isi_konten = decodedText;
        kontenStatis.status = "draft";
        kontenStatis.publish_date_from = publish_date_from;
        kontenStatis.publish_date_to = publish_date_end;
        kontenStatis.active = true;
        long id = kontenStatis.save();
    	flash.success(Messages.get("notif.success.draft"));
        edit(id);

    }

    @AllowAccess({Acl.COM_KONTEN_STATIS_EDIT})
    public static void edit(Long id){
        KontenStatis viewKontenStatis = KontenStatis.findById(id);
        String publish_date_from;
        String publish_date_to;

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        List<KategoriKonten> kategoriKontenList = findAllByKonten(2);
        publish_date_from = dateFormat.format(viewKontenStatis.publish_date_from);
        if(viewKontenStatis.publish_date_to != null){
            publish_date_to = dateFormat.format(viewKontenStatis.publish_date_to);
        }else{
            publish_date_to = null;
        }

        render("cms/KontenStatisCtr/edit.html", viewKontenStatis, kategoriKontenList, publish_date_from, publish_date_to);
    }

    @AllowAccess({Acl.COM_KONTEN_STATIS_EDIT})
    public static void editSubmit(Long data_id, String judul, String status, Long kategori,
                                  @As(binder = DatetimeBinder.class)Date publish_date_from,
                                  @As(binder = DatetimeBinder.class)Date publish_date_end,
                                  String isi_konten){
        KontenStatis kontenStatis = KontenStatis.findById(data_id);
        kontenStatis.judul_konten = judul;
        kontenStatis.konten_kategori_id = kategori;
        kontenStatis.slug = judul.replaceAll("\\s","0").replaceAll("\\W","").replace("0","-");

        String decodedText = HtmlUtil.decodeFromTinymce(isi_konten);

        kontenStatis.isi_konten = decodedText;
        kontenStatis.status = status;
        kontenStatis.publish_date_from = publish_date_from;
        kontenStatis.publish_date_to = publish_date_end;
        kontenStatis.active = true;
        kontenStatis.save();
        flash.success(Messages.get("notif.success.update"));
        edit(data_id);
    }

    @AllowAccess({Acl.COM_KONTEN_STATIS_ADD})
    public static void detail(Long id){
        KontenStatis kontenStatis = KontenStatis.findById(id);
        render("cms/KontenStatisCtr/detail.html",kontenStatis);
    }

    @AllowAccess({Acl.COM_KONTEN_STATIS_DELETE})
    public static void delete(Long id){
        KontenStatis.deleteById(id);
        flash.success(Messages.get("notif.success.delete"));
        index();
    }
}
