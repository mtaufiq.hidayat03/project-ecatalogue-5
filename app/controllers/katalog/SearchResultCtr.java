package controllers.katalog;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import controllers.BaseController;
import ext.Pageable;
import models.common.AktifUser;
import models.elasticsearch.NewBase.PenyediaElastic;
import models.elasticsearch.ProductCommodity;
import models.elasticsearch.SearchQuery;
import models.elasticsearch.SearchResult;
import models.katalog.Komoditas;
import models.katalog.ProdukKategori;
import models.masterdata.Manufaktur;
import models.penyedia.Penyedia;
import org.apache.http.client.utils.URLEncodedUtils;
import play.Logger;
import play.i18n.Lang;
import repositories.elasticsearch.ElasticsearchRepository;
import utils.LogUtil;
import utils.UrlUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 10/10/2017
 */
public class SearchResultCtr extends BaseController {

    public static final String TAG = "SearchResultCtr";

    public static void index() {
        AktifUser aktifuser = AktifUser.getAktifUser();
        SearchQuery model = new SearchQuery(params);
        SearchResult searchResult = new SearchResult();
        try {
            searchResult = ElasticsearchRepository.open().searchAll(model);
            String query = "?";
            query += URLEncodedUtils.format(model.getParams(), "UTF-8");
            searchResult.injectQueryIntoCommodity(query);
        }catch (Exception e){
            Logger.error("[SearchResultCtr.index] - msg : ", e.getMessage());
        }
//        final boolean showBuyButton = getAktifUser() != null && (getAktifUser().isPp() || getAktifUser().isPpk());

        List<Manufaktur> manufakturList = Manufaktur.getManufactures();
        List<PenyediaElastic> penyediaList = Penyedia.getPenyediaKontrak();

        renderArgs.put("searchResult", searchResult);
        renderArgs.put("providers", penyediaList);
        renderArgs.put("manufactures", manufakturList);
//        renderArgs.put("showBuyButton", showBuyButton);

        render("PublikCtr/searchResult/index.html", model, aktifuser);
    }

    public static void searchSugestion(String q) {
        SearchQuery model = new SearchQuery(params);
        SearchResult searchResult = ElasticsearchRepository.open().searchSuggestion(model);
        JsonArray jsonArray = new JsonArray();
        JsonObject global = new JsonObject();
        global.addProperty("label", "Seluruh komoditas");
        global.addProperty("desc", q + " di Seluruh komoditas");
        global.addProperty("url", UrlUtil.builder()
                .setUrl("katalog.SearchResultCtr.index")
                .setParam("language", Lang.get())
                .setParam("q", q)
                .build());
        jsonArray.add(global);
        if (searchResult != null && searchResult.commodities  != null && !searchResult.commodities.isEmpty()) {
            for (ProductCommodity commodity : searchResult.commodities) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("label", commodity.name);
                jsonObject.addProperty("desc", q +" di " + commodity.name);
                jsonObject.addProperty("url", commodity.generateUrl(q));
                jsonArray.add(jsonObject);
            }
        }
        LogUtil.d(TAG, jsonArray);
        renderJSON(jsonArray);
    }

}
