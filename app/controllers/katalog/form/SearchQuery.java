package controllers.katalog.form;

import ext.contracts.ParamAble;
import org.apache.http.message.BasicNameValuePair;
import play.mvc.Scope;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 12/22/2017
 */
public class SearchQuery implements ParamAble {

    public String keyword = "";
    public long commodity = 0;
    public int isDisplayed = -1;
    public int page = 0;
    public String type;
    public int buy = -1;

    public SearchQuery(Scope.Params params) {
        this.keyword = params.get("keyword") != null ? params.get("keyword") : "";
        this.page = getCurrentPage(params);
        this.type = params.get("type") != null ? params.get("type") : "";
        this.buy = params.get("buy") != null ? params.get("buy", Integer.class) : -1;
        this.isDisplayed = params.get("display") != null ? params.get("display", Integer.class) : -1;
        this.commodity = params.get("commodity") != null ? params.get("commodity", Integer.class) : 0;
    }

    @Override
    public List<BasicNameValuePair> getParams() {
        List<BasicNameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("keyword", keyword != null ? keyword : ""));
        params.add(new BasicNameValuePair("commodity", String.valueOf(commodity)));
        params.add(new BasicNameValuePair("display", String.valueOf(isDisplayed)));
        params.add(new BasicNameValuePair("type", type != null ? type : ""));
        params.add(new BasicNameValuePair("buy", String.valueOf(buy)));
        return params;
    }

    @Override
    public int getPage() {
        return this.page;
    }
}
