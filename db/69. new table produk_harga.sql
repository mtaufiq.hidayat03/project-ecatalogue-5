CREATE TABLE `produk_harga`(
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`produk_id` Int( 11 ) NOT NULL,
	`kurs_id` Int( 11 ) NOT NULL,
	`harga` text NOT NULL,
	`tanggal_dibuat` DateTime NULL,
	`apakah_disetujui` Int( 1 ) NULL,
	`tanggal_disetujui` DateTime NULL,
	`tanggal_ditolak` DateTime NULL,
	`setuju_tolak_oleh` Int( 5 ) NULL,
	`created_date` DateTime NULL,
	`created_by` Int( 5 ) NULL,
	`modified_date` DateTime NULL,
	`modified_by` Int( 5 ) NULL,
	`deleted_date` DateTime NULL,
	`deleted_by` Int( 5 ) NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`auditdate` Timestamp NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`active` Int(1) NULL,
	PRIMARY KEY (`id`)
)
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;