package utils.performance;

import java.util.Comparator;
import java.util.Set;

import play.mvc.Http.Request;

/**Untuk monitoring terhadap controller
 * 1. yg paling banyak diakses
 * 2. yg paling lama response
 * 
 * @author Andik Yulianto<andikyulianto@yahoo.com>
 */
public class ActionEntry {

	public String action;
	public long duration=0;
	public int count=0;
	
	public ActionEntry(String action)
	{
		this.action=action;
	}
	
	public boolean equals(Object o)
	{
		if(o==null)
			return false;
		return this.action==((ActionEntry)o).action;
	}
	
	public int hashcode()
	{
		return action.hashCode();
	}
	
	
	
	/**Comparator Durasi
	 * @author Andik Yulianto<andikyulianto@yahoo.com>
	 */
	public static class DurationComparator implements Comparator<ActionEntry>
	{
		@Override
		public int compare(ActionEntry arg0, ActionEntry arg1) {
			if(arg0.duration==arg1.duration)
				return 0;
			return arg0.duration < arg1.duration ? 1 : -1;
		}
		
	}
	
	
	/**Comparator untuk Count
	 * @author Andik Yulianto<andikyulianto@yahoo.com>
	 */
	public static class CountComparator implements Comparator<ActionEntry>
	{
		@Override
		public int compare(ActionEntry arg0, ActionEntry arg1) {
			if(arg0.count==arg1.count)
				return 0;
			return arg0.count < arg1.count ? 1 : -1;
		}
		
	}

}
