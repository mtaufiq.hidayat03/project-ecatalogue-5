package models.katalog.komoditasmodel.contracts;

import models.katalog.komoditasmodel.KomoditasProdukAtributTipe;

import java.util.List;

/**
 * @author HanusaCloud on 12/5/2017
 */
public interface CommodityContract {

    Long getId();

    void setAttributes(List<KomoditasProdukAtributTipe> list);

    default void withAttributesProduct() {
        if (getId() != null) {
            setAttributes(KomoditasProdukAtributTipe.getAllByCommodity(getId()));
        }
    }

}
