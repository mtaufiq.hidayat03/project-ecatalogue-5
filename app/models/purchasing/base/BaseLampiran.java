package models.purchasing.base;

import models.BaseKatalogTable;
import models.util.Config;
import org.apache.commons.io.FilenameUtils;
import play.Logger;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import utils.DateTimeUtils;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author HanusaCloud on 10/23/2017
 */
public class BaseLampiran extends BaseKatalogTable {

    @Id
    public Long id;
    public String file_sub_location;
    public String file_name;
    public String original_file_name;
    public Long file_size;
    public Long paket_id;
    public Integer posisi_file;

    public Timestamp created_date;
    public Integer created_by;
    public Timestamp modified_date;
    public Integer modified_by;
    public Timestamp deleted_date;
    public Integer deleted_by;

    public void setFromFile(File file) {
        this.original_file_name = file.getName();
        this.file_size = file.length();
        this.file_sub_location = DateTimeUtils.generateDirFormattedDate();
        this.file_name = regenerateFileName();
        final File newFile = getFile();
        if (file.renameTo(newFile)) {
            Logger.debug("rename file");
        }
    }

    public File getFile() {
        File dir = getDirectory();
        return new File(dir.getAbsolutePath() + "/" + file_name);
    }

    public File getDirectory() {
        Config config = Config.getInstance();
        File dir = new File( config.fileStorageDir + config.pengirimanLampiranDir + "/" + file_sub_location);
        if (!dir.exists() && dir.mkdirs()) {
            Logger.debug("dir not exists, creating..");
        }
        return dir;
    }

    public String regenerateFileName() {
        return (created_by != null ? String.valueOf(created_by) : "" ) + System.currentTimeMillis() + "." + getExtension();
    }

    public String getExtension() {
        return FilenameUtils.getExtension(this.original_file_name);
    }

    public BaseLampiran dataForm(BaseDataForm model) {
        if (model.attachment != null) {
            setFromFile(model.attachment);
        }
        return this;
    }

    public BaseLampiran position(int position) {
        this.posisi_file = position;
        return this;
    }

    public void createAttachment() {
        this.id = (long) save();
    }

}
