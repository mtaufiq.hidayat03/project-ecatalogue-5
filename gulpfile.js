var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var cleanCss = require('gulp-clean-css');
var merge = require('merge-stream');
var concat = require('gulp-concat');

gulp.task('build-css', function () {
    var scssStream = gulp.src('./resources/scss/app.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(rename({extname: '.min.css'}))
        .pipe(gulp.dest('./resources/build/'));

    var cssStream = gulp.src([
        './resources/build/app.min.css',
        './resources/css/ladda-themeless.min.css',
        './resources/css/jquery.toast.min.css'
    ]);

    return merge(scssStream, cssStream)
        .pipe(cleanCss())
        .pipe(concat('app.min.css'))
        .pipe(gulp.dest('./public/stylesheets/'));
});