package jobs.ExtProduct;

import com.google.gson.Gson;
import jobs.JobTrail.*;
import models.api.produk.HistoryDatafeed;
import models.api.produk.ProdukApiInfo;
import models.api.produk.detail.image.Item;
import models.jcommon.config.Configuration;
import models.jcommon.http.SimpleHttpRequest;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.katalog.*;
import models.util.Config;
import play.Logger;
import play.db.jdbc.Query;
import play.jobs.Job;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

public class ExtProduct {
    final int maxRetry = 3; //next set from db configuration
    public int seqRetry = 0;

    public int defaultWaitime = 1;
    public int nextWaitime = 1;
    public ProdukApiInfo jsParsed ;
    private String jsResult = "{}";
    public String urlForExecutejob = "";

    public PenyediaKomoditasApi penyediaKomoditasApi;
    public HistoryDatafeed historyDatafeed;
    public List<models.api.produk.Produk> failed = new ArrayList<>();
    public List<models.api.produk.Produk> success = new ArrayList<>();
    public List<models.api.produk.Produk> updated = new ArrayList<>();

    //http://lkpp.bhinneka.com/lkpp/updated_produk?per_page=500&page=1&secretkey=f3ebe642baf41256ebd17e7d63ed99e4b6438857
//    public Boolean getData(Integer minuteTimeout, String url, int per_page, int t_perpage){
//        Boolean resultGet = false;
//
//        return false;
//    }

    public Boolean getDatas(Integer minuteTimeout, String urls, HistoryDatafeed _hdf){

        Boolean resultGet = false;
        boolean resultSave = false;
        jsResult = "{}";

        Integer seconds = 0;
        if(minuteTimeout > 0){
            seconds = (minuteTimeout*60);
        }
        Logger.info("run with minuteTimeout: "+minuteTimeout+" "+urls);
        try {

            SimpleHttpRequest connect = new SimpleHttpRequest(seconds);
            connect.get(urls);
            jsResult = connect.getString();
            resultGet = true;
            if(resultGet){
                ProdukApiInfo objResult = parseAndGetData();
                if(objResult!= null && null != objResult.total && objResult.total > 0){

                    //sub saving here
                    SaveProses(objResult);
                    int nextPage = (objResult.current_page + 1);
                    if (_hdf.message==null){
                        String urlNew = fixingurl(penyediaKomoditasApi,objResult.current_page ,objResult.per_page);
                        _hdf.total_product = objResult.total;
                        _hdf.message = "URL : "+urlNew+"\n"+"\n";
                        _hdf.total_inserted = this.success.size();
                        _hdf.total_updated = this.updated.size();
                        _hdf.total_failed = this.failed.size();
                    }else {
                        String urlNext = fixingurl(penyediaKomoditasApi,nextPage,objResult.per_page);
                        _hdf.message = _hdf.message + "URL : "+urlNext+"\n"+"\n";
                        _hdf.total_inserted = _hdf.total_inserted + this.success.size();
                        _hdf.total_updated = _hdf.total_updated  + this.updated.size();
                        _hdf.total_failed = _hdf.total_failed + this.failed.size();
                    }

                    //int nextPage = (objResult.current_page + 1);
                    //continue next page, meskipun saving gagal
                    if(nextPage <= objResult.total_page) {//if true after saving
                        //generate url next page
                        try {
                            //next page url execution
                            urls = fixingurl(penyediaKomoditasApi, nextPage, objResult.per_page );
                            final String newurls = urls;
                            Logger.info("create job for next page: "+nextPage+" "+urls);

                            ExtProdukJob2 exp = new ExtProdukJob2(defaultWaitime,penyediaKomoditasApi,nextWaitime, newurls, _hdf);
                            exp.in(10);
//                            new Job(){
//                                UUID id = UUID.randomUUID();
//                                int _defaultWaitime = defaultWaitime;
//                                PenyediaKomoditasApi _pk = penyediaKomoditasApi;
//                                int _nextWaitime = nextWaitime;
//                                String _newurls = newurls;
//                                @Override
//                                public void doJob() {
//                                    ExtProduct ext = new ExtProduct();
//                                    ext.penyediaKomoditasApi = _pk;
//                                    ext.defaultWaitime = _defaultWaitime;
//                                    ext.nextWaitime = _nextWaitime;
//                                    ext.historyDatafeed = _hdf;
//                                    ext.getDatas(ext.defaultWaitime, _newurls,ext.historyDatafeed);
//                                }
//                                public void before(){
//                                    //Logger.info("on continue next page, running job id:"+id);
//                                    Logger.info("on continue next page, penyedia:"+_pk.nama_penyedia);
//                                }
//                            }.in(10);
                        } catch (Exception e) {
                            Logger.error("create job next page error : "+e.getMessage());
                        }
                    }else{
                        _hdf.end_time = new Timestamp(new Date().getTime()).toString();
                        Logger.info("last page "+objResult.current_page);
                    }
                }else{
                    //cant parse, assums failed get, result wrong
                    resultGet = false;
                }

            }
        } catch (InterruptedException e) {
            Logger.error("Interupted Exception:"+e.getMessage());
            e.printStackTrace();
        }catch (IOException e) {
            Logger.error("IO Exception:"+e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            Logger.error("Exception:"+e.getMessage());
            e.printStackTrace();
        }

        if(resultGet==false){
            nextWaitime = (nextWaitime+1);
            //Configuration.updateConfigurationValue(DEFAULT_WAIT_TIME_NEXT, String.valueOf(newWait));
            //propJob.replace(DEFAULT_WAIT_TIME_NEXT, String.valueOf(newWait));

            if(seqRetry < maxRetry){
                int nexSeqRetry = (seqRetry+1);
                Logger.info("retry "+nexSeqRetry+" newjob failed for url: "+urls);
                //ExtProduct exts = new ExtProduct();
                //exts.penyediaKomoditasApi = penyediaKomoditasApi;
                //exts.seqRetry = nexSeqRetry;
//                exts.defaultWaitime = defaultWaitime;
//                exts.nextWaitime = nextWaitime;
//                exts.urlForExecutejob = urlForExecutejob;
                ExtProdukJob3 exp = new ExtProdukJob3(defaultWaitime, penyediaKomoditasApi,_hdf,nextWaitime, seqRetry, urlForExecutejob);
                exp.in(10);
//                new Job(){
//                    UUID id = UUID.randomUUID();
//                    int _defaultWaitime = defaultWaitime;
//                    PenyediaKomoditasApi _pk = penyediaKomoditasApi;
//                    HistoryDatafeed hdf = historyDatafeed;
//                    int _nextWaitime = nextWaitime;
//                    int _seqRetry = nexSeqRetry;
//                    String _urlForExecutejob = urlForExecutejob;
//                    @Override
//                    public void doJob() {
//                        ExtProduct ext = new ExtProduct();
//                        ext.penyediaKomoditasApi = _pk;
//                        ext.defaultWaitime = _defaultWaitime;
//                        ext.nextWaitime = _nextWaitime;
//                        ext.seqRetry = _seqRetry;
//                        ext.historyDatafeed = hdf;
//                        ext.getData(ext.nextWaitime, _urlForExecutejob,hdf);
//                    }
//
//                }.in(10);
            }else {
                Logger.info("Max retry reached for:"+urls);
            }

        }
        HistoryDatafeed hisDf = HistoryDatafeed.find("id_job like ?",_hdf.id_job).first();
        hisDf.message = _hdf.message;
        hisDf.total_product = _hdf.total_product;
        hisDf.total_inserted = _hdf.total_inserted;
        hisDf.total_updated = _hdf.total_updated;
        hisDf.total_failed = _hdf.total_failed;
        hisDf.end_time = _hdf.end_time;
        hisDf.save();
        return resultGet;
    }

    public Boolean getData(Integer minuteTimeout, String urls, HistoryDatafeed hdf){

        Boolean resultGet = false;
        boolean resultSave = false;
        jsResult = "{}";

        Integer seconds = 0;
        if(minuteTimeout > 0){
            seconds = (minuteTimeout*60);
        }
        Logger.info("run with minuteTimeout: "+minuteTimeout+" "+urls);
        try {

                SimpleHttpRequest connect = new SimpleHttpRequest(seconds);
                connect.get(urls);
                jsResult = connect.getString();
                resultGet = true;
                if(resultGet){
                    ProdukApiInfo objResult = parseAndGetData();
                    if(objResult!= null && null != objResult.total && objResult.total > 0){

                        //sub saving here
                        SaveProses(objResult);

                        int nextPage = (objResult.current_page + 1);
                        if (hdf.message==null){
                            String urlNew = fixingurl(penyediaKomoditasApi,objResult.current_page ,objResult.per_page);
                            hdf.total_product = objResult.total;
                            hdf.message = "URL : "+urlNew+"\n"+"\n";
                            hdf.total_inserted = this.success.size();
                            hdf.total_updated = this.updated.size();
                            hdf.total_failed = this.failed.size();
                        }else {
                            String urlNext = fixingurl(penyediaKomoditasApi,nextPage,objResult.per_page);
                            hdf.message = hdf.message + "URL : "+urlNext+"\n"+"\n";
                            hdf.total_inserted = hdf.total_inserted + this.success.size();
                            hdf.total_updated = hdf.total_updated  + this.updated.size();
                            hdf.total_failed = hdf.total_failed + this.failed.size();
                        }
                        //continue next page, meskipun saving gagal
                        if(nextPage <= objResult.total_page) {//if true after saving
                            //generate url next page
                            try {
                                //next page url execution
                                urls = fixingurl(penyediaKomoditasApi, nextPage, objResult.per_page );
                                Logger.info("create job for next page: "+nextPage+" "+urls);
                                ExtProduct exts = new ExtProduct();
                                exts.penyediaKomoditasApi = penyediaKomoditasApi;
                                exts.historyDatafeed = hdf;
                                exts.defaultWaitime = defaultWaitime;
                                exts.nextWaitime = nextWaitime;
                                exts.urlForExecutejob = urls;
                                ExtProdukJob4 job4 = new ExtProdukJob4(this);
                                job4.in(10);
//                                new Job(){
//                                    UUID id = UUID.randomUUID();
//                                    ExtProduct ext = exts;
//
//                                    @Override
//                                    public void doJob() {
//                                        ext.getData(ext.defaultWaitime, ext.urlForExecutejob, ext.historyDatafeed);
//                                    }
//                                    public void before(){
//                                        //Logger.info("on continue next page, running job id:"+id);
//                                        Logger.info("on continue next page, penyedia:"+ext.penyediaKomoditasApi.nama_penyedia);
//                                        super.before();
//                                    }
//                                    @Override
//                                    public void after() {
//                                        //after finish send mail
//                                        Logger.info(ext.penyediaKomoditasApi.nama_penyedia+" job is done on continue, reporting job id:"+id);
//
//                                    }
//                                }.in(10);
                            } catch (Exception e) {
                                Logger.error("create job next page error : "+e.getMessage());
                            }
                        }else{
                            hdf.end_time = new Timestamp(new Date().getTime()).toString();
                            Logger.info("last page "+objResult.current_page);
                        }
                    }else{
                        //cant parse, assums failed get, result wrong
                        resultGet = false;
                    }

                }
        } catch (InterruptedException e) {
            Logger.error("Interupted Exception:"+e.getMessage());
            e.printStackTrace();
        }catch (IOException e) {
            Logger.error("IO Exception:"+e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            Logger.error("Exception:"+e.getMessage());
            e.printStackTrace();
        }

        if(resultGet==false){
            nextWaitime = (nextWaitime+1);
            //Configuration.updateConfigurationValue(DEFAULT_WAIT_TIME_NEXT, String.valueOf(newWait));
            //propJob.replace(DEFAULT_WAIT_TIME_NEXT, String.valueOf(newWait));

            if(seqRetry < maxRetry){
                int nexSeqRetry = (seqRetry+1);
                Logger.info("retry "+nexSeqRetry+" newjob failed for url: "+urls);
                ExtProduct exts = new ExtProduct();
                exts.penyediaKomoditasApi = penyediaKomoditasApi;
                exts.historyDatafeed = hdf;
                exts.seqRetry = nexSeqRetry;
                exts.defaultWaitime = defaultWaitime;
                exts.nextWaitime = nextWaitime;
                exts.urlForExecutejob = urlForExecutejob;

                ExtProdukJob5 job5 = new ExtProdukJob5(this);
                job5.in(10);
//                new Job(){
//                    UUID id = UUID.randomUUID();
//                    ExtProduct ext = exts;
//                    @Override
//                    public void doJob() {
//                        ext.getData(ext.nextWaitime, ext.urlForExecutejob, ext.historyDatafeed);
//                    }
//
//                    @Override
//                    public void before(){
//                        Logger.info("retry failed proses job, new job id:"+id);
//                        Logger.info("penyedia:"+ext.penyediaKomoditasApi.nama_penyedia);
//                        super.before();
//                    }
//                    @Override
//                    public void after() {
//                        //after finish send mail
//                        Logger.info(ext.penyediaKomoditasApi.nama_penyedia+" job is done, reporting failed proses on job id:"+id);
//                    }
//                }.in(10);
            }else {
                Logger.info("Max retry reached for:"+urls);
            }

        }
        HistoryDatafeed hisDf = HistoryDatafeed.find("id_job like ?",hdf.id_job).first();
        hisDf.message = hdf.message;
        hisDf.total_product = hdf.total_product;
        hisDf.total_inserted = hdf.total_inserted;
        hisDf.total_updated = hdf.total_updated;
        hisDf.total_failed = hdf.total_failed;
        hisDf.end_time = hdf.end_time;
        hisDf.save();
        return resultGet;
    }

//    public void newJobGetDataAllPenyedia(String url){
//         new TrackableJob(){
//             ExtProduct ext = new ExtProduct();
//             public void before(){
//                 ext.defaultWaitime = defaultWaitime;
//                 ext.nextWaitime = nextWaitime;
//                 ext.urlForExecutejob = url;
//                 super.before();
//             }
//             @Override
//             public void doJob() {
//                 ext.urlForExecutejob = url;
//                 ext.getData(defaultWaitime,ext.urlForExecutejob);
//             }
//         }.in(5);
//    }

    private ProdukApiInfo parseAndGetData(){
        try{
            //GsonBuilder gsonBuilder = new GsonBuilder().registerTypeAdapter(Date.class, null);
            jsParsed = new Gson().fromJson(jsResult, ProdukApiInfo.class);
            //Logger.info("result parsed, produk data:"+jsParsed.produk.size());
        }catch (Exception e){
            jsParsed = null;
            Logger.error("error parse data:"+e.getMessage());
            e.printStackTrace();
        }
        return jsParsed;
    }

    public PenyediaKomoditasApi getPenyediaKomoditasApi(Long idp){
        PenyediaKomoditasApi result  = null;
        try{
            String query = "SELECT p.id as penyediaid, p.nama_penyedia, p.email,pk.komoditas_id as komoditasid, k.kode_komoditas, p.produk_api_url "+
                    ",(select date(max(modified_date)) as start_date from produk pd where pd.penyedia_id = penyediaid and pd.komoditas_id = komoditasid) as start_date "+
                    ",(select kh.id from komoditas_harga_atribut kh where kh.komoditas_id = komoditasid and kh.apakah_harga_retail = true) as harga_retail "+
                    ",(select kh.id from komoditas_harga_atribut kh where kh.komoditas_id = komoditasid and (kh.apakah_harga_pemerintah = true || kh.apakah_harga_utama = true)) as harga_pemerintah " +
                    ",(select kh.id from komoditas_harga_atribut kh where kh.komoditas_id = komoditasid and kh.apakah_ongkir = true) as harga_ongkir " +
                    "FROM komoditas k " +
                    "JOIN penyedia_komoditas pk ON pk.komoditas_id = k.id AND pk.active >= 0 AND k.active >= 0 AND k.terkunci = 1 AND k.kelas_harga = 'nasional' " +
                    "JOIN penyedia p ON p.id = pk.penyedia_id AND p.active = 1 AND ( p.produk_api_url <> '' OR p.produk_api_url IS NOT NULL) " +
                    "JOIN user u ON u.id = p.user_id " +
                    "WHERE p.produk_api_url != '' " +
                    //" AND p.id=409611 "+
                    "ORDER BY p.nama_penyedia ASC ";
            result = Query.find(query,PenyediaKomoditasApi.class).first();
        }catch (Exception e){
            Logger.error("error get all urlpenyedia:"+e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    public List<PenyediaKomoditasApi> getUrlPenyedia(long komoditasId, long penyediaId){
        List<PenyediaKomoditasApi> list = new ArrayList<>();
        try{
            String query = "SELECT pkm.penyedia_id as penyediaid " +
                    ", p.nama_penyedia " +
                    ", p.email " +
                    ", pkm.komoditas_id as komoditasid " +
                    ", k.kode_komoditas " +
                    ", pkm.produk_api_url " +
                    ",(select date(max(modified_date)) as start_date from produk pd where pd.penyedia_id = penyediaid and pd.komoditas_id = komoditasid) as start_date " +
                    ",(select kh.id from komoditas_harga_atribut kh where kh.komoditas_id = komoditasid and kh.apakah_harga_retail = true) as harga_retail " +
                    ",(select kh.id from komoditas_harga_atribut kh where kh.komoditas_id = komoditasid and (kh.apakah_harga_pemerintah = true || kh.apakah_harga_utama = true)) as harga_pemerintah " +
                    ",(select kh.id from komoditas_harga_atribut kh where kh.komoditas_id = komoditasid and kh.apakah_ongkir = true) as harga_ongkir " +
                    "FROM penyedia_komoditas pkm " +
                    "LEFT JOIN penyedia_kontrak pk ON pkm.penyedia_id = pk.penyedia_id " +
                    "LEFT JOIN penyedia p ON p.id = pk.penyedia_id " +
                    "LEFT JOIN komoditas k ON pkm.komoditas_id = k.id " +
                    "WHERE " +
                    "pkm.komoditas_id = ? and " +
                    "pkm.penyedia_id = ? and "+
                    "(pkm.produk_api_url <> '' OR pkm.produk_api_url is not null) and pkm.active = 1 AND k.active = 1 AND k.kelas_harga = 'nasional' " +
                    "AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1 " +
                    "GROUP BY pkm.penyedia_id, pkm.komoditas_id";
            list = Query.find(query,PenyediaKomoditasApi.class, komoditasId, penyediaId).fetch();
        }catch (Exception e){
            Logger.error("error get all urlpenyedia:"+e.getMessage());
            e.printStackTrace();
        }
    return list;
    }

    public Map<String, List<String>> splitQuery(URL url) throws UnsupportedEncodingException {
        Map<String, List<String>> query_pairs = new LinkedHashMap<String, List<String>>();
        if(url.getQuery() != null){
            String[] pairs = url.getQuery().split("&");
            for (String pair : pairs) {
                int idx = pair.indexOf("=");
                String key = idx > 0 ? URLDecoder.decode(pair.substring(0, idx), "UTF-8") : pair;
                if (!query_pairs.containsKey(key)) {
                    query_pairs.put(key, new LinkedList<String>());
                }
                String value = idx > 0 && pair.length() > idx + 1 ? URLDecoder.decode(pair.substring(idx + 1), "UTF-8") : null;
                query_pairs.get(key).add(value);
            }
        }
        return query_pairs;
    }

    public void executeOne(PenyediaKomoditasApi pk , HistoryDatafeed hdf){
        //List<PenyediaKomoditasApi> list = this.getUrlPenyedia();
        int defQjobwait = 3;//next 5
        int qjobwait = defQjobwait;//next 5

            try {
                URL urls = new URL(pk.produk_api_url);
                Logger.info("get from url: "+pk.produk_api_url+" "+pk.start_date);
                //create new object for independence object
                ExtProdukJob6 job6 = new ExtProdukJob6(defaultWaitime,pk,nextWaitime,hdf);
                job6.in(qjobwait);

//                new Job(){
//                    UUID id = UUID.randomUUID();
//                    int _defaultWaitime = defaultWaitime;
//                    PenyediaKomoditasApi _pk = pk;
//                    int _nextWaitime = nextWaitime;
//
//                    @Override
//                    public void doJob() {
//
//                        ExtProduct ext = new ExtProduct();
//                        ext.penyediaKomoditasApi = _pk;
//                        ext.defaultWaitime = _defaultWaitime;
//                        ext.nextWaitime = _nextWaitime;
//                        ext.historyDatafeed = hdf;
//                        //ext.urlForExecutejob = _pk.produk_api_url;
//                        ext.getDatas(ext.defaultWaitime,_pk.produk_api_url, ext.historyDatafeed);
//                    }
//
//                    public void before(){
//                        Logger.info("running job id:"+id+", 1st page, penyedia:"+_pk.nama_penyedia);
//
//                    }
//                    @Override
//                    public void after() {
//                        //after finish send mail
//                        Logger.info(_pk.nama_penyedia+" job is done, reporting job id:"+id);
//
//                    }
//                }.in(qjobwait);
                qjobwait = qjobwait+defQjobwait+1;
            }catch (MalformedURLException e) {
                Logger.error("not valid url penyedia:"+ pk.produk_api_url+" "+pk.start_date);
            }

    }

    public void executeAll(List<PenyediaKomoditasApi> list, HistoryDatafeed _hdf){
        //List<PenyediaKomoditasApi> list = this.getUrlPenyedia();
        int defQjobwait = 3;//next 5
        int qjobwait = defQjobwait;//next 5
        for (PenyediaKomoditasApi pk:list) {
            try {
                URL urls = new URL(pk.produk_api_url);
                Logger.info("get from url: "+pk.produk_api_url+" "+pk.start_date);
                //create new object for independence object
                ExtProduct exts = new ExtProduct();
                exts.penyediaKomoditasApi = pk;
                exts.historyDatafeed = _hdf;
                exts.defaultWaitime = defaultWaitime;
                exts.nextWaitime = nextWaitime;
                exts.urlForExecutejob = pk.produk_api_url;
                ExtProdukJob7 job7 = new ExtProdukJob7(this);
                job7.in(qjobwait);
//                new Job(){
//                    UUID id = UUID.randomUUID();
//                    ExtProduct ext = exts;
//                    @Override
//                    public void doJob() {
//                        ext.getData(ext.defaultWaitime,ext.urlForExecutejob, ext.historyDatafeed);
//                    }
//
//                    public void before(){
//                        Logger.info("running job id:"+id+", 1st page, penyedia:"+ext.penyediaKomoditasApi.nama_penyedia);
//                    }
//                    @Override
//                    public void after() {
//                        //after finish send mail
//                        Logger.info(ext.penyediaKomoditasApi.nama_penyedia+" job is done, reporting job id:"+id);
//                    }
//                }.in(qjobwait);
                qjobwait = qjobwait+defQjobwait+1;
            }catch (MalformedURLException e) {
                Logger.error("not valid url penyedia:"+ pk.produk_api_url+" "+pk.start_date);
            }
        }
    }

    public String fixingurl(PenyediaKomoditasApi pk, Integer next, Integer perpage){
        String urls = pk.produk_api_url;
        try {
            URL newurl = new URL(urls);
            if(null != newurl.getQuery()){
                urls = urls.replace(newurl.getQuery(),"");
            }
            if(urls.indexOf("?") < 0){
                urls = urls+"?";
            }
            Map<String, List<String>> dturl = splitQuery(newurl);
            String pattern = "yyyy-MM-dd";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

            urls = urls+"startdate="+  simpleDateFormat.format(pk.start_date);
            urls = urls+"&enddate="+ simpleDateFormat.format(new Date());

            if(dturl.get("secretkey") != null){
                if(dturl.get("secretkey").size()>0){
                    urls = urls+"&secretkey="+dturl.get("secretkey").get(0);
                }
            }
            if(null != next){
                urls = urls+"&page="+next;
            }
            if(null != perpage){
                urls = urls+"&per_page="+ perpage;
            }


        }catch (Exception e){
            Logger.info("failed fixing urls:"+e.getMessage());
        }
        return urls;
    }

    public void executeUpdate(String query){

        Query.update(query);
    }

    public void SaveProses(ProdukApiInfo objResult){
        //saving produk
        boolean result = true;
        String msgFailed = "<p>total produk failed:</p>";
        String skus = "<br/><p>";
        for (models.api.produk.Produk produkAPi: objResult.produk) {
            try{
                //cek existing produk
                Produk existP = Produk.getCurentProdukForCrawl(produkAPi, penyediaKomoditasApi);
                Produk newp = new Produk();
                //set fields
                newp = produkInfoToPrroduk(newp,produkAPi);
                if(existP == null){//no existing produk, create new
                    //newp. = produkAPi.informasi ;
                    if(null != newp.unit_pengukuran_id && null != newp.unspsc_id){
                        //save Produk
                        newp.saveProduct();
                        Logger.info("save new produk:"+newp.nama_produk);
                        //save riwayat
                        saveProdukRiwayat(newp,"produk_riwayat_datafeed_insert");
                        success.add(produkAPi);
                    }
                    //pastinya null karena new produk, begitujuga gambar dan lampiran
                    ProdukHargaNasional harga = null;
                    //update or add harga
                    updateOrAddHarga(harga,newp,produkAPi);
                    //update or add gambar;
                    ProdukGambar gambar = ProdukGambar.getAktifByProdukId(newp.id);
                    updateOrAddGambar(gambar,produkAPi, newp);
                    //update or add lampiran
                    ProdukLampiran lampiran = ProdukLampiran.getAktifByProdukId(newp.id);
                    updateOrAddLAmpiran(lampiran,produkAPi, newp);
                }else{//exisiting ada, update
                    //replace newP with newp from DB
                    newp = existP;
                    //updatefield
                    newp = produkInfoToPrroduk(newp,produkAPi);
                    //$harga = $this->db->get_where("produk_harga_nasional", array("produk_id" => $produk_id, "active" => 1))->row_array();
                    //catatan: bila tidak ada produk harga nasional yang aktif maka dianggap true
                    ProdukHargaNasional harga = ProdukHargaNasional.getAktifByProdukId(newp.id);
                if(produkAPi.informasi.getTglUpdateAsDate() != newp.modified_date ||
                        (null == harga || harga.modified_date != produkAPi.harga.getTglUpdateAsTimestamp()) ||
                        produkAPi.spesifikasi.getTglUpdateAsDate() != newp.modified_date){
                    //update
                    newp.save();
                    Logger.info("update produk:"+newp.nama_produk+" "+newp.id);
                    //save riwayat
                    saveProdukRiwayat(newp,"produk_riwayat_datafeed_update");
                    updated.add(produkAPi);
                    }else{
                        Logger.info("not save update produk:"+newp.nama_produk+" "+newp.id);
                    }
                    //update or add harga
                    updateOrAddHarga(harga,newp,produkAPi);
                    //update or add gambar;
                    ProdukGambar gambar = ProdukGambar.getAktifByProdukId(newp.id);
                    updateOrAddGambar(gambar,produkAPi, newp);
                    //update or add lampiran
                    ProdukLampiran lampiran = ProdukLampiran.getAktifByProdukId(newp.id);
                    updateOrAddLAmpiran(lampiran,produkAPi, newp);
                }
            }catch (Exception e){
                Logger.error("error saving produk, report to penyedia:"+e.getMessage());
                //mail to penyedia
                failed.add(produkAPi);
                skus += produkAPi.informasi.no_produk_penyedia+"; ";
                sendEmailReportToAdmin(e.toString(),"crawl produk penyedia error");
            }
        }
        if(failed.size()>0){
            skus += "</p>";
            if(!Config.getInstance().prodDev){
                msgFailed+=failed.size();

                //
                //sendEmailReport(this.penyediaKomoditasApi.email,msgFailed,"Report failed produk save");
            }
            sendEmailReportToAdmin(msgFailed+skus,"Report failed produk save");
        }
    }
    public void updateOrAddHarga(ProdukHargaNasional harga, Produk newp, models.api.produk.Produk produkAPi){
        if((null == harga) || (harga.modified_date != produkAPi.harga.getTglUpdateAsTimestamp())){
            int countphn = Query.update("update produk_harga_nasional set active = 0 where produk_id = ? AND active = 1",newp.id);
            //saveProdukRiwayat(newp,"produk_riwayat_datafeed_update_harga");
            //$isHargaUpdated = true;
            //create harga retail,pemerintah,ongkir
            createHargaNasional(newp,produkAPi,penyediaKomoditasApi);

            //update produk
            //Update Harga utama
            newp.harga_utama = new BigDecimal(produkAPi.harga.harga_pemerintah);
            newp.harga_ongkir = new BigDecimal(produkAPi.harga.ongkos_kirim);
            newp.harga_kurs_id = produkAPi.harga.kurs_id;
            newp.harga_tanggal = produkAPi.informasi.getTglUpdateAsDate();
            newp.margin_harga = (((produkAPi.harga.harga_retail - produkAPi.harga.harga_pemerintah)/produkAPi.harga.harga_retail)*100);

            newp.save();

        }
    }
    public void updateOrAddGambar(ProdukGambar gambar, models.api.produk.Produk produkAPi, Produk newp){
        //catatan gambar: bila tidak ada produk gambar yang aktif maka dianggap true

        if((null == gambar)||(gambar.modified_date != produkAPi.image.getTglUpdateAsTimestamp())){
            int countphn = Query.update("update produk_gambar set active = 0 where produk_id = ? AND active = 1",newp.id);
            //saveProdukRiwayat(newp,"produk_riwayat_datafeed_update_gambar");
            //$isGambarUpdated
            int posisi = 1;
            ProdukGambar pgb = null;
            //save
            List<ProdukGambar> dtGambar = new ArrayList<>();
            for (Item item:produkAPi.image.item) {
                pgb = new ProdukGambar();
                pgb.produk_id = newp.id;
                pgb.deskripsi = item.deskripsi;
                String filename ="";
                try {
                    int awal = item.image_300x300.lastIndexOf("/");
                    int akhir = item.image_300x300.length();
                    int lengthfile = (akhir - awal);
                    filename = item.image_300x300.substring(awal,lengthfile);
                }catch (Exception e){}
                pgb.file_name = filename;
                pgb.original_file_name = filename;
                pgb.file_url = item.image_300x300;
                pgb.thumb_url= item.image_50x50;
                pgb.posisi_file = posisi++;
                pgb.active = 1;
                pgb.modified_date = produkAPi.image.getTglUpdateAsTimestamp();
                pgb.created_by=-99;
                dtGambar.add(pgb);
            }
            //save all produk gambar
            ProdukGambar.saveAll(dtGambar);
            //Logger.info("save all produk gambar");
        }
    }
    public void updateOrAddLAmpiran(ProdukLampiran lampiran, models.api.produk.Produk produkAPi, Produk newp){
        //catatan: bila tidak ada produk lampiran yang aktif maka dianggap lanjut

        if((null == lampiran && null != produkAPi.lampiran) || (null != produkAPi.lampiran && lampiran.modified_date != produkAPi.lampiran.getTglUpdateAsTimestamp())){
            int countphn = Query.update("update produk_lampiran set active = 0 where produk_id = ? AND active = 1",newp.id);
            //saveProdukRiwayat(newp,"produk_riwayat_datafeed_update_lampiran");
            //$isLampiranUpdated = true;
            List<ProdukLampiran> dtlmp = new ArrayList<>();
            ProdukLampiran plm = null;
            for (models.api.produk.detail.lampiran.Item item:produkAPi.lampiran.item) {
                plm =  new ProdukLampiran();
                plm.produk_id = newp.id.intValue();
                plm.deskripsi = item.deskripsi;
                String filename ="";
                try {
                    int awal = item.file.lastIndexOf("/");
                    int akhir = item.file.length();
                    int lengthfile = (akhir - awal);
                    filename = item.file.substring(awal,lengthfile);
                }catch (Exception e){}
                plm.file_name = filename;
                plm.original_file_name = filename;
                plm.file_url = item.file;
                plm.active = 1;
                plm.modified_date = produkAPi.lampiran.getTglUpdateAsTimestamp();
                plm.created_by=-99;
                dtlmp.add(plm);
            }
            ProdukLampiran.saveAll(dtlmp);
            //Logger.info("save all produk lampiran");
        }
    }

    public void sendEmailReportToAdmin(String body, String title){
        Logger.info("sending report failed save produk:");
        String report = Configuration.getConfigurationValue(MailQueue.SMTP_MAIL_ADMIN, "");
        String[] ary = report.split(";");
        for (String to : ary) {
            MailQueue mq = new MailQueue();
            try {
                Logger.info(to+" "+body+" "+title);
                mq = EmailManager.createEmail(to, body, title);
            } catch (Exception e) {
                Logger.error("error create mail:"+e.getMessage());
            }
            Logger.info(mq.save()+"");
        }
    }
    public void sendEmailReport(String to, String body, String title){
        MailQueue mq = new MailQueue();
        try {
            //Logger.info("Error save produk"+to+" "+body+" "+title);
            mq = EmailManager.createEmail(to, body, title);
        } catch (Exception e) {
            Logger.error("error create mail:"+e.getMessage());
        }
    }
    private void createHargaNasional(Produk newP,models.api.produk.Produk produkAPi, PenyediaKomoditasApi pk){
        //createHarga retail
        List<ProdukHargaNasional> data = new ArrayList<>();

        ProdukHargaNasional retail = new ProdukHargaNasional(newP.id,produkAPi.harga.kurs_id, produkAPi.harga.harga_retail,pk.harga_retail);
        retail.modified_date = produkAPi.harga.getTglUpdateAsTimestamp();
        retail.active=true;
        retail.created_by=-99;
        ProdukHargaNasional pemerintah = retail;
        pemerintah.komoditas_harga_atribut_id = pk.harga_pemerintah;
        pemerintah.harga = produkAPi.harga.harga_pemerintah;

        ProdukHargaNasional ongkir = retail;
        ongkir.komoditas_harga_atribut_id = pk.harga_ongkir;
        ongkir.harga = produkAPi.harga.ongkos_kirim;

        //add all
        data.add(retail);
        data.add(pemerintah);
        data.add(ongkir);

        //saveAll
        data.add(retail);
        ProdukHargaNasional.saveAll(data);
        //Logger.info("create Harga Nasional");
    }

    private void saveProdukRiwayat(Produk newp, String aksi){
        ProdukRiwayat pr = new ProdukRiwayat();
        pr.produk_id = newp.id;
        pr.aksi = aksi;
        pr.created_by = -99;
        pr.created_date = new Timestamp(new Date().getTime());
        pr.save();
        //Logger.info("Produk riwayat save");
    }
    public Produk produkInfoToPrroduk (Produk newp, models.api.produk.Produk produkAPi){

        newp.agr_total_gambar = 0;// refer to ProductForm.getTotalPicture();
        newp.agr_total_lampiran = 0;// refer to ProductForm.getTotalAttachment();
        newp.agr_total_wilayah_jual=0;// refer to ProductForm.getAgrWilayahJual();
        newp.produk_kategori_id = produkAPi.informasi.id_kategori_produk_lkpp.intValue();
        newp.penyedia_id = penyediaKomoditasApi.penyediaid;
        newp.manufaktur_id = produkAPi.informasi.id_manufaktur;
        newp.unspsc_id = produkAPi.informasi.unspsc;
        newp.unit_pengukuran_id = produkAPi.informasi.id_unit_pengukuran_lkpp;
        newp.no_produk_penyedia = produkAPi.informasi.no_produk_penyedia;
        newp.url_produk = produkAPi.informasi.url_produk ;
        newp.image_800x800 = produkAPi.informasi.image_800x800 ;
        //50x50 , 300, 100 tidak ada
        //produk_gambar_thumb_url
        newp.komoditas_id = penyediaKomoditasApi.komoditasid.intValue();
        newp.jenis_produk = produkAPi.informasi.apakah_produk_lokal==1?"lokal":"impor" ;
        newp.active = produkAPi.informasi.produk_aktif == 1? true:false ;
        newp.jumlah_stok = new BigDecimal(produkAPi.informasi.kuantitas_stok);
        newp.modified_date = new Timestamp(produkAPi.informasi.getTglUpdateAsDate().getTime());
        newp.setuju_tolak_tanggal = new Date();
        newp.created_by = -99;
        newp.modified_by = -99;
        //apakah dapat diedit
        newp.active = produkAPi.informasi.produk_aktif==1 ;
        newp.setuju_tolak = "setuju";
        newp.harga_utama = new BigDecimal(produkAPi.harga.harga_pemerintah);
        newp.harga_ongkir = new BigDecimal(produkAPi.harga.ongkos_kirim) ;
        newp.kurs_id = produkAPi.harga.kurs_id ;
        newp.harga_tanggal = produkAPi.informasi.getTglUpdateAsDate();
        Double margin = (((produkAPi.harga.harga_retail - produkAPi.harga.harga_pemerintah)/produkAPi.harga.harga_retail)*100);
        newp.margin_harga = margin ;
        newp.berlaku_sampai = produkAPi.informasi.getBerlakuSampaiAsDate() ;
        newp.spesifikasi = new Gson().toJson(produkAPi.spesifikasi.item) ;
        newp.nama_produk = produkAPi.informasi.nama_produk ;
        //set no_produk
        newp.setProductNumber(penyediaKomoditasApi.kode_komoditas, produkAPi.informasi.unspsc);
        return newp;
    }

    public void executeSingle(Long idp){
        PenyediaKomoditasApi pk = getPenyediaKomoditasApi(idp);
        int defQjobwait = 3;//next 5
        int qjobwait = defQjobwait;//next 5

        ExtProdukJob9 job9 = new ExtProdukJob9(pk,defaultWaitime,nextWaitime);
        job9.in(qjobwait);
//            new Job(){
//                UUID id = UUID.randomUUID();
//                PenyediaKomoditasApi pke = pk;
//                @Override
//                public void doJob() {
//                    try {
//                        URL urls = new URL(pk.produk_api_url);
//                        Logger.info("get from url: "+pk.produk_api_url+" "+pk.start_date);
//                        //create new object for independence object
//                        ExtProduct exts = new ExtProduct();
//                        exts.penyediaKomoditasApi = pk;
//                        exts.defaultWaitime = defaultWaitime;
//                        exts.nextWaitime = nextWaitime;
//                        exts.urlForExecutejob = pk.produk_api_url;
//
//                        HistoryDatafeed hdf = new HistoryDatafeed();
//                        hdf.id_job = id.toString();
//                        hdf.penyedia_id = pk.penyediaid;
//                        hdf.penyedia_name = pk.nama_penyedia;
//                        hdf.start_time = new Timestamp(new Date().getTime()).toString();
//
//                        exts.getData(exts.defaultWaitime,exts.urlForExecutejob, hdf);
//
//                    }catch (MalformedURLException e) {
//                        Logger.error("not valid url penyedia:"+ pk.produk_api_url+" "+pk.start_date);
//                    }
//                }
//            }.in(qjobwait);
            //qjobwait = qjobwait+defQjobwait+1;
//        }catch (MalformedURLException e) {
//            Logger.error("not valid url penyedia:"+ pk.produk_api_url+" "+pk.start_date);
//        }

    }
}
