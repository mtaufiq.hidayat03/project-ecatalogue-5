package ext;

import controllers.katalog.ProdukCtr;
import groovy.lang.Closure;
import models.katalog.Komoditas;
import models.katalog.ProdukKategori;
import models.masterdata.Kldi;
import play.Logger;
import play.i18n.Lang;
import play.i18n.Messages;
import play.mvc.Http;
import play.mvc.Router;
import play.mvc.Scope.Flash;
import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;
import repositories.produkkategori.ProdukKategoriRepository;
import utils.DateTimeUtils;
import utils.KatalogUtils;
import utils.UrlUtil;

import java.io.PrintWriter;
import java.util.*;

public class CustomTag extends FastTags {

	/**
	 * Fungsi {@code _topmenu} merupakan custom tag Play!Framework yang digunakan untuk render menu setiap halaman
	 * pada SPSE, baik itu menu pada halaman publik maupun halaman user.
	 *
	 * @param args standar fast tag
	 * @param body standar fast tag
	 * @param out standar fast tag
	 * @param template standar fast tag
	 * @param fromLine standar fast tag
	 */
	public static void _alert(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		 StringBuilder content = new StringBuilder();
//       if(Validation.hasErrors()) { // error sebab oleh play.validation
//           StringBuilder msg_content = new StringBuilder();
//           List<play.data.validation.Error> list = Validation.errors();
//           list.remove(list.size() - 1);
//           for (play.data.validation.Error error:list)
//               if(StringUtils.isNotEmpty(error.message()))
//                   msg_content.append("<li>" + error.message() + "</li>");
//           if(StringUtils.isNotEmpty(msg_content.toString())) {
//               content.append("<div class=\"alert alert-danger alert-dismissable\">");
//               content.append("<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>");
//               content.append("<ul class=\"list-unstyled\">");
//               content.append(msg_content.toString());
//               content.append("</ul></div>");
//           }
//       }
//       else {
           Flash flash = Flash.current();
           if(flash != null && flash.get("error") != null) {	// error sebab diset oleh system spse
           	content.append("<div class=\"alert alert-danger\">").append(flash.get("error")).append("</div>");
           }
           if(flash != null && flash.get("success") != null) {
           	content.append("<div class=\"alert alert-success\">").append(flash.get("success")).append("</div>");
           }
//       }          
        out.print(content.toString());
	}

	public static void _information(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine){
		String message = (String) args.get("arg");
		String type = (String) args.get("type");
		StringBuilder content = new StringBuilder();

		if(type != null || type != ""){
			content.append("<div class=\"alert alert-"+type+"\">").append(message).append("</div>");
		}else{
			content.append("<div class=\"alert alert-info\">").append(message).append("</div>");
		}
		out.print(content.toString());
	}

	public static void _pagination(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Pageable page = (Pageable) args.get("arg");
		StringBuilder content = new StringBuilder();
		if (page.getTotal() > 0) {
			int from=1, to=1;
			content.append("<div class=\"row\">");
			if (page.getLimit() < page.getTotal()) {
				content.append("<div class=\"col-sm-9 col-xs-12 col-md-8\">");
				content.append("<ul class=\"pagination pagination-sm\">");
				content.append("<li ").append(!page.isPrevious() ? "class=\"disabled\"" : "").append(">");
				content.append("<a href=\"").append(page.isPrevious() ? page.getPrevLink() : "javascript:void(0)").append("\">" + Messages.get("sebelumnya.label") + "</a>");
				content.append("</li>");
				int start, size;
				if (page.getPageCount() <= Pageable.MAX_PAGE_ITEM_DISPLAY) {
					start = 1;
					size = page.getPageCount();
				} else {
					if (page.getCurPage() <= Pageable.MAX_PAGE_ITEM_DISPLAY - Pageable.MAX_PAGE_ITEM_DISPLAY / 2) {
						start = 1;
					} else if (page.getCurPage() >= page.getPageCount() - Pageable.MAX_PAGE_ITEM_DISPLAY / 2) {
						start = page.getPageCount() - Pageable.MAX_PAGE_ITEM_DISPLAY + 1;
					} else {
						start = page.getCurPage() - Pageable.MAX_PAGE_ITEM_DISPLAY / 2;
					}
					size = Pageable.MAX_PAGE_ITEM_DISPLAY;
				}
				from = start;
				for (int i = 0; i < size; i++) {
					content.append("<li ").append(page.getCurPage() == (start + i) ? "class=\"active\">" : ">");
					content.append("<a href=\"").append(page.getLink() + "offset=" + (start + i)).append("\">");
					content.append((start + i));
					content.append("</a>");
					content.append("</li>");
				}
				to = start + (size-1);
				content.append("<li ").append(!page.isNext() ? "class=\"disabled\"" : "").append(">");
				content.append("<a href=\"").append(page.isNext() ? page.getNextLink() : "javascript:void(0)").append("\">" + Messages.get("berikutnya.label") + "</a>");
				content.append("</li>");
				content.append("</ul>");
				content.append("</div>");

			}
			content.append("<div class=\"col-sm-3 col-xs-12 col-md-4\">");
			content.append("<div class=\"pagination\">").append(Messages.get("menampilkan.label") + " ").append(page.getCurrentTotal()).append(" " + Messages.get("sampai_halaman.label") + " ").append(page.from()).append(" " + Messages.get("dari_halaman.label") + " ").append(page.getTotal()).append(" " + Messages.get("data_halaman.label") + "</div>");
			content.append("</div>");
			content.append("</div>");
		}
		out.print(content.toString());
	}

	public static void _kategoriTree(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Http.Request request = Http.Request.current();
		final Long catId = request.params.get("cat", Long.class);
		Set<Long> cats = ProdukKategoriRepository.traceCategories(catId);
		Map<String, ProdukKategori> kategoriMap = (Map<String, ProdukKategori>) args.get("arg");
		StringBuilder content = new StringBuilder();
		content.append("<div class=\"kategori-list-item " + (cats.isEmpty() ? "selected" : "") + "\" data-value=\"-99\">")
				.append("<a class=\"kategori-item-link\">")
				.append(Messages.get("semua.label") + Messages.get("kategori.label"))
				.append("</a></div>");
		for(ProdukKategori p : kategoriMap.values()){
			if(p.parent_id == null || p.parent_id == 0){
				boolean hasChild = false;
				final String isSelected = cats.contains(p.id) ? "selected" : "";
				content.append("<div class=\"kategori-list-item "+isSelected+"\" data-value=\""+p.id+"\"><a class=\"kategori-item-link\">" +
						p.nama_kategori);
				for (String key : kategoriMap.keySet()){
					ProdukKategori pk = kategoriMap.get(key);
					if(!hasChild && key.contains("-"+p.id)){
						hasChild = true;
					}
				}
				if(hasChild){
					content.append(" <i class=\"pull-right fa fa-angle-right\"></i>");
				}

				content.append("</a></div>");
				if(hasChild){
					content.append("<div class=\"subkategori-list-item parent-item-"+p.id+"\">");
					content.append(printSubCategory2(kategoriMap, p.id, cats));
					content.append("</div>");
				}
			}
		}

		out.print(content.toString());
	}

	private static String printSubCategory2(Map<String, ProdukKategori> kategoriMap, Long parent, Set<Long> cats){
		StringBuilder content = new StringBuilder();
		for(ProdukKategori p : kategoriMap.values()){
			boolean hasChild = false;
			if(p.parent_id.equals(parent)){
				final String isSelected = cats.contains(p.id) ? "selected" : "";
				content.append("<div class=\"kategori-list-item "+isSelected+"\" data-value=\""+p.id+"\"><a class=\"kategori-item-link\">" +
						p.nama_kategori);
				for (String key : kategoriMap.keySet()){
					if(!hasChild && key.contains("-"+p.id)){
						hasChild = true;
					}
				}
				if(hasChild){
					content.append(" <i class=\"pull-right fa fa-angle-right\"></i>");
				}
				content.append("</a></div>");
				if(hasChild){
					content.append("<div class=\"subkategori-list-item parent-item-"+p.id+"\">");
					content.append(printSubCategory2(kategoriMap, p.id, cats));
					content.append("</div>");
				}
			}
		}
		return content.toString();
	}

	public static void _produkBreadcrumb(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Integer kategori = (Integer) args.get("arg");
		String nama = (String) args.get("nama");
		//String urlHome =Router.getFullUrl("controllers.PublikCtr.home");

		String lang = !Lang.get().isEmpty() ? Lang.get() : "id";
		UrlUtil.Builder builder = UrlUtil.builder()
				.setUrl("PublikCtr.home")
				.setParam("language", lang);
		String urlHome = builder.build();
		Logger.info("urlhome -> :"+urlHome);

		Boolean addedHome = args.containsKey("home") ? (Boolean) args.get("home") : true;
		StringBuilder content = new StringBuilder();
		List<String> breadcrumb = new ArrayList<>();
		content.append("<ol class=\"breadcrumb\">\n");
		if (addedHome) {
			content.append("<li><a href=\""+urlHome+"\">Home</a></li>\n");
		}
		ProdukKategori produkKategori = new ProdukKategori();
		if (kategori!=null) {
			produkKategori = ProdukKategori.find("id = ? and active = 1", kategori).first();
		}
		boolean isTheLast = false;
		int idx = 0;
		while (!isTheLast){
			if(produkKategori == null){
				break;
			}
			String urlBread = "#";

			try{
				Komoditas komo =  Komoditas.findById(produkKategori.komoditas_id);
				String slugKOmo = KatalogUtils.generateSlug(komo.nama_komoditas);
				urlBread = "/"+Lang.get()+"/katalog/produk/"+slugKOmo+"/"+komo.id+"?cat="+produkKategori.id;
			}catch (Exception e){}

			breadcrumb.add(idx, "<li><a href=\""+ urlBread + "\">"+produkKategori.nama_kategori+"</a></li>");
			idx += 1;

			if(produkKategori.parent_id == null || produkKategori.parent_id == 0){
				isTheLast = true;
				continue;
			}
			produkKategori = ProdukKategori.find("id = ? and active = 1", produkKategori.parent_id).first();
		}
		for(int i=breadcrumb.size()-1; i >= 0; i--){
			content.append(breadcrumb.get(i));
		}
		content.append("<li class=\"active\">"+nama+"</li>");
		content.append("</ol>");

		out.print(content.toString());
	}
	
	public static void _printBerita(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		String content = (String) args.get("arg");
		if(content != null)
			out.print(content.toString());
	}

	public static void _subKomoditasListItem(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		List<Komoditas> komoditasList = Komoditas.findAllActive();
		List<Komoditas> kldiSektoralTemp = Komoditas.findSektoralKldi();
		List<Komoditas> kldiLokalTemp = Komoditas.findLokalKldi();
		List<Kldi> kldiLokal = Kldi.findLokal();
		List<Kldi> Sektoral = Kldi.findSektoral();

		StringBuilder content = new StringBuilder();

		StringBuilder contentNasional = new StringBuilder().append("<div class=\"subkomoditas-list-item\" id=\"nasional-item\">");
		StringBuilder contentLokal = new StringBuilder().append("<div class=\"subkomoditas-list-item\" id=\"lokal-item\">");
		StringBuilder contentSektoral = new StringBuilder().append("<div class=\"subkomoditas-list-item\" id=\"sektoral-item\">");

		for (Komoditas kldis : kldiSektoralTemp){
			for (Kldi k : Sektoral){
//				Map<String,Object> params = new HashMap<String, Object>();
//				params.put("id",k.id);
//				String url = Router.getFullUrl("PublikCtr.komoditasKldiIndex",params);

				String lang = !Lang.get().isEmpty() ? Lang.get() : "id";
				UrlUtil.Builder builder = UrlUtil.builder()
						.setUrl("PublikCtr.komoditasKldiIndex")
						.setParam("language", lang)
						//.setParam("komoditas_slug", KatalogUtils.generateSlug(komoditas.nama_komoditas))
						.setParam("id", k.id);
				String url = builder.build();
				//Logger.info("urlsektoral -> :"+url);
				if (kldis.kldi_id.equalsIgnoreCase(k.id)){
					String result = "<div class=\"komoditas-list-item\"><a class=\"komoditas-item-link\" href=\""+url+"?first=true"+"\">"+k.nama+"</a></div>";
					contentSektoral.append(result);
				}
		}

		}

		for (Komoditas kldil : kldiLokalTemp){
			for(Kldi k : kldiLokal){
				if(kldil.kldi_id.equalsIgnoreCase(k.id)){
//					Map<String,Object> params = new HashMap<String, Object>();
//					params.put("id",k.id);
//					String url = Router.getFullUrl("PublikCtr.komoditasKldiIndex",params);

					String lang = !Lang.get().isEmpty() ? Lang.get() : "id";
					UrlUtil.Builder builder = UrlUtil.builder()
							.setUrl("PublikCtr.komoditasKldiIndex")
							.setParam("language", lang)
							//.setParam("komoditas_slug", KatalogUtils.generateSlug(komoditas.nama_komoditas))
							.setParam("id", k.id);
					String url = builder.build();
					//Logger.info("urllokal -> :"+url);
					String result = "<div class=\"komoditas-list-item\"><a class=\"komoditas-item-link\" href=\""+url+"?first=true"+"\">"+k.nama+"</a></div>";
					contentLokal.append(result);
				}
			}
		}

		for (Komoditas komoditas : komoditasList){
			Map<String,Object> params = new HashMap<String, Object>();

//			params.put("language", Lang.get().isEmpty() ? "id" : Lang.get());
//			params.put("komoditas_slug", KatalogUtils.generateSlug(komoditas.nama_komoditas));
//			params.put("id",komoditas.id);
//			String url = Router.getFullUrl("katalog.ProdukCtr.listProduk",params);

			String lang = !Lang.get().isEmpty() ? Lang.get() : "id";
			UrlUtil.Builder builder = UrlUtil.builder()
					.setUrl("katalog.ProdukCtr.listProduk")
					.setParam("language", lang)
					.setParam("komoditas_slug", KatalogUtils.generateSlug(komoditas.nama_komoditas))
					.setParam("id", komoditas.id);
			String url = builder.build();
			String result = "<div class=\"komoditas-list-item\"><a class=\"komoditas-item-link\" href=\""+url+"?first=true"+"\">"+komoditas.nama_komoditas+"</a></div>";

			if(komoditas.komoditas_kategori_id == Komoditas.KOMODITAS_LOKAL){//				contentLokal.append(result);

			}else if(komoditas.komoditas_kategori_id == Komoditas.KOMODITAS_SEKTORAL){
//				contentSektoral.append(result);
			}else {
				contentNasional.append(result);
			}

		}

		contentNasional.append("</div>");
		contentLokal.append("</div>");
		contentSektoral.append("</div>");

		content.append(contentNasional);
		content.append(contentLokal);
		content.append(contentSektoral);

		out.print(content.toString());
	}

	public static void _formatDate(Map<?,?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine){
		Date date = (Date) args.get("arg");
		String format = (String) args.get("format");
		String readableDate = "";

		if (format.equals("date")){
			readableDate = DateTimeUtils.parseToReadableDate(date);
		}else if(format.equals("datetime")){
			readableDate = DateTimeUtils.parseToReadableDateTime(date);
		}

		StringBuilder content = new StringBuilder();
		content.append(readableDate);

		out.print(content.toString());
	}

}