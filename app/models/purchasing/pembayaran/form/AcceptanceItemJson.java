package models.purchasing.pembayaran.form;

import models.purchasing.pembayaran.AcceptanceItem;
import org.apache.http.util.TextUtils;

/**
 * @author HanusaCloud on 11/1/2017
 */
public class AcceptanceItemJson {

    public Long id;
    public Long packageId;
    public String documentDate;
    public String documentNumber;
    public Double total;
    public String acceptanceDate;
    public String description;
    public Long paymentId;
    public String acceptanceUrl;

    public AcceptanceItemJson(AcceptanceItem model) {
        this.id = model.id;
        this.packageId = model.packageId;
        this.documentDate = model.getReadableDocumentDate();
        this.documentNumber = model.getFormattedDocNumber();
        this.total = model.total;
        this.acceptanceDate = model.getReadableAcceptanceDate();
        this.description = model.description;
        this.paymentId = model.paymentId;
        this.acceptanceUrl = model.getAcceptanceDetailUrl();
        if (TextUtils.isEmpty(this.description)) {
            this.description = "-";
        }
    }

    public Long getId() {
        return id;
    }

}
