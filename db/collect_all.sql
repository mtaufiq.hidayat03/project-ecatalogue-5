-- select * from configuration;
-- solusi next val
-- select auto_increment from information_schema.TABLES where TABLE_NAME ='dokumen_template' and TABLE_SCHEMA='lkpp_katalog_portal_30062018';
-- select auto_increment as id from information_schema.TABLES where TABLE_NAME ='dokumen_template' and TABLE_SCHEMA='lkpp_katalog_portal_30062018';
-- no 28, no 29, no 30 harus query manual
CREATE TABLE IF NOT EXISTS `configuration` (
  `cfg_category` varchar(80) NOT NULL,
  `cfg_sub_category` varchar(80) NOT NULL,
  `audittype` char(1) NOT NULL DEFAULT 'C',
  `audituser` varchar(100) NOT NULL DEFAULT 'ADMIN',
  `auditupdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `cfg_value` varchar(2000) DEFAULT NULL,
  `cfg_comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`cfg_category`,`cfg_sub_category`)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB;

-- configuration value
LOCK TABLES `configuration` WRITE;
/*!40000 ALTER TABLE `mail_queue` DISABLE KEYS */;
INSERT INTO configuration (cfg_category, cfg_sub_category, audittype, audituser, auditupdate, cfg_value, cfg_comment) VALUES ('CONFIG', 'file-storage-dir', 'C', 'ADMIN', '2018-09-13 11:44:00', '/Volumes/Data/ekatalogdev/storage', null);
INSERT INTO configuration (cfg_category, cfg_sub_category, audittype, audituser, auditupdate, cfg_value, cfg_comment) VALUES ('CONFIG', 'file.storage-dir', 'C', 'ADMIN', '2017-05-07 11:49:02', '/Volumes/Data/ekatalogdev/altstorage', null);
INSERT INTO configuration (cfg_category, cfg_sub_category, audittype, audituser, auditupdate, cfg_value, cfg_comment) VALUES ('CONFIG', 'fileImageGallery', 'C', 'ADMIN', '2018-09-19 16:05:50', '/public/files/image', '+ Play.applicationPath');
INSERT INTO configuration (cfg_category, cfg_sub_category, audittype, audituser, auditupdate, cfg_value, cfg_comment) VALUES ('CONFIG', 'job.sirup.waittime', 'C', 'ADMIN', '2018-08-14 13:27:12', '1', null);
INSERT INTO configuration (cfg_category, cfg_sub_category, audittype, audituser, auditupdate, cfg_value, cfg_comment) VALUES ('CONFIG', 'job.sirup.waittime.next', 'C', 'ADMIN', '2018-08-14 13:28:51', '1', null);
INSERT INTO configuration (cfg_category, cfg_sub_category, audittype, audituser, auditupdate, cfg_value, cfg_comment) VALUES ('CONFIG', 'kurs.max.retry', 'C', 'ADMIN', '2017-10-30 13:23:57', '5', null);
INSERT INTO configuration (cfg_category, cfg_sub_category, audittype, audituser, auditupdate, cfg_value, cfg_comment) VALUES ('CONFIG', 'kurs.retry.delay', 'C', 'ADMIN', '2017-10-30 13:23:58', '5', null);
INSERT INTO configuration (cfg_category, cfg_sub_category, audittype, audituser, auditupdate, cfg_value, cfg_comment) VALUES ('CONFIG', 'kurs.source.url', 'C', 'ADMIN', '2017-10-30 13:23:57', 'http://www.bi.go.id/id/moneter/informasi-kurs/transaksi-bi/Default.aspx', null);
INSERT INTO configuration (cfg_category, cfg_sub_category, audittype, audituser, auditupdate, cfg_value, cfg_comment) VALUES ('CONFIG', 'mail.delay', 'C', 'ADMIN', '2018-07-31 11:39:09', '10', null);
INSERT INTO configuration (cfg_category, cfg_sub_category, audittype, audituser, auditupdate, cfg_value, cfg_comment) VALUES ('CONFIG', 'mail.port', 'C', 'ADMIN', '2017-05-07 11:49:02', '25', null);
INSERT INTO configuration (cfg_category, cfg_sub_category, audittype, audituser, auditupdate, cfg_value, cfg_comment) VALUES ('CONFIG', 'mail.retry', 'C', 'ADMIN', '2017-05-07 14:20:12', '6', null);
INSERT INTO configuration (cfg_category, cfg_sub_category, audittype, audituser, auditupdate, cfg_value, cfg_comment) VALUES ('CONFIG', 'mail.sender', 'C', 'ADMIN', '2017-05-07 11:49:02', 'noreply-eproc@lkpp.go.id', null);
INSERT INTO configuration (cfg_category, cfg_sub_category, audittype, audituser, auditupdate, cfg_value, cfg_comment) VALUES ('CONFIG', 'mail.smtp.host', 'C', 'ADMIN', '2017-05-07 11:49:02', 'mail.lkpp.go.id', null);
INSERT INTO configuration (cfg_category, cfg_sub_category, audittype, audituser, auditupdate, cfg_value, cfg_comment) VALUES ('CONFIG', 'mail.smtp.password', 'C', 'ADMIN', '2018-07-31 11:39:09', 'eproc-lkpp##', null);
INSERT INTO configuration (cfg_category, cfg_sub_category, audittype, audituser, auditupdate, cfg_value, cfg_comment) VALUES ('CONFIG', 'mail.test', 'C', 'ADMIN', '2018-08-02 10:46:03', 'asep.sofyan@bening-semesta.com;af.permana@lkpp.go.id;asepbelajardanmengajar@gmail.com', null);
INSERT INTO configuration (cfg_category, cfg_sub_category, audittype, audituser, auditupdate, cfg_value, cfg_comment) VALUES ('CONFIG', 'mail.user', 'C', 'ADMIN', '2018-07-31 11:37:46', 'noreply-eproc@lkpp.go.id', null);
INSERT INTO configuration (cfg_category, cfg_sub_category, audittype, audituser, auditupdate, cfg_value, cfg_comment) VALUES ('CONFIG', 'pp.batas.belipaket', 'C', 'ADMIN', '2018-09-05 10:52:18', '200000000', null);
INSERT INTO configuration (cfg_category, cfg_sub_category, audittype, audituser, auditupdate, cfg_value, cfg_comment) VALUES ('CONFIG', 'rating.enable', 'U', 'ADMIN', '2018-09-28 14:45:54', '0', null);
/*!40000 ALTER TABLE `mail_queue` ENABLE KEYS */;
UNLOCK TABLES;

-- email job
CREATE TABLE IF NOT EXISTS `mail_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exception` text,
  `to_addresses` varchar(1000) DEFAULT NULL,
  `cc_addresses` varchar(1000) DEFAULT NULL,
  `bcc_addresses` varchar(1000) DEFAULT NULL,
  `subject` varchar(1000) DEFAULT NULL,
  `from_address` varchar(255) DEFAULT NULL,
  `body` text,
  `mime` decimal(1,0) DEFAULT '0',
  `retry` decimal(2,0) DEFAULT NULL,
  `enqueue_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `send_date` datetime DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `noemail` text,
  `lls_id` decimal(19,0) NOT NULL DEFAULT '0',
  `jenis` int(11) NOT NULL DEFAULT '0',
  `rkn_id` decimal(19,0) DEFAULT '0',
  `bcstat` decimal(1,0) NOT NULL DEFAULT '1',
  `prioritas` int(11) DEFAULT '2',
  `audittype` varchar(1) DEFAULT NULL,
  `auditupdate` datetime DEFAULT NULL,
  `audituser` varchar(100) DEFAULT NULL,
  `engine_version` varchar(50) DEFAULT NULL,
  `ref_1` bigint(20) DEFAULT NULL,
  `ref_2` bigint(20) DEFAULT NULL,
  `ref_3` bigint(20) DEFAULT NULL,
  `ref_a` varchar(100) DEFAULT NULL,
  `ref_b` varchar(100) DEFAULT NULL,
  `ref_c` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
)CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;

-- no 5 tahapan new
CREATE TABLE IF NOT EXISTS `tahapan_new` (
  `id` Int( 11 ) AUTO_INCREMENT NOT NULL,
  `nama_tahapan` VARCHAR(255) NOT NULL,
  `jenis_tahapan` CHAR(1) NOT NULL,
  `apakah_fitur_paten` INT(1) NULL,
  `created_date` DateTime NULL,
  `created_by` Int( 5 ) NULL,
  `modified_date` DateTime NULL,
  `modified_by` Int( 5 ) NULL,
  `deleted_date` DateTime NULL,
  `deleted_by` Int( 5 ) NULL,
  `audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `auditdate` Timestamp NULL,
  `audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `active` INT(1) NULL,
  PRIMARY KEY (`id`)
)CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;
-- insert data
LOCK TABLES `tahapan_new` WRITE;
INSERT INTO tahapan_new (id, nama_tahapan, jenis_tahapan, apakah_fitur_paten, created_date, created_by, modified_date, modified_by, deleted_date, deleted_by, audittype, auditdate, audituser, active) VALUES (1, 'Pemasukan Penawaran', '1', 1, null, null, null, null, null, null, null, null, null, null);
INSERT INTO tahapan_new (id, nama_tahapan, jenis_tahapan, apakah_fitur_paten, created_date, created_by, modified_date, modified_by, deleted_date, deleted_by, audittype, auditdate, audituser, active) VALUES (2, 'Evaluasi Administrasi dan Pembuktian Kualifikasi', '1', 1, null, null, null, null, null, null, null, null, null, null);
UNLOCK TABLES;
-- end data
-- no 6 new table template jadwal
CREATE TABLE IF NOT EXISTS `komoditas_template_jadwal`(
  `id` Int( 11 ) AUTO_INCREMENT NOT NULL,
  `komoditas_id` Int(11) NOT NULL,
  `nama_template` VARCHAR(255) NOT NULL,
  `deskripsi` VARCHAR(255) NULL,
  `agr_tahapan_jadwal` Int(11) NULL,
  `created_date` DateTime NULL,
  `created_by` Int( 5 ) NULL,
  `modified_date` DateTime NULL,
  `modified_by` Int( 5 ) NULL,
  `deleted_date` DateTime NULL,
  `deleted_by` Int( 5 ) NULL,
  `audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `auditdate` Timestamp NULL,
  `audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `active` INT(1) NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;

CREATE TABLE IF NOT EXISTS `komoditas_tahapan_template_jadwal`(
  `id` Int( 11 ) AUTO_INCREMENT NOT NULL,
  `tahapan_id`  Int(11) NOT NULL,
  `template_id`  Int(11) NOT NULL,
  `form_id`  Int(11) NULL,
  `created_date` DateTime NULL,
  `created_by` Int( 5 ) NULL,
  `modified_date` DateTime NULL,
  `modified_by` Int( 5 ) NULL,
  `deleted_date` DateTime NULL,
  `deleted_by` Int( 5 ) NULL,
  `audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `auditdate` Timestamp NULL,
  `audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `active` INT(1) NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;
-- no 7
-- Menambahkan kolom yang menjadi flag apakah template ini sudah dipakai atau belum
ALTER TABLE `komoditas_template_jadwal` ADD `apakah_dipakai` INT (1) DEFAULT 0;

-- no 8
-- Melakukan perubahan tabel tahapan lama menjadi tahapan yang baru
-- Menentukan fitur utama tahapan, yaitu Pemasukan Penawaran dan Klarifikasi Negosiasi dan Harga
ALTER TABLE `tahapan`
  ADD `jenis_tahapan` CHAR(1) NULL,
  ADD `apakah_fitur_paten` TinyInt(1) NOT NULL DEFAULT 0;

UPDATE `tahapan`
SET `apakah_fitur_paten` = 1
WHERE `nama_tahapan` = "pemasukan_penawaran" OR `nama_tahapan` = "klarifikasi_negosiasi_dan_harga";

ALTER TABLE `tahapan`
  DROP COLUMN `active`;

ALTER TABLE `tahapan`
  ADD `active` Int(11) NOT NULL DEFAULT 1;

-- no 9
CREATE TABLE IF NOT EXISTS `usulan_jadwal`(
  `id` Int( 11 ) AUTO_INCREMENT NOT NULL,
  `usulan_id` Int(11) NOT NULL,
  `tahapan_id` Int(11) NOT NULL,
  `tanggal_mulai` DateTime NOT NULL,
  `tanggal_selesai` DateTime NOT NULL,
  `adalah_awal_penawaran` INT(1) DEFAULT 0,
  `adalah_akhir_penawaran` INT(1) DEFAULT 0,
  `created_date` DateTime NULL,
  `created_by` Int( 5 ) NULL,
  `modified_date` DateTime NULL,
  `modified_by` Int( 5 ) NULL,
  `deleted_date` DateTime NULL,
  `deleted_by` Int( 5 ) NULL,
  `audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `auditdate` Timestamp NULL,
  `audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `active` INT(1) NULL,
  PRIMARY KEY (`id`)
)CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;

-- no 10
ALTER TABLE `usulan`
  ADD `komoditas_id` Int(11) NULL;
-- no 11
ALTER TABLE `usulan`
  ADD `kldi_id` VARCHAR(6) NULL;
-- no 12
ALTER TABLE `usulan`
  ADD `pengusul_nik` VARCHAR(20) NULL;

-- no 13
ALTER TABLE `penyedia_kontrak`
  ADD `komoditas_id` Int(11) NULL;

ALTER TABLE `penyedia_kontrak`
  ADD `file_hash` VARCHAR (255) NULL;

ALTER TABLE `penyedia_kontrak`
  ADD `file_signature` VARCHAR(255) NULL;

ALTER TABLE `penyedia_kontrak`
  ADD `dok_id_attachment` Int(11) NULL;

-- no 14 dokumen template
-- sudah change ke longtext pada field template
CREATE TABLE IF NOT EXISTS `dokumen_template`(
  `id` Int( 11 ) AUTO_INCREMENT NOT NULL,
  `konten_kategori_id` Int(11) NOT NULL,
  `jenis_dokumen` VARCHAR(255) NOT NULL,
  `template` longtext NULL,
  `created_date` DateTime NULL,
  `created_by` Int( 5 ) NULL,
  `modified_date` DateTime NULL,
  `modified_by` Int( 5 ) NULL,
  `deleted_date` DateTime NULL,
  `deleted_by` Int( 5 ) NULL,
  `audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `auditdate` Timestamp NULL,
  `audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `active` INT(1) NULL,
  PRIMARY KEY (`id`)
)CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;


INSERT INTO dokumen_template (id, konten_kategori_id, jenis_dokumen, template, created_date, created_by, modified_date, modified_by, deleted_date, deleted_by, audittype, auditdate, audituser, active) VALUES (1, 42, 'BERITA ACARA HASIL EVALUASI TEKNIS', '<p style="text-align: center;">KELOMPOK KERJA KATALOG ELEKTRONIK ${baEvalTeknik.komoditas}</p>
<p style="text-align: center;">LEMBAGA KEBIJAKAN PENGADAAN BARANG/JASA PEMERINTAH</p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;">Berita Acara Evaluasi Teknis, Evaluasi Harga, Klarifikasi Teknis dan Negosiasi Harga</p>
<p style="text-align: center;">Pemilihan Penyedia Katalog Elektronik Alat Kesehatan</p>
<p style="text-align: center;">PT. Anugrahmitra Selaras</p>
<p style="text-align: center;">No.: 126/ BAE-TH/ALKES/08/2017</p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: justify;">Pada hari ini Rabu, tanggal Tiga Puluh bulan Agustus tahun 2017 (30-08-2017) Pukul 10.00 s.d 12.30 WIB bertempat di Aston Bogor Hotel &amp; Resort Lantai 5 Ruang Padma V telah dilakukan Evaluasi, Klarifikasi, dan Negosiasi Teknis serta Harga terhadap PT. Anugrahmitra Selaras untuk Pemilihan Penyedia Katalog Elektronik:</p>
<ol style="list-style-type: lower-alpha;">
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;"><strong>Unsur-unsur yang di Evaluasi Teknis</strong>
<ol>
<li style="text-align: justify;">Daftar spesifikasi teknis dari seluruh produk yang ditawarkan</li>
<li style="text-align: justify;">Jaminan garansi produk dari produsen sekurang-kurangnya 1 tahun untuk tiap item produk(dikecualikan untuk barang habis pakai)</li>
<li>Jaminan ketersediaan sparepart sekurang-kurangnya 5 tahun untuk tiap item produk (dikecualikan untuk barang habis pakai)</li>
</ol>
</li>
</ol>
</ol>
<p>&nbsp;</p>
<ol style="list-style-type: lower-alpha;">
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;"><strong>Unsur-unsur yang di Evaluasi Harga</strong>
<ol>
<li style="text-align: justify;">Daftar harga penawaran retail&nbsp; non pemerintah dan pemerintah untuk setiap produk yang ditawarkan harus dalam bentuk rupiah atau mata uang asing lainnya.</li>
<li>Analisa harga satuan untuk setiap produk yang ditawarkan yang menggambarkan komponen perhitungan harga penawaran pemerintah, sudah termasuk biaya produksi (apabila ada), biaya operasional, biaya pengepakan (apabila ada), biaya instalasi (apabila ada), biaya testing (apabila ada), biaya pelatihan (apabila ada), pajak-pajak (termasuk PPN), bea retribusi, keuntungan, dan pungutan resmi lain yang sah . bBiaya &nbsp;operasionaloverhead, biaya pengiriman, keuntungan, pajak, bea, retribusi, dan pungutan lain yang sah serta biaya asuransi&nbsp; (apabila diperlukan) yang harus dibayar oleh penyedia untuk pelaksanaan pengadaan ini diperhitungkan dalam harga penawaran</li>
<li>Khusus produk impor, harus menyertakan dokumen PIB (Pemberitahuan Impor Barang) paling lama tahun 2015 beserta invoice pembelian barangnya.</li>
<li>Untuk produk dalam negeri, harus menyertakan invoice pembelian bahan baku paling lama tahun 2015 dan invoice penjualan produk paling lama tahun 2015.</li>
</ol>
</li>
</ol>
</ol>
<p>&nbsp;</p>
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;"><strong>Hasil Evaluasi, Klarifikasi, dan Negosiasi Teknis serta Harga (terlampir) dengan ketentuan:</strong>
<ol>
<li style="text-align: justify;">Berita acara ini ditanda tangani oleh Pokja Katalog Alat Kesehatan dan PT. Anugrahmitra Selaras tanpa ada unsur paksaan dan tidak dibawah tekanan.</li>
<li style="text-align: justify;">Penyedia menjamin bahwa hasil negosiasi ini merupakan harga dengan keuntungan yang wajar, sesuai dengan dokumen yang ditunjukkan tanpa mengurangi spesifikasi produk dan standard pelayanan.</li>
<li>Semua produk yang dinegosiasikan sudah termasuk biaya produksi, biaya operasional, biaya pengepakan, biaya testing, biaya pelatihan, pajak-pajak (termasuk PPN), bea retribusi, keuntungan, dan pungutan resmi lain yang sah . bBiaya &nbsp;operasionaloverhead, biaya pengiriman, keuntungan, pajak, bea, retribusi, dan pungutan lain yang sah serta biaya asuransi&nbsp; (apabila diperlukan) yang harus dibayar oleh penyedia untuk pelaksanaan pengadaan ini diperhitungkan dalam harga penawaran.</li>
<li>PT. Anugrahmitra selaras bersedia memberikan penggantian produk, apabila produk yang diterima dalam keadaan rusak.</li>
<li>Harga Franco DKI Jakarta. Area layanan meliputi seluruh Indonesia. Harga hasil negosiasi belum termasuk ongkos kirim. Penyedia menyatakan tidak akan mengambil keuntungan dari ongkos kirim tersebut. Ongkos kirim yang ditagihkan kepada K/L/D/I sesuai/sama dengan yang dibayarkan Penyedia kepada perusahaan ekspedisi (<em>at cost)</em>. Penyedia harus menyampaikan invoice ongkos kirim kepada K/L/D/I pemesan.</li>
<li>Penawaran yang tidak mendapatkan kesepakatan, maka penawaran tersebut tidak dapat diproses lebih lanjut dan dinyatakan gugur. Selanjutnya mekanisme pengusulan Barang/Jasa E-katalog mengacu pada Peraturan Kepala LKPP Nomor 6 Tahun 2016.</li>
<li>PT. Anugrahmitra Selaras bertanggung jawab atas keabsahan dan keaslian data dukung yang diberikan maupun yang ditunjukan kepada Pokja Katalog Alat Kesehatan</li>
</ol>
</li>
</ol>
<p style="text-align: justify;">Demikian Berita Acara Evaluasi Teknis, Klarifikasi Teknis dan Negosiasi Teknis dan Harga ini dibuat dan ditandatangani pada hari, tanggal dan bulan sebagaimana tersebut diatas untuk dipergunakan sebagaimana mestinya.</p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;"><strong>Kelompok Kerja Katalog Elektronik Alat Kesehatan</strong></p>
<p style="text-align: center;"><strong>Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah </strong></p>
<p style="text-align: center;">&nbsp;</p>
<table style="width: 100%;" cellspacing="0">
<tbody>
<tr>
<th>No</th>
<th>Nama</th>
<th>Jabatan</th>
<th>Tanda Tangan</th>
</tr>
<tr>
<td style="text-align: center; width: 10%;">
<ol>
<li>&nbsp;</li>
</ol>
</td>
<td>Mahsa Elvina Rahmawyanet</td>
<td>Pokja</td>
</tr>
<tr>
<td style="text-align: center;">
<ol start="2">
<li>&nbsp;</li>
</ol>
</td>
<td>Ikbal Amaludin</td>
<td>Pokja</td>
</tr>
<tr>
<td style="text-align: center;">
<ol start="3">
<li>&nbsp;</li>
</ol>
</td>
<td>Indah Nursyamsi Hanyani</td>
<td>Pokja</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;"><strong>PT.Anugrahitra Selaras </strong></p>
<p>&nbsp;</p>
<table style="width: 100%;" cellspacing="0">
<tbody>
<tr>
<th>No</th>
<th>Nama</th>
<th>Jabatan</th>
<th>Tanda Tangan</th>
</tr>
<tr>
<td style="text-align: center; width: 10%;">
<ol>
<li>&nbsp;</li>
</ol>
</td>
<td>Mahsa Elvina Rahmawyanet</td>
<td>Pokja</td>
</tr>
<tr>
<td style="text-align: center;">
<ol start="2">
<li>&nbsp;</li>
</ol>
</td>
<td>Ikbal Amaludin</td>
<td>Pokja</td>
</tr>
<tr>
<td style="text-align: center;">
<ol start="3">
<li>&nbsp;</li>
</ol>
</td>
<td>Indah Nursyamsi Hanyani</td>
<td>Pokja</td>
</tr>
</tbody>
</table>', null, 2, '2018-08-07 13:24:41', 436086, null, null, null, null, null, 0);
INSERT INTO dokumen_template (id, konten_kategori_id, jenis_dokumen, template, created_date, created_by, modified_date, modified_by, deleted_date, deleted_by, audittype, auditdate, audituser, active) VALUES (2, 43, 'BERITA ACARA HASIL EVALUASI KUALIFIKASI', '<p style="text-align:center">TIM PENGEMBANGAN KATALOG ELEKTRONIK</p>

<p style="text-align:center">LEMBAGA KEBIJAKAN PENGADAAN BARANG/JASA PEMERINTAH</p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center">Berita Acara Hasil Evaluasi dan Pembuktian Kualifikasi dan Administrasi</p>

<p style="text-align:center">Pemilihan Penyedia Katalog Elektronik ${dok.namaKomoditas}</p>

<p style="text-align:center">${dok.namaPenyedia}</p>

<p style="text-align:center">No.:${dok.nomorSurat}</p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:justify">Pada hari ini ${dok.hari}, tanggal ${dok.tanggal} bulan ${dok.bulan} tahun ${dok.tahun} Pukul ${dok.jam} bertempat di <em>Lembaga Kebijakan </em><em>Pengadaan</em><em> Barang/Jasa Pemerintah</em>, telah dilakukan Evaluasi dan Pembuktian Kualifikasi dan Administrasi ${dok.namaPenyedia} untuk Pemilihan Penyedia Katalog Elektronik ${dok.namaKomoditas}:</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:left"><strong>A. </strong><strong>Hasil Evaluasi dan Pembuktian Kualifikasi sebagai berikut :</strong></p>

<p>${dok.listKualifikasi}</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:left"><strong>B. </strong><strong>Hasil </strong><strong>Evaluasi Administrasi </strong><strong>sebagai berikut :</strong></p>

<p>${dok.hasilEvaluasi}</p>

<p style="text-align:justify"><strong>Kesimpulan: ${dok.kesimpulan}</strong></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">Demikian Berita Acara Hasil Evaluasi dan Pembuktian Kualifikasi dan Administrasi ini dibuat dan ditandatangani pada hari, tanggal dan bulan sebagaimana tersebut diatas untuk dipergunakan sebagaimana mestinya.</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:center"><strong>Tim Pengembangan Katalog Elektronik</strong></p>

<p style="text-align:center"><strong>Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah </strong></p>

<p style="text-align:center">&nbsp;</p>

<p>${dok.timPengembangKatalog}</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:center"><strong>${dok.namaPenyedia}</strong></p>

<p style="text-align:center">&nbsp;</p>

<p>${dok.ttdPenyedia}</p>

<p style="text-align:justify">&nbsp;</p>
', null, 2, '2017-11-23 17:01:18', 2, null, null, null, null, null, 0);
INSERT INTO dokumen_template (id, konten_kategori_id, jenis_dokumen, template, created_date, created_by, modified_date, modified_by, deleted_date, deleted_by, audittype, auditdate, audituser, active) VALUES (3, 44, 'SURAT PESANAN', '<p style="text-align: center;"><strong>SURAT PESANAN</strong></p>
<table style="width: 100%;" border="1px solid black" cellspacing="0">
<tbody>
<tr>
<th style="vertical-align: middle; width: 55%;" rowspan="2"><strong>SURAT PESANAN (SP)</strong></th>
<td style="vertical-align: top;">
<p style="text-align: justify;">SATUAN KERJA PEJABAT PENANDATANGAN/PENGESAHAN TANDA BUKTI PERJANJIAN :</p>
</td>
</tr>
<tr>
<td>
<p style="text-align: justify;">NOMOR DAN TANGGAL SP :</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Yang bertanda tangan di bawah ini :</p>
<p>&nbsp;</p>
<p>${paket?.buyer.nama_lengkap}</p>
<p>${paket?.buyer.jabatan}</p>
<p>${paket?.satuan_kerja_alamat}&amp;nbsp;- ${paket?.satuan_kerja_kabupaten_nama} -${paket?.satuan_kerja_provinsi_nama}</p>
<p>selanjutnya disebut sebagai Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian;</p>
<p>&nbsp;</p>
<p>${paket?.provider?.nama_penyedia}</p>
<p>${paket?.provider?.alamat}</p>
<p>selanjutnya disebut sebagai Penyedia;</p>
<p>&nbsp;</p>
<p>untuk mengirimkan barang dengan memperhatikan ketentuan-ketentuan sebagai berikut :</p>
<hr />
<p>Rincian Barang</p>
<p style="text-align: justify;">#{include ''produkTemplate.html''/}</p>
<p>&nbsp;</p>
<p>TERBILANG : ${terbilangHarga}</p>
<hr />
<p>SYARAT DAN KETENTUAN :</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Hak dan Kewajiban
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;">Penyedia
<ol>
<li style="text-align: justify;">Penyedia memiliki hak menerima pembayaran atas pembelian barang sesuai dengan total harga dan waktu yang tercantum di dalam SP ini.</li>
<li style="text-align: justify;">Penyedia memiliki kewajiban:
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;">tidak membuat dan/atau menyampaikan dokumen dan/atau keterangan lain yang tidak benar untuk memenuhi persyaratan Katalog Elektronik;</li>
<li style="text-align: justify;">tidak menjual barang melalui e-Purchasing lebih mahal dari harga barang yang dijual selain melalui e-Purchasing pada periode penjualan, jumlah, dan tempat serta spesifikasi teknis dan persyaratan yang sama;</li>
<li style="text-align: justify;">mengirimkan barang sesuai spesifikasi dalam SP ini selambat-lambatnya pada (tanggal/bulan/tahun) sejak SP ini diterima oleh Penyedia;</li>
<li style="text-align: justify;">bertanggungjawab atas keamanan, kualitas, dan kuantitas barang yang dipesan;</li>
<li style="text-align: justify;">mengganti barang setelah Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian melalui Pejabat/Panitia Penerima Hasil Pekerjaan (PPHP) melakukan pemeriksaan barang dan menemukan bahwa:
<ol>
<li style="text-align: justify;">barang rusak akibat cacat produksi;</li>
<li style="text-align: justify;">barang rusak pada saat pengiriman barang hingga barang diterima oleh Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian; dan/atau</li>
<li style="text-align: justify;">barang yang diterima tidak sesuai dengan spesifikasi barang sebagaimana tercantum pada SP ini.</li>
</ol>
</li>
<li style="text-align: justify;">memberikan layanan tambahan yang diperjanjikan seperti instalasi, testing, dan pelatihan (apabila ada);</li>
<li style="text-align: justify;">memberikan layanan purnajual sesuai dengan ketentuan garansi masing-masing barang.</li>
</ol>
</li>
</ol>
</li>
<li style="text-align: justify;">PEJABAT PENANDATANGAN/PENGESAHAN TANDA BUKTI PERJANJIAN
<ol>
<li style="text-align: justify;">Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian memiliki hak:
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;">menerima barang dari Penyedia sesuai dengan spesifikasi yang tercantum di dalam SP ini.</li>
<li style="text-align: justify;">mendapatkan jaminan keamanan, kualitas, dan kuantitas barang yang dipesan;</li>
<li style="text-align: justify;">mendapatkan penggantian barang, dalam hal:
<ol>
<li style="text-align: justify;">barang rusak akibat cacat produksi;</li>
<li style="text-align: justify;">barang rusak pada saat pengiriman barang hingga barang diterima oleh Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian; dan/atau</li>
<li style="text-align: justify;">barang yang diterima tidak sesuai dengan spesifikasi barang sebagaimana tercantum pada SP ini.</li>
</ol>
</li>
<li style="text-align: justify;">Mendapatkan layanan tambahan yang diperjanjikan seperti instalasi, testing, dan pelatihan (apabila ada);</li>
<li style="text-align: justify;">Mendapatkan layanan purnajual sesuai dengan ketentuan garansi masing-masing barang.</li>
</ol>
</li>
<li style="text-align: justify;">Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian memiliki kewajiban:
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;">melakukan pembayaran sesuai dengan total harga yang tercantum di dalam SP ini; dan</li>
<li style="text-align: justify;">memeriksa kualitas dan kuantitas barang;</li>
<li style="text-align: justify;">memastikan layanan tambahan telah dilaksanakan oleh penyedia seperti instalasi, testing, dan pelatihan (apabila ada).</li>
</ol>
</li>
</ol>
</li>
</ol>
</li>
</ol>
</li>
</ol>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Waktu Pengiriman Barang</li>
</ol>
</li>
</ol>
<p style="text-align: justify;">Penyedia mengirimkan barang dan melaksanakan laysesuai spesifikasi dalam SP ini selambat-lambatnya pada (tanggal/bulan/tahun)sejak SP ini diterima oleh Penyedia.</p>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Alamat Pengiriman Barang</li>
</ol>
</li>
</ol>
<p style="text-align: justify;">Penyedia mengirimkan barang ke alamat sebagai berikut:</p>
<p>${paket?.pengiriman_alamat} - ${paket?.pengiriman_kabupaten_nama} - ${paket?.pengiriman_provinsi_nama}</p>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Tanggal Barang Diterima</li>
</ol>
</li>
</ol>
<p style="text-align: justify;">Barang diterima pada .....................</p>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Penerimaan, Pemeriksaan, dan Retur Barang
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;">Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian melalui PPHP menerima barang dan melakukan pemeriksaan barang berdasarkan ketentuan di dalam SP ini.</li>
<li style="text-align: justify;">Dalam hal pada saat pemeriksaan barang, Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian menemukan bahwa:
<ol>
<li style="text-align: justify;">barang rusak akibat cacat produksi;</li>
<li style="text-align: justify;">barang rusak pada saat pengiriman barang hingga barang diterima oleh Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian; dan/atau</li>
<li style="text-align: justify;">barang yang diterima tidak sesuai dengan spesifikasi barang sebagaimana tercantum pada SP ini.</li>
</ol>
<p style="text-align: justify;">Maka Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian dapat menolak penerimaan barang dan menyampaikan pemberitahuan tertulis kepada Penyedia atas cacat mutu atau kerusakan barang tersebut.</p>
</li>
<li style="text-align: justify;">Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian dapat meminta Tim Teknis untuk melakukan pemeriksaan atau uji mutu terhadap barang yang diterima.</li>
<li style="text-align: justify;">Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian dapat memerintahkan Penyedia untuk menemukan dan mengungkapkan cacat mutu serta melakukan pengujian terhadap barang yang dianggap Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian mengandung cacat mutu atau kerusakan.</li>
<li style="text-align: justify;">Penyedia bertanggungjawab atas cacat mutu atau kerusakan barang dengan memberikan penggantian barang selambat-lambatnya (${suratPesanan?.hariKerja}) hari kerja.</li>
</ol>
</li>
</ol>
</li>
</ol>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Harga
<ol>
<li style="text-align: justify;">Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian membayar kepada Penyedia atas pelaksanaan pekerjaan sebesar harga yang tercantum pada SP ini.</li>
<li style="text-align: justify;">Harga SP telah memperhitungkan keuntungan, pajak, biaya overhead, biaya pengiriman, biaya asuransi, biaya layanan tambahan (apabila ada) dan biaya layanan purna jual.</li>
<li style="text-align: justify;">Rincian harga SP sesuai dengan rincian yang tercantum dalam daftar kuantitas dan harga.</li>
</ol>
</li>
</ol>
</li>
</ol>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Perpajakan</li>
</ol>
</li>
</ol>
<p style="text-align: justify;">Penyedia berkewajiban untuk membayar semua pajak, bea, retribusi, dan pungutan lain yang sah yang dibebankan oleh hukum yang berlaku atas pelaksanaan SP. Semua pengeluaran perpajakan ini dianggap telah termasuk dalam harga SP.</p>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Pengalihan dan/atau subkontrak
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;">Pengalihan seluruh Kontrak hanya diperbolehkan dalam hal terdapat pergantian nama Penyedia, baik sebagai akibat peleburan (merger), konsolidasi, atau pemisahan.</li>
<li style="text-align: justify;">Pengalihan sebagian pelaksanaan Kontrak dilakukan dengan ketentuan sebagai berikut:
<ol>
<li style="text-align: justify;">Pengalihan sebagian pelaksanaan Kontrak untuk barang/jasa yang bersifat standar dilakukan untuk pekerjaan seperti pengiriman barang (distribusi barang) dari Penyedia kepada Kementerian/Lembaga/Satuan Kerja Perangkat Daerah/Institusi; dan</li>
<li style="text-align: justify;">Pengalihan sebagian pelaksanaan Kontrak dapat dilakukan untuk barang/jasa yang bersifat tidak standar misalnya untuk pekerjaan konstruksi (minor), pengadaan ambulans, ready mix, hot mix dan lain sebagainya.</li>
</ol>
</li>
</ol>
</li>
</ol>
</li>
</ol>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Perubahan SP
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;">SP hanya dapat diubah melalui adendum SP.</li>
<li style="text-align: justify;">Perubahan SP dapat dilakukan apabila disetujui oleh para pihak dalam hal terjadi perubahan jadwal pengiriman barang atas permintaan Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian atau permohonan Penyedia yang disepakati oleh Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian.</li>
</ol>
</li>
</ol>
</li>
</ol>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Peristiwa Kompensasi
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;">Peristiwa Kompensasi dapat diberikan kepada penyedia dalam hal Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian terlambat melakukan pembayaran prestasi pekerjaan kepada Penyedia.</li>
<li style="text-align: justify;">Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian dikenakan ganti rugi atas keterlambatan pembayaran sebesar ${suratPesanan?.gantiRugi}.</li>
</ol>
</li>
</ol>
</li>
</ol>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Hak Atas Kekayaan Intelektual
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;">Penyedia berkewajiban untuk memastikan bahwa barang yang dikirimkan/dipasok tidak melanggar Hak Atas Kekayaan Intelektual (HAKI) pihak manapun dan dalam bentuk apapun.</li>
<li style="text-align: justify;">Penyedia berkewajiban untuk menanggung Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian dari atau atas semua tuntutan, tanggung jawab, kewajiban, kehilangan, kerugian, denda, gugatan atau tuntutan hukum, proses pemeriksaan hukum, dan biaya yang dikenakan terhadap Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian sehubungan dengan klaim atas pelanggaran HAKI, termasuk pelanggaran hak cipta, merek dagang, hak paten, dan bentuk HAKI lainnya yang dilakukan atau diduga dilakukan oleh Penyedia.</li>
</ol>
</li>
</ol>
</li>
</ol>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Jaminan Bebas Cacat Mutu/Garansi
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;">Penyedia dengan jaminan pabrikan dari produsen pabrikan (jika ada) berkewajiban untuk menjamin bahwa selama penggunaan secara wajar oleh Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian, Barang tidak mengandung cacat mutu yang disebabkan oleh tindakan atau kelalaian Penyedia, atau cacat mutu akibat desain, bahan, dan cara kerja.</li>
<li style="text-align: justify;">Jaminan bebas cacat mutu ini berlaku sampai dengan 12 (dua belas) bulan setelah serah terima Barang atau jangka waktu lain yang ditetapkan dalam SP ini.</li>
<li style="text-align: justify;">Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian akan menyampaikan pemberitahuan cacat mutu kepada Penyedia segera setelah ditemukan cacat mutu tersebut selama Masa Layanan Purnajual.</li>
<li style="text-align: justify;">Terhadap pemberitahuan cacat mutu oleh Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian, Penyedia berkewajiban untuk memperbaiki atau mengganti Barang dalam jangka waktu yang ditetapkan dalam pemberitahuan tersebut.</li>
<li style="text-align: justify;">Jika Penyedia tidak memperbaiki atau mengganti Barang akibat cacat mutu dalam jangka waktu yang ditentukan, maka Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian akan menghitung biaya perbaikan yang diperlukan dan Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian secara langsung atau melalui pihak ketiga yang ditunjuk oleh Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian akan melakukan perbaikan tersebut. Penyedia berkewajiban untuk membayar biaya perbaikan atau penggantian tersebut sesuai dengan klaim yang diajukan secara tertulis oleh Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian. Biaya tersebut dapat dipotong oleh Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian dari nilai tagihan Penyedia.</li>
</ol>
</li>
</ol>
</li>
</ol>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Pembayaran
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;">pembayaran prestasi hasil pekerjaan yang disepakati dilakukan oleh Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian, dengan ketentuan:
<ol>
<li style="text-align: justify;">penyedia telah mengajukan tagihan;</li>
<li style="text-align: justify;">pembayaran dilakukan dengan .................................; dan</li>
<li style="text-align: justify;">pembayaran harus dipotong denda (apabila ada) dan pajak.</li>
</ol>
</li>
<li style="text-align: justify;">pembayaran terakhir hanya dilakukan setelah pekerjaan selesai 100% (seratus perseratus) dan bukti penyerahan pekerjaan diterbitkan.</li>
<li style="text-align: justify;">Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian melakukan proses pembayaran atas pembelian barang selambat-lambatnya (..........) hari kerja setelah PPK menilai bahwa dokumen pembayaran lengkap dan sah.</li>
</ol>
</li>
</ol>
</li>
</ol>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Sanksi
<ol style="list-style-type: lower-alpha;">
<li>
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;">Penyedia dikenakan sanksi apabila:
<ol>
<li style="text-align: justify;">Tidak menanggapi pesanan barang selambat-lambatnya (..........) hari kerja;</li>
<li style="text-align: justify;">Tidak dapat memenuhi pesanan sesuai dengan kesepakatan dalam transaksi melalui e-Purchasing dan SP ini tanpa disertai alasan yang dapat diterima; dan/atau</li>
<li style="text-align: justify;">menjual barang melalui proses e-Purchasing dengan harga yang lebih mahal dari harga Barang/Jasa yang dijual selain melalui e-Purchasing pada periode penjualan, jumlah, dan tempat serta spesifikasi teknis dan persyaratan yang sama.</li>
</ol>
</li>
<li style="text-align: justify;">Penyedia yang melakukan perbuatan sebagaimana dimaksud dalam huruf a dikenakan sanksi administratif berupa:
<ol>
<li style="text-align: justify;">peringatan tertulis;</li>
<li style="text-align: justify;">denda; dan</li>
<li style="text-align: justify;">pelaporan kepada LKPP untuk dilakukan:
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;">penghentian sementara dalam sistem transaksi e-Purchasing; atau</li>
<li style="text-align: justify;">penurunan pencantuman dari Katalog Elektronik (e-Catalogue).</li>
</ol>
</li>
</ol>
</li>
<li style="text-align: justify;">Tata Cara Pengenaan Sanksi</li>
</ol>
</li>
</ol>
<p style="text-align: justify;">Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian mengenakan sanksi sebagaimana dimaksud dalam huruf a dan huruf b berdasarkan ketentuan mengenai sanksi sebagaimana diatur dalam Peraturan Kepala LKPP tentang e-Purchasing.</p>
</li>
</ol>
</li>
</ol>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Penghentian dan Pemutusan SP
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;">Penghentian SP dapat dilakukan karena pekerjaan sudah selesai atau terjadi Keadaan Kahar.</li>
<li style="text-align: justify;">Pemutusan SP oleh Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian
<ol>
<li style="text-align: justify;">Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian dapat melakukan pemutusan SP apabila:
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;">kebutuhan barang/jasa tidak dapat ditunda melebihi batas berakhirnya SP;</li>
<li style="text-align: justify;">berdasarkan penelitian Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian, Penyedia tidak akan mampu menyelesaikan keseluruhan pekerjaan walaupun diberikan kesempatan sampai dengan 50 (lima puluh) hari kalender sejak masa berakhirnya pelaksanaan pekerjaan untuk menyelesaikan pekerjaan;</li>
<li style="text-align: justify;">setelah diberikan kesempatan menyelesaikan pekerjaan sampai dengan 50 (lima puluh) hari kalender sejak masa berakhirnya pelaksanaan pekerjaan, Penyedia Barang/Jasa tidak dapat menyelesaikan pekerjaan;</li>
<li style="text-align: justify;">Penyedia lalai/cidera janji dalam melaksanakan kewajibannya dan tidak memperbaiki kelalaiannya dalam jangka waktu yang telah ditetapkan;</li>
<li style="text-align: justify;">Penyedia terbukti melakukan KKN, kecurangan dan/atau pemalsuan dalam proses Pengadaan yang diputuskan oleh instansi yang berwenang; dan/atau</li>
<li style="text-align: justify;">pengaduan tentang penyimpangan prosedur, dugaan KKN dan/atau pelanggaran persaingan sehat dalam pelaksanaan pengadaan dinyatakan benar oleh instansi yang berwenang.</li>
</ol>
</li>
<li style="text-align: justify;">Pemutusan SP sebagaimana dimaksud pada angka 1) dilakukan selambat-lambatnya (..........) hari kerja setelah Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian menyampaikan pemberitahuan rencana pemutusan SP secara tertulis kepada Penyedia.</li>
</ol>
</li>
<li style="text-align: justify;">Pemutusan SP oleh Penyedia
<ol>
<li style="text-align: justify;">Penyedia dapat melakukan pemutusan Kontrak jika terjadi hal-hal sebagai berikut:
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;">akibat keadaan kahar sehingga Penyedia tidak dapat melaksanakan pekerjaan sesuai ketentuan SP atau adendum SP;</li>
<li style="text-align: justify;">Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian gagal mematuhi keputusan akhir penyelesaian perselisihan; atau</li>
<li style="text-align: justify;">Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian tidak memenuhi kewajiban sebagaimana dimaksud dalam SP atau Adendum SP.</li>
</ol>
</li>
<li style="text-align: justify;">Pemutusan SP sebagaimana dimaksud pada angka 1) dilakukan selambat-lambatnya (..........) kerja setelah Penyedia menyampaikan pemberitahuan rencana pemutusan SP secara tertulis kepada Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian.</li>
</ol>
</li>
</ol>
</li>
</ol>
</li>
</ol>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Denda Keterlambatan Pelaksanaan Pekerjaan</li>
</ol>
</li>
</ol>
<p style="text-align: justify;">Penyedia yang terlambat menyelesaikan pekerjaan dalam jangka waktu sebagaimana ditetapkan dalam SP ini karena kesalahan Penyedia, dikenakan denda keterlambatan sebesar 1/1000 (satu perseribu) dari total harga atau dari sebagian total harga sebagaimana tercantum dalam SP ini untuk setiap hari keterlambatan.</p>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Keadaan Kahar
<ol style="list-style-type: lower-alpha;">
<li style="text-align: justify;">Keadaan Kahar adalah suatu keadaan yang terjadi diluar kehendak para pihak dan tidak dapat diperkirakan sebelumnya, sehingga kewajiban yang ditentukan dalam SP menjadi tidak dapat dipenuhi.</li>
<li style="text-align: justify;">Dalam hal terjadi Keadaan Kahar, Penyedia memberitahukan tentang terjadinya Keadaan Kahar kepada Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian secara tertulis dalam waktu selambat-lambatnya 14 (empat belas) hari kalender sejak terjadinya Keadaan Kahar yang dikeluarkan oleh pihak/instansi yang berwenang sesuai ketentuan peraturan perundang-undangan.</li>
<li style="text-align: justify;">Tidak termasuk Keadaan Kahar adalah hal-hal merugikan yang disebabkan oleh perbuatan atau kelalaian para pihak.</li>
<li style="text-align: justify;">Keterlambatan pelaksanaan pekerjaan yang diakibatkan oleh terjadinya Keadaan Kahar tidak dikenakan sanksi.</li>
<li style="text-align: justify;">Setelah terjadinya Keadaan Kahar, para pihak dapat melakukan kesepakatan, yang dituangkan dalam perubahan SP.</li>
</ol>
</li>
</ol>
</li>
</ol>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Penyelesaian Perselisihan</li>
</ol>
</li>
</ol>
<p style="text-align: justify;">Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian dan penyedia berkewajiban untuk berupaya sungguh-sungguh menyelesaikan secara damai semua perselisihan yang timbul dari atau berhubungan dengan SP ini atau interpretasinya selama atau setelah pelaksanaan pekerjaan. Jika perselisihan tidak dapat diselesaikan secara musyawarah maka perselisihan akan diselesaikan melalui arbitrase, mediasi, konsiliasi atau pengadilan negeri dalam wilayah hukum Republik Indonesia.</p>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Larangan Pemberian Komisi</li>
</ol>
</li>
</ol>
<p style="text-align: justify;">Penyedia menjamin bahwa tidak satu pun personil satuan kerja Pejabat Penandatangan/Pengesahan Tanda Bukti Perjanjian telah atau akan menerima komisi dalam bentuk apapun (gratifikasi) atau keuntungan tidak sah lainnya baik langsung maupun tidak langsung dari SP ini. Penyedia menyetujui bahwa pelanggaran syarat ini merupakan pelanggaran yang mendasar terhadap SP ini.</p>
<p>&nbsp;</p>
<ol>
<li>
<ol>
<li style="text-align: justify;">Masa Berlaku SP</li>
</ol>
</li>
</ol>
<p style="text-align: justify;">SP ini berlaku sejak tanggal SP ini ditandatangani oleh para pihak sampai dengan selesainya pelaksanaan pekerjaan.</p>
<ol>
<li>.................................................</li>
</ol>
<p>&nbsp;</p>
<p style="text-align: justify;">Demikian SP ini dibuat dan ditandatangani dalam 2 (dua) rangkap bermaterai dan masing-masing memiliki kekuatan hukum yang sama.</p>
<p style="text-align: justify;">&nbsp;</p>
<table style="width: 100%;" cellspacing="0">
<tbody>
<tr>
<td style="width: 50%;">
<p style="text-align: center;">Untuk dan atas nama ${paket?.kldi?.nama}</p>
<p style="text-align: center;">Pejabat Penandatangan/Pengesahan<br />Tanda Bukti Perjanjian</p>
<p>&nbsp;</p>
<p style="text-align: center;"><em>[tanda tangan dan cap(jika salinan<br />asli ini untuk Penyedia maka rekatkan<br />materai Rp.6.000,-)]</em></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="text-align: center;">${paket?.buyer.nama_lengkap}</p>
<p style="text-align: center;">${paket?.buyer.jabatan}</p>
</td>
<td style="width: 50%;">
<p style="text-align: center;">Untuk dan atas nama Penyedia/Kemitraan (KSO)</p>
<p style="text-align: center;">${paket?.distributor?.nama_distributor}</p>
<p>&nbsp;</p>
<p style="text-align: center;"><em>[tanda tangan dan cap(jika salinan asli ini untuk<br />proyek/satuan kerja Pejabat Pembuat Komitmen<br />maka rekatkan materai Rp.6.000,-)]</em></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="text-align: center;">${paket?.provider?.nama_penyedia}</p>
<p style="text-align: center;">${paket?.provider?.alamat}</p>
</td>
</tr>
</tbody>
</table>
<p style="text-align: center;">&nbsp;</p>', null, 2, '2018-09-18 15:01:18', 2, null, null, null, null, null, 0);
INSERT INTO dokumen_template (id, konten_kategori_id, jenis_dokumen, template, created_date, created_by, modified_date, modified_by, deleted_date, deleted_by, audittype, auditdate, audituser, active) VALUES (4, 45, 'SK PENETAPAN PRODUK', '<p style="text-align:center"><img alt="" src="/public/files/image/logo-lkpp.png" style="height:12%; width:20%" /></p>

<p style="text-align:center;margin:0;">&nbsp;</p>

<p style="text-align:center;margin:0 0 5px 0">LEMBAGA KEBIJAKAN PENGADAAN BARANG/JASA PEMERINTAH</p>

<p style="text-align:center;margin:0 0 5px 0;">REPUBLIK INDONESIA</p>

<p style="margin:0;">&nbsp;</p>

<p style="text-align:center;margin:0 0 5px 0;">KEPUTUSAN</p>

<p style="text-align:center;margin:0 0 5px 0;">KEPALA LEMBAGA KEBIJAKAN PENGADAAN BARANG/JASA PEMERINTAH</p>

<p style="margin:0;">&nbsp;</p>

<p style="text-align:center;margin:0 0 5px 0;">NOMOR :&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TAHUN</p>

<p style="margin:0;">&nbsp;</p>

<p style="text-align:center;margin:0 0 5px 0;">TENTANG</p>

<p style="margin:0;">&nbsp;</p>

<p style="text-align:center;margin:0 0 5px 0;text-transform:uppercase;">PENETAPAN PRODUK ${dokumenPenetapanProduk.namaKomoditas}</p>

<p style="text-align:center;margin:0 0 5px 0;text-transform:uppercase;">${dokumenPenetapanProduk.namaBadanUsaha}</p>

<p style="margin:0;">&nbsp;</p>

<p style="text-align:center;margin:0 0 5px 0;">KEPALA LEMBAGA KEBIJAKAN PENGADAAN BARANG/JASA PEMERINTAH</p>

<p style="margin:0;">&nbsp;</p>

<table cellspacing="0" style="width:100%">
  <tbody>
    <tr style="line-height:150%">
      <td style="vertical-align:top; width:15%">Menimbang</td>
      <td style="vertical-align:top">:</td>
      <td colspan="2" style="vertical-align:top; width:85%">
      <p style="text-align:justify; margin:0;">Bahwa untuk melaksanakan ketentuan Pasal 110 ayat (2a) Peraturan Presiden Nomor 54 Tahun 2010 tentang Pengadaan Barang/Jasa Pemerintah berikut perubahannya perlu menetapkan Keputusan Kepala Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah tentang Penetapan Produk ${dokumenPenetapanProduk.namaKomoditas} <span style="text-transform: capitalize;">${dokumenPenetapanProduk.namaBadanUsaha};</span></p>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr style="line-height:150%">
      <td style="vertical-align:top">Mengingat</td>
      <td style="vertical-align:top">:</td>
      <td style="vertical-align:top">1.</td>
      <td style="vertical-align:top">
      <p style="text-align:justify; margin:0;">Peraturan Presiden Nomor 106 Tahun 2007 tentang Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah sebagaimana diubah dengan Peraturan Presiden Nomor 157 Tahun 2014 tentang Perubahan Atas Peraturan Presiden Nomor 106 Tahun 2007 tentang Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah;</p>
      </td>
    </tr>
    <tr style="line-height:150%">
      <td colspan="2" style="vertical-align:top">&nbsp;</td>
      <td style="vertical-align:top">2.</td>
      <td style="vertical-align:top">
      <p style="text-align:justify; margin:0;">Peraturan Presiden Nomor 54 Tahun 2010 tentang Pengadaan Barang/Jasa Pemerintah sebagaimana diubah dengan Peraturan Presiden Nomor 4 Tahun 2015 tentang Perubahan Keempat Atas Peraturan Presiden Nomor 54 Tahun 2010 tentang Pengadaan Barang/Jasa Pemerintah;</p>
      </td>
    </tr>
    <tr style="line-height:150%">
      <td colspan="2" style="vertical-align:top">&nbsp;</td>
      <td style="vertical-align:top">3.</td>
      <td style="vertical-align:top">
      <p style="text-align:justify; margin:0;">Peraturan Kepala Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah No.6 Tahun 2016 tentang Katalog Elektronik dan <em>E-Purchasing</em>;</p>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr style="line-height:150%">
      <td style="vertical-align:top">Memperhatikan</td>
      <td style="vertical-align:top">:</td>
      <td style="vertical-align:top">1.</td>
      <td style="vertical-align:top">
      <p style="text-align:justify; margin:0;">Dokumen Pengadaan Penyediaan ${dokumenPenetapanProduk.namaKomoditasDokumen};</p>
      </td>
    </tr>
    <tr style="line-height:150%">
      <td colspan="2" style="vertical-align:top">&nbsp;</td>
      <td style="vertical-align:top">2.</td>
      <td style="vertical-align:top">
      <p style="text-align:justify; margin:0;">Surat ${dokumenPenetapanProduk.jabatanPihakKedua} <span style="text-transform: capitalize;">${dokumenPenetapanProduk.namaBadanUsaha}</span> Nomor ${dokumenPenetapanProduk.nomorSurat} tertanggal ${dokumenPenetapanProduk.tanggalSurat} perihal ${dokumenPenetapanProduk.perihalSurat};</p>
      </td>
    </tr>
    <tr style="line-height:150%">
      <td colspan="2" style="vertical-align:top">&nbsp;</td>
      <td style="vertical-align:top">3.</td>
      <td style="vertical-align:top">
      <p style="text-align:justify; margin:0;">Berita Acara Negosiasi Harga Pengadaan Katalog ${dokumenPenetapanProduk.namaKomoditas} <span style="text-transform: capitalize;">${dokumenPenetapanProduk.namaBadanUsaha}</span> Nomor ${dokumenPenetapanProduk.nomorBeritaAcaraNego} tertanggal ${dokumenPenetapanProduk.tanggalBeritaAcaraNego};</p>
      </td>
    </tr>
  </tbody>
</table>

<p style="margin:0;">&nbsp;</p>
<p style="margin:0;">&nbsp;</p>
<p style="margin:0;">&nbsp;</p>

<p style="text-align:center; margin:0 0 10px 0;">MEMUTUSKAN :</p>

<p style="margin:0;">&nbsp;</p>

<table cellspacing="0" style="width:100%">
  <tbody>
    <tr style="line-height:150%">
      <td style="vertical-align:top; width:15%">Menetapkan</td>
      <td style="vertical-align:top">:</td>
      <td colspan="2" style="vertical-align:middle; width:85%">
      <p style="text-align:justify; margin:0;text-transform:uppercase;">KEPUTUSAN KEPALA LEMBAGA KEBIJAKAN PENGADAAN BARANG/JASA PEMERINTAH TENTANG PENETAPAN PRODUK ${dokumenPenetapanProduk.namaKomoditas} ${dokumenPenetapanProduk.namaBadanUsaha}</p>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr style="line-height:150%">
      <td style="vertical-align:top">KESATU</td>
      <td style="vertical-align:top">:</td>
      <td colspan="2" style="vertical-align:middle">
      <p style="text-align:justify; margin:0;">Berdasarkan Dokumen Pengadaan Katalog Elektronik dengan ini menetapkan:</p>
      </td>
    </tr>
  </tbody>
</table>

<p style="margin:0;">&nbsp;</p>

<table cellspacing="0" style="width:100%">
  <tbody>
    <tr style="line-height: 150%">
      <td colspan="2" style="width:15%">&nbsp;</td>
      <td style="vertical-align:top; width: 25%">Nama Penyedia</td>
      <td style="vertical-align:top;">:</td>
      <td style="vertical-align: middle;">
      <p style="text-align:justify; margin:0;text-transform: capitalize;">${dokumenPenetapanProduk.namaBadanUsaha}</p>
      </td>
    </tr>
    <tr style="line-height: 150%">
      <td colspan="2" style="width:15%">&nbsp;</td>
      <td style="vertical-align:top; width: 25%">Alamat</td>
      <td style="vertical-align:top;">:</td>
      <td style="vertical-align: middle;">
      <p style="text-align:justify; margin:0;text-transform: capitalize;">${dokumenPenetapanProduk.alamatBadanUsaha}</p>
      </td>
    </tr>
    <tr style="line-height: 150%">
      <td colspan="2" style="width:15%">&nbsp;</td>
      <td style="vertical-align:top; width: 25%">Data Produk dan Harga</td>
      <td style="vertical-align:top;">:</td>
      <td style="vertical-align: middle;">
      <p style="text-align:justify; margin:0;">Terlampir</p>
      </td>
    </tr>
  </tbody>
</table>

<p style="margin:0;">&nbsp;</p>

<table cellspacing="0" style="width:100%">
  <tbody>
    <tr style="line-height: 150%">
      <td style="vertical-align:top; width:15%">KEDUA</td>
      <td style="vertical-align:top">:</td>
      <td colspan="2" style="vertical-align:middle; width:85%">
      <p style="text-align:justify; margin:0;">Penetapan ini sebagai syarat Produk ${dokumenPenetapanProduk.namaKomoditas} ${dokumenPenetapanProduk.namaBadanUsaha} untuk ditayangkan ke dalam Katalog Elektronik.</p>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr style="line-height: 150%">
      <td style="vertical-align:top">KETIGA</td>
      <td style="vertical-align:top">:</td>
      <td colspan="2" style="vertical-align:middle">
      <p style="text-align:justify; margin:0;">Keputusan ini berlaku pada tanggal ditetapkan sampai dengan berakhirnya masa berlaku Kontrak Katalog.</p>
      </td>
    </tr>
  </tbody>
</table>

<p style="margin:0;">&nbsp;</p>

<table cellspacing="0" style="width:100%">
  <tbody>
    <tr style="line-height: 150%">
      <td style="vertical-align:top; width:65%">&nbsp;</td>
      <td style="vertical-align:middle; width:35%">
      <p style="text-align:left; margin:0;">Ditetapkan di : Jakarta</p>
      </td>
    </tr>
    <tr style="line-height: 150%">
      <td style="vertical-align:top">&nbsp;</td>
      <td style="vertical-align:middle">
      <p style="text-align:left; margin:0;">pada tanggal :</p>
      </td>
    </tr>
    <tr style="line-height: 150%">
      <td style="vertical-align:top">&nbsp;</td>
      <td style="vertical-align:middle">
      <p style="text-align:left; margin:0;">KEPALA LEMBAGA KEBIJAKAN</p>
      </td>
    </tr>
    <tr style="line-height: 150%">
      <td style="vertical-align:top">&nbsp;</td>
      <td style="vertical-align:middle">
      <p style="text-align:left; margin:0;">PENGADAAN BARANG/JASA</p>
      </td>
    </tr>
    <tr style="line-height: 150%">
      <td style="vertical-align:top">&nbsp;</td>
      <td style="vertical-align:middle">
      <p style="text-align:left; margin:0;">PEMERINTAH,</p>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr style="line-height: 150%">
      <td style="vertical-align:top">&nbsp;</td>
      <td style="vertical-align:middle">
      <p style="text-align:left; margin:0;">${dokumenPenetapanProduk.namaPihakPertama}</p>
      </td>
    </tr>
  </tbody>
</table>

<p style="margin:0;">&nbsp;</p>

<p style="margin:0;">&nbsp;</p>

<p style="margin:0;">&nbsp;</p>

<p style="margin:0;">&nbsp;</p>

<p style="margin:0;">&nbsp;</p>

<p style="margin:0;">&nbsp;</p>

<table cellspacing="0" style="width:100%">
  <tbody>
    <tr style="line-height: 150%">
      <td style="vertical-align:middle; width:50%">
      <p style="text-align:left; margin:0;">Tembusan:</p>
      </td>
    </tr>
    <tr style="line-height: 150%">
      <td style="vertical-align:middle; width:50%">
      <p style="text-align:left; margin:0;">1. Sekretaris Utama LKPP;</p>
      </td>
    </tr>
    <tr style="line-height: 150%">
      <td style="vertical-align:middle; width:50%">
      <p style="text-align:left; margin:0;">2. Deputi Bidang Monitoring-Evaluasi dan Pengembangan Sistem Informasi;</p>
      </td>
    </tr>
    <tr style="line-height: 150%">
      <td style="vertical-align:middle; width:50%">
      <p style="text-align:left; margin:0;text-transform: capitalize;">3. ${dokumenPenetapanProduk.namaBadanUsaha}.</p>
      </td>
    </tr>
  </tbody>
</table>

<p style="margin:0;">&nbsp;</p>

<p style="margin:0;">&nbsp;</p>

<table cellspacing="0" style="width:100%">
  <tbody>
    <tr style="line-height: 150%">
      <td style="vertical-align:top;width:55%">&nbsp;</td>
      <td style="vertical-align:top;width:15%">LAMPIRAN</td>
      <td style="vertical-align:top;width:5%">:</td>
      <td colspan="2" style="vertical-align:middle; width:25%">
      <p style="text-align:justify;margin:0;text-transform:uppercase;">KEPUTUSAN KEPALA LEMBAGA KEBIJAKAN PENGADAAN BARANG/JASA PEMERINTAH TENTANG PENETAPAN PRODUK ${dokumenPenetapanProduk.namaKomoditas} ${dokumenPenetapanProduk.namaBadanUsaha}</p>
      </td>
    </tr>
    <tr style="line-height: 150%">
      <td style="vertical-align:top">&nbsp;</td>
      <td style="vertical-align:top">NOMOR</td>
      <td style="vertical-align:top">:</td>
      <td colspan="2" style="vertical-align:middle">&nbsp;</td>
    </tr>
    <tr style="line-height: 150%">
      <td style="vertical-align:top">&nbsp;</td>
      <td style="vertical-align:top">TANGGAL</td>
      <td style="vertical-align:top">:</td>
      <td colspan="2" style="vertical-align:middle">&nbsp;</td>
    </tr>
  </tbody>
</table>

<p style="margin:0;">&nbsp;</p>

<p style="text-align:center;margin:0 0 5px 0;text-transform: uppercase;">PRODUK ${dokumenPenetapanProduk.namaKomoditas} ${dokumenPenetapanProduk.namaBadanUsaha}</p>

<p style="margin:0;">&nbsp;</p>

<p style="text-align:justify; margin:0;">${dokumenPenetapanProduk.tblProdukHarga}</p>', null, 2, '2017-12-17 19:18:21', 2, null, null, null, null, null, 0);
INSERT INTO dokumen_template (id, konten_kategori_id, jenis_dokumen, template, created_date, created_by, modified_date, modified_by, deleted_date, deleted_by, audittype, auditdate, audituser, active) VALUES (5, 46, 'KONTRAK KATALOG', '<p style="text-align:center;margin: 0;text-transform:uppercase;">KELOMPOK KERJA KATALOG ELEKTRONIK ${dok.namaKomoditas}</p>

<p style="text-align:center;margin: 0">LEMBAGA KEBIJAKAN PENGADAAN BARANG/JASA PEMERINTAH</p>
<hr style="margin-bottom: 0px;"/>
<p></p>
<p style="text-align:center;margin:0">Berita Acara Hasil Evaluasi dan Pembuktian Kualifikasi dan Administrasi</p>

<p style="text-align:center;margin: 0">Pemilihan Penyedia Katalog Elektronik ${dok.namaKomoditas}</p>

<p style="text-align:center; margin: 0">${dok.namaPenyedia}</p>

<p style="text-align:center; margin: 0">No.:${dok.nomorSurat}</p>


<p style="text-align:justify">Pada hari ini ${dok.hari}, tanggal ${dok.tanggal} bulan ${dok.bulan} tahun ${dok.tahun} Pukul ${dok.jam} bertempat di di <em>Lembaga Kebijakan </em><em>Pengadaan</em><em> Barang/Jasa Pemerintah</em>, telah dilakukan Evaluasi Kualifikasi dan Administrasi serta Pembuktian Kualifikasi terhadap ${dok.namaPenyedia} untuk Pemilihan Penyedia Katalog Elektronik ${dok.namaKomoditas}:</p>

<table style="width:100%">

  <ol style="list-style-type:upper-alpha">
    <li><strong>Hasil Evaluasi dan Pembuktian Kualifikasi sebagai berikut :</strong></li>
    <p>${dok.listKualifikasi}</p>
    <br/>
    <p>${dok.listEvaluasiAdministrasi}</p>
    <br/>
    <p>${dok.listEvaluasiHarga}</p>
    <br/>
    <p>${dok.listEvaluasiTeknis}</p>

              <p style="text-align:justify"><strong>Kesimpulan : Lulus/Tidak Lulus Evaluasi Kualifikasi</strong></p>


              <li><strong>Hasil Evaluasi Kualifikasi sebagai berikut :</strong></li>



          <p style="text-align:justify"><strong>Lulus/Tidak Lulus Evaluasi Kualifikasi</strong></p>


          <li><strong>Hasil Pembuktian Kualifikasi</strong></li>
          <table border="1" cellspacing="0" style="width:100%">
            <tr>
              <th rowspan="2">No</th>
              <th rowspan="2">Uraian kualifikasi sebagai berikut:</th>
              <th rowspan="2">Keterangan Dokumen</th>
              <th colspan="2">Hasil Evaluasi Kualifikasi</th>


            </tr>
            <tr>
              <th>Tercantum</th>
              <th>Tidak Tercantum</th>
            </tr>
            <tr>
              <td>
                <ol>
                  <li>&nbsp;</li>
                </ol>
              </td>
              <td>
                Penyedia Produsen/Pabrikan:
                <ol type="a">
                  <li>
                    Memiliki Akta Pendirian dan Pengesahan Kementerian Hukum dan HAM;
                  </li>
                  <li>Memiliki Perubahan Terakhir Akta Pendirian dan Pengesahan Kementerian Hukum dan HAM (apabila ada);
                  </li>
                  <li>
                    Memiliki Surat Izin Usaha Industri/Tanda Daftar Industri (TDI) milik Perusahaan Produsen Alat Kesehatan / Original Equipment Manufacturer (OEM) / Makloon Alat Kesehatan;
                  </li>
                  <li>Memiliki Sertifikat produksi alat kesehatan milik Perusahaan Produsen / Original Equipment Manufacturer (OEM) / Makloon Alat Kesehatan / bukti perpanjangan (Sebelum proses pemuatan produk di e-Katalog Penyedia harus menunjukkan sertifikat produksi alat kesehatan terbaru);</li>
                  <li>
                    Memiliki Surat Ijin Usaha Perdagangan (SIUP) milik Perusahaan Produsen Alat Kesehatan atau Agen / Agen Tunggal / Ditributor / Distributor Tunggal;
                  </li>
                  <li>
                    Memiliki Tanda Daftar Perusahaan (TDP);
                  </li>
                  <li>
                    Memiliki Surat Keterangan Domisili;
                  </li>
                  <li>
                    Memiliki Sertifikat Merek/Surat Keterangan Pengurusan Sertifikat Merek dari Kementerian Hukum dan HAM (apabila ada);
                  </li>
                  <li>
                    Memiliki Nomor Pokok Wajib Pajak (NPWP);
                  </li>
                  <li>
                    Memiliki Surat Pengukuhan Pengusaha Kena Pajak (SPPKP);
                  </li>
                  <li>
                    Memiliki Surat Pemberitahuan Tahunan (SPT) Pajak Terakhir;
                  </li>
                  <li>
                    Bukti perjanjian kerjasama antara Pemegang Merek dan OEM / Makloon Alat Kesehatan;
                  </li>
                  <li>
                    Untuk OEM atau Makloon Alat Kesehatan dari luar negeri memiliki:
                    <li>
                      Pemberitahuan Import Barang (PIB);
                    </li>
                    <li>
                      Angka Pengenal Importir (API).
                    </li>

                  </li>
                  <li>
                    Memiliki IPAK milik Perusahaan Produsen Alat Kesehatan atau Agen / Agen Tunggal / Ditributor / Distributor Tunggal;
                  </li>
                  <li>
                    Memiliki AKD / Surat Keterangan Pengurusan Perpanjangan izin edar milik Perusahaan Produsen Alat Kesehatan atau Agen / Agen Tunggal / Ditributor / Distributor Tunggal; dan
                  </li>
                </ol>
              </td>
              <td>
              </td>
              <td>
              </td>
              <td>
              </td>
            </tr>
            <tr>
              <td>
                <ol start="2">
                  <li>
                  </li>
                </ol>
              </td>
              <td>
                Penyedia Sebagai Agen/AgenTunggal/Distributor/Distributor Tunggal:
                <ol type="a">
                  <li>
                    Memiliki Akta Pendirian dan Pengesahan Kementerian Hukum dan HAM;
                  </li>
                  <li>
                    Memiliki Perubahan Terakhir Akta Pendirian dan Pengesahan Kementerian Hukum dan HAM (apabila ada);
                  </li>
                  <li>
                    Memiliki Surat Ijin Usaha Perdagangan (SIUP);
                  </li>
                  <li>
                    Memiliki Tanda Daftar Perusahaan (TDP);
                  </li>
                  <li>
                    Memiliki Surat Tanda Pendaftaran (STP)/Surat Keterangan Pengurusan Perpanjangan STP sebagai Agen/Agen Tunggal/Distributor Tunggal Produk Dalam/Luar Negeri dari Kementerian Perdagangan;
                  </li>
                  <li>
                    Memiliki Nomor Identitas Kepabeanan;
                  </li>
                  <li>
                    Memiliki Surat Keterangan Domisili;
                  </li>
                  <li>
                    Memiliki Nomor Pokok Wajib Pajak (NPWP);
                  </li>
                  <li>
                    Memiliki Surat Pengukuhan Pengusaha Kena Pajak (SPPKP);
                  </li>
                  <li>
                    Memiliki Surat Pemberitahuan Tahunan (SPT) Pajak Terakhir;
                  </li>
                  <li>
                    Memiliki IPAK milik Perusahaan Produsen Alat Kesehatan, atau Agen / Agen Tunggal / Ditributor / Distributor Tunggal;
                  </li>
                  <li>
                    Memiliki AKL / AKD / Surat Keterangan Pengurusan Perpanjangan izin edar;
                  </li>
                  <li>
                    Memiliki Perjanjian Kerjasama dengan Principle/LOA;
                  </li>
                  <li>
                    Untuk produk dari Luar negeri memiliki:
                    <li>
                      Angka Pengenal Importir (API) bagi Importir; dan
                    </li>
                    <li>
                      Memiliki Pemberitahuan Import Barang (PIB).
                    </li>
                  </li>
                </ol>
              </td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>
                <ol start="3">
                  <li>
                  </li>
                </ol>
              </td>
              <td>
                Penyedia PMA Sebagai Distributor (Wholesaler)
                <ol type="a">
                  <li>
                    Menunjuk perusahaan perdagangan nasional sebagai agen, agen tunggal, distributor atau distributor tunggal;
                  </li>
                  <li>
                    Penunjukan sebagaimana dimaksud pada huruf a dibuat dalam bentuk perjanjian yang dilegalisir oleh notaris;
                  </li>
                  <li>
                    Perjanjian dengan perusahaan perdagangan nasional sebagaimana dimaksud pada huruf b harus mendapat persetujuan tertulis dari prinsipal produsen yang diwakilinya di luar negeri;
                  </li>
                  <li>
                    Memiliki Akta Pendirian dan Pengesahan Kementerian Hukum dan HAM serta Perubahan Terakhir Akta Pendirian dan Pengesahan Kementerian Hukum dan HAM (apabila ada);
                  </li>
                  <li>
                    Memiliki Tanda Daftar Perusahaan (TDP);
                  </li>
                  <li>
                    Memiliki Nomor Identitas Kepabeanan (Apabila Ada);
                  </li>
                  <li>
                    Memiliki Nomor Pokok Wajib Pajak (NPWP);
                  </li>
                  <li>
                    Memiliki Surat Pemberitahuan Tahunan (SPT) Pajak Terakhir;
                  </li>
                  <li>
                    Memiliki Izin Penyalur Alat Kesehatan (IPAK);
                  </li>
                  <li>
                    Memiliki AKL / AKD / Surat Keterangan Pengurusan Perpanjangan izin edar;
                  </li>
                  <li>
                    Memiliki Pemberitahuan Import Barang (PIB) paling lama Tahun 2015.
                  </li>
                </ol>
              </td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </table>
          <p style="text-align:justify"><strong>Kesimpulan : Lulus/Tidak Lulus Pembuktian Kualifikasi</strong></p>


          <p>&nbsp;</p>

          <p style="text-align:justify">Demikian Berita Acara Hasil Evaluasi Kualifikasi dan Administrasi serta Pembuktian Kualifikasiini dibuat dan ditandatangani pada hari, tanggal dan bulan sebagaimana tersebut diatas untuk dipergunakan sebagaimana mestinya. </p>
          </ol>
          </table>


          <p style="text-align:center">Kelompok Kerja Katalog Elektronik Alat Kesehatan</p>
          <p style="text-align:center">Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah</p>
          <table style="width:100%">
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Jabatan</th>
              <th>Tanda Tangan</th>
            </tr>
            <tr><td>
              <ol><li></li></ol>
            </td>
            <td>Eko Rinaldo Octavianus</td>
            <td>Pokja</td>
            <td></td>
          </tr>
          <tr><td>
            <ol start="2">
              <li></li>
            </ol>
          </td>
          <td>Muhammad Harris</td>
          <td>Pokja</td>
          <td></td>
        </tr>
        <tr><td>
          <ol start="3">
            <li></li>
          </ol>
        </td>
        <td>Muhamad Saifudin</td>
        <td>Pokja</td>
        <td></td>
      </tr>
      <tr><td>
        <ol start="4">
          <li></li>
        </ol>
      </td>
      <td>M. Qadr Siddiq Zam</td>
      <td>Pokja</td>
      <td></td>
    </tr>
    <tr>
    <td>
      <ol start="5">
        <li></li>
      </ol>
    </td>
    <td>Lulu Haryani</td>
    <td>Pokja</td>
    <td></td>
  </tr>
  <tr><td>
    <ol start="6">
      <li></li>
    </ol>
  </td>
  <td>Rr. Ajeng Kusharyeni</td>
  <td>Pokja</td>
  <td></td>
</tr>
<tr><td>
  <ol start="7">
    <li></li>
  </ol>
</td>
<td>Bambang Saputra</td>
<td>Pokja</td>
<td></td>
</tr>
<tr><td>
  <ol start="8">
    <li></li>
  </ol>
</td>
<td>Meleis Susanti</td>
<td>Pokja</td>
<td></td>
</tr>
<tr><td>
  <ol start="9">
    <li></li>
  </ol>
</td>
<td>Heldi Yudiyatna</td>
<td>Pokja</td>
<td></td>
</tr>
</table>


<p>&nbsp;</p>
<p style="text-align:center">PT/CV/Firma</p>
<table style="width: 100%">
  <tr>
    <th>No</th>
    <th>Nama</th>
    <th>Jabatan</th>
    <th>Tanda Tangan</th>
  </tr>
  <tr>
    <td>
      <ol>
        <li>

        </li>
      </ol>
    </td>
    <td></td>
    <td></td>
    <td></td>
  </tr>

</table>



', null, 2, null, null, null, null, null, null, null, 0);
INSERT INTO dokumen_template (id, konten_kategori_id, jenis_dokumen, template, created_date, created_by, modified_date, modified_by, deleted_date, deleted_by, audittype, auditdate, audituser, active) VALUES (6, 47, 'BERITA ACARA EVALUASI', '<p style="text-align:center;margin: 0;text-transform:uppercase;">KELOMPOK KERJA KATALOG ELEKTRONIK ${dok.namaKomoditas}</p>

<p style="text-align:center;margin: 0">LEMBAGA KEBIJAKAN PENGADAAN BARANG/JASA PEMERINTAH</p>
<hr style="margin-bottom: 0px;"/>
<p></p>
<p style="text-align:center;margin:0">Berita Acara Hasil Evaluasi dan Pembuktian Kualifikasi dan Administrasi</p>

<p style="text-align:center;margin: 0">Pemilihan Penyedia Katalog Elektronik ${dok.namaKomoditas}</p>

<p style="text-align:center; margin: 0">${dok.namaPenyedia}</p>

<p style="text-align:center; margin: 0">No.:${dok.nomorSurat}</p>


<p style="text-align:justify">Pada hari ini ${dok.hari}, tanggal ${dok.tanggal} bulan ${dok.bulan} tahun ${dok.tahun} Pukul ${dok.jam} bertempat di di <em>Lembaga Kebijakan </em><em>Pengadaan</em><em> Barang/Jasa Pemerintah</em>, telah dilakukan Evaluasi Kualifikasi dan Administrasi serta Pembuktian Kualifikasi terhadap ${dok.namaPenyedia} untuk Pemilihan Penyedia Katalog Elektronik ${dok.namaKomoditas}:</p>

<table style="width:100%">

  <ol style="list-style-type:upper-alpha">
    <li><strong>Hasil Evaluasi dan Pembuktian Kualifikasi sebagai berikut :</strong></li>
    <p>${dok.listKualifikasi}</p>
    <br/>
    <p>${dok.listEvaluasiAdministrasi}</p>
    <br/>
    <p>${dok.listEvaluasiHarga}</p>
    <br/>
    <p>${dok.listEvaluasiTeknis}</p>

              <p style="text-align:justify"><strong>Kesimpulan : Lulus/Tidak Lulus Evaluasi Kualifikasi</strong></p>


              <li><strong>Hasil Evaluasi Kualifikasi sebagai berikut :</strong></li>

              <p>${dok.listEvaluasiAdministrasiTambahan}</p>

          <p style="text-align:justify"><strong>Lulus/Tidak Lulus Evaluasi Kualifikasi</strong></p>

          <p style="text-align:justify">Demikian Berita Acara Hasil Evaluasi Kualifikasi dan Administrasi serta Pembuktian Kualifikasiini dibuat dan ditandatangani pada hari, tanggal dan bulan sebagaimana tersebut diatas untuk dipergunakan sebagaimana mestinya. </p>
          </ol>
          </table>


<p style="text-align:center;margin:0"><strong>Kelompok Kerja Katalog Elektronik ${dok.namaKomoditas}</strong></p>
<p style="text-align:center;margin:0"><strong>Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah</strong></p>

<p>${dok.timPengembangKatalog}</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:center"><strong>${dok.namaPenyedia}</strong></p>


<p>${dok.ttdPenyedia}</p>




', null, 2, '2017-12-18 16:41:36', 2, null, null, null, null, null, 0);
INSERT INTO dokumen_template (id, konten_kategori_id, jenis_dokumen, template, created_date, created_by, modified_date, modified_by, deleted_date, deleted_by, audittype, auditdate, audituser, active) VALUES (7, 48, 'NODIN REVIEW', '<p style="text-align:center"><strong>LEMBAGA KEBIJAKAN PENGADAAN BARANG/JASA PEMERINTAH</strong></p>

<p style="text-align:center"><strong>(LKPP)</strong></p>

<p>&nbsp;</p>

<p><strong>&nbsp;</strong></p>

<p style="text-align:center"><strong>NOTA DINAS</strong></p>

<p style="text-align:center"><strong>&nbsp;</strong></p>

<p style="text-align:center">Nomor:&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;/D.2.2/07/2017</p>

<p>&nbsp;</p>

<table cellspacing="0" style="width:16.0cm">
	<tbody>
		<tr>
			<td style="width:3.0cm">
			<p>Yth.</p>
			</td>
			<td style="width:21.3pt">
			<p>:</p>
			</td>
			<td style="width:347.25pt">
			<p>Deputi Bidang Monitoring-Evaluasi dan Pengembangan Sistem Informasi</p>
			</td>
		</tr>
		<tr>
			<td style="width:3.0cm">
			<p>Dari</p>
			</td>
			<td style="width:21.3pt">
			<p>:</p>
			</td>
			<td style="width:347.25pt">
			<p>Tim Reviu Hasil Pemilihan Penyedia Katalog Elektronik Kendaraan Bermotor Tahun 2017</p>
			</td>
		</tr>
		<tr>
			<td style="width:3.0cm">
			<p>Hal</p>
			</td>
			<td style="width:21.3pt">
			<p>:</p>
			</td>
			<td style="width:347.25pt">
			<p>Laporan Hasil Reviu Proses Pemilihan Penyedia Katalog Elektronik Kendaraan Bermotor Tahun 2017</p>
			</td>
		</tr>
		<tr>
			<td style="width:3.0cm">
			<p>Tanggal</p>
			</td>
			<td style="width:21.3pt">
			<p>:</p>
			</td>
			<td style="width:347.25pt">
			<p>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Juli 2017</p>
			</td>
		</tr>
	</tbody>
</table>

<p>Berdasarkan Surat Tugas Deputi Bidang Monitoring-Evaluasi dan Pengembangan Sistem Informasi No. 3666/D.2/07/2017, bersama ini kami laporkan hasil reviu pemilihan penyedia katalog elektronik kendaraan bermotor tahun 2017 sebagai berikut:</p>

<ol>
	<li>Proses Pengusulan Barang/Jasa</li>
</ol>

<p>Proses pemilihan penyedia tidak dilengkapi dengan kajian sesuai ketentuan Pasal 12 s.d Pasal 13 dan/atau Pasal 43 Peraturan Kepala LKPP No. 6 Tahun 2016 tentang Katalog Elektronik dan <em>E-Purchasing</em>.</p>

<ol start="2">
	<li>Proses Pemilihan Penyedia E-Katalog Kendaraan Bermotor 2017</li>
	<li><strong>Pokja tidak melakukan Evaluasi Teknis</strong></li>
</ol>

<ul>
	<li>Pokja tidak melakukan evaluasi dan klarifikasi teknis serta harga berdasarkan ketentuan Pasal 16 Peraturan Kepala LKPP No. 6 Tahun 2016 tentang Katalog Elektronik dan <em>E-Purchasing</em>. Pokja tidak membuat Berita Acara Evaluasi dan Klarifikasi Teknis serta Harga untuk tiap Penyedia E-Katalog Kendaraan Tahun 2017. Evaluasi teknis tersebut bertujuan untuk menilai apakah tipe kendaraan bermotor (karoseri) memiliki dokumen pengesahan rancang bangun dan rekayasa kendaraan bermotor sesuai ketentuan peraturan perundang-undangan.</li>
	<li>Sesuai ketentuan Pasal 131 s.d Pasal 133 Peraturan Pemerintah Nomor 55 Tahun 2012 tentang Kendaraan mengatur bahwa penelitian rancang bangun dan rekayasa kendaraan bermotor dilakukan terhadap salah satu diantaranya yaitu Kendaraan Bermotor yang dimodifikasi yang menyebabkan perubahan tipe berupa dimensi, mesin, dan kemampuan daya angkut. Tujuan penelitian rancang bangun ini adalah untuk menilai apakah rancang bangun dan rekayasa kendaraan bermotor memenuhi persyaratan teknis dan laik jalan.</li>
	<li>Selain ketentuan diatas, Pasal 10 Peraturan Kepala LKPP No. 6 Tahun 2016 tentang Katalog Elektronik dan <em>E-Purchasing</em> bahwa penyedia Katalog Elektronik harus memiliki ijin terkait produksi dan/atau perdagangan barang atau pelaksanaan jasa yang diterbitkan oleh Kementerian/Lembaga/Pemerintah Daerah.</li>
	<li>Bahwa untuk menilai kelayakan produk yang ditawarkan memenuhi persyaratan sesuai ketentuan peraturan perundang-undangan, maka Pokja seharusnya melakukan evaluasi dan klarifikasi teknis dan harga.</li>
</ul>

<p>&nbsp;</p>

<ol>
	<li><strong>Pokja tidak melakukan evaluasi harga</strong></li>
</ol>

<ul>
	<li>Pokja tidak melakukan evaluasi dan klarifikasi teknis serta harga berdasarkan ketentuan Pasal 16 Peraturan Kepala LKPP No. 6 Tahun 2016 tentang Katalog Elektronik dan <em>E-Purchasing</em>. Pokja tidak membuat Berita Acara terkait evaluasi harga untuk tiap penawaran Penyedia E-Katalog Kendaraan Tahun 2017.</li>
	<li>Ketentuan Evaluasi Harga sebagaimana tercantum di dalam Dokumen Pengadaan yaitu unsur-unsur yang perlu dievaluasi adalah harga maksimal sama dengan harga penawaran kepada masyarakat umum di luar Pemerintah.</li>
	<li>Terdapat penawaran Penyedia yang dinilai melebihi harga penawaran retail (masyarakat umum diluar Pemerintah) yaitu sebagai contoh reviu atas penawaran PT. Suzuki Indomobil Sales (Mobil).</li>
</ul>

<ol start="3">
	<li>Terlampir kami laporkan tabel rekap reviu hasil pemilihan penyedia kendaraan bermotor tahun 2017.</li>
	<li>Berdasarkan angka 1 sampai dengan angka 3 diatas, kami menilai bahwa proses pemilihan katalog elektronik kendaraan bermotor tahun 2017 tahap kedua tidak layak dilanjutkan ke dalam proses SK Penetapan dan Kontrak Katalog karena tidak memenuhi prosedur pemilihan sebagaimana diatur dalam Peraturan Kepala LKPP Nomor 6 Tahun 2016 tentang Katalog Elektronik dan <em>E-Purchasing</em> dan Dokumen Pengadaan Pemilihan Penyedia untuk Katalog Elektronik Kendaraan Bermotor Tahun 2017.</li>
</ol>

<p>&nbsp;</p>

<p>Demikian laporan ini kami sampaikan, mohon arahan Ibu lebih lanjut.</p>

<p>&nbsp;</p>

<p style="text-align:center">Tim Reviu Proses Pemilihan Kendaraan Bermotor 2017</p>

<table cellspacing="0" style="width:439.95pt">
	<tbody>
		<tr>
			<td style="width:219.45pt">
			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>Emin Adhy Muhaemin</strong></p>
			</td>
			<td style="width:220.5pt">
			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>Eko Rinaldo Octavianus</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width:219.45pt">
			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>Mustika Rosalina Putri</strong></p>
			</td>
			<td style="width:220.5pt">
			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>M. Harris</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width:219.45pt">
			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>Sigit Apriyanto</strong></p>
			</td>
			<td style="width:220.5pt">
			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>Lulu Haryani</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width:219.45pt">
			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>&nbsp;</strong></p>

			<p><strong>M. Qadr Siddik Zam</strong></p>
			</td>
			<td style="width:220.5pt">
			<p><strong>&nbsp;</strong></p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Tembusan Yth:</p>

<p>Kepala LKPP</p>
', null, 2, '2017-12-06 15:26:05', 2, null, null, null, null, null, 0);
INSERT INTO dokumen_template (id, konten_kategori_id, jenis_dokumen, template, created_date, created_by, modified_date, modified_by, deleted_date, deleted_by, audittype, auditdate, audituser, active) VALUES (8, 49, 'BERITA ACARA PEMBUKTIAN', '<p style="text-align:center;margin: 0;text-transform:uppercase;">KELOMPOK KERJA KATALOG ELEKTRONIK ${dok.namaKomoditas}</p>

<p style="text-align:center;margin: 0">LEMBAGA KEBIJAKAN PENGADAAN BARANG/JASA PEMERINTAH</p>
<hr style="margin-bottom: 0px;"/>
<p></p>
<p style="text-align:center;margin:0">Berita Acara Hasil Evaluasi dan Pembuktian Kualifikasi dan Administrasi</p>

<p style="text-align:center;margin: 0">Pemilihan Penyedia Katalog Elektronik ${dok.namaKomoditas}</p>

<p style="text-align:center; margin: 0">${dok.namaPenyedia}</p>

<p style="text-align:center; margin: 0">No.:${dok.nomorSurat}</p>


<p style="text-align:justify">Pada hari ini ${dok.hari}, tanggal ${dok.tanggal} bulan ${dok.bulan} tahun ${dok.tahun} Pukul ${dok.jam} bertempat di di <em>Lembaga Kebijakan </em><em>Pengadaan</em><em> Barang/Jasa Pemerintah</em>, telah dilakukan Evaluasi Kualifikasi dan Administrasi serta Pembuktian Kualifikasi terhadap ${dok.namaPenyedia} untuk Pemilihan Penyedia Katalog Elektronik ${dok.namaKomoditas}:</p>

<table style="width:100%">

  <ol style="list-style-type:upper-alpha">
    <li><strong>Hasil Evaluasi dan Pembuktian Kualifikasi sebagai berikut :</strong></li>
    <p>${dok.listKualifikasi}</p>
    <br/>
    <p>${dok.listEvaluasiAdministrasi}</p>
    <br/>
    <p>${dok.listEvaluasiHarga}</p>
    <br/>
    <p>${dok.listEvaluasiTeknis}</p>

              <p style="text-align:justify"><strong>Kesimpulan : Lulus/Tidak Lulus Evaluasi Kualifikasi</strong></p>


              <li><strong>Hasil Evaluasi Kualifikasi sebagai berikut :</strong></li>

              <p>${dok.listEvaluasiAdministrasiTambahan}</p>

          <p style="text-align:justify"><strong>Lulus/Tidak Lulus Evaluasi Kualifikasi</strong></p>

          <p style="text-align:justify">Demikian Berita Acara Hasil Evaluasi Kualifikasi dan Administrasi serta Pembuktian Kualifikasiini dibuat dan ditandatangani pada hari, tanggal dan bulan sebagaimana tersebut diatas untuk dipergunakan sebagaimana mestinya. </p>
          </ol>
          </table>


<p style="text-align:center;margin:0"><strong>Kelompok Kerja Katalog Elektronik ${dok.namaKomoditas}</strong></p>
<p style="text-align:center;margin:0"><strong>Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah</strong></p>

<p>${dok.timPengembangKatalog}</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:center"><strong>${dok.namaPenyedia}</strong></p>


<p>${dok.ttdPenyedia}</p>




', null, 2, null, null, null, null, null, null, null, 0);
INSERT INTO dokumen_template (id, konten_kategori_id, jenis_dokumen, template, created_date, created_by, modified_date, modified_by, deleted_date, deleted_by, audittype, auditdate, audituser, active) VALUES (9, 50, 'BERITA ACARA HASIL EVALUASI KUALIFIKASI, EVALUASI ADMINISTRASI, DAN PEMBUKTIAN KUALIFIKASI', '<p class="MsoNormal" style="text-align: center; tab-stops: 2.0cm;" align="center"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">KELOMPOK KERJA KATALOG ELEKTRONIK&nbsp;</span><span style="font-family: ''Footlight MT Light''; font-size: 16px;">${ba?.nama_komoditas.toUpperCase()}</span></p>
<p class="MsoNormal" style="text-align: center;" align="center"><span style="font-size: 12.0pt; font-family: ''Footlight MT Light'';">LEMBAGA KEBIJAKAN PENGADAAN BARANG/JASA PEMERINTAH</span></p>
<div style="mso-element: para-border-div; border: none; border-bottom: solid windowtext 1.5pt; padding: 0cm 0cm 1.0pt 0cm;">
<p class="MsoNormal" style="text-align: center; border: none; mso-border-bottom-alt: solid windowtext 1.5pt; padding: 0cm; mso-padding-alt: 0cm 0cm 1.0pt 0cm;" align="center"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
</div>
<p class="MsoNormal" style="text-align: center; tab-stops: 0cm 49.5pt 63.0pt 279.0pt 324.0pt 360.0pt 396.0pt 432.0pt;" align="center"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center;" align="center"><span style="font-size: 12.0pt; font-family: ''Footlight MT Light'';">Berita Acara Hasil Evaluasi Kualifikasi, Evaluasi Administrasi, dan Pembuktian Kualifikasi</span></p>
<p class="MsoNormal" style="text-align: center;" align="center"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Pemilihan Penyedia Katalog Elektronik&nbsp;</span><span style="font-family: ''Footlight MT Light''; font-size: 16px;">${ba?.nama_komoditas}</span></p>
<p class="MsoNormal" style="text-align: center;" align="center"><span style="font-size: 12.0pt; font-family: ''Footlight MT Light'';">${ba?.nama_penyedia}</span></p>
<p class="MsoNormal" style="text-align: center;" align="center"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">No.:</span> <span style="font-size: 12.0pt; font-family: ''Footlight MT Light'';">__</span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">/BAE-KA/PERALATANPENDIDIKAN</span><span style="font-size: 12.0pt; font-family: ''Footlight MT Light'';">SMK</span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">/</span><span style="font-size: 12.0pt; font-family: ''Footlight MT Light'';">03</span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">/201</span><span style="font-size: 12.0pt; font-family: ''Footlight MT Light'';">8</span></p>
<p class="MsoNormal" style="text-align: center;" align="center"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Pada hari ini</span> <span style="font-size: 12.0pt; font-family: ''Footlight MT Light'';">${hari}</span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">,</span><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;"> tanggal ${tanggal} bulan ${bulan}&nbsp;</span><span style="font-size: 12.0pt; font-family: ''Footlight MT Light'';">${ba?.pic_nama}&nbsp;</span><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">tahun ${tahun}</span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;(${formatTanggal}</span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">) </span><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Pukul </span><span style="font-size: 12.0pt; font-family: ''Footlight MT Light'';">___ </span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">s.d </span><span style="font-size: 12.0pt; font-family: ''Footlight MT Light'';">___</span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;"> WIB </span><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">bertempat di&nbsp;</span><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">___, telah dilakukan </span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Evaluasi Kualifikasi dan Administrasi serta Pembuktian Kualifikasi terhadap </span><span style="font-size: 12.0pt; font-family: ''Footlight MT Light'';">______ </span><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">untuk </span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Pemilihan Penyedia Katalog Elektronik: </span></p>
<p class="MsoNormal"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal"><span style="font-size: 12.0pt; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="margin-left: 21.3pt; mso-add-space: auto; text-indent: -18.0pt; line-height: normal; mso-list: l7 level1 lfo5;"><!-- [if !supportLists]--><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: FI;"><span style="mso-list: Ignore;">A.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp; </span></span></span></strong><!--[endif]--><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Hasil Evaluasi </span></strong><strong style="mso-bidi-font-weight: normal;"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Kualifikasi</span></strong><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;"> sebagai berikut :</span></strong></p>
<table class="MsoNormalTable" style="width: 454.5pt; margin-left: 5.4pt; border-collapse: collapse; border: none; mso-border-alt: solid black .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid black; mso-border-insidev: .5pt solid black;" border="1" width="455" cellspacing="0" cellpadding="0">
<thead>
<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 8.55pt;">
<td style="width: 23.5pt; border: solid black 1.0pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 8.55pt;" rowspan="2" width="24">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">No</span></strong></p>
</td>
<td style="width: 188.6pt; border: solid black 1.0pt; border-left: none; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 8.55pt;" rowspan="2" width="189">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Uraian kualifikasi sebagai berikut:</span></strong></p>
</td>
<td style="width: 118.3pt; border: solid black 1.0pt; border-left: none; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 8.55pt;" rowspan="2" width="118">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Keterangan Dokumen</span></strong></p>
</td>
<td style="width: 124.1pt; border: solid black 1.0pt; border-left: none; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 8.55pt;" colspan="2" width="124">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Hasil </span></strong><strong style="mso-bidi-font-weight: normal;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Evaluasi</span></strong><strong style="mso-bidi-font-weight: normal;"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;"> Kualifikasi</span></strong></p>
</td>
</tr>
<tr style="mso-yfti-irow: 1;">
<td style="width: 50.0pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="50">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Tercantum</span></p>
</td>
<td style="width: 74.1pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="74">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Tidak Tercantum</span></p>
</td>
</tr>
</thead>
<tbody>
<tr style="mso-yfti-irow: 2; mso-yfti-lastrow: yes;">
<td style="width: 23.5pt; border: solid black 1.0pt; border-top: none; mso-border-top-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="24">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">1</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">2.</span></p>
</td>
<td style="width: 188.6pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="189">
<p class="MsoNormal" style="line-height: 115%;"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Pakta Integritas yang mencantumkan</span></p>
<p class="MsoNormal" style="margin-left: 12.45pt; text-indent: -12.45pt; line-height: 115%; mso-list: l4 level1 lfo1;"><!-- [if !supportLists]--><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">a.<span style="font: 7.0pt ''Times New Roman'';">&nbsp; </span></span></span><!--[endif]--><span lang="SV" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: SV;">tidak akan melakukan praktek </span><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Korupsi, Kolusi dan Nepotisme (</span><span lang="SV" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: SV;">KKN</span><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">)</span><span lang="SV" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: SV;">;</span></p>
<p class="MsoNormal" style="margin-left: 12.45pt; text-indent: -12.45pt; line-height: 115%; mso-list: l4 level1 lfo1;"><!-- [if !supportLists]--><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">b.<span style="font: 7.0pt ''Times New Roman'';">&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">akan melaporkan kepada Inspektorat</span> <span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">LKPP apabila mengetahui ada indikasi KKN di dalam proses pengadaan ini;</span></p>
<p class="MsoNormal" style="margin-left: 12.45pt; text-indent: -12.45pt; line-height: 115%; mso-list: l4 level1 lfo1;"><!-- [if !supportLists]--><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">c.<span style="font: 7.0pt ''Times New Roman'';">&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">akan mengikuti proses pengadaan secara bersih, transparan, dan profesional untuk memberikan hasil kerja terbaik sesuai ketentuan peraturan perundang-undangan;</span></p>
<p class="MsoNormal" style="margin-left: 12.45pt; text-indent: -12.45pt; line-height: 115%; mso-list: l4 level1 lfo1;"><!-- [if !supportLists]--><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">d.<span style="font: 7.0pt ''Times New Roman'';">&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">apabila melanggar hal-hal yang dinyatakan dalam PAKTA INTEGRITAS ini, bersedia menerima sanksi administratif, menerima sanksi pencantuman dalam Daftar Hitam, digugat secara perdata </span><span lang="SV" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: SV;">dan</span><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">/atau dilaporkan secara</span><span lang="SV" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: SV;"> pidana</span><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">. </span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Formulir isian kualifikasi yang mencantumkan:</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><strong style="mso-bidi-font-weight: normal;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Bagi Pabrikan/Produsen/ Pemegang Merek :</span></strong></p>
<p class="MsoListParagraphCxSpFirst" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l5 level1 lfo2; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 40.65pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">1.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Akta Pendirian dan Pengesahan Kementerian Hukum dan HAM</span><span style="font-family: ''Footlight MT Light'';">, khusus untuk badan usaha yang berbentuk CV hanya akta pendirian saja.</span></p>
<p class="MsoListParagraphCxSpMiddle" style="mso-add-space: auto; text-align: justify; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 40.65pt;"><span style="font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpMiddle" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l5 level1 lfo2; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.1pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">2.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Akta </span><span style="font-family: ''Footlight MT Light'';">Perubahan Terakhir </span><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">dan Pengesahan Kementerian Hukum dan HAM</span><span style="font-family: ''Footlight MT Light'';"> (apabila ada), khusus untuk badan usaha yang berbentuk CV hanya akta perubahan terakhir saja (apabila ada).</span></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-bottom: .0001pt; mso-add-space: auto;"><span style="font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpMiddle" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l5 level1 lfo2; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.1pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">3.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">A. Surat Ijin Usaha Industri/ Tanda Daftar Industri (TDI)<span style="mso-spacerun: yes;">&nbsp; </span>Peralatan Pendidikan kecuali penyedia yang berstatus sebagai pemegang merek/Makloon/OEM; atau</span></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-bottom: .0001pt; mso-add-space: auto; text-align: justify; tab-stops: 55.05pt;"><span style="font-family: ''Footlight MT Light'';">B.<span style="mso-spacerun: yes;">&nbsp; </span></span><span style="font-size: 12.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Surat Ijin Usaha Industri/ Tanda Daftar Industri (TDI) lainnya sepanjang benar terbukti telah berpengalaman memproduksi peralatan pendidikan SMK (akan dilakukan survei/cek lokasi) minimal 3 (tiga) tahun.</span></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-bottom: .0001pt; mso-add-space: auto; text-align: justify; tab-stops: 55.05pt;"><span style="font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpMiddle" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l5 level1 lfo2; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.1pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">4.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">Surat izin usaha perdagangan peralatan pendidikan milik produsen/pabrikan/ pemegang merek.</span></p>
<p class="MsoListParagraphCxSpMiddle" style="mso-add-space: auto; text-align: justify; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.1pt;"><span style="font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpMiddle" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l5 level1 lfo2; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.1pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">5.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">Sertifikat merek dari Kemenkumham atau tanda bukti pendaftaran sertifikat merek bertanggal sampai dengan batas akhir pemasukan dokumen penawaran.</span></p>
<p class="MsoListParagraphCxSpLast" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l5 level1 lfo2; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.1pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">6.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">Bukti perjanjian kerjasama antara pemegang merek dengan perusahaan manufaktur untuk pemegang merek/Makloon/OEM.</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 37.1pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l5 level1 lfo2; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.1pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">7.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;">Nomor Pokok Wajib Pajak (</span><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">NPWP</span><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;">)</span><span style="font-family: ''Footlight MT Light'';">.</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 37.1pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpFirst" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l5 level1 lfo2; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.1pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">8.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Bukti Penerimaan </span><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;">Surat SPT</span><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;"> Tahunan</span> <span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Tahun 201</span><span style="font-family: ''Footlight MT Light'';">6.</span></p>
<p class="MsoListParagraphCxSpLast" style="margin-bottom: .0001pt; mso-add-space: auto; tab-stops: 8.75pt;"><strong style="mso-bidi-font-weight: normal;"><span style="font-family: ''Footlight MT Light'';">&nbsp;</span></strong></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 8.75pt;"><strong style="mso-bidi-font-weight: normal;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Bagi agen/agen tunggal/ distributor/ distributor tunggal</span></strong> <strong style="mso-bidi-font-weight: normal;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">:</span></strong></p>
<p class="MsoListParagraphCxSpFirst" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l6 level1 lfo3; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">1.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Akta Pendirian dan Pengesahan Kementerian Hukum dan HAM</span><span style="font-family: ''Footlight MT Light'';">, khusus untuk badan usaha yang berbentuk CV cukup melampirkan akta pendirian saja.</span></p>
<p class="MsoListParagraphCxSpMiddle" style="mso-add-space: auto; text-align: justify; margin: 0cm 0cm .0001pt 33.55pt;"><span style="font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpLast" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l6 level1 lfo3; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">2.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Akta </span><span style="font-family: ''Footlight MT Light'';">Perubahan Terakhir </span><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">dan Pengesahan Kementerian Hukum dan HAM</span><span style="font-family: ''Footlight MT Light'';"> (apabila ada), khusus untuk badan usaha yang berbentuk CV cukup melampirkan akta perubahan terakhir saja (apabila ada).</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l6 level1 lfo3; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">3.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">Nomor Identitas Kepabeanan (NIK) bagi agen/agen tunggal/distributor/ distributor tunggal produk luar negeri.</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l6 level1 lfo3; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">4.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">Surat izin usaha perdagangan peralatan pendidikan milik agen/agen tunggal/ distributor/ distributor tunggal.</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpFirst" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l6 level1 lfo3; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">5.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;">Nomor Pokok Wajib Pajak (</span><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">NPWP</span><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;">).</span></p>
<p class="MsoListParagraphCxSpMiddle" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l6 level1 lfo3; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">6.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Bukti Penerimaan </span><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;">Surat SPT</span><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;"> Tahunan</span> <span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Tahun 201</span><span style="font-family: ''Footlight MT Light'';">6.</span></p>
<p class="MsoListParagraphCxSpLast" style="mso-add-space: auto; tab-stops: 55.05pt; margin: 0cm 0cm .0001pt 86.75pt;"><span style="font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><strong style="mso-bidi-font-weight: normal;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Bagi PMA sebagai Distributor (<em style="mso-bidi-font-style: normal;">Wholesaler</em>):</span></strong></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l0 level1 lfo4; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">1.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Akta Pendirian dan Pengesahan Kementerian Hukum dan HAM</span><span style="font-family: ''Footlight MT Light'';">.</span></p>
<p class="MsoNormal" style="margin-left: 15.55pt; line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="margin-left: 15.55pt; line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l0 level1 lfo4; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">2.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Akta Perubahan</span><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;"> Terakhir </span><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">dan Pengesahan Kementerian Hukum HAM</span><span style="font-family: ''Footlight MT Light'';"> (apabila ada).</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l0 level1 lfo4; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">3.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">Perjanjian penunjukan perusahaan perdagangan nasional sebagai agen, agen tunggal, distributor atau distributor tunggal yang dilegalisir oleh notaris.</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l0 level1 lfo4; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">4.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">Persetujuan tertulis dari prinsipal produsen yang diwakilinya di luar negeri terhadap penunjukan perusahaan perdagangan nasional sebagai agen, agen tunggal, distributor atau distributor tunggal.</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l0 level1 lfo4; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">5.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">Ijin prinsip/ijin usaha dari BKPM.</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpFirst" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l0 level1 lfo4; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">6.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">Nomor Identitas Kepabeanan (NIK).</span></p>
<p class="MsoListParagraphCxSpLast" style="mso-add-space: auto; text-align: justify; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 33.55pt;"><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l0 level1 lfo4; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">7.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;">Nomor Pokok Wajib Pajak (</span><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">NPWP</span><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;">)</span><span style="font-family: ''Footlight MT Light'';">.</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l0 level1 lfo4; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">8.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Bukti Penerimaan </span><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;">Surat SPT</span><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;"> Tahunan</span> <span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Tahun 201</span><span style="font-family: ''Footlight MT Light'';">6.</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 25.2pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-bidi-font-family: Arial;">&nbsp;</span></p>
</td>
<td style="width: 118.3pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="118">
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Penandatangan :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Penandatangan :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Akta :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Pengesahan :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Akta :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Pengesahan :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Akta :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Pengesahan :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Akta :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Pengesahan :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Akta :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Pengesahan :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Akta :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Pengesahan :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
</td>
<td style="width: 50.0pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="50">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
</td>
<td style="width: 74.1pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="74">
<p class="MsoNormal" style="line-height: 115%;"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<p class="MsoListParagraph" style="margin: 0cm; margin-bottom: .0001pt; mso-add-space: auto; text-align: justify; line-height: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">&nbsp;</span></p>
<p class="MsoNormal"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Kesimpulan : Lulus/Tidak Lulus Evaluasi Kualifikasi</span></strong></p>
<p class="MsoListParagraphCxSpFirst" style="margin-left: 18.0pt; mso-add-space: auto;"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">&nbsp;</span></strong></p>
<p class="MsoListParagraphCxSpLast" style="margin-left: 21.3pt; mso-add-space: auto; text-indent: -18.0pt; line-height: normal; mso-list: l7 level1 lfo5;"><!-- [if !supportLists]--><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: FI;"><span style="mso-list: Ignore;">B.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span></strong><!--[endif]--><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Hasil </span></strong><strong style="mso-bidi-font-weight: normal;"><span style="font-size: 12.0pt; font-family: ''Footlight MT Light'';">Evaluasi Administrasi </span></strong><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">sebagai berikut :</span></strong></p>
<table class="MsoNormalTable" style="width: 454.5pt; margin-left: 5.4pt; border-collapse: collapse; border: none; mso-border-alt: solid black .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid black; mso-border-insidev: .5pt solid black;" border="1" width="455" cellspacing="0" cellpadding="0">
<tbody>
<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 28.1pt;">
<td style="width: 26.75pt; border: solid black 1.0pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 28.1pt;" width="27">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">No</span></strong></p>
</td>
<td style="width: 214.25pt; border: solid black 1.0pt; border-left: none; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 28.1pt;" width="214">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Uraian sebagai berikut:</span></strong></p>
</td>
<td style="width: 92.15pt; border: solid black 1.0pt; border-left: none; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 28.1pt;" width="92">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Hasil Evaluasi</span></strong></p>
</td>
<td style="width: 121.35pt; border: solid black 1.0pt; border-left: none; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 28.1pt;" valign="top" width="121">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">&nbsp;</span></strong></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Keterangan</span></strong></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">&nbsp;</span></strong></p>
</td>
</tr>
<tr style="mso-yfti-irow: 1;">
<td style="width: 26.75pt; border: solid black 1.0pt; border-top: none; mso-border-top-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="27">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="FI" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">1</span></p>
</td>
<td style="width: 214.25pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="214">
<p class="MsoNormal" style="line-height: 115%;"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Nomor Surat Penawaran </span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
</td>
<td style="width: 92.15pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="92">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Ada/Tidak</span></p>
</td>
<td style="width: 121.35pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="121">
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
</td>
</tr>
<tr style="mso-yfti-irow: 2;">
<td style="width: 26.75pt; border: solid black 1.0pt; border-top: none; mso-border-top-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="27">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">2</span></p>
</td>
<td style="width: 214.25pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="214">
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal </span><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Surat Penawaran </span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
</td>
<td style="width: 92.15pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="92">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Ada/Tidak</span></p>
</td>
<td style="width: 121.35pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="121">
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
</td>
</tr>
<tr style="mso-yfti-irow: 3;">
<td style="width: 26.75pt; border: solid black 1.0pt; border-top: none; mso-border-top-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="27">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">3</span></p>
</td>
<td style="width: 214.25pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="214">
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Penandatangan </span><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Surat Penawaran</span></p>
</td>
<td style="width: 92.15pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="92">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Sesuai/Tidak Sesuai</span></p>
</td>
<td style="width: 121.35pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="121">
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Penandatangan :</span></p>
</td>
</tr>
<tr style="mso-yfti-irow: 4;">
<td style="width: 26.75pt; border: solid black 1.0pt; border-top: none; mso-border-top-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="27">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">4</span></p>
</td>
<td style="width: 214.25pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="214">
<p class="MsoNormal" style="text-align: left; line-height: 115%;" align="left"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Surat </span><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Kuasa </span><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Penandatangan Dokumen Penawaran </span><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">(apabila diperlukan)</span></p>
</td>
<td style="width: 92.15pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="92">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Ada/Tidak</span></p>
</td>
<td style="width: 121.35pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="121">
<p class="MsoNormal"><span style="font-size: 11.0pt; font-family: ''Footlight MT Light'';">Penandatangan :</span></p>
<p class="MsoNormal"><span style="font-size: 11.0pt; font-family: ''Footlight MT Light'';">Yang dikuasakan :</span></p>
</td>
</tr>
<tr style="mso-yfti-irow: 5; mso-yfti-lastrow: yes;">
<td style="width: 26.75pt; border: solid black 1.0pt; border-top: none; mso-border-top-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="27">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">5</span></p>
</td>
<td style="width: 214.25pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="214">
<p class="MsoNormal" style="text-align: left; line-height: 115%;" align="left"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Surat </span><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Kuasa </span><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Mengikuti Tahapan Proses Pemilihan Penyedia</span><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">(apabila diperlukan)</span></p>
</td>
<td style="width: 92.15pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="92">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Ada/Tidak</span></p>
</td>
<td style="width: 121.35pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="121">
<p class="MsoNormal"><span style="font-size: 11.0pt; font-family: ''Footlight MT Light'';">Penandatangan :</span></p>
<p class="MsoNormal"><span style="font-size: 11.0pt; font-family: ''Footlight MT Light'';">Yang dikuasakan :</span></p>
</td>
</tr>
</tbody>
</table>
<p class="MsoNormal"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Kesimpulan: Lulus/Tidak Lulus Evaluasi Administrasi</span></strong></p>
<p class="MsoNormal"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">&nbsp;</span></strong></p>
<p class="MsoNormal"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">&nbsp;</span></strong></p>
<p class="MsoListParagraph" style="margin-left: 21.3pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l7 level1 lfo5; tab-stops: 21.3pt;"><!-- [if !supportLists]--><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: FI;"><span style="mso-list: Ignore;">C.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp; </span></span></span></strong><!--[endif]--><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Hasil Pembuktian Kualifikasi</span></strong></p>
<table class="MsoNormalTable" style="margin-left: 5.4pt; border-collapse: collapse; mso-table-layout-alt: fixed; border: none; mso-border-alt: solid black .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid black; mso-border-insidev: .5pt solid black;" border="1" width="455" cellspacing="0" cellpadding="0">
<thead>
<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 11.6pt;">
<td style="width: 1.0cm; border: solid black 1.0pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 11.6pt;" rowspan="2" width="28">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">No</span></strong></p>
</td>
<td style="width: 183.15pt; border: solid black 1.0pt; border-left: none; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 11.6pt;" rowspan="2" width="183">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Uraian kualifikasi sebagai berikut:</span></strong></p>
</td>
<td style="width: 121.5pt; border: solid black 1.0pt; border-left: none; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 11.6pt;" rowspan="2" width="122">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Keterangan Dokumen</span></strong></p>
</td>
<td style="width: 121.5pt; border: solid black 1.0pt; border-left: none; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 11.6pt;" colspan="2" width="122">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Hasil Pembuktian</span></strong><strong style="mso-bidi-font-weight: normal;"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;"> Kualifikasi</span></strong></p>
</td>
</tr>
<tr style="mso-yfti-irow: 1; height: 11.6pt;">
<td style="width: 49.5pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 11.6pt;" width="50">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Sesuai Dengan Asli</span></p>
</td>
<td style="width: 72.0pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 11.6pt;" valign="top" width="72">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Tidak Sesuai Dengan Asli</span></p>
</td>
</tr>
</thead>
<tbody>
<tr style="mso-yfti-irow: 2; mso-yfti-lastrow: yes;">
<td style="width: 1.0cm; border: solid black 1.0pt; border-top: none; mso-border-top-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="28">
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">1</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span lang="IN" style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
</td>
<td style="width: 183.15pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="183">
<p class="MsoListParagraphCxSpFirst" style="margin: 0cm; margin-bottom: .0001pt; mso-add-space: auto; text-align: justify;"><strong style="mso-bidi-font-weight: normal;"><span style="font-family: ''Footlight MT Light'';">Bagi Pabrikan/Produsen/ Pemegang Merek :</span></strong></p>
<p class="MsoListParagraphCxSpMiddle" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l3 level1 lfo6; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.15pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">1.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Akta Pendirian dan Pengesahan Kementerian Hukum dan HAM</span><span style="font-family: ''Footlight MT Light'';">, khusus untuk badan usaha yang berbentuk CV hanya akta pendirian saja.</span></p>
<p class="MsoListParagraphCxSpMiddle" style="mso-add-space: auto; text-align: justify; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.15pt;"><span style="font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpLast" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l3 level1 lfo6; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.1pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">2.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Akta </span><span style="font-family: ''Footlight MT Light'';">Perubahan Terakhir </span><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">dan Pengesahan Kementerian Hukum dan HAM</span><span style="font-family: ''Footlight MT Light'';"> (apabila ada), khusus untuk badan usaha yang berbentuk CV hanya akta perubahan terakhir saja (apabila ada).</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 37.1pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpFirst" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l3 level1 lfo6; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.1pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">3.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">A. Surat Ijin Usaha Industri/ Tanda Daftar Industri (TDI)<span style="mso-spacerun: yes;">&nbsp; </span>Peralatan Pendidikan kecuali penyedia yang berstatus sebagai pemegang merek/Makloon/OEM; atau</span></p>
<p class="MsoListParagraphCxSpLast" style="margin-bottom: .0001pt; mso-add-space: auto; text-align: justify; tab-stops: 55.05pt;"><span style="font-family: ''Footlight MT Light'';">B. </span><span style="font-size: 12.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Surat Ijin Usaha Industri/ Tanda Daftar Industri (TDI) lainnya sepanjang benar terbukti telah berpengalaman memproduksi peralatan pendidikan SMK (akan dilakukan survei/cek lokasi) minimal 3 (tiga) tahun.</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 55.05pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpFirst" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l3 level1 lfo6; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.1pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">4.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">Surat izin usaha perdagangan peralatan pendidikan milik produsen/pabrikan/ pemegang merek.</span></p>
<p class="MsoListParagraphCxSpMiddle" style="mso-add-space: auto; text-align: justify; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.1pt;"><span style="font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpLast" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l3 level1 lfo6; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.1pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">5.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">Sertifikat merek dari Kemenkumham atau tanda bukti pendaftaran sertifikat merek bertanggal sampai dengan batas akhir pemasukan dokumen penawaran.</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 37.1pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l3 level1 lfo6; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.1pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">6.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">Bukti perjanjian kerjasama antara pemegang merek dengan perusahaan manufaktur untuk pemegang merek/Makloon/OEM.</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 37.1pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l3 level1 lfo6; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.1pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">7.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;">Nomor Pokok Wajib Pajak (</span><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">NPWP</span><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;">)</span><span style="font-family: ''Footlight MT Light'';">.</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 37.1pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpFirst" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l3 level1 lfo6; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.1pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">8.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Bukti Penerimaan </span><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;">Surat SPT</span><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;"> Tahunan</span> <span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Tahun 201</span><span style="font-family: ''Footlight MT Light'';">6.</span></p>
<p class="MsoListParagraphCxSpMiddle" style="mso-add-space: auto; text-align: justify; tab-stops: 37.1pt; margin: 0cm 0cm .0001pt 37.1pt;"><span style="font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpMiddle" style="margin: 0cm; margin-bottom: .0001pt; mso-add-space: auto; text-align: justify; tab-stops: 8.75pt;"><strong style="mso-bidi-font-weight: normal;"><span style="font-family: ''Footlight MT Light'';">Bagi agen/agen tunggal/ distributor/ distributor tunggal</span></strong> <strong style="mso-bidi-font-weight: normal;"><span style="font-family: ''Footlight MT Light'';">:</span></strong></p>
<p class="MsoListParagraphCxSpLast" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l2 level1 lfo7; margin: 0cm 0cm .0001pt 37.15pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">1.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Akta Pendirian dan Pengesahan Kementerian Hukum dan HAM</span><span style="font-family: ''Footlight MT Light'';">, khusus untuk badan usaha yang berbentuk CV cukup melampirkan akta pendirian saja.</span></p>
<p class="MsoNormal" style="margin-left: 19.15pt; line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l2 level1 lfo7; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">2.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Akta </span><span style="font-family: ''Footlight MT Light'';">Perubahan Terakhir </span><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">dan Pengesahan Kementerian Hukum dan HAM</span><span style="font-family: ''Footlight MT Light'';"> (apabila ada), khusus untuk badan usaha yang berbentuk CV cukup melampirkan akta perubahan terakhir saja (apabila ada).</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l2 level1 lfo7; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">3.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">Nomor Identitas Kepabeanan (NIK) bagi agen/agen tunggal/distributor/ distributor tunggal produk luar negeri.</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l2 level1 lfo7; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">4.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">Surat izin usaha perdagangan peralatan pendidikan milik agen/agen tunggal/ distributor/ distributor tunggal.</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l2 level1 lfo7; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">5.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;">Nomor Pokok Wajib Pajak (</span><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">NPWP</span><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;">).</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpFirst" style="mso-add-space: auto; text-align: justify; text-indent: -21.25pt; mso-list: l2 level1 lfo7; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light'';"><span style="mso-list: Ignore;">6.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Bukti Penerimaan </span><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;">Surat SPT</span><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;"> Tahunan</span> <span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Tahun 201</span><span style="font-family: ''Footlight MT Light'';">6.</span></p>
<p class="MsoListParagraphCxSpMiddle" style="mso-add-space: auto; tab-stops: 55.05pt; margin: 0cm 0cm .0001pt 86.75pt;"><span style="font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpMiddle" style="margin: 0cm; margin-bottom: .0001pt; mso-add-space: auto; text-align: justify;"><strong style="mso-bidi-font-weight: normal;"><span style="font-family: ''Footlight MT Light'';">Bagi PMA sebagai Distributor (<em style="mso-bidi-font-style: normal;">Wholesaler</em>):</span></strong></p>
<p class="MsoListParagraphCxSpLast" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l1 level1 lfo8; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 37.15pt;"><!-- [if !supportLists]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">1.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Akta Pendirian dan Pengesahan Kementerian Hukum dan HAM</span><span style="font-family: ''Footlight MT Light'';">.</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpFirst" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l1 level1 lfo8; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">2.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Akta Perubahan</span><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;"> Terakhir </span><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">dan Pengesahan Kementerian Hukum HAM</span><span style="font-family: ''Footlight MT Light'';"> (apabila ada).</span></p>
<p class="MsoListParagraphCxSpLast" style="mso-add-space: auto; text-align: justify; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 33.55pt;"><span style="font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpFirst" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l1 level1 lfo8; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">3.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">Perjanjian penunjukan perusahaan perdagangan nasional sebagai agen, agen tunggal, distributor atau distributor tunggal yang dilegalisir oleh notaris.</span></p>
<p class="MsoListParagraphCxSpMiddle" style="mso-add-space: auto; text-align: justify; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 33.55pt;"><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoListParagraphCxSpLast" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l1 level1 lfo8; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">4.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">Persetujuan tertulis dari prinsipal produsen yang diwakilinya di luar negeri terhadap penunjukan perusahaan perdagangan nasional sebagai agen, agen tunggal, distributor atau distributor tunggal.</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l1 level1 lfo8; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">5.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">Ijin prinsip/ijin usaha dari BKPM.</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-fareast-font-family: Calibri;">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l1 level1 lfo8; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">6.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-family: ''Footlight MT Light'';">Nomor Identitas Kepabeanan (NIK).</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-fareast-font-family: Calibri;">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l1 level1 lfo8; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">7.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;">Nomor Pokok Wajib Pajak (</span><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">NPWP</span><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;">)</span><span style="font-family: ''Footlight MT Light'';">.</span></p>
<p class="MsoNormal" style="margin-left: 15.55pt; line-height: 115%; tab-stops: 33.55pt;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; text-indent: -18.0pt; mso-list: l1 level1 lfo8; tab-stops: 33.55pt; margin: 0cm 0cm .0001pt 33.55pt;"><!-- [if !supportLists]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="mso-list: Ignore;">8.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Bukti Penerimaan </span><span lang="EN-AU" style="font-family: ''Footlight MT Light''; mso-ansi-language: EN-AU;">Surat SPT</span><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;"> Tahunan</span> <span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Tahun 201</span><span style="font-family: ''Footlight MT Light'';">6.</span></p>
</td>
<td style="width: 121.5pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="122">
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Akta :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Pengesahan :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Akta :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Pengesahan :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Akta :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Pengesahan :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Akta :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Pengesahan :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Akta :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Pengesahan :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Akta :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No Pengesahan :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Masa Berlaku :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">No :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">Tanggal :</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
</td>
<td style="width: 49.5pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="50">
<p class="MsoListParagraphCxSpFirst" style="mso-add-space: auto; text-align: justify; margin: 0cm 0cm .0001pt 12.7pt;"><span style="font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpLast" style="mso-add-space: auto; text-align: justify; margin: 0cm 0cm .0001pt 12.7pt;"><span style="font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; margin: 0cm 0cm .0001pt 12.7pt;"><span style="font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-fareast-font-family: Calibri;">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpFirst" style="mso-add-space: auto; text-align: justify; margin: 0cm 0cm .0001pt 12.7pt;"><span style="font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpMiddle" style="mso-add-space: auto; text-align: justify; margin: 0cm 0cm .0001pt 12.7pt;"><span style="font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoListParagraphCxSpLast" style="mso-add-space: auto; text-align: justify; margin: 0cm 0cm .0001pt 12.7pt;"><span style="font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal">&nbsp;</p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&radic;</span></p>
<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Footlight MT Light'';">&nbsp;</span></p>
</td>
<td style="width: 72.0pt; border-top: none; border-left: none; border-bottom: solid black 1.0pt; border-right: solid black 1.0pt; mso-border-top-alt: solid black .5pt; mso-border-left-alt: solid black .5pt; mso-border-alt: solid black .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="72">
<p class="MsoListParagraph" style="mso-add-space: auto; text-align: justify; margin: 0cm 0cm .0001pt 12.7pt;"><span lang="IN" style="font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<p class="MsoNormal"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">&nbsp;</span></strong></p>
<p class="MsoNormal"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Kesimpulan : Lulus/Tidak Lulus Pembuktian Kualifikasi</span></strong></p>
<p class="MsoNormal"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">D</span><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">emikian Berita Acara Hasil </span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Evaluasi Kualifikasi dan Administrasi serta Pembuktian Kualifikasi</span><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;"> ini dibuat dan ditandatangani pada hari, tanggal dan bulan sebagaimana tersebut diatas untuk dipergunakan sebagaimana mestinya</span></p>
<p class="MsoNormal" style="text-align: center;" align="center"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Kelompok Kerja Katalog Elektronik&nbsp;</span></strong><strong><span style="font-family: ''Footlight MT Light''; font-size: 16px;">${ba?.nama_komoditas}</span></strong></p>
<p class="MsoNormal" style="text-align: center;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah </span></strong></p>
<p class="MsoNormal" style="text-align: center;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></strong>${listPokja}</p>
<div align="center">&nbsp;</div>', null, 2, '2018-08-15 10:35:00', 2, null, null, null, null, null, 0);
INSERT INTO dokumen_template (id, konten_kategori_id, jenis_dokumen, template, created_date, created_by, modified_date, modified_by, deleted_date, deleted_by, audittype, auditdate, audituser, active) VALUES (10, 51, 'SK PERPANJANGAN PRODUK', '<p>Hello World</p>', '2018-08-07 13:33:39', 436086, null, null, null, null, null, null, null, 0);
INSERT INTO dokumen_template (id, konten_kategori_id, jenis_dokumen, template, created_date, created_by, modified_date, modified_by, deleted_date, deleted_by, audittype, auditdate, audituser, active) VALUES (11, 52, 'BERITA ACARA EVALUASI DAN KLARIFIKASI TEKNIS SERTA HARGA', '<p><!-- [if gte mso 9]><xml>
 <o:OfficeDocumentSettings>
  <o:AllowPNG/>
  <o:PixelsPerInch>96</o:PixelsPerInch>
  <o:TargetScreenSize>800x600</o:TargetScreenSize>
 </o:OfficeDocumentSettings>
</xml><![endif]--> <!-- [if gte mso 9]><xml>
 <w:WordDocument>
  <w:View>Normal</w:View>
  <w:Zoom>0</w:Zoom>
  <w:TrackMoves/>
  <w:TrackFormatting/>
  <w:PunctuationKerning/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>EN-US</w:LidThemeOther>
  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
   <w:SplitPgBreakAndParaMark/>
   <w:EnableOpenTypeKerning/>
   <w:DontFlipMirrorIndents/>
   <w:OverrideTableStyleHps/>
  </w:Compatibility>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!-- [if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="false"
  DefSemiHidden="false" DefQFormat="false" DefPriority="99"
  LatentStyleCount="381">
  <w:LsdException Locked="false" Priority="0" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 9"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 9"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Normal Indent"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="footnote text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="annotation text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="header"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="footer"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index heading"/>
  <w:LsdException Locked="false" Priority="35" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="table of figures"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="envelope address"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="envelope return"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="footnote reference"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="annotation reference"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="line number"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="page number"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="endnote reference"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="endnote text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="table of authorities"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="macro"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="toa heading"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 5"/>
  <w:LsdException Locked="false" Priority="10" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Closing"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Signature"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="true"
   UnhideWhenUsed="true" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text Indent"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Message Header"/>
  <w:LsdException Locked="false" Priority="11" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Salutation"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Date"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text First Indent"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text First Indent 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Note Heading"/>
  <w:LsdException Locked="false" Priority="0" SemiHidden="true"
   UnhideWhenUsed="true" Name="Body Text 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text Indent 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text Indent 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Block Text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Hyperlink"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="FollowedHyperlink"/>
  <w:LsdException Locked="false" Priority="22" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Document Map"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Plain Text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="E-mail Signature"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Top of Form"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Bottom of Form"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Normal (Web)"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Acronym"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Address"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Cite"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Code"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Definition"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Keyboard"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Preformatted"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Sample"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Typewriter"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Variable"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Normal Table"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="annotation subject"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="No List"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Outline List 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Outline List 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Outline List 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Simple 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Simple 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Simple 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Colorful 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Colorful 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Colorful 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table 3D effects 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table 3D effects 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table 3D effects 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Contemporary"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Elegant"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Professional"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Subtle 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Subtle 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Web 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Web 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Web 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Balloon Text"/>
  <w:LsdException Locked="false" Priority="39" Name="Table Grid"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Theme"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Note Level 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Note Level 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Note Level 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Note Level 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Note Level 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Note Level 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Note Level 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Note Level 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Note Level 9"/>
  <w:LsdException Locked="false" SemiHidden="true" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" SemiHidden="true" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" QFormat="true"
   Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" QFormat="true"
   Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" QFormat="true"
   Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" QFormat="true"
   Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" QFormat="true"
   Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" QFormat="true"
   Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" SemiHidden="true"
   UnhideWhenUsed="true" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="TOC Heading"/>
  <w:LsdException Locked="false" Priority="41" Name="Plain Table 1"/>
  <w:LsdException Locked="false" Priority="42" Name="Plain Table 2"/>
  <w:LsdException Locked="false" Priority="43" Name="Plain Table 3"/>
  <w:LsdException Locked="false" Priority="44" Name="Plain Table 4"/>
  <w:LsdException Locked="false" Priority="45" Name="Plain Table 5"/>
  <w:LsdException Locked="false" Priority="40" Name="Grid Table Light"/>
  <w:LsdException Locked="false" Priority="46" Name="Grid Table 1 Light"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark"/>
  <w:LsdException Locked="false" Priority="51" Name="Grid Table 6 Colorful"/>
  <w:LsdException Locked="false" Priority="52" Name="Grid Table 7 Colorful"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 1"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 1"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 1"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 2"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 2"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 2"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 3"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 3"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 3"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 4"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 4"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 4"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 5"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 5"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 5"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 6"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 6"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 6"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="46" Name="List Table 1 Light"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark"/>
  <w:LsdException Locked="false" Priority="51" Name="List Table 6 Colorful"/>
  <w:LsdException Locked="false" Priority="52" Name="List Table 7 Colorful"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 1"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 1"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 1"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 2"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 2"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 2"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 3"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 3"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 3"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 4"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 4"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 4"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 5"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 5"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 5"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 6"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 6"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 6"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Mention"/>
 </w:LatentStyles>
</xml><![endif]--><!-- [if gte mso 10]>
<style>
 /* Style Definitions */
table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:Calibri;}
</style>
<![endif]--> <!--StartFragment--></p>
<p class="MsoNormal" style="text-align: center; tab-stops: 2.0cm;" align="center"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">KELOMPOK KERJA KATALOG ELEKTRONIK&nbsp;</span><span style="font-family: ''Footlight MT Light''; font-size: 16px;">${ba?.nama_komoditas.toUpperCase()}</span></p>
<p class="MsoNormal" style="text-align: center;" align="center"><span style="font-size: 12.0pt; font-family: ''Footlight MT Light'';">LEMBAGA KEBIJAKAN PENGADAAN BARANG/JASA PEMERINTAH</span></p>
<div style="mso-element: para-border-div; border: none; border-bottom: solid windowtext 1.5pt; padding: 0cm 0cm 1.0pt 0cm;">
<p class="MsoNormal" style="text-align: center; border: none; mso-border-bottom-alt: solid windowtext 1.5pt; padding: 0cm; mso-padding-alt: 0cm 0cm 1.0pt 0cm;" align="center"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
</div>
<p class="MsoNormal" style="text-align: center; tab-stops: 0cm 49.5pt 63.0pt 279.0pt 324.0pt 360.0pt 396.0pt 432.0pt;" align="center"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal" style="text-align: center;" align="center"><span style="font-size: 12.0pt; font-family: ''Footlight MT Light'';">Berita Acara </span><span lang="EN-ID" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: EN-ID;">Evaluasi</span> <span lang="EN-ID" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: EN-ID;">dan</span> <span lang="EN-ID" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: EN-ID;">Klarifikasi</span> <span lang="EN-ID" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: EN-ID;">Teknis Serta Harga</span></p>
<p class="MsoNormal" style="text-align: center;" align="center"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Pemilihan Penyedia Katalog&nbsp;</span><span style="font-family: ''Footlight MT Light''; font-size: 16px;">${ba?.nama_komoditas}</span></p>
<p class="MsoNormal" style="text-align: center;" align="center"><span style="font-family: ''Footlight MT Light''; font-size: 16px;">${ba?.nama_penyedia}</span></p>
<p class="MsoNormal" style="text-align: center;" align="center"><span lang="EN-ID" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: EN-ID;">20</span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">/BAE-</span><span style="font-size: 12.0pt; font-family: ''Footlight MT Light'';">TH</span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">/PERALATANPENDIDIKAN</span><span lang="EN-ID" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: EN-ID;">SMK</span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">/03/201</span><span style="font-size: 12.0pt; font-family: ''Footlight MT Light'';">8</span></p>
<p class="MsoNormal" style="text-align: center;" align="center"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Pada hari ini</span> <span style="font-family: ''Footlight MT Light''; font-size: 16px;">${hari}</span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">,</span><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;"> tanggal&nbsp;</span><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">${tanggal}&nbsp;Bulan&nbsp;</span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">${bulan}&nbsp;</span><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">tahun&nbsp;</span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">${tahun}&nbsp;</span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">(${formatTanggal}</span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">)&nbsp;</span>&nbsp;</span><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Pukul ____</span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span><span lang="EN-ID" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: EN-ID;">WIB </span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">s.d </span><span lang="EN-ID" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: EN-ID;">____ WIB </span><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">bertempat di _____</span><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">, telah dilakukan </span><span lang="EN-ID" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: EN-ID;">Evaluasi</span> <span lang="EN-ID" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: EN-ID;">dan Klarifikasi Harga</span> <span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">terhadap&nbsp;<span style="text-align: center;">${ba?.nama_penyedia}</span>&nbsp;</span><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">untuk </span><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Pemilihan Penyedia Katalog Elektronik dengan hasil sebagai berikut:</span></p>
<p class="MsoNormal" style="line-height: 115%; tab-stops: 1.0cm;"><span style="font-size: 12.0pt; line-height: 115%; font-family: ''Footlight MT Light''; mso-bidi-font-family: Calibri;">&nbsp;</span></p>
<div align="center">
<p><span style="text-align: justify;">${produk_penawaran}</span></p>
</div>
<p class="MsoNormal" style="text-align: center;"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">&nbsp;</span></strong></p>
<p class="MsoNormal"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Kesimpulan : </span></strong></p>
<p class="MsoNormal" style="margin-left: 14.2pt; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: FI;"><span style="mso-list: Ignore;">1.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp; </span></span></span></strong><!--[endif]--><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Lulus untuk produk nomor 2 s/d 7;</span></strong></p>
<p class="MsoNormal" style="margin-left: 14.2pt; text-indent: -18.0pt; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-fareast-font-family: ''Footlight MT Light''; mso-bidi-font-family: ''Footlight MT Light''; mso-ansi-language: FI;"><span style="mso-list: Ignore;">2.<span style="font: 7.0pt ''Times New Roman'';">&nbsp;&nbsp;&nbsp; </span></span></span></strong><!--[endif]--><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Tidak Lulus untuk produk nomor 1.</span></strong></p>
<p class="MsoNormal"><strong style="mso-bidi-font-weight: normal;"><span lang="EN-ID" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: EN-ID;">&nbsp;</span></strong></p>
<p class="MsoNormal"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></p>
<p class="MsoNormal"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">D</span><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">emikian Berita Acara Hasil </span><span lang="EN-ID" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: EN-ID;">Evaluasi dan Klarifikasi Teknis Serta Harga</span><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;"> ini dibuat dan ditandatangani pada hari, tanggal dan bulan sebagaimana tersebut diatas untuk dipergunakan sebagaimana mestinya</span></p>
<p class="MsoNormal" style="line-height: 150%;"><strong style="mso-bidi-font-weight: normal;"><span lang="IN" style="font-size: 12.0pt; line-height: 150%; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></strong></p>
<p class="MsoNormal" style="text-align: center;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Kelompok Kerja Katalog Elektronik Peralatan Pendidikan SMK</span></strong></p>
<p class="MsoNormal" style="text-align: center;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah </span></strong></p>
<div align="center">&nbsp;</div>
<p class="MsoNormal" style="text-align: center;" align="center"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;"><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;">${listPokja}</span></span></p>
<p class="MsoBodyText2"><strong style="mso-bidi-font-weight: normal;"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></strong></p>
<p class="MsoNormal" style="text-align: center;" align="center"><strong><span style="font-family: ''Footlight MT Light''; font-size: 16px;">${ba?.nama_penyedia}</span></strong></p>
<p class="MsoBodyText2" style="text-align: center;" align="center"><strong><span lang="SV" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: SV;">&nbsp;</span></strong></p>
<div align="center">
<table class="MsoNormalTable" style="width: 415.65pt; border-collapse: collapse; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt;" border="0" width="416" cellspacing="0" cellpadding="0">
<tbody>
<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 29.25pt;">
<td style="width: 50.35pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 29.25pt;" width="50">
<p class="MsoBodyText2" style="text-align: center;" align="center"><strong><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">No.</span></strong></p>
</td>
<td style="width: 111.75pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 29.25pt;" width="112">
<p class="MsoBodyText2" style="text-align: center;" align="center"><strong><span lang="SV" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: SV;">Nama</span></strong></p>
</td>
<td style="width: 154.55pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 29.25pt;" valign="top" width="155">
<p class="MsoBodyText2" style="text-align: center;" align="center"><strong><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">&nbsp;</span></strong></p>
<p class="MsoBodyText2" style="text-align: center;" align="center"><strong><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">Jabatan</span></strong></p>
</td>
<td style="width: 99.0pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 29.25pt;" width="99">
<p class="MsoBodyText2" style="text-align: center;" align="center"><strong><span lang="SV" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: SV;">Tanda Tanga</span></strong><strong><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN;">n</span></strong></p>
</td>
</tr>
<tr style="mso-yfti-irow: 1;">
<td style="width: 50.35pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="50">
<p class="MsoBodyText2" style="margin-top: 12.0pt; text-align: center;" align="center"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN; mso-bidi-font-weight: bold;">1.</span></p>
</td>
<td style="width: 111.75pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="112">
<p class="MsoBodyText2" style="margin-top: 12.0pt;">&nbsp;</p>
</td>
<td style="width: 154.55pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="155">
<p class="MsoBodyText2" style="margin-top: 12.0pt; text-align: center;" align="center">&nbsp;</p>
</td>
<td style="width: 99.0pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="99">
<p class="MsoBodyText2" style="margin-top: 12.0pt; text-align: center;" align="center"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN; mso-bidi-font-weight: bold;">.............................</span></p>
</td>
</tr>
<tr style="mso-yfti-irow: 2; mso-yfti-lastrow: yes;">
<td style="width: 50.35pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="50">
<p class="MsoBodyText2" style="margin-top: 12.0pt; text-align: center;" align="center"><span style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-bidi-font-weight: bold;">2.</span></p>
</td>
<td style="width: 111.75pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="112">
<p class="MsoBodyText2" style="margin-top: 12.0pt;">&nbsp;</p>
</td>
<td style="width: 154.55pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="155">
<p class="MsoBodyText2" style="margin-top: 12.0pt; text-align: center;" align="center">&nbsp;</p>
</td>
<td style="width: 99.0pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="99">
<p class="MsoBodyText2" style="margin-top: 12.0pt; text-align: center;" align="center"><span lang="IN" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: IN; mso-bidi-font-weight: bold;">.............................</span></p>
</td>
</tr>
</tbody>
</table>
</div>
<p class="MsoNormal" style="text-align: center;" align="center"><strong style="mso-bidi-font-weight: normal;"><span lang="FI" style="font-size: 12.0pt; font-family: ''Footlight MT Light''; mso-ansi-language: FI;">&nbsp;</span></strong></p>
<p><!--EndFragment--></p>', null, 2, '2018-08-15 10:32:14', 2, null, null, null, null, null, 0);

-- 15
CREATE TABLE IF NOT EXISTS `usulan_kualifikasi`(
  `id` Int( 11 ) AUTO_INCREMENT NOT NULL,
  `usulan_id` Int( 11 ) NOT NULL,
  `jenis_kualifikasi` VARCHAR(255) NOT NULL,
  `jenis_kualifikasi_alias` VARCHAR(255) NOT NULL,
  `created_date` DateTime NULL,
  `created_by` Int( 5 ) NULL,
  `modified_date` DateTime NULL,
  `modified_by` Int( 5 ) NULL,
  `deleted_date` DateTime NULL,
  `deleted_by` Int( 5 ) NULL,
  `audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `auditdate` Timestamp NULL,
  `audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `active` INT(1) NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;
-- 16
-- notes 24 10 2018 akan langsung di isikan di katalog 4
-- ALTER TABLE `user`
--   ADD `rkn_id` int(11) NULL;

-- 16 PART2 dan 21
-- berdasarkan analisa nextval ini sepertinya ditinggalkan karena id dari model2 hanya menggunakan id autoincrement
CREATE TABLE IF NOT EXISTS `sequence_data` (
  `sequence_name` varchar(100) NOT NULL,
  `sequence_increment` int(11) unsigned NOT NULL DEFAULT '1',
  `sequence_min_value` int(11) unsigned NOT NULL DEFAULT '1',
  `sequence_max_value` bigint(20) unsigned NOT NULL DEFAULT '18446744073709551615',
  `sequence_cur_value` bigint(20) unsigned DEFAULT '1',
  `sequence_cycle` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sequence_name`)
) ENGINE=MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci;

INSERT INTO sequence_data (sequence_name, sequence_increment, sequence_min_value, sequence_max_value, sequence_cur_value, sequence_cycle)
VALUES ('seq_epns', 1, 1, 18446744073709551615, 112, 0);

CREATE FUNCTION nextval(seq_name VARCHAR(100))
  RETURNS BIGINT
  BEGIN
    DECLARE cur_value BIGINT(20);
    SELECT LAST_INSERT_ID(null) INTO cur_value;
    UPDATE sequence_data
    SET sequence_cur_value = LAST_INSERT_ID(
                               IF (
                                 (sequence_cur_value + sequence_increment) > sequence_max_value,
                                 IF (
                                   sequence_cycle = TRUE,
                                   sequence_min_value,
                                   NULL
                                     ),
                                 sequence_cur_value + sequence_increment
                                   )
        )
    WHERE sequence_name = seq_name;
    SELECT LAST_INSERT_ID() INTO cur_value;
    RETURN cur_value;
  END;

-- 17
CREATE TABLE IF NOT EXISTS `usulan_dokumen_penawaran`(
  `id` Int( 11 ) AUTO_INCREMENT NOT NULL,
  `usulan_id` Int( 11 ) NOT NULL,
  `nama_dokumen` VARCHAR(255) NOT NULL,
  `created_date` DateTime NULL,
  `created_by` Int( 5 ) NULL,
  `modified_date` DateTime NULL,
  `modified_by` Int( 5 ) NULL,
  `deleted_date` DateTime NULL,
  `deleted_by` Int( 5 ) NULL,
  `audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `auditdate` Timestamp NULL,
  `audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `active` INT(1) NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;

-- 18
CREATE TABLE IF NOT EXISTS `blob_table` (
  id int(11) NOT NULL primary key auto_increment,
  `blb_id_content` int(11) NOT NULL,
  `blb_versi` int(11) DEFAULT NULL,
  `blb_date_time` datetime DEFAULT NULL,
  `blb_path` varchar(500) DEFAULT NULL,
  `blb_engine` varchar(255) DEFAULT NULL,
  `blb_mirrored` int(11) DEFAULT NULL,
  `blb_ukuran` int(11) DEFAULT NULL,
  `cdn_host_id` int(255) DEFAULT NULL,
  `audittype` varchar(255) DEFAULT NULL,
  `auditupdate` timestamp NULL DEFAULT NULL,
  `audituser` varchar(255) DEFAULT NULL,
  `classUsed` text default null
  -- ,PRIMARY KEY (`blb_id_content`)
  -- , UNIQUE KEY `blob_table_blb_id_content_uindex` (`blb_id_content`) USING BTREE
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;

-- 19 a
ALTER TABLE `user`
  ADD `centrum_user_id` int(11) NULL;

ALTER TABLE `user`
  ADD `centrum_username` VARCHAR(100) NULL;

-- 19 b
ALTER TABLE `user_role_grup`
  ADD `batasi_pembelian` INT(1) DEFAULT 0;

ALTER TABLE `user_role_grup`
  ADD `nilai_batas_pembelian` INT(11);

-- 20
CREATE TABLE IF NOT EXISTS `dokumen_usulan` (
  `id`                int(11)   NOT NULL AUTO_INCREMENT,
  `usulan_id`         int(11)            DEFAULT NULL,
  `dok_judul`         varchar(255)       DEFAULT NULL,
  `dok_hash`          varchar(255)       DEFAULT NULL,
  `dok_signature`     varchar(255)       DEFAULT NULL,
  `dok_id_attachment` int(11)            DEFAULT NULL,
  `created_date`      timestamp NULL     DEFAULT CURRENT_TIMESTAMP
  ON UPDATE CURRENT_TIMESTAMP,
  `created_by`        int(11)            DEFAULT NULL,
  `modified_date`     timestamp NULL,
  `modified_by`       int(11)   NULL,
  `deleted_date`      timestamp NULL,
  `deleted_by`        int(11)            DEFAULT NULL,
  `audittype`         varchar(255)       DEFAULT NULL,
  `auditupdate`       timestamp NULL,
  `audituser`         varchar(255)       DEFAULT NULL,
  `dok_label`         varchar(255)       DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;
-- 21
-- liat ke atas no 16

-- 22
CREATE TABLE IF NOT EXISTS activeuser
(
  id INT PRIMARY KEY AUTO_INCREMENT,
  user_id VARCHAR(255),
  ip_address VARCHAR(255),
  session_id VARCHAR(255),
  payload JSON
)CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB;
-- 23
ALTER TABLE `produk_kategori_atribut` ADD COLUMN `level_hirarki` Int(2) DEFAULT 0;
-- 24
ALTER TABLE `produk_kategori` ADD COLUMN `level_kategori` Int(2) DEFAULT 0;
-- 25
ALTER TABLE `produk_lampiran`
  ADD COLUMN `dok_hash` VARCHAR(255);

ALTER TABLE `produk_lampiran`
  ADD COLUMN `dok_signature` VARCHAR(255);

ALTER TABLE `produk_lampiran`
  ADD COLUMN `dok_id_attachment` int(11);
-- 26
CREATE TABLE IF NOT EXISTS `dokumen_penawaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penawaran_id` int(11) DEFAULT NULL,
  `dok_judul` varchar(255) DEFAULT NULL,
  `dok_keterangan` varchar(255) DEFAULT NULL,
  `dok_file_name` varchar(255) DEFAULT NULL,
  `dok_hash` varchar(255) DEFAULT NULL,
  `dok_signature` varchar(255) DEFAULT NULL,
  `dok_id_attachment` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `modified_date` timestamp NULL,
  `modified_by` int(11) NOT NULL,
  `deleted_date` timestamp NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `audittype` varchar(255) DEFAULT NULL,
  `auditupdate` timestamp NULL,
  `audituser` varchar(255) DEFAULT NULL,
  `dok_label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
)CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;
-- 27
UPDATE produk_gambar SET modified_date = NULL WHERE CAST(modified_date AS CHAR(20)) = '0000-00-00 00:00:00';
ALTER TABLE `produk_gambar`
  ADD COLUMN `dok_hash` VARCHAR(255);

ALTER TABLE `produk_gambar`
  ADD COLUMN `dok_signature` VARCHAR(255);

ALTER TABLE `produk_gambar`
  ADD COLUMN `dok_id_attachment` int(11);

-- 28, 29, 30 harus manual
-- no 31
UPDATE produk SET berlaku_sampai = NULL WHERE CAST(berlaku_sampai AS CHAR(20)) = '0000-00-00';
ALTER TABLE produk ADD agr_total_dibeli INT NULL;
-- 32
ALTER TABLE komoditas ADD agr_jumlah_produk_terjual INT NULL;
-- 33
create PROCEDURE updateJumlahProdukTerjualKomoditas()
  BEGIN
    update komoditas k set k.agr_jumlah_produk_terjual = (select sum(p.agr_total_dibeli) from produk p where komoditas_id = k.id);
  END;

create PROCEDURE updateJumlahProdukTerjual()
  BEGIN
    update produk prod set prod.agr_total_dibeli = (
                                                   select sum(pp.kuantitas) from paket_produk pp join paket pak on pp.paket_id = pak.id
                                                   where pp.produk_id = prod.id and pak.apakah_selesai = 1 and pak.apakah_batal = 0
                                                   ) where (prod.harga_utama != null or prod.harga_utama != 0) and prod.active = 1 and prod.apakah_ditayangkan = 1 and prod.minta_disetujui = 0
                                                       and berlaku_sampai < curdate() and prod.setuju_tolak = 'setuju';
  END;

create EVENT updateProdukAndKomoditas ON SCHEDULE EVERY 1 DAY STARTS '2017-09-20 23:59.00'
DO BEGIN
  CALL updateJumlahProdukTerjual();
  CALL updateJumlahProdukTerjualKomoditas();
END;
-- no 34
ALTER TABLE `tahapan`
  DROP COLUMN `apakah_fitur_paten`;

ALTER TABLE `tahapan`
  DROP COLUMN `jenis_tahapan`;
-- no 35
ALTER TABLE `komoditas_template_jadwal`
  ADD COLUMN `tahapan_penawaran` INT(11) NULL;

ALTER TABLE `komoditas_template_jadwal`
  ADD COLUMN `tahapan_negosiasi` INT(11) NULL;
-- 36
INSERT INTO role_grup (nama_grup, kategori, active, created_date) VALUES ("Tahapan", "komoditas", 1, CURDATE());
INSERT INTO role_item (role_grup_id, nama_role, deskripsi, active, created_date) VALUES
  ((SELECT id FROM role_grup WHERE nama_grup = "Tahapan"), "comTahapan", "Akses Master Data Tahapan", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Tahapan"), "comTahapanAdd", "Akses Tambah Tahapan", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Tahapan"), "comTahapanEdit", "Akses Edit Tahapan", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Tahapan"), "comTahapanDelete", "Akses Hapus Tahapan", 1, CURDATE());
-- 37
INSERT into user_role_item (user_role_grup_id, role_item_id, active, created_date, created_by, modified_date, modified_by)
VALUES
       (1, (select id from role_item where nama_role = "comTahapan"), 1, curdate(), -99, curdate(), -99),
       (1, (select id from role_item where nama_role = "comTahapanAdd"), 1, curdate(), -99, curdate(), -99),
       (1, (select id from role_item where nama_role = "comTahapanEdit"), 1, curdate(), -99, curdate(), -99),
       (1, (select id from role_item where nama_role = "comTahapanDelete"), 1, curdate(), -99, curdate(), -99);
-- 38
INSERT INTO role_grup (nama_grup, kategori, active, created_date) VALUES ("Berita", "cms", 1, CURDATE());
INSERT INTO role_item (role_grup_id, nama_role, deskripsi, active, created_date) VALUES
  ((SELECT id FROM role_grup WHERE nama_grup = "Berita"), "comBerita", "Akses Manajemen Berita", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Berita"), "comBeritaAdd", "Akses Tambah Berita", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Berita"), "comBeritaEdit", "Akses Edit Berita", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Berita"), "comBeritaDelete", "Akses Hapus Berita", 1, CURDATE());
-- 39
INSERT into user_role_item (user_role_grup_id, role_item_id, active, created_date, created_by, modified_date, modified_by)
VALUES
       (1, (select id from role_item where nama_role = "comBerita"), 1, curdate(), -99, curdate(), -99),
       (1, (select id from role_item where nama_role = "comBeritaAdd"), 1, curdate(), -99, curdate(), -99),
       (1, (select id from role_item where nama_role = "comBeritaEdit"), 1, curdate(), -99, curdate(), -99),
       (1, (select id from role_item where nama_role = "comBeritaDelete"), 1, curdate(), -99, curdate(), -99);
-- 40
INSERT into role_basic (nama_role, nama_role_alias, active, created_date)
VALUES
       ("Pokja", "Pokja", 1, CURDATE());
-- 41
ALTER TABLE `usulan`
  ADD `template_jadwal_id` int(11);

ALTER TABLE `usulan`
  ADD `pokja_id` int(11);
-- 42
INSERT INTO role_grup (nama_grup, kategori, active, created_date) VALUES ("Dokumen Template", "cms", 1, CURDATE());
INSERT INTO role_item (role_grup_id, nama_role, deskripsi, active, created_date) VALUES
  ((SELECT id FROM role_grup WHERE nama_grup = "Dokumen Template"), "comDocumentTemplate", "Akses Template Dokumen", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Dokumen Template"), "comDocumentTemplateAdd", "Akses Tambah Template Dokumen", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Dokumen Template"), "comDocumentTemplateEdit", "Akses Edit Template Dokumen", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Dokumen Template"), "comDocumentTemplateDelete", "Akses Hapus Template Dokumen", 1, CURDATE());
-- 43
INSERT into user_role_item (user_role_grup_id, role_item_id, active, created_date, created_by, modified_date, modified_by)
VALUES
       (1, (select id from role_item where nama_role = "comDocumentTemplate"), 1, curdate(), -99, curdate(), -99),
       (1, (select id from role_item where nama_role = "comDocumentTemplateAdd"), 1, curdate(), -99, curdate(), -99),
       (1, (select id from role_item where nama_role = "comDocumentTemplateEdit"), 1, curdate(), -99, curdate(), -99),
       (1, (select id from role_item where nama_role = "comDocumentTemplateDelete"), 1, curdate(), -99, curdate(), -99);
-- 44
-- sudah dirubah ke longtext diatas dan data di import dari yang dev, karena gak berubah
-- ALTER TABLE dokumen_template MODIFY template TEXT;
# INSERT INTO `dokumen_template`
#     (`jenis_dokumen`)
# VALUES ('SK PENETAPAN PRODUK'),('KONTRAK KATALOG');
-- 45
CREATE TABLE IF NOT EXISTS `parameter_aplikasi`(
  `id` Int( 11 ) AUTO_INCREMENT NOT NULL,
  `nama_parameter` VARCHAR(255) NOT NULL,
  `isi_parameter` VARCHAR(255) NOT NULL,
  `deskripsi` VARCHAR(255) NOT NULL,
  `created_date` DateTime NULL,
  `created_by` Int( 5 ) NULL,
  `modified_date` DateTime NULL,
  `modified_by` Int( 5 ) NULL,
  `deleted_date` DateTime NULL,
  `deleted_by` Int( 5 ) NULL,
  `audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `auditdate` Timestamp NULL,
  `audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `active` Int(1) NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;
-- 46
INSERT INTO `parameter_aplikasi` (nama_parameter, isi_parameter, deskripsi, active)
VALUES
       ("website_lkpp", "http://www.lkpp.go.id", "Website LKPP.", 1),
       ("faksimili_lkpp", "(021) 299 12451", "Nomor Fax LKPP.", 1),
       ("lokasi_lkpp", "Gedung  LKPP, Komplek Rasuna Epicentrum, Jl. Epicentrum Tengah Lot 11B, Jakarta Selatan", "Lokasi/Alamat LKPP", 1);
-- 47
INSERT INTO role_grup (nama_grup, kategori, active, created_date) VALUES ("Parameter Aplikasi", "master_data", 1, CURDATE());
INSERT INTO role_item (role_grup_id, nama_role, deskripsi, active, created_date) VALUES
  ((SELECT id FROM role_grup WHERE nama_grup = "Parameter Aplikasi"), "comParameterAplikasi", "Akses Manajemen Parameter Aplikasi", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Parameter Aplikasi"), "comParameterAplikasiEdit", "Akses Edit Parameter Aplikasi", 1, CURDATE());
-- 48
INSERT into user_role_item (user_role_grup_id, role_item_id, active, created_date, created_by, modified_date, modified_by)
VALUES
       (1, (select id from role_item where nama_role = "comParameterAplikasi"), 1, curdate(), -99, curdate(), -99),
       (1, (select id from role_item where nama_role = "comParameterAplikasiEdit"), 1, curdate(), -99, curdate(), -99);
-- 49
INSERT INTO role_item (role_grup_id, nama_role, deskripsi, active, created_date) VALUES ((SELECT id FROM role_grup WHERE nama_grup = "Usulan"), "comUsulanUbahJadwal", "Ubah Jadwal Usulan", 1, CURDATE())
-- 50
INSERT into user_role_item (user_role_grup_id, role_item_id, active, created_date, created_by, modified_date, modified_by)
VALUES
       (1, (select id from role_item where nama_role = "comUsulanUbahJadwal"), 1, curdate(), -99, curdate(), -99)
-- 51
CREATE TABLE IF NOT EXISTS `usulan_jadwal_riwayat`(
  `id` Int( 11 ) AUTO_INCREMENT NOT NULL,
  `user_id` Int( 11 ) NOT NULL,
  `usulan_id` Int( 11 ) NOT NULL,
  `tahapan_id` Int( 11 ) NOT NULL,
  `tanggal_mulai` DateTime NULL,
  `tanggal_selesai` DateTime NULL,
  `keterangan` VARCHAR(255) NOT NULL,
  `created_date` DateTime NULL,
  `created_by` Int( 5 ) NULL,
  `modified_date` DateTime NULL,
  `modified_by` Int( 5 ) NULL,
  `deleted_date` DateTime NULL,
  `deleted_by` Int( 5 ) NULL,
  `audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `auditdate` Timestamp NULL,
  `audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `active` Int(1) NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;
-- 52
ALTER TABLE `produk`
  ADD `penawaran_id` int(11);
ALTER TABLE `produk`
  ADD `status` VARCHAR(255);
-- 53
CREATE TABLE IF NOT EXISTS `komoditas_template_kontrak`(
  `id` Int( 11 ) AUTO_INCREMENT NOT NULL,
  `komoditas_id` Int( 11 ) NOT NULL,
  `jenis_dokumen` VARCHAR(255) NOT NULL,
  `template` text NOT NULL,
  `created_date` DateTime NULL,
  `created_by` Int( 5 ) NULL,
  `modified_date` DateTime NULL,
  `modified_by` Int( 5 ) NULL,
  `deleted_date` DateTime NULL,
  `deleted_by` Int( 5 ) NULL,
  `audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `auditdate` Timestamp NULL,
  `audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `active` Int(1) NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;
-- 54
CREATE TABLE IF NOT EXISTS `usulan_kategori_produk`(
  `id` Int( 11 ) AUTO_INCREMENT NOT NULL,
  `usulan_id` Int( 11 ) NOT NULL,
  `produk_kategori_id` Int(11) NOT NULL,
  `created_date` DateTime NULL,
  `created_by` Int( 5 ) NULL,
  `modified_date` DateTime NULL,
  `modified_by` Int( 5 ) NULL,
  `deleted_date` DateTime NULL,
  `deleted_by` Int( 5 ) NULL,
  `audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `auditdate` Timestamp NULL,
  `audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `active` Int(1) NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;
-- 55
CREATE TABLE IF NOT EXISTS `paket_pembayaran_lampiran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paket_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `file_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `original_file_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_size` bigint(20) NOT NULL DEFAULT '0',
  `file_sub_location` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `deskripsi` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `posisi_file` int(11) NOT NULL DEFAULT '1',
  `doc_attachment_id` int(11) DEFAULT NULL,
  `item_param` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` int(1) NOT NULL DEFAULT '1',
  `active` int(11) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `modified_date` timestamp NULL DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `deleted_date` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ppenlamp_ppi` (`model_id`)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;
-- ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;
ALTER TABLE `paket_penerimaan` ADD COLUMN `paket_pengiriman_id` int(11) NULL;
ALTER TABLE `paket_penerimaan` ADD COLUMN `paket_pembayaran_id` int(11) NULL;
ALTER TABLE `paket_pengiriman` ADD COLUMN `paket_penerimaan_pembayaran_id` int(11) NULL;
ALTER TABLE `paket_pembayaran` ADD COLUMN `transaction_type` int(1) NOT NULL DEFAULT 1;
-- 56
-- catatan 24 10 18, akan di isikan datanya di katalog 4
ALTER TABLE `penyedia`
ADD COLUMN `rkn_id` INT(11);
-- 56 b
ALTER TABLE `paket_penerimaan_lampiran` ADD COLUMN `blb_id` int(11) NULL;
ALTER TABLE `paket_pengiriman_lampiran` ADD COLUMN `blb_id` int(11) NULL;
ALTER TABLE `paket_pembayaran_lampiran` ADD COLUMN `blb_id` int(11) NULL; -- di no 56 part 2 pemabyaran
ALTER TABLE `paket_kontrak` ADD COLUMN `blb_id` int(11) NULL;
-- 57
ALTER TABLE `penyedia_distributor`
  ADD COLUMN `rkn_id` INT(11);
-- 58 diperbesar jadi 255 juga no 59
ALTER TABLE paket MODIFY kode_anggaran VARCHAR(255);
-- 59
alter table konten add tags varchar(255) after slug;
-- 60
CREATE TABLE if not exists `usulan_penyedia`(
  `id` Int( 11 ) AUTO_INCREMENT NOT NULL,
  `user_id` Int( 11 ) NOT NULL,
  `usulan_id` Int( 11 ) NOT NULL,
  `penyedia_id` Int(11) NULL,
  `created_date` DateTime NULL,
  `created_by` Int( 5 ) NULL,
  `modified_date` DateTime NULL,
  `modified_by` Int( 5 ) NULL,
  `deleted_date` DateTime NULL,
  `deleted_by` Int( 5 ) NULL,
  `audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `auditdate` Timestamp NULL,
  `audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `active` Int(1) NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;
-- 61
-- ALTER TABLE `penawaran` ADD COLUMN `user_id` INT(11);
ALTER TABLE `penawaran`
  ADD COLUMN `penyedia_id` INT(11);
-- 62
ALTER TABLE `penawaran`
  ADD COLUMN `pic_jabatan` VARCHAR(255);
ALTER TABLE `penawaran`
  ADD COLUMN `alasan_tolak` VARCHAR(255);
ALTER TABLE `penawaran`
  ADD COLUMN `alasan_revisi` VARCHAR(255);
ALTER TABLE `penawaran`
  ADD COLUMN `data_sikap` JSON;
ALTER TABLE `penawaran`
  ADD COLUMN `batas_akhir_revisi` Date;
-- 63
ALTER TABLE `dokumen_penawaran` ADD COLUMN `active` INT(1);
-- 64
ALTER TABLE `dokumen_penawaran` MODIFY `modified_by` INT(11) NULL;
-- 65
alter table konten_statis add konten_kategori_id int(11) after id;
-- 67
ALTER TABLE `produk` MODIFY `harga_ongkir` DECIMAL(20,2) NULL;
-- 67 b
CREATE TABLE if not exists `syarat_ketentuan_hasil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `syarat_ketentuan_id` int(11) NOT NULL,
  `client_ip` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(2) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `deleted_date` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT=DYNAMIC;
-- 68
ALTER TABLE `produk` MODIFY `penyedia_id` INT(11) NULL;
-- 69
CREATE TABLE IF NOT EXISTS `produk_harga`(
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`produk_id` Int( 11 ) NOT NULL,
	`kurs_id` Int( 11 ) NOT NULL,
	`harga` text NOT NULL,
	`tanggal_dibuat` DateTime NULL,
	`apakah_disetujui` Int( 1 ) NULL,
	`tanggal_disetujui` DateTime NULL,
	`tanggal_ditolak` DateTime NULL,
	`setuju_tolak_oleh` Int( 5 ) NULL,
	`created_date` DateTime NULL,
	`created_by` Int( 5 ) NULL,
	`modified_date` DateTime NULL,
	`modified_by` Int( 5 ) NULL,
	`deleted_date` DateTime NULL,
	`deleted_by` Int( 5 ) NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`auditdate` Timestamp NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`active` Int(1) NULL,
	PRIMARY KEY (`id`)
)  CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;
-- 70 supposed exist
ALTER TABLE `produk_tunggu_setuju` ADD COLUMN `deleted_date` DateTime NULL;
ALTER TABLE `produk_tunggu_setuju` ADD COLUMN `deleted_by` Int( 5 ) NULL;
-- 71 supposed exist
ALTER TABLE `produk_wilayah_jual_provinsi` ADD COLUMN `approved` Int(1) NULL;
-- 72
ALTER TABLE `produk_wilayah_jual_kabupaten` MODIFY `approved` Int(1) NULL;
-- 73
INSERT INTO role_grup (nama_grup,kategori,active,created_date) VALUES ('Kategori Konten','cms',1,CURDATE());
INSERT INTO role_item (role_grup_id, nama_role, deskripsi, active, created_date) VALUES
  ((SELECT id FROM role_grup WHERE nama_grup = "Kategori Konten"), "comKategoriKonten", "Akses kategori konten", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Kategori Konten"), "comKategoriKontenDelete", "Hapus kategori konten", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Kategori Konten"), "comKategoriKontenEdit", "Edit kategori konten", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Kategori Konten"), "comKategoriKontenAdd", "Tambah kategori konten", 1, CURDATE());
-- 74
alter table pustaka_file change pustaka_file_kategori_id konten_kategori_id int(11);
update pustaka_file set konten_kategori_id = 17 where konten_kategori_id = 1;
update pustaka_file set konten_kategori_id = 18 where konten_kategori_id = 2;
-- 75 sudah ada
 INSERT INTO role_item (role_grup_id, nama_role, deskripsi, active, created_date) VALUES ((select id from role_grup where nama_grup = "Penawaran"), "comPenawaranCetakBaHasilEvaluasi", "Mencetak Berita Acara Hasil Evaluasi", 1, CURDATE());
-- 75 b
ALTER TABLE sumber_dana_paket ADD created_date DATETIME NULL;
ALTER TABLE sumber_dana_paket ADD modified_date DATETIME NULL;
ALTER TABLE sumber_dana_paket ADD deleted_date DATETIME NULL;
ALTER TABLE sumber_dana_paket ADD created_by INT NULL;
ALTER TABLE sumber_dana_paket ADD modified_by INT NULL;
ALTER TABLE sumber_dana_paket ADD deleted_by INT NULL;
-- 76
INSERT INTO role_item (role_grup_id, nama_role, deskripsi, active, created_date) VALUES ((select id from role_grup where nama_grup = "Penawaran"), "comPenawaranPersetujuanProduk", "Menyetujui Produk dari Penawaran Penyedia", 1, CURDATE());
-- 76 b,
ALTER TABLE produk_harga ADD komoditas_harga_atribut_id INT NULL;
ALTER TABLE produk_harga ADD kelas_harga VARCHAR(255) NULL;

UPDATE produk_harga_nasional SET harga_tanggal = NULL WHERE CAST(harga_tanggal  AS CHAR(20)) = '0000-00-00';
UPDATE produk_harga_nasional SET created_date = NULL WHERE CAST(created_date  AS CHAR(20)) = '0000-00-00 00:00:00';
UPDATE produk_harga_nasional SET modified_date = NULL WHERE CAST(modified_date  AS CHAR(20)) = '0000-00-00 00:00:00';
UPDATE produk_harga_nasional SET deleted_date = NULL WHERE CAST(deleted_date  AS CHAR(20)) = '0000-00-00 00:00:00';
UPDATE produk_harga_nasional SET approved_date = NULL WHERE CAST(approved_date  AS CHAR(20)) = '0000-00-00 00:00:00';

CREATE INDEX nasional_produk_id_harga_atribut_id_tanggal_index ON produk_harga_nasional (produk_id, komoditas_harga_atribut_id, harga_tanggal);
CREATE INDEX migration_idx ON produk_harga_kabupaten (produk_id, komoditas_harga_atribut_id, created_date);
-- 77 sudah ada
-- ALTER TABLE penyedia_kontrak ADD file_hash VARCHAR(255) NULL;
-- 78
ALTER TABLE produk_harga ADD alasan_ditolak VARCHAR(255);
-- 79
ALTER TABLE produk ADD kontrak_id INT;
-- 79 b
ALTER TABLE produk_harga ADD harga_xls_json JSON;
ALTER TABLE produk_harga ADD checked_locations_json JSON;
-- 80

CREATE TABLE `auto_sequence`(
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
 	PRIMARY KEY (`id`)
 ) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 443609;

 -- 81

 CREATE TABLE IF NOT EXISTS `audit_trail`(
	`id` Int(11) AUTO_INCREMENT NOT NULL,
	`action` VARCHAR(100) NULL,
	`path` text NULL,
	`payload` text NULL,
	`remoteAddress` VARCHAR(50) NULL,
	`method` VARCHAR(10) NULL,
	`session` VARCHAR(50) NULL,
	`user_agent` text NULL,
	`query_string` text NULL,
	`created_by` Int(11) NULL,
	`created_at` VARCHAR(30) NULL,
	PRIMARY KEY (`id`)
)  CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;
-- 82
INSERT INTO role_item (role_grup_id, nama_role, deskripsi, active, created_date) VALUES
  ((select id from role_grup where nama_grup = "Penyedia"), "comCetakDokumenSKPenetapanProduk", "Cetak SK penetapan produk", 1, CURDATE());
-- 82 b
CREATE TABLE IF NOT EXISTS `usulan_dokumen_pengadaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usulan_id` int(11) NOT NULL,
  `dok_judul` varchar(255) DEFAULT NULL,
  `dok_keterangan` varchar(255) DEFAULT NULL,
  `dok_file_name` varchar(255) DEFAULT NULL,
  `dok_hash` varchar(255) DEFAULT NULL,
  `dok_signature` varchar(255) DEFAULT NULL,
  `dok_id_attachment` int(11) DEFAULT NULL,
  `active` int(1) NULL,
  `file_size` BigInt( 20 ) NULL,
  `created_date` DateTime NULL,
  `created_by` int(11) NULL,
  `modified_date` Datetime NULL,
  `modified_by` int(11) NULL,
  `deleted_date` Datetime NULL,
  `deleted_by` int(11) NULL,
  `audittype` varchar(255) DEFAULT NULL,
  `auditupdate` Timestamp NULL,
  `audituser` varchar(255) DEFAULT NULL,
  `dok_label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;
-- 83
ALTER TABLE usulan ADD judul_pengumuman VARCHAR(255);
-- 84
ALTER TABLE produk ADD ongkir JSON NULL;
-- 85
ALTER TABLE produk ADD edit_able TIMESTAMP NULL;
-- 86
ALTER TABLE usulan_dokumen_penawaran ADD jenis_evaluasi VARCHAR(100) AFTER usulan_id;
-- 87
INSERT into user_role_item (user_role_grup_id, role_item_id, active, created_date, created_by, modified_date, modified_by)
    VALUES
      (2, (select id from role_item where nama_role = "comPaketRiwayatPembayaranInput"), 1, curdate(), -99, curdate(), -99);

-- 88
INSERT INTO role_item (role_grup_id, nama_role, deskripsi, active, created_date) VALUES
(16, "comProdukBukaEditFitur", "Buka fitur ubah produk", 1, CURDATE());
-- 89
CREATE TABLE if not exists dokumen_kategori (
  id int(11) AUTO_INCREMENT,
  nama_kategori varchar(255),
  active int(11),
  created_date datetime,
  created_by int(5),
  modified_date datetime,
  modified_by int(5),
  deleted_date datetime,
  deleted_by int(5),
  PRIMARY KEY(id)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;

CREATE TABLE if not exists dokumen_upload (
  id int(11) AUTO_INCREMENT,
  dokumen_kategori_id int(11),
  penyedia_id int(11),
  penawaran_id int(11),
  nama_dokumen varchar(255),
  file_path varchar(100),
  active int(11),
  created_date datetime,
  created_by int(5),
  modified_date datetime,
  modified_by int(5),
  deleted_date datetime,
  deleted_by int(5),
  PRIMARY KEY(id)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;

INSERT INTO dokumen_kategori(nama_kategori,active,created_date,created_by) VALUE('BA Evaluasi',1,now(),2);
INSERT INTO dokumen_kategori(nama_kategori,active,created_date,created_by) VALUE('BA Pembuktian',1,now(),2);
INSERT INTO dokumen_kategori(nama_kategori,active,created_date,created_by) VALUE('Kontrak Katalog',1,now(),2);
INSERT INTO dokumen_kategori(nama_kategori,active,created_date,created_by) VALUE('SK Penetapan',1,now(),2);
INSERT INTO dokumen_kategori(nama_kategori,active,created_date,created_by) VALUE('Surat Pesanan',1,now(),2);
-- 90
alter table dokumen_upload add COLUMN data text AFTER file_path;
-- 91
alter table komoditas_template_jadwal modify COLUMN deskripsi text;
-- 92
CREATE TABLE if not exists `berita_acara` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usulan_id` int(11) DEFAULT NULL,
  `tanggal` Date NULL,
  `tahapan` varchar(255) DEFAULT NULL,
  `pembahasan` varchar(2000) DEFAULT NULL,
  `nama_file` varchar(255) DEFAULT NULL,
  `file_hash` varchar(255) DEFAULT NULL,
  `blb_id_content` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `modified_date` timestamp NULL,
  `modified_by` int(11) NULL,
  `deleted_date` timestamp NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `audittype` varchar(255) DEFAULT NULL,
  `auditupdate` timestamp NULL,
  `audituser` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;
-- 93
alter table paket
add column satuan_kerja_kabupaten int(11) DEFAULT NULL,
add COLUMN pengiriman_alamat varchar(1000) default null,
add column pengiriman_kabupaten int(11) DEFAULT NULL;
-- 94
alter table report add COLUMN reporter_telp varchar(25) not null;
-- 95
ALTER TABLE report ADD jenis_laporan varchar(255) NOT NULL;
-- 95 b
CREATE TABLE if not exists `rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
	`pointRating_id` int(11) NOT NULL,
	`valueRating` int(11) NOT NULL,
	`penyedia_id` int(11) NOT NULL,
	`feedBack` text DEFAULT NULL,
  `created_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(255) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;

CREATE TABLE if not exists `point_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
	`nama_point` varchar(255) NOT NULL,
	`deskripsi` text DEFAULT NULL,
  `created_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(255) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;

alter table point_rating add column active int(1);
-- 96
-- bikin dulu rolegrupnya = dapeting role_group_id
insert into role_grup (nama_grup, kategori,active) values ('Others','Akses','1'); -- ->dev->34
insert into role_grup (nama_grup, kategori,active) values ('Rating','cms','1'); -- ->dev->35
-- tambah aksesnya , button buy
insert into role_item(role_grup_id, nama_role, deskripsi, active) values ((select id from role_grup where nama_grup='Others'),'comBuyButtonKomoditas','Tombol Beli Button pada komoditas',1);
-- tambah aksesnya, menu point-rating
insert into role_item(role_grup_id, nama_role, deskripsi, active) values ((select id from role_grup where nama_grup='Rating'),'comMenuPointRating','Menu Point Rating',1);
-- 97
CREATE TABLE if not exists `agenda_kegiatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usulan_id` int(11) DEFAULT NULL,
  `tanggal_kegiatan` date DEFAULT NULL,
  `waktu_kegiatan` time DEFAULT NULL,
  `judul_kegiatan` varchar(255) DEFAULT NULL,
  `lokasi` varchar(255) DEFAULT NULL,
  `keterangan` text,
  `nama_file` varchar(255) DEFAULT NULL,
  `blb_id_content` int(11) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `deleted_date` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;

-- 98
insert into role_item (role_grup_id,nama_role,deskripsi,active,created_date) VALUES(25,'comUsulanAgendaKegiatanAdd','Tambah Agenda kegiatan',1,now());
insert into role_item (role_grup_id,nama_role,deskripsi,active,created_date) VALUES(25,'comUsulanAgendaKegiatEdit','Edit Agenda kegiatan',1,now());
-- 99
-- alter table produk_gambar add column deskripsi text;
-- alter table produk_gambar add column thumb_url text;
-- alter table produk_lampiran add column deskripsi text;
-- alter table produk_lampiran add column file_url text;
-- alter table produk_lampiran add column active int;
-- 100
alter table paket_pengiriman add ongkir decimal (20,5);
-- 101
alter table paket add column agr_total_harga_diterima_dan_ongkir numeric(20,5) not null default 0;
alter table paket_pengiriman add column agr_total_harga_diterima_dan_ongkir numeric(20,5) not null default 0;
-- 102 sudah ada
/*
ALTER TABLE blob_table add COLUMN id INT not null primary key auto_increment;
alter table blob_table drop index blob_table_blb_id_content_uindex;
ALTER TABLE blob_table DROP INDEX `blob_table_blb_id_content_uindex`;
ALTER TABLE blob_table MODIFY COLUMN blb_id_content INT not null ;
alter table blob_table add column classUsed varchar(255) not null;
alter table blob_table MODIFY column classUsed varchar(255) null;
*/
-- 103
-- ALTER TABLE paket_penerimaan MODIFY COLUMN agr_total_harga_diterima_dan_ongkir decimal(20, 5) NULL DEFAULT NULL ;
ALTER TABLE paket_penerimaan
  ADD COLUMN agr_total_harga_diterima_dan_ongkir
  decimal(20, 5) NULL DEFAULT NULL ;
-- 104
create TABLE if not exists blacklist
(
	id bigint auto_increment primary key
	, blacklist_source_id bigint
	, rkn_id varchar(20)
	, repo_id varchar(20)
	, sk varchar(500)
	, nama_penyedia varchar(500)
	, npwp varchar(500)
	, alamat varchar(500)
	, alamat_tambahan varchar(500)
	, direktur varchar(500)
	, npwp_direktur varchar(500)
	, tahun_anggaran int
	, created_at date
	, sk_pencabutan varchar(500)
	, kabupaten_id bigint
	, kabupaten_nama varchar(500)
	, provinsi_id bigint
	, provinsi_name varchar(500)
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;
alter table blacklist add column satker_id bigint;
alter table blacklist add column satker_nama varchar(500);
alter table blacklist CHANGE provinsi_name  provinsi_nama varchar(500);
create table if not exists blacklist_pelanggaran
(
	id bigint auto_increment primary key
	, blacklist_id bigint references blacklist(id)
	, sk varchar(500)
	, started_at date
	, expired_at date
	, jenis_pelanggaran varchar(500)
	, deskripsi_pelanggaran varchar(2000)
	, created_at date
	, published_at date
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;
-- 105
drop table dce_kabupaten;
create table dce_provinsi (id int(11) primary key, nama varchar(500));
create table dce_kabupaten (id int(11) primary key, dce_provinsi_id int(11) references dce_provinsi(id), nama varchar(500));
-- 106
alter table provinsi add dce_id int(11) default null;
alter table kabupaten add dce_id int(11) default null;
-- 107
DELIMITER $$
CREATE FUNCTION levenshtein( s1 VARCHAR(255), s2 VARCHAR(255) )
  RETURNS INT
  DETERMINISTIC
  BEGIN
    DECLARE s1_len, s2_len, i, j, c, c_temp, cost INT;
    DECLARE s1_char CHAR;
    -- max strlen=255
    DECLARE cv0, cv1 VARBINARY(256);
    SET s1_len = CHAR_LENGTH(s1), s2_len = CHAR_LENGTH(s2), cv1 = 0x00, j = 1, i = 1, c = 0;
    IF s1 = s2 THEN
      RETURN 0;
    ELSEIF s1_len = 0 THEN
      RETURN s2_len;
    ELSEIF s2_len = 0 THEN
      RETURN s1_len;
    ELSE
      WHILE j <= s2_len DO
        SET cv1 = CONCAT(cv1, UNHEX(HEX(j))), j = j + 1;
      END WHILE;
      WHILE i <= s1_len DO
        SET s1_char = SUBSTRING(s1, i, 1), c = i, cv0 = UNHEX(HEX(i)), j = 1;
        WHILE j <= s2_len DO
          SET c = c + 1;
          IF s1_char = SUBSTRING(s2, j, 1) THEN
            SET cost = 0; ELSE SET cost = 1;
          END IF;
          SET c_temp = CONV(HEX(SUBSTRING(cv1, j, 1)), 16, 10) + cost;
          IF c > c_temp THEN SET c = c_temp; END IF;
            SET c_temp = CONV(HEX(SUBSTRING(cv1, j+1, 1)), 16, 10) + 1;
            IF c > c_temp THEN
              SET c = c_temp;
            END IF;
            SET cv0 = CONCAT(cv0, UNHEX(HEX(c))), j = j + 1;
        END WHILE;
        SET cv1 = cv0, i = i + 1;
      END WHILE;
    END IF;
    RETURN c;
  END;

CREATE FUNCTION levenshtein_ratio( s1 VARCHAR(255), s2 VARCHAR(255) )
  RETURNS INT
  DETERMINISTIC
  BEGIN
    DECLARE s1_len, s2_len, max_len INT;
    SET s1_len = LENGTH(s1), s2_len = LENGTH(s2);
    IF s1_len > s2_len THEN
      SET max_len = s1_len;
    ELSE
      SET max_len = s2_len;
    END IF;
    RETURN ROUND((1 - LEVENSHTEIN(s1, s2) / max_len) * 100);
  END;
select levenshtein('abc','aac') as result;
select k.id, dk.id, k.nama_kabupaten, dk.nama
from provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
where k.dce_id is null
and   k.active=1
-- and dp.id=1
and ((
		k.nama_kabupaten like 'Kabupaten%'
		and dk.nama like '%(Kab.)')
	or
	(
		k.nama_kabupaten like 'Kota%'
		and dk.nama like '%(Kota)')
)
-- and (
--	replace(replace(k.nama_kabupaten,'Kabupaten ',''),' ','') like concat('%', replace( replace(dk.nama,'(Kab.)','') ,' ',''),'%')
-- OR
--	replace(replace(dk.nama,'(Kab.)',''),' ','') like concat('%', replace( replace(k.nama_kabupaten,'Kabupaten ','') ,' ',''),'%')
-- )
-- and levenshtein(replace(replace(replace(k.nama_kabupaten,'Kabupaten ',''),' ',''),'-','') ,  replace( replace( replace(dk.nama,'(Kab.)','') ,' ','') , '-','') ) < 3;
order by k.nama_kabupaten, dk.nama;
-- update
update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and k.nama_kabupaten=dk.nama;

update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and replace(k.nama_kabupaten,'Kabupaten ','')=replace(dk.nama,'(Kab.)','')
;

update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and replace(k.nama_kabupaten,'Kota ','')=replace(dk.nama,'(Kota)','')
;

update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and replace(replace(replace(k.nama_kabupaten,'Kabupaten ',''),' ',''),'-','') = replace( replace( replace(dk.nama,'(Kab.)','') ,' ','') , '-','')
;

update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and replace(replace(replace(k.nama_kabupaten,'Kota ',''),' ',''),'-','') = replace( replace( replace(dk.nama,'(Kota)','') ,' ','') , '-','')
;


update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and levenshtein(replace(replace(replace(k.nama_kabupaten,'Kabupaten ',''),' ',''),'-','') ,  replace( replace( replace(dk.nama,'(Kab.)','') ,' ','') , '-','') ) < 3;
;


update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and levenshtein(replace(replace(replace(k.nama_kabupaten,'Kota ',''),' ',''),'-','') ,  replace( replace( replace(dk.nama,'(Kota)','') ,' ','') , '-','') ) < 3;
;

update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and replace(replace(replace(replace(k.nama_kabupaten,'Kota ',''),' ',''),'-',''),'Kepulauan','') =
replace(replace( replace( replace(dk.nama,'(Kota)','') ,' ','') , '-',''),'Kepulauan','')
;

update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and replace(replace(replace(replace(k.nama_kabupaten,'Kabupaten ',''),' ',''),'-',''),'Kepulauan','') =
replace(replace( replace( replace(dk.nama,'(Kab.)','') ,' ','') , '-',''),'Kepulauan','')
;


update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and replace(replace(replace(replace(k.nama_kabupaten,'Kabupaten ',''),' ',''),'-',''),'Pulau','') =
replace(replace( replace( replace(dk.nama,'(Kab.)','') ,' ','') , '-',''),'Pulau','')
;


update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and replace(replace(replace(replace(k.nama_kabupaten,'Kota ',''),' ',''),'-',''),'Pulau','') =
replace(replace( replace( replace(dk.nama,'(Kota)','') ,' ','') , '-',''),'Pulau','')
;

-- tambahan dari pengecekan setealh run
alter table konten add column viewer int(11) null;
-- chat
create table chat
(
  id            int auto_increment
    primary key,
  id_pengirim   int          null,
  id_penerima   int          null,
  ppk_user_id   int          null,
  paket_id      int          null,
  produk_id     int          null,
  pesan_akhir   text         null,
  file          varchar(255) null,
  baca_penerima int(255)     null,
  baca_pengirim int(255)     null,
  active        varchar(255) null,
  created_date  datetime     null
  on update CURRENT_TIMESTAMP,
  created_by    varchar(255) null,
  modified_date datetime     null
  on update CURRENT_TIMESTAMP,
  modified_by   varchar(255) null,
  deleted_date  datetime     null
  on update CURRENT_TIMESTAMP,
  deleted_by    varchar(255) null
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB;

-- auto-generated definition
create table produk_diskusi
(
  id              int auto_increment
    primary key,
  produk_id       int          null,
  user_id         int          null,
  user_nama       varchar(255) null,
  user_foto       varchar(255) null,
  diskusi         text         null,
  user_penerima   int          null,
  status_komentar int(2)       null,
  active          varchar(255) null,
  created_date    datetime     null,
  created_by      varchar(255) null,
  modified_date   datetime     null,
  modified_by     varchar(255) null,
  deleted_date    datetime     null,
  deleted_by      varchar(255) null
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB;

create table produk_diskusi_balas
(
  id                int auto_increment
    primary key,
  produk_id         int          null,
  produk_diskusi_id int          null,
  user_id           int          null,
  user_nama         varchar(255) null,
  user_foto         varchar(255) null,
  diskusi_balas     text         null,
  active            varchar(255) null,
  created_date      datetime     null,
  created_by        varchar(255) null,
  modified_date     datetime     null,
  modified_by       varchar(255) null,
  deleted_date      datetime     null,
  deleted_by        varchar(255) null
) CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB;


-- 108 : modify field faq_kategori_id to konten_kategori_id on table faq
ALTER TABLE faq CHANGE faq_kategori_id konten_kategori_id int(11);

-- 109 : insert data table konten_kategori
INSERT INTO `konten_kategori` (parent_id,nama_kategori,deskripsi,posisi_kategori,active,created_date,created_by) VALUES
('1', 'Katalog', 'Berita dengan kategori ini adalah berita yang isi kontenya bersangkutan atau berhubungan dengan e-katalog', '1', '1', NOW(), '2'),
('3', 'ePurchasing', 'FAQ mengenai ePurchasing', '1', '1', NOW(), '2'),
('3', 'eKatalog', 'FAQ mengenai eKatalog', '1', '1', NOW(), '2'),
('3', 'Cara Mendaftar di SPSE', 'FAQ Terkait Registrasi Online', '1', '1', NOW(), '2'),
('4', 'Undangan', 'Unggah Undangan', '1', '1', NOW(), '2'),
('4', 'Informasi', 'Unggah Informasi', '1', '1', NOW(), '2'),
('5', 'Salah Kategori', 'Produk Salah Kategori', '1', '1', NOW(), '2'),
('5', 'Harga Tidak Wajar', 'Harga Produk Tidak Wajar', '1', '1', NOW(), '2'),
('5', 'Transaksi', 'Transaksi Produk', '1', '1', NOW(), '2'),
('5', 'Pelanggaran Merk Dagang', 'Pelanggaran Merk Dagang', '1', '1', NOW(), '2'),
('5', 'Lain-Lain', 'Laporan Lainnya', '1', '1', NOW(), '2'),
('6', 'Berita Acara Hasil Evaluasi Teknis', 'Berita Acara Hasil Evaluasi Teknis', '1', '1', NOW(), '2'),
('6', 'Berita Acara Hasil Evaluasi Kualifikasi', 'Berita Acara Hasil Evaluasi Kualifikasi', '1', '1', NOW(), '2'),
('6', 'Surat Pesanan', 'Surat Pesanan', '1', '1', NOW(), '2'),
('6', 'SK Penetapan Produk', 'SK Penetapan Produk', '1', '1', NOW(), '2'),
('6', 'SK Perpanjangan Produk', 'SK Perpanjangan Produk', '1', '1', NOW(), '2'),
('6', 'Kontrak Katalog', 'Kontrak Katalog', '1', '1', NOW(), '2'),
('6', 'Berita Acara Evaluasi', 'Berita Acara Evaluasi', '1', '1', NOW(), '2'),
('6', 'Nodin Review', 'Nodin Review', '1', '1', NOW(), '2'),
('6', 'Berita Acara Pembuktian', 'Berita Acara Pembuktian', '1', '1', NOW(), '2'),
('6', 'Berita Acara Hasil Evaluasi Kualifikasi, Evaluasi Administrasi, dan Pembuktian Kualifikasi', 'Berita Acara Hasil Evaluasi Kualifikasi, Evaluasi Administrasi, dan Pembuktian Kualifikasi', '1', '1', NOW(), '2'),
('6', 'Berita Acara Evaluasi dan Klarifikasi Teknis serta Negosiasi Harga', 'Berita Acara Evaluasi dan Klarifikasi Teknis serta Negosiasi Harga', '1', '1', NOW(), '2');

-- tambah kolom insansi_id di kldi_satker
ALTER TABLE `kldi_satker` ADD `instansi_id` Int(11) null;
ALTER TABLE `kldi_satker` ADD `active` tinyint(1) null;

-- Tambah Role Item : comExportLaporan pada group Laporan, lewat input
-- tambah role item; comExecProdukElastics menu api execute manual for

-- penyesuaian prakatalog(untuk kedepannya nego non nego(omen))
alter table komoditas add prakatalog int(2) default 1;
-- tambah field dokumen kontrak di paket_kontrak
alter table paket_kontrak add column dokumen_kontrak longtext null;
-- update storage location: latihan-e-katalog.lkpp.go.id
UPDATE configuration set cfg_value='/home/ekatalog5/appserv/dataapps/storage' WHERE cfg_sub_category='file.storage-dir';
UPDATE configuration set cfg_value='/home/ekatalog5/appserv/dataapps/altstorage' WHERE cfg_sub_category='file-storage-dir';

-- Merubah table berita acara dengan menghapus dan menambahkan
DROP TABLE IF EXISTS `berita_acara`;
CREATE TABLE `berita_acara` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penawaran_id` int(11) DEFAULT NULL,
  `dokumen_template_id` int(11) DEFAULT NULL,
  `no_ba` varchar(100) DEFAULT NULL,
  `dokumen_ba` longtext,
  `nama_file` varchar(255) DEFAULT NULL,
  `blb_id` int(11) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `deleted_date` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `audittype` varchar(255) DEFAULT NULL,
  `auditupdate` timestamp NULL DEFAULT NULL,
  `audituser` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;

-- Tambah kolom penawaran_id di penyedia_kontrak
ALTER TABLE `penyedia_kontrak` ADD `penawaran_id` Int(11) null;

-- buka kunci penyedia 1 komoditas 1 kontrak
DROP INDEX uq_penyedia_komoditas_id ON penyedia_komoditas;

-- tambah kolom kontrak_id di penawaran
ALTER TABLE penawaran ADD kontrak_id int(11) NULL;

-- tambah kolom tkdn di produk
ALTER TABLE produk ADD tkdn_produk varchar(100) NULL;

-- tambah kolom kldi di komoditas
ALTER TABLE komoditas ADD kldi_id varchar(6) NULL;

-- ubah collation tabel kldi
ALTER TABLE kldi COLLATE=utf8_unicode_ci;

-- perpanjangan kontrak
create table if not exists perpanjangan_kontrak
(
  id             int(11) auto_increment,
  usulan_id      int(11) null ,
  penawaran_id   int(11) null ,
  kontrak_id     int(11) null ,
  penyedia_id    int(11) null,
  selesai        int(1)  null,
  created_date   timestamp default CURRENT_TIMESTAMP null
  on update CURRENT_TIMESTAMP,
  created_by     int(11)                                 null,
  modified_date  timestamp                           null,
  modified_by    int(11)                                 null,
  deleted_date  timestamp                           null,
  deleted_by    int(11)                                 null,
  constraint unique_id
  unique (id)
  )CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB;

create table if not exists perpanjangan_staging
(
  id             int(11) auto_increment,
  perpanjangan_id      int(11) null ,
  class_name varchar(255),
  isList        int(1)  default 0,
  object_json text,
  created_date   timestamp default CURRENT_TIMESTAMP null
  on update CURRENT_TIMESTAMP,
  created_by     int(11)                             null,
  modified_date  timestamp                           null,
  modified_by    int(11)                             null,
  deleted_date  timestamp                            null,
  deleted_by    int(11)                              null,
  constraint unique_id
  unique (id)
  )CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB;

CREATE TABLE `dokumen_usulan_pokja` (
                                      `id` int(11) NOT NULL AUTO_INCREMENT,
                                      `usulan_id` int(11) DEFAULT NULL,
                                      `dok_judul` varchar(255) DEFAULT NULL,
                                      `dok_hash` varchar(255) DEFAULT NULL,
                                      `dok_signature` varchar(255) DEFAULT NULL,
                                      `dok_id_attachment` int(11) DEFAULT NULL,
                                      `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                      `created_by` int(11) DEFAULT NULL,
                                      `modified_date` timestamp NULL DEFAULT NULL,
                                      `modified_by` int(11) DEFAULT NULL,
                                      `deleted_date` timestamp NULL DEFAULT NULL,
                                      `deleted_by` int(11) DEFAULT NULL,
                                      `audittype` varchar(255) DEFAULT NULL,
                                      `auditupdate` timestamp NULL DEFAULT NULL,
                                      `audituser` varchar(255) DEFAULT NULL,
                                      `dok_label` varchar(255) DEFAULT NULL,
                                      PRIMARY KEY (`id`),
                                      UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

CREATE TABLE `usulan_pokja` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `usulan_id` int(11) NOT NULL,
                              `pokja_id` int(11) NOT NULL,
                              `active` int(2) NOT NULL DEFAULT '1',
                              `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `created_by` int(11) NOT NULL,
                              `modified_date` timestamp NULL DEFAULT NULL,
                              `modified_by` int(11) DEFAULT NULL,
                              `deleted_date` timestamp NULL DEFAULT NULL,
                              `deleted_by` int(11) DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              UNIQUE KEY `uq_usulan_pokja_id` (`usulan_id`,`pokja_id`, `active`)
) ENGINE=InnoDB AUTO_INCREMENT=22229 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

insert into usulan_pokja(usulan_id, pokja_id, active, created_date, created_by)
select id, pokja_id, active, NOW(), -99 from usulan where pokja_id is not null;

-- tambah kolom minta_disetujui di penyedia_distributor
ALTER TABLE penyedia_distributor ADD minta_disetujui int(2) NOT NULL;
ALTER TABLE penyedia_distributor ADD setuju_tolak_oleh int(11) NULL;
ALTER TABLE penyedia_distributor ADD setuju_tolak_tanggal timestamp NULL;
ALTER TABLE penyedia_distributor ADD setuju_tolak varchar(10) NULL;
ALTER TABLE penyedia_distributor ADD setuju_tolak_alasan text COLLATE utf8_unicode_ci;
-- update field persetujuan di penyedia_distributor
update penyedia_distributor set minta_disetujui_oleh = 2, minta_disetujui_tanggal = NOW(), setuju_tolak = 'setuju';
-- add column penawaran_id di berita acara
ALTER TABLE `berita_acara` ADD `penawaran_id` Int(11) null;
ALTER TABLE `berita_acara` ADD `no_ba` varchar(100) DEFAULT NULL;
ALTER TABLE `berita_acara` ADD `dokumen_ba` longtext;
ALTER TABLE `berita_acara` ADD `blb_id` int(11) DEFAULT NULL;
ALTER TABLE `berita_acara` ADD `active` int(1) DEFAULT NULL;
ALTER TABLE `berita_acara` ADD  `dokumen_template_id` int(11) DEFAULT NULL;
-- tambahkan kolom konten_kategori_id di report
alter table report add COLUMN konten_kategori_id int(11) DEFAULT NULL;
alter table report add COLUMN created_by Int( 11 ) NULL;
alter table report add COLUMN modified_by Int( 11 ) NULL;
alter table report add COLUMN deleted_by Int( 11 ) NULL;
alter table report add COLUMN modified_date DateTime NULL;
alter table report add COLUMN deleted_date DateTime NULL;
-- tambahkan komlom terkait report riwayat
alter table report_riwayat add COLUMN nama_file varchar(255) DEFAULT NULL;
alter table report_riwayat add COLUMN created_by Int( 11 ) NULL;
alter table report_riwayat add COLUMN modified_by Int( 11 ) NULL;
alter table report_riwayat add COLUMN deleted_by Int( 11 ) NULL;
alter table report_riwayat add COLUMN created_date DateTime NULL;
alter table report_riwayat add COLUMN modified_date DateTime NULL;
alter table report_riwayat add COLUMN deleted_date DateTime NULL;
alter table report_riwayat add COLUMN file_hash varchar(255) DEFAULT NULL;
alter table report_riwayat add COLUMN blb_id_content Int( 11 ) NULL;
-- peserta usulan
create table peserta_usulan
(
  id            int auto_increment
    primary key,
  usulan_id     int       null,
  user_id       int       null,
  active        int(1)    null,
  created_date  timestamp null,
  created_by    int       null,
  modified_date timestamp null,
  modified_by   int       null,
  deleted_date  timestamp null,
  deleted_by    int       null,
  constraint peserta_usulan_ibfk_1
    foreign key (usulan_id) references usulan (id),
  constraint peserta_usulan_ibfk_2
    foreign key (user_id) references user (id)
);

-- tambahkan kolom batas pengiriman di paket produk
alter  table  paket_produk add  column batas_pengiriman DateTime NULL;

-- tambahkkan kolom batas pengiriman paket produk nego detail
alter table paket_produk_nego_detail add  column  batas_pengiriman DateTime NULL;

-- Menambahkan kolom data_kontrak
ALTER  table penawaran add column  data_kontrak json null ;

-- tambahkan kolom untuk review
-- tambah acl grup Usulan -> comUsulanReview
alter table usulan add COLUMN doc_review varchar(255) DEFAULT NULL;
alter table usulan add COLUMN doc_review_id Int( 11 ) NULL;

-- TAMBAH TABLE USULAN DOKUMEN PERPANJANGAN
create table if not exists usulan_dokumen_perpanjangan
(
  id int auto_increment primary key ,
  usulan_id     int           null ,
  dok_label     varchar (255) null ,
  deskripsi     varchar (255) null ,
  active        int           null ,
  created_date  timestamp     null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  created_by    int           null ,
  modified_date timestamp     null ,
  modified_by   int           null ,
  deleted_date  timestamp     null ,
  deleted_by    int           null
)CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;

-- TAMBAH TABLE DOKUMEN PERPANJANGAN
create table if not exists dokumen_perpanjangan
(
  id int auto_increment primary key ,
  perp_id            int           null ,
  usulan_dok_perp_id     int           null ,
  dok_judul          varchar (255) null ,
  dok_hash          varchar(255)   null ,
  dok_id_attachment int(11)        null ,
  keterangan        varchar(255)   null ,
  active             int           null ,
  created_date       timestamp     null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  created_by         int           null ,
  modified_date      timestamp     null ,
  modified_by        int           null ,
  deleted_date       timestamp     null ,
  deleted_by         int           null
)CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT = 1;

-- TAMBAH TABEL BROADCAST
CREATE TABLE `broadcast`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `deskripsi` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `filter_id` int(11) NULL DEFAULT NULL,
  `user_role_grup_id` int(11) NULL DEFAULT NULL,
  `komoditas_id` int(11) NULL DEFAULT NULL,
  `active` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_date` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `created_by` int(11) NULL DEFAULT NULL,
  `modified_date` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted_date` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- TAMBAH TABEL BROADCAST DETAIL
CREATE TABLE `broadcast_detail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `broadcast_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `is_read` int(2) NULL DEFAULT 0,
  `active` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_date` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `modified_date` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted_date` datetime(0) NULL DEFAULT NULL,
  `deleted_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- MENAMBAHKAN KOLOM penyedia_distributor_id KE TABEL CHAT
ALTER TABLE chat ADD penyedia_distributor_id int(11) NULL DEFAULT NULL;

-- table wilayah komoditas
create table if not exists komoditas_wilayah
(
  id int(11) NOT NULL AUTO_INCREMENT primary key,
  kldi_id VARCHAR(6) not null ,
  komoditas_id int(11) not null ,
  prp_id int(11) not null ,
  kbp_id int(11) not null ,
  created_date datetime(0) NULL DEFAULT NULL,
  created_by int(11) NULL DEFAULT NULL,
  modified_date datetime(0) NULL DEFAULT NULL,
  modified_by int(11) NULL DEFAULT NULL,
  deleted_date datetime(0) NULL DEFAULT NULL,
  deleted_by int(11) NULL DEFAULT NULL
)CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT =1;

-- table penyedia-wilayah
create table if not exists penyedia_wilayah
(
  id int(11) NOT NULL AUTO_INCREMENT primary key,
  kldi_id VARCHAR(6) not null ,
  penyedia_id int(11) not null ,
  prp_id int(11) not null ,
  kbp_id int(11) not null ,
  created_date datetime(0) NULL DEFAULT NULL,
  created_by int(11) NULL DEFAULT NULL,
  modified_date datetime(0) NULL DEFAULT NULL,
  modified_by int(11) NULL DEFAULT NULL,
  deleted_date datetime(0) NULL DEFAULT NULL,
  deleted_by int(11) NULL DEFAULT NULL
)CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT =1;

-- table penyedia-sektoral
create table if not exists penyedia_sektoral
(
  id int(11) NOT NULL AUTO_INCREMENT primary key,
  kldi_id VARCHAR(6) not null ,
  penyedia_id int(11) not null ,
  created_date datetime(0) NULL DEFAULT NULL,
  created_by int(11) NULL DEFAULT NULL,
  modified_date datetime(0) NULL DEFAULT NULL,
  modified_by int(11) NULL DEFAULT NULL,
  deleted_date datetime(0) NULL DEFAULT NULL,
  deleted_by int(11) NULL DEFAULT NULL
)CHARACTER SET = utf8 COLLATE = utf8_general_ci ENGINE = InnoDB AUTO_INCREMENT =1;



-- Add column id_job
ALTER TABLE history_datafeed ADD COLUMN id_job varchar(255);
ALTER TABLE history_datafeed
  ADD COLUMN created_date datetime(0) NULL DEFAULT NULL,
  ADD COLUMN created_by int(11) NULL DEFAULT NULL,
  ADD COLUMN modified_date datetime(0) NULL DEFAULT NULL,
  ADD COLUMN modified_by int(11) NULL DEFAULT NULL,
  ADD COLUMN deleted_date datetime(0) NULL DEFAULT NULL,
  ADD COLUMN deleted_by int(11) NULL DEFAULT NULL;

-- tabel penawaran manufaktur
CREATE TABLE `penawaran_manufaktur` (
    `id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
    `penawaran_id` Int( 11 ) not null ,
    `manufaktur_id` Int( 11 ) not null ,
    `komoditas_id` Int( 11 ) not null ,
    `produk_kategori_id` Int( 11 ) not null ,
    `created_date` Timestamp NULL,
    `created_by` Int( 11 ) NULL,
    `modified_date` Timestamp NULL,
    `modified_by` Int( 11 ) NULL,
    `deleted_date` Timestamp NULL,
    `deleted_by` Int( 11 ) NULL,
    `active` INT( 1 ) NULL DEFAULT '0',
    PRIMARY KEY ( `id` ) )
    CHARACTER SET = utf8
    COLLATE = utf8_general_ci
    ENGINE = InnoDB
    AUTO_INCREMENT = 1;

-- penambahan kolom konten_kategori_utama_id
ALTER TABLE `konten_kategori` ADD `konten_kategori_utama_id` int(11) NULL;

UPDATE `user` SET `rkn_id` = 4328042 where user_name like 'marketing_altrak';

alter table activeuser add logindate timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
alter table activeuser add logoutdate timestamp NULL DEFAULT NULL;

-- ----------------------------
-- penambahan tabel bidang
-- ----------------------------
CREATE TABLE `bidang`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_bidang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_date` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `modified_date` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `modified_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `deleted_date` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
-- end penambahan tabel bidang

-- penambahan kolom bidang_id di tabel komoditas
ALTER TABLE   komoditas   ADD   bidang_id int (10) NULL DEFAULT NULL AFTER id;