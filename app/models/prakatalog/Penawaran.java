package models.prakatalog;

import controllers.BaseController;
import models.BaseKatalogTable;
import models.common.AktifUser;
import models.common.DokumenKontrak;
import models.jcommon.util.CommonUtil;
import models.prakatalog.contracts.PenawaranStatusContract;
import models.util.sikap.Sikap;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;
import play.mvc.Router;
import services.prakatalog.UsulanService;
import utils.DateTimeUtils;
import utils.KatalogUtils;
import utils.LogUtil;

import javax.persistence.Transient;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dadang on 8/8/17.
 */

@Table(name = "penawaran")
public class Penawaran extends BaseKatalogTable implements PenawaranStatusContract {

    public static final String TAG = "Penawaran";

    @Id
    public Long id;
    public Long usulan_id;
    public Long komoditas_id;
    public Long penyedia_id;
    public Long user_id;
    public String pic_nama;
    public String pic_email;
    public String pic_no_telp;
    public String pic_alamat;
    public String pic_jabatan;
    public String status;
    public String alasan_tolak;
    public String alasan_revisi;
    public String data_sikap;
    public Date batas_akhir_revisi;
    public boolean active;
    public Long kontrak_id;
    public String data_kontrak;

    @Transient
    public String nama_penyedia;

    @Transient
    public String nama_lengkap;

    @Transient
    public String nama_usulan;

    @Transient
    public Long pokja_id;

    @Transient
    public Long doc_review_id;

    @Transient
    public String nama_komoditas;

    @Transient
    public Long komoditas_kategori_id;

    @Transient
    public Long tahapan_penawaran;

    @Transient
    public Long tahapan_negosiasi;

    @Transient
    public String kelas_harga;

    @Transient
    public boolean revisi_sudah_kadaluarsa;

    @Transient
    public String tanggal_batas_akhir_boleh_revisi;

    public static final String STATUS_MENUNGGU_VERIFIKASI = "menunggu_verifikasi";
    public static final String STATUS_LOLOS_KUALIFIKASI = "lolos_kualifikasi";
    public static final String STATUS_GAGAL_KUALIFIKASI = "gagal_kualifikasi";
    public static final String STATUS_REVISI = "revisi";
    public static final String STATUS_NEGOSIASI = "negosiasi";

    public static final String STATUS_NEGOSIASI_SELESAI = "negosiasi_selesai";
    public static final String STATUS_PERSIAPAN_KONTRAK = "persiapan_kontrak";
    public static final String STATUS_KONTRAK_DIPROSES = "kontrak_diproses";
    public static final String STATUS_SELESAI = "selesai";

    public static final int KOMODITAS_DATAFEED = 3;

    public static final String[] NOT_ALLOWED_ADD_PRODUCT = {
            STATUS_SELESAI,
            STATUS_NEGOSIASI_SELESAI,
            STATUS_PERSIAPAN_KONTRAK,
            STATUS_KONTRAK_DIPROSES,
            STATUS_REVISI
    };

    public static final String[] ALLOWED_BRITA_ACARA = {
            STATUS_NEGOSIASI_SELESAI, STATUS_MENUNGGU_VERIFIKASI, STATUS_LOLOS_KUALIFIKASI, STATUS_NEGOSIASI, STATUS_NEGOSIASI_SELESAI,
            STATUS_PERSIAPAN_KONTRAK, STATUS_KONTRAK_DIPROSES, STATUS_SELESAI
    };

    public static Map<String,String> statusMaps(){

        Map<String,String> map = new HashMap<String,String>();
        map.put(STATUS_MENUNGGU_VERIFIKASI,KatalogUtils.getStatusLabel(STATUS_MENUNGGU_VERIFIKASI));
        map.put(STATUS_LOLOS_KUALIFIKASI,KatalogUtils.getStatusLabel(STATUS_LOLOS_KUALIFIKASI));
        map.put(STATUS_REVISI,KatalogUtils.getStatusLabel(STATUS_REVISI));
        map.put(STATUS_GAGAL_KUALIFIKASI,KatalogUtils.getStatusLabel(STATUS_GAGAL_KUALIFIKASI));
        map.put(STATUS_NEGOSIASI,KatalogUtils.getStatusLabel(STATUS_NEGOSIASI));
        map.put(STATUS_NEGOSIASI_SELESAI,KatalogUtils.getStatusLabel(STATUS_NEGOSIASI_SELESAI));
        map.put(STATUS_KONTRAK_DIPROSES,KatalogUtils.getStatusLabel(STATUS_KONTRAK_DIPROSES));
        map.put(STATUS_SELESAI,KatalogUtils.getStatusLabel(STATUS_SELESAI));

        return map;

    }

    public static Map<String,String> statusMapsAll(){

        Map<String,String> map = new HashMap<String,String>();
        map.put(STATUS_NEGOSIASI_SELESAI,KatalogUtils.getStatusLabel(STATUS_NEGOSIASI_SELESAI));
        map.put(STATUS_MENUNGGU_VERIFIKASI,KatalogUtils.getStatusLabel(STATUS_MENUNGGU_VERIFIKASI));
        map.put(STATUS_LOLOS_KUALIFIKASI,KatalogUtils.getStatusLabel(STATUS_LOLOS_KUALIFIKASI));
        map.put(STATUS_REVISI,KatalogUtils.getStatusLabel(STATUS_REVISI));
        map.put(STATUS_GAGAL_KUALIFIKASI,KatalogUtils.getStatusLabel(STATUS_GAGAL_KUALIFIKASI));
        map.put(STATUS_NEGOSIASI,KatalogUtils.getStatusLabel(STATUS_NEGOSIASI));
        map.put(STATUS_NEGOSIASI_SELESAI,KatalogUtils.getStatusLabel(STATUS_NEGOSIASI_SELESAI));
        map.put(STATUS_PERSIAPAN_KONTRAK,KatalogUtils.getStatusLabel(STATUS_PERSIAPAN_KONTRAK));
        map.put(STATUS_KONTRAK_DIPROSES,KatalogUtils.getStatusLabel(STATUS_KONTRAK_DIPROSES));
        map.put(STATUS_SELESAI,KatalogUtils.getStatusLabel(STATUS_SELESAI));

        return map;

    }

    public static long getJumlahPenawaran(long usulan_id){
        return Penawaran.count("usulan_id = ? and active = ?", usulan_id, AKTIF);
    }

    public static boolean checkTelahMelakukanPenawaran(long penyedia_id, long usulan_id){

        long jumlahPenawaran = Penawaran.count("penyedia_id = ? and usulan_id = ? and active = ?", penyedia_id, usulan_id, AKTIF);

        if(jumlahPenawaran > 0){
            return true;
        }
        return false;
    }

    public static Penawaran getPenawaranByPenyediaAndUsulan(long user_id, long usulan_id){
        return Penawaran.find("user_id =? and usulan_id = ? and active = ?",user_id,usulan_id, AKTIF).first();
    }

    public static Penawaran getPenawaranPenyediaKomoditas(Long penawaran_id){
        String query = "select pn.id as id, pn.usulan_id as usulan_id, k.nama_komoditas as nama_komoditas, p.nama_penyedia as nama_penyedia from penawaran pn join penyedia p " +
                "on pn.penyedia_id = p.id join usulan u on u.id = pn.usulan_id join komoditas k " +
                "on u.komoditas_id = k.id where pn.id = ?";

        return Query.find(query, Penawaran.class, penawaran_id).first();
    }

    public static Penawaran getPenawaranUserKomoditas(Long penawaran_id){
        String query = "select pn.id as id, pn.usulan_id as usulan_id, k.nama_komoditas as nama_komoditas, usr.nama_lengkap as nama_penyedia, usr.id as user_id from penawaran pn join user usr " +
                "on pn.user_id = usr.id join usulan u on u.id = pn.usulan_id join komoditas k " +
                "on u.komoditas_id = k.id where pn.id = ?";
        return Query.find(query, Penawaran.class, penawaran_id).first();
    }


    public static Penawaran getPenawaranAndUser(Long penawaran_id){
        String query = "select pn.id as id, u.nama_lengkap as nama_penyedia from penawaran pn join user u " +
                "on pn.user_id = u.id join " +
                "where pn.id = ?";

        return Query.find(query, Penawaran.class, penawaran_id).first();
    }

    public static Penawaran getDataBA(Long penawaran_id){
        String query = "select u.nama_lengkap nama_penyedia, kom.nama_komoditas, kom.kelas_harga from penawaran pn " +
                "join user u on pn.user_id = u.id join komoditas kom on kom.id = pn.komoditas_id where pn.id= "+penawaran_id;

        return Query.find(query, Penawaran.class).first();
    }

    public static boolean sudahMengajukanPenawaran(long user_id, long usulan_id){
        long total = Penawaran.count("user_id = ? and usulan_id = ?", user_id, usulan_id);
        LogUtil.debug(TAG, "total: " + total);
        return total > 0;
    }

    public static boolean sudahMengajukanPenawaranDanDitolak(long user_id, long usulan_id){
        long jumlahDitolak = Penawaran.count("user_id = ? and usulan_id = ? and status = ?",user_id, usulan_id, STATUS_GAGAL_KUALIFIKASI);
        LogUtil.debug(TAG, "total: " + jumlahDitolak);
        return jumlahDitolak > 1;
    }

    public Sikap getDataSikap() {
        if (TextUtils.isEmpty(this.data_sikap)) {
            return new Sikap();
        }
        return CommonUtil.fromJson(this.data_sikap, Sikap.class);
    }

    public void changeStatus(String status){
        this.status = status;
        this.active = true;
        this.save();
    }

    public DokumenKontrak getDataKontrak() {
        if (TextUtils.isEmpty(this.data_kontrak)) {
            return new DokumenKontrak();
        }
        return CommonUtil.fromJson(this.data_kontrak, DokumenKontrak.class);
    }

    @Override
    public String getStatusPenawaran() {
        return this.status;
    }

    public static List<Penawaran> findAllPenawaranForScheduler(){
        String query = "select p.id, p.status, p.usulan_id, ktj.tahapan_penawaran, ktj.tahapan_negosiasi " +
                "from penawaran p " +
                "join usulan u on u.id = p.usulan_id " +
                "join komoditas_template_jadwal ktj on ktj.id = u.template_jadwal_id " +
                "where p.active= ? and (p.status = ? or p.status = ?)";

        return Query.find(query,Penawaran.class, AKTIF,STATUS_LOLOS_KUALIFIKASI,STATUS_NEGOSIASI).fetch();
    }

    public static Penawaran getByUseridDanUsulanId(Long user_id, Long usulan_id){
        String query = "select * from penawaran where user_id = ? and usulan_id=? and active=1";
        return Query.find(query,Penawaran.class, user_id,usulan_id).first();
    }

    public static boolean isPenawaranKadaluarsa(Penawaran penawaran){
        boolean kadaluarsa = false;
        Date dateNow = new Date();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date todayWithZeroTime = null;
        try {
            todayWithZeroTime = formatter.parse(formatter.format(dateNow));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (penawaran.alasan_revisi != null){
            kadaluarsa = todayWithZeroTime.after(penawaran.batas_akhir_revisi) ? true : false;
        }

        return kadaluarsa;
    }

    public static String tetapkanBatasAwalRevisi(Long penawaran_id){
        String tglRevisi = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Date dateNow = new Date();
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date dateNowFormat = null;

        try {
            dateNowFormat = formatter.parse(formatter.format(dateNow));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String query = "select uj.tanggal_selesai as tanggal_selesai " +
                "from penawaran pnw " +
                "left join usulan us ON pnw.usulan_id = us.id " +
                "left join komoditas_template_jadwal knj ON us.template_jadwal_id = knj.id and knj.active = 1 " +
                "left join usulan_jadwal uj ON uj.usulan_id = us.id and knj.tahapan_penawaran = uj.tahapan_id " +
                "where pnw.id = ?";
        UsulanJadwal usulanJadwal = Query.find(query,UsulanJadwal.class, penawaran_id).first();
        Date tglAkhir = usulanJadwal.tanggal_selesai;

        if (tglAkhir != null){
            Date tglAkhirFormat = null;
            try {
                tglAkhirFormat = formatter.parse(formatter.format(tglAkhir));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (dateNowFormat.before(tglAkhirFormat)){
                tglRevisi = format.format(tglAkhirFormat);
            }else {
                tglRevisi = format.format(dateNow);
            }

            return tglRevisi;

        } else {
            tglRevisi = format.format(dateNow);
        }



        /*jadwal pemasukan penawaran akhir -> A
        hari ini -> B
        jadwal batas akhir revisi -> C

        if B <= A maka C >= A
        if B > A maka C >= B*/

        return tglRevisi;
    }

    public String getReadableCreatedDate() {
        return DateTimeUtils.parseToReadableDate(created_date);
    }

    public String getReadableStatus(){
        Map<String,String> statusMaps = statusMaps();
        String result = "";

        for (Object key : statusMaps.keySet()) {
            if(key.toString().equalsIgnoreCase(status)){
                result = statusMaps.get(key);
            }
        }
        return result;
    }

    // TODO HTML related function should not be here, the code style is also bad "Pyramid of Doom".
    public StringBuilder getMakeButtonGroups() {
        try {
            AktifUser aktifUser = BaseController.getAktifUser();
            UsulanPokja usulanPokja = new UsulanPokja();
            List<UsulanPokja> usulanPokjas = usulanPokja.findByPenawaranId(id);
            Map<String, Object> pkomo = new HashMap<String, Object>();
            pkomo.put("id", komoditas_id);
            pkomo.put("penawaran_id", id);

            Map<String, Object> params = new HashMap<String, Object>();
            params.put("id", id);

            String urlVerification = Router.reverse("controllers.prakatalog.penawaranCtr.verification", params).url;
            String urlDetail = Router.reverse("controllers.prakatalog.penawaranCtr.detail", params).url;
            String urlPersetujuan = Router.reverse("controllers.prakatalog.persetujuanCtr.persetujuanProduk", params).url;
            String urlProduk = Router.reverse("controllers.prakatalog.penawaranCtr.daftarProduk", params).url;
            String urlProdukKat = Router.reverse("controllers.prakatalog.penawaranCtr.inputMerekKategori", pkomo).url;
            String urlRevisi = Router.reverse("controllers.prakatalog.penawaranCtr.edit", params).url;
            String urlSelesaiNegosiasi = Router.reverse("controllers.prakatalog.PersetujuanCtr.selesaiNegosiasi", params).url;
            String urlEdit = Router.reverse("controllers.prakatalog.penawaranCtr.edit", params).url;

            Map<String, Object> contractParam = new HashMap<String, Object>();
            contractParam.put("pid", id);
            contractParam.put("type", "penawaran");

            Map<String, Object> contractParamPenyedia = new HashMap<String, Object>();
            contractParamPenyedia.put("pid", id);
            contractParamPenyedia.put("type", "penawaran");
            contractParamPenyedia.put("kategori", "kontrak katalog");

            String urlContract = Router.reverse("controllers.penyedia.KontrakCtr.create", contractParam).url;
            String urlPrintContract = Router.reverse("penyedia.KontrakCtr.cetakKontrak", contractParam).url;
            String urlPrintContractPenyedia = Router.reverse("penyedia.KontrakCtr.listDokumenUpload", contractParamPenyedia).url;
            String urlPrintPenetapanProduk = Router.reverse("penyedia.KontrakCtr.listLampiranKontrak", contractParam).url;
            String urlInputDataKontrak = Router.reverse("penyedia.KontrakCtr.renderInputDataKontrak", contractParam).url;

            Map<String, Object> paramrev = new HashMap<String, Object>();
            paramrev.put("usulan_id", usulan_id);//usulan_id
            String docReviewid = doc_review_id != null ? doc_review_id.toString() : null;
            Boolean belumReview = docReviewid == null;
            String urlReviewUsulan = Router.reverse("controllers.admin.UsulanCtr.reviewUsulan", paramrev).url;

            StringBuilder htmlBuilder = new StringBuilder("<ul class='dropdown-menu' aria-labelledby='dropdownMenu4'>");

            boolean requiredPokja = false;
            Long user_id = 0l;
            for (UsulanPokja up : usulanPokjas) {
                if (up.pokja_id.equals(aktifUser.user_id)) {
                    requiredPokja = true;
                }
                user_id = up.user_id;
            }

            if (aktifUser != null) {
                Usulan usulan = Usulan.findById(usulan_id);
                if (aktifUser.isProvider()) {
                    if (aktifUser.user_id.equals(user_id)) {
                        htmlBuilder.append("<li><a href='").append(urlDetail).append("'>").append(Messages.get("detail.label")).append("</a></li>");

                          if (UsulanService.isRevision(usulan) &&
                                !UsulanService.isAfterPokjaAskedForRevision(this, usulan)
                                  && batas_akhir_revisi != null) {
                                htmlBuilder.append("<li><a href='")
                                    .append(urlRevisi)
                                    .append("'>")
                                    .append(Messages.get("revisi.label") + " " + Messages.get("penawaran.label"))
                                    .append("</a></li>");
                        }

                        if (!status.equals(Penawaran.STATUS_MENUNGGU_VERIFIKASI)) {

                            if (!isDatafeed(komoditas_kategori_id) && !status.equals(Penawaran.STATUS_GAGAL_KUALIFIKASI)) {
                                htmlBuilder.append("<li><a href='").append(urlProduk).append("'>").append(Messages.get("produk.label")).append("</a></li>");
                            } else if (isDatafeed(komoditas_kategori_id)) {
                                htmlBuilder.append("<li><a href='").append(urlProdukKat).append("'>").append(Messages.get("merk.kategori.label")).append("</a></li>");
                            }
                        } else {
                            if (!isDatafeed(komoditas_kategori_id)) {
                                htmlBuilder.append("<li><a href='").append(urlProduk).append("'>").append(Messages.get("produk.label")).append("</a></li>");
                            }
                        }
                        if (status.equals(Penawaran.STATUS_KONTRAK_DIPROSES) || status.equals(Penawaran.STATUS_NEGOSIASI_SELESAI) || status.equals(Penawaran.STATUS_SELESAI)) {
                            htmlBuilder.append("<li><a href='").append(urlPrintContractPenyedia).append("'>").append(Messages.get("cetak_kontrak.label")).append("</a></li>");
//                            htmlBuilder.append("<li><a href='").append(urlInputDataKontrak).append("'>").append(Messages.get("cetak_kontrak.label")).append("</a></li>");
                        }
                        if (UsulanService.isPenawaran(usulan)
                                && batas_akhir_revisi == null) {
                            htmlBuilder.append("<li><a href='")
                                    .append(urlEdit)
                                    .append("'>")
                                    .append(Messages.get("edit.label") + " " + Messages.get("penawaran.label"))
                                    .append("</a></li>");
                        }
                    }
                } else if (requiredPokja || aktifUser.isAdmin() || aktifUser.isAdminLokalSektoral()) {
                    String btnPersetujuanProduk = "class='disabled'";
                    htmlBuilder.append("<li><a href='").append(urlDetail).append("'>").append(Messages.get("detail.label")).append("</a></li>");

                    //if (status.equals(Penawaran.STATUS_MENUNGGU_VERIFIKASI)) {
                    if (!UsulanService.isRevision(usulan)
                            && UsulanService.isAfterPokjaAskedForRevision(this, usulan)) {
                        htmlBuilder.append("<li><a href='").append(urlVerification).append("'>").append(Messages.get("verifikasi.label")).append("</a></li>");
                    } else {
                        if (!isDatafeed(komoditas_kategori_id)) {
                            htmlBuilder.append("<li><a href='").append(urlProduk).append("'>").append(Messages.get("produk.label")).append("</a></li>");
                        }

                        if (isNegotiation()) {
                            if (UsulanService.isNegotiation(usulan)) {
                                btnPersetujuanProduk = "";
                            }
                            htmlBuilder.append("<li><a id = 'selesaiNegosiasi' href='").append(urlSelesaiNegosiasi).append("'>").append(Messages.get("selesai_negosiasi.label")).append("</a></li>");
                        } else if (status.equals(Penawaran.STATUS_KONTRAK_DIPROSES)) {
                            if (aktifUser.isAdmin() || aktifUser.isAdminLokalSektoral()) {
                                htmlBuilder.append("<li><a href='").append(urlPrintContract).append("'>").append(Messages.get("cetak_kontrak.label")).append("</a></li>");
                                if (belumReview) {
                                    //ada acl disini
                                    if (aktifUser.isAuthorized("comUsulanReview")) {
                                        htmlBuilder.append("<li><a href='").append(urlReviewUsulan).append("'>").append(Messages.get("review_usulan.label")).append("</a></li>");
                                    } else {
                                        //memberikan info no akses ACL
                                        htmlBuilder.append("<li><a href='").append("#").append("'>").append(Messages.get("review_usulan.label") + " (Acl)").append("</a></li>");
                                    }
                                } else {
                                    Map<String, Object> param = new HashMap<String, Object>();
                                    param.put("id", usulan_id);
                                    String urlDetailUsulan = Router.reverse("controllers.admin.UsulanCtr.detail", param).url;
                                    htmlBuilder.append("<li><a href='").append(urlDetailUsulan).append("'>").append(Messages.get("detail_review_usulan.label")).append("</a></li>");
                                    htmlBuilder.append("<li><a href='").append(urlContract).append("'>").append(Messages.get("unggah.label") + " " + Messages.get("kontrak.label")).append("</a></li>");
                                }
                            }

                        } else if ((status.equals(Penawaran.STATUS_NEGOSIASI_SELESAI) || status.equals(Penawaran.STATUS_PERSIAPAN_KONTRAK)) && (aktifUser.isAdmin() || aktifUser.isAdminLokalSektoral())) {
                            if (belumReview) {
                                if (aktifUser.isAuthorized("comUsulanReview")) {
                                    htmlBuilder.append("<li><a href='").append(urlReviewUsulan).append("'>").append(Messages.get("review_usulan.label")).append("</a></li>");
                                } else {
                                    //memberikan info no akses ACL
                                    htmlBuilder.append("<li><a href='").append("#").append("'>").append(Messages.get("review_usulan.label") + " (Acl)").append("</a></li>");
                                }
                            } else {
                                Map<String, Object> param = new HashMap<String, Object>();
                                param.put("id", usulan_id);
                                String urlDetailUsulan = Router.reverse("controllers.admin.UsulanCtr.detail", param).url;
                                htmlBuilder.append("<li><a href='").append(urlDetailUsulan).append("'>").append(Messages.get("detail_review_usulan.label")).append("</a></li>");
                                htmlBuilder.append("<li><a href='").append(urlPrintContract).append("'>").append(Messages.get("cetak_kontrak.label")).append("</a></li>");
                            }

                        } else if (status.equals(Penawaran.STATUS_SELESAI)) {
                            if (aktifUser.isAdmin() || aktifUser.isAdminLokalSektoral()) {
                                htmlBuilder.append("<li><a href='").append(urlPrintPenetapanProduk).append("'>").append(Messages.get("lampiran_kontrak.label")).append("</a></li>");
                            }
                        }

                    }
                    htmlBuilder.append("<li><a href='").append(urlPersetujuan).append("' ").append(btnPersetujuanProduk).append(">").append(Messages.get("persetujuan_produk.label")).append("</a></li>");
                }
            }

            htmlBuilder.append("</ul>");
            return htmlBuilder;
        } catch (Exception e) {
            Logger.error("Error penawarantatatableservice buttongroup:", e);
            e.printStackTrace();
            throw new UnsupportedOperationException(e);
        }
    }

    public boolean isDatafeed(long komoditas_kategori_id){
        return (komoditas_kategori_id == KOMODITAS_DATAFEED);
    }

    public static List<Penawaran> findPenawaranByProduk(String idp){
        String sql = "SELECT pw.*, us.nama_usulan " +
                    "FROM produk p " +
                    "LEFT JOIN penawaran pw ON p.penawaran_id = pw.id " +
                    "LEFT JOIN usulan us ON pw.usulan_id = us.id " +
                    "WHERE p.id IN ("+idp+") GROUP BY p.penawaran_id";
        return Query.find(sql, Penawaran.class).fetch();
    }

    public static void updateKontrak(Long kontrak_id, Long penawaran_id){
        String query = "UPDATE penawaran set kontrak_id = ? WHERE id=?";
        Query.update(query,kontrak_id, penawaran_id );
    }

    public Boolean isNegotiation() {
        return !TextUtils.isEmpty(status) && (status.equals(Penawaran.STATUS_NEGOSIASI) || status.equals(Penawaran.STATUS_LOLOS_KUALIFIKASI));
    }

    public static Penawaran checkNegotation(Long id){
        String query = "SELECT * FROM penawaran where id='" + id + "'";
        return Query.find(query, Penawaran.class).first();
    }

    public static Boolean isPenawaranNegotation(Long id) {
        Penawaran penawaran = checkNegotation(id);
        if (penawaran.status.equals(Penawaran.STATUS_REVISI)) {
            return true;
        }
        return false;
    }

    public static List<Penawaran> infoKomoditasPenyedia(Long penyedia_id){
        String query = "select distinct k.nama_komoditas " +
                "from penawaran pw " +
                "left join komoditas k on pw.komoditas_id = k.id " +
                "where pw.penyedia_id = ? and pw.kontrak_id != '' and pw.kontrak_id is not null order by k.nama_komoditas asc";

        return Query.find(query,Penawaran.class, penyedia_id).fetch();
    }

    public static Boolean isMenungguVerifikasiOrLolosVerifikasiOrNegosiasi(Penawaran penawaran) {
        return !TextUtils.isEmpty(penawaran.status)
                && (penawaran.status.equals(Penawaran.STATUS_MENUNGGU_VERIFIKASI)
                || penawaran.status.equals(Penawaran.STATUS_LOLOS_KUALIFIKASI)
                || penawaran.status.equals(Penawaran.STATUS_NEGOSIASI)
        );
    }
}
