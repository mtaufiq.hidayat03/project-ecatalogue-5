package repositories;

import models.api.produk.HistoryDatafeed;
import models.product.HistoryDatafeedSearch;
import models.product.search.ProductSearchResult;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.util.TextUtils;
import play.db.jdbc.Query;
import repositories.produk.ProductRepository;

public class DatafeedRepository {

    public static ProductSearchResult searchDatafeed(HistoryDatafeedSearch search) {
        ProductSearchResult model = new ProductSearchResult(search);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * from history_datafeed hd ");
        sb.append(" WHERE hd.penyedia_id is not null ");

        if (search.komoditas > 0) {
            sb.append(" AND hd.komoditas_id = ").append(search.komoditas);
        }

        if(!TextUtils.isEmpty(search.penyedia)){
            sb.append(" AND hd.penyedia_id = ").append(search.penyedia);
        }
//
        if (!TextUtils.isEmpty(search.tanggal)) {
            sb.append(" AND hd.start_time LIKE '").append(StringEscapeUtils.escapeSql(search.tanggal)).append("%'");
        }

        sb.append(" ORDER BY hd.id DESC ");
        Long total = DatafeedRepository.generateTotal(sb.toString());
        model.setTotal(total);
        if (!model.isEmpty()) {
            sb.append(" LIMIT ").append(model.getMax()).append(" OFFSET ").append(model.getFrom());
            model.items = Query.find(sb.toString(), HistoryDatafeed.class).fetch();
        }
        return model;
    }
//        sb.append("SELECT DISTINCT p.id, " +
//                "p.produk_gambar_file_sub_location, " +
//                "p.produk_gambar_file_name, " +
//                "(case when (concat(p.produk_gambar_file_sub_location,p.produk_gambar_file_name) is null) then concat(pg.file_sub_location,pg.file_name) else concat(p.produk_gambar_file_sub_location,p.produk_gambar_file_name) end) as file_url, " +
//                "m.nama_manufaktur, " +
//                "pen.nama_penyedia, " +
//                "p.no_produk_penyedia, " +
//                "p.minta_disetujui, " +
//                "p.apakah_dapat_dibeli, " +
//                "p.apakah_ditayangkan, " +
//                "p.apakah_dapat_diedit, " +
//                "p.status, " +
//                "p.komoditas_id, " +
//                "p.no_produk, " +
//                "p.setuju_tolak, " +
//                "p.jumlah_stok, " +
//                "p.jumlah_stok_inden, " +
//                "pk.nama_kategori, " +
//                "p.nama_produk, " +
//                "pnw.status as penawaran_status, " +
//                "k.kelas_harga, "+
//                "( CASE WHEN (p.active = 1 AND p.apakah_ditayangkan = 1 AND k.active = 1 AND p.setuju_tolak = 'setuju' " +
//                "AND (( p.berlaku_sampai >= CURDATE( ) AND pnyk.tgl_masa_berlaku_mulai <= curdate( ) AND pnyk.tgl_masa_berlaku_selesai >= curdate( ) AND pnyk.active = 1 ) " +
//                " OR " +
//                " (p.berlaku_sampai IS NULL AND pnyk.tgl_masa_berlaku_mulai <= curdate( ) AND pnyk.tgl_masa_berlaku_selesai >= curdate( ) AND pnyk.active = 1 ) ) " +
//                "AND ( k.apakah_stok_unlimited = 1 OR ( k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0 ) )) THEN 'Tayang' ELSE 'Tidak Tayang' END ) as sudah_tayang ")
//                .append("FROM produk p ")
//                .append("LEFT JOIN penyedia pen on p.penyedia_id = pen.id ")
//                .append("LEFT JOIN komoditas k on k.id = p.komoditas_id ")
//                .append("LEFT JOIN penawaran pnw on p.penawaran_id = pnw.id  ")
//                .append("LEFT JOIN manufaktur m on m.id = p.manufaktur_id ")
//                .append("LEFT JOIN produk_kategori pk on pk.id = p.produk_kategori_id ")
//                .append("LEFT JOIN produk_gambar pg on pg.produk_id = p.id and pg.active=1 and pg.produk_id > 0 ")
//                .append("LEFT JOIN penyedia_kontrak pnyk on pnyk.id = pnw.kontrak_id ");
//
//        sb.append(" WHERE p.active = 1 ");
//
//        if (search.komoditas > 0) {
//            sb.append(" AND p.komoditas_id = ").append(search.komoditas);
//        }
//
//        if(!TextUtils.isEmpty(search.penyedia)){
//            sb.append(" AND p.penyedia_id = ").append(search.penyedia);
//        }


//        sb.append(" GROUP BY p.id ORDER BY p.id DESC ");
//        Long total = DatafeedRepository.generateTotal(sb.toString());
//        model.setTotal(total);
//
//        if (!model.isEmpty()) {
//            sb.append(" LIMIT ").append(model.getMax()).append(" OFFSET ").append(model.getFrom());
//            model.items = Query.find(sb.toString(), Produk.class).fetch();
//        }
//        return model;
//    }

    private static Long generateTotal(String query) {
        Long total = Query.find("SELECT COUNT(id) FROM (" + query + ") p", Long.class).first();
        return total;
    }

}
