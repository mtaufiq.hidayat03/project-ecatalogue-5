package models.user;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;
import java.util.Set;
import java.util.stream.*;

@Table(name="user_role_item")
public class UserRoleItem extends BaseKatalogTable {

	@Id
	public Long id;
	public boolean active = true;
	public Long role_item_id;
	public Long user_role_grup_id;

	@Transient
	public String role_item_name;

	public UserRoleItem() {}

	public UserRoleItem(Long roleItemId, Long groupId) {
		this.role_item_id = roleItemId;
		this.user_role_grup_id = groupId;
	}

	public static List<UserRoleItem> findByUserRoleGrupId(long id){
		String query = "select uri.id, uri.role_item_id, uri.user_role_grup_id, ri.nama_role as role_item_name from user_role_item uri " +
				"inner join role_item ri on uri.role_item_id = ri.id where uri.active = 1 and uri.user_role_grup_id = ?";
		return Query.find(query, UserRoleItem.class, id).fetch();
	}

	public static Set<Long> findByUserRoleGrupIdSet(long id){
		List<UserRoleItem> userRoleItems = findByUserRoleGrupId(id);
		return userRoleItems.stream().map(u -> u.role_item_id).collect(
				Collectors.toSet());
	}

	public static void deleteByUserRoleGrupId(long id){
		String query = "update user_role_item set active = ? where user_role_grup_id = ?";
		Query.update(query, NOT_AKTIF, id);
	}

	public void saveRole() {
		final int result = save();
		if (this.id == null) {
			this.id = (long) result;
		}
	}

	public static Integer getTotalItemByUserRoleGrupId(Long id) {
		return Query.find("SELECT COUNT(*) FROM user_role_item " +
				"WHERE user_role_grup_id =? AND active =?", Integer.class, id, AKTIF).first();
	}

}	
