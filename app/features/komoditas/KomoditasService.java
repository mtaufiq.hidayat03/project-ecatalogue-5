package features.komoditas;

import features.komoditas.repositories.KomoditasRepositoryContract;
import models.katalog.Komoditas;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 3/26/2020 1:07 PM
 */
public class KomoditasService {

    private static KomoditasService komoditasService;
    public KomoditasRepositoryContract komoditasRepository = KomoditasRepository.getInstance();

    public static KomoditasService getInstance() {
        if (komoditasService == null) {
            komoditasService = new KomoditasService();
        }
        return komoditasService;
    }

    public Map<String, List<Komoditas>> groupByCategory(List<Komoditas> commodities) {
        Map<String, List<Komoditas>> results = commodities.stream()
                .collect(
                        Collectors.groupingBy(
                                Komoditas::groupCategoryByName,
                                LinkedHashMap::new,
                                Collectors.toList())
                );
        List<Komoditas> umkms = komoditasRepository.getUmkms();
        if (!umkms.isEmpty()) {
            results.put("UKM", umkms);
        }
        return results;
    }

}
