INSERT INTO role_grup (nama_grup, kategori, active, created_date) VALUES
("Tahapan", "komoditas", 1, CURDATE());

INSERT INTO role_item (role_grup_id, nama_role, deskripsi, active, created_date) VALUES
  ((SELECT id FROM role_grup WHERE nama_grup = "Tahapan"), "comTahapan", "Akses Master Data Tahapan", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Tahapan"), "comTahapanAdd", "Akses Tambah Tahapan", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Tahapan"), "comTahapanEdit", "Akses Edit Tahapan", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Tahapan"), "comTahapanDelete", "Akses Hapus Tahapan", 1, CURDATE());
