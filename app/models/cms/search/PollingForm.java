package models.cms.search;

import models.cms.Polling;
import models.cms.PollingPilihan;

import java.util.ArrayList;
import java.util.List;

public class PollingForm {
    public List<Polling> pollingForm = new ArrayList<>();
    public List<PollingPilihan> pollingList = new ArrayList<>();
}
