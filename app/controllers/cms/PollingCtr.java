package controllers.cms;

import controllers.BaseController;
import controllers.security.AllowAccess;
import ext.DatetimeBinder;
import models.cms.Polling;
import models.cms.PollingPilihan;
import static models.cms.PollingPilihan.*;
import models.secman.Acl;
import org.apache.http.util.TextUtils;
import org.json.JSONException;
import org.json.JSONObject;
import play.data.binding.As;
import play.i18n.Messages;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by user on 10/11/2017.
 */
public class PollingCtr extends BaseController {
    @AllowAccess({Acl.COM_POLLING})
    public static void index(){render();}

    @AllowAccess({Acl.COM_POLLING_ADD})
    public static void create(){
        render("cms/PollingCtr/create.html");
    }

    @AllowAccess({Acl.COM_POLLING_ADD})
    public static void createSubmit(String subjek, String deskripsi,
                                    @As(binder = DatetimeBinder.class)Date publish_date_from,
                                    @As(binder = DatetimeBinder.class)Date publish_date_end,
                                    String[] nama_pilihan){
        Polling pol = new Polling();
        pol.subjek = subjek;
        pol.slug = subjek.replaceAll("\\s","0").replaceAll("\\W","").replace("0","-");
        pol.deskripsi_singkat = deskripsi;
        pol.status = "draft";
        pol.terkunci = false;
        pol.apakah_utama = false;
        pol.publish_date_from = publish_date_from;
        pol.publish_date_to = publish_date_end;
        pol.param = "";
        pol.active = true;
        long polId = pol.save();

        if(nama_pilihan != null && nama_pilihan.length > 0) {
            submitPilihan(polId, nama_pilihan);
        }
        flash.success(Messages.get("notif.success.draft"));
        edit(polId);
    }

    @AllowAccess({Acl.COM_POLLING_EDIT})
    public static void edit(long id){
        Polling viewPol = Polling.findById(id);
        String publish_date_from;
        String publish_date_to;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        publish_date_from = dateFormat.format(viewPol.publish_date_from);
        if(viewPol.publish_date_to != null){
            publish_date_to = dateFormat.format(viewPol.publish_date_to);
        }else{
            publish_date_to = null;
        }
        List<PollingPilihan> pilihanList = PollingPilihan.find("polling_id = ?", id).fetch();
        render("cms/PollingCtr/edit.html", viewPol, publish_date_from, publish_date_to, pilihanList);
    }

    @AllowAccess({Acl.COM_POLLING_EDIT})
    public static void editSubmit(Long data_id, String subjek, String status, String deskripsi,
                                  @As(binder = DatetimeBinder.class)Date publish_date_from,
                                  @As(binder = DatetimeBinder.class)Date publish_date_end,
                                  String[] nama_pilihan){
        Polling pol = Polling.findById(data_id);
        pol.subjek = subjek;
        pol.slug = subjek.replaceAll("\\s","0").replaceAll("\\W","").replace("0","-");
        pol.deskripsi_singkat = deskripsi;
        pol.status = status;
        pol.publish_date_from = publish_date_from;
        pol.publish_date_to = publish_date_end;
        pol.active = true;
        pol.save();

        PollingPilihan.delete("polling_id = ?",data_id);
        if(nama_pilihan != null && nama_pilihan.length > 0) {
            submitPilihan(data_id, nama_pilihan);
        }
        flash.success(Messages.get("notif.success.update"));
        edit(data_id);
    }

    @AllowAccess({Acl.COM_POLLING_EDIT})
    public static void update(long id, String param){
        Polling pol = Polling.findById(id);
        String msg = "";
        long value = 1;
        switch (param){
            case "terkunci":
                value = (pol.terkunci?1:0);
                msg = pol.terkunci ? Messages.get("notif.cms.polling.unlocked") : Messages.get("notif.cms.polling.locked");
                break;
            case "apakah_utama":
                value = (pol.apakah_utama?1:0);
                msg = pol.apakah_utama ? Messages.get("notif.cms.polling.main.unactive") : Messages.get("notif.cms.polling.main.active");
                break;
            case "active":
                value = (pol.active?1:0);
                break;
        }
        value = (value==1?0:1);
        Polling.updateByParam(id,param,value);
        flash.success(msg);
        index();
    }

    @AllowAccess({Acl.COM_POLLING_DELETE})
    public static void delete(long id){
        Polling.deleteById(id);
        flash.success(Messages.get("notif.success.delete"));
        index();
    }

    private static void submitPilihan(long id, String[] nama_pilihan){
        for (int i = 0; i < nama_pilihan.length; i++){
            if(!TextUtils.isEmpty(nama_pilihan[i])){
                PollingPilihan pilihan = new PollingPilihan();
                pilihan.polling_id = id;
                pilihan.pilihan_jawaban = nama_pilihan[i];
                pilihan.posisi_pilihan = Long.valueOf(i) + 1;
                pilihan.active = true;
                pilihan.save();
            }
        }
    }

    public static void viewPolling(Long polId) throws JSONException {
        JSONObject obj = new JSONObject();
        List<String> listLabel = new ArrayList<>();
        List<Long> listValue = new ArrayList<>();

        List<PollingPilihan> pollingPilihanList = viewPollingResult(polId);
        for(int i = 0; i < pollingPilihanList.size(); i++){
            listLabel.add(pollingPilihanList.get(i).pilihan_jawaban);
            listValue.add(pollingPilihanList.get(i).pilihan_hasil);
        }
        obj.put("label", listLabel);
        obj.put("value", listValue);
        renderJSON(obj.toString());
    }
}
