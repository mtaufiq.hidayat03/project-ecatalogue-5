INSERT into user_role_item (user_role_grup_id, role_item_id, active, created_date, created_by, modified_date, modified_by)
    VALUES
      (1, (select id from role_item where nama_role = "comDocumentTemplate"), 1, curdate(), -99, curdate(), -99),
      (1, (select id from role_item where nama_role = "comDocumentTemplateAdd"), 1, curdate(), -99, curdate(), -99),
      (1, (select id from role_item where nama_role = "comDocumentTemplateEdit"), 1, curdate(), -99, curdate(), -99),
      (1, (select id from role_item where nama_role = "comDocumentTemplateDelete"), 1, curdate(), -99, curdate(), -99);
