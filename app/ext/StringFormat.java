package ext;

import java.text.NumberFormat;

import play.templates.JavaExtensions;

/**
 * Created by rayhanfrds on 9/19/17.
 */

public class StringFormat extends JavaExtensions {

	/**
	 * Fungsi {@code rupiah} merupakan ekstensi template Play!Framework untuk format
	 * nilai ke dalam format rupiah. Fungsinya dapat digunakan sebagai
	 * pengganti fungsi {@link FormatUtils#formatCurrencyRupiah(Number) formatCurrencyRupiah}
	 * Cara penggunaan di Template : ${some_variable_type_double.rupiah()}
	 *
	 * @param harga nilai yang diformat
	 * @return string terformat
	 */
	public static String rupiah(Double harga) {
		return FormatUtils.formatCurrencyRupiah(harga);
	}

	/**
	 * Fungsi {@code formatCurrency} digunakan untuk format angka kedalam bentuk currency
	 * @param number nilai yang diformat
	 * @return nilai {@code number} yang sudah terformat
	 */
	public static String formatCurrency(Number number) {
		String hasil = "";
		if (number != null) {
			NumberFormat format = NumberFormat.getInstance();
			hasil = format.format(number);
		}
		return hasil;
	}
	
}
