package models.katalog;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.ArrayList;
import java.util.List;

@Table(name ="produk_diskusi_balas")

public class ProdukDiskusiBalas extends BaseKatalogTable {
    @Id
    public Long id;
    public Long produk_id;
    public Long produk_diskusi_id;
    public Long user_id;
    public String user_nama;
    public String user_foto;
    public String diskusi_balas;
    public Boolean active;

    public static List<ProdukDiskusiBalas> findProdukDiskusiBalasByIdProduk(Long produk_id){
        List<ProdukDiskusiBalas> list = new ArrayList<>();
        try{
            list = ProdukDiskusiBalas.find("produk_id = ?", produk_id).fetch();
        }catch (Exception e){}
        return list;
    }
}
