package models.api;

import models.blacklist.Pelanggaran;
import models.jcommon.util.CommonUtil;
import play.Logger;
import utils.KatalogUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by reza on 27/08/18
 */
public class DceDetailPelanggaran {
	public String jenis_pelanggaran;
	public String deskripsi;
	public String dibuat;
}
