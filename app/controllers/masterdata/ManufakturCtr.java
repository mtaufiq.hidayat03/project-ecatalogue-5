package controllers.masterdata;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.jcommon.util.CommonUtil;
import models.katalog.Komoditas;
import models.katalog.komoditasmodel.KomoditasManufaktur;
import models.masterdata.Manufaktur;
import models.secman.Acl;
import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.db.jdbc.Query;
import play.i18n.Messages;
import services.produk.ProdukService;

import static models.katalog.Komoditas.*;

public class ManufakturCtr extends BaseController {

    @AllowAccess({Acl.COM_MANUFAKTUR})
    public static void index(){
        List<Komoditas> komoditasList = findAllData();
        render(komoditasList);
    }

    @AllowAccess({Acl.COM_MANUFAKTUR_ADD})
    public static void create(){
        List<Komoditas> komoditasList = findAllActive();

        render("masterdata/ManufakturCtr/create.html", komoditasList);
    }

    @AllowAccess({Acl.COM_MANUFAKTUR_DELETE})
    public static void delete(long id) {
        Manufaktur model = Manufaktur.findById(id);
        notFoundIfNull(model);
        if (ProdukService.getTotalProductByManufacture(model.id) == 0) {
            model.softDelete();
            flash.success(Messages.get("notif.success.delete"));
        } else {
            flash.error(Messages.get("notif.failed.delete.relation"));
        }
        index();
    }

    @AllowAccess({Acl.COM_MANUFAKTUR_ADD})
    public static void createSubmit(Komoditas komoditas, String[] nama_manufaktur_arr) {
        checkAuthenticity();

        if(nama_manufaktur_arr != null){

            //validasi manufaktur
            boolean validate = validateManufaktur(nama_manufaktur_arr);
            boolean coba = StringUtils.isAlphanumeric("tes");
            boolean coba2 = StringUtils.isAlphanumeric("@#$%^&");

            //edit by sudi, manufaktur validasinya dilepas karna kata mas priyo boleh menggunakan karkater lain selain alphanumeric
            // sebab produk2 tersebut masih berada dibawah pengawan LKPP
//            for (String s : nama_manufaktur_arr) {
//                if (!StringUtils.isAlphanumeric(s)) {
//                    flash.error(Messages.get("Nama Manufaktur hanya boleh berisi alphanumeric."));
//                    List<String> manufakturList = Arrays.asList(nama_manufaktur_arr);
//                    List<Komoditas> komoditasList = findAllActive();
//                    render("masterdata/ManufakturCtr/create.html", manufakturList, komoditas, komoditasList);
//                }
//            }

            validate = komoditas.id != null ? validate : false;

            if(!validate){

                if (validation.hasErrors()) {

                    flash.error(Messages.get("Gagal Simpan Manufaktur, periksa kembali inputan anda."));

                    if(komoditas.id == null)
                        validation.addError("komoditas", "Komoditas harus di pilih.", "var");
                        flash.error(Messages.get("Gagal Simpan Manufaktur, periksa kembali inputan anda."));

                }

                List<String> manufakturList = Arrays.asList(nama_manufaktur_arr);

                List<Komoditas> komoditasList = findAllActive();

                render("masterdata/ManufakturCtr/create.html", manufakturList, komoditas, komoditasList);

            }

            for(int i = 0; i < nama_manufaktur_arr.length; i++){
                Manufaktur manufaktur = new Manufaktur();
                manufaktur.nama_manufaktur = nama_manufaktur_arr[i];
                manufaktur.active = true;

                long manufakturId = manufaktur.save();

                KomoditasManufaktur komoditasManufaktur = new KomoditasManufaktur();
                komoditasManufaktur.manufaktur_id = manufakturId;
                komoditasManufaktur.komoditas_id = komoditas.id;
                komoditasManufaktur.active = true;
                komoditasManufaktur.save();
            }

            flash.success("Data berhasil disimpan");
            index();
        }

    }

    @AllowAccess({Acl.COM_MANUFAKTUR_EDIT})
    public static void editSubmit(long id_komoditas_manufaktur, long id_manufaktur, long id_komoditas, String nama_manufaktur) {
        checkAuthenticity();
        if (CommonUtil.isEmpty(nama_manufaktur)) {
            flash.error("Gagal Ubah manufaktur, nama manufaktur tidak boleh kosong");
        } else{
            Manufaktur manufaktur = Manufaktur.findById(id_manufaktur);
            manufaktur.nama_manufaktur = nama_manufaktur;
            manufaktur.save();

            KomoditasManufaktur komoditasManufaktur = KomoditasManufaktur.findById(id_komoditas_manufaktur);
            komoditasManufaktur.komoditas_id = id_komoditas;
            komoditasManufaktur.save();
        }

        flash.success("Data berhasil disimpan");
        index();
    }

    @AllowAccess({Acl.COM_MANUFAKTUR, Acl.COM_MANUFAKTUR_ADD, Acl.COM_MANUFAKTUR_EDIT})
    public static void importExcel(){
        List<Komoditas> komoditasList = findAllActive();

        render("masterdata/ManufakturCtr/importExcel.html", komoditasList);
    }

    @AllowAccess({Acl.COM_MANUFAKTUR, Acl.COM_MANUFAKTUR_ADD, Acl.COM_MANUFAKTUR_EDIT})
    public static void importSubmit(Komoditas komoditas, String nama_manufaktur_arr){
        checkAuthenticity();
        JsonArray results = new Gson().fromJson(nama_manufaktur_arr, JsonArray.class);

        if(results != null){
            for(int i = 0; i < results.size(); i++){
                JsonObject obj = results.get(i).getAsJsonObject();

                Manufaktur manufaktur = new Manufaktur();
                manufaktur.nama_manufaktur = obj.get("Nama Manufaktur").getAsString();
                manufaktur.active = true;

                long manufakturId = manufaktur.save();

                KomoditasManufaktur komoditasManufaktur = new KomoditasManufaktur();
                komoditasManufaktur.manufaktur_id = manufakturId;
                komoditasManufaktur.komoditas_id = komoditas.id;
                komoditasManufaktur.active = true;
                komoditasManufaktur.save();
            }

            flash.success("Data berhasil disimpan");
            index();
        }

        flash.success("Data berhasil disimpan");
        index();
    }

    private static boolean validateManufaktur(String[] nama_manufaktur_arr){

        boolean validate = true;

        for(int i = 0; i < nama_manufaktur_arr.length; i++){

            if(CommonUtil.isEmpty(nama_manufaktur_arr[i])) {

                validate = false;

                validation.addError("manufaktur_"+(i+1), "Nama Manufaktur Harus Diisi", "var");

            }

        }

        return validate;

    }

    @AllowAccess({Acl.COM_MANUFAKTUR_EDIT})
    public static void setAktif(long id) {
//        Manufaktur manufaktur = Manufaktur.findById(id);
//        notFoundIfNull(manufaktur);
//        manufaktur.setAktif();
        KomoditasManufaktur komoditasManufaktur = KomoditasManufaktur.findById(id);
        notFoundIfNull(komoditasManufaktur);
        komoditasManufaktur.active = true;
        komoditasManufaktur.save();
        flash.success(Messages.get("record_update_aktifnonaktif_relation_success"));
        index();
    }

    @AllowAccess({Acl.COM_MANUFAKTUR_EDIT})
    public static void setNonAktif(long id) {
//        Manufaktur manufaktur = Manufaktur.findById(id);
//        notFoundIfNull(manufaktur);
//        if(!manufakturIsInUse(id)){
//            manufaktur.setNonAktif();
//            flash.success(Messages.get("record_update_aktifnonaktif_relation_success"));
//        }else{
//            flash.error(Messages.get("record_update_aktifnonaktif_relation_failed"));
//        }
        KomoditasManufaktur komoditasManufaktur = KomoditasManufaktur.findById(id);
        notFoundIfNull(komoditasManufaktur);
        komoditasManufaktur.active = false;
        komoditasManufaktur.save();
        flash.success(Messages.get("record_update_aktifnonaktif_relation_success"));
        index();
    }

    private static boolean manufakturIsInUse(long id) {
//        final KomoditasManufaktur km = KomoditasManufaktur.
//             find("komoditas_id=? and active=?", id, 1).first();
        final KomoditasManufaktur km1 = Query.find("select km.* from komoditas_manufaktur km"
             + " join manufaktur m on km.manufaktur_id=m.id where (km.active=1 or m.active=1) and km.komoditas_id=?",
             KomoditasManufaktur.class, id).first();
        Logger.debug("id = %s", id);
        Logger.debug("km1 = %s", km1);
        Logger.debug("km1.active = %s", km1!=null ? km1.active:null);
        Logger.debug("km1.id = %s", km1!=null ? km1.komoditas_id.toString():null);
		return km1 != null;
    }

}
