package models.katalog;

import models.BaseKatalogTable;
import models.masterdata.Provinsi;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by dadang on 8/27/17.
 */

@Table(name = "produk_wilayah_jual_provinsi")
public class ProdukWilayahJualProvinsi extends BaseKatalogTable {

	@Id
	public Long id;
	public Long provinsi_id;
	public Long produk_id;
	public BigDecimal harga_utama;
	public BigDecimal harga_ongkir;
	public Date harga_tanggal;
	public Long harga_kurs_id;
	public Double margin_harga;
	public boolean active;
	public boolean approved;

	@Transient
	public Double harga_pemerintah;
	@Transient
	public String nama_lokasi;
	@Transient
	public String nama_produk;
	@Transient
	public String nama_manufaktur;
	@Transient
	public String nama_unit_pengukuran;
	@Transient
	public String nama_provinsi;

	public static List<Provinsi> findWilayahJual(Long produk_id){
		String query = "select p.* from produk_wilayah_jual_provinsi pwj join provinsi p on p.id = pwj.provinsi_id " +
				"where pwj.produk_id = ?";

		return Query.find(query, Provinsi.class, produk_id).fetch();
	}

	public static ProdukWilayahJualProvinsi getHargaByProvinsiId(Long produk_id, Long provinsi_id){

		return find("produk_id = ? AND provinsi_id = ? AND active = 1", produk_id, provinsi_id).first();
	}

	public double getMainPrice() {
		if (this.harga_utama != null) {
			return this.harga_utama.doubleValue();
		}
		return 0;
	}

	public static List<ProdukWilayahJualProvinsi> findProdukByPenawaran(long penawaran_id){
		String query = "select " +
				"p.id" +
				",p.komoditas_id" +
				",k.nama_komoditas" +
				",p.produk_kategori_id" +
				",pk.nama_kategori" +
				",p.penyedia_id" +
				",py.nama_penyedia" +
				",p.manufaktur_id" +
				",m.nama_manufaktur" +
				",p.unspsc_id" +
				",p.unit_pengukuran_id" +
				",up.nama_unit_pengukuran" +
				",p.no_produk" +
				",p.no_produk_penyedia" +
				",p.nama_produk" +
				",p.jenis_produk" +
				",p.setuju_tolak" +
				",p.setuju_tolak_tanggal" +
				",p.setuju_tolak_oleh" +
				",p.setuju_tolak_alasan" +
				",pwjp.provinsi_id" +
				",prov.nama_provinsi" +
				",pwjp.harga_utama" +
				",pwjp.harga_ongkir" +
				",pwjp.harga_tanggal" +
				",pwjp.harga_kurs_id" +
				",pwjp.margin_harga" +
				",p.berlaku_sampai" +
				",p.kontrak_id" +
				",p.ongkir " +
				"from produk_wilayah_jual_provinsi pwjp " +
				"left join produk p on pwjp.produk_id = p.id " +
				"left join provinsi prov on pwjp.provinsi_id = prov.id " +
				"left join penyedia py on p.penyedia_id = py.id " +
				"left join komoditas k on p.komoditas_id = k.id " +
				"left join produk_kategori pk on p.produk_kategori_id = pk.id " +
				"left join manufaktur m on p.manufaktur_id = m.id " +
				"left join unit_pengukuran up on p.unit_pengukuran_id = up.id " +
				"where pwjp.active = " + AKTIF + " and p.active = " + AKTIF + " and p.status in ('" + Produk.STATUS_TERIMA + "','" + Produk.STATUS_TAYANG + "') " +
				"and penawaran_id = ?";

		return Query.find(query, ProdukWilayahJualProvinsi.class, penawaran_id).fetch();
	}

}
