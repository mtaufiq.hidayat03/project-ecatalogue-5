package jobs.JobTrail.viewmodels;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ResultFeedBackEs {
    public long took = 0;
    public boolean errors = false;
    public List<ItemResultEs> items = new ArrayList<>();
    public List<ItemResultEs> getfailedItems(){
        List<ItemResultEs> result = new ArrayList<>();
        try{
            result = items.stream().filter(f-> f.create.status != 201).collect(Collectors.toList());
        }catch (Exception e){}
        return result;
    }
    public String getIdsFailedIds(){
        String result = "";
        try{
            List<ItemResultEs> failed  = getfailedItems();
            if(null != getfailedItems()){
                result = failed.stream().map(ItemResultEs::getId).collect(Collectors.joining(","));
            }
        }catch (Exception e){}
        return result;
    }
}
