package repositories.paket;

import models.common.AktifUser;
import models.purchasing.penerimaan.LampiranPenerimaan;
import models.purchasing.penerimaan.ProdukPenerimaan;
import models.purchasing.penerimaan.PaketRiwayatPenerimaan;
import models.purchasing.penerimaan.TotalPenerimaan;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.db.jdbc.Query;
import utils.DateTimeUtils;
import utils.KatalogUtils;
import utils.LogUtil;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static constants.paket.PaketPenerimaanConstant.*;

/**
 * @author HanusaCloud on 10/23/2017
 */
public class AcceptanceRepository {

    public static final String TAG = "AcceptanceRepository";

    public static List<PaketRiwayatPenerimaan> getHistoriesByPackageId(Long packageId) {
        final String query = "SELECT pp.id, " +
                "pp.paket_id, " +
                "pp.no_dokumen, " +
                "pp.deskripsi, " +
                "pp.tanggal_dokumen, " +
                "pp.tanggal_terima, " +
                "pp.paket_pengiriman_no_dokumen_sistem, " +
                "ppl.original_file_name, " +
                "ppl.file_sub_location, " +
                "ppl.file_name, " +
                "ppl.id as file_id " +
                "FROM paket_penerimaan pp " +
                "LEFT JOIN paket_penerimaan_lampiran ppl " +
                "ON ppl.paket_penerimaan_id = pp.id AND ppl.active > 0 " +
                "WHERE pp.paket_id =? AND pp.active > 0 " +
                "GROUP BY pp.id " +
                "ORDER BY ppl.posisi_file DESC";
        Logger.debug("acceptance repo: " + query);
        List<PaketRiwayatPenerimaan> results = Query.find(query, PaketRiwayatPenerimaan.class, packageId).fetch();
        if (!results.isEmpty()) {
            Map<Long, LampiranPenerimaan> attachments = getAttachments(packageId);
            for (PaketRiwayatPenerimaan model : results) {
                LampiranPenerimaan attachment = attachments.get(model.id);
                if (attachment != null) {
                    model.latestAttachment = attachment;
                }
            }
        }
        return results;
    }

    public static List<ProdukPenerimaan> getProductsByAcceptanceIdAndPackageId(Long packageId, Long acceptanceId) {
        final String query = "SELECT pp.id, " +
                "pp.deskripsi," +
                "pp.kuantitas, " +
                "ppr.kuantitas as kuantitas_awal, " +
                "ppr.catatan, " +
                "p.nama_produk, " +
                "p.id as produk_id, " +
                "p.no_produk, " +
                "p.produk_kategori_id, " +
                "up.nama_unit_pengukuran " +
                "FROM `paket_penerimaan_produk` pp " +
                "JOIN paket_produk ppr " +
                "ON ppr.id = pp.paket_produk_id AND ppr.paket_id = pp.paket_id " +
                "JOIN produk p " +
                "ON p.id = pp.produk_id " +
                "JOIN unit_pengukuran up " +
                "ON up.id = p.unit_pengukuran_id " +
                "WHERE pp.paket_id =? " +
                "AND pp.paket_penerimaan_id =? " +
                "AND pp.active > 0;";
        Logger.debug(query);
        return Query.find(query, ProdukPenerimaan.class, packageId, acceptanceId).fetch();
    }

    public static List<ProdukPenerimaan> getProductsToBeAccepted(Long paket_id) {
        final String query = "SELECT " +
                "pp.kuantitas as kuantitas_awal, " +
                "pp.catatan, " +
                "p.nama_produk, " +
                "p.id as produk_id, " +
                "p.unit_pengukuran_id, " +
                "p.no_produk, " +
                "up.nama_unit_pengukuran, " +
                "ppr.delivered, " +
                "(pp.kuantitas - COALESCE(SUM(ppr.delivered), 0)) as leftovers, " +
                "ppr.id " +
                "FROM paket_produk pp " +
                "JOIN produk p " +
                "ON p.id = pp.produk_id " +
                "LEFT JOIN (SELECT id, produk_id, SUM(kuantitas) as delivered " +
                "FROM paket_penerimaan_produk " +
                "WHERE paket_id =? AND active > 0 " +
                "GROUP BY produk_id) ppr " +
                "ON ppr.produk_id = pp.produk_id " +
                "JOIN unit_pengukuran up " +
                "ON up.id = p.unit_pengukuran_id " +
                "WHERE pp.paket_id =? " +
                "HAVING leftovers > 0 ";
        Logger.debug(query);
        return Query.find(query, ProdukPenerimaan.class, paket_id, paket_id).fetch();
    }

    public static TotalPenerimaan getCurrentTotalDelivered(Long paketId) {
        final String query = "SELECT COALESCE(SUM(pp.kuantitas), 0) as total_requested" +
                ", COALESCE(SUM(ppr.delivered), 0) as total_delivered " +
                "FROM paket_produk pp " +
                "JOIN produk p ON p.id = pp.produk_id " +
                "LEFT JOIN (SELECT id, produk_id, SUM(kuantitas) as delivered " +
                "FROM paket_penerimaan_produk " +
                "WHERE paket_id =? AND active > 0 GROUP BY produk_id) ppr " +
                "ON ppr.produk_id = pp.produk_id " +
                "JOIN unit_pengukuran up " +
                "ON up.id = p.unit_pengukuran_id " +
                "WHERE pp.paket_id =?";
        Logger.debug(query);
        return Query.find(query, TotalPenerimaan.class, paketId, paketId).first();
    }

    public static Map<Long, LampiranPenerimaan> getAttachments(Long packageId) {
        final String query = "SELECT id, paket_penerimaan_id, file_name, original_file_name, file_sub_location " +
                "FROM paket_penerimaan_lampiran " +
                "where paket_id = ? AND active > 0 " +
                "GROUP BY paket_penerimaan_id " +
                "ORDER BY posisi_file DESC;";
        List<LampiranPenerimaan> attachments = Query.find(query, LampiranPenerimaan.class, packageId).fetch();
        return attachments.stream().collect(Collectors.toMap(LampiranPenerimaan::getPenerimaanId, p -> p));
    }

    public static void deactivateProducts(Long acceptanceId) {
        Query.update("UPDATE paket_penerimaan_produk" +
                " SET active = 0" +
                ", deleted_by = '" + AktifUser.getActiveUserId() + "'" +
                ", deleted_date = '" + DateTimeUtils.getStandardDateTime() + "'" +
                " WHERE paket_penerimaan_id =?", acceptanceId);
    }

    public static void deactivateAttachments(Long acceptanceId) {
        Query.update("UPDATE paket_penerimaan_lampiran" +
                " SET active = 0" +
                ", deleted_by = '" + AktifUser.getActiveUserId() + "'" +
                ", deleted_date = '" + DateTimeUtils.getStandardDateTime() + "'" +
                " WHERE paket_penerimaan_id =?", acceptanceId);
    }

    public static List<LampiranPenerimaan> getAcceptanceAttachments(Long packageId, Long acceptanceId) {
        final String query = "SELECT * FROM produk_penerimaan_lampiran WHERE paket_id =? AND paket_penerimaan_id =?";
        return Query.find(query, LampiranPenerimaan.class, packageId, acceptanceId).fetch();
    }

    public static LampiranPenerimaan getLatestAttachment(Long packageId, Long acceptance) {
        final String query = "SELECT id" +
                ", paket_penerimaan_id" +
                ", paket_id, file_name" +
                ", original_file_name" +
                ", file_sub_location" +
                " FROM paket_penerimaan_lampiran" +
                " WHERE paket_id =? AND paket_penerimaan_id =? AND active > 0" +
                " ORDER BY posisi_file DESC LIMIT 1";
        return Query.find(query, LampiranPenerimaan.class, packageId, acceptance).first();
    }

    public static List<ProdukPenerimaan> searchProductsToBeAccepted(Long paket_id, String keyword, String except) {
        StringBuilder sb = new StringBuilder("SELECT " +
                "pp.kuantitas as kuantitas_awal, " +
                "pp.catatan, " +
                "p.nama_produk, " +
                "p.id as produk_id, " +
                "p.unit_pengukuran_id, " +
                "p.no_produk, " +
                "up.nama_unit_pengukuran, " +
                "ppr.delivered, " +
                "(pp.kuantitas - COALESCE(SUM(ppr.delivered), 0)) as leftovers, " +
                "ppr.id " +
                "FROM paket_produk pp " +
                "JOIN produk p " +
                "ON p.id = pp.produk_id " +
                "LEFT JOIN (SELECT id, produk_id, SUM(kuantitas) as delivered " +
                "FROM paket_penerimaan_produk " +
                "WHERE paket_id =? AND active > 0 " +
                "GROUP BY produk_id) ppr " +
                "ON ppr.produk_id = pp.produk_id " +
                "JOIN unit_pengukuran up " +
                "ON up.id = p.unit_pengukuran_id " +
                "WHERE pp.paket_id =? ");
        if (!TextUtils.isEmpty(except)) {
            sb.append("AND p.id NOT IN (");
            sb.append(except);
            sb.append(")");
        }
        if (!TextUtils.isEmpty(keyword)) {
            sb.append("AND p.nama_produk LIKE '%")
                    .append(keyword)
                    .append("%' ");
        }
        sb.append("HAVING leftovers > 0 ");
        if (!TextUtils.isEmpty(keyword)) {
            sb.append("LIMIT 10");
        }
        Logger.debug(sb.toString());
        return Query.find(sb.toString(), ProdukPenerimaan.class, paket_id, paket_id).fetch();
    }

    /**
     * @return Map id as the key*/
    public static Map<Long, ProdukPenerimaan> getCurrentProductsIdKey(Long packageId, Long acceptanceId) {
        final String query = "SELECT *" +
                " FROM paket_penerimaan_produk" +
                " WHERE paket_id =? AND paket_penerimaan_id =? AND active > 0;";
        List<ProdukPenerimaan> attachments = Query.find(query, ProdukPenerimaan.class, packageId, acceptanceId).fetch();
        return attachments.stream().collect(Collectors.toMap(ProdukPenerimaan::getId, p -> p));
    }

    public static void deactivateAllProducts(Long packageId, Long acceptance, String ids) {
        StringBuilder sb = new StringBuilder("UPDATE paket_penerimaan_produk" +
                " SET active = 0" +
                ", deleted_by = '" + AktifUser.getActiveUserId() + "'" +
                ", deleted_date = '" + DateTimeUtils.getStandardDateTime() + "'" +
                " WHERE paket_id =?" +
                " AND paket_penerimaan_id =?");
        if (!TextUtils.isEmpty(ids)) {
            sb.append(" AND produk_id IN (").append(ids).append(")");
        }
        Logger.debug("packageId: " + packageId + ": " + acceptance);
        Logger.debug(sb.toString());
        Query.update(sb.toString(), packageId, acceptance);
    }

    public static Integer getTotalProductFullyAccepted(Long packageId) {
        LogUtil.d(TAG, "get total product fully accepted");
        final String query = "SELECT COUNT(*) as total " +
                "FROM (SELECT ppr.received, " +
                "(pp.kuantitas - COALESCE(SUM(ppr.received), 0)) as leftovers, ppr.id " +
                "FROM paket_produk pp " +
                "JOIN produk p ON p.id = pp.produk_id " +
                "LEFT JOIN (SELECT id, produk_id, paket_produk_id, SUM(kuantitas) as received " +
                "FROM paket_penerimaan_produk " +
                "WHERE paket_id =? AND active > 0 " +
                "GROUP BY paket_produk_id) ppr " +
                "ON ppr.paket_produk_id = pp.id " +
                "JOIN unit_pengukuran up " +
                "ON up.id = p.unit_pengukuran_id " +
                "WHERE pp.paket_id =? " +
                "GROUP BY pp.id " +
                "HAVING leftovers = 0) t;";
        LogUtil.d(TAG, query);
        return Query.count(query, Integer.class, packageId, packageId);
    }

    public static void setPaymentId(Long id, Long packageId, Long paymentId) {
        String value = "";
        if (paymentId != null && paymentId > 0) {
            value = String.valueOf(paymentId);
        }
        Query.update("UPDATE " + TABLE_NAME +
                " SET " + PAKET_PEMBAYARAN_ID + " = '" + value + "'" +
                " , " + MODIFIED_DATE + " = '" + DateTimeUtils.getStandardDateTime() + "'" +
                " , " + MODIFIED_BY + " = '" + AktifUser.getActiveUserId() + "'" +
                " WHERE " + ID + " =?" +
                " AND " + PAKET_ID + " =?", id, packageId);
    }

    public static void resetPaymentId(Long id, Long packageId) {
        Query.update("UPDATE " + TABLE_NAME +
                " SET " + PAKET_PEMBAYARAN_ID + " = null " +
                " , " + MODIFIED_DATE + " = '" + DateTimeUtils.getStandardDateTime() + "'" +
                " , " + MODIFIED_BY + " = '" + AktifUser.getActiveUserId() + "'" +
                " WHERE " + ID + " =?" +
                " AND " + PAKET_ID + " =?", id, packageId);
    }

    public static List<PaketRiwayatPenerimaan> getByIds(Long packageId, String ids, String[] fields) {
        String fieldPart = KatalogUtils.generateSelectedColumns(fields);
        final String query = "SELECT " + fieldPart + " FROM " + TABLE_NAME +
                " WHERE " + PAKET_ID + " =?" +
                " AND " + ID + " IN (" + ids + ")";
        return Query.find(query, PaketRiwayatPenerimaan.class, packageId).fetch();
    }

    public static Map<Long, PaketRiwayatPenerimaan> getMapByIds(Long packageId, String ids) {
        final String[] fields = {ID, PAKET_ID};
        return getByIds(packageId, ids, fields).stream().collect(Collectors.toMap(PaketRiwayatPenerimaan::getId, p -> p));
    }

    public static List<PaketRiwayatPenerimaan> getByPaymentId(Long packageId, Long paymentId) {
        final String query = "SELECT id, paket_id FROM " + TABLE_NAME +
                " WHERE " + PAKET_ID + " =?" +
                " AND " + PAKET_PEMBAYARAN_ID + " =?";
        return Query.find(query, PaketRiwayatPenerimaan.class, packageId, paymentId).fetch();
    }

    /**
     * set delivery id to acceptance history
     * @param id local id
     * @param packageId paket id
     * @param parent delivery id*/
    public static void setDeliveryId(Long id, Long packageId, Long parent) {

    }

    /**
     * reset delivery id on specify acceptance history
     * @param id acceptanceId
     * @param packageId paket id */
    public static void resetDeliveryId(Long id, Long packageId) {

    }

}
