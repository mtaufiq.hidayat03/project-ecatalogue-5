package models.user;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Table(name="user_role_item_override")
public class UserRoleItemOverride extends BaseKatalogTable {

	@Id
	public Long id;
	public boolean active;
	public Long role_item_id;
	public Long user_id;

	@Transient
	public String role_item_name;

	public UserRoleItemOverride() {}

	public UserRoleItemOverride(Long itemId, Long userId) {
		this.role_item_id = itemId;
		this.user_id = userId;
		this.active = true;
	}

	public static List<UserRoleItemOverride> findByUserId(long id){
		String query = "select uri.id, uri.role_item_id, uri.user_id, ri.nama_role as role_item_name from user_role_item_override uri " +
				"inner join role_item ri on uri.role_item_id = ri.id where uri.active = 1 and uri.user_id = ?";

		return Query.find(query, UserRoleItemOverride.class, id).fetch();
	}

	public static Set<Long> findByUserIdMap(long id){
		List<UserRoleItemOverride> userRoleItems = findByUserId(id);
		return userRoleItems.stream().map(i -> i.role_item_id).collect(
				Collectors.toSet());
	}

	public static void deleteByUserId(long userId) {
		String query = "delete from user_role_item_override where user_id = ?";
		Query.update(query, userId);
	}
}
