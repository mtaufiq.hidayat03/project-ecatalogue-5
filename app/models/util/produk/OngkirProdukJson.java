package models.util.produk;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by dadang on 8/23/17.
 */
public class OngkirProdukJson {
	public BigDecimal ongkir;
	public Long provinsiId;
	public Long kabupatenId;
}
