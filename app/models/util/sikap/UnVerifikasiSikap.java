package models.util.sikap;

public class UnVerifikasiSikap {
    public String[] identitas;
    public String[] izin_usaha;
    public String[] akta_pendirian;
    public String[] akta_perubahan;
    public String[] pemilik;
    public String[] pengurus;
    public String[] tenaga_ahli;
    public String[] peralatan;
    public String[] pengalaman;
    public String[] pajak;
}
