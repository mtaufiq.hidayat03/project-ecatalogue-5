//var urlvalcode = '@{admin.KomoditasAtributHargaCtr.delete()}';
//var urlvalname = '@{admin.KomoditasAtributHargaCtr.delete()}';
var $validator = $(".form_create").validate({
    rules:{
        'form.templateJadwal.deskripsi' : {
            required: function()
            {
                CKEDITOR.instances.description.updateElement();
            }
        },
        'form.komoditas.nama_komoditas': {
            required: true,
            alphanumeric: true,
            remote: {
                url:"/admin/komoditas/validate-name",
                type:"post"
            }
        },
        'form.komoditas.kode_komoditas': {
            required: true,
            alphanumeric: true,
            remote: {
                url:"/admin/komoditas/validate-kode",
                type:"post"
            }
        },
        'form.komoditas.margin_harga': {
            required: true,
            number: true,
            min : 0,
            max: 100
        },
        'form.templateJadwal.nama_template' : {
            required: true,
            alphanumeric: true
        },
        'usulan.nama_usulan': {
            required: true,
            alphanumeric: true
        },
        'usulan.pengusul_nama': {
            required: true,
            alphanumeric: true
        },
        'usulan.pengusul_nik': {
            required: true,
            number: true
        },
        'usulan.kldi_id': {required: true},
        'usulan.template_jadwal_id': {required: true},
        'usulan.pengusul_email': {
            required: true,
            emailAddress: true
        },
        'usulan.pokja_id': {required: true},
        'kategori[]': {required: true},
        'file-counter': {required: true, min: 1},
        'file-pokja-counter': {required: true, min: 1},
        'usulan.pengusul_no_telp': {number: true}
    },
    messages:{
        'form.komoditas.nama_komoditas': {
            required: "Nama Komoditas belum diisi!",
            alphanumeric: "Nama Komoditas hanya berupa alphanumeric!",
            remote: "Nama Komoditas sudah digunakan"
        },
        'form.komoditas.kode_komoditas': {
            required: "Kode Komoditas belum diisi!",
            alphanumeric: "Kode Komoditas hanya berupa alphanumeric!",
            remote: "Kode Komoditas sudah digunakan"
        },
        'form.komoditas.margin_harga': {
            required: "Margin Harga belum diisi!",
            number: "Margin Harga hanya bisa berupa angka desimal!",
            min: "Nilai Margin Harga minimal 0 %",
            max: "Nilai Margin Harga maksimal 100 %"
        },
        'form.komoditas.external_url': {
            required: "External URL belum diisi!",
            url: "Harus berupa format URL yang benar!"
        },
        'form.komoditas.jenis_produk_override': "Wajib dipilih salah satu!",
        'form.komoditas.apakah_stok_unlimited': "Wajib dipilih salah satu!",
        'form.komoditas.apakah_external_url': "Wajib dipilih salah satu!",
        'form.komoditas.perlu_negosiasi_harga': "Wajib dipilih salah satu!",
        'form.komoditas.perlu_ongkir': "Wajib dipilih salah satu!",
        'form.komoditas.apakah_iklan': "Wajib dipilih salah satu!",
        'form.komoditas.apakah_paket_produk_duplikat': "Wajib dipilih salah satu!",
        'form.templateJadwal.nama_template': {
            required: "Nama Template Jadwal belum diisi!",
            alphanumeric: "Nama Template Jadwal hanya berupa alphanumeric!"
        },
        'form.templateJadwal.deskripsi': {
            required: "Deskripsi Template Jadwal wajib diisi!"
        },
        'usulan.nama_usulan': {
            required: "Judul usulan belum diisi!",
            alphanumeric: "Judul usalan hanya berupa alphanumeric!"
        },
        'usulan.pengusul_nama': {
            required: "Nama pengusul belum diisi!",
            alphanumeric: "Nama pengusul hanya berupa alphanumeric!"
        },
        'usulan.pengusul_nik': {
            required: "NIK/NIP pengusul belum diisi!",
            number: "NIK/NIP hanya bisa berupa angka!"
        },
        'usulan.kldi_id': 'Belum memilih KLDI!',
        'usulan.pengusul_email': {
            required: "Belum mengisi alamat email!",
            emailAddress: "Format email tidak tepat"
        },
        'usulan.pengusul_no_telp': {
            number: "No. telp hanya bisa berupa angka!"
        },
        'usulan.pokja_id': "Belum memilih Pokja",
        'kategori[]': "Belum memilih kategori produk!",
        'usulan.template_jadwal_id': 'Belum memilih template jadwal!',
        'file-counter': "Belum mengupload file dokumen",
        'file-pokja-counter': "Belum mengupload file pokja"
    },
    highlight: function(element) {
        $(element).parents('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).parents('.form-group').removeClass('has-error');
    },
    errorPlacement: function(error, element)
    {
        if ( element.is(":radio") ){
            error.appendTo( element.parents('.radioGroup') );
        }else { // This is the default behavior
            error.insertAfter( element );
        }
    },
    onkeyup: false

});

function validateTab(currentIndex){
    if(currentIndex == 1){
       return validateManufaktur();
    } else if(currentIndex == 2){
        return validateProductCategories();
    }else if(currentIndex == 3){
        return validateTipeAtribut();
    }else if(currentIndex == 4){
        return validateAtributKategori();
    }else if(currentIndex == 5){
        return validateAtributHarga();
    }else if(currentIndex == 6){
        $('#file-counter').val(totalUploaded);
        return validateTemplateJadwal();
    }else{
        
        if(currentIndex == 0){
            var katkom = $("#kategoriKomoditas").val();
            var butuhNego = $("input[name='form.komoditas.perlu_negosiasi_harga']:checked").val();
            var currentFlag = isLelangOrDatafeed;
            if(katkom == datafeedId || (katkom == katalogId && butuhNego == 0)){
                isLelangOrDatafeed = true;
                $("#header-negosiasi").hide();
            }else{
                $("#header-negosiasi").show();
                isLelangOrDatafeed = false;
            }

            if(currentFlag != isLelangOrDatafeed){
                $("#tahapan_jadwal tbody").html("");
                jadwalIndex = 0;
                counterTemplateJadwal = 0;

                var tahapanObjLength = Object.keys(tahapanObj).length;
                if(tahapanObjLength > 0){
                    for(var i = 0; i < tahapanObjLength; i++){
                        if(i in tahapanObj){
                            var str = generateSelectTahapan(tahapanObj,i,selectedNego,selectedPenawaran);
                            $("#tahapan_jadwal").append(str);
                            jadwalIndex++;
                            counterTemplateJadwal++;
                        }
                    }
                }
            }

        }

        var $valid = $(".form_create").valid();
        if (!$valid) {
            $validator.focusInvalid();
            return false;
        }

        return true;
    }
}

jQuery('.form_create').submit(function (e) {
    var $valid = $(".form_create").valid();
    if (!$valid) {
        $validator.focusInvalid();
        return false;
    }else{
        // encrypt form, ketika validasi sudah oke
        try{
            //alert(1);
            replaceWordWithEncrypt();
        }catch (e) {
            //alert(2);
            CKEDITOR.replace( 'description' );
            replaceWordWithEncrypt();
        }
    }
});

function replaceWordWithEncrypt(){
    var kontrakDocs  = CKEDITOR.instances.description.getData();
    var kontrakDocsE = htmlEscape(kontrakDocs);//ini fungsi encode
    CKEDITOR.instances.description.setData(kontrakDocsE);
}

function checkArray(my_arr){
    for(var i=0;i<my_arr.length;i++){
        if(my_arr[i] === "")
            return false;
    }
    return true;
}

function checkAlphanumeric(my_arr){
    for(var i=0;i<my_arr.length;i++){
        var item = my_arr[i];
        var regExp = /^[0-9a-zA-Z.\-_\s]+$/;
        if(item.match(regExp) == null)
            return false;
    }
    return true;
}

function checkAlphanumericAtributKategori(my_arr){
    for(var i=0;i<my_arr.length;i++){
        var item = my_arr[i];
        var regExp = /^[0-9a-zA-Z.\-_\s\+\(\)\.\/]+$/;
        if(item.match(regExp) == null)
            return false;
    }
    return true;
}

function validateManufaktur(){
    // console.log(counterManufaktur);
    if(counterManufaktur == 0 ){
        $("#alert-manufaktur").show();
        return false;
    }else{
        $("#alert-manufaktur").hide();
    }

    var values = $("input[name='form.manufaktur']")
        .map(function(){return $(this).val();}).get();

    if(!checkArray(values)){
        $("#alert-manufaktur").show();
        return false;
    }else if(!checkAlphanumeric(values)){
        $("#alert-manufaktur-alphanumeric").show();
        return false;
    }else{
        $("#alert-manufaktur").hide();
        $("#alert-manufaktur-alphanumeric").hide();
    }

    return true;
}

function validateProductCategories(){
    var ol = $('ol.sortable');
    if(ol.html() == ""){
        $("#alert-kategori-produk").show();
        return false;
    }else{
        var array = $('ol.sortable').nestedSortable('toArray', {startDepthCount: 0});
        var result = [];
        var totalEmptyName = 0;
        var totalNotAlphaNumeric = 0;
        for (var i in array) {
            var item = array[i];
            if (item.id) {
                item['name'] = $("#list_" + item.id).find("input[type=text]").val();
                $("#list_" + item.id).find("input[type=text]").attr("value",item['name']);
                if (item.name === '') {
                    totalEmptyName++;
                }
                
                if(item.name.match(/^[0-9a-zA-Z.\-_\s]+$/) == null){
                    totalNotAlphaNumeric++;
                }
                var part = {"id":item.id,"name":item['name'],"depth":item.depth};
                result.push(part);
            }
        }
        if (totalEmptyName === 0 && totalNotAlphaNumeric === 0) {

            $('#position').val(JSON.stringify(array));
            var html = $("#list-kategori").html();
            $("#kategori_html").val(html);

            $("#alert-kategori-produk").hide();
            $("#alert-kategori-produk-alphanumeric").hide();

            if(isKategoriChange){
                atributKategoriObj = {};
                generateInsertAtributProduk(result);
                isKategoriChange = false;
            }
            return true;

        } else {
            if(totalEmptyName > 0){
                $("#alert-kategori-produk").show();
            }
            if(totalNotAlphaNumeric > 0){
                $("#alert-kategori-produk-alphanumeric").show();
            }
            return false;
        }
    }
}

function validateTipeAtribut(){
    if(counterTipeAtribut == 0 ){
        $("#alert-tipe-atribut-kosong").show();
        return false;
    }else{
        $("#alert-tipe-atribut-kosong").hide();
    }

    var namaArray = $("input[name='form.tipeAtribut']")
        .map(function(){return $(this).val();}).get();

    if(!checkArray(namaArray)){
        $("#alert-tipe-atribut-required").show();
        return false;
    }else if(!checkAlphanumeric(namaArray)){
        $("#alert-tipe-atribut-alphanumeric").show();
        return false;
    }else{
        $("#alert-tipe-atribut-required").hide();
        $("#alert-tipe-atribut-alphanumeric").hide();
    }

    tipeAtributJson = JSON.stringify(namaArray);
    updateValueSelectTipeInformasi();
    return true;
}

function updateValueSelectTipeInformasi(){
    var arrAttr = JSON.parse(tipeAtributJson);
    $('.table.table-condensed #inputForm').each(function(iTb, fieldTb){
        $(fieldTb).children('tr').each(function(iTr, fieldTr){
            var fSelect = $(fieldTr).find('.dropdown-information-type')[0];
            var notIn = []; 
            $(arrAttr).each(function(iAttr, fAttr){
                var fOpt = $(fSelect).children('option[value="' + fAttr + '"]');
                if(fOpt.length == 0){
                    $(fSelect).append('<option value="' + fAttr + '">' + fAttr + '</option>');
                }
                notIn.push('[value="' + fAttr + '"]');
            })
            
            var fOpts = $(fSelect).find(':not(' + notIn.join(',') + ')');
            fOpts.each(function(i,rmvOpt){
                $(rmvOpt).remove();
            });
        });
    });
}

function validateAtributKategori(){
    for (var property in atributKategoriObj) {
        if (atributKategoriObj.hasOwnProperty(property) && atributKategoriObj[property] == 0) {
            $("#alert-atribut-kategori-kosong").show();
            return false;
        }
    }

    var values = $("#accordion input[required]")
        .map(function(){return $(this).val();}).get();

    if(!checkArray(values)){
        $("#alert-atribut-kategori").show();
        return false;
    }else if(!checkAlphanumericAtributKategori(values)){
        $("#alert-atribut-kategori-alphanumeric").show();
        return false;
    }else{
        $("#alert-atribut-kategori").hide();
        $("#alert-atribut-kategori-alphanumeric").hide();
        
        
        
        return true;
    }
}

function validateAtributHarga(){
    if(index == 0 ){
        $("#alert-atribut-harga-kosong").show();
        return false;
    }else{
        $("#alert-atribut-harga-kosong").hide();
    }

    var values = $("#tabel-atribut-harga input[required]")
        .map(function(){return $(this).val();}).get();

    var radiosUtama = document.getElementsByName('atributHarga.harga_utama');
    var radiosRetail = document.getElementsByName('atributHarga.harga_retail');

    var checkedUtama = false;
    var checkedRetail = false;


    for (var i = 0, length = radiosUtama.length; i < length; i++)
    {
        if (radiosUtama[i].checked)
        {
            checkedUtama = true;
        } else {

        }
    }

    for (var i = 0, length = radiosRetail.length; i < length; i++)
    {
        if (radiosRetail[i].checked)
        {
            checkedRetail = true;
        } else {

        }
    }

    if(values.length == 0){
        $("#alert-atribut-harga-kosong").show();
        return false;
    }else if(!checkArray(values)){
        $("#alert-atribut-harga").show();
        return false;
    }else if(!checkAlphanumeric(values)){
        $("#alert-atribut-harga-alphanumeric").show();
        return false;
    }else{
        $("#alert-atribut-harga").hide();
        $("#alert-atribut-harga-alphanumeric").hide();
        $("#alert-atribut-harga-kosong").hide();
    }

    if(!checkedUtama){
        $("#alert-atribut-pilih-harga-utama").show();
        return false;
    }else{
        $("#alert-atribut-pilih-harga-utama").hide();
    }

    if(!checkedRetail){
        $("#alert-atribut-pilih-harga-retail").show();
        return false;
    }else{
        $("#alert-atribut-pilih-harga-retail").hide();
    }

    return true;
}

function validateTemplateJadwal(){
    var $valid = $(".form_create").valid();
    var deskK = CKEDITOR.instances.description.getData();
    if (!$valid) {
        $validator.focusInvalid();
        return false;
    }
    if(deskK==""){
        $(window).scrollTop(0);
        $("#alert-template-jadwal-deskripsi").show();
        return false;
    }else{
        $("#alert-template-jadwal-deskripsi").hide();
    }

    if(counterTemplateJadwal == 0 ){
        $("#alert-template-jadwal-kosong").show();
        return false;
    }else{
        $("#alert-template-jadwal-kosong").hide();
    }

    var tahapan = $("select[name='form\\.tahapan_arr']")
        .map(function(){return $(this).val();}).get();

    //harus minimal ada 2 tahapan
    if(tahapan.length <=0){
        $("#alert-template-jadwal-tahapan-required").show();
        return false;
    }else{
        if(!checkArray(tahapan)){
            $("#alert-template-jadwal-tahapan-required").show();
            return false;
        }else{
            $("#alert-template-jadwal-tahapan-required").hide();
        }
    }


    var penawaran = $("input[name='form.penawaran']:checked").length;
    // console.log(penawaran);

    if(penawaran == 0){
        $("#alert-template-jadwal-penawaran-required").show();
        return false;
    }else{
        $("#alert-template-jadwal-penawaran-required").hide();
    }

    if(!isLelangOrDatafeed){
        var negosiasi = $("input[name='form.negosiasi']:checked").length;
        // console.log(negosiasi);

        if(negosiasi == 0){
            $("#alert-template-jadwal-negosiasi-required").show();
            return false;
        }else{
            $("#alert-template-jadwal-negosiasi-required").hide();
        }
    }

    return true;
}
