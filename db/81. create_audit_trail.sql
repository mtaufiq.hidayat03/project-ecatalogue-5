CREATE TABLE `audit_trail`(
	`id` Int(11) AUTO_INCREMENT NOT NULL,
	`action` VARCHAR(100) NULL,
	`path` text NULL,
	`payload` text NULL,
	`remoteAddress` VARCHAR(50) NULL,
	`method` VARCHAR(10) NULL,
	`session` VARCHAR(50) NULL,
	`user_agent` text NULL,
	`query_string` text NULL,
	`created_by` Int(11) NULL,
	`created_at` VARCHAR(30) NULL,
	PRIMARY KEY (`id`)
)
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;