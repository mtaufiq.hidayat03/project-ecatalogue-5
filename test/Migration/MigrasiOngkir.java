package Migration;

import com.google.gson.Gson;
import models.katalog.Produk;
import models.common.OngkosKirim;
import play.Play;
import play.db.jdbc.Query;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MigrasiOngkir {
    String filename = "reportProdukOngkir_"+ UUID.randomUUID().toString().substring(0,5)+".txt";
    static String rootPath = Play.applicationPath.getAbsolutePath();
    String msg = "";
    public String sqlHeaderProduk = "";
    public String sqlBody = "";
    int pageLength = 50;
    //List<Produk> listProduk = new ArrayList<>();
    public MigrasiOngkir(){
        sqlHeaderProduk = "select distinct(p.id) as id, p.nama_produk \n";
        sqlBody = "from produk p \n" +
                "join komoditas k on p.komoditas_id = k.id\n" +
                "join produk_wilayah_jual_kabupaten pp on p.id = pp.produk_id and pp.active = 1\n" +
                "join kabupaten kb on kb.id = pp.kabupaten_id\n" +
                "join provinsi pv on pv.id = kb.provinsi_id\n"+
                "JOIN penyedia pn\n" +
                "\t\t\tON p.penyedia_id = pn.id\n" +
                "\t\t\tJOIN penyedia_kontrak pk\n" +
                "\t\t\tON pn.id = pk.penyedia_id\n" +
                "where  p.active = 1 AND p.apakah_ditayangkan = 1 and k.perlu_ongkir = 1\n" +
                "AND p.berlaku_sampai <> '' AND p.minta_disetujui = 0 AND p.setuju_tolak = 'setuju'\n" +
                "AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1 \n" +
                "AND (k.apakah_stok_unlimited = 1 OR (k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0))\n";
    }

    protected List<Produk> getProdukPerPage(int start, int end){
        String sql = sqlHeaderProduk+ sqlBody+" limit ?, ?";
        try{
            return  Query.find(sql, Produk.class, start,end).fetch();
        }catch (Exception e){
            msg = "error getProdukPerpage:"+e.getMessage();
            writeReportToFile(msg);
        }
        return new ArrayList<>();
    }

    public void startMigration(){
        msg="Starting Migration ONGKIR";writeReportToFile(msg);

        int start = 0;
        int end = pageLength;
        long totalProduk =  getTotal();//new Long(4493);

        msg = "total produk: "+totalProduk; writeReportToFile(msg);

        int totalPage = new Long(totalProduk/pageLength).intValue();
        int page = 1;

        while (totalProduk > start){
            //start info
            msg = "page "+page+" of "+totalPage+" page data";writeReportToFile(msg);

            List<Produk> listProduk = getProdukPerPage(start,pageLength);
            msg =  listProduk.size()+" length produk to execute";writeReportToFile(msg);

            for (Produk p : listProduk) {
                Long idP = p.id;
                String jsonOngkir = getJsonOngkir(idP);
                p = Produk.findById(idP);
                p.ongkir = jsonOngkir;
                //msg= "ongkirnya:"+jsonOngkir;

                //saving produkOngkir
                try{
                    int result = p.save();
                    msg = "result update objek produk, id : " + result+" produk_id="+idP;
                }catch (Exception e){
                    msg = "error update produk: "+e.getMessage();writeReportToFile(msg);
                    e.printStackTrace();
                }
                writeReportToFile(msg);
                //msg = "update produk set ongkir='"+jsonOngkir+ "' where id = "+p.id;

                //int restUpdate = Query.update("update produk set ongkir=? where id=?", jsonOngkir ,idP);
                //msg = "result update:"+ restUpdate+" for produk_id="+p.id;writeReportToFile(msg);
            }
            //end info
            //paging and next count
            end+=pageLength;start+=pageLength;page++;
        }
    }
    public long getTotal(){
        msg = "Get TOTAL produk";writeReportToFile(msg);
        Long total = 0L;
        try {
            String sql = "select count(distinct(p.id)) " + sqlBody;
            total = Query.count(sql);
        }catch (Exception e){
            msg = "error get total: "+e.getMessage(); writeReportToFile(msg);
        }
        return total;
    }

    public String getJsonOngkir(long produk_id){
        msg = "getList ongkir for produk_id: "+produk_id;writeReportToFile(msg);
        String result = "[]";
        List<OngkosKirimRquery> listOngkir = new ArrayList<>();
        //p.id, JSON_OBJECT('ongkir', pp.harga_ongkir,'provinsiId', kb.provinsi_id, 'kabupatenId', pp.kabupaten_id) ongkir,
        String headQuery = "select pp.harga_ongkir as ongkir, \n"+
                "kb.provinsi_id as provinsiId,\n" +
                "pp.kabupaten_id as kabupatenId,\n" +
                "kb.nama_kabupaten as kabupaten,\n"+
                "pv.nama_provinsi as provinsi \n";
        String sqlJson = headQuery + sqlBody +" AND p.id="+produk_id;
        try {
            listOngkir = Query.find(sqlJson, OngkosKirimRquery.class).fetch();
            msg = "length ongkirdata:"+listOngkir.size();writeReportToFile(msg);
            result = new Gson().toJson(listOngkir);
            //msg= result; writeReportToFile(msg);
        }catch (Exception e){
            msg = "error getjsonongkir:"+e.getMessage();writeReportToFile(msg);
            e.printStackTrace();
        }
        return result;
    }

    public void writeReportToFile(String text) {
        System.out.println(text);
        text += " \n";
        if (!Files.exists(Paths.get(rootPath + "/"+filename))) {
            PrintWriter writer = null;
            try {
                System.out.println("new file name report:"+filename);
                writer = new PrintWriter(rootPath + "/"+filename, "UTF-8");
            } catch (FileNotFoundException e) {

            } catch (UnsupportedEncodingException e) {

            }
            writer.println("----<The first line >--- \n");
            writer.close();
        }
        try {
            Files.write(Paths.get(filename), text.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
            System.out.println("Eror write filereport:" + e.getMessage());
            e.printStackTrace();
        }
        msg = "";
    }
}
