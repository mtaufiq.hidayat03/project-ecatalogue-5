package models.purchasing;

import models.BaseKatalogTable;
import models.katalog.Komoditas;
import models.katalog.Produk;
import models.purchasing.contracts.PackageProductContract;
import models.purchasing.contracts.PriceContract;
import models.purchasing.riwayat.PaketRiwayatNegosiasi;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.UrlUtil;

import javax.persistence.Transient;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author HanusaCloud on 10/16/2017
 */
@Table(name = "paket_produk")
public class PaketProduk extends BaseKatalogTable implements PackageProductContract, PriceContract {

    @Id
    public Long id;
    public Long produk_id;
    public BigDecimal kuantitas;
    public Long paket_id;
    public String catatan;

    public Double harga_satuan;
    public Double harga_ongkir;
    public Date batas_pengiriman;
    public Double harga_total;

    public Long provinsi_id;
    public Long kabupaten_id;

    public Double konversi_harga_satuan = new Double(0);
    public Double konversi_harga_ongkir = new Double(0);
    public Double konversi_harga_total = new Double(0);

    public boolean active = true;

    @Transient
    public String nama_manufaktur;
    @Transient
    public String nama_produk;
    @Transient
    public Integer produk_kategori_id;
    @Transient
    public String no_produk;
    @Transient
    public Double best_price;
    @Transient
    public Double conversion_best_price;
    @Transient
    public String instansi;
    @Transient
    public String nama_paket;
    @Transient
    public String satuan_kerja_nama;
    @Transient
    public String satuan_kerja_alamat;
    @Transient
    public String no_paket;
    @Transient
    public String nama_provinsi;
    @Transient
    public String tahun;

    @Transient
    public Komoditas komoditas;
    @Transient
    public List<PaketRiwayatNegosiasi> histories = new ArrayList<>();
    @Transient
    public Produk produk;
    @Transient
    public Paket paket;

    @Transient
    public String namaKurs = "IDR";
    public String getNamaKurs(){
        try{
            return this.paket.getKurs().nama_kurs;
        }catch (Exception e){}
        return namaKurs;
    }

    public Long getProdukId() {
        return this.produk_id;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public Long getPackageId() {
        return this.paket_id;
    }

    @Override
    public Long getProductId() {
        return this.produk_id;
    }

    @Override
    public Long getPaketId() {
        return this.paket_id;
    }

    @Override
    public void setProduct(Produk product) {
        this.produk = product;
    }

    @Override
    public void setPaket(Paket paket) {
        this.paket = paket;
    }

    @Override
    public void setPackageProductNegotiations(List<PaketRiwayatNegosiasi> list) {
        this.histories = list;
    }

    @Override
    public Double getUnitPrice() {
        return this.harga_satuan;
    }

    @Override
    public Double getDeliveryCost() {
        return this.harga_ongkir;
    }

    @Override
    public Double getTotalPrice() {
        return this.harga_total;
    }

    @Override
    public Double getConversionUnitPrice() {
        return this.konversi_harga_satuan;
    }

    @Override
    public Double getConversionDeliveryCost() {
        return this.konversi_harga_ongkir;
    }

    @Override
    public Double getConversionTotalPrice() {
        return this.konversi_harga_total;
    }

    @Override
    public Double getBestPrice() {
        return this.best_price;
    }

    @Override
    public Double getConversionBestPrice() {
        return this.conversion_best_price;
    }

    @Override
    public String getCheckedUser(){
        return null;
    }

    public String getDetailUrl() {
        return UrlUtil.builder()
                .setUrl("katalog.produkctr.detail")
                .setParam("id", this.produk_id)
                .build();
    }

    public static void deleteByPaketId(long paket_id){
        String query = "DELETE from paket_produk where paket_id = ?";
        Query.update(query, paket_id);
    }

    public static void deletePaketProdukById(long paket_produk_id){
        String query = "DELETE from paket_produk where id = ?";
        Query.update(query, paket_produk_id);
    }

    public static void inactiveProdukByPaketId(long paket_id){
        String query = "UPDATE paket_produk set active = 0 where paket_id = ?";
        Query.update(query, paket_id);
    }

    public  String convertBatasPengirimanDate(){
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String s = "";
        if (batas_pengiriman!=null){
            try {
                s = df.format(batas_pengiriman);
            }
            catch(Exception e) {}
        }
        return s;
    }

    public static List<PaketProduk> findIdPaketAndActive(Long idPaket){
        return find("paket_id = ? and active = ?",idPaket, AKTIF).fetch();
    }

    public Double getHargaTotal() {
        return harga_total;
    }

    public Double getKonversiHargaTotal() {
        return konversi_harga_total;
    }

}
