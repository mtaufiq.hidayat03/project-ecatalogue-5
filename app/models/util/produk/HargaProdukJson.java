package models.util.produk;

import models.masterdata.Kabupaten;
import models.masterdata.Provinsi;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dadang on 8/23/17.
 */
public class HargaProdukJson {
	public double harga_utama;
	public double ongkos_kirim;
	public List<HargaProvinsiJson> provs;

	public BigDecimal harga;
	public Long provinsiId;
	public Long kabupatenId;

	public Long getProvinsiId(){
		Long ret = 0l;
		if(null != provinsiId){
			return provinsiId;
		}
		return ret;
	}
	public Long getKabupatenId(){
		Long ret = 0l;
		if(null != kabupatenId){
			return kabupatenId;
		}
		return ret;
	}

	public String getProvinceName(){
		String result = "";
		Provinsi prv = Provinsi.findById(provinsiId);
		if(prv != null){
			result = prv.nama_provinsi;
		}
		return result;
	}

	public String getRegencyName(){
		String result = "";
		Kabupaten prv = Kabupaten.findById(kabupatenId);
		if(prv != null){
			result = prv.nama_kabupaten;
		}
		return result;
	}

}
