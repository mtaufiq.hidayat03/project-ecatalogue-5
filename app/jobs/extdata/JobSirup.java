package jobs.extdata;

import models.api.MailQueueF;
import models.jcommon.config.Configuration;
import models.jcommon.mail.MailQueue;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import play.jobs.On;

import java.util.Date;
import java.util.UUID;

/** job di eksekusi setiap jam 01:00 **/
// @On("0 0 1 * * ?") // daily at 08 : 39 @On("0 39 08 * * ?")
@NoTransaction
public class JobSirup extends Job {
    String uniqueID = UUID.randomUUID().toString();
    public void before() {
        Logger.info("job id:" + uniqueID);
        super.before();
    }
    public void doJob(){
        pullData();
        Logger.info("JobSirup selesai "+ new Date().toString());
//        new Job() {
//            String uniqueID = UUID.randomUUID().toString();
//            @Override
//            public void before() {
//                Logger.info("job id:" + uniqueID);
//                super.before();
//            }
//
//            public void doJob() {
//                pullData();
//                Logger.info("JobSirup selesai "+ new Date().toString());
//            }
//
//        }.in(1);
    }

    private void pullData() {
        Logger.info("Job SIRUP Start");
        ExtData ext = new ExtData();
        Date tgl = ext.getSirupBetweenDate();
        Logger.info(">>>>>> getData Sirup with date:"+tgl.toLocaleString());
        ext.getSyrupData(tgl);
        Logger.info("%%%% done getDate, saving to DB now %%%");
        ext.saveDataToDb();
        Logger.info("%%%% done saving to DB, sendEmailReport now %%%");
        ext.sendEmailReport();

    }
    public void after(){
        sendEmailReport();
    }

    public void sendEmailReport(){
        String body = "Done Execution";
        String title = "Report JOB SIRUP";
        String report = Configuration.getConfigurationValue(MailQueue.SMTP_MAIL_ADMIN, "");
        String[] ary = report.split(";");
        for (String to : ary) {
            MailQueueF mq = new MailQueueF();
            try {
                Logger.info(to+" "+body+" "+title);
                mq = MailQueueF.createEmail(to, body, title);
            } catch (Exception e) {
                Logger.error("error create mail:"+e.getMessage());
            }
            Logger.info(mq.save()+"");
        }
    }

}
