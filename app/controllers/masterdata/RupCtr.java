package controllers.masterdata;

import com.google.gson.Gson;
import controllers.BaseController;
import controllers.security.AllowAccess;
import jobs.extdata.ExtData;
import jobs.extdata.JobSirup;
import models.api.Rup;
import models.api.Sirup;
import models.api.SirupById;
import models.secman.Acl;
import org.json.simple.JSONObject;
import play.Logger;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class RupCtr extends BaseController {
	private static Rup rup = new Rup();

	@AllowAccess({Acl.COM_RUP})
	public static void index() {
		render("masterdata/RupCtr/index.html");
	}

//	@AllowAccess({Acl.COM_RUP_ADD})
	public static void manualFetch() {
		JobSirup job = new JobSirup();
		job.doJob();
		index();
	}

	public static void getRupByDate(String auditupdate, Long idrup){
		ExtData ext = new ExtData();
		Date tgl = new Date();
		String sDate1 = auditupdate;
		sDate1 = auditupdate.isEmpty() ? "2019-09-02" : auditupdate;
		try {
			tgl = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Logger.info(">>>>>> getData Sirup with date:"+tgl.toLocaleString());
		ext.getSyrupData(tgl);
		Logger.info("%%%% done getDate, saving to DB now %%%");
		List<Sirup> sirupList = new ArrayList<>();
		List<RupListSelect> rupListSelectList = new ArrayList<>();
		//List<String[]> list = new ArrayList<>();
		sirupList = ext.parseAndGetData();
		for (int a = 0; a < sirupList.size(); a++) {
			if (sirupList.get(a).id.equals(idrup)){
//				String[] data = new String[3];
//				data[0] = sirupList.get(a).id.toString();
//				data[1] = sirupList.get(a).nama;
//				data[2] = auditupdate;
				RupListSelect rupListSelect = new RupListSelect(sirupList.get(a).id, sirupList.get(a).nama, auditupdate, 0);
				rupListSelectList.add(rupListSelect);
				//list.add(data);
				rup = sirupList.get(a).getAsRup();
			}
		}

		//JsonObject json = new
		renderJSON(createSimpleJson(rupListSelectList));
	}

	public static void pullRupByDate(String auditupdate){
		//0 sukses, 1 penarikan gagal, 2 simpan ada yang gagal
		int tipe = 0;
		String notif = "";
		ExtData ext = new ExtData();
		Date tgl = new Date();
		String sDate1 = auditupdate;
		sDate1 = auditupdate.isEmpty() ? "2019-09-02" : auditupdate;
		try {
			tgl = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Logger.info(">>>>>> getData Sirup with date:"+tgl.toLocaleString());
		ext.getSyrupData(tgl);
		Logger.info("%%%% done getDate, saving to DB now %%%");
		List<Sirup> sirupList = new ArrayList<>();
		List<RupListSelect> rupListSelectList = new ArrayList<>();
		//List<String[]> list = new ArrayList<>();
		sirupList = ext.parseAndGetData();

		for (int a = 0; a < sirupList.size(); a++) {
			int result = 0;
			Rup rups = new Rup();
			rups = sirupList.get(a).getAsRup();

			Rup rupObj = new Rup();
			rupObj.id = rups.id;
			rupObj.nama = rups.nama;
			rupObj.kegiatan = rups.kegiatan;
			rupObj.jenis_belanja = rups.jenis_belanja ;
			rupObj.jenis_pengadaan = rups.jenis_pengadaan;
			rupObj.volume = rups.volume;
			rupObj.metode_pengadaan = rups.metode_pengadaan;
			rupObj.tanggal_awal_pengadaan= rups.tanggal_awal_pengadaan;
			rupObj.tanggal_akhir_pengadaan = rups.tanggal_akhir_pengadaan;
			rupObj.tanggal_awal_pekerjaan = rups.tanggal_awal_pekerjaan;
			rupObj.tanggal_akhir_pekerjaan = rups.tanggal_akhir_pekerjaan;
			rupObj.lokasi = rups.lokasi;
			rupObj.keterangan = rups.keterangan;
			rupObj.tahun_anggaran = rups.tahun_anggaran;
			rupObj.id_sat_ker = rups.id_sat_ker;
			rupObj.kode_kldi = rups.kode_kldi;
			rupObj.aktif = rups.aktif;
			rupObj.is_delete = rups.is_delete;
			rupObj.audit_update = new Timestamp(rups.audit_update.getTime());
			rupObj.kode_anggaran = rups.kode_anggaran;
			rupObj.sumber_dana = rups.sumber_dana;
			result = rupObj.save();

			if (result == 0){
				RupListSelect rupListSelect = new RupListSelect(sirupList.get(a).id, sirupList.get(a).nama, auditupdate, 1);
				rupListSelectList.add(rupListSelect);
			}
		}

		if (sirupList == null || sirupList.size() == 0){
			notif = "Penarikan data RUP dari SiRup gagal!";
			tipe = 1;
		} else if (rupListSelectList != null && rupListSelectList.size() > 0){
			notif = "Ada data RUP yang gagal disimpan!";
			tipe = 2;
		} else {
			notif = "Penarikan Selesai!";
			tipe = 0;
		}

		//JsonObject json = new
		renderJSON(createSimpleJsonWithInfo(notif, tipe, rupListSelectList));
	}

	public static void getRupById(String idrup){
		//0 sukses, 1 penarikan gagal, 2 simpan ada yang gagal
		int tipe = 0;
		String notif = "";
		ExtData ext = new ExtData();

		String[] idrups = idrup.split(",");
		List<String> idrupsList = new ArrayList<>();
		List<RupListSelect> rupListSelectList = new ArrayList<>();
		List<SirupById> sirupList = new ArrayList<>();

		for(int a = 0; a < idrups.length; a++){
			if(!idrups[a].isEmpty()){
				idrupsList.add(idrups[a]);
			}
		}

		if (idrupsList.size()>0){
			for(int i = 0; i < idrupsList.size(); i++) {
				ext.getSyrupById(idrupsList.get(i));
				Logger.info("%%%% done getDate %%%");
				//List<String[]> list = new ArrayList<>();
				sirupList = ext.parseAndGetDataById();

				if (sirupList.size()>0){
					for (int a = 0; a < sirupList.size(); a++) {
						int result = 0;
						Rup rups = new Rup();
						rups = sirupList.get(a).getAsRup();

						if(rups.metode_pengadaan == 9) {
							Rup rupObj = new Rup();
							rupObj.id = rups.id;
							rupObj.nama = rups.nama;
							rupObj.kegiatan = rups.kegiatan;
							rupObj.jenis_belanja = rups.jenis_belanja;
							rupObj.jenis_pengadaan = rups.jenis_pengadaan;
							rupObj.volume = rups.volume;
							rupObj.metode_pengadaan = rups.metode_pengadaan;
							rupObj.tanggal_awal_pengadaan = rups.tanggal_awal_pengadaan;
							rupObj.tanggal_akhir_pengadaan = rups.tanggal_akhir_pengadaan;
							rupObj.tanggal_awal_pekerjaan = rups.tanggal_awal_pekerjaan;
							rupObj.tanggal_akhir_pekerjaan = rups.tanggal_akhir_pekerjaan;
							rupObj.lokasi = rups.lokasi;
							rupObj.keterangan = rups.keterangan;
							rupObj.tahun_anggaran = rups.tahun_anggaran;
							rupObj.id_sat_ker = rups.id_sat_ker;
							rupObj.kode_kldi = rups.kode_kldi;
							rupObj.aktif = rups.aktif;
							rupObj.is_delete = rups.is_delete;
							rupObj.audit_update = new Timestamp(rups.audit_update.getTime());
							rupObj.kode_anggaran = rups.kode_anggaran;
							rupObj.sumber_dana = rups.sumber_dana;
							result = rupObj.save();
						}

						if (result == 0){
							RupListSelect rupListSelect = new RupListSelect(rups.id, rups.nama, rups.audit_update.toString(),1);
							rupListSelectList.add(rupListSelect);
						} else {
							RupListSelect rupListSelect = new RupListSelect(rups.id, rups.nama, rups.audit_update.toString(),0);
							rupListSelectList.add(rupListSelect);
						}
					}
				} else {
					RupListSelect rupListSelect = new RupListSelect(Long.valueOf(idrupsList.get(i)), "-", "-",2);
					rupListSelectList.add(rupListSelect);
				}
			}
		}

		if (sirupList == null){
			notif = "Penarikan data RUP dari SiRup gagal!";
			tipe = 1;
		} else if (rupListSelectList != null && rupListSelectList.size() > 0){
			notif = "Ada data RUP yang gagal disimpan!";
			tipe = 2;
		} else {
			notif = "Penarikan Selesai!";
			tipe = 0;
		}

		//JsonObject json = new
		renderJSON(createSimpleJsonWithInfo(notif, tipe, rupListSelectList));
	}

	public static void simpanDataRup(Long idrup, String namarup){
		Rup rups = rup;
		Map<String, String> resultrup = new HashMap<>();
		if (idrup!=0){
			String successAndExist = "0";
			
			if (rups.id.equals(idrup) && rups.nama.equals(namarup)){
				Logger.info("RUP temp dan real cocok");
				Rup rupObj = Rup.findById(idrup);
				int result = 0;

				try{
					if(rupObj != null){
						rupObj.nama = rups.nama;
						rupObj.kegiatan = rups.kegiatan;
						rupObj.jenis_belanja = rups.jenis_belanja ;
						rupObj.jenis_pengadaan = rups.jenis_pengadaan;
						rupObj.volume = rups.volume;
						rupObj.metode_pengadaan = rups.metode_pengadaan;
						rupObj.tanggal_awal_pengadaan= rups.tanggal_awal_pengadaan;
						rupObj.tanggal_akhir_pengadaan = rups.tanggal_akhir_pengadaan;
						rupObj.tanggal_awal_pekerjaan = rups.tanggal_awal_pekerjaan;
						rupObj.tanggal_akhir_pekerjaan = rups.tanggal_akhir_pekerjaan;
						rupObj.lokasi = rups.lokasi;
						rupObj.keterangan = rups.keterangan;
						rupObj.tahun_anggaran = rups.tahun_anggaran;
						rupObj.id_sat_ker = rups.id_sat_ker;
						rupObj.kode_kldi = rups.kode_kldi;
						rupObj.aktif = rups.aktif;
						rupObj.is_delete = rups.is_delete;
						rupObj.audit_update = new Timestamp(rups.audit_update.getTime());
						rupObj.kode_anggaran = rups.kode_anggaran;
						rupObj.sumber_dana = rups.sumber_dana;
						result = rupObj.save();
						Logger.info("update:"+result+ " ID : "+rupObj.id);
					}else{
						rupObj = new Rup();
						rupObj.id = rups.id;
						rupObj.nama = rups.nama;
						rupObj.kegiatan = rups.kegiatan;
						rupObj.jenis_belanja = rups.jenis_belanja ;
						rupObj.jenis_pengadaan = rups.jenis_pengadaan;
						rupObj.volume = rups.volume;
						rupObj.metode_pengadaan = rups.metode_pengadaan;
						rupObj.tanggal_awal_pengadaan= rups.tanggal_awal_pengadaan;
						rupObj.tanggal_akhir_pengadaan = rups.tanggal_akhir_pengadaan;
						rupObj.tanggal_awal_pekerjaan = rups.tanggal_awal_pekerjaan;
						rupObj.tanggal_akhir_pekerjaan = rups.tanggal_akhir_pekerjaan;
						rupObj.lokasi = rups.lokasi;
						rupObj.keterangan = rups.keterangan;
						rupObj.tahun_anggaran = rups.tahun_anggaran;
						rupObj.id_sat_ker = rups.id_sat_ker;
						rupObj.kode_kldi = rups.kode_kldi;
						rupObj.aktif = rups.aktif;
						rupObj.is_delete = rups.is_delete;
						rupObj.audit_update = new Timestamp(rups.audit_update.getTime());
						rupObj.kode_anggaran = rups.kode_anggaran;
						rupObj.sumber_dana = rups.sumber_dana;
						result = rupObj.save();
						Logger.info("save:"+result + " ID : "+rupObj.id);
					}

					if (result != 0){
						Rup rupAfterSave = Rup.findById(idrup);

						if (rupAfterSave!=null){
							if (rupAfterSave.id.equals(idrup) && rupAfterSave.nama.equals(namarup)){
								successAndExist = "1";
							} else {
								successAndExist = "0";
							}
						} else {
							successAndExist = "0";
						}
					} else {
						successAndExist = "0";
					}

					resultrup.put("success", successAndExist);

				}catch (Exception e){
					Logger.info("error save or update rup:"+e.getMessage());
					e.printStackTrace();
					resultrup.put("success", "0");
				}
			} else {
				successAndExist = "0";
			}
		} else {
			resultrup.put("success", "2");
		}
		renderJSON(resultrup);
	}

	//untuk kebutuhan model rup tarik manual data terpilih saja
	public static class RupListSelect{
		public Long id;
		public String nama;
		public String auditupdate;
		public int isSuccess; //0 sukses, 1 gagal simpan, 2 gagal tarik

		public RupListSelect(Long id, String nama, String auditupdate, int isSuccess) {
			this.id = id;
			this.nama = nama;
			this.auditupdate = auditupdate;
			this.isSuccess = isSuccess;
		}
	}

	private static JSONObject createSimpleJson(List<RupListSelect> list){
		JSONObject jsonObject = new JSONObject();
		Gson gson = new Gson();
		jsonObject.put("draw",list.size());
		jsonObject.put("recordsTotal",new Long(list.size()));
		jsonObject.put("recordsFiltered",new Long(list.size()));
		jsonObject.put("data",gson.toJsonTree(list));
		return jsonObject;
	}

	private static JSONObject createSimpleJsonWithInfo(String notif, int tipe, List<RupListSelect> list){
		JSONObject jsonObject = new JSONObject();
		Gson gson = new Gson();
		jsonObject.put("draw",list.size());
		jsonObject.put("recordsTotal",new Long(list.size()));
		jsonObject.put("recordsFiltered",new Long(list.size()));
		jsonObject.put("tipe",tipe);
		jsonObject.put("notif",notif);
		jsonObject.put("data",gson.toJsonTree(list));
		return jsonObject;
	}

	public static void detail(long id) {
		Rup model = Rup.findByIdRup(id);
		notFoundIfNull(model);
		render("masterdata/RupCtr/detail.html", model);
	}

//	@AllowAccess({Acl.COM_RUP_ADD})
	public static void create() { render("masterdata/RupCtr/create.html"); }

//	@AllowAccess({Acl.COM_RUP_ADD})
	public static void createSubmit(String[] nama_rup_arr) {
//		checkAuthenticity();
//		if(nama_rup_arr != null){
//			//validasi sumber dana
//			boolean validate = validateRup(nama_rup_arr);
//			if(!validate){
//				if (validation.hasErrors())
//					flash.error(Messages.get("notif.failed.submit"));
//				List<String> sumberDanaList = Arrays.asList(nama_rup_arr);
//				render("masterdata/RupCtr/create.html", sumberDanaList);
//			}
//
//			for(int i = 0; i < nama_rup_arr.length; i++){
//				Rup rup = new Rup();
//
//				rup.active = true;
//				rup.save();
//			}
//
//			flash.success(Messages.get("notif.success.submit"));
//			index();
//		}

	}

//	@AllowAccess({Acl.COM_RUP_EDIT})
	public static void editSubmit(long id_sumber, String nama_rup) {
//		checkAuthenticity();
//		//UPDATE HERE
//		flash.success(Messages.get("notif.success.update"));
//		index();
	}

//	@AllowAccess({Acl.COM_RUP_DELETE})
	public static void delete(long id) {
//		Rup model = Rup.findById(id);
//		notFoundIfNull(model);
//		if (Rup.checkChildByFundSourceId(model.id) == 0) {
//			model.softDelete();
//			flash.success(Messages.get("notif.success.delete"));
//		} else {
//			flash.error(Messages.get("notif.failed.delete.relation"));
//		}
//		index();
	}

	public static void getRup(){
		List<Rup> sumberDanaList = Rup.find("active = 1").fetch();
		renderJSON(sumberDanaList);
	}

}
