package models.purchasing.contracts;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import models.purchasing.base.ProductJson;
import models.purchasing.penerimaan.LampiranPenerimaan;
import models.purchasing.penerimaan.ProdukPenerimaan;
import models.purchasing.pengiriman.PaketRiwayatPengiriman;
import models.purchasing.pengiriman.form.DeliveryItemJson;
import repositories.paket.AcceptanceRepository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 11/2/2017
 */
public interface AcceptanceContract extends HistoryContract {

    void setAttachment(LampiranPenerimaan model);
    void setProducts(List<ProdukPenerimaan> list);
    void setDelivery(DeliveryItemJson model);

    Long getDeliveryId();
    DeliveryItemJson getDelivery();
    List<ProdukPenerimaan> getProducts();

    /**
     * set acceptance products */
    default void withProducts() {
        if (getId() != null && getPackageId() != null) {
            List<ProdukPenerimaan> list = AcceptanceRepository.getProductsByAcceptanceIdAndPackageId(getPackageId(), getId());
            if (!list.isEmpty()) {
                for (ProdukPenerimaan model : list) {
                    model.setImage();
                }
                setProducts(list);
            }
        }
    }

    /**
     * set history file attachment based on id*/
    default void withAttachment() {
        if (getId() != null && getPackageId() != null) {
            setAttachment(AcceptanceRepository.getLatestAttachment(getPackageId(), getId()));
        }
    }

    /**
     * set delivery report */
    default void withDelivery() {
        if (getDeliveryId() != null) {
            PaketRiwayatPengiriman history = PaketRiwayatPengiriman.findById(getDeliveryId());
            if (history != null) {
                setDelivery(new DeliveryItemJson(history));
            }
        }
    }

    default String getDeliveryJSON() {
        if (getDelivery() != null) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add(getDelivery().getModelId(), getDelivery().getJsonObject());
            return jsonObject.toString();
        }
        return "{}";
    }

    default String getProductsJSON() {
        if (getProducts() != null && !getProducts().isEmpty()) {
            Map<Long, ProductJson> result = getProducts().stream()
                    .map(p -> new ProductJson(p)).collect(Collectors.toMap(ProductJson::getProductId, p -> p));
            return new Gson().toJson(result);
        }
        return "{}";
    }

}
