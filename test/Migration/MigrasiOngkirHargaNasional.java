package Migration;

import com.google.gson.Gson;
import models.katalog.Komoditas;
import models.katalog.Produk;
import models.katalog.ProdukHargaNasional;
import models.katalog.komoditasmodel.KomoditasAtributHarga;
import models.masterdata.Kabupaten;
import play.Play;
import play.db.jdbc.Query;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MigrasiOngkirHargaNasional {
    String filename = "reportOngkirNasional_"+ UUID.randomUUID().toString().substring(0,5)+".txt";
    static String rootPath = Play.applicationPath.getAbsolutePath();
    String msg = "";
    int pageLength = 50;

    public void start(){

        msg=">>>>>>>>>>>>>>>>> Starting Migration Harga Nasional >>>>>>>>>>>>";writeReportToFile(msg);
        int start = 0;
        int end = pageLength;

        //long totalProduk =  getTotal();//new Long(4493);
        long totalProduk =  new Long(100);
        msg = "total produk: "+totalProduk; writeReportToFile(msg);

        int totalPage = new Long(totalProduk/pageLength).intValue();
        int page = 1;

        while (totalProduk > start) {
            List<Produk> listProduk = new ArrayList<>();
            //ambil sebanyak page data
            msg = "page " + page + " of " + totalPage + " page data";writeReportToFile(msg);
            msg = "start " + start + " to " + end+" end "+totalProduk;writeReportToFile(msg);
            //dataproduk perpage
            listProduk = getProdukPerPage(start, pageLength, "nasional");
            msg = "totalpagedata:"+listProduk.size();writeReportToFile(msg);

            for (Produk prd: listProduk) {
                Komoditas komo = Komoditas.findById(prd.komoditas_id);
                List<KomoditasAtributHarga> kah = KomoditasAtributHarga.find("komoditas_id = ? and apakah_ongkir = 1 and active = 1",prd.komoditas_id).fetch();
                msg = "KomoditasAtributHarga: "+kah.size();writeReportToFile(msg);
                msg = "@@@@@ komoditashargaatribut produk_id:" + prd.id ;writeReportToFile(msg);


                for (KomoditasAtributHarga kh:kah) {
                    msg = "atribut_id:"+kh.id+" label:"+kh.label_harga;writeReportToFile(msg);
                    ProdukHargaNasional pha = ProdukHargaNasional.find("produk_id=? and komoditas_harga_atribut_id=? and active=1", prd.id, kh.id).first();
                    List<Kabupaten> listKabupaten = Kabupaten.find("active=1").fetch();
                    OngkirProdukNasionalMigration hpm = new OngkirProdukNasionalMigration();

                    List<String> hargaAll = new ArrayList<>();
                    for (int i = 0; i < listKabupaten.size(); i++) {
                        hpm.kabupatenId = listKabupaten.get(i).id;
                        hpm.provinsiId = listKabupaten.get(i).provinsi_id;
                        hpm.ongkir = new BigDecimal(pha.harga);
                        String hrg = new Gson().toJson(hpm);
                        hargaAll.add(hrg);
                    }
                    prd.ongkir = new Gson().toJson(hargaAll);
                    msg = ">> new Ongkir produk :"+prd.ongkir+" produk_id:"+prd.id;
                    //msg = ">> new Ongkir produk:"+prd.save()+" produk_id:"+prd.id;

                    writeReportToFile(msg);
                }

            }

            end+=pageLength;start+=pageLength;page++;
        }
        msg = "END MIGRATION NASIONAL";writeReportToFile(msg);
    }



    protected List<Produk> getProdukPerPage(int start, int page, String kelas_harga){
        String sql = "select p.id, p.komoditas_id, p.nama_produk\n" +
                "from produk p\n" +
                "join komoditas k on p.komoditas_id = k.id\n" +
                "JOIN penyedia pn ON p.penyedia_id = pn.id\n" +
                "JOIN penyedia_kontrak pk ON pn.id = pk.penyedia_id\n" +
                "where p.active = 1 AND p.apakah_ditayangkan = 1\n" +
                "AND k.kelas_harga = ? \n" +
                "AND p.berlaku_sampai <> '' AND p.minta_disetujui = 0 AND p.setuju_tolak = 'setuju'\n" +
                "AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1\n" +
                "AND (k.apakah_stok_unlimited = 1 OR (k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0)) limit ?, ?";
        try{
            return Query.find(sql, Produk.class,kelas_harga, start, page).fetch();
        }catch (Exception e){
            msg = "error getProdukPerpage:"+e.getMessage();
            writeReportToFile(msg);
        }
        return new ArrayList<>();
    }
    public long getTotal(){
        msg = "Get TOTAL produk";writeReportToFile(msg);
        Long total = 0L;
        try {
            String sql = "select count(p.id) from produk p\n" +
                    "join komoditas k on k.id = p.komoditas_id\n" +
                    "where k.kelas_harga='nasional'";
            total = Query.count(sql);
        }catch (Exception e){
            msg = "error get total: "+e.getMessage(); writeReportToFile(msg);
        }
        return total;
    }
    public void writeReportToFile(String text) {
        System.out.println(text);
        text += " \n";
        if (!Files.exists(Paths.get(rootPath + "/"+filename))) {
            PrintWriter writer = null;
            try {
                System.out.println("new file name report:"+filename);
                writer = new PrintWriter(rootPath + "/"+filename, "UTF-8");
            } catch (FileNotFoundException e) {

            } catch (UnsupportedEncodingException e) {

            }
            writer.println("----<The first line >--- \n");
            writer.close();
        }
        try {
            Files.write(Paths.get(filename), text.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
            System.out.println("Eror write filereport:" + e.getMessage());
            e.printStackTrace();
        }
        msg = "";
    }
}
