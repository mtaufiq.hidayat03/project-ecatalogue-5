package jobs.instansisatker;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import jobs.JobTrail.JobInstansi1;
import models.api.DceInstansi;
import models.jcommon.config.Configuration;
import models.jcommon.http.SimpleHttpRequest;
import models.masterdata.Kldi;
import models.util.Config;
import play.Logger;
import play.jobs.Job;
import utils.KatalogUtils;

import java.io.IOException;
import java.util.*;

public class ExtInstansiData {
	private Properties propJob;
	public static final String DEFAULT_WAIT_TIME = "job.sirup.waittime";
	public static final String DEFAULT_WAIT_TIME_NEXT = "job.sirup.waittime.next";
	private String jsResult = "[]";
	private Boolean resultGet = false;
	public List<DceInstansi> instansiList = new ArrayList<>();
	private int defaultWaitTime = 3;
	private String instansiUrl = Config.getInstance().instansi_api;
	private String satkerUrl = Config.getInstance().satker_api;
	private int nextWaitTime = 0;
	private int delayTaskJob = 5;// second
	private int delayJob = 10;// second
	//
	public ExtInstansiData() {
		propJob = new Properties();
		propJob.put(DEFAULT_WAIT_TIME,
				Configuration.getConfigurationValue(DEFAULT_WAIT_TIME, ""));
		propJob.put(DEFAULT_WAIT_TIME_NEXT, Configuration
				.getConfigurationValue(DEFAULT_WAIT_TIME_NEXT, ""));

		defaultWaitTime = Integer
				.parseInt(propJob.getProperty(DEFAULT_WAIT_TIME));
		nextWaitTime = Integer
				.parseInt(propJob.getProperty(DEFAULT_WAIT_TIME_NEXT));

	}

	public List<DceInstansi> getInstansiData() {
		return getInstansiData(defaultWaitTime);
	}
	public List<DceInstansi> getInstansiData(int minuteTimeOut) {
		if (fetchInstansiData(minuteTimeOut)) {
			this.instansiList = parseInstansiData();
		}
		return this.instansiList;
	}

	private Boolean fetchInstansiData(Integer minuteTimeout) {
		resultGet = false;
		jsResult = "[]";
		Date tgl_update = new Date();
		String date = KatalogUtils.toDateyyyyMMdd(tgl_update);
		Integer seconds = 0;
		if (minuteTimeout > 0) {
			seconds = (minuteTimeout * 60);
		}
		Logger.info("run with minuteTimeout: " + minuteTimeout + ", and date "
				+ tgl_update.toLocaleString());
		try (SimpleHttpRequest connect = new SimpleHttpRequest(seconds)) {
			String params = "";
			connect.get(instansiUrl + params);
			jsResult = connect.getString();
			resultGet = true;
		} catch (InterruptedException e) {
			Logger.error("Interupted Exception:" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Logger.error("IO Exception:" + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		if (resultGet == false) {
			defaultWaitTime++;
			nextWaitTime = defaultWaitTime;
			// Configuration.updateConfigurationValue(DEFAULT_WAIT_TIME_NEXT,
			// String.valueOf(newWait));
			// propJob.replace(DEFAULT_WAIT_TIME_NEXT, String.valueOf(newWait));

			// newJob(tgl_update);
		}
		return resultGet;
	}

	private List<DceInstansi> parseInstansiData() {
		try {
			instansiList = new Gson().fromJson(jsResult,
					new TypeToken<List<DceInstansi>>() {
					}.getType());
			Logger.info("total:" + instansiList.size());
		} catch (Exception e) {
			Logger.error("error parse datasirup:" + e.getMessage());
			e.printStackTrace();
		}
		return instansiList;
	}

	public void saveDataInstansiToDb() {
		List<Kldi> failed = new ArrayList<>();
		List<Kldi> success = new ArrayList<>();
		Logger.info("save data rup:");
		for (DceInstansi s : instansiList) {
//			Instansi rupObj = Instansi.findById(s.prp_id);
			Kldi kldi = Kldi.findById(s.id);
			try {
				if (kldi != null) {
//					rupObj = s.getAsInstansi();
					kldi.prp_id = s.prp_id;
					kldi.kbp_id = s.kbp_id;
					kldi.nama = s.nama;
					kldi.jenis = s.jenis;
					kldi.website = s.website;
					kldi.is2014 = s.is2014;
					kldi.is2015 = s.is2015;
					int result = kldi.save();
					Logger.info("update:" + result);
				} else {
					kldi = new Kldi();
					kldi.id = s.id;
					kldi.prp_id = s.prp_id;
					kldi.kbp_id = s.kbp_id;
					kldi.nama = s.nama;
					kldi.jenis = s.jenis;
					kldi.website = s.website;
					kldi.is2014 = s.is2014;
					kldi.is2015 = s.is2015;
					int result = kldi.save();
					Logger.info("save:" + result);
				}
				if (kldi != null) {
					success.add(kldi);
				}
			} catch (Exception e) {
				if (kldi != null) {
					failed.add(kldi);
				}
				Logger.info("error save or update rup:" + e.getMessage());
				e.printStackTrace();
			}
		}

	}

	private void newJob() {
		JobInstansi1 job1 = new JobInstansi1();
		job1.in(delayJob);
//		new Job() {
//			String uniqueID = UUID.randomUUID().toString();
//			@Override
//			public void before() {
//				Logger.info("job id:" + uniqueID);
//				super.before();
//			}
//
//			public void doJob() {
//				jalankan();
//			}
//		}.in(delayJob);
	}

	public void jalankan() {
		Logger.info("on job execute, waittime:" + nextWaitTime);
		//getInstansiData(nextWaitTime);
		Logger.info("%%%%done getDate, saving now%%%");
		saveDataInstansiToDb();
		Logger.info("done job:" + instansiList.size());
	}
}
