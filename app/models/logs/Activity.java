package models.logs;

import javax.persistence.EnumType;

import play.db.jdbc.Enumerated;

@Enumerated(EnumType.STRING)
public enum Activity {
	LOGIN, LOGOUT, SAVE_DATA;
}
