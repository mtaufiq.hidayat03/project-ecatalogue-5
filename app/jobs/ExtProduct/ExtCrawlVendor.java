package jobs.ExtProduct;

import com.google.gson.Gson;
import models.api.produk.HistoryDatafeed;
import models.api.produk.ProdukApiInfo;
import models.api.produk.detail.image.Item;
import models.jcommon.config.Configuration;
import models.jcommon.http.SimpleHttpRequest;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.katalog.*;
import models.katalog.komoditasmodel.KomoditasAtributHarga;
import models.util.Config;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.db.jdbc.Query;
import play.mvc.Http;

import javax.persistence.Transient;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static java.nio.charset.StandardCharsets.UTF_8;

public class ExtCrawlVendor {
    public ProdukApiInfo jsParsed ;
    private String jsResult = "{}";
    public PenyediaKomoditasApi penyediaKomoditasApi;
    public List<models.api.produk.Produk> failed = new ArrayList<>();
    public List<models.api.produk.Produk> success = new ArrayList<>();
    public List<models.api.produk.Produk> updated = new ArrayList<>();

    public void CrawlVendor(long komoditas_id, long penyedia_id){
        ExtProduct ext = new ExtProduct();
        //mengambil data penyedia komoditas url
        List<PenyediaKomoditasApi> list = ext.getUrlPenyedia(komoditas_id, penyedia_id);
        HistoryDatafeed hdf = new HistoryDatafeed();
        for (PenyediaKomoditasApi pk: list) {
            Logger.info(">>> nama penyedia : "+pk.nama_penyedia);
            UUID id = UUID.randomUUID();
            //inisiasi input history datafeed
            hdf.id_job = id.toString();
            hdf.penyedia_id = pk.penyediaid;
            hdf.penyedia_name = pk.nama_penyedia;
            hdf.start_time = new Timestamp(new Date().getTime()).toString();
            hdf.save();
            Logger.info(">>> job id :" + id.toString());

            File file = createOrAddFileLog(hdf);
            Logger.info(file.getPath());
            String Startcontent = hdf.penyedia_id+", "+hdf.penyedia_name+", "+hdf.start_time;
            writeNioFile(file, Startcontent);

            executeOne(pk,hdf);
        }
    }

    public void executeOne(PenyediaKomoditasApi pk , HistoryDatafeed hdf){
        try {
            URL urls = new URL(pk.produk_api_url);
            getDatas(pk, hdf);
        }catch (MalformedURLException e) {
            Logger.error(">>> not valid url penyedia:"+ pk.produk_api_url+" "+pk.start_date);
        }
    }

    public Boolean getDatas(PenyediaKomoditasApi pk, HistoryDatafeed hdf){
        Boolean resultGet = false;
        boolean resultSave = false;
        int total_page = 1;
        int per_page = 500;

        jsResult = "{}";

        for (int page = 1; page <= total_page; page++) {
            String urls = fixingurl(pk, page, per_page);
            Logger.info(">>> run with url : " + urls);

            try {
                //melakukan koneksi url data penyedia
                SimpleHttpRequest connect = new SimpleHttpRequest(60);
                connect.get(urls);
                jsResult = connect.getString();
                //log penarikan data url

                File file = createOrAddFileLog(hdf);
                String Startcontent = pk.komoditasid+", "+ pk.kode_komoditas+", ResponStatus:"+connect.getStatus()+", "+pk.produk_api_url;
                writeNioFile(file, Startcontent);

                resultGet = true;
                if (resultGet) {
                    ProdukApiInfo objResult = parseAndGetData();
                    total_page = objResult.total_page;
                    if (objResult != null && null != objResult.total && objResult.total > 0) {
                        SaveProses(objResult, pk);
                        if (hdf.message == null) {
                            hdf.total_product = objResult.total;
                            hdf.message = "URL : " + urls + "\n" + "\n";
                            hdf.total_inserted = this.success.size();
                            hdf.total_updated = this.updated.size();
                            hdf.total_failed = this.failed.size();
                        } else {
                            hdf.message = hdf.message + "URL : " + urls + "\n" + "\n";
                            hdf.total_inserted = hdf.total_inserted + this.success.size();
                            hdf.total_updated = hdf.total_updated + this.updated.size();
                            hdf.total_failed = hdf.total_failed + this.failed.size();
                        }
                    } else {
                        // message error
                        hdf.message = "URL : " + urls + " was not accessible\n";
                    }
                }
            } catch (InterruptedException e) {
                Logger.error("Interupted Exception:" + e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Logger.error("IO Exception:" + e.getMessage());
                e.printStackTrace();
            } catch (Exception e) {
                Logger.error("Exception:" + e.getMessage());

            }
        }

        HistoryDatafeed hisDf = HistoryDatafeed.find("id_job like ?",hdf.id_job).first();
        hisDf.message = hdf.message;
        hisDf.total_product = hdf.total_product;
        hisDf.total_inserted = hdf.total_inserted;
        hisDf.total_updated = hdf.total_updated;
        hisDf.total_failed = hdf.total_failed;
        hisDf.end_time = hdf.end_time;
        hisDf.save();
        return resultGet;
    }

    private ProdukApiInfo parseAndGetData(){
        try{
            jsParsed = new Gson().fromJson(jsResult, ProdukApiInfo.class);
        }catch (Exception e){
            jsParsed = null;
            Logger.error("error parse data:"+e.getMessage());
            e.printStackTrace();
        }
        return jsParsed;
    }

    public String fixingurl(PenyediaKomoditasApi pk, Integer next, Integer perpage){
        String urls = pk.produk_api_url;
        try {
//            URL newurl = new URL(urls);
//            if(null != newurl.getQuery()){
//                urls = urls.replace(newurl.getQuery(),"");
//            }
            if(urls.indexOf("?") < 0){
                urls = urls+"?";
            }else{
                urls = urls+"&";
            }

//            Map<String, List<String>> dturl = splitQuery(newurl);
            String pattern = "yyyy-MM-dd";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

            urls = urls+"start_date="+  simpleDateFormat.format(pk.start_date);
            urls = urls+"&end_date="+ simpleDateFormat.format(new Date());

//            if(dturl.get("secretkey") != null){
//                if(dturl.get("secretkey").size()>0){
//                    urls = urls+"&secretkey="+dturl.get("secretkey").get(0);
//                }
//            }
            if(null != next){
                urls = urls+"&page="+next;
            }
            if(null != perpage){
                urls = urls+"&per_page="+ perpage;
            }
        }catch (Exception e){
            Logger.info("failed fixing urls:"+e.getMessage());
        }
        return urls;
    }

    public void SaveProses(ProdukApiInfo objResult, PenyediaKomoditasApi pk){
        //saving produk
        boolean result = true;
        String msgFailed = "<p>total produk failed:</p>";
        String skus = "<br/><p>";
        for (models.api.produk.Produk produkAPi: objResult.produk) {
            try{
                Produk newp = new Produk();
                //cek existing produk
                Produk existP = Produk.getCurentProdukForCrawl(produkAPi, pk);

                if(existP == null){//no existing produk, create new
                    //set default fields
                    newp = produkInfoToPrroduk(newp,produkAPi,pk);

                    if(null != newp.unit_pengukuran_id && null != newp.unspsc_id && null != newp.tkdn_produk && StringUtils.isAlphanumeric(newp.unspsc_id.toString())){
                        //save Produk
                        newp.saveProduct();
                        Logger.info("save new produk:"+newp.nama_produk);
                        //save riwayat
                        saveProdukRiwayat(newp,"produk_riwayat_datafeed_insert");
                        success.add(produkAPi);

                        //pastinya null karena new produk, begitu juga gambar dan lampiran
                        ProdukHargaNasional harga = null;
                        //update or add harga
                        updateOrAddHarga(harga,newp,produkAPi,pk);

                        //update or add gambar;
                        ProdukGambar gambar = null;
                        updateOrAddGambar(gambar,produkAPi, newp);

                        //update or add lampiran
                        ProdukLampiran lampiran = null;
                        updateOrAddLAmpiran(lampiran,produkAPi, newp);
                    }else{
                        if(newp.unit_pengukuran_id == null){
                            Logger.info(newp.no_produk_penyedia + " tidak memiliki unit pengukuran");
                        } else if(newp.unspsc_id == null){
                            Logger.info(newp.no_produk_penyedia + " tidak memiliki unspsc_id");
                        } else if(!StringUtils.isAlphanumeric(newp.unspsc_id.toString())){
                            Logger.info(newp.no_produk_penyedia + " unspsc_id bukan integer");
                        } else if(newp.tkdn_produk == null){
                            Logger.info(newp.no_produk_penyedia + " tidak memiliki nilai tkdn");
                        }
                    }
                }else{//exisiting ada, update
                    //replace newP with newp from DB
                    newp = existP;

                    if(compareDates(newp.modified_date, produkAPi.informasi.getTglUpdateAsDate()) ||
                            compareDates(newp.modified_date, produkAPi.spesifikasi.getTglUpdateAsDate())){
                        //updatefield
                        newp = produkInfoToPrroduk(newp,produkAPi,pk);
                        //update
                        newp.save();
                        Logger.info(">>> update produk : "+newp.nama_produk+" "+newp.id);
                        //save riwayat
                        saveProdukRiwayat(newp,"produk_riwayat_datafeed_update");
                        updated.add(produkAPi);
                    }else{
                        Logger.info(">>> not save update produk:"+newp.nama_produk+" "+newp.id);
                    }

                    //catatan: bila tidak ada produk harga nasional yang aktif maka dianggap true
                    ProdukHargaNasional harga = ProdukHargaNasional.getAktifByProdukId(newp.id);

                    //update or add harga
                    updateOrAddHarga(harga,newp,produkAPi,pk);

                    //update or add gambar;
                    ProdukGambar gambar = ProdukGambar.getAktifByProdukId(newp.id);
                    updateOrAddGambar(gambar,produkAPi, newp);

                    //update or add lampiran
                    ProdukLampiran lampiran = ProdukLampiran.getAktifByProdukId(newp.id);
                    updateOrAddLAmpiran(lampiran,produkAPi, newp);
                }
            }catch (Exception e){
                Logger.error("error saving produk, report to penyedia:"+e.getMessage());
                //mail to penyedia
                failed.add(produkAPi);
                skus += produkAPi.informasi.no_produk_penyedia+"; ";
                sendEmailReportToAdmin(e.toString(),"crawl produk penyedia error");
            }
        }
        if(failed.size()>0){
            skus += "</p>";
            if(!Config.getInstance().prodDev){
                msgFailed+=failed.size();
                //sendEmailReport(this.penyediaKomoditasApi.email,msgFailed,"Report failed produk save");
            }
            sendEmailReportToAdmin(msgFailed+skus,"Report failed produk save");
        }
    }

    public static boolean compareDates(Date modified_date, Date tanggal_update) {
        if(modified_date.compareTo(tanggal_update) < 0 || modified_date.compareTo(tanggal_update) == 0){
            return true;
        }else{
            return false;
        }
    }

    public Produk produkInfoToPrroduk (Produk newp, models.api.produk.Produk produkAPi, PenyediaKomoditasApi penyediaKomoditasApi){
        newp.komoditas_id = penyediaKomoditasApi.komoditasid.intValue();
        newp.produk_kategori_id = produkAPi.informasi.id_kategori_produk_lkpp.intValue();
        newp.penyedia_id = penyediaKomoditasApi.penyediaid;
        newp.manufaktur_id = produkAPi.informasi.id_manufaktur;
        newp.unspsc_id = produkAPi.informasi.unspsc;
        newp.unit_pengukuran_id = produkAPi.informasi.id_unit_pengukuran_lkpp;
        newp.no_produk_penyedia = produkAPi.informasi.no_produk_penyedia;
        newp.nama_produk = produkAPi.informasi.nama_produk;
        newp.jenis_produk = produkAPi.informasi.apakah_produk_lokal==1?"lokal":"impor" ;
        newp.produk_gambar_thumb_url = produkAPi.informasi.image_100x100;
        newp.url_produk = produkAPi.informasi.url_produk;
        newp.jumlah_stok = new BigDecimal(produkAPi.informasi.kuantitas_stok);
        newp.jumlah_stok_inden = BigInteger.valueOf(0);
        newp.setuju_tolak = "setuju";
        newp.setuju_tolak_tanggal = new Date();
        newp.harga_utama = new BigDecimal(produkAPi.harga.harga_pemerintah);
        newp.harga_ongkir = new BigDecimal(produkAPi.harga.ongkos_kirim);
        newp.harga_tanggal = produkAPi.harga.getHargaTanggalAsDate();
        newp.harga_kurs_id = produkAPi.harga.kurs_id;
        Double margin = (((produkAPi.harga.harga_retail - produkAPi.harga.harga_pemerintah)/produkAPi.harga.harga_retail)*100);
        newp.margin_harga = margin;
        newp.agr_total_gambar = 0;// refer to ProductForm.getTotalPicture();
        newp.agr_total_lampiran = 0;// refer to ProductForm.getTotalAttachment();
        newp.agr_total_wilayah_jual=0;// refer to ProductForm.getAgrWilayahJual();
        newp.apakah_ditayangkan = true;
        newp.apakah_dapat_dibeli = true;
        newp.apakah_dapat_diedit = 0;
        newp.active = produkAPi.informasi.produk_aktif == 1? true:false;
        newp.created_by = -99;
        newp.modified_date = new Timestamp(System.currentTimeMillis());
        newp.modified_by = -99;
        newp.berlaku_sampai = produkAPi.informasi.getBerlakuSampaiAsDate();
        newp.spesifikasi = new Gson().toJson(produkAPi.spesifikasi.item);
        newp.image_50x50 = produkAPi.informasi.image_50x50;
        newp.image_100x100 = produkAPi.informasi.image_100x100;
        newp.image_300x300 = produkAPi.informasi.image_300x300;
        newp.image_800x800 = produkAPi.informasi.image_800x800;
        newp.penawaran_id = produkAPi.informasi.id_penawaran_lkpp.intValue();
        newp.tkdn_produk = produkAPi.informasi.tkdn_produk;
        //set no_produk
        newp.setProductNumber(penyediaKomoditasApi.kode_komoditas, produkAPi.informasi.unspsc);
        return newp;
    }

    private void saveProdukRiwayat(Produk newp, String aksi){
        ProdukRiwayat pr = new ProdukRiwayat();
        pr.produk_id = newp.id;
        pr.aksi = aksi;
        pr.deskripsi = "";
        pr.created_by = -99;
        pr.created_date = new Timestamp(new Date().getTime());
        pr.save();
        //Logger.info("Produk riwayat save");
    }

    public void updateOrAddHarga(ProdukHargaNasional harga, Produk newp, models.api.produk.Produk produkAPi, PenyediaKomoditasApi pk){
        if(null == harga || compareDates(harga.modified_date, produkAPi.harga.getTglUpdateAsTimestamp())){
            int countphn = Query.update("update produk_harga_nasional set active = 0 where produk_id = ? AND active = 1",newp.id);
            int countph = Query.update("update produk_harga set active = 0 where produk_id = ? AND active = 1",newp.id);
            if(countphn > 0 || countph > 0){
                Logger.info(">>> HARGA "+ newp.no_produk_penyedia + " set non active");
                saveProdukRiwayat(newp,"produk_riwayat_datafeed_update_harga");
            }

            //create harga retail,pemerintah,ongkir
            createHargaNasional(newp,produkAPi,pk);

            //update produk - harga utama dan harga ongkir
            newp.harga_utama = new BigDecimal(produkAPi.harga.harga_pemerintah);
            newp.harga_ongkir = new BigDecimal(produkAPi.harga.ongkos_kirim);
            newp.harga_kurs_id = produkAPi.harga.kurs_id;
            newp.harga_tanggal = produkAPi.harga.getHargaTanggalAsDate();
            newp.margin_harga = (((produkAPi.harga.harga_retail - produkAPi.harga.harga_pemerintah)/produkAPi.harga.harga_retail)*100);

            newp.save();
        }
    }

    public void updateOrAddGambar(ProdukGambar gambar, models.api.produk.Produk produkAPi, Produk newp){
        //catatan gambar: bila tidak ada produk gambar yang aktif maka dianggap true
        if(null == gambar || compareDates(gambar.modified_date, produkAPi.image.getTglUpdateAsTimestamp())){
            int countphn = Query.update("update produk_gambar set active = 0 where produk_id = ? AND active = 1",newp.id);
            if(countphn > 0){
                Logger.info(">>> GAMBAR "+ newp.no_produk_penyedia + " set non active");
                saveProdukRiwayat(newp,"produk_riwayat_datafeed_update_gambar");
            }

            //$isGambarUpdated
            int posisi = 1;
            ProdukGambar pgb = null;
            //save
            List<ProdukGambar> dtGambar = new ArrayList<>();
            for (Item item:produkAPi.image.item) {
                pgb = new ProdukGambar();
                pgb.produk_id = newp.id;
                pgb.deskripsi = item.deskripsi;
                String filename ="";
                try {
                    int awal = item.image_300x300.lastIndexOf("/");
                    int akhir = item.image_300x300.length();
                    int lengthfile = (akhir - awal);
                    filename = item.image_300x300.substring(awal,lengthfile);
                }catch (Exception e){}
                pgb.file_name = filename;
                pgb.original_file_name = filename;
                pgb.file_url = item.image_300x300;
                pgb.thumb_url= item.image_50x50;
                pgb.posisi_file = posisi++;
                pgb.active = 1;
                pgb.created_by=-99;
                dtGambar.add(pgb);
            }
            //save all produk gambar
            ProdukGambar.saveAll(dtGambar);
            Logger.info(">>> GAMBAR "+ newp.no_produk_penyedia + " was updated");

            newp.agr_total_gambar = posisi-1;
            newp.save();
        }
    }

    public void updateOrAddLAmpiran(ProdukLampiran lampiran, models.api.produk.Produk produkAPi, Produk newp){
        //catatan: bila tidak ada produk lampiran yang aktif maka dianggap lanjut
        if(null == lampiran || (null != produkAPi.lampiran && compareDates(lampiran.modified_date, produkAPi.lampiran.getTglUpdateAsTimestamp()))){
            int countphn = Query.update("update produk_lampiran set active = 0 where produk_id = ? AND active = 1",newp.id);
            if(countphn > 0){
                Logger.info(">>> LAMPIRAN "+ newp.no_produk_penyedia + " set non active");
                saveProdukRiwayat(newp,"produk_riwayat_datafeed_update_lampiran");
            }

            //$isLampiranUpdated = true;
            int posisi = 1;
            List<ProdukLampiran> dtlmp = new ArrayList<>();
            ProdukLampiran plm = null;
            for (models.api.produk.detail.lampiran.Item item:produkAPi.lampiran.item) {
                plm =  new ProdukLampiran();
                plm.produk_id = newp.id.intValue();
                plm.deskripsi = item.deskripsi;
                String filename ="";
                try {
                    int awal = item.file.lastIndexOf("/");
                    int akhir = item.file.length();
                    int lengthfile = (akhir - awal);
                    filename = item.file.substring(awal,lengthfile);
                }catch (Exception e){}
                plm.file_name = filename;
                plm.original_file_name = filename;
                plm.file_url = item.file;
                plm.posisi_file = posisi++;
                plm.active = 1;
                plm.created_by=-99;
                dtlmp.add(plm);
            }
            ProdukLampiran.saveAll(dtlmp);
            Logger.info(">>> LAMPIRAN "+ newp.no_produk_penyedia + " was updated");

            newp.agr_total_lampiran = posisi-1;
            newp.save();
        }
    }

    private void createHargaNasional(Produk newp,models.api.produk.Produk produkAPi, PenyediaKomoditasApi pk){
        //createHarga retail
        List<ProdukHargaNasional> data = new ArrayList<>();

        ProdukHargaNasional retail = new ProdukHargaNasional(newp.id,produkAPi.harga.kurs_id, produkAPi.harga.harga_retail,pk.harga_retail);
        retail.harga_tanggal = produkAPi.harga.getHargaTanggalAsDate();
        retail.active=true;
        retail.created_by=-99;
        data.add(retail);

        ProdukHargaNasional pemerintah = new ProdukHargaNasional(newp.id,produkAPi.harga.kurs_id, produkAPi.harga.harga_pemerintah,pk.harga_pemerintah);
        pemerintah.harga_tanggal = produkAPi.harga.getHargaTanggalAsDate();
        pemerintah.active=true;
        pemerintah.created_by=-99;
        data.add(pemerintah);

        ProdukHargaNasional ongkir = new ProdukHargaNasional(newp.id,produkAPi.harga.kurs_id, produkAPi.harga.ongkos_kirim,pk.harga_ongkir);
        ongkir.harga_tanggal = produkAPi.harga.getHargaTanggalAsDate();
        ongkir.active=true;
        ongkir.created_by=-99;
        data.add(ongkir);

        //saveAll
        ProdukHargaNasional.saveAll(data);
        //Logger.info("create Harga Nasional");

        Komoditas komo = Komoditas.findById(pk.komoditasid);
        List<KomoditasAtributHarga> kah = KomoditasAtributHarga.findByKomoditas(pk.komoditasid);
        for (KomoditasAtributHarga kh : kah) {
            List<ProdukHargaNasional> pha = ProdukHargaNasional.getByProductIdAndAtributid(newp.id, kh.id);
            for (ProdukHargaNasional phn : pha){
                ProdukHarga ph = ProdukHarga.getByProductIdAndAtributidAndPriceDate(newp.id, kh.id, (phn.harga_tanggal).toString());
                if(null == ph) {
                    ph = new ProdukHarga();
                    ph.produk_id = newp.id;
                    ph.kurs_id = phn.kurs_id;
                    ph.tanggal_dibuat = phn.harga_tanggal;
                    ph.apakah_disetujui = phn.approved;
                    ph.tanggal_disetujui = phn.approved_date;
                    ph.tanggal_ditolak = null;
                    ph.created_by = phn.created_by;
                    ph.created_date = phn.created_date;
                    ph.modified_by = phn.modified_by;
                    ph.modified_date = phn.modified_date;
                    ph.deleted_by = phn.deleted_by;
                    ph.deleted_date = phn.deleted_date;
                    ph.active = phn.active;
                    ph.komoditas_harga_atribut_id = phn.komoditas_harga_atribut_id;
                    ph.kelas_harga = komo.kelas_harga;
                    ph.setuju_tolak_oleh = phn.approved_by;
                }else{
                    ph.modified_date = phn.modified_date;
                }

                jobs.Migration.HargaProdukNasionalMigration hpm = new jobs.Migration.HargaProdukNasionalMigration();
                hpm.harga = new BigDecimal(phn.harga);
                ph.harga = new Gson().toJson(hpm);
                ph.save();
            }
        }
    }

    public void sendEmailReportToAdmin(String body, String title){
        Logger.info("sending report failed save produk:");
        String report = Configuration.getConfigurationValue(MailQueue.SMTP_MAIL_ADMIN, "");
        String[] ary = report.split(";");
        for (String to : ary) {
            MailQueue mq = new MailQueue();
            try {
                Logger.info(to+" "+body+" "+title);
                mq = EmailManager.createEmail(to, body, title);
            } catch (Exception e) {
                Logger.error("error create mail:"+e.getMessage());
            }
            Logger.info(mq.save()+"");
        }
    }

    public File createOrAddFileLog(HistoryDatafeed hdf){
        String rootPath = conf.dataFeedLogPath+"/";
        String dateDir = createSubDirDate(rootPath);
        String fileNamePath = dateDir+"/"+hdf.id_job+"-"+hdf.penyedia_id+".txt";
        return new File(fileNamePath);
    }

    public static String createSubDirDate(String rootDir){
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        String strDatePath = rootDir+formatter.format(date);
        File fdir = new File(strDatePath+"/datafeed/");
        if(!fdir.exists()){
            try{
                Files.createDirectories(Paths.get(fdir.getPath()));
            }catch (IOException e){}
        }
        return fdir.getPath();
    }

    private static void writeNioFile(File fileLog, String content){
        if(!fileLog.exists()){
            try {
                Files.write(Paths.get(fileLog.getPath()), (content + System.lineSeparator()).getBytes(UTF_8), StandardOpenOption.CREATE,StandardOpenOption.APPEND);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            try {
                Files.write(Paths.get(fileLog.getPath()), (content + System.lineSeparator()).getBytes(UTF_8), StandardOpenOption.WRITE,StandardOpenOption.APPEND);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    @Transient
    public static Config conf=Config.getInstance();
}
