package Migration;

import models.common.OngkosKirim;

import java.math.BigDecimal;

public class OngkosKirimRquery {
    //public Long id;
    public Long provinsiId;
    public String provinsi;
    public Long kabupatenId;
    public String kabupaten;
    public Double ongkir;
}
