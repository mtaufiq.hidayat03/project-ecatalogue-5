package jobs.JobTrail;

import jobs.ExtProduct.ExtProduct;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;

import java.util.UUID;

@NoTransaction
public class ExtProdukJob5 extends Job {
    UUID id = UUID.randomUUID();
    ExtProduct ext = new ExtProduct();

    public ExtProdukJob5(ExtProduct _ext){
        ext = _ext;
    }

    @Override
    public void doJob() {
        ext.getData(ext.nextWaitime, ext.urlForExecutejob, ext.historyDatafeed);
    }

    @Override
    public void before(){
        Logger.info("retry failed proses job, new job id:"+id);
        Logger.info("penyedia:"+ext.penyediaKomoditasApi.nama_penyedia);
        super.before();
    }
    @Override
    public void after() {
        //after finish send mail
        Logger.info(ext.penyediaKomoditasApi.nama_penyedia+" job is done, reporting failed proses on job id:"+id);
    }
}
