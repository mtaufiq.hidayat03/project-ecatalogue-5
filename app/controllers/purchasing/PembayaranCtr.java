package controllers.purchasing;

import constants.paket.PembayaranConstant;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.jcommon.blob.BlobTable;
import models.purchasing.Paket;
import models.purchasing.pembayaran.Pembayaran;
import models.purchasing.pembayaran.PembayaranLampiran;
import models.purchasing.pembayaran.form.AcceptanceItemJson;
import models.purchasing.pembayaran.form.PaymentDataForm;

import models.secman.Acl;
import play.Logger;
import play.i18n.Lang;
import play.i18n.Messages;
import services.paket.PaymentService;

import java.util.List;

/**
 * @author HanusaCloud on 10/24/2017
 */
public class PembayaranCtr extends BaseController {

    public static final String TAG = "PembayaranCtr";

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PEMBAYARAN})
    public static void riwayatPembayaran(String language, Long paket_id) {
        Paket model = Paket.findById(paket_id);
        notFoundIfNull(model);
        if (model != null) {
            model.withPayments(getAktifUser());
            model.withStatusMessage(getAktifUser());
        }
        renderArgs.put("paket", model);
        renderArgs.put("activeUser", getAktifUser());
        render("purchasing/PaketCtr/pembayaran/pembayaran.html");
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PEMBAYARAN_INPUT})
    public static void createPembayaran(String language, Long paket_id) {
        Paket model = Paket.findById(paket_id);
        notFoundIfNull(model);
        if (!model.accessPayment(getAktifUser())) {
            notFound();
        }
        if (model != null) {
            model.payment = new Pembayaran();
        }
        renderArgs.put("paket", model);
        renderArgs.put("action", "create");
    	render("purchasing/PaketCtr/pembayaran/createPembayaranPpk.html");
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PEMBAYARAN})
    public static void detailPembayaran(String language, Long paket_id, Long id) {
        Paket model = Paket.findById(paket_id);
        notFoundIfNull(model);
        if (model != null) {
            model.payment = Pembayaran.findById(id);
            notFoundIfNull(model.payment);
            if (model.payment != null) {
                model.payment.withAcceptancePayments();
                model.payment.withAttachment();
            }
        }
        renderArgs.put("paket", model);
        renderArgs.put("action", "create");
        render("purchasing/PaketCtr/pembayaran/createPembayaranPpk.html");
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PEMBAYARAN_INPUT})
    public static void updatePembayaran(String language, Long paket_id, Long id) {
        Paket model = Paket.findById(paket_id);
        notFoundIfNull(model);
        if (!model.accessPayment(getAktifUser())) {
            notFound();
        }
        if (model != null) {
            model.payment = Pembayaran.findById(id);
            notFoundIfNull(model.payment);
            if (model.payment != null) {
                model.payment.withAcceptancePayments();
            }
        }
        renderArgs.put("paket", model);
        renderArgs.put("action", "update");
        render("purchasing/PaketCtr/pembayaran/createPembayaranPpk.html");
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PEMBAYARAN_INPUT})
    public static void deletePembayaran(Long paket_id, Long id) {
        final boolean isSucceed = PaymentService.instance().deletePayment(id, getAktifUser());
        if (!isSucceed) {
            flash.error(Messages.get("notif.failed.delete"));
        }
        riwayatPembayaran(Lang.get(), paket_id);
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PEMBAYARAN_INPUT})
    public static void store(Long paket_id, PaymentDataForm model) {
        checkAuthenticity();
        try {
            if (model != null) {
                model.packageId = paket_id;
            }
            //bugs pembayaran terjadi 2x, tidak mengecek apakah sudah ada
            //PaymentService.instance().store(model, PembayaranConstant.TYPE_PAYMENT);
            String message = PaymentService.instance().storeGetMessage(model, PembayaranConstant.TYPE_PAYMENT);
            flash.success(message);
        } catch (Exception e) {
            e.printStackTrace();
            flash.error(Messages.get("notif.failed.submit"));
        }
        redirect(new Paket(paket_id).getPaymentHistory());
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PEMBAYARAN_INPUT})
    public static void storeVerification(Long paket_id, PaymentDataForm model) {
        checkAuthenticity();
        try {
            if (model != null) {
                model.packageId = paket_id;
            }
            PaymentService.instance().store(model, PembayaranConstant.TYPE_PAYMENT_CONFIRMATION);
            flash.success(Messages.get("notif.success.submit"));
        } catch (Exception e) {
            e.printStackTrace();
            flash.error(Messages.get("notif.failed.submit"));
        }
        redirect(new Paket(paket_id).getPaymentHistory());
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PEMBAYARAN_INPUT})
    public static void createVerification(String language, Long paket_id) {
        Paket model = Paket.findById(paket_id);
        notFoundIfNull(model);
        if (!model.accessPaymentVerification(getAktifUser())) {
            notFound();
        }
        if (model != null) {
            model.payment = new Pembayaran();
        }
        renderArgs.put("paket", model);
        renderArgs.put("action", "create");
        render("purchasing/PaketCtr/pembayaran/createPembayaranPenyedia.html");
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PEMBAYARAN_INPUT})
    public static void updateVerification(Long paket_id, Long id) {
        Paket model = Paket.findById(paket_id);
        notFoundIfNull(model);
        if (!model.accessPaymentVerification(getAktifUser())) {
            notFound();
        }
        if (model != null) {
            model.payment = Pembayaran.findById(id);
            notFoundIfNull(model.payment);
            if (model != null) {
                model.payment.withDeliveries();
            }
        }
        renderArgs.put("paket", model);
        renderArgs.put("action", "update");
        render("purchasing/PaketCtr/pembayaran/createPembayaranPenyedia.html");
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PEMBAYARAN})
    public static void deleteVerification(Long paket_id, Long id) {
        PaymentService.instance().deletePayment(id, getAktifUser());
        redirect(new Paket(paket_id).getPaymentHistory());
    }

    //@AllowAccess({Acl.COM_PAKET_RIWAYAT_PEMBAYARAN_INPUT})
    public static void searchPengiriman(Long paket_id, String keyword, String exception) {
        Logger.debug(TAG + " search delivery");
        checkAuthenticity();
        List<AcceptanceItemJson> results = PaymentService.instance().searchDeliveryApi(paket_id, keyword, exception);
        Logger.debug( TAG + " " + paket_id + " " + keyword + " " + exception);
        renderJSON(results);
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PEMBAYARAN_INPUT})
    public static void searchPenerimaan(Long paket_id, String keyword, String exception) {
        Logger.debug(TAG + " search acceptance");
        checkAuthenticity();
        List<AcceptanceItemJson> results = PaymentService.instance().searchAcceptanceApi(paket_id, keyword, exception);
        Logger.debug(TAG + " " + paket_id + " " + keyword + " " + exception);
        renderJSON(results);
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PEMBAYARAN})
    public static void downloadLampiran(Long id) {
        PembayaranLampiran model = PembayaranLampiran.findById(id);
        if (model != null) {
            BlobTable blobTable = model.withBlobContent();
            if (blobTable != null && blobTable.getFile().exists()) {
                renderBinary(blobTable.getFile(), model.original_file_name);
            } else if (model.getFile().exists()) {
                renderBinary(model.getFile(), model.original_file_name);
            } else {
                error(Messages.get("notif.files_not_found"));
            }
        } else {
            error(Messages.get("notif.files_not_found"));
        }
    }

}
