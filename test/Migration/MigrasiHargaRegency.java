package Migration;

import com.google.gson.Gson;
import models.katalog.Komoditas;
import models.katalog.Produk;
import models.katalog.ProdukHarga;
import models.katalog.ProdukHargaKabupaten;
import models.katalog.komoditasmodel.KomoditasAtributHarga;
import play.Play;
import play.db.jdbc.Query;

import java.util.*;
import java.util.stream.Collectors;

public class MigrasiHargaRegency {
    String filename = "reportProdukHargaRegency_"+ UUID.randomUUID().toString().substring(0,5)+".txt";
    static String rootPath = Play.applicationPath.getAbsolutePath();
    String msg = "";
    public String sqlHeaderProduk = "";
    public String sqlBody = "";
    int pageLength = 50;
    //List<Produk> listProduk = new ArrayList<>();

    public MigrasiHargaRegency(){
        sqlBody = "from produk p\n" +
                "join komoditas k on p.komoditas_id = k.id\n" +
                "join produk_harga_kabupaten pp on p.id = pp.produk_id and pp.active = 1\n" +
                "join provinsi pr on pp.provinsi_id = pr.id\n" +
                "join kabupaten kb on kb.id = pp.kabupaten_id\n" +
                "JOIN penyedia pn ON p.penyedia_id = pn.id\n" +
                "JOIN penyedia_kontrak pk ON pn.id = pk.penyedia_id\n" +
                "where p.active = 1 AND p.apakah_ditayangkan = 1\n" +
                "AND p.berlaku_sampai <> '' AND p.minta_disetujui = 0 AND p.setuju_tolak = 'setuju'\n" +
                "AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1\n" +
                "AND (k.apakah_stok_unlimited = 1 OR (k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0));";
    }

    public void startMigration(String kelas_harga){
        msg=">>>>>>>>>>>>>>>>> Starting Migration Harga Regency >>>>>>>>>>>>";writeReportToFile(msg);
        int start = 0;
        int end = pageLength;

        long totalProduk =  new Long(20) ;getTotal(kelas_harga);//new Long(4493);
        msg = "total produk: "+totalProduk; writeReportToFile(msg);

        int totalPage = new Long(totalProduk/pageLength).intValue();
        int page = 1;

        while (totalProduk > start){
            List<Produk> listProduk = new ArrayList<>();
            //ambil sebanyak page data
            msg = "page "+page+" of "+totalPage+" page data";writeReportToFile(msg);
            msg = "start "+start+" end "+end;writeReportToFile(msg);
            //dataproduk perpage
            listProduk = getProdukPerPage(start,pageLength, kelas_harga);
            //lakukan koleksi data berikut untuk setiap produk
            //bikin checked location {"regency":{"1x":true,"2x":true},"province":{}}
            //bikin harga xls json
            //bikin harga
            for (Produk prd: listProduk) {
                Komoditas komo =Komoditas.findById(prd.komoditas_id);
                //data yg ada checkedLocationJson (274002,274006,274006,274002)
                //prd = Produk.findById(274002);
                List<KomoditasAtributHarga> kah = KomoditasAtributHarga.findByKomoditas(prd.komoditas_id);
                msg = "@@@@@ komoditashargaatribut produk_id:"+prd.id+" total:"+kah.size();writeReportToFile(msg);
                for (KomoditasAtributHarga kh:kah) {
                    ProdukHarga ph = ProdukHarga.getByProductIdAndAtributid(prd.id, kh.id);
                    ProdukHargaKabupaten pha = ProdukHargaKabupaten.find("produk_id=? and komoditas_harga_atribut_id=? and active=1", prd.id, kh.id).first();
                    if(null == pha){
                        pha = new ProdukHargaKabupaten();
                        pha.kurs_id = new Long(1);//IDR
                    }
                    if(null == ph){
                        msg = "new  produk harga:"+prd.id;writeReportToFile(msg);
                        ph = new ProdukHarga();
                        ph.produk_id = prd.id;
                        ph.kurs_id = pha.kurs_id;
                        ph.tanggal_dibuat = pha.harga_tanggal;
                        ph.apakah_disetujui = pha.approved;
                        ph.tanggal_disetujui = pha.approved_date;
                        ph.tanggal_ditolak = null;
                        ph.created_by = pha.created_by;
                        ph.created_date = pha.created_date;
                        ph.modified_by = pha.modified_by;
                        ph.modified_date = pha.modified_date;
                        ph.deleted_by = pha.deleted_by;
                        ph.deleted_date = pha.deleted_date;
                        ph.active = pha.active;
                        ph.komoditas_harga_atribut_id = pha.komoditas_harga_atribut_id;
                        ph.kelas_harga = komo.kelas_harga;
                        ph.setuju_tolak_oleh = pha.approved_by;
                        ph.komoditas_harga_atribut_id = pha.komoditas_harga_atribut_id;
                        ph.kurs_id = pha.kurs_id;
                    }
                    ph.checked_locations_json = createCheckedLocation(kelas_harga, prd.id);
                    ph.harga_xls_json = createJsonXls(prd.id);
                    ph.harga = createHarga(prd.id, kh.id);
                    //saving
                    //ph.save();
                    msg = "produk harga: "+new Gson().toJson(ph);writeReportToFile(msg);
                    if(null == ph.id){
                        msg = "-----> save produk harga: "+ph.produk_id;
                    }else{
                        msg = "save produk harga: "+ph.produk_id;
                    }
                    writeReportToFile(msg);
                }
                msg = "***** ENDproduk_id:"+prd.id+" total:"+kah.size();writeReportToFile(msg);
//                msg="produkid_id:"+prd.id;writeReportToFile(msg);
//                String checkedLocationJson = getCheckedLocation(kelas_harga, prd.id);
//                msg = "Checked loactionjson:"+checkedLocationJson;writeReportToFile(msg);
//                String JsonXls = createJsonXls(prd.id);
//                msg = "jsonxls: "+JsonXls; writeReportToFile(msg);


            }

            msg = "size produk:"+listProduk.size();writeReportToFile(msg);
            end+=pageLength;start+=pageLength;page++;
        }
    }

    public long getTotal(String kelas_harga){
        //produk harga kabupaten mencakup produk yang komoditas harganya nasional dan kabupaten
        //kalo provinsi ada di produk_harga_provinsi
        //intinya semua yang ada di produk_harga_provinsi dan kabupaten dimasukan secara kelompok
        //ke produk_harga, field harga dengan format json
        msg = "Get TOTAL produk";writeReportToFile(msg);
        Long total = 0L;
        try {
            String sql = "select count(distinct (p.id))\n" +
                    "from produk p\n" +
                    "join komoditas k on p.komoditas_id = k.id\n" +
                    "JOIN penyedia pn ON p.penyedia_id = pn.id\n" +
                    "JOIN penyedia_kontrak pk ON pn.id = pk.penyedia_id\n" +
                    "where p.active = 1 AND p.apakah_ditayangkan = 1\n" +
                    "AND k.kelas_harga = ? \n" +
                    "AND p.berlaku_sampai <> '' AND p.minta_disetujui = 0 AND p.setuju_tolak = 'setuju'\n" +
                    "AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1\n" +
                    "AND (k.apakah_stok_unlimited = 1 OR (k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0))";
            total = Query.count(sql, kelas_harga);
        }catch (Exception e){
            msg = "error get total: "+e.getMessage(); writeReportToFile(msg);
        }
        return total;
    }

    public void writeReportToFile(String text) {
        System.out.println(text);
//        text += " \n";
//        if (!Files.exists(Paths.get(rootPath + "/"+filename))) {
//            PrintWriter writer = null;
//            try {
//                System.out.println("new file name report:"+filename);
//                writer = new PrintWriter(rootPath + "/"+filename, "UTF-8");
//            } catch (FileNotFoundException e) {
//
//            } catch (UnsupportedEncodingException e) {
//
//            }
//            writer.println("----<The first line >--- \n");
//            writer.close();
//        }
//        try {
//            Files.write(Paths.get(filename), text.getBytes(), StandardOpenOption.APPEND);
//        } catch (IOException e) {
//            //exception handling left as an exercise for the reader
//            System.out.println("Eror write filereport:" + e.getMessage());
//            e.printStackTrace();
//        }
        msg = "";
    }

    protected List<Produk> getProdukPerPage(int start, int page, String kelas_harga){
        String sql = "select p.id, p.komoditas_id, p.nama_produk\n" +
                "from produk p\n" +
                "join komoditas k on p.komoditas_id = k.id\n" +
                "JOIN penyedia pn ON p.penyedia_id = pn.id\n" +
                "JOIN penyedia_kontrak pk ON pn.id = pk.penyedia_id\n" +
                "where p.active = 1 AND p.apakah_ditayangkan = 1\n" +
                "AND k.kelas_harga = ? \n" +
                "AND p.berlaku_sampai <> '' AND p.minta_disetujui = 0 AND p.setuju_tolak = 'setuju'\n" +
                "AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1\n" +
                "AND (k.apakah_stok_unlimited = 1 OR (k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0)) limit ?, ?";
        try{
            return Query.find(sql, Produk.class,kelas_harga, start, page).fetch();
        }catch (Exception e){
            msg = "error getProdukPerpage:"+e.getMessage();
            writeReportToFile(msg);
        }
        return new ArrayList<>();
    }

    public String createCheckedLocation(String kelas_harga, Long produk_id){
        //result = {"1":true,"2":true,"3":true,"4":true,"5":true,"6":true,"7":true,"8":true,"9":true,"10":true}
        // provinsi checked
        CheckedLocationJson clj = new CheckedLocationJson();
        String header = "select distinct(pp.provinsi_id) as id, 1 as checked\n";
        String body = "from produk p\n" +
                "\t\t\t join komoditas k on p.komoditas_id = k.id\n" +
                "\t\t\t join produk_harga_kabupaten pp on p.id = pp.produk_id and pp.active = 1\n" +
                "\t\t\t join provinsi pr on pp.provinsi_id = pr.id\n" +
                "\t\t\t join kabupaten kb on kb.id = pp.kabupaten_id\n" +
                "where p.id= ? ";

        //provins checked location
        String sql = header+body;
        List<ProvinceChecked> list =  Query.find(sql,ProvinceChecked.class, produk_id).fetch();
        Map<String, Boolean> map =
                (Map<String, Boolean>) list.stream()
                        .collect(Collectors.toMap(ProvinceChecked::getId, ProvinceChecked::getChecked));
        clj.province = map;

        //kabupaten checked
        header = "select distinct(pp.kabupaten_id) as id, 1 as checked\n";
        sql = header + body;
        list =  Query.find(sql,ProvinceChecked.class, produk_id).fetch();
        map =
                (Map<String, Boolean>) list.stream()
                        .collect(Collectors.toMap(ProvinceChecked::getId, ProvinceChecked::getChecked));
        clj.regency = map;
        return new Gson().toJson(clj);
    }

    public List<PriceJson> getPriceJsonXls(Long produk_id, Date tanggal_harga){
        Long p1 = produk_id,p2 = produk_id,p3 =produk_id;
        String query = "select distinct(kb.nama_kabupaten) as Kabupaten,pv.nama_provinsi as Provinsi, p.id as produk_id,rt.harga as HargaRetail,onk.harga as OngkosKirim, hu.harga as HargaUtama\n" +
                "from produk p\n" +
                "join komoditas k on k.id=p.komoditas_id\n" +
                "join komoditas_harga_atribut kh on kh.id = p.komoditas_id\n" +
                "join produk_harga_kabupaten kp on kp.produk_id = p.id\n" +
                "join kabupaten kb on kb.id = kp.kabupaten_id\n" +
                "join provinsi pv on pv.id = kb.provinsi_id\n" +
                "left join (select  phk.produk_id, kha1.label_harga, phk.harga as harga, phk.kabupaten_id\n" +
                "from produk_harga_kabupaten phk\n" +
                "join komoditas_harga_atribut kha1 on kha1.id=phk.komoditas_harga_atribut_id\n" +
                "where kha1.apakah_harga_retail=1 and phk.produk_id=? and phk.harga_tanggal=?)\n" +
                "rt on rt.produk_id = p.id and rt.kabupaten_id = kp.kabupaten_id\n" +
                "left join (select  phk.produk_id, kha2.label_harga, phk.harga as harga, phk.kabupaten_id\n" +
                "from produk_harga_kabupaten phk\n" +
                "join komoditas_harga_atribut kha2 on kha2.id=phk.komoditas_harga_atribut_id\n" +
                "where kha2.apakah_harga_utama=1 and phk.produk_id=? and phk.harga_tanggal=?)\n" +
                "hu on hu.produk_id = p.id and hu.kabupaten_id = kp.kabupaten_id\n" +
                "left join (select phk3.produk_id, kha3.label_harga, phk3.harga as harga, phk3.kabupaten_id\n" +
                "from produk_harga_kabupaten phk3\n" +
                "join komoditas_harga_atribut kha3 on kha3.id=phk3.komoditas_harga_atribut_id\n" +
                "where kha3.apakah_ongkir=1 and phk3.produk_id=? and phk3.harga_tanggal=?)\n" +
                "onk on onk.produk_id = p.id and onk.kabupaten_id = kp.kabupaten_id\n" +
                "where p.id=? and kp.harga_tanggal=?";
        List<PriceJson> list = Query.find(query, PriceJson.class, produk_id, tanggal_harga,produk_id, tanggal_harga,produk_id, tanggal_harga,produk_id, tanggal_harga ).fetch();
        return list;
    }

    public List<CostJson> getCostJsonXls(Long produk_id){
        String sql = "select distinct(kb.nama_kabupaten) as Kabupaten,pr.nama_provinsi as Provinsi\n" +
                "from produk p\n" +
                "\t\t\t join komoditas k on p.komoditas_id = k.id\n" +
                "\t\t\t join produk_harga_kabupaten pp on p.id = pp.produk_id\n" +
                "\t\t\t join provinsi pr on pp.provinsi_id = pr.id\n" +
                "\t\t\t join kabupaten kb on kb.id = pp.kabupaten_id\n" +
                "\n" +
                "where p.id=?";
        List<CostJson>list = Query.find(sql,CostJson.class, produk_id).fetch();
        return list;
    }
    public String createJsonXls(Long produk_id){
        HargaXlsJson hxls = new HargaXlsJson();
        hxls.cost = getCostJsonXls(produk_id);
        hxls.price = getPriceJsonXls(produk_id, new Date(""));
        return new Gson().toJson(hxls);
    }

    public String createHarga(Long produk_id, Long atribut_id){
        String query = "select distinct (kp.kabupaten_id) as kabupatenId, kb.provinsi_id as provinsiId,0.0 as ongkos_kirim,0.0 as harga_utama,\n" +
                "\tkp.harga,\n" +
                "\tkp.komoditas_harga_atribut_id\n" +
                "from produk p\n" +
                "join komoditas k on k.id=p.komoditas_id\n" +
                "join produk_harga_kabupaten kp on kp.produk_id = p.id\n" +
                "join kabupaten kb on kb.id = kp.kabupaten_id\n" +
                "where kp.produk_id = ? and kp.komoditas_harga_atribut_id=?";
        List<HargaProdukMigration> list = Query.find(query, HargaProdukMigration.class, produk_id, atribut_id).fetch();
        return new Gson().toJson(list);
    }
}
