package controllers.katalog;

import com.google.gson.JsonObject;
import controllers.BaseController;
import controllers.security.AllowAccess;
import services.produk.ProdukService;

import static models.secman.Acl.COM_PRODUK_BUKA_EDIT_FITUR;

/**
 * @author HanusaCloud on 12/22/2017
 */
public class ProductPermissionCtr extends BaseController {

    @AllowAccess(COM_PRODUK_BUKA_EDIT_FITUR)
    public static void openByProduct(Long id, String expired) {
        JsonObject jsonObject = new JsonObject();
        if (getAktifUser().isAdmin() && ProdukService.openByProduct(id, expired)) {
            jsonObject.addProperty("isSuccess", true);
            jsonObject.addProperty("message", play.i18n.Messages.get("edit_button_berhasil_dibuka.message"));
        } else {
            jsonObject.addProperty("isSuccess", false);
            jsonObject.addProperty("message", play.i18n.Messages.get("edit_button_gagal_dibuka.message"));
        }
        renderJSON(jsonObject);
    }

    @AllowAccess(COM_PRODUK_BUKA_EDIT_FITUR)
    public static void openByProvider(Long id, String expired) {
        JsonObject jsonObject = new JsonObject();
        if (getAktifUser().isAdmin() && ProdukService.openByProvider(id, expired)) {
            jsonObject.addProperty("isSuccess", true);
            jsonObject.addProperty("message", play.i18n.Messages.get("edit_button_berhasil_dibuka.message"));
        } else {
            jsonObject.addProperty("isSuccess", false);
            jsonObject.addProperty("message", play.i18n.Messages.get("edit_button_gagal_dibuka_ensure_valid.message"));
        }
        renderJSON(jsonObject);
    }

}
