UPDATE produk_gambar SET modified_date = NULL WHERE CAST(modified_date AS CHAR(20)) = '0000-00-00 00:00:00';

ALTER TABLE `produk_gambar`
ADD COLUMN `dok_hash` VARCHAR(255);

ALTER TABLE `produk_gambar`
ADD COLUMN `dok_signature` VARCHAR(255);

ALTER TABLE `produk_gambar`
ADD COLUMN `dok_id_attachment` int(11);