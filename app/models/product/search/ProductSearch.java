package models.product.search;

import ext.contracts.ParamAble;
import org.apache.http.message.BasicNameValuePair;
import play.mvc.Scope;

import java.util.ArrayList;
import java.util.List;

public class ProductSearch implements ParamAble {

    public String keyword = "";
    public long komoditas = 0;
    public String jenis_produk = "";
    public String penyedia = "";
    public String status = "";
    public String apakah_dibeli = "";
    public String apakah_ditayangkan = "";
    public List<Long> listProdukIdsIn = new ArrayList<>();
    public List<Long> listProdukIdsNotIn = new ArrayList<>();
    public Long id = null;
    public long per_page = 20;
    public int page = 0;

    public ProductSearch(Scope.Params params) {
        this.keyword = !isNull(params,"keyword") ? params.get("keyword") : "";
        this.komoditas = !isNull(params, "komoditas") ? params.get("komoditas", Long.class) : 0;
        this.jenis_produk = !isNull(params, "jenis_produk") ? params.get("jenis_produk") : "";
        this.penyedia = !isNull(params, "penyedia") ? params.get("penyedia") : "";
        this.status = !isNull(params, "status") ? params.get("status") : "";
        this.apakah_dibeli = !isNull(params, "apakah_dibeli") ? params.get("apakah_dibeli") : "";
        this.apakah_ditayangkan = !isNull(params, "apakah_ditayangkan") ? params.get("apakah_ditayangkan") : "";
        this.per_page = !isNull(params, "per_page") ? params.get("per_page", Long.class) : 20;
        this.page = getCurrentPage(params);
        this.id = !isNull(params, "id") ? params.get("id", Long.class) : Long.valueOf(0);
    }

    @Override
    public List<BasicNameValuePair> getParams() {
        List<BasicNameValuePair> results = new ArrayList<>();
        results.add(new BasicNameValuePair("keyword", this.keyword));
        results.add(new BasicNameValuePair("komoditas", String.valueOf(this.komoditas)));
        results.add(new BasicNameValuePair("jenis_produk", this.jenis_produk));
        results.add(new BasicNameValuePair("penyedia", this.penyedia));
        results.add(new BasicNameValuePair("status", this.status));
        results.add(new BasicNameValuePair("apakah_dibeli", this.apakah_dibeli));
        results.add(new BasicNameValuePair("apakah_ditayangkan", this.apakah_ditayangkan));
        results.add(new BasicNameValuePair("per_page", String.valueOf(this.per_page)));
        results.add(new BasicNameValuePair("id", String.valueOf(this.id)));
        return results;
    }

    @Override
    public int getPage() {
        return page;
    }
}
