package utils;

import models.jcommon.datatable.DatatableQuery;
import models.jcommon.datatable.DatatableResultsetHandler;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import models.jcommon.util.CommonUtil;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.sql2o.ResultSetHandler;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.db.jdbc.Query;
import play.mvc.Http.Request;
import play.mvc.Scope.Params;

import java.util.List;


public class RupDatatableQuery extends DatatableQuery {

	private StringBuilder fromTable;
	private String where="";
	private String select="";
	private String columns[];
	private Object[] params;
	private DatatableResultsetHandler<String[]> resultsetHandler;
	private String dbname="default";
	private String[] match=null;
	private String cacheDuration="1min";
	
	//sementara blm gunakan multiDB
//	private RupDatatableQuery(String dbname, DatatableResultsetHandler<String[]> resultsetHandler) {
//		this.resultsetHandler=resultsetHandler;
//		this.columns=resultsetHandler.columns;
//		this.dbname=dbname;
//	}
	
	public RupDatatableQuery(DatatableResultsetHandler<String[]> resultsetHandler) {
		super(resultsetHandler);
//		this("default", resultsetHandler);
		this.resultsetHandler=resultsetHandler;
		this.columns=resultsetHandler.columns;
		this.dbname="default";
	}

	
	/**Secara refault, jumlah totalRows di-Cache selama 1 menit.
	 * Pada kasus tertentu di mana Cache bisa menjadi masalah maka bisa dimatikan
	 */
	public RupDatatableQuery disabledCache()
	{
		cacheDuration=null;
		return this;
	}
	

	public RupDatatableQuery select(String select)
	{
		this.select="SELECT " + select;
		return this;
	}
	
	public RupDatatableQuery from(String fromTable) {
		this.fromTable=new StringBuilder(" FROM " + fromTable);
		return this;
	}
	
	public RupDatatableQuery where(String where)
	{
		this.where=" WHERE " + where;
		return this;
	}
	
	/** Untuk mysql, field mana saja yg bisa pakai match. Pastikan bahwa semua field telah di-index FULLTEXT
	 */
	public RupDatatableQuery match(String ... match)
	{
		this.match=match;
		return this;
	}
	
	public RupDatatableQuery params(Object ... params)
	{
		this.params=params;
		return this;
	}
	
	
	public JsonObject executeQuery()
	{
		return datatable(select, fromTable, columns, resultsetHandler, params);
	}
	
	private JsonObject datatable(String select, StringBuilder from, final String[] column,
			final ResultSetHandler<String[]> resultset, Object[] param) {
		from.append(where);
		Request request=Request.current.get();
		if(request==null)
			return null;
		Params params= request.params;
		// PAGING
		final Integer start = params.get("start", Integer.class);
		final Integer length = params.get("length", Integer.class);
		String sql="SELECT count(" + column[0] + ") " + from.toString();
		Logger.debug("iTotal sql = %s", sql);
		sql = "SELECT count(id) from rup r "+where.toString();
		Logger.debug("new iTotal sql = %s", sql);
//		String countSql = "select count(id) from rup r where "+whereParams.toString();
		Long iTotal =0l;
		try
		{
			/* Adanya Cache di sini sangat mempercepat Query hingga 5X lipat pada salah satu testing
			 */
			if(cacheDuration!=null)
			{
				String key="DataTableQuery" + (sql + ArrayUtils.toString(param)).hashCode();
				iTotal=Cache.get(key, Long.class);
				if(iTotal==null)
				{
					Long startLog = System.currentTimeMillis();
					iTotal = Query.count(sql, param);
					Long duration = System.currentTimeMillis() - startLog;
					Logger.debug("iTotal duration = %d ms", duration);
					Cache.set(key, iTotal, cacheDuration);
				}
			}
			else
				iTotal = Query.count(sql, param);
		}
		catch(Exception e)
		{
			Logger.error("[DataTableQuery] %s\nSQL: %s", e, sql);
			return null;
		}
		// FILTERING
		StringBuilder filter = new StringBuilder();
		String search = params.get("search[value]");
		if (!CommonUtil.isEmpty(search)) {
			//pakai LIKE
			if(match==null)
			{
				search = "%" + search.trim().toLowerCase() + "%";
				for (int i = 0; i < column.length; i++) {
					boolean searchable = Boolean.parseBoolean(params.get("columns[" + i + "][searchable]"));
					searchable = column[i].equals("lls_id") ? true : searchable;
					if (searchable) {
						filter.append(" LOWER(").append(column[i]).append(") LIKE ?");
						if (i < (column.length - 1))
							filter.append(" OR ");
						param = ArrayUtils.add(param, search);
					} 
				}
			}
			else
				//PAKAI match
			{
				filter.append("MATCH (");
				int fieldCount=0;
				for (int i = 0; i < column.length; i++) {
					boolean searchable = Boolean.parseBoolean(params.get("columns[" + i + "][searchable]"));
					searchable = column[i].equals("lls_id") ? true : searchable;
					if (searchable) {
						if(fieldCount>=1)
							filter.append(',');
						filter.append(column[i]);
						fieldCount++;
					} 
				}
				filter.append(")");
				filter.append(" AGAINST ('" + StringEscapeUtils.escapeSql(search) + "')");
			}
		}
		if (filter.length() > 0) {
			if (filter.toString().endsWith(" OR ")) {
				filter = filter.delete(filter.length() - 4, filter.length());
			}
			from.append(StringUtils.isNotEmpty(where) ? " AND " : " WHERE ");
			from.append("(").append(filter).append(")");
		}
		// INDIVIDUAL COLUMN FILTERING (tidak dipakai). Code di bawahini dicopy dari SPSE4
//		boolean searchable = false;
//		String column_search = null;
//		filter = new StringBuilder();
//		for (int i = 0; i < column.length; i++) {
//			searchable = Boolean.parseBoolean(params.get("columns[" + i + "][searchable]"));
//			column_search = params.get("columns[" + i + "][search][value]");
//			if (searchable && !StringUtils.isEmpty(column_search)) {
//				column_search = "%" + column_search.toLowerCase() + "%";
//				filter.append(" LOWER(").append(column[i]).append(") LIKE ?");
//				if (i < (column.length - 1))
//					filter.append(" OR ");
//				param = ArrayUtils.add(param, column_search);
//			}
//		}
//		if (filter.length() > 0) {
//			if (filter.toString().endsWith(" OR ")) {
//				filter = filter.delete(filter.length() - 4, filter.length());
//			}
//			from.append(StringUtils.isNotEmpty(where) ? " AND " : " WHERE ");
//			from.append("(").append(filter).append(")");
//		}
		sql="SELECT count(" + column[0] + ") " + from.toString();
		Logger.debug("iFilteredTotal sql = %s", sql);
		Long startLog = System.currentTimeMillis();
		long iFilteredTotal = Query.count(sql,  param);
		Long duration = System.currentTimeMillis() - startLog;
		Logger.debug("iFilteredTotal duration = %d ms", duration);
		// ORDERING
		String requestColumn = null;
		int columnIdx = 0;
		boolean firstOrder = true;
		for (int i = 0; i < column.length; i++) {
			requestColumn = params.get("order[" + i + "][column]");
			if (StringUtils.isEmpty(requestColumn))
				continue;
			columnIdx = Integer.parseInt(requestColumn);
			boolean orderable = Boolean.parseBoolean(params.get("columns[" + i + "][orderable]"));
			if (orderable) {
				String order_status = params.get("order[" + i + "][dir]");
				if (firstOrder) {
					from.append(" ORDER BY ").append(column[columnIdx]).append(' ').append(order_status);
					firstOrder = false;
				} else {
					from.append(',').append(column[columnIdx]).append(' ').append(order_status);
				}
			}
		}

		if (start != null && length >= 0) {
			from.append(" LIMIT ").append(start).append(",").append(length);
		}
		sql=select + " " + from.toString();

		if ("true".equals(Play.configuration.get("jpa.debugSQL")))
			Logger.debug("[DataTableCtr] SQL: %s", sql);

		try
		{
			//TODO 
			startLog = System.currentTimeMillis();
			List<String[]> data = Query.find(sql, resultset, param).fetch();
			duration = System.currentTimeMillis() - startLog;
			Logger.debug("data sql = %s ", sql);
			Logger.debug("data duration = %d ms", duration);
			JsonObject output = new JsonObject();
			output.addProperty("draw", params.get("draw"));
			output.addProperty("recordsTotal", iTotal);
			output.addProperty("recordsFiltered", iFilteredTotal);
			Gson gson = new Gson();
			output.add("data", gson.toJsonTree(data));
			return output;
		}
		catch(Exception e)
		{
			Logger.error("[DataTableModel] ERROR: %s,\nSQL: %s", e, sql);
			return null;
		}
	}


}
