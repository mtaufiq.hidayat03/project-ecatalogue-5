package models.prakatalog;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

@Table(name = "perpanjangan_kontrak")
public class Perpanjangan extends BaseKatalogTable
{
    @Id
    public Long id;

    public Long usulan_id;
    public Long penawaran_id;
    public Long kontrak_id;
    public Long penyedia_id;
    public Boolean selesai;

}
