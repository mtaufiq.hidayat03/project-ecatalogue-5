package controllers;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import models.common.AktifUser;
import models.secman.Acl;
import org.apache.commons.lang.ArrayUtils;
import play.mvc.Scope.Session;

// session user disimpan di cache, class models.common.AktifUser
@Deprecated
public class SessionInfo
{
	public SessionInfo(String nama, int role_basic_id, Long user_id, String centrum_code,
					   boolean akses_seluruh_paket, boolean akses_seluruh_komoditas,
					   List<String> roleItems, List<Long> paketList, List<Long> komoditasList, Long rkn_id) {
		super();
		this.nama_lengkap = nama;
		this.role_basic_id = role_basic_id;
		this.user_id = user_id;
		this.centrum_code = centrum_code;
		this.akses_seluruh_komoditas = akses_seluruh_komoditas;
		this.akses_seluruh_paket = akses_seluruh_paket;
		this.roleItems = roleItems;
		this.paketList = paketList;
		this.komoditasList = komoditasList;
		this.rkn_id = rkn_id;
	}

	SessionInfo(String str)
	{
		String ary[]=str.split("\n");
		nama_lengkap=ary[0];
		role_basic_id=Integer.parseInt(ary[1]);
		user_id=Long.parseLong(ary[2]);
		centrum_code=ary[3];
		akses_seluruh_komoditas=ary[4].equalsIgnoreCase("true");
		akses_seluruh_paket=ary[5].equalsIgnoreCase("true");
		roleItems=new Gson().fromJson(ary[6], new TypeToken<List<String>>(){}.getType());
		paketList=new Gson().fromJson(ary[7], new TypeToken<List<Long>>(){}.getType());
		komoditasList=new Gson().fromJson(ary[8], new TypeToken<List<Long>>(){}.getType());
		rkn_id=Long.parseLong(ary[9]);

	}

	public String nama_lengkap;
	public Integer role_basic_id;
	public Long user_id;
	public Long rkn_id;
	public String centrum_code;
	public List<String> roleItems;
	public boolean akses_seluruh_paket;
	public boolean akses_seluruh_komoditas;
	public List<Long> paketList;
	public List<Long> komoditasList;

	private String roleItemsToJson(){
		return new Gson().toJson(roleItems).toString();
	}

	private String paketListToJson(){
		return new Gson().toJson(paketList).toString();
	}

	private String produkListToJson(){
		return new Gson().toJson(komoditasList).toString();
	}

	public void saveToSession()
	{
		String str=String.format("%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s", nama_lengkap,role_basic_id, user_id,
				centrum_code, akses_seluruh_komoditas, akses_seluruh_paket,
				roleItemsToJson(), paketListToJson(), produkListToJson(), rkn_id);
		Session.current.get().put(AktifUser.USER_SESSION_ID, str);
	}

	public boolean isAuthorized(Acl[] acls){
		if(ArrayUtils.contains(acls, Acl.ALL_AUTHORIZED_USERS)){
			return true;
		}

		for (String acl : roleItems){
			if(ArrayUtils.contains(acls, acl)){
				return true;
			}
		}
		return false;
	}
}