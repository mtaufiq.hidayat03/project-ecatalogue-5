package models.katalog;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import models.BaseKatalogTable;
import models.common.OngkosKirim;
import models.jcommon.util.CommonUtil;
import models.katalog.komoditasmodel.KomoditasAtributHarga;
import models.masterdata.Kabupaten;
import models.masterdata.Kurs;
import models.masterdata.Provinsi;
import models.util.produk.*;
import org.apache.http.util.TextUtils;
import org.json.JSONException;
import play.Logger;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.DateTimeUtils;
import utils.KatalogUtils;
import utils.LogUtil;

import javax.persistence.Transient;
import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * Created by dadang on 8/24/17.
 */

@Table(name = "produk_harga")
public class ProdukHarga extends BaseKatalogTable {

	public static final String TAG = "ProdukHarga";

	@Id
	public Long id;
	public Long produk_id;
	public Long kurs_id;
	public String harga;
	public Date tanggal_dibuat;
	public boolean apakah_disetujui;
	public Date tanggal_disetujui;
	public Date tanggal_ditolak;
	public Long setuju_tolak_oleh;
	public Long komoditas_harga_atribut_id;
	public String kelas_harga;
	public boolean active;
	public String alasan_ditolak;
	public String harga_xls_json;
	public String checked_locations_json;

	public static final String KELAS_HARGA_NASIONAL = "NASIONAL";

	public static final String KELAS_HARGA_PROVINSI = "PROVINSI";

	public static final String KELAS_HARGA_KABUPATEN = "KABUPATEN";

	@Transient
	public String nama_kurs;

	@Transient
	public String nama_produk;
	@Transient
	public String nama_manufaktur;
	@Transient
	public String nama_unit_pengukuran;
	@Transient
	public String wilayah_jual;
	@Transient
	public String wilayah_provinsi;
	@Transient
	public String created_date_string;
	
	@Transient
	public boolean harga_utama;
	@Transient
	public boolean harga_pemerintah;

	public Date getTanggalDibuat(){
		return tanggal_dibuat;
	}
	/**
	 * apakah_ongkir field from komoditas_harga_atribut table
	 */
	@Transient
	public boolean apakah_ongkir;

	@Transient
	public List<Kabupaten> activeKabs = new ArrayList<>();
	@Transient
	public List<Provinsi> activeProvs = new ArrayList<>();

	@Transient
	public HargaProdukJson priceJson;
	
	class HargaListProdukJson extends ArrayList<HargaProdukJson>{}

	@Transient
	public HargaListProdukJson pricesJson;

	public void setPriceJson () {
		if (!TextUtils.isEmpty(this.harga)) {
			this.priceJson = CommonUtil.fromJson(this.harga, HargaProdukJson.class);
		}
	}

	public void setPricesJson () {
		if (!TextUtils.isEmpty(this.harga)) {
			this.pricesJson = CommonUtil.fromJson(this.harga, HargaListProdukJson.class );
		}
	}

	public List<HargaProdukJson> getPricesJson(){
		setPricesJson();
		return pricesJson;
	}

	public String getMainPriceString() {
		if (this.priceJson != null) {
			DecimalFormat format = new DecimalFormat("0.#");
			return format.format(this.priceJson.harga_utama);
		}
		return "0";
	}

	public String getKursDateString() {
		return DateTimeUtils.convertToDdMmYyyy(this.tanggal_dibuat);
	}

	public String getStandarDateString() {
		return DateTimeUtils.convertToStandardDate(this.tanggal_dibuat);
	}

	public ProdukHarga() {}


	public ProdukHarga(Long produk_id, Long komoditas_harga_atribut_id, Long kurs_id, String hargaJson, Date tanggalDibuat, String hargaXlsJson, String checkedLocations) {
		Produk produk = Produk.getProdukKomoditas(produk_id);
		this.kelas_harga = produk.kelas_harga;

		this.produk_id = produk_id;
		this.komoditas_harga_atribut_id = komoditas_harga_atribut_id;
		this.kurs_id = kurs_id;
		if (!TextUtils.isEmpty(hargaJson)) {
			this.harga = hargaJson;
		}
		this.tanggal_dibuat = tanggalDibuat;
		this.apakah_disetujui = false;
		if (!TextUtils.isEmpty(hargaXlsJson) && !hargaXlsJson.equalsIgnoreCase("{}")) {
			this.harga_xls_json = hargaXlsJson;
		}
		if (!TextUtils.isEmpty(checkedLocations) && !checkedLocations.equalsIgnoreCase("{}")) {
			this.checked_locations_json = checkedLocations;
		}
		this.active = true;
	}

	public static ProdukHarga getByProductIdAndAtributidAndPriceDate(Long produk_id, Long atribut_id, String harga_tanggal) {
		return find("produk_id = ? AND komoditas_harga_atribut_id = ? AND tanggal_dibuat like '"+ harga_tanggal +"%' AND apakah_disetujui = ? AND active = ? order by created_date desc", produk_id, atribut_id, AKTIF, AKTIF).first();
	}

	public static ProdukHarga getByProductIdAndAtributid(Long produk_id, Long atribut_id) {
		return find("produk_id =? AND komoditas_harga_atribut_id=? AND active =? order by created_date desc", produk_id, atribut_id, AKTIF).first();
	}

	public static ProdukHarga getByProductId(Long id) {
		return find("produk_id =? AND active =? order by created_date desc", id, AKTIF).first();
	}

	public static List<ProdukHarga> geListByProductIdSetuju(Long id) {
		return find("produk_id =? AND active =? AND apakah_disetujui = 1 order by created_date desc", id, AKTIF).fetch();
	}
	public static List<ProdukHarga> geListByProductId(Long id) {
		return find("produk_id =? AND active =? order by created_date desc", id, AKTIF).fetch();
	}

	public static List<ProdukHarga> geListByProductIdSetuju(Long id, Date harga_tgl) {
		return find("produk_id =? AND tanggal_dibuat = ? AND apakah_disetujui = 1 AND active =? order by created_date desc" ,id, harga_tgl, AKTIF).fetch();
	}
	public static ProdukHarga getByDateAndAtributId(String created_date, Long komoditas_harga_atribut_id){
		return find("date_format(created_date, '%Y-%m-%d %H:%i') = ? AND komoditas_harga_atribut_id = ?",created_date,komoditas_harga_atribut_id).first();
	}

	public static List<Perbandingan> getPerbandinganHarga(long produkId, Long[] attrIds){
		Map<String, List<ProdukHarga>> groupHarga = ProdukHarga.findRiwayatByProdukId(produkId).stream()
			.collect(
				Collectors.groupingBy(
					x -> x.getStandarDateString()
				)
			);
		groupHarga = new TreeMap<>(groupHarga).descendingMap();
		String[] keys = groupHarga.keySet().toArray(new String[groupHarga.keySet().size()]);

		List<ProdukHarga> hSesudah = new ArrayList<>();
		List<ProdukHarga> hSebelum = new ArrayList<>();
		if(keys.length > 0)  hSesudah = groupHarga.get(keys[0]);
		if(keys.length > 1)  hSebelum = groupHarga.get(keys[1]);

		ProdukHarga produkHarga = ProdukHarga.find("produk_id = ? order by tanggal_dibuat DESC", produkId).first();
		List<Perbandingan> perbandinganList = new ArrayList<>();
		try {
			Produk produk = Produk.getProdukKomoditas(produkId);
			if(produk.kelas_harga.equalsIgnoreCase("kabupaten")){
				List<HargaProdukJson> hargaProdukJson = new Gson().fromJson(produkHarga.harga, new TypeToken<List<HargaProdukJson>>(){}.getType());
				Map<Long, String> kabupatens = Kabupaten.findAllActive();
				for(HargaProdukJson hpj : hargaProdukJson){
					Perbandingan perbandingan = new Perbandingan();
					perbandingan.id = hpj.kabupatenId;
					perbandingan.nama = kabupatens.get(hpj.kabupatenId);

					for(Long attrId: attrIds){
						ProdukHarga ph = hSesudah.stream().filter(x -> x.komoditas_harga_atribut_id.equals(attrId)).sorted( (o1, o2) -> o2.tanggal_dibuat.compareTo(o1.tanggal_dibuat) )
							.findFirst().orElse(null);
						if(ph != null) {
							List<HargaProdukJson> hpjk = new Gson().fromJson(ph.harga,  new TypeToken<List<HargaProdukJson>>(){}.getType());
							perbandingan.hargaSesudah.put(attrId, hpjk.stream().filter(x -> x.kabupatenId.equals(hpj.kabupatenId)).findFirst().orElse(null).harga);
						}
						
						ProdukHarga phSeb = hSebelum.stream().filter(x -> x.komoditas_harga_atribut_id.equals(attrId)).sorted( (o1, o2) -> o2.tanggal_dibuat.compareTo(o1.tanggal_dibuat) )
							.findFirst().orElse(null);
						if(phSeb != null) {
							List<HargaProdukJson> hpjk = new Gson().fromJson(phSeb.harga,  new TypeToken<List<HargaProdukJson>>(){}.getType());
							perbandingan.hargaSebelum.put(attrId, hpjk.stream().filter(x -> x.kabupatenId.equals(hpj.kabupatenId)).findFirst().orElse(null).harga);
						}
					}

					perbandinganList.add(perbandingan);
				}
			} else if(produk.kelas_harga.equalsIgnoreCase("provinsi")){
				List<HargaProdukJson> hargaProdukJson = new Gson().fromJson(produkHarga.harga, new TypeToken<List<HargaProdukJson>>(){}.getType());
				Map<Long, String> provinsi = Provinsi.findAllActiveMap();
				for(HargaProdukJson hp : hargaProdukJson){
					Perbandingan perbandingan = new Perbandingan();
					perbandingan.id = hp.provinsiId;
					perbandingan.nama = provinsi.get(hp.provinsiId);

					for(Long attrId: attrIds){
						ProdukHarga ph = hSesudah.stream().filter(x -> x.komoditas_harga_atribut_id.equals(attrId)).sorted( (o1, o2) -> o2.tanggal_dibuat.compareTo(o1.tanggal_dibuat) )
							.findFirst().orElse(null);
						if(ph != null) {
							List<HargaProdukJson> hpjk = new Gson().fromJson(ph.harga,  new TypeToken<List<HargaProdukJson>>(){}.getType());
							perbandingan.hargaSesudah.put(attrId, hpjk.stream().filter(x -> x.provinsiId.equals(hp.provinsiId)).findFirst().orElse(null).harga);
						}
						
						ProdukHarga phSeb = hSebelum.stream().filter(x -> x.komoditas_harga_atribut_id.equals(attrId)).sorted( (o1, o2) -> o2.tanggal_dibuat.compareTo(o1.tanggal_dibuat) )
							.findFirst().orElse(null);
						if(phSeb != null) {
							List<HargaProdukJson> hpjk = new Gson().fromJson(phSeb.harga,  new TypeToken<List<HargaProdukJson>>(){}.getType());
							perbandingan.hargaSebelum.put(attrId, hpjk.stream().filter(x -> x.provinsiId.equals(hp.provinsiId)).findFirst().orElse(null).harga);
						}
					}

					perbandinganList.add(perbandingan);
				}
			} else if(produk.kelas_harga.equalsIgnoreCase("nasional")){
				Perbandingan perbandingan = new Perbandingan();
				perbandingan.nama = "Nasional";

				for(Long attrId: attrIds){
					ProdukHarga ph = hSesudah.stream().filter(x -> x.komoditas_harga_atribut_id.equals(attrId)).sorted( (o1, o2) -> o2.tanggal_dibuat.compareTo(o1.tanggal_dibuat) )
						.findFirst().orElse(null);
					if(ph != null) {
						HargaProdukJson hargaProdukJson = new Gson().fromJson(ph.harga, HargaProdukJson.class);
						perbandingan.hargaSesudah.put(attrId, hargaProdukJson.harga);
					}
					
					ProdukHarga phSeb = hSebelum.stream().filter(x -> x.komoditas_harga_atribut_id.equals(attrId)).sorted( (o1, o2) -> o2.tanggal_dibuat.compareTo(o1.tanggal_dibuat) )
						.findFirst().orElse(null);
					if(phSeb != null) {
						HargaProdukJson hargaProdukJson = new Gson().fromJson(phSeb.harga, HargaProdukJson.class);
						perbandingan.hargaSebelum.put(attrId, hargaProdukJson.harga);
					}
				}
				
				perbandinganList.add(perbandingan);
			}

			perbandinganList = insertHargaSebelum(perbandinganList, produkHarga.tanggal_dibuat, produkId);

		} catch (Exception e){
			e.printStackTrace();
		}

		return perbandinganList;
	}

	private static List<Perbandingan> insertHargaSebelum(List<Perbandingan> perbandinganList,
										   Date tglHargaTerbaru,
										   long produk_id){
		String date = KatalogUtils.toDateyyyyMMdd(tglHargaTerbaru);
		String querys = "produk_id = "+produk_id+" and tanggal_dibuat < '"+date+"' order by tanggal_dibuat DESC";
		ProdukHarga produkHarga = ProdukHarga.find(querys).first();
		List<Perbandingan> result = new ArrayList<>();

		if(produkHarga == null){
			result = perbandinganList;
		}else{
			try {

				Produk produk = Produk.getProdukKomoditas(produk_id);
				if(produk.kelas_harga.equalsIgnoreCase("kabupaten")){
					List<HargaKabupatenJson> hargaProdukJson = new Gson().fromJson(produkHarga.harga, new TypeToken<List<HargaKabupatenJson>>(){}.getType());
					Map<Long, String> kabupatens = Kabupaten.findAllActive();
					for(Perbandingan perbandingan : perbandinganList){
						for(HargaKabupatenJson hk : hargaProdukJson){
							if(perbandingan.id == hk.kabupatenId){
								perbandingan.hargaOngkirSebelum = hk.ongkos_kirim;
								perbandingan.hargaPemerintahSebelum = hk.harga;
								perbandingan.hargaRetailSebelum = hk.harga;
								result.add(perbandingan);
							}

						}

					}
				} else if(produk.kelas_harga.equalsIgnoreCase("provinsi")){
					List<HargaProvinsiJson> hargaProdukJson = new Gson().fromJson(produkHarga.harga, new TypeToken<List<HargaProvinsiJson>>(){}.getType());
					Map<Long, String> provinsi = Provinsi.findAllActiveMap();
					for(Perbandingan perbandingan : perbandinganList){
						for(HargaProvinsiJson hp : hargaProdukJson){
							if(perbandingan.id == hp.provinsiId){
								perbandingan.hargaOngkirSebelum = new Double(0); //TODO : Pastikan penggunaannya di purchasing karena harga ongkir sekarang ada di kabupaten
								perbandingan.hargaPemerintahSebelum = hp.harga;
								perbandingan.hargaRetailSebelum = hp.harga;
								result.add(perbandingan);
							}
						}
					}
				} else if(produk.kelas_harga.equalsIgnoreCase("nasional")){
					HargaProdukJson hargaProdukJson = new Gson().fromJson(produkHarga.harga, HargaProdukJson.class);
					for(Perbandingan perbandingan : perbandinganList){
						perbandingan.hargaOngkirSebelum = hargaProdukJson.ongkos_kirim;
						perbandingan.hargaPemerintahSebelum = hargaProdukJson.harga.doubleValue();
						perbandingan.hargaRetailSebelum = hargaProdukJson.harga.doubleValue();
						result.add(perbandingan);
					}
				}

			} catch (Exception e){
				e.printStackTrace();
			}
		}
		return result;
	}

	public static int tolakHargaProduk(long produk_id, String alasan){
		String query = "update produk_harga set apakah_disetujui = 0, tanggal_ditolak = ?, alasan_ditolak = ? " +
				"where produk_id = ?";

		return Query.update(query, new Date(), alasan, produk_id);
	}

	public static int setujuHargaProduk(long produk_id){
		String query = "update produk_harga set apakah_disetujui = 1, tanggal_disetujui = ? " +
				"where produk_id = ?";

		return Query.update(query, new Date(), produk_id);
	}

	public static Double getOngkosKirim(long produk_id, long prp_id, long kbp_id){
		ProdukHarga produkHarga = ProdukHarga.find("produk_id = ? and apakah_disetujui = ?", produk_id, 1).first();
		if(produkHarga != null){
			Produk produk = Produk.findById(produk_id);
			Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
			if(komoditas.kelas_harga.equalsIgnoreCase(ProdukHarga.KELAS_HARGA_NASIONAL)){
				OngkosKirim obj = new Gson().fromJson(produk.ongkir, OngkosKirim.class);
				return obj.ongkir.doubleValue();
			}else {
				List<OngkosKirim> listOngkir = new Gson().fromJson(produk.ongkir, new TypeToken<ArrayList<OngkosKirim>>() {
				}.getType());
				//ok.provinsi = listPropinsi.stream().filter(it -> it.id.equals(ok.provinsiId)).findFirst().orElse(new Provinsi()).nama_provinsi;
				if(komoditas.kelas_harga.equalsIgnoreCase(ProdukHarga.KELAS_HARGA_PROVINSI)){
					OngkosKirim ok = listOngkir.stream()
							.filter(it -> it.provinsiId.equals(prp_id))
							.findFirst().orElse(new OngkosKirim());
				}else if(komoditas.kelas_harga.equalsIgnoreCase(ProdukHarga.KELAS_HARGA_KABUPATEN)){
					OngkosKirim ok = listOngkir.stream()
							.filter(it -> it.provinsiId.equals(prp_id) && it.kabupatenId.equals(kbp_id))
							.findFirst().orElse(new OngkosKirim());
				}
				return new Double(0);

			}
        }
		return new Double(0);
	}

	public static Double fixGetOngkosKirim(long produk_id, long prp_id, long kbp_id){
		ProdukHarga produkHarga = ProdukHarga.find("produk_id = ?", produk_id).first();
		if(produkHarga != null){
			Produk produk = Produk.findById(produk_id);
			Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
			//Logger.info(komoditas.kelas_harga);
			//Logger.info(""+produk.komoditas_id);
				List<OngkosKirim> listOngkir = new Gson().fromJson(produk.ongkir, new TypeToken<ArrayList<OngkosKirim>>() {
				}.getType());
				OngkosKirim ok = listOngkir.stream().filter(it -> it.provinsiId.equals(prp_id) && it.kabupatenId.equals(kbp_id)).findFirst().orElse(new OngkosKirim());
				//Logger.info("ongkir = "+ok.ongkir);
				if (ok.ongkir != null) {
					return ok.ongkir.doubleValue();
				}
		}
		return new Double(0);
	}

	public static Long countByHargaAtribut(long atribut_id){
		return count("komoditas_harga_atribut_id = ? and active = ?",atribut_id, AKTIF);
	}

	public static List<ProdukHarga> findRiwayatByProdukId(long produk_id){
		String query = "Select ph.id, ph.kurs_id, ph.produk_id, ph.tanggal_dibuat, ph.komoditas_harga_atribut_id, ph.harga, k.nama_kurs, kha.apakah_ongkir " +
				"from produk_harga ph " +
				"join komoditas_harga_atribut kha on ph.komoditas_harga_atribut_id = kha.id " +
				"join kurs k on ph.kurs_id = k.id " +
				"where ph.produk_id = ? and ph.active=1 " +
				"order by ph.tanggal_dibuat DESC";

		return Query.find(query,ProdukHarga.class,produk_id).fetch();
	}

	public static List<ProdukHarga> findRiwayatAllByProdukIdAndTanggal(Long id, Date harga_tanggal){
		String query = "Select ph.id, ph.kurs_id, ph.produk_id, ph.tanggal_dibuat, ph.komoditas_harga_atribut_id, ph.harga, k.nama_kurs, kha.apakah_ongkir " +
				"from produk_harga ph " +
				"join komoditas_harga_atribut kha on ph.komoditas_harga_atribut_id = kha.id " +
				"join kurs k on ph.kurs_id = k.id " +
				"where ph.produk_id = ? and ph.active=1 and ph.apakah_disetujui=1 and ph.tanggal_dibuat = ? " +
				"order by ph.komoditas_harga_atribut_id ASC";

		return Query.find(query,ProdukHarga.class,id,harga_tanggal).fetch();
	}


	//==================================================//
	// 					MIGRASI HARGA					//
	//==================================================//

	public static void testMigrate() throws SQLException {
		List<ProdukHargaKabupaten> produkHargaKabupatens = ProdukHargaKabupaten.find("active > 0 limit 500000").fetch();

		Logger.info("[INFO JUMLAH] " + produkHargaKabupatens.size());
	}

	public static void migrasiHargaNasional() throws Exception {

		long offset = 0;
		long limit = 20;
		long maxProd = 100;//ProdukHargaNasional.count("active = 1");
		int maxThread = (int) Math.ceil(new Double(maxProd) / new Double(limit));
		Logger.info("[MIGRASI HARGA NASIONAL] - Max Thread : " + maxThread);

		ExecutorService nasionalExecutor = Executors.newFixedThreadPool(maxThread);

		while (offset < maxProd){
			long finalOffset = offset;
			nasionalExecutor.execute(new Runnable() {
				public void run() {
					try {
						List<ProdukHargaNasional> listSumber = Query.find("select * from produk_harga_nasional " +
								"where active > 0 and produk_id = ? limit ? offset ?", ProdukHargaNasional.class, 297086, limit, finalOffset).fetch();

						for(ProdukHargaNasional phn : listSumber) {
							//populate json utk harga
							JsonObject head = new JsonObject();
							BigDecimal b = null;
							if (phn.harga != null) {
								b = new BigDecimal(phn.harga.doubleValue(), MathContext.DECIMAL64);
								head.addProperty("harga", b);
							}
							//end of populate json utk harga

							ProdukHarga ph = new ProdukHarga();
							ph.produk_id = phn.produk_id == null ? null : phn.produk_id;
							ph.kurs_id = phn.kurs_id == null ? null : phn.kurs_id;
							ph.harga = head.toString();
							ph.tanggal_dibuat = phn.harga_tanggal == null ? null : phn.harga_tanggal;
							ph.apakah_disetujui = phn.approved ? false : phn.approved;
							ph.tanggal_disetujui = phn.approved_date == null ? null : phn.approved_date;
							ph.setuju_tolak_oleh = phn.approved_by == null ? null : phn.approved_by;
							ph.komoditas_harga_atribut_id = phn.komoditas_harga_atribut_id == null ? null : phn.komoditas_harga_atribut_id;
							ph.kelas_harga = KELAS_HARGA_NASIONAL;
							ph.active = phn.active;
							int id = ph.save();
							Logger.info("[NASIONAL] - Page : " + finalOffset + " ID paket : " + id);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			offset = offset + limit;
		}
		nasionalExecutor.shutdown();
	}

	public static void migrasiHargaProvinsi() throws Exception {
		try {
			long offset = 0;
			long limit = 500000;
			long maxProd = ProdukHargaProvinsi.count("active = 1");
			while(offset < maxProd){
				List<ProdukHargaProvinsi> listSumber = Query.find("select * from produk_harga_provinsi group by " +
						"produk_id, created_date, komoditas_harga_atribut_id limit ? offset ?", ProdukHargaProvinsi.class, limit, offset).fetch();
				Logger.info("[MIGRASI HARGA PROVINSI] - Terdapat " + listSumber.size() + " riwayat harga");

				for(ProdukHargaProvinsi php : listSumber){
					List<ProdukHargaProvinsi> produkHargaProvinsis = ProdukHargaProvinsi
							.find("produk_id = ? and created_date = ? and komoditas_harga_atribut_id = ?",
									php.produk_id, php.created_date, php.komoditas_harga_atribut_id).fetch();
					JsonArray array = new JsonArray();
					JsonObject head = null;
					for(ProdukHargaProvinsi p : produkHargaProvinsis){
						//populate json utk harga
						head = new JsonObject();
						BigDecimal b = new BigDecimal(p.harga.doubleValue(), MathContext.DECIMAL64);
						head.addProperty("provinsi_id", p.provinsi_id);
						head.addProperty("harga", b);
						array.add(head);
						//end of populate json utk harga
					}

					ProdukHarga ph =  new ProdukHarga();
					ph.produk_id = php.produk_id;
					ph.kurs_id = php.kurs_id;
					ph.harga = array.toString();
					ph.tanggal_dibuat = php.harga_tanggal;
					ph.apakah_disetujui = php.approved;
					ph.tanggal_disetujui = php.approved_date;
					ph.setuju_tolak_oleh = php.approved_by;
					ph.komoditas_harga_atribut_id = php.komoditas_harga_atribut_id;
					ph.kelas_harga = KELAS_HARGA_PROVINSI;
					int id = ph.save();
					Logger.info("[MIGRASI HARGA PROVINSI] - Berhasil : " + id);
				}

				offset = offset + limit;
			}
		} catch (Exception e){
			throw new Exception("[MIGRASI HARGA PROVINSI] - Failed : " + e.getMessage());
		}
	}

	public static void migrasiHargaKabupaten() throws Exception {
		try {
			long offset = 0;
			long limit = 100;
			long maxProd = ProdukHargaKabupaten.count();
			while(offset < maxProd){
				List<ProdukHargaKabupaten> listSumber = Query.find("select * from produk_harga_kabupaten group by " +
						"produk_id, created_date, komoditas_harga_atribut_id limit ? offset ?", ProdukHargaKabupaten.class, limit, offset).fetch();
				Logger.info("[MIGRASI HARGA KABUPATEN] - Terdapat " + listSumber.size() + " riwayat harga");

				for(ProdukHargaKabupaten php : listSumber){
					List<ProdukHargaKabupaten> produkHargaKabupatens = ProdukHargaKabupaten
							.find("produk_id = ? and created_date = ? and komoditas_harga_atribut_id = ?",
									php.produk_id, php.created_date, php.komoditas_harga_atribut_id).fetch();
					JsonArray array = new JsonArray();
					JsonObject head = null;
					for(ProdukHargaKabupaten p : produkHargaKabupatens){
						//populate json utk harga
						head = new JsonObject();
						BigDecimal b = new BigDecimal(p.harga.doubleValue(), MathContext.DECIMAL64);
						head.addProperty("provinsi_id", p.provinsi_id);
						head.addProperty("kabupaten_id", p.kabupaten_id);
						head.addProperty("harga", b);
						array.add(head);
						//end of populate json utk harga
					}

					ProdukHarga ph =  new ProdukHarga();
					ph.produk_id = php.produk_id;
					ph.kurs_id = php.kurs_id;
					ph.harga = array.toString();
					ph.tanggal_dibuat = php.harga_tanggal;
					ph.apakah_disetujui = php.approved;
					ph.tanggal_disetujui = php.approved_date;
					ph.setuju_tolak_oleh = php.approved_by;
					ph.komoditas_harga_atribut_id = php.komoditas_harga_atribut_id;
					ph.kelas_harga = KELAS_HARGA_KABUPATEN;
					int id = ph.save();
					Logger.info("[MIGRASI HARGA KABUPATEN] - Berhasil : " + id);
				}

				offset = offset + limit;
			}
		} catch (Exception e){
			throw new Exception("[MIGRASI HARGA KABUPATEN] - Failed : " + e.getMessage());
		}
	}

	public List<RiwayatHargaProdukJson> riwayatHarga() {
		List<RiwayatHargaProdukJson> hargaProdukJson = new Gson().fromJson(this.harga, new TypeToken<List<RiwayatHargaProdukJson>>(){}.getType());
		List<Kurs> kursList = Kurs.findAll();
//		if(null == this.nama_kurs){
//			Kurs k = kursList.stream().filter(f-> f.id.equals(this.kurs_id)).findFirst().orElse(new Kurs());
//			this.nama_kurs=k.nama_kurs;
//		}
		hargaProdukJson.stream().forEach(x -> {
			x.tanggal=this.getStandarDateString();
			x.tanggal_dibuat=this.tanggal_dibuat;
			x.kurs=this.nama_kurs;
			x.komoditas_harga_atribut_id=this.komoditas_harga_atribut_id;
			});
		return hargaProdukJson;
	}


	public static List<ProdukHarga> getHargaByPenawaran(long penawaranId){

		String query = "SELECT prdk.nama_produk, ph.produk_id, prdk.penawaran_id, ph.kurs_id, ph.harga, ph.apakah_disetujui, man.nama_manufaktur, up.nama_unit_pengukuran " +
				"FROM produk_harga ph " +
				"JOIN produk prdk ON ph.produk_id = prdk.id " +
				"JOIN manufaktur man ON prdk.manufaktur_id = man.id " +
				"JOIN unit_pengukuran up ON prdk.unit_pengukuran_id = up.id "+
				"JOIN komoditas_harga_atribut kha ON ph.komoditas_harga_atribut_id = kha.id "+
				"WHERE ph.active = 1 and prdk.active = 1 and kha.apakah_harga_utama = 1 and prdk.penawaran_id = ?";

		return Query.find(query, ProdukHarga.class, penawaranId).fetch();
	}

	public static ProdukHarga getByProdAndTglHarga(Long produk_id, Date tglHarga, Long komoditas_harga_atribut_id){
		String query = "select * from produk_harga where produk_id=? and tanggal_dibuat=? and komoditas_harga_atribut_id=? and active=1";
		return Query.find(query,ProdukHarga.class,produk_id, tglHarga, komoditas_harga_atribut_id).first();
	}

	public void setNama_kursFdb(){
		try{
			Kurs k = Kurs.findById(this.kurs_id);
			nama_kurs=k.nama_kurs;
		}catch (Exception e){
			nama_kurs = "Not Defined";
		}
	}

	public org.json.JSONObject getxlsjsonAsJsObject(){
		try {
			return  new org.json.JSONObject(harga_xls_json);
		} catch (JSONException e) {}
		return null;
	}

	public ArrayList<Object> getxlsjsonPriceObj(){
		try {
			ArrayList<Object> yourArray = new Gson().fromJson(getxlsjsonAsJsObject().get("price").toString(), new TypeToken<List<Object>>(){}.getType());

			return yourArray;
		} catch (JSONException e) {}
		return new ArrayList<>();
	}

	public List<String> getFieldJsonHargaXls(){
		org.json.JSONArray dt = null;
		try {
			dt = (org.json.JSONArray) getxlsjsonAsJsObject().get("price");
			if(dt.length()>0){
				dt =((org.json.JSONObject)dt.get(0)).names();
				ArrayList<String> arrayList =
						new Gson().fromJson(dt.toString(), new TypeToken<List<String>>(){}.getType());
				return arrayList;
			}
		} catch (JSONException e) {}
		return new ArrayList<>();
	}
	public String getAtrHargaLabel(){
		KomoditasAtributHarga atr = KomoditasAtributHarga.findById(komoditas_harga_atribut_id);
		return  atr.label_harga;
	}

	public String findProvName(Long prov_id){
		Provinsi p = activeProvs.stream().filter(f-> f.id.equals(prov_id)).findFirst().orElse(null);
		if(null != p){
			return p.nama_provinsi;
		}else{
			return "";
		}
	}
	public String findKabName(Long kab_id){
		Kabupaten p = activeKabs.stream().filter(f-> f.id.equals(kab_id)).findFirst().orElse(null);
		if(null != p){
			return p.nama_kabupaten;
		}else{
			return "";
		}
	}

	public String revertPriceToXls() {
		LogUtil.debug(TAG, "get xls price");
		if (TextUtils.isEmpty(harga_xls_json)
				|| !harga_xls_json.substring(0, 1).equalsIgnoreCase("{")) {
			LogUtil.debug(TAG, "Oops.. harga xls json seems empty or not an array");
			return "[]";
		}
		XlsPrice xlsPrice = CommonUtil.fromJson(harga_xls_json, XlsPrice.class);
		if (xlsPrice == null || xlsPrice.prices == null || xlsPrice.prices.isEmpty()) {
			LogUtil.debug(TAG, "Oops.. xlsprice is empty or null");
			return "[]";
		}
		return CommonUtil.toJson(xlsPrice.prices);
	}

}
