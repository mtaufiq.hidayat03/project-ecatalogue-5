package models.api;

import models.masterdata.Instansi;
import play.Logger;

/**
 * Created by reza on 27/08/18
 */
public class DceInstansi {
	public String id;
	public Long prp_id;
	public Long kbp_id;
	public String nama;
	public String alamat;
	public String jenis;
	public String website;
	public Boolean isIntegrasi;
	public Boolean is2014;
	public Boolean is2015;
//	public List<DceSatker> kabupatenList;


	public Instansi getAsInstansi(){
		try{
			Instansi instansi = new Instansi();
			instansi.kldi_id=this.id;
			instansi.nama=this.nama;
			instansi.prp_id=this.prp_id;
			instansi.kbp_id=this.kbp_id;

			instansi.active=true;
			return instansi;
		}catch (Exception e){
			Logger.error("error parse DceInstansi to instansi"+ e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

}
