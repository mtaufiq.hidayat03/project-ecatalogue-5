package models.elasticsearch.response.bucket;

import models.elasticsearch.response.MetaProduct;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 10/10/2017
 */
public class HitBucket {

    public List<MetaProduct> hits = new ArrayList<>();

}
