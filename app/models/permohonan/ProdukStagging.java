package models.permohonan;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import models.common.AktifUser;
import models.jcommon.blob.BlobTable;
import models.katalog.*;
import models.katalog.komoditasmodel.KomoditasAtributHarga;
import models.util.produk.ProdukHargaJson;
import play.Logger;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProdukStagging {
    public Long penyedia_id;
    public Long produk_id;
    public Long permohonan_pembaruan_id;
    public boolean approved = false;
    //public List<Object> listObjek = new ArrayList<>();
    //public Object singleObjek = null;
    public boolean info = false;
    public boolean spec = false;
    public boolean lamp = false;
    public boolean gambar = false;
    public boolean harga = false;
    public String object_json = "";
    public String class_name = "";
    public String getType(){
        if(info){
            return "info";
        }else if(spec){
            return "spec";
        } else if(lamp){
            return "lamp";
        }else if(gambar){
            return "gambar";
        }else{
            return "harga";
        }
    }
    public List<Object> getListObject(){
        ObjectMapper mapper = new ObjectMapper();
        try {
            java.util.List<Object> pp3 = mapper.readValue(this.object_json, new TypeReference<ArrayList>() {});
            return pp3;
        }catch (Exception e){
            Logger.info("getListObject error:"+e.getMessage());
        }
        return new ArrayList<>();
    }


    public List<ProdukAtributValue> getListProdukAtributValue(){
        return  createGson().fromJson(object_json, new TypeToken<List<ProdukAtributValue>>(){}.getType());
    }

    public List<ProdukLampiran> getListProdukLampiran(){
        return  createGson().fromJson(object_json, new TypeToken<List<ProdukLampiran>>(){}.getType());
    }

    public List<ProdukLampiran> withDocuments() {
        List<ProdukLampiran> attachments = getListProdukLampiran();
        if (attachments != null && !attachments.isEmpty()) {
            for (ProdukLampiran attachment : attachments) {
                if (attachment.dok_id_attachment != null) {
                    //BlobTable blob = BlobTable.find("blb_id_content = ?", attachment.dok_id_attachment).first();
                    BlobTable blob = BlobTable.find("id = ?", attachment.dok_id_attachment).first();
                    if (blob != null) {
                        attachment.original_file_name = attachment.file_name;
                        attachment.urlDownload = blob.getDownloadUrl(null);
                    }
                }
                else{
                    attachment.urlDownload = "/public/files/upload/produk_lampiran/"+attachment.file_sub_location+attachment.file_name;
                }
            }
        }
        return attachments;
    }
    public List<ProdukGambar> getListProdukImages(){
        return  createGson().fromJson(object_json, new TypeToken<List<ProdukGambar>>(){}.getType());
    }

    public Gson createGson(){
        return new GsonBuilder()
                .registerTypeAdapter(Date.class, new ISO8601DateAdapter())
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .create();
    }
// fungsi kalo ingin saveas new
//    public Produk getProdukAsNew(){
//        Produk newp = getProduk();
//        newp.created_date = new Timestamp(new Date().getTime());
//        newp.created_by = AktifUser.getActiveUserId();
//        newp.modified_by = null;
//        newp.modified_date = null;
//        newp.id = null;
//        return newp;
//    }
    public Produk getProduk(){
        try {
            return (Produk) createGson().fromJson(this.object_json, Class.forName(this.class_name));

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ProdukHargaStagging getIfProdukHargaStagging(){
        if(harga){
            ProdukHargaStagging psg = createGson().fromJson(object_json, ProdukHargaStagging.class);
            return psg;
        }
        return null;
    }

    public void applyStaggingProdukInfo(){
        if(info){
            getProduk().save();
        }
    }

    public void applyStaggingProdukHarga(){
        ProdukHargaStagging pshg = getIfProdukHargaStagging();
        Produk produk = Produk.findById(this.produk_id);
        Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
        if(null != pshg.ongkir && !pshg.ongkir.isEmpty()){
            produk.ongkir = pshg.ongkir;
            //Logger.info("apply save stagging produk->ongkir:"+pshg.ongkir);
        }
        if(null != pshg.harga_tanggal){
            produk.harga_tanggal = pshg.harga_tanggal;
            //Logger.info("apply save stagging produk->harga_tanggal:"+pshg.harga_tanggal);
        }
        if (komoditas.kelas_harga.equalsIgnoreCase("nasional")){
            for (ProdukHarga ph: pshg.produkHargas){
                KomoditasAtributHarga komoditasAtributHarga = KomoditasAtributHarga.findById(ph.komoditas_harga_atribut_id);
                if (komoditasAtributHarga.apakah_harga_utama){
                    ProdukHargaJson hargaProdukJson = new Gson().fromJson(ph.harga, ProdukHargaJson.class);
                    Double harga_utama_stagging = hargaProdukJson.harga;
                    produk.harga_utama = BigDecimal.valueOf(harga_utama_stagging);
                }
            }
        }
        //Logger.info("save stagging produk");
        produk.save();
//mesti di cek terkait produk harga yang nasional apakah ada atribut value yang tumpang tindih dengan spesifikasi
//        List<ProdukAtributValue> listOldPA = ProdukAtributValue.findByProdukId(this.produk_id);
//        List<ProdukAtributValue> listNewPA = getIfProdukHargaStagging().produkAtributValues;
//        if(listNewPA.size() > 0) {
//            //Logger.info("apply save stagging produk->ProdukAtribut:"+listNewPA.size());
//            // notactive produk Atribut value
//            for (ProdukAtributValue pa : listOldPA) {
//                pa.active = false;
//                pa.save();
//                //Logger.info("PAo:"+pa.save());
//            }
//            //save new
//            for ( ProdukAtributValue pa : listNewPA) {
//                pa.id = null;
//                pa.save();
//                //Logger.info("PAn:"+pa.save());
//            }
//        }
        List<ProdukHarga> listOldPH =  ProdukHarga.geListByProductIdSetuju(produk_id);
        List<ProdukHarga> listNewPH = getIfProdukHargaStagging().produkHargas;
        if(listNewPH.size() > 0){
            //Logger.info("apply save stagging produk->ProdukHarga:"+listNewPH.size());
            //not active produk harga
            for (ProdukHarga ph : listOldPH) {
                ph.apakah_disetujui = false;
                ph.save();
                //Logger.info("pho:"+ph.save());
            }
            //save new
            for (ProdukHarga ph : listNewPH) {
                ph.id = null;
                ph.apakah_disetujui = true;
                ph.save();
                //Logger.info("phn:"+ph.save());
            }
        }
    }

    public void applyStaggingLampiran(){
       List<ProdukLampiran> listOldLamp = ProdukLampiran.getDocumentsByProductId(produk_id);
        List<ProdukLampiran> listNewLamp = getListProdukLampiran();
        if(listNewLamp.size() >0){
            for ( ProdukLampiran pl: listOldLamp) {
                pl.active = 0;
                pl.save();
                //Logger.info("plo:"+pl.save());
            }
            for ( ProdukLampiran pl: listNewLamp) {
                pl.id = null;
                pl.save();
                //git Logger.info("pln:"+pl.save());
            }
        }
    }

    public void applyStaggingGambar(){
       List<ProdukGambar> listOld = ProdukGambar.findByProduk(produk_id);
        List<ProdukGambar> listNew = getListProdukImages();

        if(listNew.size() > 0){
            for ( ProdukGambar pg: listOld) {
                pg.active = 0;
                pg.save();
            }

            for ( ProdukGambar pg: listNew) {

                ProdukGambar saveProdGam = new ProdukGambar(this.produk_id);
                saveProdGam.produk_id = this.produk_id;
                saveProdGam.file_name = pg.file_name;
                saveProdGam.original_file_name = pg.original_file_name;
                saveProdGam.file_size = pg.file_size;
                saveProdGam.file_sub_location = pg.file_sub_location;
                saveProdGam.file_url = pg.file_url;
                saveProdGam.file_url = pg.file_url;
                saveProdGam.posisi_file = pg.posisi_file;
                saveProdGam.active = pg.active;
                saveProdGam.created_date = pg.created_date;
                saveProdGam.save();

            }
        }
    }

    //mesti di cek terkait produk harga yang nasional apakah ada atribut value yang tumpang tindih dengan spesifikasi
    public void applyStaggingSpec(){
        List<ProdukAtributValue> listOldPA = ProdukAtributValue.findByProdukId(this.produk_id);
        List<ProdukAtributValue> listNewPA = getListProdukAtributValue();
        if(listNewPA.size() > 0) {
            //Logger.info("apply save stagging produk->ProdukAtribut:"+listNewPA.size());
            // notactive produk Atribut value
            for (ProdukAtributValue pa : listOldPA) {
                pa.active = false;
                pa.save();
                //Logger.info("PAo:"+pa.save());
            }
            //save new
            for ( ProdukAtributValue pa : listNewPA) {
                pa.id = null;
                pa.save();
                //Logger.info("PAn:"+pa.save());
            }
        }
    }
}
