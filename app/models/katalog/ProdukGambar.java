package models.katalog;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import models.BaseKatalogTable;

import models.jcommon.util.CommonUtil;
import models.util.Config;
import org.apache.commons.io.FilenameUtils;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.db.jdbc.*;
import play.libs.Files;
import play.mvc.Router;
import utils.DateTimeUtils;
import utils.HtmlUtil;
import utils.KatalogUtils;

import javax.imageio.ImageIO;
import javax.persistence.Transient;

@Table(name="produk_gambar")
@CacheMerdeka
public class ProdukGambar extends BaseKatalogTable {

	@Id
	public Long id;
	public Long produk_id;
	public String file_name;
	public String original_file_name;
	public String dok_hash;
	//menyimpan file di lokal cek fungsi simpanGambarProduk
	public String file_sub_location;
	// 0 default (local di public)
	// 1 (on cdn server)
	public Integer posisi_file;
	public static final int _POSISI_FILE_LOKAL = 0;
	public static final int _POSISI_FILE_SERVER = 1;
	public static final String _imageFoldersub = "/produk_gambar/";
	public Integer active;
	public Long dok_id_attachment;
	public String file_url;
    public String thumb_url;
	public Long file_size = 0L;
	public String deskripsi;

	public ProdukGambar() {}

	public ProdukGambar(Long productId) {
		this.produk_id = productId != null ? productId : 0;
	}

	@Transient
	public static String key1 = "listgambarbyprodukid";
	public static List<ProdukGambar> findByProduk(Long produk_id)
	{
		List<ProdukGambar> result = Cache.get(key1+produk_id,List.class);
		if(null == result){
			result = find("produk_id =? AND active =? ORDER BY posisi_file", produk_id, AKTIF).fetch();
			Cache.set(key1+produk_id,result,"30mn");
		}
		return result;
	}
	
	public static ProdukGambar findFirstByProduk(Long produk_id)
	{
		return find("produk_id=? ORDER BY posisi_file", produk_id).first();
	}
	
	//dapatkan URL untuk download
	public String getUrl()
	{
		String url = "";
		//public/files/image -> 2019/09/10/xxxxx.jpg
		this.file_url = this.getLocalImageStorage()+this.file_sub_location+this.file_name;
		this.file_url = KatalogUtils.cleanForLocalPathImage(this.file_url);
		if(KatalogUtils.fileExistInPathApps(this.file_url)){
			url = this.file_url;
		}else {
			//result akan ditambahkan dengan depan cdn, contoh
			//cdnServerImage100=http://e-katalog.lkpp.go.id/public/files/image
			Logger.info("produkgambar:getUrl() else fileurl not exist in path");
			Config conf = Config.getInstance();
			String param = file_sub_location + file_name;
			url = conf.cdnServerImage300 + "/produk_gambar/" + param;
		}
		return url;
	}

	public String getUrl(boolean cdn)
	{
		String url = "";
		if(cdn) {
			Config conf = Config.getInstance();
			String param = file_sub_location + file_name;
			url = conf.cdnServerImage300 + "/produk_gambar/" + param;
			HtmlUtil.fileExistsAtUrl(url);
			Logger.info("FILE EXIST AT URL: "+HtmlUtil.fileExistsAtUrl(url)+url);
		}else{
			this.file_url = this.getLocalImageStorage()+this.file_sub_location+this.file_name;
			url = this.file_url;
		}
		return url;
	}

	public void simpanGambarProduk(File file) throws Exception {
		try {

			Date today = new Date();

			String fileExtension = FilenameUtils.getExtension(file.getName());
			String namaFile = String.valueOf(System.nanoTime()) + "." + fileExtension;
			String fileSubLocation = DateTimeUtils.formatDateToString(today,"yyyy/MM/dd/");

			try{
				// asep
				// harus -Djava.awt.headless=true
				// https://confluence.atlassian.com/confkb/could-not-initialize-class-sun-awt-image-integerinterleavedraster-when-resizing-an-image-200709986.html
				BufferedImage originalImage = ImageIO.read(file);
				int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
				BufferedImage resizeImage = resizeImage(originalImage, type, 300, 300);
				ImageIO.write(resizeImage, fileExtension, file);
			}catch (IOException e){}

			File dokumenroot = Play.applicationPath;
			String pathLocation =  getLocalImageStorage() +fileSubLocation+namaFile;
			File destination = new File(dokumenroot + pathLocation);
			Files.copy(file,destination);

			this.file_name = namaFile;
			this.original_file_name = file.getName();
			this.file_sub_location = fileSubLocation;//DateTimeUtils.formatDateToString(today,"yyyy/MM/dd/");
			this.posisi_file = _POSISI_FILE_LOKAL;
			this.file_size = destination.length();
			this.active = 1;
			long result = save();
			if (this.id == null) {
				this.id = result;
			}
		}catch (Exception e){
			Logger.debug("error upload image %s",e.getMessage());
			throw new UnsupportedOperationException(e);
		}
	}

	public void simpanGambarProdukForStagging(File file) throws Exception {
		try {

			Date today = new Date();

			String fileExtension = FilenameUtils.getExtension(file.getName());
			String namaFile = String.valueOf(System.nanoTime()) + "." + fileExtension;
			String fileSubLocation = DateTimeUtils.formatDateToString(today,"yyyy/MM/dd/");

			try{
				// asep
				// harus -Djava.awt.headless=true
				// https://confluence.atlassian.com/confkb/could-not-initialize-class-sun-awt-image-integerinterleavedraster-when-resizing-an-image-200709986.html
				BufferedImage originalImage = ImageIO.read(file);
				int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
				BufferedImage resizeImage = resizeImage(originalImage, type, 300, 300);
				ImageIO.write(resizeImage, fileExtension, file);
			}catch (IOException e){}

			File dokumenroot = Play.applicationPath;
			String pathLocation =  getLocalImageStorage() +fileSubLocation+namaFile;
			File destination = new File(dokumenroot + pathLocation);
			Files.copy(file,destination);

			this.file_name = namaFile;
			this.original_file_name = file.getName();
			this.file_sub_location = fileSubLocation;//DateTimeUtils.formatDateToString(today,"yyyy/MM/dd/");
			this.posisi_file = _POSISI_FILE_LOKAL;
			this.file_size = destination.length();
			this.active = 1;
			//tidak save karena untuk staging
//			long result = save();
//			if (this.id == null) {
//				this.id = result;
//			}
		}catch (Exception e){
			Logger.debug("error upload image %s",e.getMessage());
			throw new UnsupportedOperationException(e);
		}
	}

	public String getLocalImageStorage(){
		return Config.getInstance().fileImageGallery + _imageFoldersub;
	}
	public static void updateProdukGambar(long id, long produk_id){
		String sql = "update produk_gambar set produk_id = :produk_id where id = :id";

		Query.update(sql, produk_id, id);
	}

	private BufferedImage resizeImage(BufferedImage originalImage, int type, int width, int height){
		BufferedImage resizedImage = new BufferedImage(width, height, type);
		Graphics2D g = resizedImage.createGraphics();
		g.setComposite(AlphaComposite.Src);

		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
				RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();

		return resizedImage;
	}

	public String getActiveUrl() {
		if (!CommonUtil.isEmpty(this.file_url)) {
			return this.file_url;
		} else if(!CommonUtil.isEmpty(this.file_sub_location)) {
			return this.getUrl();
		} else {
			if(Play.mode.isProd()){
				return Play.configuration.getProperty("application.baseUrl")+this.getUrl();
			}
			return Router.getBaseUrl() + "/public/images/image-not-found.png";
		}
	}

	public static ProdukGambar getAktifByProdukId(Long produkid){
		String sql = "select pg.* from produk_gambar pg where produk_id=? and active=1 order by pg.modified_date desc";
		ProdukGambar result = Query.find(sql, ProdukGambar.class,produkid).first();
		return result;
	}
	public long getIdProduk(){
		return this.produk_id;
	}
}
