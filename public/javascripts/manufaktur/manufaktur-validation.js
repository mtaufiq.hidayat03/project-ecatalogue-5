function checkAlphanumeric(my_arr){
    for(var i=0;i<my_arr.length;i++){
        var item = my_arr[i];
        var regExp = /^[0-9a-zA-Z.-_\s]+$/;
        if(item.match(regExp) == null)
            return false;
    }
    return true;
}

function validateManufaktur(){
    var values = $("input[name='nama_manufaktur_arr']").map(function(){return $(this).val();}).get();
        if(!checkAlphanumeric(values)){
            $("#alert-manufaktur-alphanum").show();
            return false;
        }else{
            $("#alert-manufaktur-alphanum").hide();
                var str = "<tr><td><a id='removeInput' class='btn btn-danger'><i class='fa fa-trash'></i></a></td><td><input class='form-control' name='nama_manufaktur_arr'/></td></tr>";
                $("#inputForm").append(str);

        }

    return true;
}
