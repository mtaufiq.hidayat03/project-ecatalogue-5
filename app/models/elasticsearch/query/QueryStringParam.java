package models.elasticsearch.query;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.http.util.TextUtils;
import play.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class QueryStringParam {

    public static final String TAG = "QueryStringParam";

    public static final String QUERY = "query";
    public static final String FUZZY_PREFIX_LENGTH = "fuzzy_prefix_length";
    public static final String FIELDS = "fields";
    public static final String QUERY_STRING = "query_string";
    public static final String DEFAULT_OPERATOR = "default_operator";
    public static final String USE_DIS_MAX = "use_dis_max";

    public static final String OPERATOR_OR = "OR";
    public static final String OPERATOR_AND = "AND";

    private String query;
    private int fuzzyPrefixLength = 5;
    private List<String> fields = new ArrayList<>();
    private String defaultOperator = OPERATOR_OR;
    private Boolean disMaxValue = true;

    public QueryStringParam setKeyword(String keyword) {
        String resw = "+ - = && || > < ! ( ) { } [ ] ^ \" ~ * ? : \\ /".trim();
        List<Character> reserved = resw.chars().mapToObj(e -> (char)e).collect(Collectors.toList());
        List<Character> word = keyword.chars().mapToObj(e -> (char)e).collect(Collectors.toList());
        word.retainAll(reserved);
        if(word.size() > 0){
            this.query = ("*" + keyword + "/*~").replaceAll("//","");
        }else {
            this.query = "*" + keyword + "*~";
        }
        return this;
    }

    public QueryStringParam setFuzzyLength(int length) {
        if (length > 50) {
            length = 50;
            Logger.debug(TAG + " fuzzy length more than 50 set it back to 50 manually");
        }
        this.fuzzyPrefixLength = length;
        return this;
    }

    public QueryStringParam defaultOperator(String value) {
        this.defaultOperator = value;
        return this;
    }

    public QueryStringParam setFieldToSearch(String field) {
        this.fields.add(field);
        return this;
    }

    public QueryStringParam setFieldsToSearch(String[] fields) {
        this.fields.addAll(Arrays.asList(fields));
        return this;
    }

    public JsonObject buildQueryString() {
        if (TextUtils.isEmpty(query)) {
            return null;
        }
        JsonObject model = new JsonObject();
        if (!TextUtils.isEmpty(query)) {
            model.addProperty(QUERY, this.query);
        }
        model.addProperty(FUZZY_PREFIX_LENGTH, this.fuzzyPrefixLength);
        model.addProperty(DEFAULT_OPERATOR, this.defaultOperator);
        model.addProperty(USE_DIS_MAX, this.disMaxValue);
        if (!fields.isEmpty()) {
            JsonArray jsonArray = new JsonArray();
            for (String s : fields) {
                jsonArray.add(s);
            }
            model.add(FIELDS, jsonArray);
        }
        JsonObject queryString = new JsonObject();
        queryString.add(QUERY_STRING, model);
        return queryString;
    }

}
