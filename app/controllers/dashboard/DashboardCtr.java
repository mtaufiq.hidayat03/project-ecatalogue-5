package controllers.dashboard;

import java.util.ArrayList;
import java.util.List;

import controllers.BaseController;
import models.elasticsearch.SearchResult;
import models.prakatalog.Usulan;
import repositories.elasticsearch.ElasticsearchRepository;

public class DashboardCtr extends BaseController {
	
	public static void dashboardPenyedia() {
		List<Usulan> usulanList = new ArrayList<>();
		try{
			usulanList = Usulan.findAllByStatus(Usulan.STATUS_PENAWARAN_DIBUKA);
		}catch (Exception e){}
		SearchResult popular = ElasticsearchRepository.open().getPopularList();
		renderArgs.put("popularProducts", popular.aggregateProducts);
		render(usulanList);
	}
	
	public static void dashboardPpk() {
		render();
	}
	
	public static void dashboardPp() {
		render();
	}
	
	public static void dashboardPokja() {
		render();
	}

}
