package models.purchasing.negosiasi;

import models.BaseKatalogTable;
import models.purchasing.contracts.PackageHeaderDetailContract;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import utils.DateTimeUtils;

import javax.persistence.Transient;
import java.util.Date;

/**
 * @author HanusaCloud on 11/8/2017
 */
@Table(name = "paket_produk_nego_header")
public class PaketProdukHeader extends BaseKatalogTable implements PackageHeaderDetailContract {

    @Id
    public Long id;
    public Long paket_id;
    public Integer revisi;
    public Long kurs_id_from;
    public Double kurs_nilai;
    public Date kurs_tanggal;
    public Date panitia_setuju_tanggal;
    public Date penyedia_setuju_tanggal;
    public boolean penyedia_setuju = false;
    public boolean panitia_setuju = false;
    public Long penyedia_setuju_user_id;
    public Long panitia_setuju_user_id;
    public boolean active = true;

    @Transient
    public PaketProdukNegoDetail detail;

    /**
     * get latest paket produk nego header by paket_id
     * @param id paket_id*/
    public static PaketProdukHeader getLatestHeader(Long id) {
        return find("paket_id =? AND active > 0 ORDER BY created_date DESC", id).first();
    }

    public Long getProviderId() {
        return this.penyedia_setuju_user_id;
    }

    public Long getPpId() {
        return this.panitia_setuju_user_id;
    }

    public boolean isBuyerApproved() {
        return panitia_setuju;
    }

    public boolean isProviderApproved() {
        return penyedia_setuju;
    }

    public String getFormattedBuyerApprovedDate() {
        return DateTimeUtils.parseToReadableDate(panitia_setuju_tanggal);
    }

    public String getFormattedProviderApprovedDate() {
        return DateTimeUtils.parseToReadableDate(penyedia_setuju_tanggal);
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public Long getPackageId() {
        return this.paket_id;
    }

    @Override
    public void setDetail(PaketProdukNegoDetail model) {
        this.detail = model;
    }

    public String getNoteLabel() {
        if (penyedia_setuju_tanggal != null) {
            return "Panitia";
        }
        return "penyedia";
    }

    public String[] getLatestNegotiationValues() {
        String[] results = {"", ""};
        if (detail != null) {
            results[0] = detail.getFormattedTotalPrice();
            results[1] = detail.getFormattedConversionTotalPrice();
        }
        return results;
    }

}
