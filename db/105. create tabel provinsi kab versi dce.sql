create table dce_provinsi (id int(11) primary key, nama varchar(500));

create table dce_kabupaten (id int(11) primary key, dce_provinsi_id int(11) references dce_provinsi(id), nama varchar(500));