package models.common;

import java.io.Serializable;

/**
 * model untuk informasi saat generate dokumen ba hasil evaluasi
 * @author lkpp
 *
 */
public class DokumenHasilEvaluasi implements Serializable {

	public Long komoditas;

	public String namaKomoditas;

	public String namaPenyedia;

	public String nomorSurat;

	public String tanggalForm;

	public String hari;

	public String tanggal;

	public String bulan;

	public String tahun;

	public String jam;

	public String listKualifikasi;

	public String listEvaluasiAdministrasi;

	public String listEvaluasiAdministrasiTambahan;

	public String listEvaluasiHarga;

	public String listEvaluasiTeknis;

	public String hasilEvaluasi;

	public String kesimpulan;

	public String timPengembangKatalog;

	public String ttdPenyedia;

	public String[] namaKualifikasi;

	public String[] namaKualifikasiTambahan;

	public String[] hasilKualifikasi;

	public String[] hasilKualifikasiTambahan;



	public String[] namaAdministrasi;

	public String[] namaDokAdmTambahan;

	public String[] namaHarga;

	public String[] namaTeknis;

	public String[] hasilAdministrasi;

	public String[] hasilDokAdmTambahan;

	public String[] hasilHarga;

	public String[] hasilTeknis;

	public String[] namaTimKatalog;

	public String[] jabatanTimKatalog;

	public String[] namaTimPenyedia;

	public String[] jabatanTimPenyedia;

	public String[] hasilPembuktianKualifikasi;

	public String[] hasilPembuktianKualifikasiTambahan;

	public String[] hasilPembuktianAdministrasi;

	public String[] hasilPembuktianAdmTambahan;

	public String[] hasilPembuktianHarga;

	public String[] hasilPembuktianTeknis;
}
