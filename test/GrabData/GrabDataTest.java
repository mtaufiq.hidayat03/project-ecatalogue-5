package GrabData;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import jobs.ExtProduct.ExtDataProduct;
import jobs.ExtProduct.ExtProduct;
import jobs.TrackableJob;
import jobs.extdata.ExtData;
import jobs.extdata.SirupData;
import models.api.MailQueueF;
import models.api.Sirup;
import models.api.produk.Produk;
import models.api.produk.detail.spesifikasi.Item;
import models.jcommon.http.SimpleHttpRequest;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.penyedia.Penyedia;
import models.user.User;
import org.junit.Test;
import play.Logger;
import play.jobs.Job;
import play.libs.F;
import play.libs.WS;
import play.test.UnitTest;
import utils.DateTimeUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class GrabDataTest extends UnitTest {

    ////@Test
//    public void testExecuteSirupData(){
//        SirupData task = new SirupData();
//        Timestamp startSirup = task.getLatestAuditUpdate();
//        LocalDate lcStartDate = startSirup.toLocalDateTime().toLocalDate();
//        LocalDate lcEndDate = LocalDate.now();
//        for (LocalDate date = lcStartDate; date.isBefore(lcEndDate); date = date.plusDays(1))
//        {
//            Date dates = Date.from(date.atStartOfDay()
//                    .atZone(ZoneId.systemDefault())
//                    .toInstant());
//            Logger.info(">>>>>>>>>>>>>>>>>>>"+dates.toString());
//            task.executeGrabData(dates);
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//    }

//    //@Test
//    public void testExtData(){
//        ExtData ext = new ExtData();
//        Date tgl = ext.getLatestAuditUpdate();
//        //Date tgl = new Date();
//        List<Sirup> data = ext.getSyrupData(tgl);
//        try {
//                Thread.sleep(480000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//    }

    ////@Test
    public void testLoopDate(){
        ExtData ext = new ExtData();
        //ext.getSyrupData(new Date());
        //ext.saveDataToDb();
        //ext.sendEmailReport();
        //ext.getSirupBetweenDate();

        new Job(){

            public void doJob(){
                Logger.info("do job");
                try {
                    MailQueue mq =  new MailQueue();
                    mq = EmailManager.createEmail("asep.sofyan@bening-semesta.com","body","judul");
                    mq.save();
                    Logger.info("end job id save:"+mq.save());
                }catch (Exception e){}

            }
        }.in(2);

        try {
            Thread.sleep(30000);//
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //@Test
    public void testGrabdataPenyedia(){
        ExtProduct ext = new ExtProduct();
        //ext.executeAll();
        try {
                Thread.sleep(480000);//
            }catch (InterruptedException e) {
                e.printStackTrace();
            }

//        if(ext.getData(3, ext.url)){
//            Logger.info("total info :"+ext.jsParsed.total);
//            for (Produk p: ext.jsParsed.produk) {
//                Logger.info("info -> "+p.informasi.deskripsi_lengkap);
//                for (Item i:p.spesifikasi.item) {
//                    Logger.info("  *> item desc :"+i.deskripsi);
//                }
//                for (models.api.produk.detail.image.Item i:p.image.item) {
//                    Logger.info("  ^^> item images :"+i.deskripsi);
//                }
//                for (models.api.produk.detail.lampiran.Item i:p.lampiran.item) {
//                    Logger.info("  @@> item lamp :"+i.deskripsi);
//                }
//            }
//        }

//        String url = "http://lkpp.bhinneka.com/lkpp/updated_produk?per_page=500&page=1&secretkey=f3ebe642baf41256ebd17e7d63ed99e4b6438857";
//        try {
//            URL urls = new URL(url);
//            Map<String, List<String>> params = ext.splitQuery(urls);
//            Logger.info(params.get("per_page")+": perpage");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
    }

}
