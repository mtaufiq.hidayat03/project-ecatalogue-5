package models.aanwijzing;

import models.BaseKatalogTable;
import models.aanwijzing.contract.AanwijzingContract;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.user.User;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;
import repositories.AanwijzingRepository;

import javax.persistence.Transient;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Table(name = "question_answer")
public class AanwijzingAnswer extends BaseKatalogTable implements AanwijzingContract {
    @Id
    public Long id;

    @Required
    public Integer produk_id;

    @Required
    public Integer pertanyaan_id;

    @Required
    public String jawaban;

    public Integer status;

    public String doc_answer;

    public String file_hash;

    public Long blb_id_content;

    @Transient
    public String download_url;

    public AanwijzingAnswer() {
    }

    @Override
    public String getQuestion() {
        return null;
    }

    @Override
    public String getAnswer() {
        return jawaban;
    }

    @Transient
    public String createdBy(Long id) {
        User user = Query.find("SELECT nama_lengkap FROM user where id='" + id + "'", User.class).first();
        return user.nama_lengkap;
    }

    @Transient
    public Boolean isYourComment(Long id, Integer userId) {
        AanwijzingAnswer model = AanwijzingRepository.getAnswerBy(id, userId);
        if (model != null){
            return true;
        }
        return false;
    }

    @Transient
    public String getCreatedDateAndTime() {
        DateFormat timeFormat = new SimpleDateFormat("HH:mm");
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM YYY");
        String timeCreated = " " + Messages.get("pukul.label") + " "+  timeFormat.format(created_date);
        String dateCreated = dateFormat.format(created_date);
        return dateCreated + timeCreated;
    }

    @Transient
    public String getCreatedDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM YYY");
        String dateCreated = dateFormat.format(created_date);
        return dateCreated;
    }

    @Transient
    public String getFileExtension() {
        String name = doc_answer;
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return ""; // empty extension
        }
        return name.substring(lastIndexOf).toLowerCase();
    }

    public static void createAanwijzingAnswer(Integer pertanyaan_id, Integer produk_id, String answer, File file) throws Exception {
        AanwijzingAnswer aanwijzingAnswer = new AanwijzingAnswer();
        BlobTable blob;
        if(file != null) {
            if (file.length() <= 1048576) {
                /* String nama_file = UsulanCtr.getRandom()+"-"+file.getName().replace(" ","-");
                File dokumenroot = Play.applicationPath;
                File destination = new File(dokumenroot+"/public/files/dokumen/"+nama_file);
                Files.copy(file,destination);
                aanwijzingAnswer.doc_answer = nama_file; */
                blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, aanwijzingAnswer.blb_id_content, AanwijzingAnswer.class.getSimpleName());
                aanwijzingAnswer.doc_answer = blob.blb_nama_file;
                aanwijzingAnswer.file_hash = blob.blb_hash;
                aanwijzingAnswer.blb_id_content = blob.id;
                if (null != blob) {
                    blob.save();
                }
            }
        }
        aanwijzingAnswer.produk_id = produk_id;
        aanwijzingAnswer.pertanyaan_id = pertanyaan_id;
        aanwijzingAnswer.jawaban = answer;
        aanwijzingAnswer.status = 1;
        aanwijzingAnswer.save();
    }

    public static void editAanwijzingAnswer(String answer, Integer editid) {
        AanwijzingAnswer aanwijzingAnswer = findById(editid);
        aanwijzingAnswer.jawaban = answer;
        aanwijzingAnswer.save();
    }

    public static void deleteAanwijzingAnswer(long id) {
        AanwijzingAnswer aanwijzingAnswer = findById(id);
        aanwijzingAnswer.status = 0;
        aanwijzingAnswer.save();
    }

    public static AanwijzingAnswer findFileAanwijzingAnswer(BlobTable blob){
        AanwijzingAnswer aanwijzingAnswer = new AanwijzingAnswer();
        aanwijzingAnswer.id = blob.blb_id_content;
        aanwijzingAnswer.download_url = blob.getDownloadUrl(null);

        return aanwijzingAnswer;
    }

    public String getFileUrl() {
        String url = "";

        if(blb_id_content != null && blb_id_content.toString() != ""){
            BlobTable blobTable = BlobTable.findByBlobId(blb_id_content);
            if(blobTable != null){
                AanwijzingAnswer aanwijzingAnswer = findFileAanwijzingAnswer(blobTable);
                url = aanwijzingAnswer.download_url;
            }
        }

        return url;
    }

    public File getFileAttachment() {
        File file = null;

        if(blb_id_content != null && blb_id_content.toString() != ""){
            BlobTable blobTable = BlobTable.findByBlobId(blb_id_content);
            if(blobTable != null){
                file = blobTable.getFile();
            }
        }
        return file;
    }

    public boolean isFileExist() {
        File file = getFileAttachment();
        boolean exists = false;
        if(file != null) {
            exists = file.exists();
        }
        return exists;
    }
}
