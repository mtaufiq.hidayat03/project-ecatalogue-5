package models.prakatalog;

import ext.contracts.PaginateAble;
import ext.contracts.ParamAble;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 12/18/2017
 */
public class SearchUsulan implements PaginateAble {

    public long total = 0;
    public List<Usulan> items = new ArrayList<>();
    private ParamAble paramAble;

    public SearchUsulan(ParamAble paramAble) {
        this.paramAble = paramAble;
    }

    @Override
    public long getTotal() {
        return total;
    }

    @Override
    public long getTotalCurrentPage() {
        return items.size();
    }

    @Override
    public int getMax() {
        return 12;
    }

    @Override
    public int getPage() {
        return paramAble.getPage();
    }

    @Override
    public List<BasicNameValuePair> getParams() {
        return paramAble.getParams();
    }

    @Override
    public void setTotal(Long total) {
        this.total = total;
    }

}
