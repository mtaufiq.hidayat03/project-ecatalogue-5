START TRANSACTION;

insert into usulan(nama_usulan, created_by, created_date)
select 'MIGRASI E-KATALOG4',1,now() from usulan where not exists(
select nama_usulan from usulan where nama_usulan='MIGRASI E-KATALOG4'
) limit 1;


insert into penawaran(nama_penawaran,usulan_id, penyedia_id, kontrak_id, komoditas_id, user_id, created_by, created_date)
select concat('MIGRASI E-KATALOG4 ',k.tgl_masa_berlaku_selesai), u.id, penyedia_id, k.id as kontrak_id, komoditas_id, py.user_id, -99, now()
from penyedia_kontrak k
join penyedia py on k.penyedia_id=py.id
, (select id from usulan where nama_usulan='MIGRASI E-KATALOG4' limit 1) u 
where k.tgl_masa_berlaku_selesai >= now()
and k.id not in (select distinct kontrak_id from penawaran)
;

update produk p
join (select p.id as produk_id, pn.id as penawaran_id
from produk p
join penyedia py on p.penyedia_id=py.id
join penyedia_kontrak k on k.penyedia_id=py.id
join penawaran pn on k.id=pn.kontrak_id
where p.penawaran_id is null) pn
set p.penawaran_id=pn.penawaran_id where p.id=pn.produk_id
;
commit;

UPDATE penawaran set created_by = -99 , created_date = now() where created_date is null;

COMMIT;

UPDATE penawaran p 
join penyedia_kontrak k on p.kontrak_id=k.id
set k.penawaran_id=p.id where k.penawaran_id is null;

commit;