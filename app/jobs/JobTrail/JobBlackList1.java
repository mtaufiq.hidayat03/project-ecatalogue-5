package jobs.JobTrail;

import jobs.blacklist.ExtBlacklistData;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import java.util.UUID;

@NoTransaction
public class JobBlackList1 extends Job {
    ExtBlacklistData bl = new ExtBlacklistData();
    int nextWaitTime = 0;
    public JobBlackList1(ExtBlacklistData _bl, int _nextWaitTime){
        bl = _bl;
        nextWaitTime = _nextWaitTime;
    }
    String uniqueID = UUID.randomUUID().toString();
    @Override
    public void before() {
        Logger.info("job id:" + uniqueID);
        super.before();
    }

    public void doJob() {
        Logger.info("on job execute, waittime:" + nextWaitTime);
        bl.getBlacklistData(nextWaitTime);
        Logger.info("%%%%done getDate, saving now%%%");
        bl.saveDataBlacklistToDb();
        Logger.info("done job:" + bl.blacklistList.size());
    }
}
