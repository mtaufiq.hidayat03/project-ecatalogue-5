package jobs.extdata;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import jobs.JobTrail.JobSirupData;
import jobs.JobTrail.JobSirupData2;
import models.api.Rup;
import models.api.Sirup;
import models.api.SirupById;
import models.jcommon.config.Configuration;
import models.jcommon.http.SimpleHttpRequest;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.util.Config;
import play.Logger;
import play.db.jdbc.Query;
import play.jobs.Job;
import play.libs.URLs;
import utils.KatalogUtils;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

//import jobs.TrackableJob;

public class ExtData {
    private Properties propJob;
    public static final String DEFAULT_WAIT_TIME = "job.sirup.waittime";
    public static final String DEFAULT_WAIT_TIME_NEXT = "job.sirup.waittime.next";
    private int defaultWaitime = 0;
    private int nextWaitime = 0;
    private Boolean resultGet;
    private String jsResult = "[]";
    private String url = Config.getInstance().sirup_api;
    public List<Sirup> sirupList = new ArrayList<>();
    public List<SirupById> sirupById = new ArrayList<>();
    private int delayTaskJob=5;//second
    private int delayJob=10;//second
    public ExtData(){
        propJob = new Properties();
        propJob.put(DEFAULT_WAIT_TIME, Configuration.getConfigurationValue(DEFAULT_WAIT_TIME,""));
        propJob.put(DEFAULT_WAIT_TIME_NEXT, Configuration.getConfigurationValue(DEFAULT_WAIT_TIME_NEXT,""));

        defaultWaitime = Integer.parseInt(propJob.getProperty(DEFAULT_WAIT_TIME));
        nextWaitime = Integer.parseInt(propJob.getProperty(DEFAULT_WAIT_TIME_NEXT));

    }

    public List<Sirup> getSyrupData(Date tglupdate){
        if(getData(tglupdate)) {
            this.sirupList = parseAndGetData();
        }
        return sirupList;
    }
    public List<Sirup> getSyrupData(int waittime,Date tglupdate){
        if(getData(waittime,tglupdate)) {
            this.sirupList = parseAndGetData();
        }
        return sirupList;
    }

    private Boolean getData(Date date){
        return getData(defaultWaitime,date);
    }
    private Boolean getData(Integer minuteTimeout, Date tgl_update){
        resultGet = false;
        jsResult = "[]";
        String date = KatalogUtils.toDateyyyyMMdd(tgl_update);
        Integer seconds = 0;
        if(minuteTimeout > 0){
            seconds = (minuteTimeout*60);
        }
        Logger.info("run with minuteTimeout: "+minuteTimeout+", and date "+ tgl_update.toLocaleString());
        try (
                SimpleHttpRequest connect = new SimpleHttpRequest(seconds)) {
                String params = "?auditupdate=" + URLs.encodePart(date);
                connect.get(url+params);
                jsResult = connect.getString();
                resultGet = true;
        } catch (InterruptedException e) {

            Logger.error("Interupted Exception:"+e.getMessage());
            e.printStackTrace();

        }catch (IOException e) {
            Logger.error("IO Exception:"+e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            Logger.error("Exception:"+e.getMessage());
            e.printStackTrace();
        }
        if(resultGet==false){
            defaultWaitime++;
            nextWaitime = defaultWaitime;
            //Configuration.updateConfigurationValue(DEFAULT_WAIT_TIME_NEXT, String.valueOf(newWait));
            //propJob.replace(DEFAULT_WAIT_TIME_NEXT, String.valueOf(newWait));
            newJob(tgl_update);
        }
        return resultGet;
    }

    private void newTaskJob(Date tg_update){
        System.out.println("Masuk");
//        new Job() {
//            String uniqueID = UUID.randomUUID().toString();
//            @Override
//            public void before() {
//                Logger.info("job id:"+uniqueID+" for date:"+tg_update);
//                super.before();
//            }
//            public void doJob(){
//                Logger.info(">>>>>> Do Job with id:"+uniqueID+" and date:"+tg_update.toLocaleString());
//                getSyrupData(tg_update);
//                Logger.info("%%%% done getDate, saving now %%%");
//                saveDataToDb();
//                //sendEmailReport();
//            }
//            public void after(){
//                Logger.info("<<<<<< Done Job with id:"+uniqueID+" and date:"+tg_update.toLocaleString());
//                //mail will be here
//            }
//        }.in(delayTaskJob);

        JobSirupData js = new JobSirupData(this,tg_update);
        js.in(delayJob);

    }
    private void newJob(Date tgl_update){
        new JobSirupData2(this,tgl_update,delayJob).in(delayJob);
//        new Job() {
//            String uniqueID = UUID.randomUUID().toString();
//            @Override
//            public void before() {
//                Logger.info("job id:"+uniqueID);
//                super.before();
//            }
//
//            public void doJob(){
//                Logger.info("on job execute, waittime:"+nextWaitime);
//                getSyrupData(nextWaitime,tgl_update);
//                Logger.info("%%%% done getDate, saving now %%%");
//                saveDataToDb();
//                sendEmailReport();
//                Logger.info("done job:"+sirupList.size());
//            }
//        }.in(delayJob);

    }
    public List<Sirup> parseAndGetData(){
        try{
            sirupList = new Gson().fromJson(jsResult, new TypeToken<List<Sirup>>(){}.getType());
            //Logger.info("total:"+sirupList.size());
        }catch (Exception e){
            Logger.error("error parse datasirup:"+e.getMessage());
            e.printStackTrace();
        }
        return sirupList;
    }

    public Timestamp getLatestAuditUpdate(){
        List<Rup> data = Query.find("select r.* from rup r order by r.audit_update desc limit 1",Rup.class).fetch();
        if(data.size()> 0){
            return data.get(0).audit_update;
        }
        return new Timestamp(new Date().getTime());
    }

    public Date getSirupBetweenDate(){
        Timestamp startSirup = getLatestAuditUpdate();
        LocalDate lcStartDate = startSirup.toLocalDateTime().toLocalDate();
        Date tanggal = new Date();
        LocalDate lcEndDate = LocalDate.now();
        if(lcStartDate.getYear() < lcEndDate.getYear()){
//            int selisih = lcEndDate.getYear() - lcStartDate.getYear();
//            lcStartDate.plusYears(selisih);
            lcStartDate = LocalDate.of(lcEndDate.getYear(),lcEndDate.getMonth(),1);
        }
        if(lcStartDate.isBefore(lcEndDate)) {
            for (LocalDate date = lcStartDate; date.isBefore(lcEndDate); date = date.plusDays(1)) {
                Date dates = Date.from(date.atStartOfDay()
                        .atZone(ZoneId.systemDefault())
                        .toInstant());
                Logger.info("chain job >>>>>>>>>>>>>>>>>>>" + dates.toString());
                tanggal = dates;
                //newTaskJob(dates);
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
            }
        }else{
            Date tgl = new Date();
            Logger.info("single job >>>>>>>>>>>>>>>>>>>" + tgl.toString());
            //newTaskJob(tgl);
            tanggal=tgl;
        }
        return tanggal;
    }

    public List<Rup> failed = new ArrayList<>();
    public List<Rup> success = new ArrayList<>();
    public void saveDataToDb(){
        List<Sirup> dataSirup = sirupList;
        failed = new ArrayList<>();
        success = new ArrayList<>();
        //Logger.info("save data rup:");
        for (Sirup s: dataSirup) {
            Rup rupObj = Rup.findById(s.id);
            try{
                if(rupObj != null){
                    rupObj.nama = s.nama;
                    rupObj.kegiatan = s.kegiatan;
                    rupObj.jenis_belanja = null == s.jenisBelanja ?0:s.jenisBelanja ;
                    rupObj.jenis_pengadaan = null == s.jenisPengadaan?0:s.jenisPengadaan;
                    rupObj.volume = s.volume;
                    rupObj.metode_pengadaan = null == s.metodePengadaan?0:s.metodePengadaan;
                    rupObj.tanggal_awal_pengadaan= s.getTanggalAwalPengadaan();
                    rupObj.tanggal_akhir_pengadaan = s.getTanggalAkhirPengadaan();
                    rupObj.tanggal_awal_pekerjaan = s.getTanggalAwalPekerjaan();
                    rupObj.tanggal_akhir_pekerjaan = s.getTanggalAkhirPekerjaan();
                    rupObj.lokasi = s.lokasi;
                    rupObj.keterangan = s.keterangan;
                    rupObj.tahun_anggaran = s.tahunAnggaran;
                    rupObj.id_sat_ker = s.idSatker;
                    rupObj.kode_kldi = s.kodeKldi;
                    rupObj.aktif = s.aktif;
                    rupObj.is_delete = s.is_delete;
                    rupObj.audit_update = new Timestamp(s.auditupdate.getTime());
                    rupObj.kode_anggaran = s.kodeAnggaran;
                    rupObj.sumber_dana = s.sumberDanaString;
                    int result = rupObj.save();
                    Logger.info("update:"+result+ " ID : "+rupObj.id);
                }else{
                    rupObj = new Rup();
                    rupObj.id = s.id;
                    rupObj.nama = s.nama;
                    rupObj.kegiatan = s.kegiatan;
                    rupObj.jenis_belanja = null == s.jenisBelanja ?0:s.jenisBelanja ;
                    rupObj.jenis_pengadaan = null == s.jenisPengadaan?0:s.jenisPengadaan;
                    rupObj.volume = s.volume;
                    rupObj.metode_pengadaan = null == s.metodePengadaan?0:s.metodePengadaan;
                    rupObj.tanggal_awal_pengadaan= s.getTanggalAwalPengadaan();
                    rupObj.tanggal_akhir_pengadaan = s.getTanggalAkhirPengadaan();
                    rupObj.tanggal_awal_pekerjaan = s.getTanggalAwalPekerjaan();
                    rupObj.tanggal_akhir_pekerjaan = s.getTanggalAkhirPekerjaan();
                    rupObj.lokasi = s.lokasi;
                    rupObj.keterangan = s.keterangan;
                    rupObj.tahun_anggaran = s.tahunAnggaran;
                    rupObj.id_sat_ker = s.idSatker;
                    rupObj.kode_kldi = s.kodeKldi;
                    rupObj.aktif = s.aktif;
                    rupObj.is_delete = s.is_delete;
                    rupObj.audit_update = new Timestamp(s.auditupdate.getTime());
                    rupObj.kode_anggaran = s.kodeAnggaran;
                    rupObj.sumber_dana = s.sumberDanaString;
                    int result = rupObj.save();
                    Logger.info("save:"+result + " ID : "+rupObj.id);
                }
                if(rupObj!=null) {
                    success.add(rupObj);
                }
            }catch (Exception e){
                if(rupObj!=null) {
                    failed.add(rupObj);
                }
                Logger.info("error save or update rup:"+e.getMessage());
                e.printStackTrace();
            }
        }
        Logger.info(">>>>>>>>>> save success total:"+success.size());

    }

    public void sendEmailReport(){
        String body = "Total Rup success:"+success.size()+" Total Rup failed:"+failed.size();
        String title = "Report JOB SIRUP";
        String report = Configuration.getConfigurationValue(MailQueue.SMTP_MAIL_ADMIN, "");
        String[] ary = report.split(";");
        for (String to : ary) {
            MailQueue mq = new MailQueue();
            try {
                Logger.info(to+" "+body+" "+title);
                mq = EmailManager.createEmail(to, body, title);
            } catch (Exception e) {
                Logger.error("error create mail:"+e.getMessage());
            }
            Logger.info(mq.save()+"");
        }
    }

    public List <SirupById> getSyrupById(String id){
        if(getDataById(id)) {
            this.sirupById = parseAndGetDataById();
        }
        return sirupById;
    }

    public List<SirupById> parseAndGetDataById(){
        try{
            sirupById = new Gson().fromJson(jsResult, new TypeToken<List<SirupById>>(){}.getType());
            //Logger.info("total:"+sirupList.size());
        }catch (Exception e){
            Logger.error("error parse datasirup:"+e.getMessage());
            e.printStackTrace();
        }
        return sirupById;
    }


    private Boolean getDataById(String id){
        String urlById = "https://sirup.lkpp.go.id/sirup/service2/paketPenyediaById";
        resultGet = false;
        jsResult = "[]";

        Integer seconds = 0;
        Integer minuteTimeout = 10;
        if(minuteTimeout > 0){
            seconds = (minuteTimeout*60);
        }

        try (
                SimpleHttpRequest connect = new SimpleHttpRequest(seconds)) {
            String params = "?id=" + URLs.encodePart(id);
            connect.get(urlById + params);
            jsResult = connect.getString();
            resultGet = true;
        } catch (InterruptedException e) {

            Logger.error("Interupted Exception:" + e.getMessage());
            e.printStackTrace();

        } catch (IOException e) {
            Logger.error("IO Exception:" + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            Logger.error("Exception:" + e.getMessage());
            e.printStackTrace();
        }

        return resultGet;
    }
}
