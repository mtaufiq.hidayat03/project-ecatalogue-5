package unit;

import models.katalog.Komoditas;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author HanusaCloud on 3/26/2020 12:06 PM
 */
@RunWith(Parameterized.class)
public class CommodityGroupNameTest {

    public Komoditas komoditas;
    public String expected;

    public CommodityGroupNameTest(Komoditas komoditas, String expected) {
        this.komoditas = komoditas;
        this.expected = expected;
    }

    @Test
    public void shouldBeAbleToGroupByCategory() {
        Assert.assertEquals(expected, komoditas.groupCategoryByName());
    }

    private static Komoditas generateKomoditas(Long category) {
        Komoditas komoditas = new Komoditas();
        komoditas.komoditas_kategori_id = category;
        return komoditas;
    }

    @Parameterized.Parameters
    public static Collection commodityDataSource() {
        return Arrays.asList(
                new Object[][] {
                        {generateKomoditas(1L), "Nasional"},
                        {generateKomoditas(2L), "Nasional"},
                        {generateKomoditas(5L), "Lokal"},
                        {generateKomoditas(6L), "Sektoral"}
                }
        );
    }

}
