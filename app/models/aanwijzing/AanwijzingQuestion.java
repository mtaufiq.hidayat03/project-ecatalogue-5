package models.aanwijzing;

import models.BaseKatalogTable;
import models.aanwijzing.contract.AanwijzingContract;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.user.User;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.Transient;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

@Table(name = "question")
public class AanwijzingQuestion extends BaseKatalogTable implements AanwijzingContract {
    @Id
    public Long id;

    @Required
    public Integer produk_id;

    @Required
    public String judul_pertanyaan;

    @Required
    public String pertanyaan;

    public Integer status;

    public String doc_question;

    public String file_hash;

    public Long blb_id_content;

    @Transient
    public String download_url;

    public AanwijzingQuestion() {
    }

    @Override
    public String getQuestion() {
        return pertanyaan;
    }

    @Override
    public String getAnswer() {
        return null;
    }

    @Transient
    public Integer countJawaban(Long produk_id, Long id) {
        return (int) Query.count("SELECT count(*) FROM question_answer WHERE pertanyaan_id='" + id + "' " +
                "AND produk_id='" + produk_id + "' AND deleted_date is NULL AND status=1");
    }

    @Transient
    public String createdBy(Long id) {
        User user = Query.find("SELECT nama_lengkap FROM user where id='" + id + "'", User.class).first();
        return user.nama_lengkap;
    }

    @Transient
    public List<AanwijzingAnswer> getAllAnswer(Integer produk_id, Long id) {
        return AanwijzingAnswer.find("produk_id ='" + produk_id + "' AND pertanyaan_id ='" + id + "' AND deleted_date is NULL and status=1").fetch();
    }

    @Transient
    public String getCreatedDateAndTime() {
        DateFormat timeFormat = new SimpleDateFormat("HH:mm");
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM YYY");
        String timeCreated = " " + Messages.get("pukul.label") + " " + timeFormat.format(created_date);
        String dateCreated = dateFormat.format(created_date);
        return dateCreated + timeCreated;
    }

    @Transient
    public String getFileExtension() {
        String name = doc_question;
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return ""; // empty extension
        }
        return name.substring(lastIndexOf).toLowerCase();
    }

    @Transient
    public String getCreatedDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM YYY");
        String dateCreated = dateFormat.format(created_date);
        return dateCreated;
    }

    public static AanwijzingQuestion findFileAanwijzingQuestion(BlobTable blob) {
        AanwijzingQuestion aanwijzingQuestion = new AanwijzingQuestion();
        aanwijzingQuestion.id = blob.blb_id_content;
        aanwijzingQuestion.download_url = blob.getDownloadUrl(null);

        return aanwijzingQuestion;
    }

    public String getFileUrl() {
        String url = "";

        if (blb_id_content != null && blb_id_content.toString() != "") {
            BlobTable blobTable = BlobTable.findByBlobId(blb_id_content);
            if (blobTable != null) {
                AanwijzingQuestion aanwijzingQuestion = findFileAanwijzingQuestion(blobTable);
                url = aanwijzingQuestion.download_url;
            }
        }

        return url;
    }

    public static void createAanwijzingQuestion(String judul_pertanyaan, Integer produk_id, String pertanyaan, File file) throws Exception {
        AanwijzingQuestion aanwijzingQuestion = new AanwijzingQuestion();
        BlobTable blob;
        if (file != null) {
            if (file.length() <= 1048576) {
                /* String nama_file = UsulanCtr.getRandom()+"-"+file.getName().replace(" ","-");
                File dokumenroot = Play.applicationPath;
                File destination = new File(dokumenroot+"/public/files/dokumen/"+nama_file);
                Files.copy(file,destination); */
                blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, aanwijzingQuestion.blb_id_content, AanwijzingQuestion.class.getSimpleName());
                aanwijzingQuestion.doc_question = blob.blb_nama_file;
                aanwijzingQuestion.file_hash = blob.blb_hash;
                aanwijzingQuestion.blb_id_content = blob.id;
                if (null != blob) {
                    blob.save();
                }
            }
        }
        aanwijzingQuestion.produk_id = produk_id;
        aanwijzingQuestion.pertanyaan = pertanyaan;
        aanwijzingQuestion.judul_pertanyaan = judul_pertanyaan;
        aanwijzingQuestion.status = 1;
        aanwijzingQuestion.save();

    }

    public static void editAanwijzingQuestion(String editjudul_pertanyaan, String editpertanyaan, Integer editid) {
        AanwijzingQuestion aanwijzingQuestion = findById(editid);
        aanwijzingQuestion.pertanyaan = editpertanyaan;
        aanwijzingQuestion.judul_pertanyaan = editjudul_pertanyaan;
        aanwijzingQuestion.save();
    }

    public static void deleteAanwijzingQuestion(long id) {
        AanwijzingQuestion aanwijzingQuestion = findById(id);
        aanwijzingQuestion.status = 0;
        aanwijzingQuestion.save();
    }

    public File getFileAttachment() {
        File file = null;

        if(blb_id_content != null && blb_id_content.toString() != ""){
            BlobTable blobTable = BlobTable.findByBlobId(blb_id_content);
            if(blobTable != null){
                file = blobTable.getFile();
            }
        }
        return file;
    }

    public boolean isFileExist() {
        File file = getFileAttachment();
        boolean exists = false;
        if(file != null) {
            exists = file.exists();
        }
        return exists;
    }

}
