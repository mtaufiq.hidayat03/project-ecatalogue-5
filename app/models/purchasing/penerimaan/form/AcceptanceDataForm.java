package models.purchasing.penerimaan.form;

import models.purchasing.base.BaseDataForm;
import models.purchasing.base.BaseProductForm;
import models.purchasing.penerimaan.TotalPenerimaan;
import org.apache.commons.lang.StringEscapeUtils;
import utils.DateTimeUtils;

import java.util.Date;
import java.util.List;

/**
 * @author HanusaCloud on 10/23/2017
 */
public class AcceptanceDataForm extends BaseDataForm {

    public String systemNumber;
    public String docNumber;
    public String acceptanceDate;
    public String documentDate;
    public String note;
    public Long packageId;
    public Boolean allSent = false;

    public String getNote() {
        return StringEscapeUtils.escapeSql(note);
    }

    public String getDocNumber() {
        return StringEscapeUtils.escapeSql(docNumber);
    }

    public Date getDocumentDate() {
        return DateTimeUtils.parseDdMmYyyy(documentDate);
    }

    public Date getAcceptanceDate() {
        return DateTimeUtils.parseDdMmYyyy(acceptanceDate);
    }

    public Long getTotalDelivered() {
        List<BaseProductForm> products = getSavedProducts();
        if (!products.isEmpty())  {
            Long total = 0L;
            for (BaseProductForm model : products) {
                total += model.productQuantity;
            }
            return total;
        }
        return 0L;
    }

    public AcceptanceDataForm totalPenerimaan(TotalPenerimaan totalPenerimaan) {
        if (totalPenerimaan != null) {
            this.allSent = totalPenerimaan.isCompleted(getTotalDelivered());
        }
        return this;
    }

}
