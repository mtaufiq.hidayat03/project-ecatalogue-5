package controllers.masterdata;

import controllers.BaseController;
import controllers.security.AllowAccess;
import jobs.wilayah.JobWilayah;
import models.jcommon.util.CommonUtil;
import models.masterdata.Kabupaten;
import models.masterdata.Provinsi;
import models.secman.Acl;
import play.Logger;
import play.i18n.Messages;

public class WilayahCtr extends BaseController {

	@AllowAccess({Acl.COM_WILAYAH})
	public static void index() {
		render();
	}

	@AllowAccess({Acl.COM_WILAYAH})
	public static void manualFetch() {
		JobWilayah jw = new JobWilayah();
		jw.doJob();
		index();
	}

	@AllowAccess({Acl.COM_WILAYAH_ADD})
	public static void create() {
		render();
	}

	@AllowAccess({Acl.COM_WILAYAH_ADD})
	public static void createSubmit(String[] nama_provinsi_arr) {
		// checkAuthenticity();
		// if(nama_provinsi_arr != null){
		//
		// //validasi provinsi
		// boolean validate = validateProvinsi(nama_provinsi_arr);
		//
		// if(!validate){
		//
		// if (validation.hasErrors())
		// flash.error("Gagal Simpan Provinsi, periksa kembali inputan anda.");
		//
		// List<String> provinsiList = Arrays.asList(nama_provinsi_arr);
		//
		// render("masterdata/ProvinsiCtr/create.html", provinsiList);
		//
		// }
		//
		//
		// for(int i = 0; i < nama_provinsi_arr.length; i++){
		// Provinsi sd = new Provinsi();
		// sd.nama_provinsi = nama_provinsi_arr[i];
		// sd.active = true;
		// sd.save();
		// }
		//
		// flash.success("Data berhasil disimpan");
		// index();
		// }

	}

	public static void detail(long id) {
		Provinsi model = Provinsi.findById(id);
		notFoundIfNull(model);
		render("masterdata/WilayahCtr/detail.html", model);
	}

	@AllowAccess({Acl.COM_WILAYAH_EDIT})
	public static void editSubmit(long id_provinsi, String nama_provinsi) {
		checkAuthenticity();
		if (CommonUtil.isEmpty(nama_provinsi)) {
			flash.error(
					"Gagal Ubah provinsi, nama provinsi tidak boleh kosong");
		} else {
			Provinsi provinsi = Provinsi.findById(id_provinsi);
			provinsi.nama_provinsi = nama_provinsi;
			provinsi.active = true;
			provinsi.save();
		}
		flash.success("Data berhasil disimpan");
		index();
	}

	@AllowAccess({Acl.COM_WILAYAH_EDIT})
	public static void editNilaiSubmit(long id, long id_provinsi, String nama,
			Double nilai_jual, Double nilai_beli) {
		checkAuthenticity();
		Logger.debug("Provinsi ID = %s", id_provinsi);
		if (id == 0) {
			Kabupaten kabupaten = new Kabupaten();
			kabupaten.provinsi_id = id_provinsi;
			kabupaten.nama_kabupaten = nama;
			kabupaten.save();
			flash.success("Data berhasil disimpan");
			detail(kabupaten.provinsi_id);
		} else {
			Kabupaten kabupaten = Kabupaten.findById(id);
			kabupaten.nama_kabupaten = nama;
			kabupaten.save();
			flash.success("Data berhasil disimpan");
			detail(kabupaten.provinsi_id);
		}
	}

	@AllowAccess({Acl.COM_WILAYAH_DELETE})
	public static void delete(long id) {
//		Provinsi model = Provinsi.findById(id);
//		notFoundIfNull(model);
//		if (!provinsiIsInUse(id)) {
//			model.softDelete();
//			flash.success(Messages.get("notif.success.delete"));
//		} else {
//			flash.error(Messages.get("notif.failed.delete.relation"));
//		}
//		index();
	}

	private static boolean provinsiIsInUse(long id) {
		return false;
	}

	@AllowAccess({Acl.COM_WILAYAH_EDIT})
	public static void setProvinsiAktif(long id) {
		Provinsi provinsi = Provinsi.findById(id);
		notFoundIfNull(provinsi);
		provinsi.setAktif();
		flash.success(
				Messages.get("record_update_aktifnonaktif_relation_success"));
		index();
	}

	@AllowAccess({Acl.COM_WILAYAH_EDIT})
	public static void setProvinsiNonAktif(long id) {
		Provinsi provinsi = Provinsi.findById(id);
		notFoundIfNull(provinsi);
		if (!provinsiIsInUse(id)) {
			provinsi.setNonAktif();
			flash.success(Messages
					.get("record_update_aktifnonaktif_relation_success"));
		} else {
			flash.error(Messages
					.get("record_update_aktifnonaktif_relation_failed"));
		}
		index();
	}
	
	@AllowAccess({Acl.COM_WILAYAH_EDIT})
	public static void setKabupatenAktif(long id) {
		Kabupaten kabupaten = Kabupaten.findById(id);
		notFoundIfNull(kabupaten);
		kabupaten.setAktif();
		flash.success(
				Messages.get("record_update_aktifnonaktif_relation_success"));
		index();
	}

	@AllowAccess({Acl.COM_WILAYAH_EDIT})
	public static void setKabupatenNonAktif(long id) {
		Kabupaten kabupaten = Kabupaten.findById(id);
		notFoundIfNull(kabupaten);
		if (!provinsiIsInUse(id)) {
			kabupaten.setNonAktif();
			flash.success(Messages
					.get("record_update_aktifnonaktif_relation_success"));
		} else {
			flash.error(Messages
					.get("record_update_aktifnonaktif_relation_failed"));
		}
		index();
	}


}
