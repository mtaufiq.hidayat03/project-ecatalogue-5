INSERT INTO configuration (
	cfg_category, cfg_sub_category, cfg_value
) VALUES (
	'CONFIG', 'kurs.source.url', 'http://www.bi.go.id/id/moneter/informasi-kurs/transaksi-bi/Default.aspx'
);

INSERT INTO configuration (
	cfg_category, cfg_sub_category, cfg_value
) VALUES (
	'CONFIG', 'kurs.max.retry', 5
);

INSERT INTO configuration (
	cfg_category, cfg_sub_category, cfg_value
) VALUES (
	'CONFIG', 'kurs.retry.delay', 5
);