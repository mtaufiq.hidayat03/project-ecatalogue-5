package models.cms;

import models.BaseKatalogTable;
import models.common.AktifUser;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * Created by user on 10/2/17.
 */

@Table(name="pustaka_file_kategori")
public class KategoriUnduh extends BaseKatalogTable{
    @Id
    public Long id;
    public Long parent_id;
    public String nama_kategori;
    public String deskripsi;
    public Long posisi_kategori;
    public boolean active;


    public static void deleteById(long id){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String date_now = dateFormat.format(date);

        String query = "update pustaka_file_kategori set active = 0, deleted_by = " + AktifUser.getActiveUserId() + ", deleted_date = '"+date_now+"' where id = ?";
        Query.update(query, id);
    }

    public static List<KategoriUnduh> findAllActive(){
        return KategoriUnduh.find("active = 1").fetch();
    }

    public static List<KategoriUnduh> findUsingCount(){
        String query = "SELECT kk.id, kk.nama_kategori,(SELECT COALESCE(count(kon.id),0) FROM konten kon " +
                "WHERE default_konten_kategori_id = kk.id and active =1 and status = 'published') jumlah_berita " +
                "FROM konten_kategori kk WHERE kk.active = 1 ORDER BY kk.nama_kategori";
        return Query.find(query,KategoriUnduh.class).fetch();
    }
    
    public static String getNamaKategori(String nama_kategori) {
    	return nama_kategori;
    }

}
