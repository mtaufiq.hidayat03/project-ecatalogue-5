package repositories.penyedia;

import models.masterdata.Kurs;
import models.penyedia.Penyedia;
import models.penyedia.PenyediaDistributor;
import models.penyedia.PenyediaRepresentatif;
import models.penyedia.form.UserDistributorJson;
import models.user.User;
import models.util.RekananDetail;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.cache.Cache;
import play.cache.CacheFor;
import play.db.jdbc.Query;

import javax.persistence.Transient;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author RaihanIqbal on 11/17/2017
 */
public class ProviderRepository {

    public static final String TAG = "ProviderRepository";

    public static Penyedia findByRknId(long rkn_id){

        return Penyedia.find("rkn_id = ? and active = ? limit 1", rkn_id,Penyedia.AKTIF).first();
    }

    public static Penyedia findByUserId(long user_id){
        return Penyedia.find("user_id = ? and active = ?", user_id, Penyedia.AKTIF).first();
    }

    public static List<Penyedia> findByKomoditas(long komoditas_id){
        String query = "select p.id, p.nama_penyedia, p.active " +
                "from penyedia p " +
                "join penyedia_komoditas pk on pk.penyedia_id = p.id " +
                "where pk.komoditas_id = ? and p.active = ? " +
                "order by p.nama_penyedia";

        return Query.find(query, Penyedia.class,komoditas_id, 1).fetch();
    }

    @Transient
    private static String key1 = "findPenyediaKontrakByKomoditas";
    public static List<Penyedia> findPenyediaKontrakByKomoditas(long komoditas_id){
        List<Penyedia> result = Cache.get(key1+komoditas_id,List.class);
        if(null == result) {
            String query = "select pn.id, pn.nama_penyedia " +
                    "FROM produk p " +
                    "JOIN penawaran pw ON pw.id = p.penawaran_id " +
                    "JOIN penyedia_kontrak pk ON pk.id = pw.kontrak_id " +
                    "JOIN penyedia pn ON pn.id = pk.penyedia_id " +
                    "JOIN komoditas k ON p.komoditas_id = k.id " +
                    "WHERE " +
                    "p.komoditas_id = ? " +
                    "AND p.active = 1 " +
                    "AND p.apakah_ditayangkan = 1 " +
                    "AND k.active = 1 " +
                    "AND p.setuju_tolak = 'setuju' " +
                    "AND ((p.berlaku_sampai >= CURDATE() AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1) " +
                    "OR (p.berlaku_sampai is null AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1)) " +
                    "AND (k.apakah_stok_unlimited = 1 OR (k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0)) " +
                    "GROUP BY pn.id";
            result = Query.find(query, Penyedia.class,komoditas_id).fetch();
            Cache.set(key1+komoditas_id,result,"30mn");
        }
        return result;
    }

    public static void checkAndSaveRknIdByUser(Long userId, Long rknId, RekananDetail rkndet){
        Penyedia penyedia = findByUserId(userId);
//        if(penyedia != null){
//            if(penyedia.rkn_id == null){
//                penyedia.rkn_id = rknId;
//            }
//            penyedia.active = true;
//
//        }else{
//            penyedia = new Penyedia();
//            penyedia.user_id = userId;
//            penyedia.nama_penyedia = rkndet.rkn_nama;
//            penyedia.alamat = rkndet.rkn_alamat;
//            penyedia.website = rkndet.rkn_website;
//            penyedia.email = rkndet.rkn_email;
//            penyedia.no_fax = rkndet.rkn_fax;
//            penyedia.no_telp = rkndet.rkn_telepon;
//            penyedia.no_hp = rkndet.rkn_mobile_phone;
//            penyedia.pkp = rkndet.rkn_pkp;
//            penyedia.kode_pos = rkndet.rkn_kodepos;
//            penyedia.rkn_id = rknId;
//        }
//        penyedia.save();
    }

    public static Map<Long, Penyedia> checkProviderByRknId(String rknIds) {
        if (!TextUtils.isEmpty(rknIds)) {
            final String query = "SELECT rkn_id, id FROM penyedia WHERE rkn_id IN ("+ rknIds +") AND active =?";
            List<Penyedia> results =  Query.find(query, Penyedia.class, Penyedia.AKTIF).fetch();
            Logger.debug(TAG + " total result query: " + results.size());
            if (!results.isEmpty()) {
                return results.stream().collect(Collectors.toMap(Penyedia::getRknId, p -> p));
            }
        }
        return Collections.emptyMap();
    }

    public static Map<String, User> checkProviderByUserName(String userName) {
        if (!TextUtils.isEmpty(userName)) {
//            final String query = "SELECT DISTINCT py.user_id, usr.user_name " +
//                    "FROM penyedia py " +
//                    "LEFT JOIN user usr ON py.user_id = usr.id " +
//                    "WHERE usr.user_name IN ("+ userName +") AND py.active =? " +
//                    "AND usr.status_penyedia_distributor in ('distributor_saja','penyedia_dan_distributor')";
//            List<Penyedia> results =  Query.find(query, Penyedia.class, Penyedia.AKTIF).fetch();
//            Logger.debug(TAG + " total result query: " + results.size());
//            if (!results.isEmpty()) {
//                return results.stream().collect(Collectors.toMap(Penyedia::getUserName, p -> p));
//            }
            final String query = "SELECT usr.id, usr.user_name " +
                    "FROM user usr " +
                    "WHERE usr.user_name IN ("+ userName +") AND usr.active =? " +
                    "AND usr.status_penyedia_distributor in ('distributor_saja','penyedia_dan_distributor', 'penyedia_saja')";
            List<User> results =  Query.find(query, User.class, User.AKTIF).fetch();
            Logger.debug(TAG + " total result query: " + results.size());
            if (!results.isEmpty()) {
                return results.stream().collect(Collectors.toMap(User::getUser_name, u -> u));
            }
        }
        return Collections.emptyMap();
    }

    public static Penyedia findDetailById(long id){
        String query = "select p.*, u.user_name as username, u.status_penyedia_distributor " +
                "from penyedia p join `user` u on p.user_id = u.id " +
                "where p.id = ?";

        Penyedia result = Query.find(query, Penyedia.class, id).first();

        List<PenyediaDistributor> distributors = PenyediaDistributor.findByPenyedia(id);
        result.distributors = distributors;

        List<PenyediaRepresentatif> representatifs = PenyediaRepresentatif.findByPenyedia(id);
        result.representatives = representatifs;

        return result;
    }

    public static Penyedia setByForm(Penyedia penyedia){
        Penyedia result = new Penyedia();
        if(penyedia.id != null){
            result = Penyedia.findById(penyedia.id);
        }else{
            result.agr_penyedia_kontrak = 0;
            result.agr_penyedia_distributor = 0;
            result.default_kurs_id = Kurs.findKursUtama().id;
        }

        result.nama_penyedia = penyedia.nama_penyedia;
        result.npwp = penyedia.npwp;
        result.alamat = penyedia.alamat;
        result.kode_pos = penyedia.kode_pos;
        result.email = penyedia.email;
        result.website = penyedia.website;
        result.no_telp = penyedia.no_telp;
        result.no_fax = penyedia.no_fax;
        result.pkp = penyedia.pkp;
        result.produk_api_url = penyedia.produk_api_url;
        result.active = true;

        return result;
    }

    public static List<Penyedia> findPenyediaBySearch(String keyword, String column){
        String query = "SELECT p.id, p.nama_penyedia, u.user_name as username " +
                "FROM penyedia p JOIN user u ON p.user_id = u.id " +
                "WHERE "+column+" LIKE ? ";

        return Query.find(query,Penyedia.class,"%"+keyword+"%").fetch();
    }

    public static Long saveNewByRknId(long rkn_id, long user_id){
        RekananDetail rknDetail = RekananDetail.getIdRekananDefault(rkn_id);
        Penyedia penyedia = new Penyedia(rknDetail, user_id);
        return (long) penyedia.save();
    }

    public static List<UserDistributorJson> findPenyediaJoinUserBySearch(String keyword, String column){
        if (column.equalsIgnoreCase("username")){
            column = "u.user_name";
        }
        if (column.equalsIgnoreCase("nama")){
            column = "p.nama_penyedia";
        }
        if (column.equalsIgnoreCase("email")){
            column = "p.email";
        }
        String query = "SELECT p.id as penyedia_id, p.nama_penyedia as nama, u.user_name as username, p.email as email, u.user_role_grup_id as role_id, ug.nama_grup as role_name, \n" +
                "u.status_penyedia_distributor, u.id as user_id, u.rkn_id, p.nama_penyedia as rkn_nama, p.website as rkn_website, p.no_telp as rkn_telepon, p.no_fax as rkn_fax, \n" +
                "p.pkp as rkn_pkp, p.npwp as rkn_npwp, p.email as rkn_email, p.no_hp as rkn_mobile_phone, u.user_name as rkn_namauser, u.user_password as passw,  p.alamat as rkn_alamat, p.kode_pos as rkn_kodepos " +
                "FROM penyedia p \n" +
                "JOIN user u ON p.user_id = u.id\n" +
                "join user_role_grup ug on u.user_role_grup_id = ug.id\n" +
                "WHERE u.user_role_grup_id in (2,10) \n" +
                "and u.status_penyedia_distributor in ('distributor_saja','penyedia_dan_distributor') " +
                "and "+column+" LIKE ? ";
        List<UserDistributorJson> udj = Query.find(query, UserDistributorJson.class,"%"+keyword+"%").fetch();
        return  udj;
    }

}
