package models.elasticsearch.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 10/15/2017
 */
public class ResponseInputElastic {

    public long took;
    public boolean errors;
    @SerializedName("items")
    public List<Map<String, Item>> results = new ArrayList<>();

}
