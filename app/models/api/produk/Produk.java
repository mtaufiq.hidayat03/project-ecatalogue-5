package models.api.produk;

import models.api.produk.detail.*;

/**
 * Created by dadang on 1/5/18.
 */
public class Produk {

	public Informasi informasi;
	public Spesifikasi spesifikasi;
	public Image image;
	public Harga harga;
	public Lampiran lampiran;

}
