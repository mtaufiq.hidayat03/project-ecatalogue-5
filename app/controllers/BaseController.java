package controllers;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import controllers.admin.UserCtr;
import controllers.jcommon.http.BrowserCacheAndGzipEncoding;
import controllers.security.AllowAccess;
import jobs.LoggingJob;
import models.common.AktifUser;
import models.jcommon.util.CommonUtil;
import models.logging.Logging;
import models.secman.UserLoggedIn;
import models.util.Config;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;
import play.Logger;
import play.cache.Cache;
import play.i18n.Lang;
import play.i18n.Messages;
import play.mvc.*;
import utils.KatalogUtils;
import utils.LogUtil;
import utils.SentryUtils;
import utils.performance.ActionMonitor;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@With(BrowserCacheAndGzipEncoding.class)
public class BaseController extends Controller {

	public static final String BASE_TAG = "BaseController";

	private static List<String> urlLock = new ArrayList<>();
	private static boolean firstRequest=true;
	public static Config conf=Config.getInstance();
	private static boolean writeLogToFile = false;
	protected static Gson gson = new Gson();
	@Deprecated
	public static SessionInfo getSessionInfo(){
		String str = null;
		try{
			str = Scope.Session.current.get().get(AktifUser.USER_SESSION_ID);
		}catch (Exception e){
			LogUtil.e("basecontroller->getSessionInfo error:"+e.getMessage(),e);
		}
		if(str==null)
			return null;

		return new SessionInfo(str);
	}

	public static AktifUser getAktifUser(){
		return AktifUser.getAktifUser();
	}

	private static ThreadLocal<StopWatch> tl=new ThreadLocal<>();


	@Before(unless = {"UserCtr.logout", "clearUserSession", "UserCtr.setRoles"})
	protected static void beforeAll()
	{
		if(conf.maintenanceMode){
			firstRequest = false;
			String[] urlAllow ={"/user/login", "/admin.userctr/loginlokalsubmit"};
			if(!Arrays.asList(urlAllow).contains(request.url)){
				String[] users = conf.allowUserInmaintenance.split(",");
				String username = "";
				try {
					username = AktifUser.getAktifUser().getUser().user_name;
				}catch (Exception e){}
				if(!Arrays.asList(users).contains(username)){
					clearUserSession();
					response.reset();
					FirstStart.maintenancemode();
				}
			}

//			if(!"".equals(request.url)) {
//				String[] users = conf.allowUserInmaintenance.split(",");
//				String username = "";
//				try {
//					username = AktifUser.getAktifUser().getUser().user_name;
//				}catch (Exception e){}
//				if(!Arrays.asList(users).contains(username)){
//					response.reset();
//					FirstStart.maintenancemode();
//				}
//
//			}
		}
//		List<RoleItem> roleItems = RoleItem.find("active = 1").fetch();
//
//		for(RoleItem r : roleItems){
//			Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(r.nama_role);
//			StringBuffer sb = new StringBuffer();
//			while (m.find()) {
//				m.appendReplacement(sb, "_"+m.group().toUpperCase());
//			}
//			m.appendTail(sb);
//			System.out.println(sb.toString().toUpperCase() + "(\"" + r.nama_role + "\",\"" + r.deskripsi + "\");");
//		}

		Logger.debug("URL akses:"+request.url);

		UserLoggedIn.writeLogFileGeneral(request.url);

		String language = params.get("language");
		if(language != null){
			Lang.set(language);
		}

        if(conf.monitoringEnable){
			tl.set(new StopWatch());
			tl.get().start();
		}

		if(firstRequest)
		{
			firstRequest=false;
			//firstRequestClearAll
			if(!conf.playCacheDisabled){
				Cache.clear();
				Logger.info("Play Cache clear, on first start apps.");
			}
			//hapus jika ada user yang login pada first run
			try{
				clearUserSession();
				logout();
				Logger.info("Clear cache safe delete, and logout, on first start apps.");
			}catch (Throwable e){
				Logger.warn("error clear cache, and log out at first time:"+e.getMessage());
			}
			if(conf.playCacheDisabled)
				Logger.warn("[Conf] Play.Cache is DISABLED");
			else
				Logger.info("[Conf] Play.Cache is ENABLED");


			//coba meringankan pada saat first run, jangan langsung ke home
			response.reset();
			FirstStart.index();


		}
		if(conf.playCacheDisabled){
			Cache.clear();
		}

		AllowAccess allow = null;
		String userProfilCentrum = String.format("%s/internalCtr/userProfile", conf.centrum_url);
		AktifUser aktifUser = null;
		try{
			allow = (AllowAccess) getActionAnnotation(AllowAccess.class);
			aktifUser = getAktifUser();
		}catch (Throwable e){
            Logger.error("failed get Allow Access or Aktif user:"+e.getMessage());
            //redirect to first start
            FirstStart.index();
		}

		if (allow != null) { // cek otoritas terhadap fungsi yang dijalankan
			boolean otorisasi = false;
			String user_session = "UNAUTHORIZED_USER";
			if (aktifUser != null) {
				user_session = aktifUser.user_id.toString();
				otorisasi = aktifUser.isAuthorized(allow.value());
			}

			/**
			 * [ANDIK} Pada mode development, pengecekan activeUser ditiadakan
			 * supaya tidak selalu login
			 *
			 */
			if (!otorisasi) {
				/**
				 * Untuk Controller DataTableCtr yang memberikan return berupa
				 * json maka harus diberi pesan error yang berbeda karena
				 * forbidden() tidak akan menampilkan apa-apa kecuali table yang
				 * kosong. Ini akan membuat bingung karena tidak jelas errornya.
				 * Oleh karena itu harus dibuat besan error di sisi Server
				 *
				 */
				if (request.action.contains("DataTableCtr"))
					throw new IllegalAccessError(String.format("FORBIDDEN User: %s tidak diijinkan mengakses action: %s", user_session, request.action));
				Logger.error("FORBIDDEN User: %s tidak diijinkan mengakses action: %s", user_session, request.action);
				String message = Messages.get("not-authorized");
				if (StringUtils.isEmpty(user_session))
					message += " karena <b>belum login atau session telah habis</b>.";
				//forbidden(message); // tampilkan halaman error
				flash.error(message);
				LoginCtr.index();
			}


		}

		// keanjang belanja
		if(aktifUser != null){
			Http.Cookie cookie = request.cookies.get(KatalogUtils.getProductBasketCookie(aktifUser.user_id));
			if(cookie != null){
				JsonParser parser = new JsonParser();
				JsonArray jsonArray =  parser.parse(URLDecoder.decode(cookie.value)).getAsJsonArray();
//				int jumlahProdukKeranjang = 0;
//				for(int i = 0; i < jsonArray.size(); i++){
//					JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
//					jumlahProdukKeranjang += jsonObject.get("jumlah").getAsInt();
//				}
				//LogUtil.d("base", jumlahProdukKeranjang);
				renderArgs.put("totalProdukKeranjang", jsonArray.size());
			}
		}
		if(aktifUser!= null) {
			Logger.info(request.url);
			if ((aktifUser.isPenyedia() ^ aktifUser.isDistributor()) && null == aktifUser.getUser().status_penyedia_distributor) {
				createLockurl();
				if(urlLock.indexOf(request.url) < 0) {
					PublikCtr.statusPenyediaDistributor();
					Logger.info("empty status . . . . .. . .");
				}
			}
		}

		String lang = !Lang.get().isEmpty() ? Lang.get() : "id";
		renderArgs.put("aktifUser", aktifUser);
		renderArgs.put("userProfileCentrum", userProfilCentrum);
		renderArgs.put("language", lang);
	}
	
	@Finally
	public static void afterAll()
	{
		if(conf.monitoringEnable){
			StopWatch sw=tl.get();
			sw.stop();
			ActionMonitor.add(request, sw);
			logging();
		}
	}

	protected static void clearUserSession() {
		try{
			Cache.safeDelete(session.getId()); // pastikan objek session sudah dihapus jika ada di Cache
		}catch (Exception e){
			try{
				Cache.delete(session.getId());
			}catch (Exception ex){}
		}
		session.clear(); // bersihkan session pengguna
		response.setHeader("Pragma-directive", "no-cache");
		response.setHeader("Cache-directive", "no-cache");
		response.setHeader("Cache-control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Expires", "0");
	}

	protected static Map<Long, Double> populateKuantitasKeranjang(){
		AktifUser aktifUser = getAktifUser();
		Map<Long, Double> kuantitas = new HashedMap<>();
		Http.Cookie cookie = request.cookies.get(KatalogUtils.getProductBasketCookie(aktifUser.user_id));
		if(cookie != null) {
			JsonParser parser = new JsonParser();
			JsonArray jsonArray = parser.parse(URLDecoder.decode(cookie.value)).getAsJsonArray();
			for (int i = 0; i < jsonArray.size(); i++) {
				JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
				kuantitas.put(jsonObject.get("produkId").getAsLong(), jsonObject.get("jumlah").getAsDouble());
			}
		}

		return kuantitas;
	}

	protected static void logging() {
		try{
			final AktifUser aktifUser = getAktifUser();
			if (aktifUser != null && !request.path.contains("login")) {
				// hopefully it will run asynchronously
				new LoggingJob(new Logging(request, session.getId(), aktifUser)).now();
			}
		}catch (Exception e){}
	}

	private static void logout()
	{
		UserCtr.logout();
//		AktifUser aktifUser = getAktifUser();
//		boolean isCentrum  = (aktifUser != null && aktifUser.centrum_code != null && !aktifUser.centrum_code.isEmpty());
//		UserLoggedIn userLoggedIn = UserLoggedIn.find("session_id = ?", session.current().getId()).first();
//		if(userLoggedIn != null){
//			userLoggedIn.delete();
//		}
//
//		session.clear();
//		if (isCentrum){
//			//TODO Centrum Logout
//			String url=String.format("%s/OAuthCtr/logout?code=%s&redirect_uri=%s",
//					Config.getInstance().centrum_url, aktifUser.centrum_code, Router.reverse("PublikCtr.index")).url;
//			//redirect(url);
//		}
////		else {
////			PublikCtr.home();
////		}
//		urlLock = new ArrayList<>();
	}
	protected static String generateKey(Long userId) {
		return KatalogUtils.getProductBasketCookie(userId);
	}
	protected static void forbidden(String reason) {
		try{
			params.flash();
			flash.error(reason);
		}catch (Exception e){}
		LoginCtr.index();
	}
	protected static void forbidden() {
		try{
			params.flash();
			flash.error(Messages.get("not-authorized"));
		}catch (Exception e){}
		LoginCtr.index();
	}
	protected static void notFound() {
		try{
			params.flash();
			flash.error(Messages.get("not-authorized"));
		}catch (Exception e){}
		PublikCtr.home();
	}
	protected static void notFound(String what) {
		try{
			params.flash();
			flash.error(Messages.get("not-authorized"));
		}catch (Exception e){}
		PublikCtr.home();
	}

	private static void createLockurl(){
		urlLock.add("/syarat-ketentuan");
		urlLock.add("/chat.chatctr/fetchnewchatforbordernavbar");
		urlLock.add("/status-penyedia-distributor");
		urlLock.add("/publikctr/statuspenyediadistributorpost");
		urlLock.add("/admin.userctr/logout");
	}
	protected static void clearLockUrl(){
		urlLock = new ArrayList<>();
	}

	@Catch({Throwable.class})
	public static void globalCatcher(Throwable t) {
		LogUtil.debug(BASE_TAG, "global catcher is activated");
		SentryUtils.uncaught(
				t,
				getAktifUser(),
				CommonUtil.toJson(new SentryUtils.SentryRequest(request))
		);
	}

}
