package models.util.sikap;

import com.google.gson.annotations.SerializedName;
import org.apache.http.util.TextUtils;
import play.data.validation.Required;

public class Identitas {

	public long rkn_id;
	@Required
	public String rkn_nama;
	@Required
	public String btu_id;
	@Required
	public String btu_nama;
	@Required
	public String kbp_id;
	@Required
	public String nm_kab;
	@Required
	public String id_prop;
	@Required
	public String nm_prop;
	@Required
	public String id_kec;
	@Required
	@SerializedName("nm_kec")
	public String nm_kec;
	@Required
	public String id_kel;
	@Required
	public String nm_kel;
	@Required
	public String rkn_alamat;
	@Required
	public String rkn_almtpusat;
	@Required
	public String rkn_fax;
	@Required
	public String rkn_mobile_phone;
	@Required
	public String rkn_namauser;
	@Required
	public String rkn_npwp;
	@Required
	public String rkn_kodepos;
	@Required
	public String rkn_email;
	@Required
	public String rkn_pkp;
	@Required
	public String rkn_statcabang;
	@Required
	public String rkn_status;
	@Required
	public String rkn_status_verifikasi;
	@Required
	public String rkn_telepon;
	@Required
	public String rkn_telppusat;
	@Required
	public String rkn_tgl_daftar;
	@Required
	public String rkn_tgl_setuju;
	@Required
	public String rkn_website;
	public String kantorcabang;
	public String kantorpusataddress;
	public String kantorpusatemail;
	public String kantorpusatphone;
	public String kantorpusatfax;
	public String auditupdate;
	public String pps_id;
	public String repo_id;

	public boolean isValid(){
		return !TextUtils.isEmpty(rkn_nama);
	}

}
