package ext;

import models.jcommon.config.Configuration;

public class FeatureUtil {
    public static Boolean isRatingEnable(){
        boolean result = false;
        int prFitur = 0;
        try{
            prFitur = Integer.parseInt(Configuration.getConfigurationValue("rating.enable","0"));
        }catch (Exception e){}

        if(prFitur>0){
            result=true;
        }

        return result;
    }
}
