package models.masterdata;

import models.BaseKatalogTable;
import models.katalog.komoditasmodel.KomoditasTemplateJadwal;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;


@Table(name = "tahapan")
public class Tahapan extends BaseKatalogTable {

	public static final String TAHAPAN_PENAWARAN = "tahapan_penawaran";
	public static final String TAHAPAN_NEGOSIASI = "tahapan_negosiasi";
	public static final String TAHAPAN_REVISI = "tahapan_revisi";
	public static final String TAHAPAN_REGISTRASI = "tahapan_pendaftaran";
	public static final String TAHAPAN_PENJELASAN = "tahapan_penjelasan";

	@Id
	public Long id;
	public String nama_tahapan;
	public String nama_tahapan_alias;
	public Integer urutan_tahapan;
	public String flagging;
	public boolean active;

	public static String generateName(String alias){
		String replaceSpaceWithUnderscore = alias.replace(" ","_");
		String result = replaceSpaceWithUnderscore.toLowerCase();

		return result;
	}

	public static void saveTahapan(String[] nama_tahapan_arr, Integer[] urutan_tahapan_arr){
		if(nama_tahapan_arr != null){
			for(int i = 0; i < nama_tahapan_arr.length; i++){
				Tahapan tahapan = new Tahapan();
				tahapan.nama_tahapan_alias = nama_tahapan_arr[i];
				tahapan.urutan_tahapan = urutan_tahapan_arr[i];
				tahapan.nama_tahapan = Tahapan.generateName(nama_tahapan_arr[i]);
				tahapan.active = true;
				tahapan.save();
			}

		}
	}

	public static void updateTahapan(long id, String nama_tahapan, Integer urutan_tahapan){
		Tahapan tahapan = Tahapan.findById(id);
		tahapan.nama_tahapan_alias = nama_tahapan;
		tahapan.urutan_tahapan = urutan_tahapan;
		tahapan.nama_tahapan = Tahapan.generateName(nama_tahapan);
		tahapan.save();
	}

    public static void updateFlagging(long id, String flagging){
        Tahapan tahapan = Tahapan.findById(id);
        tahapan.flagging = flagging;
        tahapan.save();
    }

    public static Boolean checkEditUrutan(Integer urutan_tahapan){
        Tahapan tahapan = getUrutan(urutan_tahapan);
        final boolean result = tahapan != null;
        return result;
    }

    public static Boolean checkNamaTahapan(String nama_tahapan){
        Tahapan tahapan = getNamaTahapan(nama_tahapan);
        final boolean result = tahapan != null;
        return result;
    }

    public static Tahapan getUrutan(Integer urutan_tahapan) {
        return Query.find("SELECT * FROM tahapan WHERE active = '1' && urutan_tahapan='" + urutan_tahapan + "'",
                Tahapan.class).first();
    }

	public static Tahapan getUrutanFlagging(Long tahapan_id) {
		return Query.find("SELECT * FROM tahapan WHERE id='" + tahapan_id + "'",
				Tahapan.class).first();
	}

	public static Integer getUrutanFlaggingInt(Long tahapan_id) {
		Tahapan tahapan = Tahapan.getUrutanFlagging(tahapan_id);
		if (tahapan != null){
			return tahapan.urutan_tahapan;
		}
		return null;
	}

	public static void deleteTahapan(long tahapan_id){
		Tahapan tahapan = Tahapan.findById(tahapan_id);
		tahapan.active = false;
		tahapan.save();
	}

	public static Tahapan getTahapanPenawaran(Long template_jadwal_id){
		KomoditasTemplateJadwal templateJadwal = KomoditasTemplateJadwal.findById(template_jadwal_id);
		return Tahapan.findById(templateJadwal.tahapan_penawaran);
	}

	public static Tahapan getTahapanNegosiasi(Long template_jadwal_id){
		KomoditasTemplateJadwal templateJadwal = KomoditasTemplateJadwal.findById(template_jadwal_id);
		return Tahapan.findById(templateJadwal.tahapan_negosiasi);
	}

    public static List<Tahapan> getListTahapan() {
        return Query.find("SELECT flagging FROM tahapan WHERE active = '1' && flagging is not null", Tahapan.class).fetch();
    }

    public static List<Tahapan> getAllFlagging() {
        return Query.find("SELECT flagging FROM tahapan WHERE active = '1' && flagging is not null && flagging != '-'", Tahapan.class).fetch();
    }

    public static Tahapan getNamaTahapan(String nama_tahapan) {
        return Query.find("SELECT * FROM tahapan WHERE active = '1' && nama_tahapan_alias='" + nama_tahapan + "'",
                Tahapan.class).first();
    }

    public static Tahapan getFlaggingTahapanPendaftaran() {
        return getByFlagString(TAHAPAN_REGISTRASI);
    }

    public static Tahapan getFlaggingTahapanPenjelasan() {
        return getByFlagString(TAHAPAN_PENJELASAN);
    }

	public static Tahapan getFlaggingTahapanPenawaran() {
		return getByFlagString(TAHAPAN_PENAWARAN);
	}

	public static Tahapan getFlaggingTahapanNegosiasi() {
		return getByFlagString(TAHAPAN_NEGOSIASI);
	}

	public static Tahapan getFlaggingTahapanRevisi() {
		return getByFlagString(TAHAPAN_REVISI);
	}

	public static Tahapan getByFlagString(String flag) {
		return Query.find("SELECT * FROM tahapan WHERE active = '1' AND flagging = ?", Tahapan.class, flag).first();
	}
}
