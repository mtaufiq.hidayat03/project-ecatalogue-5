package jobs;

import models.logging.Logging;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import utils.LogUtil;

/**
 * @author HanusaCloud on 12/6/2017
 */
@NoTransaction
public class LoggingJob extends Job {

    public static final String TAG = "LoggingJob";

    private Logging model;

    public LoggingJob(Logging model) {
        this.model = model;
    }

    @Override
    public void doJob() throws Exception {
         super.doJob();
         if (model != null) {
             LogUtil.d(TAG, "add logging into queue: " + System.currentTimeMillis());
             LogUtil.d(TAG, "current Thread: " + Thread.currentThread().getName());
             model.saveLog();
         }
    }
}
