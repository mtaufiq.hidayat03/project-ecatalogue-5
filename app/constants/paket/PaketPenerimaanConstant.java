package constants.paket;

/**
 * @author HanusaCloud on 11/1/2017
 */
public class PaketPenerimaanConstant {

    public static final String TABLE_NAME = "paket_penerimaan";

    public static final String ID = "id";
    public static final String PAKET_PEMBAYARAN_ID = "paket_pembayaran_id";
    public static final String PAKET_PENGIRIMAN_ID = "paket_pengiriman_id";
    public static final String PAKET_ID = "paket_id";

    public static final String MODIFIED_DATE = "modified_date";
    public static final String MODIFIED_BY = "modified_by";

}
