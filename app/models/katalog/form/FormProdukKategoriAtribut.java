package models.katalog.form;

import models.katalog.Komoditas;
import models.katalog.ProdukKategoriAtribut;
import models.katalog.komoditasmodel.KomoditasTemplateJadwal;
import models.prakatalog.Usulan;

import java.io.Serializable;
import java.util.List;

/**
 * Created by raihaniqbal on 9/7/17.
 */
public class FormProdukKategoriAtribut implements Serializable {

    public long kategoriId;
    public List<ProdukKategoriAtribut> kategoriAtributList;
}
