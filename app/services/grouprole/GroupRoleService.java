package services.grouprole;

import models.jcommon.util.CommonUtil;
import models.user.*;
import play.Logger;

import java.util.Arrays;

/**
 * @author HanusaCloud on 11/25/2017
 */
public class GroupRoleService {

    public static final String TAG = "GroupRoleService";

    private boolean isInsert = false;

    public GroupRoleService(boolean isInsert) {
        this.isInsert = isInsert;
    }

    public void store() {

    }

    public void storeRoles(UserRoleGrup userRoleGrup, Long[] role_item_ids, Long[] komoditas_ids, Long[] paket_ids ) {
        if (!isInsert) {
            UserRoleItem.deleteByUserRoleGrupId(userRoleGrup.id);
            UserRoleKomoditas.deleteByUserRoleGrupId(userRoleGrup.id);
            UserRolePaket.deleteByUserRoleGrupId(userRoleGrup.id);
            User.updateUserRoleBasicId(userRoleGrup.id, userRoleGrup.role_basic_id);
        }
        Logger.debug(TAG + " commodities: " + Arrays.toString(komoditas_ids));
        if(!CommonUtil.isEmpty(role_item_ids)){
            for(Long l : role_item_ids){
                new UserRoleItem(l, userRoleGrup.id).saveRole();
            }
        }

        if(komoditas_ids != null && !userRoleGrup.akses_seluruh_komoditas){
            for(Long i : komoditas_ids){
                new UserRoleKomoditas(i, userRoleGrup.id).saveRole();
            }
        }

        if(paket_ids != null && !userRoleGrup.akses_seluruh_paket){
            for(Long id : paket_ids){
                new UserRolePaket(id, userRoleGrup.id).saveRole();
            }
        }

    }

}
