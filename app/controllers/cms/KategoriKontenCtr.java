package controllers.cms;
import controllers.BaseController;
import models.cms.KategoriKonten;
import controllers.security.AllowAccess;
import models.cms.KategoriUtama;
import models.secman.Acl;
import play.i18n.Messages;

import java.util.List;

/**
 * Created by user on 10/2/17.
 */
public class KategoriKontenCtr extends BaseController{
    @AllowAccess({Acl.COM_KATEGORI_KONTEN})
    public static void index(){
        List<KategoriUtama> kategoriUtamaList = KategoriUtama.findAllActive();
        render(kategoriUtamaList);
    }

    @AllowAccess({Acl.COM_KATEGORI_KONTEN_ADD})
    public static void create(){
        List<KategoriUtama> kategoriUtamaList = KategoriUtama.findAllActive();
        render("cms/KategoriKontenCtr/create.html", kategoriUtamaList);
    }

    @AllowAccess({Acl.COM_KATEGORI_KONTEN_ADD})
    public static void createSubmit(Long konten, String nama_kategori, String deskripsi){
        KategoriKonten kategoriKonten = new KategoriKonten();
        kategoriKonten.konten_kategori_utama_id = konten;
        kategoriKonten.nama_kategori = nama_kategori;
        kategoriKonten.deskripsi = deskripsi;
        kategoriKonten.active = true;
        kategoriKonten.save();
        flash.success(Messages.get("notif.success.submit"));
        index();
    }

    @AllowAccess({Acl.COM_KATEGORI_KONTEN_EDIT})
    public static void edit(Long id){
        KategoriKonten viewKategoriKonten = KategoriKonten.findById(id);
        List<KategoriUtama> kategoriUtamaList = KategoriUtama.findAllActive();
        render("cms/KategoriKontenCtr/edit.html", viewKategoriKonten, kategoriUtamaList);
    }

    @AllowAccess({Acl.COM_KATEGORI_KONTEN_EDIT})
     public static void editSubmit(Long id_kategori, Long konten, String nama_kategori, String deskripsi){
        KategoriKonten kategoriKonten = KategoriKonten.findById(id_kategori);
        kategoriKonten.konten_kategori_utama_id = konten;
        kategoriKonten.nama_kategori = nama_kategori;
        kategoriKonten.deskripsi = deskripsi;
        kategoriKonten.active = true;
        kategoriKonten.save();
        flash.success(Messages.get("notif.success.update"));
        index();

    }

    @AllowAccess({Acl.COM_KATEGORI_KONTEN_DELETE})
    public static void delete(long id) {
        KategoriKonten.deleteById(id);
        flash.success(Messages.get("notif.success.delete"));
        index();
    }

    @AllowAccess({Acl.COM_KATEGORI_KONTEN_ADD})
    public static void listKategoriUtama(){
        render("cms/KategoriKontenCtr/listKategoriUtama.html");
    }

    @AllowAccess({Acl.COM_KATEGORI_KONTEN_ADD})
    public static void createKategoriUtama(){
        render("cms/KategoriKontenCtr/createKategoriUtama.html");
    }

    @AllowAccess({Acl.COM_KATEGORI_KONTEN_ADD})
    public static void createSubmitKategoriUtama(String nama_kategori_utama, String deskripsi){
        KategoriUtama kategoriUtama = new KategoriUtama();
        kategoriUtama.nama_kategori_utama = nama_kategori_utama;
        kategoriUtama.deskripsi = deskripsi;
        kategoriUtama.active = true;
        kategoriUtama.save();
        flash.success(Messages.get("notif.success.submit"));
        listKategoriUtama();
    }

    @AllowAccess({Acl.COM_KATEGORI_KONTEN_EDIT})
    public static void setAktivasi(long id, String param) {
        KategoriKonten model = KategoriKonten.findById(id);
        notFoundIfNull(model);
        switch (param) {
            case "non_aktif":
                model.setNonAktif();
                flash.success(Messages.get("notif.success.setnon_aktif"));
                break;
            case "aktif":
                model.setAktif();
                flash.success(Messages.get("notif.success.setaktif"));
                break;
        }
        index();
    }

    @AllowAccess({Acl.COM_KATEGORI_KONTEN_EDIT})
    public static void setAktivasiKategori(long id, String param) {
        KategoriUtama model = KategoriUtama.findById(id);
        notFoundIfNull(model);
        switch (param) {
            case "non_aktif":
                model.setNonAktif();
                flash.success(Messages.get("notif.success.setnon_aktif"));
                break;
            case "aktif":
                model.setAktif();
                flash.success(Messages.get("notif.success.setaktif"));
                break;
        }
        listKategoriUtama();
    }
}
