CREATE TABLE dokumen_kategori (
id int(11) AUTO_INCREMENT,
nama_kategori varchar(255),
active int(11),
created_date datetime,
created_by int(5),
modified_date datetime,
modified_by int(5),
deleted_date datetime,
deleted_by int(5),
PRIMARY KEY(id)
);

CREATE TABLE dokumen_upload (
id int(11) AUTO_INCREMENT,
dokumen_kategori_id int(11),
penyedia_id int(11),
penawaran_id int(11),
nama_dokumen varchar(255),
file_path varchar(100),
active int(11),
created_date datetime,
created_by int(5),
modified_date datetime,
modified_by int(5),
deleted_date datetime,
deleted_by int(5),
PRIMARY KEY(id)
);


INSERT INTO dokumen_kategori(nama_kategori,active,created_date,created_by) VALUE('BA Evaluasi',1,now(),2);
INSERT INTO dokumen_kategori(nama_kategori,active,created_date,created_by) VALUE('BA Pembuktian',1,now(),2);
INSERT INTO dokumen_kategori(nama_kategori,active,created_date,created_by) VALUE('Kontrak Katalog',1,now(),2);
INSERT INTO dokumen_kategori(nama_kategori,active,created_date,created_by) VALUE('SK Penetapan',1,now(),2);
INSERT INTO dokumen_kategori(nama_kategori,active,created_date,created_by) VALUE('Surat Pesanan',1,now(),2);