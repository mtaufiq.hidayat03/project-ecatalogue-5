package models.katalog.komoditasmodel.form;

import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 12/5/2017
 */
public class AttributePosition {

    public long id = 0;
    public String name = "";
    @SerializedName("parent_id")
    public long parentId = 0;
    public int depth = 0;
    public long left;
    public long right;
    public long modelId;

}
