package controllers.admin.komoditas;

import com.google.gson.JsonObject;
import controllers.BaseController;
import models.jcommon.datatable.DatatableQuery;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.util.TextUtils;
import utils.LogUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 11/30/2017
 */
public class KomoditasDataTable extends BaseController {

    public static final String TAG = "KomoditasDataTable";

    public static void cariKomoditas(CommoditySearch model){
        LogUtil.d(TAG, model);
        DatatableQuery query = new DatatableQuery(KomoditasResultHandler.DAFTAR_KOMODITAS)
                .select("kom.id, kom.nama_komoditas, kom.kode_komoditas, kom.kelas_harga, kk.nama_kategori, kom.terkunci, kom.active" +
                        ",(SELECT COUNT(*) FROM produk_kategori where komoditas_id = kom.id and active = 1) as total_kategori_produk" +
                        ",(SELECT COUNT(*) FROM `produk_kategori_atribut` where komoditas_id = kom.id and active = 1) as total_kategori_produk_atribut" +
                        ",(SELECT COUNT(*) FROM `komoditas_harga_atribut` where komoditas_id = kom.id and active = 1) as total_harga_atribut" +
                        ",kom.status")
                .from("komoditas kom JOIN komoditas_kategori kk ON kom.komoditas_kategori_id = kk.id");
        List<Object> params = new ArrayList<>();
       // params.add(BaseKatalogTable.AKTIF);
        StringBuilder sb = new StringBuilder();
        //sb.append("kom.active = ?");
        sb.append(" 1 = 1 ");
        if (!TextUtils.isEmpty(model.keyword)) {
            sb.append(" AND (kom.nama_komoditas LIKE ? or kom.kode_komoditas like ?)");
            params.add("%" + StringEscapeUtils.escapeSql(model.keyword) + "%");
            params.add("%" + StringEscapeUtils.escapeSql(model.keyword) + "%");
        }
        if (model.category > 0) {
            sb.append(" AND kom.komoditas_kategori_id =?");
            params.add(model.category);
        }
        if (model.negotiation > -1) {
            sb.append(" AND kom.perlu_negosiasi_harga =?");
            params.add(model.negotiation);
        }
        if (model.adds > -1) {
            sb.append(" AND kom.apakah_iklan =?");
            params.add(model.adds);
        }
        if (model.deliveryCost > -1) {
            sb.append(" AND kom.perlu_ongkir =?");
            params.add(model.deliveryCost);
        }
        if (model.approval > -1) {
            sb.append(" AND kom.perlu_approval_produk =?");
            params.add(model.approval);
        }
        if (model.isDuplicate > -1) {
            sb.append(" AND kom.apakah_paket_produk_duplikat =?");
            params.add(model.isDuplicate);
        }
        query.where(sb.toString());
        query.params(params.toArray());
        renderJSON(query.executeQuery());
    }

    public static void cariAtributHarga(Long id){
        DatatableQuery query = new DatatableQuery(KomoditasResultHandler.DAFTAR_ATRIBUT_HARGA)
                .select("id, label_harga, apakah_ongkir, apakah_harga_utama, apakah_harga_retail, apakah_harga_pemerintah")
                .from("komoditas_harga_atribut");
        query.where("komoditas_id = ? and active = ?");
        query.params(id,1);
        renderJSON(query.executeQuery());
    }

    public static void cariTemplateJadwal(long komoditas_id){
        JsonObject json= new DatatableQuery(KomoditasResultHandler.DAFTAR_TEMPLATE_JADWAL)
                .select("ktj.id,ktj.nama_template,ktj.agr_tahapan_jadwal,ktj.komoditas_id," +
                        "(select count(*) jumlah from usulan where template_jadwal_id = ktj.id) apakah_dipakai")
                .from("komoditas_template_jadwal ktj")
                .where("ktj.active=? and ktj.komoditas_id=?")
                .params(1,komoditas_id)
                .executeQuery();
        renderJSON(json);
    }

}
