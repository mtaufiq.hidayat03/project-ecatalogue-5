package models.purchasing.contracts;

import constants.paket.PaketStatusConstant;
import models.purchasing.negosiasi.PaketProdukHeader;
import org.apache.http.util.TextUtils;

import static constants.paket.PaketStatusConstant.*;

/**
 * @author HanusaCloud on 11/8/2017
 */
public interface PackageStatusContract {

    Long getId();

    Long getPackageId();

    String getCurrentPackageStatus();

    String getCurrentNegotiationStatus();

    String getTo();

    String getFrom();

    PaketProdukHeader getLatestHeader();

    Integer getModifiedBy();

    void setLatestNegotiationHeader(PaketProdukHeader model);

    default void withLatestNegotiation() {
        if (getPackageId() != null) {
            PaketProdukHeader model = PaketProdukHeader.getLatestHeader(getPackageId());
            setLatestNegotiationHeader(model);
            if (model != null) {
                model.withDetail();
            }
        }
    }

    default boolean isPreparation() {
        return getCurrentPackageStatus().equalsIgnoreCase(STATUS_PREPARATION);
    }

    default boolean isNegotiation() {
        return !TextUtils.isEmpty(getCurrentNegotiationStatus())
                && getCurrentNegotiationStatus().equalsIgnoreCase(STATUS_NEGOTIATION);
    }

    default boolean isNegotiationDone() {
        return !TextUtils.isEmpty(getCurrentNegotiationStatus())
                && getCurrentNegotiationStatus().equalsIgnoreCase("sepakat");
    }

    default boolean isBuyerApproved() {
        return !TextUtils.isEmpty(getCurrentPackageStatus())
                && getCurrentPackageStatus().equalsIgnoreCase(STATUS_PANITIA_APPROVED);
    }

    default boolean isBuyerRejected() {
        return !TextUtils.isEmpty(getCurrentPackageStatus())
                && getCurrentPackageStatus().equalsIgnoreCase(STATUS_PANITIA_REJECTED);
    }

    default boolean isDrafted() {
        return !TextUtils.isEmpty(getCurrentPackageStatus())
                && getCurrentPackageStatus().equalsIgnoreCase(STATUS_DRAFT);
    }

    default boolean isCanceld() {
        return !TextUtils.isEmpty(getCurrentPackageStatus())
                && getCurrentPackageStatus().equalsIgnoreCase(STATUS_CANCEL);
    }
    default boolean isProviderApproved() {
        return !TextUtils.isEmpty(getCurrentPackageStatus())
                && getCurrentPackageStatus().equalsIgnoreCase(STATUS_PROVIDER_APPROVED);
    }

    default boolean isProviderRejected() {
        return !TextUtils.isEmpty(getCurrentPackageStatus())
                && getCurrentPackageStatus().equalsIgnoreCase(STATUS_PROVIDER_REJECTED);
    }

    default boolean isPpkApproved() {
        return !TextUtils.isEmpty(getCurrentPackageStatus())
                && getCurrentPackageStatus().equalsIgnoreCase(STATUS_PPK_APPROVED);
    }

    default boolean isPpkRejected() {
        return !TextUtils.isEmpty(getCurrentPackageStatus())
                && getCurrentPackageStatus().equalsIgnoreCase(STATUS_PPK_REJECTED);
    }

    default boolean isToPpk() {
        return !TextUtils.isEmpty(getTo())
                && getTo().equalsIgnoreCase(PaketStatusConstant.USER_PPK);
    }

    default boolean isToPp() {
        return !TextUtils.isEmpty(getTo())
                && (getTo().equalsIgnoreCase(PaketStatusConstant.USER_PANITIA) || getTo().equalsIgnoreCase(PaketStatusConstant.USER_PANITIA_PP));
    }

    default boolean isToProvider() {
        return !TextUtils.isEmpty(getTo())
                && getTo().equalsIgnoreCase(PaketStatusConstant.USER_PROVIDER);
    }

    default boolean isDistributor() {
        return !TextUtils.isEmpty(getTo())
                && getTo().equalsIgnoreCase(PaketStatusConstant.USER_DISTRIBUTOR);
    }

    default boolean isPackageRejected() {
        return isProviderRejected() || isPpkRejected() || isBuyerRejected();
    }

    default boolean isAllowed() {
        return !isFinish() && !isPackageRejected();
    }

    default boolean fromCommittee() {
        return !TextUtils.isEmpty(getFrom())
                && getFrom().equalsIgnoreCase(PaketStatusConstant.USER_PANITIA);
    }

    default boolean fromProvider() {
        return !TextUtils.isEmpty(getFrom())
                && getFrom().equalsIgnoreCase(PaketStatusConstant.USER_PROVIDER);
    }

    default boolean fromBuyer() {
        return !TextUtils.isEmpty(getFrom())
                && getFrom().equalsIgnoreCase(PaketStatusConstant.USER_PPK);
    }

    default boolean isDelivery() {
        return getCurrentPackageStatus().equalsIgnoreCase(STATUS_DELIVERY);
    }

    default boolean isFinish() {
        return getCurrentPackageStatus().equalsIgnoreCase(STATUS_FINISHED);
    }

    default String getLocalizedPackageStatus() {
        return PaketStatusConstant.getLocalizedStatus(getCurrentPackageStatus());
    }

}
