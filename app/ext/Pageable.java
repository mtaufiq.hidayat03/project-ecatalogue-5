package ext;

import ext.contracts.PaginateAble;

/**
 * Created by rayhanfrds on 9/18/17.
 */
public class Pageable {

	public static final int MAX_SIZE_PER_PAGE = 24;
	public static final int MAX_PAGE_ITEM_DISPLAY = 10;

	private int curPage;
	private int limit = MAX_SIZE_PER_PAGE;
	private long total = 0;
	private String link;
	private String prevLink;
	private String nextLink;
	private boolean previous;
	private boolean next;
	private int[] pageNumbers;
	private int pageCount;
	private int show = 0;
	private int from = 0;
	public String extra = "";
	public Pageable() {}

	public Pageable(int curPage, int limit, int total, String link) {
		this(curPage, limit, (long) total, link);
	}

	public Pageable(PaginateAble paginateAble, String extra) {
		this.extra = extra;
		setLimit(paginateAble.getMax());
		setTotal(paginateAble.getTotal());
		calculatePageCount();
		setCurPage(paginateAble.getPage() > pageCount ? 1 : paginateAble.getPage());
		this.link = paginateAble.getQueryString();
		this.show = curPage == 1 ? 1 : paginateAble.getFrom() + 1;
		this.from = paginateAble.getTotalCurrentPage() < paginateAble.getMax()
				? (int) paginateAble.getTotal()
				: paginateAble.getMax() * curPage;
		this.previous = this.curPage != 1;
		this.next = this.curPage * limit < total;
		pageNumbers = new int[pageCount];
		for (int i = 0; i < pageNumbers.length; i++) {
			pageNumbers[i] = i + 1;
		}
		setLinks();
	}
	public Pageable(PaginateAble paginateAble) {
		setLimit(paginateAble.getMax());
		setTotal(paginateAble.getTotal());
		calculatePageCount();
		setCurPage(paginateAble.getPage() > pageCount ? 1 : paginateAble.getPage());
		this.link = paginateAble.getQueryString();
		this.show = curPage == 1 ? 1 : paginateAble.getFrom() + 1;
		this.from = paginateAble.getTotalCurrentPage() < paginateAble.getMax()
				? (int) paginateAble.getTotal()
				: paginateAble.getMax() * curPage;
		this.previous = this.curPage != 1;
		this.next = this.curPage * limit < total;
		pageNumbers = new int[pageCount];
		for (int i = 0; i < pageNumbers.length; i++) {
			pageNumbers[i] = i + 1;
		}
		setLinks();
	}

	public Pageable(int curPage, int limit, long total, String link) {
		this.limit = limit;
		this.total = total;
		this.pageCount = (int) Math.ceil(total * 1.0 / limit * 1.0);
		this.curPage = curPage > pageCount ? 1 : curPage;
		this.link = link;
		this.show = curPage == 1 ? 1 : limit + 1;
		this.from = (limit * curPage) < total ? (limit * curPage) : (int) total;
		this.previous = this.curPage != 1;
		this.next = this.curPage * limit < total;
		pageNumbers = new int[pageCount];
		for (int i = 0; i < pageNumbers.length; i++) {
			pageNumbers[i] = i + 1;
		}
		setLinks();
	}

	private void setLinks() {
		String ext = "";//extra manual params
		if(!extra.isEmpty()){
			ext = extra+"&";
		}
		if (!link.contains("?")) {
			link += "?"+ext;
		} else if (!link.endsWith("&") && !link.endsWith("?")) {
			link += "&"+ext;
		}

		if(!extra.isEmpty()){
			ext = "&"+extra;
		}
		if (previous) {
			prevLink = this.link + "offset=" + (curPage - 1)+ext;
		}
		if (next) {
			nextLink = this.link + "offset=" + (curPage + 1)+ext;;
		}
	}

	private void calculatePageCount() {
		this.pageCount = (int) Math.ceil(total * 1.0 / limit * 1.0);
	}

	public Pageable(int limit, int total, String link) {
		this(1, limit, total, link);
	}

	public void nextPage() {
		if (isNext()) {
			setCurPage(getCurPage() + 1);
			setPrevious(getCurPage() != 1);
			setNext(getCurPage() * getLimit() < getTotal());
		}
	}

	public void previousPage() {
		if (isPrevious()) {
			setCurPage(getCurPage() - 1);
			setPrevious(getCurPage() != 1);
			setNext(getCurPage() * getLimit() < getTotal());
		}
	}

	/**
	 * @return the curPage
	 */
	public int getCurPage() {
		return curPage;
	}

	/**
	 * @param curPage the curPage to set
	 */
	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}

	/**
	 * @return the limit
	 */
	public int getLimit() {
		return limit;
	}

	/**
	 * @param limit the limit to set
	 */
	public void setLimit(int limit) {
		this.limit = limit;
	}

	/**
	 * @return the total
	 */
	public long getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(long total) {
		this.total = total;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the prevLink
	 */
	public String getPrevLink() {
		return prevLink;
	}

	/**
	 * @param prevLink the prevLink to set
	 */
	public void setPrevLink(String prevLink) {
		this.prevLink = prevLink;
	}

	/**
	 * @return the nextLink
	 */
	public String getNextLink() {
		return nextLink;
	}

	/**
	 * @param nextLink the nextLink to set
	 */
	public void setNextLink(String nextLink) {
		this.nextLink = nextLink;
	}

	/**
	 * @return the previous
	 */
	public boolean isPrevious() {
		return previous;
	}

	/**
	 * @param previous the previous to set
	 */
	public void setPrevious(boolean previous) {
		this.previous = previous;
	}

	/**
	 * @return the next
	 */
	public boolean isNext() {
		return next;
	}

	/**
	 * @param next the next to set
	 */
	public void setNext(boolean next) {
		this.next = next;
	}

	/**
	 * @return the pageNumbers
	 */
	public int[] getPageNumbers() {
		return pageNumbers;
	}

	/**
	 * @param pageNumbers the pageNumbers to set
	 */
	public void setPageNumbers(int[] pageNumbers) {
		this.pageNumbers = pageNumbers;
	}

	public int getCurrentTotal() {
		return show;
	}

	public int from() {
		return from;
	}

}
