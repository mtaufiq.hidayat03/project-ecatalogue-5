package models.util.sikap;

import play.data.validation.Required;

import java.util.List;

/**
 * Created by dadang on 7/11/17.
 */
public class TenagaAhli {

	@Required
	public long rkn_id;
	@Required
	public long sta_id;
	@Required
	public long sta_status;
	@Required
	public long sta_pengalaman;
	@Required
	public String sta_nama;
	@Required
	public String sta_tgllahir;
	@Required
	public String sta_alamat;
	@Required
	public String sta_jabatan;
	@Required
	public String sta_pendidikan;
	@Required
	public String sta_keahlian;
	@Required
	public String sta_email;
	@Required
	public String sta_jenis_kelamin;
	@Required
	public String sta_kewarganegaraan;
	@Required
	public String sta_ktp;

	public String status_verifikasi;
	public boolean deleted = false;
	public String auditupdate;
	public String createdAt;
	public boolean is_verified = false;

	public List<TenagaAhliDetail> pengalaman;
	public List<TenagaAhliDetail> pendidikan;
	public List<TenagaAhliDetail> sertifikasi;
	public List<TenagaAhliDetail> bahasa;

	public String alasan_pembatalan = "";
	public String keterangan_pembuktian = "";

}
