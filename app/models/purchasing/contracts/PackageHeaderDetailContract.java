package models.purchasing.contracts;

import models.purchasing.negosiasi.PaketProdukNegoDetail;

/**
 * @author HanusaCloud on 11/13/2017
 */
public interface PackageHeaderDetailContract {

    Long getId();

    Long getPackageId();

    void setDetail(PaketProdukNegoDetail model);

    default void withDetail() {
        if (getId() != null && getPackageId() != null) {
            setDetail(PaketProdukNegoDetail.getByHeaderId(getId()));
        }
    }

}
