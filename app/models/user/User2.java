package models.user;

import models.BaseKatalogTable;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;

@Table(name="user_2")
public class User2 extends BaseKatalogTable {

    public static final String PROVIDER_AND_DISTRIBUTOR = "penyedia_dan_distributor";

    @Id
    public Long id;

    @Required
    public String user_name;

    //	@Required
    public String user_password;

    public static List<User2> findAllPenyediaDistributor(){
        String query = "select distinct(u2.id), u2.user_name, u2.user_password, u2.rkn_id from user_2 u2\n" +
                "join user_role_grup ug on ug.id = u2.user_role_grup_id\n" +
                "join user u on u.id = u2.id and u.rkn_id is null\n" +
                "where u2.user_role_grup_id in (2,10,19) and\n" +
                "u2.active=1 and u2.user_password <> ''\n" +
                "and u2.rkn_id is NULL";
        return Query.find(query, User2.class).fetch();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }
}
