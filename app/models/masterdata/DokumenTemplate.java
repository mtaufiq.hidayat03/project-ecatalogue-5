package models.masterdata;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;

@Table(name = "dokumen_template")
public class DokumenTemplate extends BaseKatalogTable {

	@Id
	public Long id;
	public Long konten_kategori_id;
	public String jenis_dokumen;
	public String template;
	public boolean active;

	public static final String kategori_berita_acara_id = "12";

	public static List<DokumenTemplate> findAllBAActive(){
		return DokumenTemplate.find("active = 1 and konten_kategori_id = " + kategori_berita_acara_id + " ORDER BY jenis_dokumen").fetch();
	}
}
