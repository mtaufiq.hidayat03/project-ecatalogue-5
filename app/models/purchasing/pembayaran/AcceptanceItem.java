package models.purchasing.pembayaran;

import ext.FormatUtils;
import utils.DateTimeUtils;
import utils.KatalogUtils;
import utils.UrlUtil;

import java.util.Date;

/**
 * @author HanusaCloud on 11/1/2017
 */
public class AcceptanceItem {

    public Long id;
    public Long packageId;
    public Date documentDate;
    public String documentNumber;
    public Double total;
    public Date acceptanceDate;
    public String description;
    public Long paymentId;
    public String verificationId;

    public String getReadableDocumentDate() {
        return DateTimeUtils.parseToReadableDate(documentDate);
    }

    public String getReadableAcceptanceDate() {
        return DateTimeUtils.parseToReadableDate(acceptanceDate);
    }

    public String getFormattedTotal() {
        return FormatUtils.formatCurrencyRupiah(total);
    }

    public String getAcceptanceDetailUrl() {
        return UrlUtil.builder()
                .setUrl("purchasing.PenerimaanCtr.detailRiwayatPenerimaan")
                .setParam("id", id)
                .setParam("paket_id", packageId)
                .build();
    }

    public String getFormattedDocNumber() {
        return KatalogUtils.getFormattedDocNumber(documentNumber);
    }

}
