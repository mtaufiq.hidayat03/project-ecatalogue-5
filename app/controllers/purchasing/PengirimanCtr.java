package controllers.purchasing;

import  com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.jcommon.blob.BlobTable;
import models.purchasing.Paket;
import models.purchasing.base.ProductJson;
import models.purchasing.pengiriman.LampiranPengiriman;
import models.purchasing.pengiriman.PaketRiwayatPengiriman;
import models.purchasing.pengiriman.form.DeliveryDataForm;
import models.purchasing.pengiriman.form.DeliveryItemJson;
import models.secman.Acl;
import play.Logger;
import play.i18n.Messages;
import repositories.paket.DeliveryRepository;
import services.paket.DeliveryService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 10/24/2017
 */
public class PengirimanCtr extends BaseController {

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PENGIRIMAN})
    public static void riwayatPengiriman(Long paket_id) {
        Paket model = Paket.findById(paket_id);
        notFoundIfNull(model);
        if (model != null) {
            model.withDeliveries();
            model.withStatusMessage(getAktifUser());
        }
        renderArgs.put("paket", model);
        renderArgs.put("activeUser", getAktifUser());
        render("purchasing/PaketCtr/pengiriman/riwayatPengiriman.html");
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PENGIRIMAN_INPUT})
    public static void createPengiriman(String language, Long paket_id) {
        Paket model = Paket.findById(paket_id);
        notFoundIfNull(model);
        if (!model.accessDelivery(getAktifUser())) {
            notFound();
        }
        if (model != null) {
            model.deliveryHistory = new PaketRiwayatPengiriman();
           // model.deliveryHistory.products = DeliveryRepository.getProductsToBeDelivered(paket_id);
        }
        renderArgs.put("paket", model);
        renderArgs.put("action", "create");
        render("purchasing/PaketCtr/pengiriman/createPengiriman.html");
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PENGIRIMAN_INPUT})
    public static void updatePengiriman(Long paket_id, Long id) {
        Paket model = Paket.findById(paket_id);
        notFoundIfNull(model);
        if (!model.accessDelivery(getAktifUser())) {
            notFound();
        }
        if (model != null) {
            model.deliveryHistory = PaketRiwayatPengiriman.findById(id);
            notFoundIfNull(model.deliveryHistory);
            if (model.deliveryHistory != null) {
                model.deliveryHistory.withProducts();
            }
        }
        renderArgs.put("paket", model);
        renderArgs.put("action", "update");
        render("purchasing/PaketCtr/pengiriman/createPengiriman.html");
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PENGIRIMAN_INPUT})
    public static void storePengiriman(Long paket_id, DeliveryDataForm deliveryDataForm) {
        checkAuthenticity();
        try {
            deliveryDataForm.packageId = paket_id;
            Logger.debug(new GsonBuilder().setPrettyPrinting().create().toJson(deliveryDataForm));
            DeliveryService.instance().store(deliveryDataForm);
            flash.success(Messages.get("notif.success.submit"));
        } catch (Exception e) {
            e.printStackTrace();
            flash.error(Messages.get("notif.failed.submit"));
        }
        riwayatPengiriman(paket_id);
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PENGIRIMAN_INPUT})
    public static void deletePengiriman(Long paket_id, Long id) {
        if (!DeliveryService.instance().deleteDelivery(id, getAktifUser())) {
            flash.error(Messages.get("notif.failed.delete"));
        }
        flash.success(Messages.get("notif.success.delete"));
        riwayatPengiriman(paket_id);
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PENGIRIMAN})
    public static void downloadLampiran(Long id) {
        LampiranPengiriman model = LampiranPengiriman.findById(id);
        if (model != null) {
            BlobTable blobTable = model.withBlobContent();
            if (blobTable != null) {
                renderBinary(blobTable.getFile(), model.original_file_name);
            } else if (model.getFile().exists()) {
                renderBinary(model.getFile(), model.original_file_name);
            } else {
                error(Messages.get("notif.files_not_found"));
            }
        } else {
            error(Messages.get("notif.files_not_found"));
        }
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PENGIRIMAN_INPUT})
    public static void searchProduct(Long paket_id, String keyword, String exception) {
        checkAuthenticity();
        List<ProductJson> results = DeliveryService.instance().searchProduct(paket_id, keyword, exception);
        renderJSON(results);
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PENGIRIMAN})
    public static void detailRiwayatPengiriman(Long paket_id, Long id) {
        Paket model = Paket.findById(paket_id);
        notFoundIfNull(model);
        if (model != null) {
            model.deliveryHistory = PaketRiwayatPengiriman.findById(id);
            notFoundIfNull(model.deliveryHistory);
            if (model.deliveryHistory != null) {
                model.deliveryHistory.withAttachment();
                model.deliveryHistory.withProducts();
            }
        }
        renderArgs.put("paket", model);
        render("purchasing/PaketCtr/pengiriman/detailRiwayatPengiriman.html");
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PENGIRIMAN_INPUT})
    public static void deleteProduk(Long id) {
        JsonObject result = new JsonObject();
        result.addProperty("status", DeliveryService.instance().deleteProduct(id));
        result.addProperty("id", id);
        renderJSON(result);
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PENGIRIMAN_INPUT})
    public static void searchDelivery(Long paket_id, String keyword) {
        checkAuthenticity();
        List<PaketRiwayatPengiriman> result = DeliveryRepository.searchDelivery(paket_id, keyword);
        List<DeliveryItemJson> histories = result.stream().map(e -> new DeliveryItemJson(e)).collect(Collectors.toList());
        renderJSON(histories);
    }

    @AllowAccess({Acl.COM_PAKET_RIWAYAT_PENGIRIMAN_INPUT})
    public static void getDeliveryProductsApi(Long paket_id, Long id) {
        checkAuthenticity();
        List<ProductJson> results = DeliveryService.instance().getProductsById(paket_id, id);
        renderJSON(results);
    }

}
