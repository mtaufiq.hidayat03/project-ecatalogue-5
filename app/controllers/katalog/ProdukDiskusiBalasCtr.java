package controllers.katalog;

import com.google.gson.JsonObject;
import controllers.BaseController;
import models.common.AktifUser;
import models.katalog.ProdukDiskusi;
import models.katalog.ProdukDiskusiBalas;

import java.io.IOException;

public class ProdukDiskusiBalasCtr extends BaseController {

    public static void createSubmitCommentBalas(Long produk_diskusi_id, String comment_balas, Long produk_id)throws IOException {

        JsonObject jsonDiskusiBalas = new JsonObject();
        AktifUser aktifUser = getAktifUser();

        ProdukDiskusiBalas prdkDiskusiBalas = new ProdukDiskusiBalas();
        prdkDiskusiBalas.produk_id = produk_id;
        prdkDiskusiBalas.produk_diskusi_id = produk_diskusi_id;
        prdkDiskusiBalas.user_id = aktifUser.user_id;
        prdkDiskusiBalas.diskusi_balas = comment_balas;
        prdkDiskusiBalas.user_nama = aktifUser.nama_lengkap;
        prdkDiskusiBalas.active = true;
        prdkDiskusiBalas.save();

        String userLoginString = ""+aktifUser.user_id;

        ProdukDiskusi produkDiskusi = ProdukDiskusi.findById(produk_diskusi_id);
        String produkDiskusiIdString = ""+produkDiskusi.user_penerima;
        if (userLoginString.equals(produkDiskusiIdString)){
            produkDiskusi.status_komentar = 0;
            produkDiskusi.active = true;
            produkDiskusi.save();
        }else {
            produkDiskusi.status_komentar = 1;
            produkDiskusi.active = true;
            produkDiskusi.save();
        }

        jsonDiskusiBalas.addProperty("namaUser", aktifUser.nama_lengkap);
        renderJSON(jsonDiskusiBalas);
    }

}
