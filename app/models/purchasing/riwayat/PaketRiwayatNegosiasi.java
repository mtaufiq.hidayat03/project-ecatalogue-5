package models.purchasing.riwayat;

import models.masterdata.Kurs;
import models.masterdata.KursNilai;
import models.purchasing.contracts.PriceContract;
import play.Logger;
import play.db.jdbc.Query;
import utils.DateTimeUtils;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import ext.FormatUtils;

/**
 * @author HanusaCloud on 10/16/2017
 */
public class PaketRiwayatNegosiasi implements PriceContract {

    public Long id;
    public Long paket_id;
    public int revisi;
    public Date created_date;
    public Double harga_satuan;
    public Double harga_total;
    public Double harga_ongkir;
    public Double konversi_harga_satuan;
    public Double konversi_harga_ongkir;
    public Double konversi_harga_total;

    public String nama_lengkap;
    public Long penyedia_setuju_user_id;
    public Long panitia_setuju_user_id;

    public String nama_manufaktur;
    public String nama_produk;
    public String no_produk;
    public Long produk_id;
    public Long kuantitas;
    public Long harga_kurs_id;
    public String nama_kurs;
    public Date batas_pengiriman;
    public String catatan;
    public int total_produk;

    public String getCheckedUser(){
        String result = "";
        if(this.penyedia_setuju_user_id != null) result = "<i class='fa fa-check-circle'></i>";
        if(this.panitia_setuju_user_id != null) result += "<i class='fa fa-check-circle-o'></i>";
        //Draft paket Riwayat negosiasi
        if(result.isEmpty() && revisi > 0){
            result ="Draft";
        }
        return result;
    }

    public String getRevision() {
        return String.format("Rev. %1s", revisi);
    }

    @Override
    public Double getUnitPrice() {
        return this.harga_satuan;
    }

    @Override
    public Double getDeliveryCost() {
        return this.harga_ongkir;
    }

    @Override
    public Double getTotalPrice() {
        return this.harga_total;
    }

    @Override
    public Double getConversionUnitPrice() {
        return this.konversi_harga_satuan;
    }

    @Override
    public Double getConversionDeliveryCost() {
        return this.konversi_harga_ongkir;
    }

    @Override
    public Double getConversionTotalPrice() {
        return this.konversi_harga_total;
    }

    @Override
    public Double getBestPrice() {
        return null;
    }

    @Override
    public Double getConversionBestPrice() {
        return null;
    }

    public String getRevisionDate() {
        return DateTimeUtils.parseToReadableDate(created_date);
    }

    public static List<PaketRiwayatNegosiasi> getHistoriesByPackageIdAndProductId(Long packageId, Long productId) {
        final String query = "SELECT h.paket_id, h.revisi, d.paket_produk_id, " +
                "d.harga_satuan, d.id, d.harga_total, d.catatan, d.harga_ongkir, d.created_date, " +
                "d.konversi_harga_ongkir, d.konversi_harga_satuan, d.konversi_harga_total, d.created_by " +
                "FROM paket_produk_nego_header h " +
                "JOIN paket_produk_nego_detail d " +
                "ON d.nego_header_id = h.id " +
                "WHERE h.paket_id =? " +
                "AND d.paket_produk_id =? AND d.active = 1 " +
                "ORDER BY h.revisi ASC;";
        Logger.debug(query);
        return Query.find(query, PaketRiwayatNegosiasi.class, packageId, productId).fetch();
    }

    public static List<PaketRiwayatNegosiasi> getHistoriesByPackageId(Long packageId) {
        final String query = "SELECT h.paket_id, h.revisi, d.paket_produk_id, " +
                "d.harga_satuan, d.id, d.harga_total, d.catatan, d.harga_ongkir, d.created_date, " +
                "d.konversi_harga_ongkir, d.konversi_harga_satuan, d.konversi_harga_total, d.created_by " +
                "FROM paket_produk_nego_header h " +
                "JOIN paket_produk_nego_detail d " +
                "ON d.nego_header_id = h.id " +
                "WHERE h.paket_id =? AND d.active = 1 " +
                "ORDER BY h.revisi ASC;";
        return Query.find(query, PaketRiwayatNegosiasi.class, packageId).fetch();
    }

    public static List<PaketRiwayatNegosiasi> getNegotiationTotalByPackageId(Long packageId) {
        final String query = "SELECT h.revisi, " +
                "h.created_date, u.nama_lengkap, h.penyedia_setuju_user_id, h.panitia_setuju_user_id," +
                "SUM(d.harga_total) as harga_total, " +
                "SUM(d.konversi_harga_total) as konversi_harga_total, " +
                "count(d.id) as total_produk "+
                "FROM paket_produk_nego_header h " +
                "JOIN paket_produk_nego_detail d " +
                "ON d.nego_header_id = h.id " +
                "left join `user` u on h.created_by = u.id " +
                "WHERE h.paket_id =? AND d.active = 1 " +
                "GROUP BY h.revisi " +
                "ORDER BY h.revisi ASC";
        return Query.find(query, PaketRiwayatNegosiasi.class, packageId).fetch();
    }

    public static List<PaketRiwayatNegosiasi> getNegotiationHistoryByPackageId(Long packageId) {
        final String query = "SELECT h.revisi, " +
                "h.created_date, " +
                "d.produk_id, " +
                "m.nama_manufaktur, " +
                "p.nama_produk, " +
                "p.no_produk, " +
                "d.kuantitas, " +
                "p.harga_kurs_id, " +
                "k.nama_kurs, " +
                "d.harga_satuan, " +
                "d.harga_ongkir, " +
                "d.batas_pengiriman, " +
                "d.harga_total, " +
                "d.konversi_harga_satuan, " +
                "d.konversi_harga_ongkir, " +
                "d.konversi_harga_total, " +
                "d.catatan, " +
                "h.penyedia_setuju_user_id, " +
                "h.panitia_setuju_user_id, " +
                "h.paket_id " +
                "FROM paket_produk_nego_header h " +
                "JOIN paket_produk_nego_detail d ON d.nego_header_id = h.id " +
                "join produk p on d.produk_id = p.id " +
                "join kurs k on p.harga_kurs_id = k.id " +
                "join manufaktur m on p.manufaktur_id = m.id  " +
                "WHERE h.paket_id = ? AND d.active = 1 " +
                "GROUP BY h.revisi, p.nama_produk " +
                "ORDER BY h.revisi ASC";
        return Query.find(query, PaketRiwayatNegosiasi.class, packageId).fetch();
    }
    
	public String getFormattedTotalPriceInUSD() {
		try {
			final Kurs kurs = Kurs.getUsd();
			final KursNilai kn = KursNilai.getNewestKursByKursId(kurs.id);
			final Double nilai_jual = kn.nilai_jual;
			final Double harga_total_dalam_usd=harga_total / nilai_jual;
			return FormatUtils.formatCurrency(harga_total_dalam_usd, Locale.US);
		} catch (Exception e) {
			e.printStackTrace();
			return "-";
		}
	}


}
