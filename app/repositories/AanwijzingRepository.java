package repositories;

import controllers.BaseController;
import models.aanwijzing.AanwijzingAnswer;
import models.aanwijzing.AanwijzingQuestion;
import play.cache.CacheFor;
import play.db.jdbc.Query;

import java.util.List;

public class AanwijzingRepository extends BaseController {

    public static List<AanwijzingQuestion> findAll(Integer produk_id) {
        return AanwijzingQuestion.find("produk_id ='" + produk_id + "' AND deleted_date is NULL and status=1").fetch();
    }

    public static List<AanwijzingQuestion> findAllWithSearch(Integer produk_id, String search) {
        String queryExtra = "";
        if (search != null && !search.isEmpty()) {
            queryExtra = " and (judul_pertanyaan LIKE '%"+search+"%' or pertanyaan LIKE '%"+search+"%')";
        }
        return AanwijzingQuestion.find("produk_id ='" + produk_id + "' AND deleted_date is NULL and status=1"+queryExtra).fetch();
    }

    public static List<AanwijzingQuestion> getDataAanwijzingQuestion(Integer start, Integer max_konten, Integer produk_id, String search) {
        return findQuestionByLimit((long) start, (long) max_konten, produk_id, search);
    }

    @CacheFor("5mn")
    public static List<AanwijzingQuestion> findLastesAanwijzing(Integer produk_id){
        String query = "SELECT * FROM question " +
                "WHERE produk_id = '" + produk_id + "' " +
                "and deleted_date is NULL and status=1 " +
                "ORDER BY created_date DESC LIMIT 4";

        return Query.find(query, AanwijzingQuestion.class).fetch();
    }

    public static List<AanwijzingQuestion> findQuestionByLimit(Long start, Long end, Integer produk_id, String search) {
        String query = "SELECT * FROM question " +
                "WHERE produk_id = '" + produk_id + "' " +
                "and deleted_date is NULL and status=1 ";
        if (search != null && !search.isEmpty()) {
            query += "and (judul_pertanyaan LIKE '%"+search+"%' or pertanyaan LIKE '%"+search+"%') ";
        }
        query += "ORDER BY created_date DESC LIMIT " + start + "," + end;
        return Query.find(query, AanwijzingQuestion.class).fetch();
    }

    public static AanwijzingQuestion getDataAanwijzingQuestionDetail(Integer produk_id, Long id) {
        return Query.find("SELECT * FROM question WHERE produk_id ='" + produk_id +
                "' AND id = '"+ id +"' AND deleted_date is NULL AND status=1", AanwijzingQuestion.class).first();
    }

    public static List<AanwijzingAnswer> findAllAnswer(Integer produk_id, Long id) {
        return AanwijzingAnswer.find("produk_id ='" + produk_id + "' AND pertanyaan_id ='" + id + "' AND deleted_date is NULL and status=1").fetch();
    }

    public static List<AanwijzingAnswer> findAllAnswerWithSearch(Integer produk_id, Long id, String search) {
        String queryExtra = "";
        if (search != null && !search.isEmpty()) {
            queryExtra = " and (jawaban LIKE '%"+search+"%')";
        }
        return AanwijzingAnswer.find("produk_id ='" + produk_id + "' AND pertanyaan_id ='" + id + "' AND deleted_date is NULL and status=1"+queryExtra).fetch();
    }

    public static List<AanwijzingAnswer> getDataAanwijzingAnswer(Integer start, Integer max_konten, Integer produk_id, Long id, String search) {
        return findAnswerByLimit((long) start, (long) max_konten, produk_id, id, search);
    }

    @CacheFor("5mn")
    public static List<AanwijzingAnswer> findLastesAanwijzingAnswer(Integer produk_id, Long id){
        String query = "SELECT * FROM question_answer " +
                "WHERE produk_id = '" + produk_id + "' " +
                "AND pertanyaan_id = '"+ id +"' " +
                "and deleted_date is NULL and status=1 " +
                "ORDER BY created_date DESC LIMIT 4";

        return Query.find(query, AanwijzingAnswer.class).fetch();
    }

    public static List<AanwijzingAnswer> findAnswerByLimit(Long start, Long end, Integer produk_id, Long id, String search) {
        String query = "SELECT * FROM question_answer " +
                "WHERE produk_id = '" + produk_id + "' " +
                "AND pertanyaan_id = '"+ id +"' " +
                "AND deleted_date is NULL AND status=1 ";
        if (search != null && !search.isEmpty()) {
            query += "and (jawaban LIKE '%"+search+"%') ";
        }
        query += "ORDER BY created_date DESC LIMIT " + start + "," + end;
        return Query.find(query, AanwijzingAnswer.class).fetch();
    }

    public static AanwijzingAnswer getAnswerBy(Long id, Integer userId) {
        final AanwijzingAnswer model =  AanwijzingAnswer.find("id =? AND created_by =? AND deleted_date IS NULL AND status=1", id, userId).first();
        return model;
    }
}
