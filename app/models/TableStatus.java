package models;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Query;

/**
 * Created by dadang on 11/8/17.
 */


public class TableStatus extends BaseTable {
	public Long Auto_increment;

	public static TableStatus getByNameLike(String name){

		return Query.find("show Table status like ?", TableStatus.class, name).first();
	}
}
