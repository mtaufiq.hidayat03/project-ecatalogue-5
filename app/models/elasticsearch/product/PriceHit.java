package models.elasticsearch.product;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class PriceHit {

    @SerializedName("total")
    public int total;
    @SerializedName("hits")
    public List<Hit> hits;

}
