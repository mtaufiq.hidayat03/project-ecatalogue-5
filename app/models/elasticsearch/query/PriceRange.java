package models.elasticsearch.query;

import com.google.gson.JsonObject;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class PriceRange extends NestedObject {

    public static final String GTE = "gte";
    public static final String LTE = "lte";

    private double maxPrice = -1;
    private double minPrice = -1;

    public PriceRange setPath(String path) {
        this.path = path;
        return this;
    }

    public PriceRange setField(String field) {
        this.field = field;
        return this;
    }

    public PriceRange setMaxPrice(double price) {
        this.maxPrice = price;
        return this;
    }

    public PriceRange setMinPrice(double price) {
        this.minPrice = price;
        return this;
    }

    @Override
    public JsonObject build() {
        JsonObject model = new JsonObject();
        JsonObject range = new JsonObject();
        if (maxPrice > -1) {
            range.addProperty(LTE, maxPrice);
        }
        if (minPrice > -1) {
            range.addProperty(GTE, minPrice);
        }
        model.add(path +"."+ field, range);
        JsonObject result = new JsonObject();
        result.add("range", model);
        return result;
    }

}
