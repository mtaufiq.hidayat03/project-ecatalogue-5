package models.elasticsearch;

import ext.Pageable;
import ext.contracts.ParamAble;
import models.elasticsearch.query.Sort;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.mvc.Scope;

import java.util.ArrayList;
import java.util.List;

import static constants.models.ElasticProductConstant.*;
import static constants.models.Sorting.*;
import static models.elasticsearch.query.Sort.ASC;
import static models.elasticsearch.query.Sort.DESC;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class SearchQuery implements ParamAble {

    public String keyword = "";
    public String productName = "";
    public Long categoryId = null;
    public Double maxPrice = null;
    public Double minPrice = null;
    public Long commodityId;
    public Long manufactureId;
    public Long providerId;
    public Long locationId;
    public Integer page = 0;
    public Integer max;
    public String order = "";
    public String sort = "";
    public Long provinceId;
    public Long regencyId;
    public boolean isGlobal = false;
    public int from = 0;
    public String type;
    public String authenticityToken = "";
    public boolean isFirstReq = false;
    public Integer isUmkm;

    public SearchQuery() {}

    public SearchQuery(Scope.Params params) {
        set(params);
    }

    public void set(Scope.Params params) {
        try {
            authenticityToken = params.get("authenticityToken") != null ? params.get("authenticityToken", String.class) : "";
            isFirstReq = params.get("first") != null ? params.get("first", Boolean.class) : false;
            page = params.get("offset") != null ? params.get("offset", Integer.class) : 1;
            max = Pageable.MAX_SIZE_PER_PAGE;
            order = params.get("order") != null ? params.get("order", String.class) : "";
            keyword = params.get("q", String.class);
            productName = params.get("nama_produk", String.class);
            providerId = params.get("pid", Long.class);
            manufactureId = params.get("mid", Long.class);
            minPrice = params.get("gt", Double.class);
            maxPrice = params.get("lt", Double.class);
            provinceId = params.get("prid", Long.class);
            regencyId = params.get("kabid", Long.class);
            categoryId = params.get("cat", Long.class);
            isUmkm = params.get("umkm", Integer.class);
            if (regencyId != null) {
                locationId = regencyId;
                type = "regency";
            } else if (provinceId != null) {
                locationId = provinceId;
                type = "province";
            }
        }catch (Exception e){
            Logger.error("[SearchQuery.set]", e.getMessage());
        }
    }

    @Override
    public List<BasicNameValuePair> getParams() {
        List<BasicNameValuePair> parameters = new ArrayList<>();
       // parameters.add(new BasicNameValuePair("sort", sort != null ? sort : ""));
        parameters.add(new BasicNameValuePair("authenticityToken", authenticityToken != null ? authenticityToken : ""));
        parameters.add(new BasicNameValuePair("q", keyword != null ? keyword : ""));
        parameters.add(new BasicNameValuePair("cat", categoryId != null ? categoryId.toString() : ""));
        parameters.add(new BasicNameValuePair("nama_produk", productName != null ? productName : ""));
        parameters.add(new BasicNameValuePair("mid", manufactureId != null ? manufactureId.toString() : ""));
        parameters.add(new BasicNameValuePair("gt", minPrice != null ? minPrice.toString() : ""));
        parameters.add(new BasicNameValuePair("lt", maxPrice != null ? maxPrice.toString() : ""));
        parameters.add(new BasicNameValuePair("pid", providerId != null ? providerId.toString() : ""));
        parameters.add(new BasicNameValuePair("prid", provinceId != null ? provinceId.toString() : ""));
        parameters.add(new BasicNameValuePair("kabid", regencyId != null ? regencyId.toString() : ""));
        parameters.add(new BasicNameValuePair("order", order != null ? order : ""));
        parameters.add(new BasicNameValuePair("umkm", String.valueOf(isUmkm)));
        return parameters;
    }

    @Override
    public int getPage() {
        return page;
    }

    public boolean isCommodityExist() {
        return commodityId != null;
    }

    public boolean isProductNameExists() {
        return !TextUtils.isEmpty(productName);
    }

    public boolean isKeywordExists() {
        return !TextUtils.isEmpty(keyword);
    }

    public boolean isProviderExists() {
        return providerId != null;
    }

    public boolean isCategoryExists() {
        return categoryId != null;
    }

    public boolean isLocationExists() {
        return locationId != null;
    }

    public boolean isLocationTypeExists() {
        return type != null;
    }

    public boolean isManufactureExists() {
        return manufactureId != null;
    }

    public boolean isMinPriceExists() {
        return minPrice != null && minPrice > 0;
    }

    public boolean isMaxPriceExists() {
        return maxPrice != null && maxPrice > 0;
    }

    public int getCurrentPage() {
        return page > 0 ? page - 1 : 0;
    }

    public boolean isUmkmExist() {
        return isUmkm != null && isUmkm >= 1 && isUmkm <= 2;
    }

    public Sort getSort() {
        Logger.debug(" order by: " + order);
        switch (order) {
            case MAX_PRICE:
                return new Sort(PRICE_LIST + "." +PRICE_HARGA_UTAMA, DESC).nestedPath(PRICE_LIST);
            case MIN_PRICE:
                return new Sort(PRICE_LIST + "." +PRICE_HARGA_UTAMA, ASC).nestedPath(PRICE_LIST);
            case NAME_ASC:
                return new Sort(NAMA_PRODUK, ASC);
            case NAME_DESC:
                return new Sort(NAMA_PRODUK, DESC);
            case NO_PRODUCT_LKPP_ASC:
                return new Sort(NO_PRODUK, ASC);
            case NO_PRODUCT_LKPP_DESC:
                return new Sort(NO_PRODUK, DESC);
            case NO_PRODUCT_PROVIDER_ASC:
                return new Sort(NO_PRODUK_PENYEDIA, ASC);
            case NO_PRODUCT_PROVIDER_DESC:
                return new Sort(NO_PRODUK_PENYEDIA, DESC);
            default:
                return null;
        }
    }

    public String getProcessedProductName() {
        return productName;//.replaceAll("([\\/\\+~*?:{}^\\[\\]\\-|=&><!\"()])","\\\\$1");
    }

    public String getProcessedKeyword() {
        return keyword;//.replaceAll("([\\/\\+~*?:{}^\\[\\]\\-|=&><!\"()])","\\\\$1");//.replaceAll("[^\\w\\s]", "");
    }

    public String getParamsString() {
        return ((commodityId != null) ? String.valueOf(commodityId) : "") +"?"+ URLEncodedUtils.format(getParams(), "UTF-8");
    }

}
