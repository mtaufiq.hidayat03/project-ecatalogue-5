package models.prakatalog;

import models.BaseKatalogTable;
import models.common.DokumenInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.LogUtil;

import java.io.File;
import java.util.List;

/**
 * Created by raihaniqbal on 8/4/2017.
 */
@Table(name = "usulan_dokumen_pengadaan")
public class UsulanDokumenPengadaan extends BaseKatalogTable {

	public static final String TAG = "UsulanDokumenPengadaan";

	@Id
	public Long id;

	@Required
	public Long usulan_id;

	public String dok_judul;

	public String dok_keterangan;

	public String dok_file_name;

	public String dok_hash;

	public String dok_signature;

	public Long dok_id_attachment;

	public Long file_size = 0L;

	public boolean active;

	public static DokumenInfo simpanPengadaan(File file, Long usulan_id) throws Exception {

		UsulanDokumenPengadaan pengadaan = new UsulanDokumenPengadaan();
		BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, null, UsulanDokumenPengadaan.class.getName());
		pengadaan.dok_id_attachment = blob.id;
		pengadaan.dok_judul = blob.blb_nama_file;
		pengadaan.dok_hash = blob.blb_hash;
		pengadaan.file_size = blob.blb_ukuran;
		pengadaan.usulan_id = usulan_id;
		pengadaan.active = false;
		pengadaan.id = Long.valueOf(pengadaan.save());
		LogUtil.debug(TAG, "doc pengadaan id: " + pengadaan.id);
		//update blob content id
		blob.blb_id_content = pengadaan.id;
		blob.save();
		return DokumenInfo.findByBlob(blob);
	}

	public static void activateDokumenPengadaan(long usulan_id, Long[] pengadaan){
		if (pengadaan == null || pengadaan.length == 0) {
			return;
		}
		String ids = getIds(pengadaan);
		LogUtil.debug(TAG,"idsPengadaan " + ids);
		StringBuilder sb = new StringBuilder("UPDATE usulan_dokumen_pengadaan SET active = ? WHERE usulan_id = ? AND dok_id_attachment IN (").append(ids).append(")");
		Query.update(sb.toString(), AKTIF,usulan_id);
	}

	public static long countByUsulan(long usulan_id){
		return count("usulan_id = ? and active = ?",usulan_id, AKTIF);
	}

	public static List<UsulanDokumenPengadaan> findByUsulan(long usulan_id){
		return find("usulan_id = ? and active = ?",usulan_id, AKTIF).fetch();
	}

	private static String getIds(Long[] pengadaan){
		StringBuilder sb = new StringBuilder();
		String glue = "";
		for (Long id : pengadaan) {
			sb.append(glue).append("'").append(id).append("'");
			glue = ",";
		}
		return sb.toString();
	}

}
