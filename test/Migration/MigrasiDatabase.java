package Migration;

import com.google.gson.Gson;
import models.katalog.Produk;
import models.katalog.ProdukHarga;
import models.util.produk.HargaProdukJson;
import org.apache.commons.lang.RandomStringUtils;
import play.Logger;
import play.Play;
import play.db.jdbc.Query;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MigrasiDatabase {
    static String rootPath = Play.applicationPath.getAbsolutePath();
    public List<Produk> listProduk = new ArrayList<>();

    public Long getTotalProduct(){
        String sql = "select count(id) from produk where active=1";
        return Query.count(sql);
    }
    public List<Produk> getAllProdukForMigration(int start, int end){
        String sqlFiltered = "select p.* from produk p\n" +
                "JOIN komoditas k on k.id = p.komoditas_id\n" +
                "JOIN penyedia pn ON p.penyedia_id = pn.id\n" +
                "JOIN penyedia_kontrak pk ON pn.id = pk.penyedia_id\n" +
                "where p.active = 1 AND p.apakah_ditayangkan = 1\n" +
                "  AND p.berlaku_sampai <> '' AND p.minta_disetujui = 0 AND p.setuju_tolak = 'setuju'\n" +
                "  AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1\n" +
                "  AND (k.apakah_stok_unlimited = 1 OR (k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0)) limit ? , ?";
        //String qry = "select * from produk where active=1 limit ? , ?";
        try{
            listProduk =  Query.find(sqlFiltered, Produk.class, start,end).fetch();
        }catch (Exception e){}
        return listProduk;
    }

    public void migrasiProdukRegency(){

        int many = 3000;getTotalProduct().intValue();
        int manypage = 50;int page =1;
        int start = 1;int end = manypage;
        String text = "TOTAL data: "+many;
        System.out.println(text);
        writeReportToFile(text);
        text = "TOTAL page: "+many/manypage;
        writeReportToFile(text);
        System.out.println();
        text = "TOTAL data /page: "+manypage;
        System.out.println(text);

        while (many > start){
            text = "page:"+page+",  start :"+start + " end:"+end;
            System.out.println(text);
            //
            setMigrasiProdRegency(getAllProdukForMigration(start,end));
            //
            end+=manypage;start+=manypage;page++;
            assert (true);
        }
    }

    public void setMigrasiProdRegency(List<Produk> produks){
        String text ="----------------->";
        writeReportToFile(text);
        for (Produk p: produks) {
            text = "produk_id:"+p.id+" "+p.nama_produk;
            writeReportToFile(text);
            jsonHarga(p);

        }
    }

    public void jsonHarga(Produk produk){
        String text ="";
        String qry = "select p.id produk_id, pp.kurs_id,\n" +
                "       JSON_OBJECT('provinsiId',pp.provinsi_id, 'kabupatenId', pp.kabupaten_id, 'harga', pp.harga) harga,\n" +
                "       pp.harga_tanggal dibuat_tanggal,\n" +
                "       pp.approved apakah_disetujui,\n" +
                "       pp.approved_date tanggal_disetujui,\n" +
                "       null tanggal_ditolak,\n" +
                "       pp.approved_by setuju_tolak_oleh,\n" +
                "       pp.created_date,\n" +
                "       pp.created_by,\n" +
                "       pp.modified_date,\n" +
                "       pp.modified_by,\n" +
                "       pp.deleted_date,\n" +
                "       pp.deleted_by,\n" +
                "       null audittype,\n" +
                "       null auditdate,\n" +
                "       null audituser,\n" +
                "       pp.active,\n" +
                "       pp.komoditas_harga_atribut_id,\n" +
                "       null kelas_harga,\n" +
                "       null alasan_ditolak,\n" +
                "       JSON_OBJECT('Provinsi',pr.nama_provinsi, 'Kabupaten', kb.nama_kabupaten) harga_xls_json,\n" +
                "       concat('\\\"', pp.provinsi_id, '\\\" : true') checked_locations_prop,\n" +
                "       concat('\\\"', pp.kabupaten_id, '\\\" : true') checked_locations_kab\n" +
                "from produk p\n" +
                "       join komoditas k on p.komoditas_id = k.id\n" +
                "       join produk_harga_kabupaten pp on p.id = pp.produk_id and pp.active = 1\n" +
                "       join provinsi pr on pp.provinsi_id = pr.id\n" +
                "       join kabupaten kb on kb.id = pp.kabupaten_id\n" +
                "       JOIN penyedia pn ON p.penyedia_id = pn.id\n" +
                "       JOIN penyedia_kontrak pk ON pn.id = pk.penyedia_id\n" +
                "where p.active = 1 AND p.apakah_ditayangkan = 1\n" +
                "  AND p.berlaku_sampai <> '' AND p.minta_disetujui = 0 AND p.setuju_tolak = 'setuju'\n" +
                "  AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1\n" +
                "  AND (k.apakah_stok_unlimited = 1 OR (k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0)) and p.id=? group by harga;";
        List<ProdukHargaMigration> phs = Query.find(qry, ProdukHargaMigration.class, produk.id).fetch();

        List<ProdukHarga> phexisting = Query.find("select * from produk_harga where active=1 and produk_id=?", ProdukHarga.class, produk.id).fetch();
        ProdukHarga phn = new ProdukHarga();
        if(phexisting.size() > 1){
            text ="existing produk harga > 1";
            writeReportToFile(text);
            //hanya ada 1 produk harga
            //ambil 1, sisanya gak usah jadikan not active
            for(int i=1; i < phexisting.size() ; i++){
                ProdukHarga ph = phexisting.get(i);
                ph.active = false;
                text = "not activkan produk harga existing:"+ph.id+" produk_id"+ph.produk_id+"";
                System.out.println(text);
                writeReportToFile(text);
                if(null == ph.produk_id){
                    text = "ph-> produk id null set ke:"+  produk.id+" "+ produk.nama_produk;
                    writeReportToFile(text);
                    ph.produk_id = produk.id;
                }
                if(null == ph.kurs_id){
                    text = "ph-> produk id null set ke:"+  produk.id+" "+ produk.nama_produk;
                    writeReportToFile(text);
                    ph.kurs_id = produk.kurs_id;
                }
                ph.save();
            }
        }else if(phexisting.size() >=1){

            phn = phexisting.get(0);
            text ="existing produk harga >= 1, produkharga dari existing:"+phn.id+" produk_id:"+phn.produk_id;
            writeReportToFile(text);
        }else{
            // get as produk harga
            text = "produk harga db > 0";
            writeReportToFile(text);
            if(phs.size() > 0) {
                phn = phs.get(0).getAsProdukharga();
                text = "ambil 1 produk harga dari db:"+phn.id+" produk_id:"+phn.produk_id;
                writeReportToFile(text);
            }else {
                text = "new Produk Harga id:"+ new Gson().toJson(phn);
                System.out.println(text);
                writeReportToFile(text);
            }
        }

        //collect harga
        List<HargaProdukJson> listJson = new ArrayList<>();
        for (ProdukHargaMigration ph: phs) {
            HargaProdukJson hpj = new Gson().fromJson(ph.harga, HargaProdukJson.class);
            listJson.add(hpj);
        }
        //create json
        phn.harga = new Gson().toJson(listJson);
        text = "json harga:"+listJson.size();
        writeReportToFile(text);
        text = "RESULT LAST: ------------------------------------------------------------------";
        System.out.println(text);
        if(phs.size() > 0){
            text = "save produk harga hargga size json >0";
            writeReportToFile(text);
            if(null == phn.produk_id){
                text = "produk id null set ke:"+  produk.id+" "+ produk.nama_produk;
                writeReportToFile(text);
                phn.produk_id = produk.id;
            }
            if(null == phn.kurs_id){
                text = "produk id null set ke:"+  produk.id+" "+ produk.nama_produk;
                writeReportToFile(text);
                phn.kurs_id = produk.kurs_id;
            }
            try{
                Long id = (long) phn.save();
                text+=" save produk harga id: " +id;
                writeReportToFile(text);
                System.out.println(text);
            }catch (Exception e){
                text ="save error 1: "+e.getMessage();
                writeReportToFile(text);
                text ="save error 1: "+ new Gson().toJson(phn);
                writeReportToFile(text);

            }

        }else{
            text = "<<<<< empty json harga: "+  produk.id+" "+ produk.nama_produk+" >>>>>";
            writeReportToFile(text);
            System.out.println(text);
        }
    }

    public void writeReportToFile(String text){
        text += "\n";
        if(!Files.exists(Paths.get(rootPath+"/reportProdukHarga.txt"))) {
            PrintWriter writer = null;
            try {
                writer = new PrintWriter(rootPath+"/reportProdukHarga.txt", "UTF-8");
            } catch (FileNotFoundException e) {
            } catch (UnsupportedEncodingException e) {}
            writer.println("The first line");
            writer.close();
        }
        try {
            Files.write(Paths.get("reportProdukHarga.txt"), text.getBytes(), StandardOpenOption.APPEND);
        }catch (IOException e) {
            //exception handling left as an exercise for the reader
            System.out.println("Eror write filereport:"+e.getMessage());
            e.printStackTrace();
        }
    }
}
