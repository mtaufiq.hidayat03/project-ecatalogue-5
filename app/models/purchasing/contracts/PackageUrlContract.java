package models.purchasing.contracts;

import play.i18n.Lang;
import utils.UrlUtil;

/**
 * @author HanusaCloud on 11/2/2017
 */
public interface PackageUrlContract extends BaseContract {

    default public String getDetailUrl() {
        return generateUrl("purchasing.PaketCtr.detail");
    }

    default public String getUpdateUrl() {
        return generateUrl("purchasing.PaketCtr.edit");
    }

    default public String getReportDeliveryUrl() {
        return generateUrl("purchasing.PengirimanCtr.riwayatPengiriman");
    }

    default public String getReportAcceptanceUrl() {
        return generateUrl("purchasing.PenerimaanCtr.riwayatPenerimaan");
    }

    default public String getPaymentHistory() {
        return generateUrl("purchasing.PembayaranCtr.riwayatPembayaran");
    }

    default public String getPackageHistory() {
        return generateUrl("purchasing.PaketCtr.riwayatPaket");
    }

    default public String getPackageNegotiationHistory() {
        return generateUrl("purchasing.PaketCtr.riwayatNegosiasiProduk");
    }

    default public String getAcceptanceSearchProductUrl() {
        return generateUrl("purchasing.PenerimaanCtr.searchProduct");
    }

    default public String getDeliverySearchProductUrl() {
        return generateUrl("purchasing.PengirimanCtr.searchProduct");
    }

    default public String getPaymentAcceptanceSearchUrl() {
        return generateUrl("purchasing.PembayaranCtr.searchPenerimaan");
    }

    default public String getPaymentDeliverySearchUrl() {
        return generateUrl("purchasing.PembayaranCtr.searchPengiriman");
    }

    default public String getDeliverySearchUrl() {
        return generateUrl("purchasing.PengirimanCtr.searchDelivery");
    }

    default public String getDeliveryProductsUrl() {
        return generateUrl("purchasing.PengirimanCtr.getDeliveryProductsApi");
    }

    default public String getInfoProviderUrl() {
        return UrlUtil.builder()
                .setUrl("purchasing.PaketCtr.infoPenyedia")
                .setParam("language", Lang.get())
                .setParam("paket_id", getId())
                .setParam("id", getProviderId())
                .build();
    }

    default public String getProviderRepresentativeUrl() {
        return UrlUtil.builder()
                .setUrl("purchasing.PaketCtr.representative")
                .setParam("language", Lang.get())
                .setParam("paket_id", getId())
                .setParam("id", getProviderId())
                .build();
    }

    default public String getDistributorRepresentativeUrl() {
        return UrlUtil.builder()
                .setUrl("purchasing.PaketCtr.distributorRepresentative")
                .setParam("language", Lang.get())
                .setParam("paket_id", getId())
                .setParam("id", getDistributorId())
                .build();
    }

    default public String getProductUrl() {
        return generateUrl("purchasing.PaketCtr.productList");
    }

    default String getContractListUrl() {
        return UrlUtil.builder()
                .setUrl("purchasing.PaketKontrakCtr.contractList")
                .setParam("language", Lang.get())
                .setParam("paket_id", getId())
                .build();
    }

    default String getCreateContractUrl() {
        return generateUrl("purchasing.PaketKontrakCtr.createContract");
    }

    default String setDistributorUrl() {
        return generateUrl("purchasing.PaketCtr.setDistributor");
    }

    default String getPaymentCreationUrl() {
        return generateUrl("purchasing.PembayaranCtr.createPembayaran");
    }

    default String getVerificationCreationUrl() {
        return generateUrl("purchasing.PembayaranCtr.createVerification");
    }

    default String getAcceptanceCreationUrl() {
        return generateUrl("purchasing.PenerimaanCtr.createPenerimaan");
    }

    default String getDeliveryCreationUrl() {
        return generateUrl("purchasing.PengirimanCtr.createPengiriman");
    }

    default String cancelPackage() {
        return generateUrl("purchasing.PaketCtr.cancelPackage");
    }

    default String getUlpMemberUrl() {
        return generateUrl("purchasing.PaketCtr.ulpList");
    }

    default String generateUrl(String path) {
        return UrlUtil.builder()
                .setUrl(path)
                .setParam("language", Lang.get())
                .setParam("paket_id", getId())
                .build();
    }

}
