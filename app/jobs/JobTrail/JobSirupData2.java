package jobs.JobTrail;

import jobs.extdata.ExtData;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import java.util.Date;
import java.util.UUID;

@NoTransaction
public class JobSirupData2 extends Job {
    ExtData ext = new ExtData();
    String uniqueID = UUID.randomUUID().toString();
    Date tgl_update = new Date();
    int nextWaitime = 0;

    public JobSirupData2(ExtData _ext, Date _tg_update, int _nextWaitime){
        ext = _ext;
        tgl_update = _tg_update;
        nextWaitime = _nextWaitime;
    }
    @Override
    public void before() {
        Logger.info("job id:"+uniqueID);
        super.before();
    }

    public void doJob(){
        Logger.info("on job execute, waittime:"+nextWaitime);
        ext.getSyrupData(nextWaitime,tgl_update);
        Logger.info("%%%% done getDate, saving now %%%");
        ext.saveDataToDb();
        ext.sendEmailReport();
        Logger.info("done job:"+ext.sirupList.size());
    }
}
