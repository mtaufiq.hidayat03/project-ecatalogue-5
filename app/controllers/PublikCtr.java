package controllers;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import controllers.admin.UserCtr;
import controllers.security.AllowAccess;
import ext.Pageable;
import features.komoditas.KomoditasService;
import models.api.produk.HistoryDatafeed;
import models.cms.*;
import models.cms.search.PollingForm;
import models.common.AktifUser;
import models.common.Comparison;
import models.common.KomoditasKategori;
import models.datatable.ResultsetHandlers;
import models.elasticsearch.SearchResult;
import models.jcommon.datatable.DatatableQuery;
import models.jcommon.util.CommonUtil;
import models.katalog.Komoditas;
import models.katalog.Produk;
import models.katalog.ProdukKategori;
import models.masterdata.Kldi;
import models.penyedia.Penyedia;
import models.penyedia.PenyediaDistributor;
import models.prakatalog.SearchUsulan;
import models.prakatalog.Usulan;
import models.prakatalog.UsulanSearchParams;
import models.product.HistoryDatafeedSearch;
import models.product.search.ProductSearchResult;
import models.secman.Acl;
import models.user.RoleBasic;
import models.user.User;
import models.user.UserRoleGrup;
import models.util.RekananDetail;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.Play;
import play.i18n.Lang;
import play.i18n.Messages;
import repositories.DatafeedRepository;
import repositories.UsulanRepository;
import repositories.penyedia.ProviderRepository;
import services.produk.ProdukService;
import utils.CookieComparison;

import java.io.File;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.util.*;

public class PublikCtr extends BaseController {

	public static final String TAG = "PublikCtr";
	public static int max_konten = 12;
	public static int max_faq = 20;

	public static void index()
	{
		if(Play.mode.isDev()){
			DevelopmentCtr.index();
		}else{
			home();
		}
	}

	public static void pollingPilihan (){
		Logger.debug("get Polling Form data");
	    /* Menu Polling */
        List<Polling> pollingForm = Polling.findAllPublished();
        String polId = "";
        if(pollingForm.size() > 0) {
            for (int i = 0; i < pollingForm.size(); i++) {
                polId += pollingForm.get(i).id + ",";
            }
            polId = polId.substring(0, polId.length() - 1);
        }
        List<PollingPilihan> pollingList = PollingPilihan.findByPollingId(polId);
        PollingForm result = new PollingForm();
        result.pollingForm = pollingForm;
        result.pollingList = pollingList;
		/* End Menu Polling */
	    renderJSON(result);
    }

	public static void home() {
		List<Usulan> usulanList = UsulanRepository.homePage();
		/* Notifikasi */
		Notifikasi notif = Notifikasi.findActive();
		/* Berita */
		List<Berita> beritaList = Berita.findLastesBerita();
		/* Pengumuman Penyedia */
		KontenStatis pengumumanPenyedia = KontenStatis.findKontenByKategori("pengumuman penyedia");
		
		if (getAktifUser() != null) {
			notif = null;
		}

		List<Komoditas> komoditasList = Komoditas.findAllActive();
		List<Banner> bannerList = Banner.findAllBannerActive();

        SearchResult popular = new SearchResult();
		renderArgs.put("bannerList", bannerList);
		renderArgs.put("popularCommodities", KomoditasService.getInstance().groupByCategory(komoditasList));//popular.commodities
		renderArgs.put("popularProducts", popular.aggregateProducts);
		render("PublikCtr/web/home.html", usulanList, notif, pengumumanPenyedia, beritaList);
	}

	//@AllowAccess({Acl.COM_PENGUMUMAN})
	//tgl 29, buka acl oleh om pri, karena list pengumuman bebas diakses, yang di batasi mendaftarnya harus login
	public static void listUsulan() {
		UsulanSearchParams searchParams = new UsulanSearchParams(params);
		List<Komoditas> komoditasList = Komoditas.findAllActive();
		SearchUsulan model = new UsulanRepository().searchUsulan(searchParams);
		Pageable pageable = new Pageable(model);
		renderArgs.put("commodities", CommonUtil.toJson(searchParams.commodities));
		renderArgs.put("commodityJson", searchParams.getCommoditiesJson());
		render("PublikCtr/web/listUsulan.html", komoditasList, model, pageable);
	}

	private static List<String> getCommodities() {
		String jsonSet = params.get("commodities");
		List<String> cats = new ArrayList<>();
		try {
			if (!TextUtils.isEmpty(jsonSet)) {
				String decoded = URLDecoder.decode(jsonSet);
				renderArgs.put("cats", decoded);
				Type listType = new TypeToken<List<String>>() {
				}.getType();
				cats = CommonUtil.fromJson(jsonSet, listType);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cats;
	}

	public static void listBerita() {
		int page= null != params.get("offset") ? params.get("offset", Integer.class) : 1;
		int start = page*max_konten-max_konten;

		List<Berita> beritaAll = Berita.findAllPublished();
		Pageable p = new Pageable(page, max_konten, beritaAll.size(), "");
		List<Berita> beritaList = Berita.findJoinKategoriAllActiveByLimit(Long.valueOf(start), Long.valueOf(max_konten));
		List<Berita> kategoriKontenList = Berita.findAllCountBerita();
		List<Berita> beritaLatestList = Berita.findLastesBerita();
//		List<Berita> beritaPopularList = Berita.findPopularBerita();
		render("PublikCtr/web/listBerita.html",p,beritaList,kategoriKontenList,beritaLatestList);//beritaPopularList
	}

    public static void statusPenyediaDistributor() {
        render("PublikCtr/web/statusPenyediaDistributor.html");
    }

    public static void statusPenyediaDistributorPost(String status_penyedia_distributor) {
//        if (getAktifUser().getUser().isProviderAndDistributor()){
            User userPenyedia = User.findById(getAktifUser().user_id);

            //pengecekan data penyedia
			long rkn_id = 0;
			long penyedia_id = 0;
			try{
				rkn_id = userPenyedia.rkn_id;
			}catch (Exception e){}

			if(rkn_id != 0){
				Penyedia penyedia = ProviderRepository.findByUserId(userPenyedia.id);
				if(penyedia == null){
					penyedia_id = ProviderRepository.saveNewByRknId(rkn_id, userPenyedia.id);
				}else{
					penyedia_id = penyedia.id;
				}
			}

            if (userPenyedia.status_penyedia_distributor == null){
                userPenyedia.status_penyedia_distributor = status_penyedia_distributor;
                if(status_penyedia_distributor.equalsIgnoreCase("distributor_saja")){
					userPenyedia.role_basic_id = RoleBasic.DISTRIBUTOR;
					userPenyedia.user_role_grup_id = UserRoleGrup.DISTRIBUTOR;
				}else if(status_penyedia_distributor.equalsIgnoreCase("penyedia_dan_distributor") && penyedia_id != 0){
					Penyedia py = ProviderRepository.findByUserId(userPenyedia.id);
                	PenyediaDistributor pd = PenyediaDistributor.findByPenyediaUser(penyedia_id, py.user_id);

					if(pd == null){
						PenyediaDistributor penyediaDistributor = new PenyediaDistributor();
						penyediaDistributor.penyedia_id = py.id;
						penyediaDistributor.user_id = py.user_id;
						penyediaDistributor.nama_distributor = py.nama_penyedia;
						penyediaDistributor.alamat = py.alamat;
						penyediaDistributor.website = py.website;
						penyediaDistributor.email = py.email;
						penyediaDistributor.no_telp = py.no_telp;
						penyediaDistributor.no_fax = py.no_fax;
						penyediaDistributor.no_hp = py.no_hp;
						penyediaDistributor.npwp = py.npwp;
						penyediaDistributor.pkp = py.pkp;
						penyediaDistributor.kode_pos = py.kode_pos;
						penyediaDistributor.active = true;
						penyediaDistributor.rkn_id = py.rkn_id;
						penyediaDistributor.minta_disetujui = 0l;
						penyediaDistributor.setuju_tolak = "Setuju";
						penyediaDistributor.setuju_tolak_oleh = -99l;
						penyediaDistributor.setuju_tolak_tanggal = new Date();
						penyediaDistributor.setuju_tolak_alasan = "Approved by System";
						penyediaDistributor.save();
					}
				}
                userPenyedia.save();
            }
//        }
        //PublikCtr.home();
		UserCtr.logout();
    }

	public static void searchingBerita(String keyword){
		int page=null != params.get("offset")  ? params.get("offset", Integer.class) : 1;
		int start = page*max_konten-max_konten;

		List<Berita> beritaLatestList = Berita.findLastesBerita();
		List<Berita> beritaPopularList = Berita.findPopularBerita();
		List<Berita> beritaList = Berita.findBerita(keyword,Long.valueOf(start), Long.valueOf(max_konten));
		List<Berita> kategoriKontenList = Berita.findAllCountBerita();
		Pageable p = new Pageable(page, max_konten, beritaList.size(),"");
		render("PublikCtr/web/listBerita.html",p,beritaList,kategoriKontenList,beritaLatestList,beritaPopularList);
	}

	public static void listBeritaFromKategori(String kategori){
//		if (!params.get("offset").equals("")) {
			int page = null != params.get("offset") ? params.get("offset", Integer.class) : 1;
			int start = page * max_konten - max_konten;
//		}

		List<Berita> beritaLatestList = Berita.findLastesBerita();
//		List<Berita> beritaPopularList = Berita.findPopularBerita();
		List<Berita> kategoriKontenList = Berita.findAllCountBerita();
		List<Berita> beritaList = Berita.findBeritaByKategori(kategori, Long.valueOf(start), Long.valueOf(max_konten));
		List<Berita> beritaAll = Berita.findAllBeritaFromKategori(kategori);
		Pageable p = new Pageable(page, max_konten, beritaAll.size(), ""+kategori);
		render("PublikCtr/web/listBerita.html",p,beritaList,kategoriKontenList,beritaLatestList);//beritaPopularList
	}

	public static void detailBerita(String slug){
		List<Berita> kategoriKontenList = Berita.findAllCountBerita();
		Berita berita = Berita.findDetailBerita(slug);
		berita.isi_konten = berita.isi_konten.replace("BASE_WEB_URL","public");
		List<Berita> beritaLatestList = Berita.findLastesBerita();
//		List<Berita> beritaPopularList = Berita.findPopularBerita();
		List<String> tags = new ArrayList<>();

		if(berita != null)
			if(berita.tags != null && berita.tags != "")
				tags = Arrays.asList(berita.tags.split("-"));

		render("PublikCtr/web/detailBerita.html",kategoriKontenList,berita,beritaLatestList,tags); //beritaPopularList
	}

	public static void listUnduh(){
		int page=null != params.get("offset")  ? params.get("offset", Integer.class) : 1;
		int start = page*max_konten-max_konten;

		List<Unduh> unduhAll = Unduh.findAllPublished();
		Pageable p = new Pageable(page, max_konten, unduhAll.size(), "");
		List<Unduh> unduhList = Unduh.findJoinKategoriAllActiveByLimit(Long.valueOf(start), Long.valueOf(max_konten));
		List<Unduh> kategoriKontenList = Unduh.findAllCountUnduh();
		render("PublikCtr/web/listUnduh.html", unduhList,kategoriKontenList,p);
	}

	public static void listUnduhFromKategori(String kategori){
		int page=null != params.get("offset")  ? params.get("offset", Integer.class) : 1;
		int start = page*max_konten-max_konten;

		List<Unduh> kategoriKontenList = Unduh.findAllCountUnduh();
		List<Unduh> unduhList = Unduh.findUnduhByKategori(kategori, Long.valueOf(start), Long.valueOf(max_konten));
		List<Unduh> unduhAll = Unduh.findAllUnduhFromKategori(kategori);
		Pageable p = new Pageable(page, max_konten, unduhAll.size(), ""+kategori);
		render("PublikCtr/web/listUnduh.html",unduhList,kategoriKontenList,p);
	}

	public static void searchingUnduh(String keyword){
		int page=null != params.get("offset")  ? params.get("offset", Integer.class) : 1;
		int start = page*max_konten-max_konten;

		List<Unduh> unduhList = Unduh.findUnduh(keyword,Long.valueOf(start), Long.valueOf(max_konten));
		List<Unduh> kategoriKontenList = Unduh.findAllCountUnduh();
		Pageable p = new Pageable(page, max_konten, unduhList.size(),"");
		render("PublikCtr/web/listUnduh.html",unduhList,kategoriKontenList,p);
	}

	public static void getFileUnduh(Long id){
		Unduh viewunduh = Unduh.findById(id);
		try {
			String generalPath = Play.applicationPath + "/public/files/pustaka_file"+viewunduh.file_sub_location;
			File test = new File (generalPath+"/"+viewunduh.file_name);
			if(test.exists() && !test.isDirectory()) {
				renderBinary(test,viewunduh.original_file_name);
			}else{
				params.flash();
				flash.error(Messages.get("file tidak ditemukan"));
				listUnduh();
			}
		}catch (Exception e){
			listUnduh();

		}

	}

	public static void searchingProduk() {
		render();
	}

	public  static void hubungiKami(){
		KontenStatis hubkami = KontenStatis.findKontenByKategori("hubungi kami");
		if(hubkami == null){
			hubkami = new KontenStatis();
			hubkami.isi_konten ="";
			hubkami.judul_konten="";
		}
		render("PublikCtr/web/hubungiKami.html",hubkami);
	}

	public  static void katalogLokalSektoral(){
		KontenStatis katalogLokalSektoral = KontenStatis.findKontenByKategori("katalog lokal sektoral");
		if(katalogLokalSektoral == null){
			katalogLokalSektoral = new KontenStatis();
			katalogLokalSektoral.isi_konten ="";
			katalogLokalSektoral.judul_konten="";
		}
		render("PublikCtr/web/katalogLokalSektoral.html",katalogLokalSektoral);
	}

	public static void bandingkanProduk() {
		List<Comparison> comparisons = CookieComparison.get();
		List<Produk> produkList = new ArrayList<>();
		if (!comparisons.isEmpty()) {
			for (Comparison model : comparisons){
				Produk produk = ProdukService.getDetailProduk(model.id, model.location_id);
				produk.withPictures();
				produk.daerah_id = model.location_id;

                boolean isCanBeBought = Produk.isCanBeBought(produk);
                renderArgs.put("isCanBeBought", isCanBeBought);

				Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
				boolean showBuyButton = AktifUser.isUserAllowedToBuy(komoditas);
                renderArgs.put("showBuyButton", showBuyButton);
				produk.apakah_dapat_dibeli = showBuyButton;
				produkList.add(produk);
			}
		}
		render("PublikCtr/perbandingan.html", produkList);
	}
	
	public static void allKomoditas() {
		Map<String, List<Komoditas>> allKomoditas = Komoditas.AllKomoditasMap();
		List<KomoditasKategori> komoditasKategoriList = Komoditas.findKomoditasKetegori();
		Map<Long, List<ProdukKategori>> childs = Komoditas.findKategoriChilds();
		
		String lang = Lang.get().isEmpty()?"id": Lang.get();
		

		render("PublikCtr/web/allKomoditas.html",allKomoditas, komoditasKategoriList, childs, lang);
	}
	
	public static void allKategori(Long id) {
		List<KomoditasKategori> komoditasKategoriList = Komoditas.findKomoditasKategoriByKomoditasId(id);
		Map<Long, List<ProdukKategori>> childs = Komoditas.findKategoriChilds();
		
		render("PublikCtr/mobile/allKategori.html", komoditasKategoriList, childs);
	}

	public static void changeLang(String lang){
        String langBefore = "";
        if (lang!=null) {
            langBefore = Lang.get();
            if (lang.equalsIgnoreCase("id")) {
                Lang.change("id");
            } else {
                Lang.change("en");
            }
        }
        if (request.headers.get("referer") != null && !request.headers.get("referer").values.isEmpty()) {
            String referer = request.headers.get("referer").values.get(0);
            final String needle = "/" + langBefore + "/";
            Logger.debug("lang before: " + langBefore + " now: " + lang);
            if (referer.contains(needle)) {
                referer = referer.replace(needle, "/" + Lang.get() + "/");
                Logger.debug("should be : " + referer);
            }
            redirect(referer);
        } else {
            PublikCtr.home();
        }
	}

	public static void bikinHtmlTemplateDokumen(){
		render();
	}

	@AllowAccess({Acl.ALL_AUTHORIZED_USERS})
	public static void syaratKetentuan(){
		KontenStatis syaratKetentuan = KontenStatis.findKontenByKategori("syarat dan ketentuan");
		SyaratKetentuanHasil syaratKetentuanHasil = new SyaratKetentuanHasil();
		if(syaratKetentuan != null) {
			syaratKetentuanHasil = SyaratKetentuanHasil.findByExist(getAktifUser().user_id, syaratKetentuan.id);
		}

		if((null == syaratKetentuan) && (null == syaratKetentuanHasil) ){
			syaratKetentuan = new KontenStatis();
			syaratKetentuan.isi_konten ="";
			syaratKetentuanHasil = new SyaratKetentuanHasil();
		}

		render("PublikCtr/web/syaratKetentuan.html",syaratKetentuan,syaratKetentuanHasil);
	}

	public static void faq(){
		int page=null != params.get("offset")  ? params.get("offset", Integer.class) : 1;
		int start = page*max_faq-max_faq;

		List<Faq> faqAll = Faq.findAllPublished();
		List<Faq> faqList = Faq.findAllByLimit(null,null, Long.valueOf(start), Long.valueOf(max_faq));
		List<Faq> faqCountList = Faq.findAllCountFaq();
		List<KategoriKonten> kategoriList = KategoriKonten.findByKategori("");
		Pageable p = new Pageable(page, max_faq, faqAll.size(), "");
		render("PublikCtr/web/listFaq.html", faqList, faqCountList, p, kategoriList);
	}

	public static void kategorifaq(String kategori){
		int page = null != params.get("offset") ? params.get("offset", Integer.class) : 1;
		int start = page*max_faq-max_faq;

		List<Faq> faqCountList = Faq.findAllCountFaq();
		List<Faq> faqList = Faq.findAllByLimit(null, kategori, Long.valueOf(start), Long.valueOf(max_faq));
		List<Faq> faqAll = Faq.findAllByLimit(null, kategori,null,null);
		List<KategoriKonten> kategoriList = KategoriKonten.findByKategori(kategori);
		Pageable p = new Pageable(page, max_faq, faqAll.size(), ""+kategori);
		render("PublikCtr/web/listFaq.html", faqList, faqCountList, p, kategoriList);
	}

	public static void searchingFaq(String keyword){
		int page= null != params.get("offset") ? params.get("offset", Integer.class) : 1;
		int start = page*max_faq-max_faq;

		List<Faq> faqList = Faq.findAllByLimit(keyword, null, Long.valueOf(start), Long.valueOf(max_faq));
		List<Faq> faqCountList = Faq.findAllCountFaq();
		List<KategoriKonten> kategoriList = KategoriKonten.findByKategori("");
		Pageable p = new Pageable(page, max_faq, faqList.size(),"");
		render("PublikCtr/web/listFaq.html", faqList, faqCountList, p, kategoriList);
	}

	public static void detailFaq(String slug){
		Faq faqDetail = Faq.findDetailFaq(slug);
		List<Faq> faqCountList = Faq.findAllCountFaq();
		render("PublikCtr/web/detailFaq.html", faqDetail,faqCountList);
	}

	public static void submitPolling(Long polId, Long vote){
		Map<String, String> result = new HashMap<>();
		if(vote != null) {
			if (getAktifUser() != null) {
				try {
					List<PollingHasil> polExist = PollingHasil.findAllByExist(getAktifUser().user_id, polId);
					if (polExist.size() > 0) {
						result.put("result", "exist");
					} else {
						PollingHasil polHasil = new PollingHasil();
						polHasil.polling_id = polId;
						polHasil.polling_pilihan_id = vote;
						polHasil.client_ip = request.remoteAddress;
						polHasil.active = true;
						polHasil.save();

						result.put("result", "ok");
					}
				} catch (Exception e) {
					e.printStackTrace();
					Logger.error("Terjadi Kesalahan : " + e.getLocalizedMessage());
					result.put("result", "fail");
				}
			} else {
				result.put("result", "login");
			}
		} else {
			result.put("result", "empty");
		}

		renderJSON(result);
	}

	public static void submitSyarat(Long konten_id){
        Map<String, String> result = new HashMap<>();
        if (getAktifUser() != null) {
            try {
                SyaratKetentuanHasil syaratKetentuanHasil = new SyaratKetentuanHasil();
                syaratKetentuanHasil.syarat_ketentuan_id = konten_id;
                syaratKetentuanHasil.client_ip = request.remoteAddress;
                syaratKetentuanHasil.active = true;
                syaratKetentuanHasil.save();
                result.put("result", "ok");
            }catch (Exception e) {
                e.printStackTrace();
                Logger.error("Terjadi Kesalahan : " + e.getLocalizedMessage());
                result.put("result", "fail");
            }
        }else {
            result.put("result", "login");
        }
    }

	public static void komoditasKldiIndex(String id){ //id kldi
		String id_kldi = id;
		Kldi kldi = Kldi.findById(id);
		render("masterdata/KomoditasKldiCtr/index.html", id_kldi, kldi);
	}

	public static void cariKomoditasKldi(String kldi_id){ //id kldi
		JsonObject json= new DatatableQuery(ResultsetHandlers.KOMODITAS_KLDI)
				.select("id,nama_komoditas")
				.from("komoditas")
				.where("kldi_id=?")
				.params(kldi_id)
				.executeQuery();
		renderJSON(json);
	}

	public static void historyDatafeed(){
		List<Komoditas> komoditas = Komoditas.findAllKomoditasOnlineShop();
		HistoryDatafeedSearch query = new HistoryDatafeedSearch(params);
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		ProductSearchResult model = new ProductSearchResult(query);
		HistoryDatafeed p = new HistoryDatafeed();


//		if (!TextUtils.isEmpty(query.tanggal)) {
//		    Date tanggal = query.tanggal;
//		    query.tanggal = tanggal.toString();
//            query.tanggal = dateFormat.format(query.tanggal);
//        }

//		if(params.all().size() > 1){
			model = DatafeedRepository.searchDatafeed(query);

//		}

		renderArgs.put("paramQuery", query);


		render("PublikCtr/web/historyDatafeed.html", komoditas, model, p);
	}

	public static void getPenyediaDatafeed(String komoditas_id){
		List<Penyedia> penyediaList = new ArrayList<>();
		JsonArray jsonArray = new JsonArray();
		if (!TextUtils.isEmpty(komoditas_id)) {
			penyediaList.addAll(ProviderRepository.findByKomoditas(Long.parseLong(komoditas_id)));
			for (Penyedia model : penyediaList) {
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("id", model.id);
				jsonObject.addProperty("nama_penyedia", model.nama_penyedia);
				jsonObject.addProperty("active", model.active);
				jsonArray.add(jsonObject);
			}
		}
		renderJSON(jsonArray);
	}

    public static void getDatafeedById (long id){
        JsonObject json = new JsonObject();
        HistoryDatafeed cleanMessage = new HistoryDatafeed();
        HistoryDatafeed historyDatafeed = HistoryDatafeed.findById(id);
        String message = "" ;

        json.addProperty("message",cleanMessage.cleansingDataMsg(historyDatafeed.message));
        json.addProperty("penyedia",historyDatafeed.penyedia_name);
        json.addProperty("start_time",historyDatafeed.start_time);
        json.addProperty("total_product",historyDatafeed.total_product);
        json.addProperty("total_inserted",historyDatafeed.total_inserted);
        json.addProperty("total_updated",historyDatafeed.total_updated);
        json.addProperty("total_failed",historyDatafeed.total_failed);
        renderJSON(json);
    }

}
