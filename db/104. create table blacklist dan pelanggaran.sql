create TABLE blacklist
(
	id bigint auto_increment primary key
	, blacklist_source_id bigint
	, rkn_id varchar(20)
	, repo_id varchar(20)
	, sk varchar(500)
	, nama_penyedia varchar(500)
	, npwp varchar(500)
	, alamat varchar(500)
	, alamat_tambahan varchar(500)
	, direktur varchar(500)
	, npwp_direktur varchar(500)
	, tahun_anggaran int
	, created_at date
	, sk_pencabutan varchar(500)
	, kabupaten_id bigint
	, kabupaten_nama varchar(500)
	, provinsi_id bigint
	, provinsi_name varchar(500)
);

alter table blacklist add column satker_id bigint;

alter table blacklist add column satker_nama varchar(500);

alter table blacklist CHANGE provinsi_name  provinsi_nama varchar(500);

create table blacklist_pelanggaran
(
	id bigint auto_increment primary key
	, blacklist_id bigint references blacklist(id)
	, sk varchar(500)
	, started_at date
	, expired_at date
	, jenis_pelanggaran varchar(500)
	, deskripsi_pelanggaran varchar(2000)
	, created_at date
	, published_at date
)