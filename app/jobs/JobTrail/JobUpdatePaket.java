package jobs.JobTrail;

import play.Logger;
import play.db.jdbc.Query;
import play.db.jpa.NoTransaction;
import play.jobs.Job;

@NoTransaction
public class JobUpdatePaket extends Job {
    Long _id = 0l;
    public JobUpdatePaket(long id){
        _id= id;
    }
    public void doJob(){
        //pengecekan kembali kesesuaian no_paket dan id paket
        try{
            String sql = "update paket set no_paket = CONCAT(left(no_paket,10), id) where id = ?";
            int result = Query.update(sql,_id);
            Logger.info("Update no paket total:"+result);
        }catch (Exception e){
            Logger.error("PAKET SAVE: gagal save paket, di pengecekan no_paket, err:"+e.getMessage());
        }
    }
}
