package services.Login;

import models.user.User2;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import play.jobs.On;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
//https://stackoverflow.com/questions/3665441/cron-expression-for-particular-date
//@NoTransaction
//@On("0 30 11 13 5 ? 2019")//13 mei 2019 jam 10 lewat 6 menit 0 detik (harinya bebas ?)
@NoTransaction
public class JobMasterUpdateRkn extends Job {

    List<User2> list = new ArrayList<>();
    @Override
    public void doJob() throws Exception {
        Logger.info("START ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        int waitNext = 500;
        int loopNow = 1;
        Map<Long, String> pass =  list.stream().collect(Collectors.toMap(User2::getId, User2::getUser_password));
        Map<Long, String> users = list.stream().collect(Collectors.toMap(User2::getId, User2::getUser_name));

        for (Map.Entry<Long, String> entry : users.entrySet()) {
            //System.out.println(userid + " / " + username + "/ "+ password);
            JobUpdateRkn_Id jobrkn = new JobUpdateRkn_Id();
            jobrkn.userid = entry.getKey();
            jobrkn.username = entry.getValue();
            jobrkn.password = pass.get(jobrkn.userid);
            jobrkn.now();
            //istirahat
            loopNow++;
            if(loopNow==waitNext){
                loopNow=1;
                Thread.sleep(10000);
            }
        }
        Logger.info("<<<<<<<<<<<< count >>>>>>>>>>>>: "+list.size());

    }

    public void setList(){
        list = User2.findAllPenyediaDistributor();
    }

    @Override
    public void before() {
        setList();
    }
}
