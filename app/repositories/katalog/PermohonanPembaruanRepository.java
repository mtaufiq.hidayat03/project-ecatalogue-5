package repositories.katalog;

import models.common.AktifUser;
import models.permohonan.PermohonanPembaruan;
import models.permohonan.search.PermohonanPembaruanSearch;
import models.permohonan.search.PermohonanPembaruanSearchResult;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.util.TextUtils;
import play.db.jdbc.Query;
import utils.DateTimeUtils;

import java.util.Date;

public class PermohonanPembaruanRepository {

    public static PermohonanPembaruanSearchResult searchPackage(PermohonanPembaruanSearch search) {
        PermohonanPembaruanSearchResult model = new PermohonanPembaruanSearchResult(search);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT pp.*, p.nama_penyedia, pps.status_permohonan, pps.from_status, pps.to_status, kldi.nama kldi_nama ")
                .append("FROM permohonan_pembaruan pp ")
                .append("JOIN penyedia p ON pp.penyedia_id = p.id ")
                .append("LEFT JOIN kldi ON pp.kldi_id = kldi.id ")
                .append("LEFT JOIN permohonan_pembaruan_status pps ON pp.id = pps.permohonan_pembaruan_id ");

        sb.append(" WHERE pp.active = 1 ");
        if (!TextUtils.isEmpty(search.keyword)) {
            sb.append(" AND (pp.deskripsi LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%'");
            sb.append(" OR pp.nomor_permohonan LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%' )");
        }

        if (search.uid > 0) {
            if(AktifUser.getAktifUser().isProvider()){
                sb.append(" AND pp.created_by = ").append(search.uid);
            }

            if(AktifUser.getAktifUser().isAdminLokalSektoral()){
                sb.append(" AND pp.kldi_id = '").append(AktifUser.getAktifUser().kldi_id).append("' ");
            }
        }

        if(!search.posisi.equalsIgnoreCase("all")){
            if(search.posisi.equalsIgnoreCase(PermohonanPembaruan.STATUS_PENYEDIA)){
                sb.append("AND (pps.to_status = '").append(search.posisi).append("' OR pps.to_status is null OR pps.to_status = '') ");
            }else{
                sb.append("AND pps.to_status = '").append(search.posisi).append("'");
            }
        }

        if (search.dis_ui > 0) {
            sb.append(" AND ( pp.dispo_user_id = ").append(search.dis_ui);
            sb.append(" OR pp.klarif_user_id = ").append(search.dis_ui).append(" )");
        }

        if (search.penyedia > 0) {
            sb.append(" AND pp.penyedia_id = ").append(search.penyedia);
        }

        if (!TextUtils.isEmpty(search.tipe)) {
            sb.append(" AND pp.tipe_permohonan = '").append(search.tipe).append("' ");
        }

        if (!TextUtils.isEmpty(search.jenis)) {
            sb.append(" AND pp.jenis_komoditas = '").append(search.jenis).append("' ");
        }

        if(!TextUtils.isEmpty(search.tglpengajuan)){
            String result = "";
            try{
                Date tanggal = DateTimeUtils.formatStringToDate(search.tglpengajuan);
                result = DateTimeUtils.formatDateToString(tanggal,"YYYY-MM-dd");
                result = StringEscapeUtils.escapeSql(result);
            }catch (Exception e){
                e.printStackTrace();
            }
            sb.append(" AND pp.minta_disetujui_tanggal LIKE '%").append(result).append("%'");
        }

        if(!TextUtils.isEmpty(search.status)){
            sb.append(" AND pp.status = '").append(search.status).append("'");
        }

        Long total = PermohonanPembaruanRepository.generateTotal(sb.toString());
        model.setTotal(total);

        if(!TextUtils.isEmpty(search.order)){
            if(search.order.equalsIgnoreCase("1")){
                sb.append(" ORDER BY pp.tipe_permohonan ASC ");
            }else if(search.order.equalsIgnoreCase("2")){
                sb.append(" ORDER BY pp.tipe_permohonan DESC ");
            }else if(search.order.equalsIgnoreCase("3")){
                sb.append(" ORDER BY pp.minta_disetujui_tanggal ASC ");
            }else if(search.order.equalsIgnoreCase("4")){
                sb.append(" ORDER BY pp.minta_disetujui_tanggal DESC ");
            }else if(search.order.equalsIgnoreCase("5")){
                sb.append(" ORDER BY pp.created_date ASC ");
            }else if(search.order.equalsIgnoreCase("6")){
                sb.append(" ORDER BY pp.created_date DESC ");
            }
        }

        if (!model.isEmpty()) {
            sb.append(" LIMIT ").append(model.getMax()).append(" OFFSET ").append(model.getFrom());
            model.items = Query.find(sb.toString(), PermohonanPembaruan.class).fetch();
        }
        return model;
    }

    private static Long generateTotal(String query) {
        Long total = Query.find("SELECT COUNT(id) FROM (" + query + ") p", Long.class).first();
        return total;
    }
}
