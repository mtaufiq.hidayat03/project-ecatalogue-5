package controllers.admin;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.ServiceResult;
import models.common.AktifUser;
import models.common.DokumenInfo;
import models.common.TahapanJadwal;
import models.katalog.Komoditas;
import models.katalog.KomoditasIkon;
import models.katalog.KomoditasWilayah;
import models.katalog.form.FormAtributHarga;
import models.katalog.form.FormKomoditas;
import models.katalog.form.FormProdukKategoriAtribut;
import models.katalog.komoditasmodel.*;
import models.masterdata.Bidang;
import models.masterdata.Kldi;
import models.masterdata.Tahapan;
import models.prakatalog.DokumenUsulan;
import models.prakatalog.Usulan;
import models.secman.Acl;
import models.user.User;
import org.apache.commons.lang.ArrayUtils;
import play.Logger;
import play.Play;
import play.data.validation.Error;
import play.data.validation.Validation;
import play.i18n.Messages;
import play.libs.Files;
import utils.HtmlUtil;
import utils.LogUtil;

import java.io.File;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

import static controllers.cms.BeritaCtr.getRandom;

public class KomoditasCtr extends BaseController {

	public static final String TAG = "KomoditasCtr";
	public static final int KOMODITAS_LOKAL = 5;
	public static final int KOMODITAS_SEKTORAL = 6;
	private static final String createTemplateJadwalHtml = "admin/KomoditasCtr/createTemplateJadwal.html";

	@AllowAccess({Acl.COM_KOMODITAS})
	public static void index() {
		List<KategoriKomoditas> kategoriKomoditas = KategoriKomoditas.findAllActive();
		List<Kldi> lokasiKldi = new ArrayList<>();
		render("admin/KomoditasCtr/index.html", kategoriKomoditas,lokasiKldi);
	}

	@AllowAccess({Acl.COM_KOMODITAS_ADD})
	public static void create() {
		FormKomoditas form = new FormKomoditas();
		FormAtributHarga atributHarga = new FormAtributHarga();
		renderCreate(null,form,null);
	}

	@AllowAccess({Acl.COM_KOMODITAS_ADD})
	public static void generateKode(String nama){
		if(nama==null || nama.isEmpty()){
			renderText("");
			return;
		}
		List<String> words = Arrays.asList(nama.toUpperCase().split(" "));
		String numbers = "0123456789";
		String candidate;
		if(words.size()>=3){
			List<String> candidates = words.subList(0,3);
			candidate=cobaKodeTigaKata(candidates);
			if(candidate!=null){
				renderText(candidate);
				return;
			}
		}
		if(words.size()>=2){
			List<String> candidates = new ArrayList<String>(words.subList(0,2));
			candidates.add(numbers);
			candidate=cobaKodeTigaKata(candidates);
			if(candidate!=null){
				renderText(candidate);
				return;
			}
		}
		if(words.size()>=1) {
			List<String> candidates = new ArrayList<String>(words.subList(0,1));
			candidates.add(numbers);
			candidates.add(numbers);
			candidate=cobaKodeTigaKata(candidates);
			if(candidate!=null){
				renderText(candidate);
				return;
			}
		}
		renderText("");
	}

	private static String cobaKodeTigaKata(List<String> words){
			String word1 = words.get(0);
			String word2 = words.get(1);
			String word3 = words.get(2);
			String candidate="";
			boolean success=false;
			for(int i = 0; i < word1.length() && !success ; i++){
				for(int j = 0 ; j < word2.length() && !success ; j++){
					for(int k = 0 ; k < word3.length() && !success ; k++){
						candidate=""+word1.charAt(i)+word2.charAt(j)+word3.charAt(k);
						success=isKodeAvailable(candidate);
					}
				}
			}
			if(success){
				return candidate;
			}
			return null;
	}

	public static void validateName(FormKomoditas form) {
		Komoditas komoditas = Komoditas.find("lower(nama_komoditas) = lower(?)", form.komoditas.nama_komoditas).first();
		boolean isOk = komoditas == null;
		if(isOk)
			renderText("true");
		else
			renderText("false");
	}

	public static void validateKode(FormKomoditas form) {
		boolean isOk = isKodeAvailable(form.komoditas.kode_komoditas);
		if(isOk)
			renderText("true");
		else
			renderText("false");
	}

	private static boolean isKodeAvailable(String kode){
		if(kode==null || kode.isEmpty())
			return false;
		Komoditas komoditas = Komoditas.find("lower(kode_komoditas) = lower(?)", kode).first();
		return komoditas == null;
	}

	public void getKldi(String param){
		List<Kldi> kldi = new ArrayList<>();
		if(param.equalsIgnoreCase("lokal")){
			kldi = Kldi.findLokal();
		}else if (param.equalsIgnoreCase("sektoral")){
			kldi = Kldi.findSektoral();
		}
		renderJSON(kldi);
	}
	public void getKldiLocation(String param){
		List<Kldi> kldi = new ArrayList<>();
		kldi = Kldi.findLokalLocation();
		renderJSON(kldi);
	}

	private static void renderCreate(Integer[] file_id, FormKomoditas form, Usulan usulan){
		List<KategoriKomoditas> kategori = KategoriKomoditas.findAllActive();
		List<Bidang> bidang = Bidang.findAllActive();
		List<DokumenInfo> dokumenInfoList = new ArrayList<DokumenInfo>();
		List<Tahapan> tahapanList = Tahapan.find("active = ?",Tahapan.AKTIF).fetch();

		List<Map<String,Object>> tahapanMapList = new ArrayList<>();
		for(Tahapan tahapan : tahapanList){
			Map<String, Object> tahapanMap = new HashMap<>();
			tahapanMap.put("id",tahapan.id);
			tahapanMap.put("nama",tahapan.nama_tahapan_alias);
			tahapanMapList.add(tahapanMap);
		}

		String tahapanJson = new Gson().toJson(tahapanMapList);

		List<User> pokjaList = User.findJustPokjaList();

		if(file_id != null){
			List<DokumenUsulan> dokumenUsulanList = new ArrayList<DokumenUsulan>();
			for(int i = 0; i<file_id.length;i++){
				DokumenUsulan dokumenUsulan = DokumenUsulan.findByDokId(file_id[i]);
				dokumenUsulanList.add(dokumenUsulan);
			}
			dokumenInfoList = DokumenInfo.getUploadInfoList(dokumenUsulanList);
		}

		List<Kldi> kldiList = Kldi.findAll();
		boolean isCreateKomoditas = true;
		int datafeed = Komoditas.KOMODITAS_DATAFEED;
		int katalog = Komoditas.KOMODITAS_KATALOG;

		render("admin/KomoditasCtr/create.html", form, bidang, usulan, kategori, kldiList, dokumenInfoList, tahapanJson,
				pokjaList, isCreateKomoditas,datafeed,katalog);
	}


	@AllowAccess({Acl.COM_KOMODITAS_ADD})
	public static void createSubmit(
			Integer[] file_id,
			FormKomoditas form,
			Usulan usulan,
			List<FormProdukKategoriAtribut> formKategoriAtribut,
			File pokjafile,
			long[] pokja,
			int[] file_pokja_id,
			String[] wilayah
	) {
		LogUtil.debug(TAG, form);
		validation.valid(form.komoditas);
		if(Validation.hasErrors()){
			for (Error error:Validation.errors()) {
				Logger.info("error validasi: "+error.message());
			}
		}
		if (validation.hasErrors()){
			validation.keep();
			params.flash();
			flash.error("Komoditas Gagal Tersimpan");
			renderCreate(file_id,form,usulan);
		}
		if (form.komoditas.id == null) {
			final String nama_komoditas = form != null && form.komoditas != null ? form.komoditas.nama_komoditas : "";
			Komoditas sameName = Komoditas.find("nama_komoditas = ?", nama_komoditas).first();
			if (sameName != null) {
				params.flash();
				flash.error("Sudah ada komoditas yang memiliki nama yang sama");
				renderCreate(file_id, form, usulan);
			}
		}
		try{
			//encode decode

			Komoditas komoditas = form.komoditas;
			komoditas.bidang_id = form.komoditas.bidang_id;
			komoditas.penyedia_input_merk = form.komoditas.penyedia_input_merk;
			komoditas.active = true;
			komoditas.terkunci = false;
			Long id = Long.valueOf(komoditas.save());
			if (komoditas.id == null) {
				komoditas.id = id;
			}

			//wilayah isinya kldi id
			if(null != wilayah && wilayah.length > 0){
				for (String kldiwilayah: wilayah) {
					KomoditasWilayah.saveNew(kldiwilayah, komoditas.id);
				}
			}

			/*
			* todo:
			* mesti dibuat manajemennya, dan belum, jika komoditas template kontrak tidak ada isinya sama sekali akan masalah
			* ketika dowwnload kontrak
			*/

			KomoditasTemplateKontrak komoditasTemplateKontrakFirst = KomoditasTemplateKontrak.getOneIdKomoditas();
			List<KomoditasTemplateKontrak> komoditasTemplateKontrakDuplicate = KomoditasTemplateKontrak.findByKomoditas(komoditasTemplateKontrakFirst.komoditas_id);
			for (KomoditasTemplateKontrak komoditasTemplateKontrakDuplicateLoop : komoditasTemplateKontrakDuplicate){
				KomoditasTemplateKontrak komoditasTemplateKontrakSave = new KomoditasTemplateKontrak();
				komoditasTemplateKontrakSave.komoditas_id = komoditas.id;
				komoditasTemplateKontrakSave.jenis_dokumen = komoditasTemplateKontrakDuplicateLoop.jenis_dokumen;
				komoditasTemplateKontrakSave.template = komoditasTemplateKontrakDuplicateLoop.template;
				komoditasTemplateKontrakSave.active = true;
				komoditasTemplateKontrakSave.save();
			}

			flash.success("Data berhasil disimpan");
			index();

		}catch(Exception e){
			Logger.error("error saving new Komoditas:"+e.getMessage());
			e.printStackTrace();
		}

	}

	@AllowAccess({Acl.COM_KOMODITAS_ADD})
	public static void automatedSaving(
			FormKomoditas form,
			FormAtributHarga atributHarga,
			String[] wilayah
	) {
		LogUtil.debug(TAG, form);
		validation.valid(form.komoditas);
		if (Validation.hasErrors()) {
			for (Error error : Validation.errors()) {
				Logger.info("error validasi: " + error.message());
			}
		}
		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
			renderJSON(new ServiceResult<>("Komoditas Gagal Tersimpan"));
		}

		try{
			//encode decode
			Komoditas komoditas = form.komoditas;
			komoditas.bidang_id = form.komoditas.bidang_id;
			komoditas.penyedia_input_merk = form.komoditas.penyedia_input_merk;
			komoditas.active = true;
			komoditas.terkunci = false;
			Long id = Long.valueOf(komoditas.save());
			if (komoditas.id == null) {
				komoditas.id = id;
			}

			if (atributHarga != null) {
				atributHarga.komoditas_id = komoditas.id;
				KomoditasAtributHarga.saveAtributHarga(atributHarga);
			}
			//wilayah isinya kldi id
			if(null != wilayah && wilayah.length > 0){
				for (String kldiwilayah: wilayah) {
					KomoditasWilayah.saveNew(kldiwilayah, komoditas.id);
				}
			}

			KomoditasTemplateKontrak komoditasTemplateKontrakFirst = KomoditasTemplateKontrak.getOneIdKomoditas();
			List<KomoditasTemplateKontrak> komoditasTemplateKontrakDuplicate = KomoditasTemplateKontrak.findByKomoditas(
					komoditasTemplateKontrakFirst.komoditas_id
			);
			for (KomoditasTemplateKontrak komoditasTemplateKontrakDuplicateLoop : komoditasTemplateKontrakDuplicate){
				KomoditasTemplateKontrak komoditasTemplateKontrakSave = new KomoditasTemplateKontrak();
				komoditasTemplateKontrakSave.komoditas_id = komoditas.id;
				komoditasTemplateKontrakSave.jenis_dokumen = komoditasTemplateKontrakDuplicateLoop.jenis_dokumen;
				komoditasTemplateKontrakSave.template = komoditasTemplateKontrakDuplicateLoop.template;
				komoditasTemplateKontrakSave.active = true;
				komoditasTemplateKontrakSave.save();
			}

			renderJSON(new ServiceResult<>(true, "success", komoditas));
		}catch(Exception e){
			LogUtil.error(TAG, e);
			renderJSON(new ServiceResult<>("Error: " + e.getMessage()));
		}
	}

	@AllowAccess({Acl.COM_KOMODITAS_EDIT})
	public static void edit(long id) {
		List<KategoriKomoditas> kategori = KategoriKomoditas.findAllActive();
		List<Bidang> bidangs = Bidang.findAllActive();
		Komoditas komoditas = Komoditas.findById(id);
		komoditas.lokasiKldi = komoditas.locationSelected();
		render("admin/KomoditasCtr/edit.html",kategori, komoditas, bidangs);
	}

	@AllowAccess({Acl.COM_KOMODITAS_EDIT})
	public static void setKunciKomoditas(long id, String param) {
		Komoditas model = Komoditas.findById(id);
		notFoundIfNull(model);
		switch (param) {
			case "terkunci":
				model.setLock();
				flash.success(Messages.get("notif.cms.polling.locked"));
				break;
//			case "terbuka":
//				model.setUnlock();
//				flash.success(Messages.get("notif.cms.polling.unlocked"));
//				break;
		}
		index();
	}

	@AllowAccess({Acl.COM_KOMODITAS_EDIT})
	public static void editSubmit(long komoditas_id, Komoditas komoditas) {
		komoditas.id = komoditas_id;
		Validation validation = Validation.current();
		validation.valid(komoditas);

		if (validation.hasErrors()){
			Logger.info(validation.errorsMap().size()+" not valid validation");
			for (Error er: Validation.errors()) {
				Logger.error(er.message());
			}
			validation.keep();
			params.flash();
			flash.error("Periksa kembali inputan anda");
			edit(komoditas_id);
		} else {
			komoditas.active = true;
			komoditas.bidang_id = komoditas.bidang_id;
			komoditas.penyedia_input_merk = komoditas.penyedia_input_merk;
			komoditas.save();

			//wilayah isinya kldi id
			if(null != komoditas.lokasiKldi && !komoditas.lokasiKldi.isEmpty()){
				List<String> existingDb = komoditas.locationSelected();
				List<String> newselected = komoditas.lokasiKldi;

				KomoditasWilayah.deleteNotIn(komoditas,komoditas.lokasiKldi);

				newselected.removeAll(existingDb);//clean yang sama, yang gak sama disimpan
				for (String kldiwilayah: newselected) {
					KomoditasWilayah.saveNew(kldiwilayah, komoditas_id);
				}



			}
			flash.success("Data berhasil disimpan");
			index();
		}


	}

	@AllowAccess({Acl.COM_KOMODITAS_DELETE})
	public static void delete(long id) {
		Komoditas model = Komoditas.findById(id);
		notFoundIfNull(model);
		if (model.isAllowToBeDeleted()) {
			model.softDelete();
			flash.success(Messages.get("notif.success.delete"));
		} else {
			flash.error(Messages.get("notif.failed.delete.relation"));
		}
		index();
	}

	@AllowAccess({Acl.COM_KOMODITAS})
	public static void detail(long id){
		Komoditas komoditas = Komoditas.getDetailById(id);
		Logger.info("id #### " + id);
		Logger.info("komoditasnya #### " + komoditas);
		String kelasHarga = Komoditas.kelasHargaMap().get(komoditas.kelas_harga);

		render("admin/KomoditasCtr/detail.html", komoditas, kelasHarga);
	}

	@AllowAccess({Acl.COM_KOMODITAS})
	public static void createTemplateJadwal(long id){
		Komoditas komoditas = Komoditas.findById(id);
		List<Tahapan> tahapanList = Tahapan.find("active = ? and urutan_tahapan is not null and urutan_tahapan != '' order by urutan_tahapan ASC",Tahapan.AKTIF).fetch();
        List<Tahapan> flaggingList = Tahapan.getAllFlagging();
		FormKomoditas form = new FormKomoditas();
		renderArgs.put("action", "create");
		render(komoditas, tahapanList, form, flaggingList);
	}

	@AllowAccess({Acl.COM_KOMODITAS})
	public static void createTemplateJadwalSubmit(FormKomoditas form) {
		checkAuthenticity();
		validation.valid(form.templateJadwal);
		LogUtil.d(TAG, form);
		if (validation.hasErrors()) {
			params.flash();
			flash.error(Messages.get("komoditas.template.create.error"));
			Komoditas komoditas = Komoditas.findById(form.templateJadwal.komoditas_id);
			List<Tahapan> tahapanList = Tahapan.find("active = ? and urutan_tahapan is not null and urutan_tahapan != '' order by urutan_tahapan ASC",Tahapan.AKTIF).fetch();

			form.jadwal = Arrays.asList(form.tahapan_arr);

			render(createTemplateJadwalHtml,komoditas, tahapanList, form);
		} else {
			//encode decode
			String decodedText = HtmlUtil.decodeFromTinymce(form.templateJadwal.deskripsi);
			form.templateJadwal.deskripsi = decodedText;

			KomoditasTemplateJadwal model;
			if (form.isCreateMode()) {
				model = new KomoditasTemplateJadwal().setDataForm(form);
			} else {
				model = KomoditasTemplateJadwal.findById(form.templateJadwal.id);
				model.setDataForm(form);
			}
			model.saveTemplateJadwal();
			if (!ArrayUtils.isEmpty(form.tahapan_arr)) {
				if (!form.isCreateMode()) {
					KomoditasTahapanTemplateJadwal.delete("template_id = ?", form.templateJadwal.id);
				}
				for (long stepId : form.tahapan_arr) {
					new KomoditasTahapanTemplateJadwal(stepId, model.id).save();
				}
			}
			detail(form.templateJadwal.komoditas_id);
		}
	}

	@AllowAccess({Acl.COM_KOMODITAS})
	public static void editTemplateJadwal(long id, long template_id){
		FormKomoditas form = new FormKomoditas(template_id);
        List<Tahapan> tahapanList = Tahapan.find("active = ? order by urutan_tahapan ASC",Tahapan.AKTIF).fetch();
        List<Tahapan> flaggingList = Tahapan.getAllFlagging();

		List<KomoditasTahapanTemplateJadwal> templatetahapanList = KomoditasTahapanTemplateJadwal.findByTemplateId(template_id);
		form.jadwal = templatetahapanList.stream().map(t -> t.tahapan_id).collect(Collectors.toList());
		String selectedTahapanJson = KomoditasTahapanTemplateJadwal.getSelectedTahapanTemplate(templatetahapanList);
		List<TahapanJadwal> listTahapanJadwal = TahapanJadwal.getList(templatetahapanList);
		renderArgs.put("action", "update");
		renderArgs.put("komoditas", form.komoditas);
		render("admin/KomoditasCtr/createTemplateJadwal.html", form, tahapanList, selectedTahapanJson, listTahapanJadwal, flaggingList);
	}

	@AllowAccess({Acl.COM_KOMODITAS})
	public static void getDetailTemplate(String template_id){
		List<KomoditasTemplateJadwal> listDetailTemplate = KomoditasTemplateJadwal.getListDetailTemplate(Long.parseLong(template_id));

		renderJSON(listDetailTemplate);
	}

	@AllowAccess({Acl.COM_KOMODITAS})
	public static void deleteTemplateJadwal(long id, long template_id){
		KomoditasTemplateJadwal model = new KomoditasTemplateJadwal().findByIdActive(template_id, KomoditasTemplateJadwal.class);
		notFoundIfNull(model);
		if (model.isAllowToBeDeleted()) {
			KomoditasTahapanTemplateJadwal.delete("template_id = ?", template_id);
			model.softDelete();
			flash.success(Messages.get("notif.success.delete"));
		} else {
			flash.error(Messages.get("notif.failed.delete.relation"));
		}
		detail(model.komoditas_id);
	}

	public static void editTemplateKontrak(long id){
		KomoditasTemplateKontrak dok = KomoditasTemplateKontrak.findById(id);
		render(dok);
	}

	public static void editTemplateKontrakSubmit(KomoditasTemplateKontrak dok){
		validation.valid(dok);
		if(validation.hasErrors()){
			params.flash();
			flash.error("Error");
			render("admin/KomoditasCtr/editTemplateKontrak.html", dok);
		}else{
			KomoditasTemplateKontrak dokDb = KomoditasTemplateKontrak.findById(dok.id);
			dokDb.template = dok.template;
			dokDb.active = true;
			dokDb.save();
		}

		detail(dok.komoditas_id);
	}

	@AllowAccess({Acl.COM_KOMODITAS_DELETE})
	public static void setAktifKomoditas(long id) {
		Komoditas model = Komoditas.findById(id);
		notFoundIfNull(model);
		model.setAktif();
		flash.success(Messages.get("notif.success.setaktif"));
		index();
	}

	@AllowAccess({Acl.COM_KOMODITAS_DELETE})
	public static void setNonAktifKomoditas(long id) {
		Komoditas model = Komoditas.findById(id);
		notFoundIfNull(model);
		model.setNonAktif();
		flash.success(Messages.get("notif.success.setnon_aktif"));
		index();
	}

	@AllowAccess({Acl.COM_KOMODITAS_DELETE})
	public static void setAktivasiKomoditas(long id, String param) {
		Komoditas model = Komoditas.findById(id);
		notFoundIfNull(model);
		switch (param) {
			case "non_aktif":
				model.setNonAktif();
				flash.success(Messages.get("notif.success.setnon_aktif"));
				break;
			case "aktif":
				model.setAktif();
				flash.success(Messages.get("notif.success.setaktif"));
				break;
		}
		index();
	}

	public static void nasional() {
		render("admin/KomoditasCtr/listNasional.html");
	}

	public static void lokal() {
		long komoditas_kategori_id = KOMODITAS_LOKAL;
		render("admin/KomoditasCtr/listLokal.html", komoditas_kategori_id);
	}

	public static void sektoral() {
		long komoditas_kategori_id = KOMODITAS_SEKTORAL;
		render("admin/KomoditasCtr/listLokal.html", komoditas_kategori_id);
	}

	public static void createSubmitIkon(Long komoditas_id, File ikon) {

		KomoditasIkon komoditasIkonCheck = KomoditasIkon.findByKomoditasId(komoditas_id);

		if (komoditasIkonCheck == null) {

			KomoditasIkon komoditasIkon = new KomoditasIkon();
			komoditasIkon.komoditas_id = komoditas_id;

			if (ikon != null) {
				String nama_file = getRandom() + "-" + ikon.getName().replace(" ", "-");
				File dokumenroot = Play.applicationPath;
				File destination = new File(dokumenroot + "/public/files/image/komoditas/" + nama_file);
				Files.copy(ikon, destination);
				komoditasIkon.file_name = nama_file;
				komoditasIkon.original_file_name = ikon.getName();
				komoditasIkon.file_size = destination.length();
				komoditasIkon.save();
			}

		}else {

			if (ikon != null) {
				String nama_file = getRandom() + "-" + ikon.getName().replace(" ", "-");
				File dokumenroot = Play.applicationPath;
				File destination = new File(dokumenroot + "/public/files/image/komoditas/" + nama_file);
				Files.copy(ikon, destination);
				komoditasIkonCheck.file_name = nama_file;
				komoditasIkonCheck.original_file_name = ikon.getName();
				komoditasIkonCheck.file_size = destination.length();
				komoditasIkonCheck.modified_by = AktifUser.getActiveUserId();
				komoditasIkonCheck.modified_date = new Timestamp(System.currentTimeMillis());
				komoditasIkonCheck.save();
			}

		}

		flash.success(Messages.get("notif.success.submit"));
		index();
	}

	public static void getImageIcon(Long komoditas_id){
		KomoditasIkon komoditasIkon = KomoditasIkon.findByKomoditasId(komoditas_id);
		JsonObject jsonIcon = new JsonObject();
		jsonIcon.addProperty("icon", komoditasIkon.file_name);
		renderJSON(jsonIcon);
	}

	public static void validasiKomoditas(){
		String nama =params.get("nama") != null ? params.get("nama", String.class) : "";
		Komoditas temp = new Komoditas();
		Map<String, Object> result = new HashMap<>(1);

		temp.nama_komoditas = nama;
		Boolean isOk = false;
		if(!nama.isEmpty()){
			isOk = !Komoditas.isExistByName(temp);
			if (Komoditas.isExistByName(temp)){
				result.put("message","Nama Komoditas sudah digunakan");
			} else{
				result.put("message","Nama Komoditas valid digunakan");
			}
		}
		result.put("valid",isOk);

		renderJSON(result);
	}

}
