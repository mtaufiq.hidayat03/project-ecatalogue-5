package models.util.produk;

import java.util.Map;

import ext.StringFormat;
import java.math.BigDecimal;
import java.util.HashMap;

/**
 * Created by dadang on 8/28/17.
 */
public class Perbandingan {

	public long id;
	public String nama;
	public String sebelum;
	public String sesudah;

	public Map<Long, BigDecimal> hargaSebelum = new HashMap<Long, BigDecimal>();
	public Map<Long, BigDecimal> hargaSesudah = new HashMap<Long, BigDecimal>();

	//harga
	public double hargaRetailSebelum;
	public double hargaPemerintahSebelum;
	public double hargaOngkirSebelum;
	public double hargaRetailSesudah;
	public double hargaPemerintahSesudah;
	public double hargaOngkirSesudah;

	public String getHarga(double harga){
		return StringFormat.formatCurrency(harga);
	}
}
