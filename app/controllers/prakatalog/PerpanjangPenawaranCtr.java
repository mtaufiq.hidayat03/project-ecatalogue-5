package controllers.prakatalog;

import com.google.gson.*;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import controllers.BaseController;
import controllers.katalog.form.ProductForm;
import ext.FormatUtils;
import models.common.AktifUser;
import models.common.AllLevelProdukKategori;
import models.common.ButtonGroup;
import models.common.DokumenInfo;
import models.jcommon.util.CommonUtil;
import models.katalog.*;
import models.katalog.komoditasmodel.KomoditasAtributHarga;
import models.masterdata.*;
import models.penyedia.Penyedia;
import models.prakatalog.*;
import models.user.User;
import models.util.produk.HargaProdukJson;
import models.util.produk.OngkirProdukJson;
import models.util.produk.ProvKabJson;
import models.util.produk.RiwayatHargaProdukJson;
import models.util.sikap.Identitas;
import models.util.sikap.Sikap;
import org.apache.commons.lang.ArrayUtils;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.cache.Cache;
import play.i18n.Messages;
import play.mvc.Router;
import repositories.WilayahRepository;
import repositories.penyedia.ProviderRepository;
import repositories.produkkategori.ProdukKategoriRepository;
import services.datatable.DataTableService;
import services.produkkategori.ProdukKategoriService;
import utils.LogUtil;
import utils.SikapUtils;

import java.io.File;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class PerpanjangPenawaranCtr extends BaseController {
	public static final String TAG = "PerpanjangPenawaranCtr";
	//http://localhost:2209/prakatalog/perpanjang_penawaran/edit/1032
	public static void edit(long id){
		String page = "edit";
		long rkn_id = new Long(0);
		Penawaran penawaran = new Penawaran();
		Sikap dataSikap = new Sikap();
		Identitas identitas = null;
		List<UsulanKualifikasi> kualifikasiUsulanList = new ArrayList<>();
		List<DokumenInfo> dokumenPenawaranList = new ArrayList<>();
		List<DokumenInfo> dokumenPerpanjanganList = new ArrayList<>();
		List<UsulanDokumenPenawaran> dokumenUsulanPenawaran = new ArrayList<>();
		List<UsulanDokumenPerpanjangan> dokumenUsulanPerpanjangan = new ArrayList<>();
		List<Produk> listProduk = new ArrayList<>();
        Perpanjangan perp = new Perpanjangan();
		try{

			AktifUser aktifUser = getAktifUser();
			rkn_id = aktifUser.rkn_id;
			penawaran = Penawaran.findById(id);
			perp= Perpanjangan.find("penawaran_id=? and selesai=0",penawaran.id).first();

			String cacheKey = String.valueOf(rkn_id+"_perp");
			if(perp!=null){

					PerpanjanganStaging obj = PerpanjanganStaging.find("class_name=? and isList=0 and perpanjangan_id=?",new Penawaran().getClass().getName(), perp.id).first();
					penawaran =  new Gson().fromJson(obj.object_json, Penawaran.class);//(Penawaran) (Object) obj.getRealObjec();

					obj = PerpanjanganStaging.find("class_name=? and isList=1 and perpanjangan_id=?",new UsulanKualifikasi().getClass().getName(), perp.id).first();
					kualifikasiUsulanList = (List<UsulanKualifikasi>)(Object)obj.getListData(new TypeToken<List<UsulanKualifikasi>>(){}.getType());

					obj =  PerpanjanganStaging.find("class_name=? and isList=1 and perpanjangan_id=?",new DokumenPenawaran().getClass().getName(), perp.id).first();
					List<DokumenPenawaran> temp = (List<DokumenPenawaran>) (Object)obj.getListData(new TypeToken<List<DokumenPenawaran>>(){}.getType());

					obj = PerpanjanganStaging.find("class_name=? and isList=1 and perpanjangan_id=?",new DokumenPerpanjangan().getClass().getName(), perp.id).first();

					List<DokumenPerpanjangan> temp2 = DokumenPerpanjangan.find("perp_id=?",perp.id).fetch();
					List<DokumenPerpanjangan> temp3 = (List<DokumenPerpanjangan>) (Object)obj.getListData(new TypeToken<List<DokumenPerpanjangan>>(){}.getType());

					dokumenPenawaranList = DokumenInfo.listDokumenPenawaran(temp);
					dokumenPerpanjanganList = DokumenInfo.listDokumenPerpanjangan(temp3);

					obj = PerpanjanganStaging.find("class_name=? and isList=0 and perpanjangan_id=?",new Sikap().getClass().getName(), perp.id).first();
					dataSikap =  new Gson().fromJson( obj.object_json, Sikap.class);// (Sikap) (Object)obj.getRealObjec();

					obj = PerpanjanganStaging.find("class_name=? and isList=1 and perpanjangan_id=?",new UsulanDokumenPenawaran().getClass().getName(), perp.id).first();
					dokumenUsulanPenawaran = (List<UsulanDokumenPenawaran>)(Object)obj.getListData(new TypeToken<List<UsulanDokumenPenawaran>>(){}.getType());

					obj = PerpanjanganStaging.find("class_name=? and isList=1 and perpanjangan_id=?",new UsulanDokumenPerpanjangan().getClass().getName(), perp.id).first();
					dokumenUsulanPerpanjangan = (List<UsulanDokumenPerpanjangan>)(Object)obj.getListData(new TypeToken<List<UsulanDokumenPerpanjangan>>(){}.getType());

					//setdatasikap
					dataSikap = penawaran.getDataSikap();
					Cache.set(cacheKey,dataSikap);
					identitas = dataSikap.identitas;

			}else{
				perp = new Perpanjangan();

				notFoundIfNull(penawaran);
				perp.penawaran_id = penawaran.id;

				perp.usulan_id = penawaran.usulan_id;
				perp.kontrak_id = penawaran.kontrak_id;
				perp.penyedia_id = penawaran.penyedia_id;

				perp.selesai=false;
				Long perp_id = new Long(perp.save());
				perp.id = perp_id;
				Logger.debug("new perpanjangan id:"+perp_id);

				if(null != perp_id && null != perp ){
					Gson gson = new Gson();

					//saving to staging
					PerpanjanganStaging perpusulanStagging = new PerpanjanganStaging();
					perpusulanStagging.perpanjangan_id = perp_id;
					Usulan usulan = Usulan.findById(penawaran.usulan_id);
					perpusulanStagging.class_name = usulan.getClass().getName();
					perpusulanStagging.object_json = gson.toJson(usulan);
					perpusulanStagging.isList = false;
					perpusulanStagging.save();
					//saving to staging
					PerpanjanganStaging perppenawaranStagging = new PerpanjanganStaging();
					perppenawaranStagging.perpanjangan_id = perp_id;
					perppenawaranStagging.class_name = penawaran.getClass().getName();
					perppenawaranStagging.object_json = gson.toJson(penawaran);
					perppenawaranStagging.isList = false;
					perppenawaranStagging.save();

					//Data SIKaP
					kualifikasiUsulanList = UsulanKualifikasi.find("usulan_id = ?",penawaran.usulan_id).fetch();

					List<DokumenPenawaran> listDokpen = DokumenPenawaran.listDokumenPenawaran(penawaran.id);
					dokumenPenawaranList = DokumenInfo.listDokumenPenawaran(listDokpen);

					List<DokumenPerpanjangan> listDokPerp = DokumenPerpanjangan.listDokumenPerpanjangan(usulan.id);
					dokumenPerpanjanganList = DokumenInfo.listDokumenPerpanjangan(listDokPerp);

					Logger.debug("usulan_id="+penawaran.usulan_id);

					identitas = SikapUtils.getRekananIdentitas(rkn_id);

					//jika tidak ada ambil dokumen penawaran yang di ajukan
					if(dokumenPenawaranList.size() <= 0){
						dokumenUsulanPenawaran = UsulanDokumenPenawaran.find("usulan_id = ?",penawaran.usulan_id).fetch();
					}
					if (dokumenPerpanjanganList.size() <= 0){
						dokumenUsulanPerpanjangan = UsulanDokumenPerpanjangan.find("usulan_id=?",perp.usulan_id).fetch();
					}
					//get produk terkait
					listProduk = Produk.getProdukForPerpanjangan(penawaran.id, penawaran.penyedia_id, penawaran.kontrak_id);

					//saving to stagging
					PerpanjanganStaging perppenkualifikasiStagging = new PerpanjanganStaging();
					perppenkualifikasiStagging.perpanjangan_id = perp_id;
					perppenkualifikasiStagging.class_name = UsulanKualifikasi.class.getName();
					perppenkualifikasiStagging.object_json = gson.toJson(kualifikasiUsulanList);
					perppenkualifikasiStagging.isList = true;
					perppenkualifikasiStagging.save();

					PerpanjanganStaging perppenDokPenStagging = new PerpanjanganStaging();
					perppenDokPenStagging.perpanjangan_id = perp_id;
					perppenDokPenStagging.class_name = DokumenPenawaran.class.getName();
					perppenDokPenStagging.object_json = gson.toJson(DokumenPenawaran.listDokumenPenawaran(penawaran.id));
					perppenDokPenStagging.isList = true;
					perppenDokPenStagging.save();

					PerpanjanganStaging perpsikapStagging = new PerpanjanganStaging();
					perpsikapStagging.perpanjangan_id = perp_id;
					perpsikapStagging.class_name = Sikap.class.getName();
					perpsikapStagging.object_json = penawaran.data_sikap;
					perpsikapStagging.isList = false;
					perpsikapStagging.save();

					PerpanjanganStaging dokusulDokpenStaging = new PerpanjanganStaging();
					dokusulDokpenStaging.perpanjangan_id = perp_id;
					dokusulDokpenStaging.class_name = UsulanDokumenPenawaran.class.getName();
					dokusulDokpenStaging.object_json = gson.toJson(dokumenUsulanPenawaran);
					dokusulDokpenStaging.isList = true;
					dokusulDokpenStaging.save();

					//Usulan Dokumen Perpanjangan
					PerpanjanganStaging dokUsulanPerpanStaging = new PerpanjanganStaging();
					dokUsulanPerpanStaging.perpanjangan_id = perp_id;
					dokUsulanPerpanStaging.class_name = UsulanDokumenPerpanjangan.class.getName();
					dokUsulanPerpanStaging.object_json=gson.toJson(dokumenUsulanPerpanjangan);
					dokUsulanPerpanStaging.isList = true;
					dokUsulanPerpanStaging.save();

					PerpanjanganStaging dokPerpanStagging = new PerpanjanganStaging();
					dokPerpanStagging.perpanjangan_id = perp_id;
					dokPerpanStagging.class_name = DokumenPerpanjangan.class.getName();
					dokPerpanStagging.object_json = gson.toJson(DokumenPerpanjangan.listDokumenPerpanjangan(usulan.id));
					dokPerpanStagging.isList = true;
					dokPerpanStagging.save();

					for (Produk p: listProduk) {
						PerpanjanganStaging perpProduk = new PerpanjanganStaging();
						perpProduk.perpanjangan_id = perp_id;
						perpProduk.class_name = Produk.class.getName();
						Gson g = new GsonBuilder()
								.setPrettyPrinting()
								.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX")
								.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
								.create();
						perpProduk.object_json = g.toJson(p);
						perpProduk.isList = false;
						perpProduk.save();
						Long produk_id = p.id;
						List<ProdukWilayahJualProvinsi> wilJualprop = ProdukWilayahJualProvinsi.find("produk_id = ?", produk_id).fetch();
						List<ProdukWilayahJualKabupaten> wilJualKab = ProdukWilayahJualKabupaten.find("produk_id = ?", produk_id).fetch();
						List<ProdukAtributValue> produkAtributValues = ProdukAtributValue.findByProdukId(produk_id);

						List<ProdukHarga> listProdukHarga = ProdukHarga.findRiwayatByProdukId(produk_id);
						ProdukHarga produkHarga = ProdukHarga.getByProductId(produk_id);
						List<ProdukGambar> list = ProdukGambar.findByProduk(produk_id);
						List<ProdukLampiran> attachments = ProdukLampiran.getDocumentsByProductId(produk_id);


					}
				}

			}

		}catch (Exception e){
			//identitas = new Identitas();
		}

		//renderEdit(penawaran, rkn_id, identitas, dataSikap);
		render("prakatalog/PerpanjangPenawaranCtr/edit.html", penawaran, rkn_id, identitas,
				kualifikasiUsulanList, dokumenPenawaranList, page, dataSikap, dokumenUsulanPenawaran,perp, dokumenUsulanPerpanjangan, dokumenPerpanjanganList);
	}

	public static void getDataSikap(String jenis_kualifikasi, long rkn_id){
		String key = String.valueOf(rkn_id);
		Sikap dataSikap = SikapUtils.getDataSikap(key,jenis_kualifikasi,rkn_id);
		renderJSON(dataSikap);

	}

	public static void editSubmit(Penawaran penawaran,
								  File[] dokumen, String[] keterangan, String[] nama_dokumen, Long[] dok_id,
								  Long perpanjangan_id, Long [] dokUsulan_id,Long penawaran_id) throws Exception {

	    if(null == dokumen){
	        dokumen = new File[0];
        }
	    if(null == dok_id){
	        dok_id = new Long[0];
        }
		Logger.debug("masuk submit");

        AktifUser aktifUser = getAktifUser();
        Logger.info("createsubmit: rkn_id:"+aktifUser.rkn_id);
        long rkn_id = aktifUser.rkn_id;
        Sikap cachedData = null;
        //handling
		PerpanjanganStaging obj = new PerpanjanganStaging();
        try{
            cachedData = Cache.get(String.valueOf(rkn_id+"_perp"),Sikap.class);
            //pada mode prod
            if(cachedData == null){
                //ambil lagi data SIKaP
                String key = String.valueOf(rkn_id);
                cachedData = SikapUtils.getDataSikap(key,"all",rkn_id);
            }

            //simppan sikap ke stagingnya
			obj = PerpanjanganStaging.find("class_name=? and isList=0 and perpanjangan_id=?",new Sikap().getClass().getName(), perpanjangan_id).first();
            obj.object_json = new Gson().toJson(cachedData);
            cachedData=null;
            obj.save();
        }catch (Exception e){
            Logger.error("error data sikap:"+e.getMessage());
            e.printStackTrace();
        }

        obj = PerpanjanganStaging.find("class_name=? and isList=0 and perpanjangan_id=?",
                        new Penawaran().getClass().getName(),
                        perpanjangan_id).first();
		//save to stagging penawaran
		Penawaran objp =  new Gson().fromJson(obj.object_json, Penawaran.class);//Penawaran) (Object)obj.getRealObjec();
		objp.pic_nama = penawaran.pic_nama;
		objp.pic_email = penawaran.pic_email;
		objp.pic_no_telp = penawaran.pic_no_telp;
		objp.pic_alamat = penawaran.pic_alamat;
		objp.pic_jabatan = penawaran.pic_jabatan;
		//save json
		if(obj != null){
			penawaran.data_sikap = new Gson().toJson(cachedData);

			penawaran.status = Penawaran.STATUS_MENUNGGU_VERIFIKASI;
			penawaran.active = true;
			obj.object_json = new Gson().toJson(objp);
			obj.save();

		}

		List<DokumenPerpanjangan> dokPerp = new ArrayList<DokumenPerpanjangan>();
		List<DokumenPenawaran> dokpen = new ArrayList<>();

		//buat list jika dokumen penawaran baru
		if(dokumen.length>0 && dok_id.length <= 0){
			for(int i = 0;i<dokumen.length;i++){
				try {
					//dokpen.add(DokumenPenawaran.setNewDokPenawaran(penawaran.id, dokumen[i], keterangan[i], nama_dokumen[i]));
					DokumenPerpanjangan dp=  DokumenPerpanjangan.setNewDokPerpanjangan(perpanjangan_id, dokUsulan_id[i], dokumen[i], nama_dokumen[i], keterangan[i]);
					dokPerp.add(dp);

				} catch (Exception e) {}
			}
		}else if(dokumen.length > 0 && dok_id.length > 0){
			obj =  PerpanjanganStaging.find("class_name=? and isList=1 and perpanjangan_id=?",new DokumenPerpanjangan().getClass().getName(), perpanjangan_id).first();
			List<DokumenPerpanjangan> temp = (List<DokumenPerpanjangan>) (Object)obj.getListData(new TypeToken<List<DokumenPerpanjangan>>(){}.getType());
			for(int i = 0;i<dok_id.length;i++){
				final Long ids = dok_id[i];
					try {
						DokumenPerpanjangan oldDokumenPerp = DokumenPerpanjangan.find("dok_id_attachment=?",ids).first();
						oldDokumenPerp.active = false;
						oldDokumenPerp.save();
						DokumenPerpanjangan a = DokumenPerpanjangan.setNewDokPerpanjangan(perpanjangan_id, oldDokumenPerp.usulan_dok_perp_id, dokumen[i], nama_dokumen[i], keterangan[i]);
						dokPerp.add(a);

					} catch (Exception e) {}
			}
		}
		obj = PerpanjanganStaging.find("class_name=? and isList=1 and perpanjangan_id=?",
				new DokumenPerpanjangan().getClass().getName(),
				perpanjangan_id).first();
		//save dok perpanjangan
		obj.object_json = new Gson().toJson(dokPerp);
		obj.save();
		flash.success(Messages.get("notif.success.submit"));
		edit(penawaran_id);
	}

	public void goToList(Long perp_id){
		render("prakatalog/PerpanjangPenawaranCtr/listProduk.html",perp_id);
	}

	public void listProduk(Long perp_id){
		List<PerpanjanganStaging> listProduk = PerpanjanganStaging.find("class_name=? and perpanjangan_id=?",new Produk().getClass().getName() , perp_id).fetch();
		HashMap<String,Object> params = new HashMap<>();
		Map<String, Object> param = new HashMap<>();
		List<String[]> result = new ArrayList<>();
		Gson gson = new GsonBuilder()
				.setPrettyPrinting()
				.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX")
				.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
				.create();
		for (int i=0;i<listProduk.size();i++){
			String [] results = new String[7];
			String str_obj = listProduk.get(i).object_json;
			Produk produk = gson.fromJson(str_obj, Produk.class);
			Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
			Manufaktur manufaktur = Manufaktur.findById(produk.manufaktur_id);
			Perpanjangan perp = Perpanjangan.findById(perp_id);
			List<ButtonGroup> buttonGroups = new ArrayList<>();
			param.put("id", produk.id);
			param.put("perp_id", perp_id);
			param.put("penawaran_id",perp.penawaran_id);
			param.put("komoditas_id",produk.komoditas_id);
			results[0] = produk.id.toString();
			results[1] = produk.no_produk+" "+manufaktur.nama_manufaktur+" "+produk.nama_produk;
			results[2] = produk.no_produk_penyedia;
			results[3] = produk.jenis_produk;
			results[4] = komoditas.apakah_stok_unlimited ? "Unlimited" : FormatUtils.formatDesimal2(produk.jumlah_stok);
			results[5] = produk.jumlah_stok_inden.toString();
			String urlEdit = Router.reverse("controllers.prakatalog.perpanjangPenawaranCtr.editProduk",param).url;
			String urlHapus =Router.reverse("controllers.admin.usulanCtr.deleteUsulanDokPerp",param).url;
			buttonGroups.add(new ButtonGroup(Messages.get("ubah.label"),urlEdit));
			//buttonGroups.add(new ButtonGroup(Messages.get("hapus.label"),urlHapus));
			results[6] = DataTableService.generateButtonGroup(buttonGroups);
			result.add(i,results);
		}
		params.put("data",result);
		params.put("recordsTotal",listProduk.size());
		params.put("recordsFiltered",listProduk.size());
		renderJSON(params);
	}

	public void editProduk(Long id, Long perp_id,Long penawaran_id, Long komoditas_id){
		Perpanjangan perpanjangan = Perpanjangan.findById(perp_id);
		Usulan usulan = Usulan.findById(perpanjangan.usulan_id);
		List<PerpanjanganStaging> listProduk = PerpanjanganStaging.find("class_name=? and perpanjangan_id=?",new Produk().getClass().getName() ,perpanjangan.id).fetch();
		Gson gson = new GsonBuilder()
				.setPrettyPrinting()
				.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX")
				.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
				.create();
		Produk produk = new Produk();
		for (int i = 0; i < listProduk.size(); i++) {
			String str_obj = listProduk.get(i).object_json;
			Produk prd = gson.fromJson(str_obj,Produk.class);
			if (id.equals(prd.id)){
				produk = prd;
			}
		}
		Komoditas komoditas = Komoditas.findById(komoditas_id);
		AktifUser aktifUser = getAktifUser();
		User user = User.findById(aktifUser.user_id);
		List<Manufaktur> manufakturList = Manufaktur.findManufakturByKomoditas(komoditas.id);
		List<ProdukKategori> produkKategoriList = ProdukKategoriRepository.getProdukKategoriList(komoditas.id);
		List<UsulanKategoriProduk> usulanKategoriProdukList = null;
		if (usulan != null) {
			usulanKategoriProdukList = UsulanKategoriProduk.findByUsulan(usulan.id);
		}
		List<AllLevelProdukKategori> kategoriList = ProdukKategoriService
				.generateSingleRowKategoriByUsulan(produkKategoriList, usulanKategoriProdukList);
		List<String> jenisProduk = new ArrayList<>(Arrays.asList(Produk.TYPE_LOCAL, Produk.TYPE_IMPORT));
		List<UnitPengukuran> unitPengukuranList = UnitPengukuran.findAllActive();
		List<Kurs> kursList = Kurs.findAll();
		Kurs kur = kursList.stream().filter(i->"IDR".equals(i.nama_kurs)).findAny().orElse(null);
		List<String> atributHargaNamaList = KomoditasAtributHarga.findNamaNotOngkirByKomoditas(komoditas.id);
		String atributHargaJson = new Gson().toJson(atributHargaNamaList);
		List<KomoditasAtributHarga> atributHargaList = KomoditasAtributHarga.findHargaNonOngkirByKomoditas(komoditas.id);

		ProdukHarga produkHarga = new ProdukHarga();
		List<PerpanjanganStaging> listPH = PerpanjanganStaging.find("class_name=? and perpanjangan_id=?",ProdukHarga.class.getName(),perp_id).fetch();
		PerpanjanganStaging saveProdukHarga = new PerpanjanganStaging();
		if (listPH==null || listPH.isEmpty()){
            produkHarga = ProdukHarga.getByProductId(id);
			saveProdukHarga.perpanjangan_id = perpanjangan.id;
			saveProdukHarga.class_name = ProdukHarga.class.getName();
			saveProdukHarga.object_json = gson.toJson(produkHarga);
			saveProdukHarga.isList = true;
			saveProdukHarga.save();
        }else {
            for (int i = 0; i < listPH.size(); i++) {
                ProdukHarga ph = gson.fromJson(listPH.get(i).object_json, ProdukHarga.class);
                if (ph!=null && ph.produk_id.equals(id)){
                    produkHarga=ph;
                }else{
					continue;
				}
            }
            if (produkHarga==null){
				produkHarga = ProdukHarga.getByProductId(id);
				saveProdukHarga.perpanjangan_id = perpanjangan.id;
				saveProdukHarga.class_name = ProdukHarga.class.getName();
				saveProdukHarga.object_json = gson.toJson(produkHarga);
				saveProdukHarga.isList = true;
				saveProdukHarga.save();
			}
        }
		List<ProvKabJson> provKab =  WilayahRepository.instance.getAllWilayah();
		String checkedLocationsJson = "{}";
		if(!komoditas.isNational()){
			checkedLocationsJson = produkHarga.checked_locations_json;
		}
		LinkedTreeMap checkedLocationMap = (new Gson()).fromJson(checkedLocationsJson, LinkedTreeMap.class);
		List<String> checkedLocationList = new ArrayList<String>();
		Map<Long, String> checkedLocations = new HashMap<Long, String>();
		Long[] wilJualId = new Long[0];
		List<Long> wilJualIds = new ArrayList<>();
		if(komoditas.isRegency()) {
			checkedLocationList = new ArrayList<String>(((Map)(checkedLocationMap.get("regency"))).keySet());
			List<Kabupaten> kabupatenList = Kabupaten.findByIds(checkedLocationList);
			checkedLocations = kabupatenList.stream().collect(Collectors.toMap(Kabupaten::getId, Kabupaten::getNama));
			List<ProdukWilayahJualKabupaten> wilJual = ProdukWilayahJualKabupaten.find("produk_id = ?", id).fetch();
			wilJualIds = new ArrayList<>();
			for (ProdukWilayahJualKabupaten p : wilJual) {
				wilJualIds.add(p.id);
			}
			wilJualId = wilJualIds.toArray(new Long[wilJualIds.size()]);
		}
		else if(komoditas.isProvince()) {
			checkedLocationList = new ArrayList<String>(((Map)(checkedLocationMap.get("province"))).keySet());
			List<Provinsi> provinsiList = Provinsi.findByIds(checkedLocationList);
			checkedLocations = provinsiList.stream().collect(Collectors.toMap(Provinsi::getId, Provinsi::getNama));
			List<ProdukWilayahJualProvinsi> wilJual = ProdukWilayahJualProvinsi.find("produk_id = ?", id).fetch();
			wilJualIds = new ArrayList<>();
			for (ProdukWilayahJualProvinsi p : wilJual) {
				wilJualIds.add(p.id);
			}
			wilJualId = wilJualIds.toArray(new Long[wilJualIds.size()]);
		}

		List<ProdukHarga> listProdukHarga = new ArrayList<>();
		Kurs k = Kurs.findById(produkHarga.kurs_id);
		produkHarga.nama_kurs=k.nama_kurs;
		if (produkHarga.tanggal_dibuat == null) {
			produkHarga.tanggal_dibuat = produkHarga.created_date;
		}
		//List<ProdukHarga> listAllPH = new ArrayList<>() ;
		listProdukHarga.add(produkHarga);
		Map<String, List<ProdukHarga>> groupHarga = listProdukHarga.stream()
				.collect(
						Collectors.groupingBy(
								x -> x.getStandarDateString()
						)
				);
		Map<String, List<ProdukHarga>> groupHarga2 = new HashMap<>();
		groupHarga2.put(produkHarga.tanggal_dibuat.toString(),listProdukHarga);
		NavigableMap<String, List<ProdukHarga>> riwayatProdukHarga = new TreeMap<>(groupHarga).descendingMap();
		Map<Long, List<RiwayatHargaProdukJson>> groupRiwayat = komoditas.isNational() ? null : listProdukHarga.stream()
				.map(x -> x.riwayatHarga())
				.flatMap(List::stream)
				.collect(
						Collectors.groupingBy(
								x -> komoditas.isProvince() ? x.provinsiId : x.kabupatenId
						)
				);
		NavigableMap<Long, List<RiwayatHargaProdukJson>> riwayatProdukHargaPerLokasi = groupRiwayat==null ? null : new TreeMap<>(groupRiwayat).descendingMap();
		List<ProdukAtributValue> produkAtributValues = new ArrayList<>();
		List<PerpanjanganStaging> listPav = PerpanjanganStaging.find("class_name=? and perpanjangan_id=?",ProdukAtributValue.class.getName(), perp_id ).fetch();
		PerpanjanganStaging savePav = new PerpanjanganStaging();
		if (listPav.size()==0 || listPav.isEmpty()){
			produkAtributValues = ProdukAtributValue.findByProdukId(id);
			savePav.perpanjangan_id = perpanjangan.id;
			savePav.class_name = ProdukAtributValue.class.getName();
			savePav.object_json = gson.toJson(produkAtributValues);
			savePav.isList = true;
			savePav.save();
		}else {
			for (int i = 0; i < listPav.size(); i++) {
				List<ProdukAtributValue> produkAtributValue = gson.fromJson(listPav.get(i).object_json, new TypeToken<ArrayList<ProdukAtributValue>>(){}.getType());
				if (produkAtributValue.size()!=0 && produkAtributValue.get(0).produk_id.equals(id)){
					produkAtributValues=produkAtributValue;
				}else {
					continue;
				}
			}
			if (produkAtributValues==null){
				produkAtributValues = ProdukAtributValue.findByProdukId(id);
				savePav.perpanjangan_id = perpanjangan.id;
				savePav.class_name = ProdukAtributValue.class.getName();
				savePav.object_json = gson.toJson(produkAtributValues);
				savePav.isList = true;
				savePav.save();
			}
		}
		//List<ProdukAtributValue> produkAtributValues = ProdukAtributValue.findByProdukId(id);
		Map<String, String> attributeMap = produkAtributValues.stream()
				.collect(Collectors.toMap(ProdukAtributValue::getCategoriAttrId, a -> a.atribut_value));
		String attrMapJson = CommonUtil.toJson(attributeMap);
		produk.withPictures();
		produk.withDocuments(true);
		produk.withProvider();
		produk.withCommodity();
		produk.withPenawaran();
		produk.withSpecifications();
		render("prakatalog/PerpanjangPenawaranCtr/editProduk.html",produk,perpanjangan,penawaran_id,komoditas,user,manufakturList,jenisProduk,unitPengukuranList, kursList, kur, atributHargaList,checkedLocations,produkHarga,listProdukHarga,riwayatProdukHarga,riwayatProdukHargaPerLokasi,kategoriList,attrMapJson, wilJualId, wilJualIds, atributHargaJson,provKab);
	}


	public  void submitEditProduk(Produk produk, ProductForm model, Perpanjangan perpanjangan){
		List<PerpanjanganStaging> listProduk = PerpanjanganStaging.find("class_name=? and perpanjangan_id=?",new Produk().getClass().getName() ,perpanjangan.id).fetch();
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX")
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        PerpanjanganStaging ps = new PerpanjanganStaging();
		for (int i = 0; i < listProduk.size(); i++) {
			String str_obj = listProduk.get(i).object_json;
			Produk prd = gson.fromJson(str_obj,Produk.class);
			if (produk.id.equals(prd.id)){
				ps = listProduk.get(i);
			}
		}

		Produk productModel = (model.isCreatedMode() || produk.id == null) ? new Produk() : produk;
		productModel.setData(productModel, model, produk.jumlah_stok_form, produk.jumlah_stok_inden_form);
		Komoditas komoditas = Komoditas.findById(produk.komoditas_id);

		Map<Long,BigDecimal> mapProductPrice = model.createMapFromProductPriceForm();
		if(komoditas.isLelang()){
			productModel.status = Produk.STATUS_TERIMA;
			productModel.minta_disetujui = false;
			productModel.setuju_tolak = Produk.SETUJU;

		}else{
			productModel.status = Produk.STATUS_MENUNGGU_PERSETUJUAN;
			productModel.minta_disetujui = true;
		}
		AktifUser aktifUser = getAktifUser();
		Penyedia penyedia = ProviderRepository.findByUserId(aktifUser.user_id);
		productModel.penyedia_id = penyedia.id;

		productModel.margin_harga = new Double(0);
		if(komoditas.isNational() && produk.harga_utama != model.getMainPrice()){
			productModel.harga_tanggal = model.parseKursDate();
			productModel.harga_utama = model.getMainPrice();
		}
		if(!komoditas.isRegency() && komoditas.isNeedDeliveryCost() && !model.ongkirJson.isEmpty()){
			productModel.ongkir = CommonUtil.toJson(getOngkirProdukJson(model,komoditas.id));
		}
		if(komoditas.isRegency() && komoditas.isNeedDeliveryCost() && !model.hargaJson.isEmpty()){
			productModel.ongkir = model.getOngkirJsonArrayFromPrice();
		}

		// Save data produk
		ps.object_json=gson.toJson(productModel);
		ps.save();

		//save produk harga
		List<KomoditasAtributHarga> hargaNonOngkir = KomoditasAtributHarga.findHargaNonOngkirByKomoditas(komoditas.id);
		String jsonProdukHarga="";
		ProdukHarga ph = new ProdukHarga();
		for(KomoditasAtributHarga harga : hargaNonOngkir){
			String hargaJsonResult;

			if(komoditas.isNational()){
				HargaProdukJson hargaProdukJson = new HargaProdukJson();
				hargaProdukJson.harga = mapProductPrice.get(harga.id);
				hargaJsonResult = CommonUtil.toJson(hargaProdukJson);
			}else{
				List<HargaProdukJson> hargaProdukJson = getHargaProdukJson(model,harga, komoditas.isNeedDeliveryCost());
				if(harga.apakah_harga_utama){
					saveWilayahJual(model, hargaProdukJson, productModel);
				}

				hargaJsonResult = CommonUtil.toJson(hargaProdukJson);
			}
			ph = new ProdukHarga(productModel.id,
					harga.id,
					productModel.harga_kurs_id,
					hargaJsonResult,
					model.parseKursDate(),
					model.getXlsJson(),
					model.checkedLocations);
			jsonProdukHarga = gson.toJson(ph);
		}

		List<PerpanjanganStaging> allProdukHarga = PerpanjanganStaging.find("class_name=? and perpanjangan_id=?",new ProdukHarga().getClass().getName() ,perpanjangan.id).fetch();
		for (int i = 0; i < allProdukHarga.size(); i++) {
			String str_obj = allProdukHarga.get(i).object_json;
			ProdukHarga produkHarga = gson.fromJson(str_obj,ProdukHarga.class);
			if (produkHarga.produk_id.equals(produk.id)){
				produkHarga= ph;
				allProdukHarga.get(i).object_json=gson.toJson(produkHarga);
				allProdukHarga.get(i).save();
			}
		}

		//Save spesifikasi produk
        Map<String, String> infoMap = getInformationFromJson(model.infoJson);
		List<ProdukAtributValue> listPav = new ArrayList<>();
		if (!infoMap.isEmpty()) {
			if (!model.isCreatedMode()) {
				ProdukAtributValue.delete("produk_id = ?", productModel.id);
			}
			for (Map.Entry<String, String> entry : infoMap.entrySet()) {
				listPav.add(new ProdukAtributValue(productModel.id, entry.getValue(), Long.valueOf(entry.getKey())));
			}
		}
		String json = gson.toJson(listPav, new TypeToken<ArrayList<ProdukAtributValue>>() {}.getType());
		List<PerpanjanganStaging> pav =PerpanjanganStaging.find("class_name=? and perpanjangan_id=?",ProdukAtributValue.class.getName(),perpanjangan.id).fetch();
		for (int i = 0; i < pav.size(); i++) {
			List<ProdukAtributValue> produkAtributValue = gson.fromJson(pav.get(i).object_json, new TypeToken<ArrayList<ProdukAtributValue>>(){}.getType());
			if (produkAtributValue.get(0).produk_id.equals(productModel.id)){
				pav.get(i).object_json = gson.toJson(json);
			}
		}
		//end of collect info spek produk

		//save lampiran
		if (!ArrayUtils.isEmpty(model.file_id)) {
			for (int id : model.file_id) {
				ProdukLampiran.updateProdukLampiran(id, productModel.id);
			}
		}

		//save gambar
		if (!ArrayUtils.isEmpty(model.pict_id)) {
			if(null != productModel.id && model.pict_id.length > 0 ){
				ProdukGambar pgs = ProdukGambar.findById(model.pict_id[0]);
				productModel.produk_gambar_file_name = pgs.file_name;
				productModel.produk_gambar_file_sub_location = pgs.file_sub_location;
			}
			for (int id : model.pict_id) {
				ProdukGambar.updateProdukGambar(id, productModel.id);
			}
		}

		flash.success(Messages.get("notif.success.submit"));
		goToList(perpanjangan.id);

	}

	public static List<OngkirProdukJson> getOngkirProdukJson(ProductForm model, long komoditas_id){
		LogUtil.d(TAG, "generate price from json");
		LogUtil.d(TAG, model.hargaJson);
		LogUtil.d(TAG, model.ongkirJson);

		List<OngkirProdukJson> ongkirJsonList = new ArrayList<>();

		if (!TextUtils.isEmpty(model.ongkirJson) || !TextUtils.isEmpty(model.ongkirJson)) {

			JsonArray ongkirJsonArray = model.getOngkirJsonArray();
			if (ongkirJsonArray != null && ongkirJsonArray.size() > 0) {

				for (int i = 0; i < ongkirJsonArray.size(); i++) {
					JsonObject obj = ongkirJsonArray.get(i).getAsJsonObject();
					OngkirProdukJson ongkirProdukJson = new OngkirProdukJson();

					String namaProvinsiFromJson = obj.get("Provinsi").getAsString();
					Logger.info(namaProvinsiFromJson+" nama provinsi");
					Provinsi provinsi = Provinsi.findByName(namaProvinsiFromJson);
					ongkirProdukJson.provinsiId = provinsi.id;

					if(obj.get("Kabupaten") != null){
						String namaKabupatenFromJson = obj.get("Kabupaten").getAsString();
						Kabupaten kabupaten = Kabupaten.findByName(namaKabupatenFromJson);
						ongkirProdukJson.kabupatenId = kabupaten.id;
					}

					ongkirProdukJson.ongkir = obj.get("Referensi Ongkos Kirim").getAsBigDecimal();
					ongkirJsonList.add(ongkirProdukJson);
				}
			}
		}
		return ongkirJsonList;
	}

	public static List<HargaProdukJson> getHargaProdukJson(ProductForm model, KomoditasAtributHarga harga, boolean isNeedDeliveryCost){
		LogUtil.d(TAG, "generate price from json");
		LogUtil.d(TAG, model.hargaJson);
		LogUtil.d(TAG, model.ongkirJson);

		List<HargaProdukJson> hargaProdukJsonList = new ArrayList<>();

		if (!TextUtils.isEmpty(model.hargaJson)) {

			JsonArray hargaJsonArray = model.getHargaJsonArray();
			if (hargaJsonArray != null && hargaJsonArray.size() > 0) {

				for (int i = 0; i < hargaJsonArray.size(); i++) {
					JsonObject obj = hargaJsonArray.get(i).getAsJsonObject();
					HargaProdukJson hargaProdukJson = new HargaProdukJson();

					String namaProvinsiFromJson = obj.get("Provinsi").getAsString();
					Provinsi provinsi = Provinsi.findByName(namaProvinsiFromJson);
					hargaProdukJson.provinsiId = provinsi.id;

					if(model.kelas_harga.equalsIgnoreCase("kabupaten")){

						String namaKabupatenFromJson = obj.get("Kabupaten").getAsString();
						Kabupaten kabupaten = Kabupaten.findByName(namaKabupatenFromJson);
						hargaProdukJson.kabupatenId = kabupaten.id;

					}
					hargaProdukJson.harga = obj.get(harga.label_harga).getAsBigDecimal();
					hargaProdukJsonList.add(hargaProdukJson);

				}

			}

		}
		return hargaProdukJsonList;
	}

	private static void saveWilayahJual(ProductForm model, List<HargaProdukJson> hargaProdukJson, Produk produk){
		LogUtil.d(TAG, "saving market share: " + model.kelas_harga);
		Date kursDate = model.parseKursDate();

		if (!model.isCreatedMode() && hargaProdukJson != null && !hargaProdukJson.isEmpty()) {
			LogUtil.d(TAG, "delete all price coverage");
			LogUtil.d(TAG, "total in json: " + hargaProdukJson.size());
			int ok = ProdukWilayahJualKabupaten.deleteByProdukId(produk.id);
			ProdukWilayahJualProvinsi.delete("produk_id = ?", produk.id);
		}

		if (!hargaProdukJson.isEmpty()) {
			if (model.kelas_harga.equalsIgnoreCase(Komoditas.KABUPATEN)) {
				LogUtil.d(TAG, "regency market");
				for (HargaProdukJson harga : hargaProdukJson) {
					ProdukWilayahJualKabupaten wKabupaten = new ProdukWilayahJualKabupaten();
					wKabupaten.kabupaten_id = harga.kabupatenId;
					wKabupaten.produk_id = produk.id;
					wKabupaten.harga_utama = harga.harga;
					wKabupaten.harga_ongkir = new BigDecimal(0);
					wKabupaten.harga_tanggal = kursDate;
					wKabupaten.harga_kurs_id = produk.harga_kurs_id;
					wKabupaten.margin_harga = new Double(0);
					wKabupaten.active = true;
					wKabupaten.save();
				}
			} else if (model.kelas_harga.equalsIgnoreCase(Komoditas.PROVINSI)) {
				LogUtil.d(TAG, "province market");
				for (HargaProdukJson harga : hargaProdukJson) {
					ProdukWilayahJualProvinsi wProvinsi = new ProdukWilayahJualProvinsi();
					wProvinsi.provinsi_id = harga.provinsiId;
					wProvinsi.produk_id = produk.id;
					wProvinsi.harga_utama = harga.harga;
					wProvinsi.harga_ongkir = new BigDecimal(0);
					wProvinsi.harga_tanggal = kursDate;
					wProvinsi.harga_kurs_id = produk.harga_kurs_id;
					wProvinsi.margin_harga = new Double(0);
					wProvinsi.active = true;
					wProvinsi.save();
				}
			}
		}
	}

	private static Map<String, String> getInformationFromJson(String json) {
		if (TextUtils.isEmpty(json)) {
			json = "{}";
		}
		Type type = new TypeToken<Map<String, String>>(){}.getType();
		Map<String, String> results = new HashMap<>();
		results.putAll(CommonUtil.fromJson(json, type));
		return results;
	}

	public static void beritaAcara(Long id) {
		Perpanjangan perpanjangan = Perpanjangan.findById(id);
		Long penawaran_id = perpanjangan.penawaran_id;
		List<DokumenTemplate> baList = DokumenTemplate.findAllBAActive();
		List<BeritaAcara> bas = BeritaAcara.findByPenawaranId(penawaran_id);
		render("/prakatalog/PerpanjangPenawaranCtr/beritaAcaraPerpanjangan.html",penawaran_id, bas,baList);
	}

}
