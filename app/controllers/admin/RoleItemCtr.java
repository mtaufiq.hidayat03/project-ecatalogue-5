package controllers.admin;

import controllers.security.AllowAccess;
import models.common.AktifUser;
import models.secman.Acl;
import models.user.RoleGrup;
import models.user.RoleItem;
import play.i18n.Messages;
import play.mvc.Controller;
import controllers.BaseController;
import java.sql.Timestamp;
import java.util.List;

public class RoleItemCtr extends BaseController {

    //@AllowAccess({Acl.COM_MASTER_ROLE_ITEM_INDEX})
    public static void index(){
        render();
    }

    //@AllowAccess({Acl.COM_MASTER_ROLE_ITEM_ADD})
    public static void create(){
        List<RoleGrup> rg = RoleGrup.findAllActive();
        RoleItem pr = new RoleItem();
        pr.active = true;
        render(pr,rg);
    }

    //@AllowAccess({Acl.COM_MASTER_ROLE_ITEM_ADD})
    public static void save(RoleItem pr){
        params.flash();
        try {
            if(pr.id != null){
                RoleItem prObj = RoleItem.findById(pr.id);
                prObj.nama_role = pr.nama_role;
                prObj.role_grup_id = pr.role_grup_id;
                prObj.deskripsi = pr.deskripsi;
                prObj.active = pr.active;
                prObj.save();
                flash.success(Messages.get("notif.success.submit"));
            }else{
                pr.save();
                flash.success(Messages.get("notif.success.submit"));
            }
        }catch (Exception e){
            flash.error(Messages.get("notif.failed.submit"));
        }
        index();
    }

    //@AllowAccess({Acl.COM_MASTER_ROLE_ITEM_ADD})
    public static void edit(Long id){
        List<RoleGrup> rg = RoleGrup.findAllActive();
        RoleItem pr = RoleItem.findById(id);
        render("admin/RoleItemCtr/create.html",pr,rg);
    }

    public static void detail(){
        render();
    }
    //@AllowAccess({Acl.COM_MASTER_ROLE_ITEM_DELETE})
    public static void delete(Long id){
        List<RoleGrup> rg = RoleGrup.findAllActive();
        RoleItem pr = RoleItem.findById(id);
        pr.active= false;//softdelete
        render("admin/RoleItemCtr/create.html",pr,rg);
    }

    //@AllowAccess({Acl.COM_MASTER_ROLE_ITEM_INDEX})
    public static void dataSrc(){
        renderJSON(RoleItem.dataSrc());
    }
}
