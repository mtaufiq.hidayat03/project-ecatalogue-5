DELIMITER $$
CREATE FUNCTION levenshtein( s1 VARCHAR(255), s2 VARCHAR(255) ) 
  RETURNS INT 
  DETERMINISTIC 
  BEGIN 
    DECLARE s1_len, s2_len, i, j, c, c_temp, cost INT; 
    DECLARE s1_char CHAR; 
    -- max strlen=255 
    DECLARE cv0, cv1 VARBINARY(256); 
    SET s1_len = CHAR_LENGTH(s1), s2_len = CHAR_LENGTH(s2), cv1 = 0x00, j = 1, i = 1, c = 0; 
    IF s1 = s2 THEN 
      RETURN 0; 
    ELSEIF s1_len = 0 THEN 
      RETURN s2_len; 
    ELSEIF s2_len = 0 THEN 
      RETURN s1_len; 
    ELSE 
      WHILE j <= s2_len DO 
        SET cv1 = CONCAT(cv1, UNHEX(HEX(j))), j = j + 1; 
      END WHILE; 
      WHILE i <= s1_len DO 
        SET s1_char = SUBSTRING(s1, i, 1), c = i, cv0 = UNHEX(HEX(i)), j = 1; 
        WHILE j <= s2_len DO 
          SET c = c + 1; 
          IF s1_char = SUBSTRING(s2, j, 1) THEN  
            SET cost = 0; ELSE SET cost = 1; 
          END IF; 
          SET c_temp = CONV(HEX(SUBSTRING(cv1, j, 1)), 16, 10) + cost; 
          IF c > c_temp THEN SET c = c_temp; END IF; 
            SET c_temp = CONV(HEX(SUBSTRING(cv1, j+1, 1)), 16, 10) + 1; 
            IF c > c_temp THEN  
              SET c = c_temp;  
            END IF; 
            SET cv0 = CONCAT(cv0, UNHEX(HEX(c))), j = j + 1; 
        END WHILE; 
        SET cv1 = cv0, i = i + 1; 
      END WHILE; 
    END IF; 
    RETURN c; 
  END$$


CREATE FUNCTION levenshtein_ratio( s1 VARCHAR(255), s2 VARCHAR(255) ) 
  RETURNS INT 
  DETERMINISTIC 
  BEGIN 
    DECLARE s1_len, s2_len, max_len INT; 
    SET s1_len = LENGTH(s1), s2_len = LENGTH(s2); 
    IF s1_len > s2_len THEN  
      SET max_len = s1_len;  
    ELSE  
      SET max_len = s2_len;  
    END IF; 
    RETURN ROUND((1 - LEVENSHTEIN(s1, s2) / max_len) * 100); 
  END$$

DELIMITER ;

select levenshtein('abc','aac');




select k.id, dk.id, k.nama_kabupaten, dk.nama
from provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
where k.dce_id is null
and   k.active=1
-- and dp.id=1
and ((
		k.nama_kabupaten like 'Kabupaten%'
		and dk.nama like '%(Kab.)')
	or
	(
		k.nama_kabupaten like 'Kota%'
		and dk.nama like '%(Kota)')
)
-- and (
--	replace(replace(k.nama_kabupaten,'Kabupaten ',''),' ','') like concat('%', replace( replace(dk.nama,'(Kab.)','') ,' ',''),'%')
-- OR
--	replace(replace(dk.nama,'(Kab.)',''),' ','') like concat('%', replace( replace(k.nama_kabupaten,'Kabupaten ','') ,' ',''),'%')
-- )
-- and levenshtein(replace(replace(replace(k.nama_kabupaten,'Kabupaten ',''),' ',''),'-','') ,  replace( replace( replace(dk.nama,'(Kab.)','') ,' ','') , '-','') ) < 3;
order by k.nama_kabupaten, dk.nama
;

update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and k.nama_kabupaten=dk.nama
;

update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and replace(k.nama_kabupaten,'Kabupaten ','')=replace(dk.nama,'(Kab.)','')
;

update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and replace(k.nama_kabupaten,'Kota ','')=replace(dk.nama,'(Kota)','')
;

update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and replace(replace(replace(k.nama_kabupaten,'Kabupaten ',''),' ',''),'-','') = replace( replace( replace(dk.nama,'(Kab.)','') ,' ','') , '-','')
;

update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and replace(replace(replace(k.nama_kabupaten,'Kota ',''),' ',''),'-','') = replace( replace( replace(dk.nama,'(Kota)','') ,' ','') , '-','')
;


update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and levenshtein(replace(replace(replace(k.nama_kabupaten,'Kabupaten ',''),' ',''),'-','') ,  replace( replace( replace(dk.nama,'(Kab.)','') ,' ','') , '-','') ) < 3;
;


update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and levenshtein(replace(replace(replace(k.nama_kabupaten,'Kota ',''),' ',''),'-','') ,  replace( replace( replace(dk.nama,'(Kota)','') ,' ','') , '-','') ) < 3;
;

update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and replace(replace(replace(replace(k.nama_kabupaten,'Kota ',''),' ',''),'-',''),'Kepulauan','') = 
replace(replace( replace( replace(dk.nama,'(Kota)','') ,' ','') , '-',''),'Kepulauan','')
;

update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and replace(replace(replace(replace(k.nama_kabupaten,'Kabupaten ',''),' ',''),'-',''),'Kepulauan','') = 
replace(replace( replace( replace(dk.nama,'(Kab.)','') ,' ','') , '-',''),'Kepulauan','')
;


update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and replace(replace(replace(replace(k.nama_kabupaten,'Kabupaten ',''),' ',''),'-',''),'Pulau','') = 
replace(replace( replace( replace(dk.nama,'(Kab.)','') ,' ','') , '-',''),'Pulau','')
;


update
provinsi p
join dce_provinsi dp on p.dce_id=dp.id
join kabupaten k on k.provinsi_id=p.id
join dce_kabupaten dk on dk.dce_provinsi_id=dp.id
set k.dce_id=dk.id
where k.dce_id is null
and   k.active=1
and replace(replace(replace(replace(k.nama_kabupaten,'Kota ',''),' ',''),'-',''),'Pulau','') = 
replace(replace( replace( replace(dk.nama,'(Kota)','') ,' ','') , '-',''),'Pulau','')
;