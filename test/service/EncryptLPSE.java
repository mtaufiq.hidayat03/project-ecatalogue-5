package service;

import models.lpse.UserFromLpse;
import models.util.Encryption;
import org.junit.Test;
import play.Logger;
import play.test.UnitTest;

import java.util.Base64;
import java.util.Date;

public class EncryptLPSE extends UnitTest {

        //@Test
    public void testEncrypt(){
            String username = "pokja500a";
            String nama = "test nama";
            String role = "REKANAN";
            Long ppsid = 119L;
            boolean isLatihan = true;
            Date tanggalLogin = new Date();
            UserFromLpse lpse = new UserFromLpse(username,nama,role,ppsid,isLatihan,tanggalLogin);
            String encrypt = username+"|"+nama+"|"+role+"|"+ppsid+"|"+isLatihan+"|"+tanggalLogin;
            Logger.info(encrypt);
            Logger.info(Encryption.encryptInaproc(encrypt));
            // xnMuQ7Q1gQkiU6mJUIgryYklpKIerkg04zD7_LS-20GOK-uirnDh_HdUXMSDOGXiYTzY-nu74ll4-Fji6a6wLfn8mJOVaZKv
            // pokja500a-test nama-REKANAN-119-true-Tue Jun 25 18:09:48 WIB 2019
            //    http://localhost:2209/lpse/xnMuQ7Q1gQkiU6mJUIgryYklpKIerkg04zD7_LS-20GOK-uirnDh_HdUXMSDOGXiYTzY-nu74ll4-Fji6a6wLfn8mJOVaZKv
            // http://localhost:2209/lpse/9bw459-8sBT0AiBM1LMLU9Zqv4MuTQ01OiB1_VCLF6zm0Qtv_AElcYTRw_wv1RAP
    }

    @Test
    public void testEncodeUrl(){
        String text = "wGr5tQsFe/FfSjC3+20S7g==";

        String encodedUrl = Base64.getUrlEncoder().encodeToString(text.getBytes());
        Logger.info(encodedUrl);
//            do {
//                String textE = Encryption.encodeUrlSafe(text);
//                textD = Encryption.decodeUrlSafe(textE);
//                Logger.info(textE);
//                Logger.info(textD);
//                Logger.info(text.equals(textD)+"");
//            }while (!text.equals(textD));

    }
}
