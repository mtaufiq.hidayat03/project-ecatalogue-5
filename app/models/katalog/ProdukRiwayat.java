package models.katalog;

import controllers.katalog.form.ProductForm;
import models.common.AktifUser;
import models.jcommon.util.CommonUtil;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by dadang on 8/28/17.
 */

@Table(name = "produk_riwayat")
public class ProdukRiwayat extends BaseTable {

	public static final String HISTORY_CREATED = "produk_riwayat_ditambahkan";
	public static final String UPDATE_PRODUCT = "produk_riwayat_update_informasi";
	public static final String ASKING_FOR_APPROVAL = "produk_riwayat_meminta_persetujuan_tayang";

	@Id
	public Long id;
	public Long produk_id;
	public String aksi;
	public String deskripsi;
	public String data;

	public Timestamp created_date ;
	public Integer created_by;

	@Transient
	public String oleh;

	public static List<ProdukRiwayat> findByProdukId(long produk_id){
		String query = "Select pr.id, pr.produk_id, pr.aksi, pr.deskripsi, pr.data, pr.created_by, " +
				"pr.created_date, u.nama_lengkap as oleh " +
				"from produk_riwayat pr " +
				"join (select id, nama_lengkap from `user` union select '-99' as id, 'Datafeed' as nama_lengkap) u on pr.created_by = u.id " +
				"where pr.produk_id = ? " +
				"order by created_date DESC";

		return Query.find(query, ProdukRiwayat.class, produk_id).fetch();
	}

	public ProdukRiwayat addForApproval(Produk produk) {
		this.aksi = ASKING_FOR_APPROVAL;
		setProduct(produk);
		return this;
	}

	public ProdukRiwayat addProduct(Produk produk) {
		this.aksi = HISTORY_CREATED;
		setProduct(produk);
		return this;
	}

	public ProdukRiwayat editProduct(Produk produk) {
		this.aksi = UPDATE_PRODUCT;
		setProduct(produk);
		return this;
	}

	public ProdukRiwayat setDataForm(ProductForm model) {
		this.deskripsi = model.reason;
		return this;
	}

	public void setProduct(Produk model) {
		this.produk_id = model.getId();
		this.data = CommonUtil.toJson(model);
	}

	public void setCreated() {
		this.created_date = new Timestamp(System.currentTimeMillis());
		this.created_by = AktifUser.getActiveUserId();
	}

	public void saveHistory() {
		setCreated();
		final long result = save();
		if (this.id == null) {
			this.id = result;
		}
	}

}
