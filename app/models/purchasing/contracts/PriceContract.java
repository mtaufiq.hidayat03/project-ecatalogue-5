package models.purchasing.contracts;

import ext.FormatUtils;

/**
 * @author HanusaCloud on 11/6/2017
 */
public interface PriceContract {

    Double getUnitPrice();

    Double getDeliveryCost();

    Double getTotalPrice();

    Double getConversionUnitPrice();

    Double getConversionDeliveryCost();

    Double getConversionTotalPrice();

    Double getBestPrice();

    Double getConversionBestPrice();

    String getCheckedUser();

    default String getFormattedUnitPrice() {
        return FormatUtils.formatCurrencyRupiah(getUnitPrice());
    }

    default String getFormattedTotalPrice() {
        return FormatUtils.formatCurrencyRupiah(getTotalPrice());
    }

    default String getFormattedDeliveryCost() {
        return FormatUtils.formatCurrencyRupiah(getDeliveryCost());
    }

    default String getFormattedConversionUnitPrice() {
        return FormatUtils.formatCurrencyRupiah(getConversionUnitPrice());
    }

    default String getFormattedConversionTotalPrice() {
        return FormatUtils.formatCurrencyRupiah(getConversionTotalPrice());
    }

    default String getFormattedConversionDeliveryCost() {
        return FormatUtils.formatCurrencyRupiah(getConversionDeliveryCost());
    }

    default String getFormattedBestPrice() {
        return FormatUtils.formatCurrencyRupiah(getBestPrice());
    }

    default String getFormattedConversionBestPrice() {
        return FormatUtils.formatCurrencyRupiah(getConversionBestPrice());
    }

}
