package models.elasticsearch.contract;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import java.io.File;

/**
 * @author HanusaCloud on 11/28/2017
 */
public interface ElasticsearchRequestContract {

    default RequestBody getRequestBody(String json) {
        return RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json);
    }

    default RequestBody getRequestBody(File file) {
        return RequestBody.create(MediaType.parse("application/json; charset=utf-8"), file);
    }

}
