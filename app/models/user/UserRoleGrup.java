package models.user;

import java.util.List;

import models.BaseKatalogTable;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;

@Table(name="user_role_grup")
public class UserRoleGrup extends BaseKatalogTable {

	@Id
	public Long id;

	@Required(message = "Nama Harus diisi")
	public String nama_grup;

	@Required
	public boolean akses_seluruh_komoditas;

	@Required
	public boolean akses_seluruh_paket;

	@Required
	public boolean batasi_pembelian;

//	@Required
	public Double nilai_batas_pembelian;

	@Required
	public boolean role_basic_default;

	public boolean active=true;

	public Long role_basic_id;

	@Transient
	public String nama_role_basic;

	public static final long PENYEDIA = 2;
	public static final long DISTRIBUTOR = 10;
	
	public static List<UserRoleGrup> findAllActive(){
		String query = "select urg.id, urg.nama_grup, urg.akses_seluruh_komoditas, "
				+ "urg.akses_seluruh_paket, urg.batasi_pembelian, urg.nilai_batas_pembelian, urg.role_basic_default, "
				+ "urg.role_basic_id, rb.nama_role_alias as nama_role_basic "
				+ "from user_role_grup urg join role_basic rb ON urg.role_basic_id = rb.id where urg.active = 1";
		
		return Query.find(query, UserRoleGrup.class).fetch();
	}

	public static void deleteById(long id){
		UserRoleGrup urg = UserRoleGrup.findById(id);
		urg.active = false;
		urg.save();
	}

	public void saveRole() {
		final int result = save();
		if (this.id == null) {
			this.id = (long) result;
		}
	}

	public boolean isAllowedToBeDeleted() {
		return User.getTotalUserByUserRoleGrupId(this.id) == 0
				;
//				&& UserRoleItem.getTotalItemByUserRoleGrupId(this.id) == 0
//				&& UserRoleKomoditas.getTotalCommodityByUserRoleGrupId(this.id) == 0
//				&& UserRolePaket.getTotalPackageByUserRoleGrupId(this.id) == 0;
	}

	public void softDelete() {
		this.active = false;
		setDeleted();
		save();
	}

}
