package utils;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import play.Logger;
import play.i18n.Lang;
import play.mvc.Http;
import play.mvc.Scope;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 10/12/2017
 */
public class GeneralSearchActionUtil {

    public static final String TAG = "GeneralSearchActionUtil";

    public static String getCurrentUrl() throws URISyntaxException {
        final String current = Http.Request.current().url;
        final Scope.Params p = Http.Request.current().params;
        List<NameValuePair> params = new ArrayList<>();
        try{
            params = URLEncodedUtils.parse(new URI(current), "UTF-8");
        }catch (Exception e){
            Logger.error("[GeneralSearchActionUtil.getCurrentUrl]", e.getMessage());
        }
        final boolean isGeneral = !current.contains("/katalog/produk");
        UrlUtil.Builder builder = new UrlUtil.Builder();
        builder.setUrl(!isGeneral ? "katalog.ProdukCtr.listProduk" : "katalog.SearchResultCtr.index");
        if (!isGeneral) {
            builder.setParam("komoditas_slug", p.get("komoditas_slug", String.class));
            builder.setParam("id", p.get("id", Long.class));
        }
        builder.setParam("language", Lang.get());
        if (!params.isEmpty()) {
            for (NameValuePair model : params) {
                if (!model.getName().equalsIgnoreCase("language")
                        || !model.getName().equalsIgnoreCase("q")) {
                    builder.setParam(model.getName(), model.getValue());
                }
            }
        }
        return builder.build();
    }

}
