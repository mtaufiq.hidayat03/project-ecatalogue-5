package controllers.admin.user;

/**
 * @author HanusaCloud on 12/1/2017
 */
public class UserSearch {

    public String keyword = "";
    public long role = 0;
    public long groupRole = 0;
    public int overrideGroupRole = -1;
    public int overrideCommodity = -1;
    public int overridePackage = -1;

}
