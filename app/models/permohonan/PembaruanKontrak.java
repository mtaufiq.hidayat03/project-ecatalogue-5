package models.permohonan;

import models.BaseKatalogTable;
import models.common.DokumenInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.penyedia.PenyediaKontrak;
import models.prakatalog.Penawaran;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;
import utils.DateTimeUtils;
import utils.LogUtil;

import javax.persistence.Transient;
import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Table(name = "permohonan_pembaruan_kontrak")
public class PembaruanKontrak extends BaseKatalogTable {
    @Id
    public Long id;
    public Long permohonan_pembaruan_id;
    public String no_kontrak;
    public String deskripsi;
    public String file_hash;
    public String nama_file;
    public Long blb_id_content;
    public Date tgl_masa_berlaku_mulai;
    public Date tgl_masa_berlaku_selesai;
    public Long penawaran_id;
    public Long komoditas_id;
    public int active;

    public Long saveAsPenyediaKontrak(Long penawaran_id){
        Penawaran penawaran = Penawaran.findById(penawaran_id);
        if(null == penawaran.penyedia_id){
            PermohonanPembaruan pmb = PermohonanPembaruan.findById(permohonan_pembaruan_id);
            penawaran.penyedia_id = pmb.penyedia_id;
        }
        PenyediaKontrak pk = new PenyediaKontrak();
        pk.apakah_berlaku = true;
        pk.penyedia_id = penawaran.penyedia_id;
        pk.no_kontrak = no_kontrak;
        pk.file_name = nama_file;
        pk.original_file_name = nama_file;
        pk.file_size = BlobTable.findByBlobId(blb_id_content).blb_ukuran;
        pk.deskripsi = deskripsi;
        pk.posisi_file = 0;
        pk.tgl_masa_berlaku_mulai = tgl_masa_berlaku_mulai;
        pk.tgl_masa_berlaku_selesai = tgl_masa_berlaku_selesai;
        pk.active = true;
        pk.komoditas_id = penawaran.komoditas_id;
        pk.file_hash = file_hash;
        pk.dok_id_attachment = blb_id_content;
        pk.penawaran_id = penawaran_id;
        int id = pk.save();
        return Long.valueOf(id);
    }

    @Transient
    public String nama_lengkap;

    public static List<PembaruanKontrak> findActiveByPermohonan(long permohonanId){
        String query = "select kontrak.*, u.nama_lengkap " +
                "from permohonan_pembaruan_kontrak kontrak " +
                "left join user u on kontrak.created_by = u.id and u.active = 1 " +
                "where kontrak.active = 1 and kontrak.permohonan_pembaruan_id = ? ";
        return Query.find(query, PembaruanKontrak.class, permohonanId).fetch();
    }

    public static List<PembaruanKontrak> findActiveByPenawaran(long permohonanId, long penawaranId){
        String query = "select kontrak.*, u.nama_lengkap " +
                "from permohonan_pembaruan_kontrak kontrak " +
                "left join user u on kontrak.created_by = u.id and u.active = 1 " +
                "where kontrak.active = 1 and kontrak.permohonan_pembaruan_id = ? and kontrak.penawaran_id = ? ";
        return Query.find(query, PembaruanKontrak.class, permohonanId, penawaranId).fetch();
    }

    public boolean kontrakSubmit(PembaruanKontrak kontrak, File file){
        try {
            BlobTable blob = null;
            if(file != null) {
//                if(file.length() <= 5242880) {
                blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, this.blb_id_content, PembaruanKontrak.class.getSimpleName());
                this.nama_file = blob.blb_nama_file;
                this.file_hash = blob.blb_hash;
                this.blb_id_content = blob.id;
//                }else{
//                    return false;
//                }
            }
            this.permohonan_pembaruan_id = kontrak.permohonan_pembaruan_id;
            this.no_kontrak = kontrak.no_kontrak;
            this.deskripsi = kontrak.deskripsi;
            this.tgl_masa_berlaku_mulai = kontrak.tgl_masa_berlaku_mulai;
            this.tgl_masa_berlaku_selesai = kontrak.tgl_masa_berlaku_selesai;
            this.penawaran_id = kontrak.penawaran_id;
            this.active = kontrak.active;

            Long idr = new Long(this.saveKontrakAndRiwayat());
            if(null != blob){
                blob.blb_id_content = idr;
                blob.save();
            }

            return true;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            LogUtil.d("***************** error ***************", e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public int saveKontrakAndRiwayat(){
        PembaruanRiwayat rw = new PembaruanRiwayat();
        int ret = this.save();
        if(null == this.id){
            rw.addPembaruanKontrak(this.permohonan_pembaruan_id,this.deskripsi);
        }else{
            rw.editPembaruanKontrak(this.permohonan_pembaruan_id,this.deskripsi);
        }
        return ret;
    }

    public String getFileUrl() {
        String url = "";

        if(blb_id_content != null && blb_id_content.toString() != ""){
            BlobTable blobTable = BlobTable.findByBlobId(blb_id_content);
            if(blobTable != null){
                DokumenInfo dokumenInfo = DokumenInfo.findDokumenKontrak(blobTable);
                url = dokumenInfo.download_url;
            }
        }

        return url;
    }

    public String getContractDate(){
        String startDate = DateTimeUtils.parseToReadableDate(tgl_masa_berlaku_mulai);
        String endDate = DateTimeUtils.parseToReadableDate(tgl_masa_berlaku_selesai);
        String contractDate = "";

        if(startDate != null && !startDate.equalsIgnoreCase("")){
            contractDate = startDate;
        }else{
            contractDate = "N/A";
        }

        contractDate += " " + Messages.get("sampai_dengan.label") + " ";

        if(endDate != null && !endDate.equalsIgnoreCase("")){
            contractDate += endDate;
        }else{
            contractDate += "N/A";
        }

        return contractDate;
    }

    public String getCreatedDate(){
        String getDate = getReadableDate(created_date);
        return getDate;
    }

    public String getModifiedDate(){
        String getDate = getReadableDate(modified_date);
        return getDate;
    }

    public String getReadableDate(Timestamp date) {
        String requestDate = DateTimeUtils.parseToReadableDate(date);
        if(requestDate != null && !requestDate.equalsIgnoreCase("")){
            return requestDate;
        }else{
            return "N/A";
        }
    }

    public static int removeDocument(Long dispoId){
        String query = "update permohonan_pembaruan_kontrak set file_hash = null, nama_file = null, blb_id_content = null where id = ?";
        return Query.update(query, dispoId);
    }

    public static List<PembaruanKontrak> getByPermohonanId(Long perm_id){
        List<PembaruanKontrak> list = new ArrayList<>();
        String sql = "SELECT * FROM permohonan_pembaruan_kontrak WHERE permohonan_pembaruan_id=?";
        list = Query.find(sql,PembaruanKontrak.class, perm_id).fetch();
        return list;
    }
}
