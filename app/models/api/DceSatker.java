package models.api;

import models.masterdata.Satker;
import play.Logger;

public class DceSatker {
	public Long id;
	public String idSatker;
	public String idKldi;
	public Boolean isDeleted;
	public String nama;
	public String statusPerubahan;
//	public String tahunAktif;


	public Satker getAsSatker(){
		try{
			Satker satker = new Satker();
			satker.id=this.id.intValue();
			satker.idSatker=this.idSatker;
			satker.idKldi=this.idKldi;
			satker.nama=this.nama;
			satker.statusPerubahan = this.statusPerubahan;
			satker.active=true;
			return satker;
		}catch (Exception e){
			Logger.error("error parse DceSatker to satker"+ e.getMessage());
			e.printStackTrace();
		}
		return null;
	}


}
