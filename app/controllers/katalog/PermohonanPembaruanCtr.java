package controllers.katalog;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import controllers.BaseController;
import controllers.HomeCtr;
import controllers.katalog.form.ProductForm;
import controllers.security.AllowAccess;
import ext.FormatUtils;
import jobs.JobTrail.JobUpdateViewCounter;
import models.cms.KategoriKonten;
import models.common.AktifUser;
import models.common.DokumenInfo;
import models.common.OngkosKirim;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.util.CommonUtil;
import models.katalog.*;
import models.katalog.komoditasmodel.KomoditasAtributHarga;
import models.masterdata.*;
import models.penyedia.PenyediaDistributor;
import models.penyedia.PenyediaDistributorRepresentatif;
import models.permohonan.PermohonanPembaruan;
import models.permohonan.ProdukStagging;
import models.permohonan.search.PermohonanPembaruanSearch;
import models.permohonan.search.PermohonanPembaruanSearchResult;
import models.penyedia.Penyedia;
import models.permohonan.PembaruanStaging;
import models.prakatalog.Penawaran;
import models.permohonan.*;
import models.product.search.ProductSearch;
import models.product.search.ProductSearchResult;
import models.purchasing.status.PaketStatus;
import models.secman.Acl;
import models.user.User;
import models.util.produk.HargaProdukJson;
import models.util.produk.ProdukHargaJson;
import models.util.produk.RiwayatHargaProdukJson;
import models.util.produk.SpesifikasiProdukJson;
import org.apache.http.util.TextUtils;
import org.json.simple.JSONObject;
import play.Logger;
import play.Play;
import play.i18n.Messages;
import play.libs.Codec;
import play.libs.Files;
import play.mvc.Router;
import repositories.WilayahRepository;
import repositories.katalog.PermohonanPembaruanRepository;
import repositories.produk.ProductRepository;
import repositories.produkkategori.ProdukKategoriRepository;
import services.produk.ProdukService;
import sun.security.util.Pem;
import utils.DateTimeUtils;
import utils.KatalogUtils;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static services.produk.ProdukService.getOngkirProdukJson;

public class PermohonanPembaruanCtr extends BaseController {
    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN})
    public static void index() {
        PermohonanPembaruanSearch query = new PermohonanPembaruanSearch(params);
        List<Penyedia> penyedia = new ArrayList<>();
        User user = User.findById(AktifUser.getActiveUserId());
        Map<String, String> jenisKomoditasMaps = PermohonanPembaruan.jenisKomoditasMaps();
        Map<String, String> tipePermohonanMaps = PermohonanPembaruan.tipePermohonanMaps();
        Map<String, String> posisiPermohonanMaps = PermohonanPembaruan.posisiPermohonanMaps();
        Map<String, String> statusPermohonanMaps = PermohonanPembaruan.statusPermohonanMaps();

        PermohonanPembaruanSearchResult model = new PermohonanPembaruanSearchResult(query);

        if (query.order.isEmpty()) {
            query.order = "6";
        }

        if(AktifUser.getAktifUser().isAdminLokalSektoral() && null != getAktifUser().kldi_jenis){
            if(getAktifUser().kldi_jenis.equalsIgnoreCase("provinsi") ||
                getAktifUser().kldi_jenis.equalsIgnoreCase("kabupaten") ||
                getAktifUser().kldi_jenis.equalsIgnoreCase("kota")){
                query.jenis = "lokal";
            }else{
                query.jenis = "sektoral";
            }
        }

        if (!AktifUser.getAktifUser().isProvider()) {
            penyedia = Penyedia.getPenyediaPermohonan();
        }

        query.uid = user.id;
        long dispoId = 0l;

        if ((getAktifUser().isAdmin() || getAktifUser().isAdminLokalSektoral()) && null != getAktifUser().jabatan
                && (getAktifUser().jabatan.equalsIgnoreCase(PermohonanPembaruan.STATUS_DIREKTUR) ||
                getAktifUser().jabatan.equalsIgnoreCase(PermohonanPembaruan.STATUS_KASUBDIT) ||
                getAktifUser().jabatan.equalsIgnoreCase(PermohonanPembaruan.STATUS_KASI))) {
//            query.dis_ui = user.id;
            dispoId = user.id;
        }

        if (query.posisi == "0") {
            if ((getAktifUser().isAdmin() || getAktifUser().isAdminLokalSektoral()) && null == getAktifUser().jabatan) {
                query.posisi = PermohonanPembaruan.STATUS_ADMIN;
            } else {
                query.posisi = (null != getAktifUser().jabatan) ? getAktifUser().jabatan : "all";
            }
        }

        if (query.posisi.equalsIgnoreCase(getAktifUser().jabatan)){
            query.dis_ui = dispoId;
        }

        model = PermohonanPembaruanRepository.searchPackage(query);

        boolean showButtonPermohonan = AktifUser.getAktifUser().isProvider() ? true : false;

        renderArgs.put("paramQuery", query);
        render("katalog/ProdukCtr/permohonan/listPermohonanPembaruan.html", model, user, jenisKomoditasMaps, tipePermohonanMaps,
                statusPermohonanMaps, posisiPermohonanMaps, showButtonPermohonan, penyedia);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_ADD})
    public static void create() {
        allowToAccessMenuPermohonanPembaruan();
        Map<String, String> tipePermohonanMaps = PermohonanPembaruan.tipePermohonanMaps();
        render("katalog/ProdukCtr/permohonan/createPermohonanPembaruan.html", tipePermohonanMaps);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_ADD})
    public static void createSubmit(PermohonanPembaruan pmb, File file) {
        String jenisPermohonan = "", tipePermohonan = "", idPermohonan = "null", noPermohonan = "";
        Penyedia penyedia = Penyedia.getProviderByUserId(AktifUser.getActiveUserId().longValue());
        /*
        KODE untuk nomor permohonan
        Permohonan Pembaruan - Nasional/Lokal/Sektoral - Profil/Distributor/Produk - id
        PPNA-PROF-ID
        PPLO-DIST-ID
        PPSE-PROD-ID
        */
        if (!pmb.jenis_komoditas.isEmpty()) {
            if (pmb.jenis_komoditas.equalsIgnoreCase(pmb.JENIS_KOMODITAS_NASIONAL)) {
                jenisPermohonan = "PPNA";
            } else if ((pmb.jenis_komoditas.equalsIgnoreCase(pmb.JENIS_KOMODITAS_LOKAL) || pmb.jenis_komoditas.equalsIgnoreCase(pmb.JENIS_KOMODITAS_SEKTORAL)) &&  pmb.kldi_id.isEmpty()) {
                flash.error("Data tidak berhasil disimpan, Silahkan Pilih K/L/PD");
                redirect("katalog.PermohonanPembaruanCtr.create");
            } else if (pmb.jenis_komoditas.equalsIgnoreCase(pmb.JENIS_KOMODITAS_LOKAL)) {
                jenisPermohonan = "PPLO";
            } else if (pmb.jenis_komoditas.equalsIgnoreCase(pmb.JENIS_KOMODITAS_SEKTORAL)) {
                jenisPermohonan = "PPSE";
            }
        } else {
            jenisPermohonan = "PPNA";
        }

        if (pmb.tipe_permohonan.equalsIgnoreCase(pmb.TIPE_PERMOHONAN_PROFIL)) {
            tipePermohonan = "PROF";
        } else if (pmb.tipe_permohonan.equalsIgnoreCase(pmb.TIPE_PERMOHONAN_PRODUK)) {
            tipePermohonan = "PROD";
        } else {
            tipePermohonan = "DIST";
        }

        noPermohonan = jenisPermohonan+"-"+tipePermohonan+"-"+idPermohonan;

        if(pmb.jenis_komoditas.equalsIgnoreCase("nasional")){
            pmb.kldi_jenis = null;
        }
        pmb.status = pmb.STATUS_DRAFT; //status awal adalah draft
        pmb.penyedia_id = penyedia.id;
        pmb.active = 1;
        pmb.nomor_permohonan = noPermohonan;

        boolean msg = pmb.permohonanSubmit(pmb, file);
        if (msg) {
            if (pmb.id != null){
                pmb.getNomor_permohonan();
            }
            flash.success("Data berhasil disimpan");
        } else {
            flash.error("Data tidak berhasil disimpan");
        }

        redirect("katalog.PermohonanPembaruanCtr.index");
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_EDIT})
    public static void edit(long id) {
        allowToAccessMenuPermohonanPembaruan();

        PermohonanPembaruan pmp = PermohonanPembaruan.findById(id);
        Kldi kldi = new Kldi();
        if(null!=pmp.kldi_id && !pmp.kldi_id.isEmpty()){
            kldi = Kldi.findbyIdKldi(pmp.kldi_id);
        }
        allowToShowMenuPermohonanPembaruan(pmp);

        Map<String, String> tipePermohonanMaps = PermohonanPembaruan.tipePermohonanMaps();

        boolean showButtonSimpan = false;
        String statusPmb = "";
        statusPmb = pmp.status != null ? pmp.status : "";

        if (statusPmb.equalsIgnoreCase(pmp.STATUS_DRAFT) || statusPmb.equalsIgnoreCase(pmp.STATUS_PERSIAPAN) || statusPmb.equalsIgnoreCase(pmp.STATUS_VERIFIKASI_TOLAK)) {
            showButtonSimpan = true;
        }

        renderArgs.put("showButtonSimpan", showButtonSimpan);
        renderArgs.put("kldi", kldi);
        render("katalog/ProdukCtr/permohonan/editPermohonanPembaruan.html", pmp, tipePermohonanMaps);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DETAIL})
    public static void detail(long id) {
        allowToAccessMenuPermohonanPembaruan();

        PermohonanPembaruan pmp = PermohonanPembaruan.findById(id);

        allowToShowMenuPermohonanPembaruan(pmp);

        Map<String, String> tipePermohonanMaps = PermohonanPembaruan.tipePermohonanMaps();
        render("katalog/ProdukCtr/permohonan/editPermohonanPembaruan.html", pmp, tipePermohonanMaps);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_EDIT})
    public static void editSubmit(PermohonanPembaruan pmb, File file) {
        PermohonanPembaruan pmp = PermohonanPembaruan.findById(pmb.id);

        if (pmp.status != null && pmp.status.equalsIgnoreCase(pmp.STATUS_VERIFIKASI_TOLAK)) {
            pmp.status = pmp.STATUS_DRAFT;
        }

        if ((pmb.jenis_komoditas.equalsIgnoreCase(pmb.JENIS_KOMODITAS_LOKAL) || pmb.jenis_komoditas.equalsIgnoreCase(pmb.JENIS_KOMODITAS_SEKTORAL)) &&  pmb.kldi_id.isEmpty()) {
            flash.error("Data tidak berhasil disimpan, Silahkan Pilih K/L/PD");
            redirect("katalog.PermohonanPembaruanCtr.edit", pmb.id);
        }

        pmp.deskripsi = pmb.deskripsi;
        pmp.kldi_jenis = pmb.kldi_jenis;
        pmp.kldi_id = pmb.kldi_id;
        pmp.jenis_komoditas = pmb.jenis_komoditas;
        pmp.active = 1;
        boolean msg = pmp.permohonanSubmit(pmp, file);
        if (msg) {
            flash.success("Data berhasil diubah");
        } else {
            flash.error("Data tidak berhasil diubah");
        }

        edit(pmb.id);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DELETE})
    public static void delete() {

    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_ADD})
    public static void formPermohonan(long id) {
        boolean showButtonSimpan = false;
        boolean showButtonKirim = false;
        String statusPmb = "";

        PermohonanPembaruan permohonanPembaruan = PermohonanPembaruan.findById(id);
        allowToAccess(permohonanPembaruan);
        statusPmb = permohonanPembaruan.status;

        if (statusPmb.equalsIgnoreCase(permohonanPembaruan.STATUS_DRAFT)) {
            showButtonKirim = false;
            showButtonSimpan = true;
        } else if (statusPmb.equalsIgnoreCase(permohonanPembaruan.STATUS_PERSIAPAN) || statusPmb.equalsIgnoreCase(permohonanPembaruan.STATUS_VERIFIKASI_TOLAK)) {
            showButtonKirim = true;
            showButtonSimpan = true;
        }

        if (permohonanPembaruan.tipe_permohonan.equalsIgnoreCase(permohonanPembaruan.TIPE_PERMOHONAN_PROFIL)) {
            PembaruanStaging stagPerm = PembaruanStaging.find("permohonan_pembaruan_id = ? and active = 1", permohonanPembaruan.id).first();
            Penyedia penyedia = null;
            if (stagPerm != null) {
                penyedia = stagPerm.getPenyediaIfPenyedia();
            } else {
                penyedia = Penyedia.getProviderByUserId(new Long(permohonanPembaruan.created_by));
                stagPerm = new PembaruanStaging();
                stagPerm.class_name = Penyedia.class.getCanonicalName();
                stagPerm.permohonan_pembaruan_id = id;
                stagPerm.object_json = gson.toJson(penyedia);
                stagPerm.isList = false;
                stagPerm.active = true;
                stagPerm.save();
            }

            renderArgs.put("showButtonKirim", showButtonKirim);
            renderArgs.put("showButtonSimpan", showButtonSimpan);
            render("katalog/ProdukCtr/stagging/editPenyediaStagingInfo.html", permohonanPembaruan, penyedia);
        } else if (permohonanPembaruan.tipe_permohonan.equalsIgnoreCase(permohonanPembaruan.TIPE_PERMOHONAN_DISTRIBUTOR)) {
            PembaruanStaging stagPerm = PembaruanStaging.find("permohonan_pembaruan_id = ? and active = 1", permohonanPembaruan.id).first();
            Penyedia penyedia = Penyedia.getProviderByUserId(AktifUser.getActiveUserId().longValue());
            PermohonanPembaruan pmb = permohonanPembaruan;
//            if (stagPerm != null) {
//                penyedia = stagPerm.getPenyediaIfPenyedia();
//            } else {
//                penyedia = Penyedia.getProviderByUserId(new Long(permohonanPembaruan.created_by));
//                stagPerm = new PembaruanStaging();
//                stagPerm.class_name = Penyedia.class.getCanonicalName();
//                stagPerm.permohonan_pembaruan_id = id;
//                stagPerm.object_json = gson.toJson(penyedia);
//                stagPerm.isList = false;
//                stagPerm.active = true;
//                stagPerm.save();
//            }
            String sStr = "";
            if (null != stagPerm) {
                List<PembaruanStaging> lstPstg = PembaruanStaging.getAllDistributorByPermohonanPembaruanId(id);

                lstPstg = lstPstg.stream().filter(f -> f.getPermohonanPembaruan()
                        .tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_DISTRIBUTOR)).collect(Collectors.toList());

                if(lstPstg.size() > 0){
                    try {
                        sStr = lstPstg.stream().map(PembaruanStaging::getDistributorIdIfTypeDistributor).collect(Collectors.joining(","));
                    } catch (Exception e) {
                    }
                }
                Long idPembaruan = id;
                renderArgs.put("idsDist", sStr);
                renderArgs.put("idPembaruan", idPembaruan);
            }

            renderArgs.put("showButtonKirim", showButtonKirim);
            renderArgs.put("showButtonSimpan", showButtonSimpan);
            render("katalog/ProdukCtr/stagging/distributor/index.html", permohonanPembaruan, penyedia, pmb);
        } else if (permohonanPembaruan.tipe_permohonan.equalsIgnoreCase(permohonanPembaruan.TIPE_PERMOHONAN_PRODUK)) {
            listProdukPermohonanPembaruan(id);
        }
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DISTRIBUTOR})
    public static void listApprovalDistributorPembaruanStaging(Long id, int approved) {
//        boolean showButtonKirim = false, showButtonSetujuTolakData = false, showButtonSetujuTolakDisposisi = false;
        String statusPmb = "";

        Long pid = new Long(0);

        PermohonanPembaruan pmb = PermohonanPembaruan.findById(id);

        if (id != null) {
            if (getAktifUser().is_penyedia) {
                Penyedia penyedia = Penyedia.getProviderByUserId(AktifUser.getAktifUser().user_id);
                pid = penyedia.id;
                renderArgs.put("penyediaId", pid);
            } else {
                renderArgs.put("penyediaId", pid);
            }

            List<PembaruanStaging> lstPstg = PembaruanStaging.getByPermohonanPembaruanIdApproved(pmb.id, approved);

//            List<PembaruanStaging> lstPstg = PembaruanStaging.getDistributorByPermohonanPembaruanId(id);

            lstPstg = lstPstg.stream().filter(f -> f.getPermohonanPembaruan()
                    .tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_DISTRIBUTOR)).collect(Collectors.toList());

//        //ambil id as sting
            List<String> sStr = new ArrayList<>();
            try {
                sStr = Arrays.asList(lstPstg.stream().map(PembaruanStaging::getDistributorIdIfTypeDistributor).collect(Collectors.joining(",")).split(","));
                Logger.info("SSTRRR " + sStr);
            } catch (Exception e) {
            }


            Long idPembaruan = id;

            renderArgs.put("idsDist", sStr);
            renderArgs.put("idPembaruan", idPembaruan);


            render("katalog/ProdukCtr/stagging/distributor/listApprovalDistributorPembaruanStaging.html", pmb, approved);
        }
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DISTRIBUTOR_ADD})
    public static void submitDistributorApprovalPeritems(long[] idDistributor, Long idPembaruan) {
        PermohonanPembaruan pmb = PermohonanPembaruan.findById(idPembaruan);
        List<PembaruanStaging> list = PembaruanStaging.getIfDistributorStagingByIdProduk(idDistributor,idPembaruan);
        if(idDistributor != null){
            for (PembaruanStaging stg : list) {
                stg.apakah_disetujui = 1;
                stg.tgl_disetujui = new Timestamp(new Date().getTime());
                stg.save();
            }
            flash.success("Persetujuan Distributor Update disimpan");
            listApprovalDistributorPembaruanStaging(pmb.id, 0);
        }else{
            flash.error("Silahkan memilih distributor yang akan di setujui");
            listApprovalDistributorPembaruanStaging(pmb.id, 0);
        }

    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PROFIL})
    public static void infoDetailPenyedia(Long id) {
        boolean showButtonSetujuTolakData = false, showButtonSetujuTolakDisposisi = false;
        PermohonanPembaruan permohonanPembaruan = PermohonanPembaruan.findById(id);
        allowToAccess(permohonanPembaruan);
        PembaruanStaging stagPerm = PembaruanStaging.find("permohonan_pembaruan_id = ? and active = 1", id).first();
        Penyedia penyedia = stagPerm.getPenyediaIfPenyedia();
        Penyedia penyediaOri = Penyedia.getProviderByUserId(new Long(permohonanPembaruan.created_by));
        PembaruanStatus ps = PembaruanStatus.getLatestStatusByPermohonanId(id);
        if ((null == getAktifUser().jabatan || getAktifUser().jabatan.isEmpty()) && (getAktifUser().isAdmin() || getAktifUser().isAdminLokalSektoral())){
            if (ps!=null && ps.status_permohonan.equalsIgnoreCase(ps.STATUS_PERMOHONAN_PERSIAPAN)
                    && ps.to_status.equalsIgnoreCase(ps.STATUS_ADMIN) && ps.from_status.equalsIgnoreCase(ps.STATUS_PENYEDIA)){
                showButtonSetujuTolakData = true;
            }
            if (ps!=null && ps.status_permohonan.equalsIgnoreCase(ps.STATUS_PERMOHONAN_LENGKAP)
                    && ps.to_status.equalsIgnoreCase(ps.STATUS_ADMIN) && ps.from_status.equalsIgnoreCase(ps.STATUS_ADMIN)){
                showButtonSetujuTolakDisposisi = true;
            }
        }
        List<User> sendToUser = new ArrayList<>();
        //ambil data user
        if ((permohonanPembaruan.status.equalsIgnoreCase(permohonanPembaruan.STATUS_VERIFIKASI_SETUJU))){
            sendToUser = PermohonanPembaruan.findUserToSend(ps.to_status, permohonanPembaruan.status, permohonanPembaruan.jenis_komoditas, permohonanPembaruan.kldi_id);
        }
        renderArgs.put("showButtonSetujuTolakDisposisi",showButtonSetujuTolakDisposisi);
        renderArgs.put("showButtonSetujuTolakData",showButtonSetujuTolakData);
        render("katalog/ProdukCtr/stagging/detailPenyediaStagingInfo.html", permohonanPembaruan, penyedia, penyediaOri, sendToUser);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK})
    public static void listProdukPermohonanPembaruanStaging(Long id) {
        boolean showButtonKirim = false, showButtonSetujuTolakData = false, showButtonSetujuTolakDisposisi = false;
        String statusPmb = "";

        ProductSearch query = new ProductSearch(params);
        ProductSearchResult model = new ProductSearchResult(query);
        List<Komoditas> komoditas = new ArrayList<>();
        List<User> sendToUser = new ArrayList<>();
        PermohonanPembaruan pmb = PermohonanPembaruan.findById(id);
        allowToAccess(pmb);
        statusPmb = pmb.status;

        PembaruanStatus ps = PembaruanStatus.getLatestStatusByPermohonanId(id);
        if ((null == getAktifUser().jabatan || getAktifUser().jabatan.isEmpty()) && (getAktifUser().isAdmin() || getAktifUser().isAdminLokalSektoral())){
            if (ps!=null && ps.status_permohonan.equalsIgnoreCase(ps.STATUS_PERMOHONAN_PERSIAPAN)
                    && ps.to_status.equalsIgnoreCase(ps.STATUS_ADMIN) && ps.from_status.equalsIgnoreCase(ps.STATUS_PENYEDIA)){
                showButtonSetujuTolakData = true;
            }
            if (ps!=null && ps.status_permohonan.equalsIgnoreCase(ps.STATUS_PERMOHONAN_LENGKAP)
                    && ps.to_status.equalsIgnoreCase(ps.STATUS_ADMIN) && ps.from_status.equalsIgnoreCase(ps.STATUS_ADMIN)){
                showButtonSetujuTolakDisposisi = true;
            }
        }

        if (getAktifUser().isProvider()) {
            try {
                Penyedia py = Penyedia.findById(pmb.penyedia_id);
                komoditas = Komoditas.findByPenyedia(pmb.penyedia_id);
                query.penyedia = Penyedia.getProviderByUserId(py.user_id).id.toString();
            } catch (Exception e) {
                Logger.error("error:" + e.getMessage());
            }
        } else {
            // belum define
        }

        if (params.all().size() > 1) {
            model = ProductRepository.searchProduct(query);
        }

        if (statusPmb.equalsIgnoreCase(pmb.STATUS_PERSIAPAN) || statusPmb.equalsIgnoreCase(pmb.STATUS_VERIFIKASI_TOLAK)) {
            showButtonKirim = true;
        }

        //all permbaruan stagging per permohonan
        List<PembaruanStaging> lstPstg = PembaruanStaging.getByPermohonanPembaruanId(pmb.id);
        //filter yang tipenya produk
        lstPstg = lstPstg.stream().filter(f -> f.getPermohonanPembaruan()
                .tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_PRODUK)).collect(Collectors.toList());
        //ambil id as sting
        List<String> sStr = new ArrayList<>();
        try {
            sStr = Arrays.asList(lstPstg.stream().map(PembaruanStaging::getProdukIdIfTypeProduk).collect(Collectors.joining(",")).split(","));
        } catch (Exception e) {
        }
        //convert as long
        List<Long> sLong = new ArrayList<>();
        try {
            sLong = sStr.stream().map(Long::parseLong).distinct().collect(Collectors.toList());
        } catch (Exception e) {
        }
        //filter buang produk yang sedang pengajuan
        //List<Long> finalSLong = sLong;
        // search ids
        query.listProdukIdsIn = sLong;

        if(params.all().size() > 1 && sLong.size() > 0){
            model = ProductRepository.searchProduct(query);
        }else{
            model = new ProductSearchResult(query);
        }
        List<PembaruanStaging> listStagging = PembaruanStaging.getProdukDistinct(pmb.id);

        //ambil data user
        if ((pmb.status.equalsIgnoreCase(pmb.STATUS_VERIFIKASI_SETUJU))){
            sendToUser = PermohonanPembaruan.findUserToSend(ps.to_status, pmb.status, pmb.jenis_komoditas, pmb.kldi_id);
        }
        renderArgs.put("listStagging",listStagging);
        renderArgs.put("showButtonSetujuTolakDisposisi",showButtonSetujuTolakDisposisi);
        renderArgs.put("showButtonSetujuTolakData",showButtonSetujuTolakData);
        renderArgs.put("paramQuery", query);
        renderArgs.put("showButtonKirim", showButtonKirim);
        render("katalog/ProdukCtr/listProdukPermohonanPembaruanStaging.html", model, komoditas, pmb, sendToUser);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK})
    public static void listProdukPermohonanPembaruan(Long id) {
        ProductSearch query = new ProductSearch(params);
        ProductSearchResult model = new ProductSearchResult(query);
        List<Komoditas> komoditas = new ArrayList<>();
        PermohonanPembaruan pmb = PermohonanPembaruan.findById(id);
        allowToAccess(pmb);
        if (getAktifUser().isProvider()) {
            try {
                Penyedia py = Penyedia.findById(pmb.penyedia_id);
                komoditas = Komoditas.findByPenyedia(pmb.penyedia_id);
                query.penyedia = Penyedia.getProviderByUserId(py.user_id).id.toString();
            } catch (Exception e) {
                Logger.error("error:" + e.getMessage());
            }
        } else {
            // belum define
        }
        if (params.all().size() > 1) {
            model = ProductRepository.searchProduct(query);
        }

        //all permbaruan stagging per permohonan
        List<PembaruanStaging> lstPstg = PembaruanStaging.getByPermohonanPembaruanId(pmb.id);
        //filter yang tipenya produk
        lstPstg = lstPstg.stream().filter(f -> f.getPermohonanPembaruan()
                .tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_PRODUK)).collect(Collectors.toList());
        //ambil id as sting
        List<String> sStr = new ArrayList<>();
        try {
            sStr = Arrays.asList(lstPstg.stream().map(PembaruanStaging::getProdukIdIfTypeProduk).collect(Collectors.joining(",")).split(","));
        } catch (Exception e) {
        }
        //convert as long
        List<Long> sLong = new ArrayList<>();
        try {
            sLong = sStr.stream().map(Long::parseLong).collect(Collectors.toList());
        } catch (NumberFormatException e) {
        }
        //filter buang produk yang sedang pengajuan
        List<Long> finalSLong = sLong;

        //query.id =id;

//        didisable karena user request (21 Nov) produk yg sudah dipilih tidak hilang dari list
        query.listProdukIdsNotIn = finalSLong;

        if (params.all().size() > 1) {
            model = ProductRepository.searchProduct(query);
        }

        boolean showButtonSimpan = false;
        String statusPmb = "";
        statusPmb = pmb.status;

        if (statusPmb.equalsIgnoreCase(pmb.STATUS_DRAFT) || statusPmb.equalsIgnoreCase(pmb.STATUS_PERSIAPAN)
                || statusPmb.equalsIgnoreCase(pmb.STATUS_VERIFIKASI_TOLAK)) {
            showButtonSimpan = true;
        }

        renderArgs.put("paramQuery", query);
        renderArgs.put("showButtonSimpan", showButtonSimpan);
        render("katalog/ProdukCtr/listProdukPermohonanPembaruan.html", model, komoditas, pmb);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK_ADD})
    public static void submitProdukPermohonan(Long pmb, long[] info, long[] spec, long[] lamp, long[] gambar, long[] harga) {
        PermohonanPembaruan pmbo = PermohonanPembaruan.findById(pmb);
        ProdukStagging pstg = null;

        pmbo.status = pmbo.STATUS_PERSIAPAN;
        pmbo.save();

        if (null != pmb && null != info) {
            Logger.info("Stagging: Info");
            for (long p : info) {
                Produk produk = Produk.findById(p);
                pstg = new ProdukStagging();
                pstg.info = true;
                pstg.permohonan_pembaruan_id = pmbo.id;
                pstg.produk_id = p;
                pstg.penyedia_id = produk.penyedia_id;
                pstg.object_json = pstg.createGson().toJson(produk);
                pstg.class_name = produk.getClass().getCanonicalName();

                PembaruanStaging stg = new PembaruanStaging();
                stg.permohonan_pembaruan_id = pmb;
                stg.class_name = pstg.getClass().getCanonicalName();
                stg.isList = false;
                stg.object_json = gson.toJson(pstg);
                stg.active = true;
                stg.save();
            }
        }
        if (null != pmb && null != spec) {
            Logger.info("Stagging: Spec");

            for (long p : spec) {
                Produk produk = Produk.findById(p);
                produk.withSpecifications();

                pstg = new ProdukStagging();
                pstg.spec = true;
                pstg.permohonan_pembaruan_id = pmbo.id;
                pstg.produk_id = p;
                pstg.penyedia_id = produk.penyedia_id;
                pstg.object_json = pstg.createGson().toJson(produk.specifications);
                pstg.class_name = produk.specifications.getClass().getCanonicalName();

                PembaruanStaging stg = new PembaruanStaging();
                stg.permohonan_pembaruan_id = pmb;
                stg.class_name = pstg.getClass().getCanonicalName();
                stg.isList = false;
                stg.object_json = gson.toJson(pstg);
                stg.active = true;
                stg.save();
            }
        }
        if (null != pmb && null != lamp) {
            Logger.info("Stagging: Lampiran");
            for (long p : lamp) {
                Produk produk = Produk.findById(p);
                produk.withDocuments();

                pstg = new ProdukStagging();
                pstg.lamp = true;
                pstg.permohonan_pembaruan_id = pmbo.id;
                pstg.produk_id = p;
                pstg.penyedia_id = produk.penyedia_id;
                pstg.object_json = pstg.createGson().toJson(produk.documents);
                pstg.class_name = produk.documents.getClass().getCanonicalName();

                PembaruanStaging stg = new PembaruanStaging();
                stg.permohonan_pembaruan_id = pmb;
                stg.class_name = pstg.getClass().getCanonicalName();
                stg.isList = false;
                stg.object_json = gson.toJson(pstg);
                stg.active = true;
                stg.save();
            }
        }
        if (null != pmb && null != gambar) {
            Logger.info("Stagging: Gambar");
            for (long p : gambar) {
                Produk produk = Produk.findById(p);
                produk.withPictures();

                pstg = new ProdukStagging();
                pstg.gambar = true;
                pstg.permohonan_pembaruan_id = pmbo.id;
                pstg.produk_id = p;
                pstg.penyedia_id = produk.penyedia_id;
                pstg.object_json = pstg.createGson().toJson(produk.pictures);
                pstg.class_name = produk.pictures.getClass().getCanonicalName();

                PembaruanStaging stg = new PembaruanStaging();
                stg.permohonan_pembaruan_id = pmb;
                stg.class_name = pstg.getClass().getCanonicalName();
                stg.isList = false;
                stg.object_json = gson.toJson(pstg);
                stg.active = true;
                stg.save();
            }
        }
        if (null != pmb && null != harga) {
            for (long p : harga) {
                Produk produk = Produk.findById(p);
                produk.withPricesEqualsDatePrice();
                if(produk.prices.size() ==0){
                    produk.withPricesApproved();
                }
                for ( ProdukHarga ph: produk.prices) {
                    if(null != ph.checked_locations_json) {
                        ph.checked_locations_json = ph.checked_locations_json.replace("\'", "");
                    }
                }
                ProdukHargaStagging phs = new ProdukHargaStagging();
                phs.produk_id = produk.id;
                phs.produkHargas = produk.prices;
                phs.produkAtributValues = ProdukAtributValue.findByProdukId(produk.id);
                phs.ongkir = produk.ongkir;
                phs.harga_tanggal = produk.harga_tanggal;

                pstg = new ProdukStagging();
                pstg.harga = true;
                pstg.permohonan_pembaruan_id = pmbo.id;
                pstg.produk_id = p;
                pstg.penyedia_id = produk.penyedia_id;
                pstg.object_json = pstg.createGson().toJson(phs);
                pstg.class_name = phs.getClass().getCanonicalName();

                PembaruanStaging stg = new PembaruanStaging();
                stg.permohonan_pembaruan_id = pmb;
                stg.class_name = pstg.getClass().getCanonicalName();
                stg.isList = false;
                stg.object_json = gson.toJson(pstg);
                stg.active = true;
                stg.save();

            }
        }
        index();
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DISPOSISI})
    public static void listDisposisi(long id) {
        boolean showButtonTambahDisposisi = true;
        boolean showButtonKirimDisposisi = false;
        boolean showButtonSetujuTolakData = false;
        boolean showButtonSetujuTolakDisposisi = false;
        Long uid = 0L;
        String jabatan = "", posisi = "", disposisiDari = "", statusDisposisi = "", dispoKe = "";

        AktifUser au = AktifUser.getAktifUser();
        List<PembaruanDisposisi> disposisi = PembaruanDisposisi.findActiveByPermohonan(id);
        PembaruanStatus ps = PembaruanStatus.getLatestStatusByPermohonanId(id);

        uid = au.user_id;
        jabatan = au.jabatan;
        posisi = ps.to_status;
        disposisiDari = ps.from_status;
        statusDisposisi = ps.status_permohonan;

        if (posisi.equalsIgnoreCase(PermohonanPembaruan.STATUS_DIREKTUR)){
            dispoKe = PermohonanPembaruan.STATUS_KASUBDIT;
        } else if (posisi.equalsIgnoreCase(PermohonanPembaruan.STATUS_KASUBDIT)){
            dispoKe = PermohonanPembaruan.STATUS_KASI;
        }

        showButtonTambahDisposisi = ps.to_status.equalsIgnoreCase(jabatan) ? true : false;

        for (PembaruanDisposisi p : disposisi) {
            if (uid.equals(new Long(p.created_by))) {
                showButtonTambahDisposisi = false;

                if (posisi.equalsIgnoreCase(jabatan) && !jabatan.equalsIgnoreCase(PermohonanPembaruan.STATUS_KASI))
                    showButtonKirimDisposisi = true;

                if (posisi.equalsIgnoreCase(jabatan) && jabatan.equalsIgnoreCase(PermohonanPembaruan.STATUS_KASI))
                    showButtonSetujuTolakData = true;
            }
        }

        if (ps.status_permohonan.equalsIgnoreCase(PembaruanStatus.STATUS_PERMOHONAN_LENGKAP)) {
            showButtonSetujuTolakDisposisi = true;
        }

        List<User> sendToUser = new ArrayList<>();
        PermohonanPembaruan pmb = PermohonanPembaruan.findById(id);
        //ambil data user
        if (!posisi.equalsIgnoreCase(PermohonanPembaruan.STATUS_KASI)){
            sendToUser = PermohonanPembaruan.findUserToSend(ps.to_status, pmb.status, pmb.jenis_komoditas, pmb.kldi_id);
        }

        renderArgs.put("showButtonTambahDisposisi", showButtonTambahDisposisi);
        renderArgs.put("showButtonKirimDisposisi", showButtonKirimDisposisi);
        renderArgs.put("showButtonSetujuTolakData", showButtonSetujuTolakData);
        renderArgs.put("showButtonSetujuTolakDisposisi", showButtonSetujuTolakDisposisi);
        render("katalog/ProdukCtr/permohonan/listPermohonanDisposisi.html", disposisi, id, dispoKe, sendToUser);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DISPOSISI_ADD})
    public static void createDispo(long permId) {
        render("katalog/ProdukCtr/permohonan/createDisposisi.html", permId);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DISPOSISI_ADD})
    public static void createDispoSubmit(PembaruanDisposisi dispo, File file) {
        dispo.active = 1;
        boolean msg = dispo.disposisiSubmit(dispo, file);
        if (msg) {
            flash.success("Data berhasil disimpan");
        } else {
            flash.error("Data tidak berhasil disimpan");
        }

        redirect("katalog.PermohonanPembaruanCtr.listDisposisi", dispo.permohonan_pembaruan_id);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DISPOSISI})
    public static void kirimDisposisi(long permId, String toUser) {
        String toStatus = "", statusPermohonan = "";
        long uid = 0;

        if (null != toUser){
            uid = Long.valueOf(toUser);
        }

        if (uid==0){
            flash.error("Disposisi gagal dikirim");
            redirect("katalog.PermohonanPembaruanCtr.listDisposisi", permId);
        }

        PembaruanStatus pembaruanStatus = PembaruanStatus.getLatestStatusByPermohonanId(permId);
        PermohonanPembaruan pemb = PermohonanPembaruan.findById(permId);
        PembaruanStatus pembaruanStatusTemp = new PembaruanStatus();
        PembaruanRiwayat pembaruanRiwayat = new PembaruanRiwayat();

        pembaruanStatusTemp = pembaruanStatus;
        pembaruanStatusTemp.from_status = pembaruanStatus.to_status;

        if (pembaruanStatus.to_status.equalsIgnoreCase(PembaruanStatus.STATUS_DIREKTUR)) {
            toStatus = PembaruanStatus.STATUS_KASUBDIT;
            statusPermohonan = PembaruanStatus.STATUS_PERMOHONAN_SETUJU_DIREKTUR;
        } else if (pembaruanStatus.to_status.equalsIgnoreCase(PembaruanStatus.STATUS_KASUBDIT)) {
            toStatus = PembaruanStatus.STATUS_KASI;
            statusPermohonan = PembaruanStatus.STATUS_PERMOHONAN_SETUJU_KASUBDIT;
        }

        pembaruanStatusTemp.to_status = toStatus;
        pembaruanStatusTemp.status_permohonan = statusPermohonan;

        int saveSucces = pembaruanStatusTemp.save();

        pemb.dispo_user_id = uid;
        pemb.save();

        if (saveSucces > 0) {
            pembaruanRiwayat.addDisposisiSendTo(permId, toStatus, pembaruanStatus.from_status);
            flash.success("Disposisi berhasil dikirim");
        } else {
            flash.error("Disposisi gagal dikirim");
        }

        redirect("katalog.PermohonanPembaruanCtr.listDisposisi", permId);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DISPOSISI})
    public static void setujuTolakDataDisposisi(long permId, String alasan, String tipe, String setujuTolak, String kirimKeUser) {
        String pathroute = "", statusPermohonanPembaruan = "", msg = "", statusDispo = "";
        long toUser = 0;

        PembaruanStatus pembaruanStatus = PembaruanStatus.getLatestStatusByPermohonanId(permId);
        PermohonanPembaruan permohonanPembaruan = PermohonanPembaruan.findById(permId);
        PembaruanRiwayat pembaruanRiwayat = new PembaruanRiwayat();
        //redirect("katalog.PermohonanPembaruanCtr.listDisposisi", permId); //jangan lupa ditutup kembali

        if (null != kirimKeUser){
            toUser = Long.parseLong(kirimKeUser);
        }

        if (tipe.equalsIgnoreCase("data")) { //setuju tolak data
            if (setujuTolak.equalsIgnoreCase("setuju")) {
                statusPermohonanPembaruan = permohonanPembaruan.STATUS_VERIFIKASI_SETUJU;
                statusDispo = pembaruanStatus.STATUS_PERMOHONAN_LENGKAP;
                pembaruanStatus.from_status = pembaruanStatus.to_status;
                pembaruanStatus.to_status = pembaruanStatus.STATUS_ADMIN;
            } else {
                statusPermohonanPembaruan = permohonanPembaruan.STATUS_VERIFIKASI_TOLAK;
                statusDispo = pembaruanStatus.STATUS_PERMOHONAN_TIDAK_LENGKAP;
                pembaruanStatus.from_status = pembaruanStatus.to_status;
                pembaruanStatus.to_status = pembaruanStatus.STATUS_PENYEDIA;
                permohonanPembaruan.minta_disetujui = 0;
                permohonanPembaruan.dispo_user_id = null;
                permohonanPembaruan.minta_disetujui_tanggal = null;
                permohonanPembaruan.minta_disetujui_oleh = null;
            }
        } else { //setuju tolak proses
            if (setujuTolak.equalsIgnoreCase("setuju")) {
                statusPermohonanPembaruan = permohonanPembaruan.STATUS_PERMOHONAN_DIPROSES;
                statusDispo = pembaruanStatus.STATUS_PERMOHONAN_SETUJU_ADMIN;
                pembaruanStatus.from_status = pembaruanStatus.to_status;
                pembaruanStatus.to_status = pembaruanStatus.STATUS_DIREKTUR;
                permohonanPembaruan.dispo_user_id = toUser;
            } else {
                statusPermohonanPembaruan = permohonanPembaruan.STATUS_SELESAI_TOLAK;
                statusDispo = pembaruanStatus.STATUS_PERMOHONAN_TOLAK_ADMIN;
                pembaruanStatus.from_status = pembaruanStatus.to_status;
                pembaruanStatus.to_status = pembaruanStatus.STATUS_PENYEDIA;
                permohonanPembaruan.minta_disetujui = 0;
                permohonanPembaruan.dispo_user_id = null;
                permohonanPembaruan.minta_disetujui_tanggal = null;
                permohonanPembaruan.minta_disetujui_oleh = null;
            }
        }

        permohonanPembaruan.status = statusPermohonanPembaruan;
        permohonanPembaruan.setuju_tolak = setujuTolak + "_" + tipe;
        permohonanPembaruan.setuju_tolak_oleh = AktifUser.getAktifUser().user_id;
        permohonanPembaruan.setuju_tolak_alasan = alasan;
        permohonanPembaruan.setuju_tolak_tanggal = new Timestamp(new java.util.Date().getTime());
        int saveData = permohonanPembaruan.save();

        msg = "Proses " + setujuTolak + " " + (tipe.equalsIgnoreCase("data") ? "verfikasi" : "permohonan");

        if (saveData > 0) {
            if (tipe.equalsIgnoreCase("data")) {
                pembaruanRiwayat.addDisposisiData(permId, pembaruanStatus.STATUS_ADMIN, setujuTolak + "_" + tipe, alasan);
            } else {
                pembaruanRiwayat.addDisposisiProses(permId, pembaruanStatus.STATUS_ADMIN, setujuTolak + "_" + tipe, alasan);
            }
            pembaruanStatus.status_permohonan = statusDispo;
            pembaruanStatus.deskripsi = alasan;
            pembaruanStatus.save();

            flash.success(msg + " berhasil disimpan.");
        } else {
            flash.error(msg + " gagal disimpan.");
        }

        if(permohonanPembaruan.tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_PROFIL)){
            infoDetailPenyedia(permId);
        }else if(permohonanPembaruan.tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_PRODUK)){
            listProdukPermohonanPembaruanStaging(permId);
        }else if(permohonanPembaruan.tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_DISTRIBUTOR)){
            listDistributorPermohonanPembaruanStaging(permId);
        }
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DISPOSISI_EDIT})
    public static void editDispo(long dispId) {
        PembaruanDisposisi dispo = PembaruanDisposisi.findById(dispId);
        render("katalog/ProdukCtr/permohonan/editDisposisi.html", dispo);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DISPOSISI_EDIT})
    public static void editDispoSubmit(PembaruanDisposisi dispo, File file) {
        PembaruanDisposisi pembaruanDisposisi = PembaruanDisposisi.findById(dispo.id);
        pembaruanDisposisi.deskripsi = dispo.deskripsi;
        pembaruanDisposisi.active = 1;
        boolean msg = pembaruanDisposisi.disposisiSubmit(pembaruanDisposisi, file);
        if (msg) {
            flash.success("Data berhasil diubah");
        } else {
            flash.error("Data tidak berhasil diubah");
        }

        redirect("katalog.PermohonanPembaruanCtr.listDisposisi", pembaruanDisposisi.permohonan_pembaruan_id);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PROFIL_ADD})
    public static void formPenyediaSubmit(Penyedia permohonanPenyedia, Long permid) {
        PembaruanStaging stagPerm = PembaruanStaging.find("permohonan_pembaruan_id = ? and active = 1", permid).first();
        if (stagPerm != null) {
            Penyedia ppy = stagPerm.getPenyediaIfPenyedia();
            ppy.nama_penyedia = permohonanPenyedia.nama_penyedia;
            ppy.alamat = permohonanPenyedia.alamat;
            ppy.website = permohonanPenyedia.website;
            ppy.email = permohonanPenyedia.email;
            ppy.no_telp = permohonanPenyedia.no_telp;
            ppy.no_fax = permohonanPenyedia.no_fax;
            ppy.no_hp = permohonanPenyedia.no_hp;
            ppy.npwp = permohonanPenyedia.npwp;
            ppy.pkp = permohonanPenyedia.pkp;
            ppy.kode_pos = permohonanPenyedia.kode_pos;
            //save to stagging
            stagPerm.object_json = gson.toJson(ppy);
            stagPerm.save();


            PermohonanPembaruan permohonanPembaruan = PermohonanPembaruan.findById(permid);
            permohonanPembaruan.status = permohonanPembaruan.STATUS_PERSIAPAN;
            permohonanPembaruan.save();

            flash.success("Data berhasil disimpan");
        } else {
            flash.error("Data tidak berhasil disimpan");
            PermohonanPembaruan permohonanPembaruan = PermohonanPembaruan.findById(permid);

            boolean showButtonSimpan = false;
            boolean showButtonKirim = false;
            String statusPmb = "";
            statusPmb = permohonanPembaruan.status;

            if (statusPmb.equalsIgnoreCase(permohonanPembaruan.STATUS_DRAFT)) {
                showButtonKirim = false;
                showButtonSimpan = true;
            } else if (statusPmb.equalsIgnoreCase(permohonanPembaruan.STATUS_PERSIAPAN)) {
                showButtonKirim = true;
                showButtonSimpan = true;
            }

            renderArgs.put("showButtonKirim", showButtonKirim);
            renderArgs.put("showButtonSimpan", showButtonSimpan);
            render("katalog/ProdukCtr/stagging/editPenyediaStagingInfo.html", permohonanPembaruan, permohonanPenyedia);
        }
        formPermohonan(permid);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_ADD})
    public static void mintaPersetujuanSubmit(long id) {
        try {
            PermohonanPembaruan pemb = PermohonanPembaruan.findById(id);
            pemb.minta_disetujui = 1;
            pemb.status = pemb.STATUS_MENUNGGU_PERSETUJUAN;
            pemb.minta_disetujui_oleh = AktifUser.getActiveUserId().longValue();
            pemb.minta_disetujui_tanggal = new Timestamp(new java.util.Date().getTime());

            /* dipindah ke admin ketika proses persetujuan dan verifikasi
            User user = User.findByJabatanAndRoleBasic(pemb.STATUS_DIREKTUR, 1);
            long uid = null != user ? user.id : 2l; //sementara hardcode user utk direktur

            pemb.dispo_user_id = uid; //sementara hardcode user utk direktur
            */
            pemb.save();

            PembaruanStatus pembaruanStatus = PembaruanStatus.getLatestStatusByPermohonanId(id);
            if (pembaruanStatus != null) {
                pembaruanStatus.status_permohonan = pembaruanStatus.STATUS_PERMOHONAN_PERSIAPAN;
                pembaruanStatus.from_status = pembaruanStatus.STATUS_PENYEDIA;
                pembaruanStatus.to_status = pembaruanStatus.STATUS_ADMIN;
                pembaruanStatus.save();
            } else {
                PembaruanStatus pembStatus = new PembaruanStatus();
                pembStatus.permohonan_pembaruan_id = id;
                pembStatus.status_permohonan = pembStatus.STATUS_PERMOHONAN_PERSIAPAN;
                pembStatus.from_status = pembStatus.STATUS_PENYEDIA;
                pembStatus.to_status = pembStatus.STATUS_ADMIN;
                pembStatus.active = 1;
                pembStatus.save();
            }

            PembaruanRiwayat pembRiwayat = new PembaruanRiwayat();
            pembRiwayat.addSendToDirektur(id);

            flash.success("Data permohonan berhasil dikirim");

            if(pemb.tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_PROFIL)){
                infoDetailPenyedia(id);
            }else if(pemb.tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_PRODUK)){
                listProdukPermohonanPembaruanStaging(id);
            }else if(pemb.tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_DISTRIBUTOR)){
                listDistributorPermohonanPembaruanStaging(id);
            }
        } catch (Exception e) {
            e.printStackTrace();
            flash.error("Data permohonan gagal dikirim");
            formPermohonan(id);
        }
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK_EDIT})
    public static void editSubmitProdukStagingInfo(Produk produk, Long pmbs_id) {
        PembaruanStaging pg = PembaruanStaging.findById(pmbs_id);
        ProdukStagging psg = pg.getIfProdukStagging();
        Produk prdStag = psg.getProduk();
        prdStag.no_produk_penyedia = produk.no_produk_penyedia;
        prdStag.unit_pengukuran_id = produk.unit_pengukuran_id;
        prdStag.berlaku_sampai = produk.berlaku_sampai;
        prdStag.jumlah_stok = BigDecimal.valueOf(produk.jumlah_stok_form);
        prdStag.jumlah_stok_inden = BigInteger.valueOf(produk.jumlah_stok_inden_form);
        prdStag.produk_kategori_id = Integer.valueOf(produk.produk_kategori_id);
        prdStag.tkdn_produk = produk.tkdn_produk;

        prdStag.unspsc_id = produk.unspsc_id;
        Komoditas k = Komoditas.findById(prdStag.komoditas_id);
        if(k.jenis_produk_override){
            prdStag.jenis_produk = produk.jenis_produk;
        }
        prdStag.setProductNumber(k.kode_komoditas, produk.unspsc_id);

        //simpan produk di edit
        psg.object_json = psg.createGson().toJson(prdStag);
        //simpan ke stagging dalam json
        pg.object_json = gson.toJson(psg);
        //simpan stagging
        pg.save();
        flash.success(Messages.get("notif.success.submit"));
        listProdukPermohonanPembaruanStaging(pg.permohonan_pembaruan_id);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK_EDIT})
    public static void editSubmitProdukStagingSpec(String infoJson, Long produk_id, Long pmb_id) {
        Map<String, String> infoMap = ProdukService.getInformationFromJson(infoJson);
        List<ProdukAtributValue> list = new ArrayList<>();
        if (!infoMap.isEmpty()) {
            for (Map.Entry<String, String> entry : infoMap.entrySet()) {
                list.add(new ProdukAtributValue(produk_id, entry.getValue(), Long.valueOf(entry.getKey())));
            }
        }
        PembaruanStaging pembaruanStaging = PembaruanStaging.findById(pmb_id);
        ProdukStagging psg = pembaruanStaging.getIfProdukStagging();
        List<ProdukAtributValue> listOld = psg.getListProdukAtributValue();

        for(int i =0; i < listOld.size();i++){
            ProdukAtributValue po = listOld.get(i);
            ProdukAtributValue poN = list.stream().filter(f-> f.produk_kategori_atribut_id.equals(po.produk_kategori_atribut_id)).findFirst().orElse(null);
            if(null != poN){
                po.atribut_value = poN.atribut_value;
            }
            listOld.set(i, po);
        }

        psg.object_json = psg.createGson().toJson(listOld);

        pembaruanStaging.object_json = gson.toJson(psg);
        pembaruanStaging.save();
        flash.success(Messages.get("notif.success.submit"));
        listProdukPermohonanPembaruanStaging(pembaruanStaging.permohonan_pembaruan_id);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK_EDIT})
    public static void editProdukStagingInfo(Long id) {
        PembaruanStaging pg = PembaruanStaging.findById(id);
        renderArgs.put("pmbstagging", pg);
        ProdukStagging psg = pg.getIfProdukStagging();
        Produk produk = psg.getProduk();
        if (produk != null) {
            produk.jumlah_stok_form = produk.jumlah_stok.intValue();
            produk.jumlah_stok_inden_form = produk.jumlah_stok_inden.intValue();
        }
        renderArgs.put("produk", produk);
        Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
        renderArgs.put("komoditas", komoditas);
        List<Manufaktur> manufakturList = Manufaktur.findManufakturByKomoditas(komoditas.id);
        renderArgs.put("manufakturList", manufakturList);
        User user = User.findById(getAktifUser().user_id);
        renderArgs.put("user", user);
        List<UnitPengukuran> unitPengukuranList = UnitPengukuran.findAllActive();
        renderArgs.put("unitPengukuranList", unitPengukuranList);
        List<ProdukKategori> kategoriList = ProdukKategoriRepository.getProdukKategoriList(komoditas.id);
        renderArgs.put("kategoriList", kategoriList);
        List<String> jenisProduk = new ArrayList<>(Arrays.asList(Produk.TYPE_LOCAL, Produk.TYPE_IMPORT));
        renderArgs.put("jenisProduk", jenisProduk);
        render("katalog/ProdukCtr/stagging/editProdukStagingInfo.html", pg);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK_EDIT})
    public static void editProdukStagingSpec(Long id) {
        PembaruanStaging pmb = PembaruanStaging.findById(id);
        renderArgs.put("pmbstagging", pmb);
        ProdukStagging psg = pmb.getIfProdukStagging();
        Produk produk = Produk.findById(psg.produk_id);
        List<ProdukAtributValue> produkAtributValues = psg.getListProdukAtributValue();
        Map<String, String> attributeMap = produkAtributValues.stream()
                .collect(Collectors.toMap(ProdukAtributValue::getCategoriAttrId, a -> a.atribut_value));
        renderArgs.put("produk", produk);
        renderArgs.put("attrMapJson", CommonUtil.toJson(attributeMap));
        render("katalog/ProdukCtr/stagging/editProdukStagingSpec.html", produkAtributValues);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK_EDIT})
    public static void uploadPictures(File imageFile, Long pmbid) {
        Long produkId = 1l;
        Map<String, Object> result = new HashMap<String, Object>(1);
        List<ProdukGambar> files = new ArrayList<>();
        List<ProdukGambar> filesResult = new ArrayList<>();
        PembaruanStaging pmb = PembaruanStaging.findById(pmbid);
        ProdukStagging psg = pmb.getIfProdukStagging();
        files = psg.getListProdukImages();
        try {
            ProdukGambar gambar = new ProdukGambar(produkId);
            gambar.simpanGambarProdukForStagging(imageFile);
            gambar.active = 1;
            if (gambar.posisi_file == ProdukGambar._POSISI_FILE_LOKAL) {
                gambar.file_url = gambar.getLocalImageStorage() + gambar.file_sub_location + gambar.file_name;
            } else {
                gambar.file_url = gambar.getActiveUrl();
            }

            files.add(gambar);
            filesResult.add(gambar);
            result.put("files", filesResult);
            result.put("status", 1);
        } catch (Exception e) {
            e.printStackTrace();
            result.put("status", 0);
        }

        //fill list lampiran to produk stagging
        psg.object_json = psg.createGson().toJson(files);
        //save prodstagging to pembaruan stagging
        pmb.object_json = gson.toJson(psg);
        pmb.save();

        if (result.get("files") == null) {
            result.clear();
            result.put("status", 0);
            result.put("errorx", "upload failed");
        }

        renderJSON(result);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK_DELETE})
    public static void hapusProdukPermohonanPembaruan(long pembaruan_id, long produk_id){
        List<PembaruanStaging> ps = PembaruanStaging.getByPermohonanPembaruanId(pembaruan_id);
        for (PembaruanStaging stg : ps) {
            if(stg!=null){
                if(stg.getIfProdukStagging().produk_id == produk_id){
                    Logger.info("PEMBARUAAAN " + stg.id);
                    PembaruanStaging.deleteById(stg.id);
                    flash.success(Messages.get("notif.success.delete"));
                }
            }
        }

        listProdukPermohonanPembaruanStaging(pembaruan_id);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK_EDIT})
    public static void uploadLampiran(File file, Long pmbid) {
        Map<String, Object> result = new HashMap<>(1);
        List<DokumenInfo> files = new ArrayList<>();
        PembaruanStaging pmb = PembaruanStaging.findById(pmbid);
        ProdukStagging psg = pmb.getIfProdukStagging();
        List<ProdukLampiran> lampirans = psg.getListProdukLampiran();
        ProdukLampiran lampiran = new ProdukLampiran();
        BlobTable blob = null;
        try {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, lampiran.dok_id_attachment, ProdukLampiran.class.getName());

            DokumenInfo dokumenInfo = DokumenInfo.findByBlob(blob);
            dokumenInfo.setDownloadUrlFromBlob();
            files.add(dokumenInfo);
            result.put("files", files);
        } catch (Exception e) {
            Logger.info("gagal upload:" + e.getMessage());
            result.put("errorx", "upload failed");
        }
        if (null != blob) {
            lampiran.dok_id_attachment = blob.id;
            lampiran.file_name = blob.blb_nama_file;
            lampiran.original_file_name = blob.blb_nama_file;
            lampiran.dok_hash = blob.blb_hash;
            lampiran.file_size = blob.blb_ukuran;
            lampiran.active = 1;//active
            lampiran.produk_id = psg.produk_id.intValue();
            lampiran.created_by = AktifUser.getActiveUserId();
            if(null == lampiran.id) {
                lampiran.id = (0l - blob.id); //untuk membedakan new file
            }
            lampirans.add(lampiran);
        }

        //fill list lampiran to produk stagging
        psg.object_json = psg.createGson().toJson(lampirans);
        //save prodstagging to pembaruan stagging
        pmb.object_json = gson.toJson(psg);
        pmb.save();
        if (result.get("files") == null) {
            result.clear();
            result.put("errorx", "upload failed");
        }
        renderJSON(result);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK_EDIT})
    public static void hapusProdukImage(String fileId, Long pmbid) {
        Map<String, Object> result = new HashMap<>(1);
        PembaruanStaging pmb = PembaruanStaging.findById(pmbid);
        ProdukStagging psg = pmb.getIfProdukStagging();
        List<ProdukGambar> images = psg.getListProdukImages();
        ProdukGambar gambar = images.stream().filter(f -> f.file_name.equals(fileId)).findFirst().orElse(null);
        if (null != gambar) {
            String path = Play.applicationPath + "/public/files/image/produk_gambar/" + gambar.file_sub_location + gambar.file_name;
            File file = new File(path);
            Files.delete(file);
            result.put("status", 1);
            images.remove(gambar);
            //fill list lampiran
            psg.object_json = psg.createGson().toJson(images);
            //save prodstagging to pembaruan stagging
            pmb.object_json = gson.toJson(psg);
            pmb.save();
        } else {
            result.put("status", 0);
        }
        renderJSON(result);
    }

    //fileid adalah id file lampiran, lihat form
    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK_EDIT})
    public static void hapusProdukLampiran(Long fileId, Long pmbid) {

        PembaruanStaging pmb = PembaruanStaging.findById(pmbid);
        ProdukStagging psg = pmb.getIfProdukStagging();
        List<ProdukLampiran> lampirans = psg.getListProdukLampiran();
        ProdukLampiran lampDelete = lampirans.stream().filter(f -> f.id.equals(fileId)).findFirst().orElse(null);
        Map<String, Object> rjs = new HashMap<String, Object>(1);

        try {
            BlobTable blobTable = null;
            if(null != lampDelete.dok_id_attachment) {
                blobTable = BlobTable.findByBlobId(lampDelete.dok_id_attachment);//.findById(fileId, 0);
            }
            if (blobTable != null) {
                blobTable.preDelete();
                blobTable.delete();
            }
            //revoe content selcted
            lampirans.remove(lampDelete);
            //fill list lampiran
            psg.object_json = psg.createGson().toJson(lampirans);
            //save prodstagging to pembaruan stagging
            pmb.object_json = gson.toJson(psg);
            pmb.save();
            rjs.put("status", 1);
        } catch (Exception e) {
            rjs.put("status", 0);
        }
        renderJSON(rjs);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK_EDIT})
    public static void editProdukStagingLamp(Long id) {
        PembaruanStaging pmb = PembaruanStaging.findById(id);
        ProdukStagging psg = pmb.getIfProdukStagging();
        Produk produk = Produk.findById(psg.produk_id);
        produk.documents = psg.withDocuments();
        renderArgs.put("produk", produk);
        renderArgs.put("pmb", pmb);
        render("katalog/ProdukCtr/stagging/editProdukStagingLamp.html");
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK_EDIT})
    public static void editProdukStagingGambar(Long id) {
        PembaruanStaging pmb = PembaruanStaging.findById(id);
        ProdukStagging psg = pmb.getIfProdukStagging();
        Produk produk = Produk.findById(psg.produk_id);
        produk.pictures = psg.getListProdukImages();
        renderArgs.put("produk", produk);
        renderArgs.put("pmb", pmb);
        render("katalog/ProdukCtr/stagging/editProdukStagingGambar.html");
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK_EDIT})
    public static void editProdukStagingHarga(String ids, Long id) {
        editProdukStagingHargaSetParam(id);
        render("katalog/ProdukCtr/stagging/editProdukStagingHarga.html", ids);
    }

    /**
     * asep:
     * yang disimpan adalah :
     * - tanggal harga, disimpan di produkhargastagging field -> setelah approved di set juga di produk
     * - produkatributvalue disimpan di produkhargastagging field list -> yang lama di nonaktifkan
     * - ongkir disimpan di produkhargastagging field ongkir -> replace yang lama
     * - selectedlocation disimpan di produkharga selectedlocation, tapi jika nasional gak ada karena semua lokasi, jadi lihat
     * ongkirnya saja. disitu ada location prov dan kab
     */

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK_EDIT})
    public static void editSubmitProdukStagingHarga(Long pmbid, ProductForm model) {
        Logger.info(new Gson().toJson(model));
        // productModel.harga_tanggal = model.parseKursDate();
        // 19 juli, harga tangggal diambil dari inputan tab upload harga
        Date tglharga = DateTimeUtils.parseDdMmYyyy(model.tanggal_harga);
        String msgs = "";
        PembaruanStaging pmb = PembaruanStaging.findById(pmbid);
        ProdukStagging psg = pmb.getIfProdukStagging();
        ProdukHargaStagging phsg = psg.getIfProdukHargaStagging();

        Produk productModel = Produk.findById(psg.produk_id);
        Komoditas komoditas = Komoditas.findById(productModel.komoditas_id);
        List<KomoditasAtributHarga> hargaNonOngkir = KomoditasAtributHarga.findHargaNonOngkirByKomoditas(komoditas.id);
        Map<Long, BigDecimal> mapProductPrice = model.createMapFromProductPriceForm();

        if (komoditas.isNational()) {
            if (null == model.price) {
                flash.error("Lengkapi harga produk");
                //belum ada harga yang disimpan, rdirect
                editProdukStagingHargaSetParam(pmbid);
                String ids = null;
                render("katalog/ProdukCtr/stagging/editProdukStagingHarga.html", ids);
            }
        } else if (!komoditas.isNational() && model.hargaJson.isEmpty()) {
            msgs = ", Harga produk menggunakan data lama";
            //editProdukStagingWilyahJual(pmbid);
        }
        // save ongkir
        if (komoditas.isNeedDeliveryCost() && model.ongkirJson.isEmpty()) {//validasi ongkir
            //redirect not valid
            if (komoditas.isNational() && phsg.ongkir.isEmpty()) {
                flash.error("Lengkapi Ongkos Kirim produk");
                editProdukStagingHargaSetParam(pmbid);
                String ids = null;
                render("katalog/ProdukCtr/stagging/editProdukStagingHarga.html", ids);
            } else if (!komoditas.isNational() && phsg.ongkir.isEmpty()) {
                editProdukStagingWilyahJual(pmbid);
            }
        } else if (!komoditas.isRegency() && komoditas.isNeedDeliveryCost() && !model.ongkirJson.isEmpty()) {
            phsg.ongkir = CommonUtil.toJson(getOngkirProdukJson(model, komoditas.id));
        } else if (komoditas.isRegency() && komoditas.isNeedDeliveryCost() && !model.hargaJson.isEmpty()) {
            phsg.ongkir = model.getOngkirJsonArrayFromPrice();
        }

        List<ProdukHarga> phList = new ArrayList<>();
        for (KomoditasAtributHarga harga : hargaNonOngkir) {
            String hargaJsonResult = null;
            if (komoditas.isNational()) {
                HargaProdukJson hargaProdukJson = new HargaProdukJson();
                hargaProdukJson.harga = mapProductPrice.get(harga.id);
                hargaJsonResult = CommonUtil.toJson(hargaProdukJson);
            } else {
                // idalam fungsi gethargaproduk hanya memakai hargaJson, ongkir tidak di proses disana
                if (!TextUtils.isEmpty(model.hargaJson)) { // jika hargajson tidak null field yang ada harga dari form
                    List<HargaProdukJson> hargaProdukJson = ProdukService.getHargaProdukJson(model, harga, komoditas.isNeedDeliveryCost());
                    //notes nanti kalo apply apa tidak
//                if(harga.apakah_harga_utama){
//                    ProdukService.savePublicWilayahJual(model, hargaProdukJson, productModel);
//                }
                    hargaJsonResult = CommonUtil.toJson(hargaProdukJson);
                }
            }

            //LogUtil.d("hargaJsonResult",hargaJsonResult);

            ProdukHarga phnew = null;
            if(null == hargaJsonResult){
                phnew = phsg.produkHargas.stream().filter(f-> f.komoditas_harga_atribut_id.equals(harga.id)).findFirst().orElse(null);
                phnew.tanggal_dibuat = tglharga;
            }else{
                phnew = new ProdukHarga(
                        pmb.getIfProdukStagging().produk_id,
                        harga.id,
                        productModel.harga_kurs_id,
                        hargaJsonResult,
                        //model.parseKursDate(),
                        tglharga,
                        model.getXlsJson(),
                        model.checkedLocations);
            }
            phList.add(phnew);
        }


        List<ProdukAtributValue> patr = new ArrayList<>();
        Map<String, String> infoMap = ProdukService.getInformationFromJson(model.infoJson);
        if (!infoMap.isEmpty()) {
            for (Map.Entry<String, String> entry : infoMap.entrySet()) {
                ProdukAtributValue pnewatr = new ProdukAtributValue(productModel.id, entry.getValue(), Long.valueOf(entry.getKey()));
                patr.add(pnewatr);
            }
        }
        //save tgl harga
        phsg.harga_tanggal = tglharga;
        //set produkhargastaging produkhargalist,atributvalue
        phsg.produkHargas = phList;
        phsg.produkAtributValues = patr;
        //set produkstagging jsonprodukhargastagging
        psg.object_json = psg.createGson().toJson(phsg);
        //set pembaruanstagging objekjson produkstagging
        pmb.object_json = gson.toJson(psg);
        try {
            pmb.save();//save data
            flash.success("save data, oke"+msgs);
        } catch (Exception e) {
            flash.error("Error saving");
        }

        listProdukPermohonanPembaruanStaging(pmb.permohonan_pembaruan_id);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK_EDIT})
    private static void UpdateStggingHargaTgl(Long pmbid, Date tglharga){
        PembaruanStaging pmbS = new PembaruanStaging();
        ProdukStagging psgS = new ProdukStagging();
        ProdukHargaStagging phsgS = new ProdukHargaStagging();

        pmbS = PembaruanStaging.findById(pmbid);
        psgS = pmbS.getIfProdukStagging();
        phsgS =  psgS.getIfProdukHargaStagging();
        phsgS.harga_tanggal = tglharga;
        PembaruanStaging.UpdateHargaTglIfStagHArga(pmbid, tglharga);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK_EDIT})
    private static void editProdukStagingHargaSetParam(Long id) {
        User user = null;
        try {
            user = User.findById(getAktifUser().user_id);
        }catch (Exception e){
            HomeCtr.Home();
        }
            PembaruanStaging pmb = PembaruanStaging.findById(id);
            ProdukStagging psg = pmb.getIfProdukStagging();
            Produk produk = Produk.findById(psg.produk_id);
            Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
//        List<ProdukAtributValue> produkAtributValues = ProdukAtributValue.findByProdukId(produk.id);
//        Map<String, String> attributeMap = produkAtributValues.stream()
//                .collect(Collectors.toMap(ProdukAtributValue::getCategoriAttrId, a -> a.atribut_value));
//        Long[] wilJualId = new Long[0];
//        List<Long> wilJualIds = new ArrayList<>();
//        if (komoditas.isRegency()) {
//            List<ProdukWilayahJualKabupaten> wilJual = ProdukWilayahJualKabupaten.find("produk_id = ?", produk.id).fetch();
//            wilJualIds = new ArrayList<>();
//            for (ProdukWilayahJualKabupaten p : wilJual) {
//                wilJualIds.add(p.id);
//            }
//            wilJualId = wilJualIds.toArray(new Long[wilJualIds.size()]);
//        } else if (komoditas.isProvince()) {
//            List<ProdukWilayahJualProvinsi> wilJual = ProdukWilayahJualProvinsi.find("produk_id = ?", produk.id).fetch();
//            wilJualIds = new ArrayList<>();
//            for (ProdukWilayahJualProvinsi p : wilJual) {
//                wilJualIds.add(p.id);
//            }
//            wilJualId = wilJualIds.toArray(new Long[wilJualIds.size()]);
//        }

        ProdukHargaStagging stg = psg.getIfProdukHargaStagging();
        //get from stagging
        produk.harga_tanggal = stg.harga_tanggal;
//        List<ProdukHarga> listProdukHarga = ProdukHarga.findRiwayatByProdukId(produk.id);
//        ProdukHarga produkHarga = ProdukHarga.getByProductId(produk.id);
        List<ProdukHarga> listProdukHarga = stg.produkHargas.stream()
                .sorted(Comparator.comparing(ProdukHarga::getTanggalDibuat)
                        .reversed()).collect(Collectors.toList());
        listProdukHarga.forEach(ProdukHarga::setNama_kursFdb);
        ProdukHarga produkHarga = listProdukHarga.get(0);


        Map<String, List<ProdukHarga>> groupHarga = listProdukHarga.stream()
                .collect(
                        Collectors.groupingBy(
                                x -> x.getStandarDateString()
                        )
                );
        NavigableMap<String, List<ProdukHarga>> riwayatProdukHarga = new TreeMap<>(groupHarga).descendingMap();
        Map<Long, List<RiwayatHargaProdukJson>> groupRiwayat = komoditas.isNational() ? null : listProdukHarga.stream()
                .map(x -> x.riwayatHarga())
                .flatMap(List::stream)
                .collect(
                        Collectors.groupingBy(
                                x -> komoditas.isProvince() ? x.getProvinsiId() : x.getKabupatenId()
                        )
                );
        NavigableMap<Long, List<RiwayatHargaProdukJson>> riwayatProdukHargaPerLokasi = groupRiwayat == null ? null : new TreeMap<>(groupRiwayat).descendingMap();


        List<String> atributHargaNamaList = KomoditasAtributHarga.findNamaNotOngkirByKomoditas(komoditas.id);
        String atributHargaJson = new Gson().toJson(atributHargaNamaList);
        List<KomoditasAtributHarga> atributHargaList = KomoditasAtributHarga.findHargaNonOngkirByKomoditas(komoditas.id);

        List<Kurs> kursList = Kurs.findAll();
        Kurs kur = kursList.stream().filter(i -> "IDR".equals(i.nama_kurs)).findAny().orElse(null);

        String checkedLocationsJson = "{}";
        if (!komoditas.isNational()) {
            checkedLocationsJson = produkHarga.checked_locations_json;
        }
        LinkedTreeMap checkedLocationMap = (new Gson()).fromJson(checkedLocationsJson, LinkedTreeMap.class);
        List<String> checkedLocationList = new ArrayList<String>();
        Map<Long, String> checkedLocations = new HashMap<Long, String>();
        if (komoditas.isRegency()) {
            checkedLocationList = new ArrayList<String>(((Map) (checkedLocationMap.get("regency"))).keySet());
            List<Kabupaten> kabupatenList = Kabupaten.findByIds(checkedLocationList);
            checkedLocations = kabupatenList.stream().collect(Collectors.toMap(Kabupaten::getId, Kabupaten::getNama));
        } else if (komoditas.isProvince()) {
            checkedLocationList = new ArrayList<String>(((Map) (checkedLocationMap.get("province"))).keySet());
            List<Provinsi> provinsiList = Provinsi.findByIds(checkedLocationList);
            checkedLocations = provinsiList.stream().collect(Collectors.toMap(Provinsi::getId, Provinsi::getNama));
        }
        renderArgs.put("checkedLocations", checkedLocations);
        renderArgs.put("user", user);
        renderArgs.put("atributHargaJson", atributHargaJson);
        renderArgs.put("atributHargaList", atributHargaList);
        renderArgs.put("provKab", WilayahRepository.instance.getAllWilayah());
        renderArgs.put("riwayatProdukHarga", riwayatProdukHarga);
        renderArgs.put("riwayatProdukHargaPerLokasi", riwayatProdukHargaPerLokasi);
        renderArgs.put("produkHarga", produkHarga);
        renderArgs.put("listProdukHarga", listProdukHarga);
        //renderArgs.put("attrMapJson", CommonUtil.toJson(attributeMap));
        renderArgs.put("produk", produk);
        renderArgs.put("kursList", kursList);
        renderArgs.put("kur", kur);
        renderArgs.put("komoditas", komoditas);
        renderArgs.put("pmb", pmb);
        renderArgs.put("provKab", WilayahRepository.instance.getAllWilayah());
        //render("katalog/ProdukCtr/stagging/editProdukStagingHarga.html");
    }

    //ids value wilayah yang mau di set ke harga, chckedlocation adalah id wilaya untuk edit nanti yang mau di checked ketika edit
    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK_EDIT})
    public static void createSubmitWilayahJual(String ids, long pmbid, String checked_Locations) {
        editProdukStagingHargaSetParam(pmbid);
        render("katalog/ProdukCtr/stagging/editProdukStagingHarga.html", ids, checked_Locations);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK_EDIT})
    public static void editProdukStagingWilyahJual(Long id) {
        PembaruanStaging pmb = PembaruanStaging.findById(id);
        ProdukStagging psg = pmb.getIfProdukStagging();
        Produk produk = Produk.findById(psg.produk_id);

        Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
        //jika komoditas harga nasional maka semua daerah, langsung saja ke input harga
        if (komoditas.isNational()) {
            editProdukStagingHarga(null, id);//ids empty list
        }

        //ProdukHarga produkHarga = ProdukHarga.getByProductId(produk.id);
        ProdukHargaStagging stg = psg.getIfProdukHargaStagging();
        List<ProdukHarga> listProdukHarga = stg.produkHargas.stream().sorted(Comparator.comparing(ProdukHarga::getTanggalDibuat).reversed()).collect(Collectors.toList());
        ProdukHarga produkHarga = listProdukHarga.get(0);

        String checkedLocationsJson = "{}";
        if (!komoditas.isNational()) {
            checkedLocationsJson = produkHarga.checked_locations_json;
        }
        LinkedTreeMap checkedLocationMap = (new Gson()).fromJson(checkedLocationsJson, LinkedTreeMap.class);
        List<String> checkedLocationList = new ArrayList<String>();
        Map<Long, String> checkedLocations = new HashMap<Long, String>();
        if (komoditas.isRegency()) {
            checkedLocationList = new ArrayList<String>(((Map) (checkedLocationMap.get("regency"))).keySet());
            List<Kabupaten> kabupatenList = Kabupaten.findByIds(checkedLocationList);
            checkedLocations = kabupatenList.stream().collect(Collectors.toMap(Kabupaten::getId, Kabupaten::getNama));
        } else if (komoditas.isProvince()) {
            checkedLocationList = new ArrayList<String>(((Map) (checkedLocationMap.get("province"))).keySet());
            List<Provinsi> provinsiList = Provinsi.findByIds(checkedLocationList);
            checkedLocations = provinsiList.stream().collect(Collectors.toMap(Provinsi::getId, Provinsi::getNama));
        }

        renderArgs.put("checkedLocations", checkedLocations);
        renderArgs.put("produkHarga", produkHarga);
        renderArgs.put("produk", produk);
        renderArgs.put("komoditas", komoditas);
        renderArgs.put("pmb", pmb);
        render("katalog/ProdukCtr/stagging/editProdukStagingWilyahJual.html");
    }

    public static boolean allowToAccessMenuPermohonanPembaruan() {
        //flash.error("Anda tidak diizinkan");
        if (!AktifUser.getAktifUser().isProvider()) {
            index();
            return false;
        } else {
            return true;
        }
    }

    public static boolean allowToShowMenuPermohonanPembaruan(PermohonanPembaruan pmp) {
        //flash.error("Anda tidak diizinkan");
        if (!AktifUser.getAktifUser().user_id.equals(new Long(pmp.created_by))) {
            index();
            return false;
        } else {
            return true;
        }
    }

    public static boolean allowToAccess(PermohonanPembaruan pmp){
        AktifUser user = AktifUser.getAktifUser();

        if(user.isProvider()){
            if(user.user_id.equals(new Long(pmp.created_by))){
                return true;
            }else{
                index();
                return false;
            }
        }else{
            return true;
        }
    }

    @AllowAccess({Acl.COM_PENYEDIA_DISTRIBUTOR_EDIT})
    public static void editDistributor(long id) {

//        PenyediaDistributor distributor  = PenyediaDistributor.findById(id);
//        List<PenyediaDistributorRepresentatif> representatifList = PenyediaDistributorRepresentatif.findByDistributor(id);
//        render("katalog/ProdukCtr/stagging/distributor/form.html", distributor, pid, representatifList, idPembaruan);

        PembaruanStaging pmb = PembaruanStaging.findById(id);
        renderArgs.put("pmbstagging", pmb);
//        ProdukStagging psg = pmb.getIfProdukStagging();

        DistributorStagging dstg = pmb.getIfDistributorStagging();
//        Produk produk = Produk.findById(psg.produk_id);
        PenyediaDistributor distributor = dstg.distributor;
//        List<ProdukAtributValue> produkAtributValues = psg.getListProdukAtributValue();
        List<PenyediaDistributorRepresentatif> representatifList = dstg.listRepresentatif;
//        Map<String, String> attributeMap = produkAtributValues.stream()
//                .collect(Collectors.toMap(PenyediaDistributorRepresentatif::, a -> a.atribut_value));
        renderArgs.put("distributor", distributor);
        renderArgs.put("representatifList", representatifList);
        renderArgs.put("idPermohonanPembaruan", pmb.permohonan_pembaruan_id);
        render("katalog/ProdukCtr/stagging/distributor/form.html");
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DISTRIBUTOR_ADD})
    public static void submitDistributorPermohonan(Long pmb, long[] idDistributor) {
//        PermohonanPembaruan pmbo = PermohonanPembaruan.findById(pmb);
        PermohonanPembaruan permohonanPembaruan = PermohonanPembaruan.findById(pmb);
        permohonanPembaruan.status = permohonanPembaruan.STATUS_PERSIAPAN;
        permohonanPembaruan.save();

        DistributorStagging dstg = null;
        if (null != pmb && null != idDistributor) {
            Logger.info("Stagging: Distributor");
            for (long d : idDistributor) {
                PenyediaDistributor pd = PenyediaDistributor.findById(d);
                List<PenyediaDistributorRepresentatif> rep = PenyediaDistributorRepresentatif.findByDistributor(pd.id);

                dstg = new DistributorStagging();
                dstg.distributor = pd;
                dstg.listRepresentatif = rep;

                PembaruanStaging stg = new PembaruanStaging();
                stg.permohonan_pembaruan_id = pmb;
                stg.class_name = dstg.getClass().getCanonicalName();
                stg.isList = false;
                stg.object_json = gson.toJson(dstg);
                stg.active = true;
                stg.save();
            }
        }
        index();
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DISTRIBUTOR})
    public static void listDistributorPermohonanPembaruanStaging(Long id) {
        boolean showButtonKirim = false, showButtonSetujuTolakData = false, showButtonSetujuTolakDisposisi = false;
        String statusPmb = "";

        Long pid = new Long(0);

        PermohonanPembaruan pmb = PermohonanPembaruan.findById(id);
        allowToAccess(pmb);
        statusPmb = pmb.status;

        if (statusPmb.equalsIgnoreCase(pmb.STATUS_PERSIAPAN) || statusPmb.equalsIgnoreCase(pmb.STATUS_VERIFIKASI_TOLAK)) {
            showButtonKirim = true;
        }

        PembaruanStatus ps = PembaruanStatus.getLatestStatusByPermohonanId(id);
        if ((null == getAktifUser().jabatan || getAktifUser().jabatan.isEmpty()) && (getAktifUser().isAdmin() || getAktifUser().isAdminLokalSektoral())){
            if (ps!=null && ps.status_permohonan.equalsIgnoreCase(ps.STATUS_PERMOHONAN_PERSIAPAN)
                    && ps.to_status.equalsIgnoreCase(ps.STATUS_ADMIN) && ps.from_status.equalsIgnoreCase(ps.STATUS_PENYEDIA)){
                showButtonSetujuTolakData = true;
            }
            if (ps!=null && ps.status_permohonan.equalsIgnoreCase(ps.STATUS_PERMOHONAN_LENGKAP)
                    && ps.to_status.equalsIgnoreCase(ps.STATUS_ADMIN) && ps.from_status.equalsIgnoreCase(ps.STATUS_ADMIN)){
                showButtonSetujuTolakDisposisi = true;
            }
        }

        if (id != null) {
            if(getAktifUser().is_penyedia){
                Penyedia penyedia = Penyedia.getProviderByUserId(AktifUser.getAktifUser().user_id);
                pid = penyedia.id;
                renderArgs.put("penyediaId", pid);
            }else {
                renderArgs.put("penyediaId", pid);
            }

            List<PembaruanStaging> lstPstg = PembaruanStaging.getAllDistributorByPermohonanPembaruanId(id);

            if (lstPstg.size() == 0) {
                showButtonKirim = false;
            }

            lstPstg = lstPstg.stream().filter(f -> f.getPermohonanPembaruan()
                    .tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_DISTRIBUTOR)).collect(Collectors.toList());

//        //ambil id as sting
            List<String> sStr = new ArrayList<>();
            try {
                sStr = Arrays.asList(lstPstg.stream().map(PembaruanStaging::getDistributorIdIfTypeDistributor).collect(Collectors.joining(",")).split(","));
                Logger.info("SSTRRR " + sStr);
            } catch (Exception e) {
            }

            List<User> sendToUser = new ArrayList<>();
            //ambil data user
            if ((pmb.status.equalsIgnoreCase(pmb.STATUS_VERIFIKASI_SETUJU))){
                sendToUser = PermohonanPembaruan.findUserToSend(ps.to_status, pmb.status, pmb.jenis_komoditas, pmb.kldi_id);
            }

            Long idPembaruan = id;
            renderArgs.put("showButtonKirim", showButtonKirim);
            renderArgs.put("idsDist", sStr);
            renderArgs.put("idPembaruan", idPembaruan);
            renderArgs.put("showButtonSetujuTolakDisposisi",showButtonSetujuTolakDisposisi);
            renderArgs.put("showButtonSetujuTolakData",showButtonSetujuTolakData);
            render("katalog/ProdukCtr/stagging/distributor/listDistributorPermohonanPembaruanStaging.html", sendToUser);

        }
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DISTRIBUTOR_ADD})
    public static void createDistributor(long pmbId){
        PenyediaDistributor distributor = new PenyediaDistributor();
        PermohonanPembaruan pmb = PermohonanPembaruan.findById(pmbId);
//        PembaruanStaging pmbstg = new PembaruanStaging();
//        renderArgs.put("pmbstagging", pmbstg);
//        renderArgs.put("idPermohonanPembaruan", pmbId);
        renderArgs.put("pid", pmb.penyedia_id);
        render("katalog/ProdukCtr/stagging/distributor/create.html", distributor, pmbId);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DISTRIBUTOR_ADD})
    public static void submitCreateDistributor(PenyediaDistributor distributor, String[] nama_representatif,
                                  String[] telp_representatif, String[] email_representatif,
                                  String username, Long userId, String rknDetailJson,long pmbId){

        long pid = distributor.penyedia_id;

        if(nama_representatif == null){
            params.flash();
            flash.error("Representatif belum diisi.");
            createDistributor(pmbId);
        }

        List<PenyediaDistributorRepresentatif> representatifList = PenyediaDistributorRepresentatif.setList(nama_representatif, telp_representatif, email_representatif);

        List<PenyediaDistributor> dist = new ArrayList<>();
        dist = PenyediaDistributor.findByPenyediaAll(pid);

        boolean isEdit = distributor.id != null;

        Logger.info("isedit " + isEdit);

        if(distributor.id == null && username.isEmpty()){
            params.flash();
            flash.error("Anda belum memilih user untuk menjadi distributor.");

            render("katalog/ProdukCtr/stagging/distributor/create.html", distributor, pmbId, pid, username, representatifList);
        }else{
            validation.valid(distributor);
            if(validation.hasErrors()) {
                params.flash();
                flash.error("Simpan Penyedia gagal. Silakan cek kembali isian Anda di tab Informasi Distributor.");

                render("katalog/ProdukCtr/stagging/distributor/create.html", distributor, pmbId, pid, username, representatifList);
            }else{

                if(dist != null && !isEdit){
                    List<PenyediaDistributor> d = new ArrayList<>();
                    d = PenyediaDistributor.findByPenyediaExist(distributor.user_id, pid);

                    if (d.size() > 0){
                        params.flash();
                        flash.error("Distributor sudah ada");
                        createDistributor(pmbId);
                    }
                }

                distributor.active = true;
                distributor.minta_disetujui = 1L;
                distributor.setuju_tolak = "";
                long distributor_id = 0L;

                if(distributor.id != null){
                    PenyediaDistributorRepresentatif.deleteByDistributor(distributor.id);
                    distributor.save();
                    distributor_id = distributor.id;
                }else{
                    distributor_id = distributor.save();
                }

                for(PenyediaDistributorRepresentatif rep : representatifList){
                    rep.penyedia_distributor_id = distributor_id;
                    rep.active = true;
                    rep.save();
                }

                if(!isEdit){
                    flash.success("Data Distributor berhasil ditambahkan. Penambahan data memerlukan persetujuan admin");
                }else {
                    flash.success("Data Distributor berhasil diperbarui. Perubahan data memerlukan persetujuan admin");
                }

                Logger.info("pid yg dikirim " + pid);

                formPermohonan(pmbId);
            }
        }
    }
    
    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DISTRIBUTOR_ADD, Acl.COM_PERMOHONAN_PEMBARUAN_DISTRIBUTOR_EDIT})
    public static void submitEditDistributor(PenyediaDistributor distributor, List<PenyediaDistributorRepresentatif> representatifList, String[] nama_representatif,
                                             String[] telp_representatif, String[] email_representatif,
                                             String username, Long userId, String rknDetailJson, long pmbstgId, long pmbId, long idPermohonanPembaruan, long[] id_representatif) {
        PermohonanPembaruan pmb = PermohonanPembaruan.findById(idPermohonanPembaruan);
        PembaruanStaging pg = PembaruanStaging.findById(pmbstgId);
//        ProdukStagging psg = pg.getIfProdukStagging();
        DistributorStagging dstg = new DistributorStagging();
        List<PenyediaDistributorRepresentatif> listRepresentatif = new ArrayList<>();
        PenyediaDistributor distStag = new PenyediaDistributor();

        Logger.info("DISTRIBUTOR " + distributor);

        if (pg != null){
//            kondisi edit distributor
            dstg = pg.getIfDistributorStagging();
            PenyediaDistributor stagDistEdit = dstg.distributor;
            //edit set data
            stagDistEdit.setFromEditStaging(distributor);
            distStag = stagDistEdit;

        }else {
//            kondisi tambah distributor baru
//            buat dan simpan permohonan pembaruan staging
            //kondisi save new
            Penyedia penyedia = Penyedia.getProviderByUserId(AktifUser.getAktifUser().user_id);
            distributor.penyedia_id = penyedia.id;//set penyedia id ketika data baru
            distStag.id = -0l; // new data, untuk handle listing data representatif
            distributor.id = -0l;
            distStag = distributor;
            distStag.penyedia_id = penyedia.id;
            distStag.active = true;
            distStag.minta_disetujui = 0L;
            distStag.setuju_tolak = "Setuju";
            //distributor_id = distStag.save();//tidak boleh saving db
            //simpan refrdetaildistributoresentatif pindah kebawah
        }

        dstg.distributor = distStag;

        listRepresentatif = PenyediaDistributorRepresentatif.setListForStagging(id_representatif,nama_representatif, telp_representatif, email_representatif, distributor.id);
        dstg.listRepresentatif = listRepresentatif;

        //simpan produk di edit
        Logger.info("list rep " + dstg.listRepresentatif);

        //simpan ke stagging dalam json
        if(pg != null){
            pg.object_json = gson.toJson(dstg);
        }else {
            //new pembaruan stagging
            pg = new PembaruanStaging();
            pg.permohonan_pembaruan_id = idPermohonanPembaruan;
            pg.class_name = dstg.getClass().getCanonicalName();
            pg.isList = false;
            pg.object_json = gson.toJson(dstg);
            pg.active = true;
            pg.object_json = gson.toJson(dstg);
        }

        //simpan stagging
        int id = pg.save();
        Logger.info("pg:"+id);
        flash.success(Messages.get("notif.success.submit"));
        listDistributorPermohonanPembaruanStaging(pg.permohonan_pembaruan_id);

    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DISTRIBUTOR_DELETE})
    public static void hapusDistributorPermohonanPembaruan(long id){
        PembaruanStaging ps = PembaruanStaging.findById(id);

        if(ps!=null){
            PembaruanStaging.deleteById(id);
            flash.success(Messages.get("notif.success.delete"));
        }else{
            flash.error(Messages.get("notif.failed.delete"));
        }

        listDistributorPermohonanPembaruanStaging(ps.permohonan_pembaruan_id);
    }

    public static void perluVerifikasiPihakKetiga(long permId, String alasan, String setujuTolak) {
        String msg = "";
        PembaruanStatus pembaruanStatus = PembaruanStatus.getLatestStatusByPermohonanId(permId);
        PermohonanPembaruan permohonanPembaruan = PermohonanPembaruan.findById(permId);
        PembaruanRiwayat pembaruanRiwayat = new PembaruanRiwayat();
//        redirect("katalog.PermohonanPembaruanCtr.listDisposisi", permId);

        pembaruanStatus.status_permohonan = setujuTolak.equalsIgnoreCase("ya") ?
                pembaruanStatus.STATUS_PERMOHONAN_VERIFIKASI_PIHAK_KETIGA : pembaruanStatus.STATUS_PERMOHONAN_KLARIFIKASI;
        pembaruanStatus.from_status = pembaruanStatus.STATUS_KASI;
        pembaruanStatus.to_status = pembaruanStatus.STATUS_ADMIN;

        permohonanPembaruan.status = setujuTolak.equalsIgnoreCase("ya") ?
                permohonanPembaruan.STATUS_VERIFIKASI_PIHAK_KETIGA : permohonanPembaruan.STATUS_KLARIFIKASI;

        int saveData = permohonanPembaruan.save();

        msg = "Data";

        if (saveData > 0) {
            pembaruanRiwayat.addKlarifikasiKetiga(permId, pembaruanStatus.STATUS_KASI, setujuTolak, alasan);

            pembaruanStatus.deskripsi = alasan;
            pembaruanStatus.save();

            flash.success(msg + " berhasil disimpan.");
        } else {
            flash.error(msg + " gagal disimpan.");
        }

        /*if (setujuTolak.equalsIgnoreCase("ya")){
            redirect("katalog.PermohonanPembaruanCtr.listDisposisi", permId);
        } else {*/
            redirect("katalog.PermohonanPembaruanCtr.index");
        /*}*/

    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KLARIFIKASI})
    public static void listKlarifikasi(long id) {
        boolean showButtonTambah = false;
        boolean showButtonSetuju = false;
        boolean showButtonBatal = false;
        boolean showButtonUbah = false;
        boolean isNowAfterSchedule = false;
        String statusPmb = "", jabatan = "", latestStatus = "", jadwal = "";
        int jumlahTolakKlarif = 0;

        List<PembaruanKlarifikasi> klarifikasi = PembaruanKlarifikasi.findByPermohonanId(id);
        PermohonanPembaruan permohonanPembaruan = PermohonanPembaruan.findById(id);
        allowToAccess(permohonanPembaruan);
        PembaruanStatus ps = PembaruanStatus.getLatestStatusByPermohonanId(id);
        statusPmb = permohonanPembaruan.status;

        AktifUser au = AktifUser.getAktifUser();

        jabatan = au.jabatan != null ? au.jabatan : "";
        latestStatus = ps.status_permohonan;
        if (statusPmb.equalsIgnoreCase(permohonanPembaruan.STATUS_KLARIFIKASI) && latestStatus.equalsIgnoreCase(ps.STATUS_PERMOHONAN_KLARIFIKASI) &&
                ps.to_status.equalsIgnoreCase(ps.STATUS_ADMIN) && (jabatan.isEmpty() && (au.isAdmin() || au.isAdminLokalSektoral()))){
                showButtonUbah = true;
            if (permohonanPembaruan.butuh_klarifikasi_ulang > 0){
                if (permohonanPembaruan.klarifikasi_ulang_ke == klarifikasi.size()) {
                    showButtonTambah = true;
                    showButtonSetuju = true;
                }
            } else {
                if (klarifikasi != null && klarifikasi.size() == 0) {
                    showButtonTambah = true;
                }
            }
        }

        if (null != klarifikasi && klarifikasi.size()>0){
            for(int i = 0; i < klarifikasi.size(); i++){
                if (null == klarifikasi.get(i).setuju_tolak){
                    //jumlahTolakKlarif = jumlahTolakKlarif+1;
                    Date nows = new Timestamp(new java.util.Date().getTime());
                    String batasJadwal = klarifikasi.get(i).tanggal_kegiatan+" "+klarifikasi.get(i).waktu_kegiatan;
                    Date dateJadwal = null;
                    //2019-11-27	14:45:00
                    try {
                        dateJadwal = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(batasJadwal);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    isNowAfterSchedule = nows.after(dateJadwal);

                    jadwal = ""+nows+" | "+dateJadwal+" | "+isNowAfterSchedule;

                    if (isNowAfterSchedule){
                        klarifikasi.get(i).status_jadwal = 0;
                    } else {
                        klarifikasi.get(i).status_jadwal = -1;
                    }
                } else {
                    isNowAfterSchedule = false;
                    if (klarifikasi.get(i).setuju_tolak.equalsIgnoreCase(permohonanPembaruan.STATUS_SETUJU)){
                        klarifikasi.get(i).status_jadwal = 1;
                    } else {
                        klarifikasi.get(i).status_jadwal = 2;
                    }
                }

            }
        }

        showButtonBatal = jumlahTolakKlarif > 0 && jumlahTolakKlarif == klarifikasi.size() ? true : false;

        renderArgs.put("showButtonTambah", showButtonTambah);
        renderArgs.put("showButtonSetuju", showButtonSetuju);
        renderArgs.put("showButtonBatal", showButtonBatal);
        renderArgs.put("showButtonUbah", showButtonUbah);
        renderArgs.put("isNowAfterSchedule", isNowAfterSchedule);
        render("katalog/ProdukCtr/permohonan/listPermohonanKlarifikasi.html", klarifikasi, id, jadwal);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KLARIFIKASI_ADD})
    public static void createKlarifikasi(long permId) {
        render("katalog/ProdukCtr/permohonan/createKlarifikasi.html", permId);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KLARIFIKASI_ADD})
    public static void createKlarifikasiSubmit(PembaruanKlarifikasi klarif, String tanggal, String waktu, File file) {
        try {
            Date tanggal_format = new SimpleDateFormat("dd-MM-yyyy").parse(tanggal);
            String tempWaktu = waktu.replace(".", ":");
            Date waktu_format = new SimpleDateFormat("kk:mm").parse(tempWaktu);

            klarif.tanggal_kegiatan = tanggal_format;
            klarif.waktu_kegiatan = waktu_format;
            klarif.active = 1;
            boolean msg = klarif.klarifikasiSubmit(klarif, file);
            if (msg) {
                PermohonanPembaruan permohonanPembaruan = PermohonanPembaruan.findById(klarif.permohonan_pembaruan_id);
                permohonanPembaruan.butuh_konfirmasi_kehadiran = 1;
                permohonanPembaruan.save();

                String email = getPenyediaEmail(permohonanPembaruan.penyedia_id);
                Penyedia penyedia = Penyedia.findById(permohonanPembaruan.penyedia_id);
                PermohonanPembaruan.sendMail(email, klarif, permohonanPembaruan, penyedia);
                flash.success("Data berhasil disimpan");
            } else {
                flash.error("Data tidak berhasil disimpan");
            }

            redirect("katalog.PermohonanPembaruanCtr.listKlarifikasi", klarif.permohonan_pembaruan_id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getPenyediaEmail(Long penyediaId){
        String mail = "";
        Penyedia penyedia = Penyedia.findById(penyediaId);
        if (null != penyedia){
            if(Play.mode.isProd()){
                mail = penyedia.email;
            }
        }
        return mail;
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KLARIFIKASI_EDIT})
    public static void editKlarifikasi(long id) {
        PembaruanKlarifikasi klarif = PembaruanKlarifikasi.findById(id);
        DateFormat format_tanggal = new SimpleDateFormat("dd-MM-yyyy");
        String tanggal_kegiatan = format_tanggal.format(klarif.tanggal_kegiatan);
        render("katalog/ProdukCtr/permohonan/editKlarifikasi.html", klarif, tanggal_kegiatan);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KLARIFIKASI_EDIT})
    public static void editKlarifikasiSubmit(PembaruanKlarifikasi klarif, String tanggal, String waktu, File file) {
        try {
            Date tanggal_format = new SimpleDateFormat("dd-MM-yyyy").parse(tanggal);
            String tempWaktu = waktu.replace(".", ":");
            Date waktu_format = new SimpleDateFormat("kk:mm").parse(tempWaktu);

            PembaruanKlarifikasi klarifikasi = PembaruanKlarifikasi.findById(klarif.id);
            klarifikasi.tanggal_kegiatan = tanggal_format;
            klarifikasi.waktu_kegiatan = waktu_format;
            klarifikasi.lokasi = klarif.lokasi;
            klarifikasi.judul_kegiatan = klarif.judul_kegiatan;
            klarifikasi.keterangan = klarif.keterangan;
            klarifikasi.active = 1;

            boolean msg = klarifikasi.klarifikasiSubmit(klarifikasi, file);
            if (msg) {
                flash.success("Data berhasil diubah");
            } else {
                flash.error("Data tidak berhasil diubah");
            }

            redirect("katalog.PermohonanPembaruanCtr.listKlarifikasi", klarifikasi.permohonan_pembaruan_id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KLARIFIKASI})
    public static void setujuTolakKehadiranKlarifikasi(long pmbId, long jadwalId, String setujuTolakKehadiran){
        String msg = "";
        PembaruanRiwayat pembaruanRiwayat = new PembaruanRiwayat();
        PembaruanKlarifikasi pembaruanKlarifikasi = PembaruanKlarifikasi.findById(jadwalId);
        PermohonanPembaruan permohonanPembaruan = PermohonanPembaruan.findById(pmbId);

        pembaruanKlarifikasi.setuju_tolak = setujuTolakKehadiran;
        pembaruanKlarifikasi.setuju_tolak_oleh = AktifUser.getAktifUser().user_id;
        pembaruanKlarifikasi.setuju_tolak_tanggal = new Timestamp(new java.util.Date().getTime());
        int saveKonfirm = pembaruanKlarifikasi.save();

        msg = "Konfirmasi kehadiran klarifikasi";

        if (saveKonfirm > 0){
            if (setujuTolakKehadiran.equalsIgnoreCase(permohonanPembaruan.STATUS_TOLAK)){
                permohonanPembaruan.status = permohonanPembaruan.STATUS_BATAL;
            }
            permohonanPembaruan.butuh_konfirmasi_kehadiran = 0;
            permohonanPembaruan.save();
            pembaruanRiwayat.addKonfirmasiKehadiranKlarif(pmbId,PermohonanPembaruan.STATUS_PENYEDIA,setujuTolakKehadiran);

            if (setujuTolakKehadiran.equalsIgnoreCase(permohonanPembaruan.STATUS_TOLAK)){
                pembaruanRiwayat.addBatalkanPermohonanKarnaTidakKlarif(pmbId);
            }
            flash.success(msg + " berhasil disimpan.");
        } else {
            flash.error(msg + " gagal disimpan.");
        }


        redirect("katalog.PermohonanPembaruanCtr.listKlarifikasi", pmbId);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KLARIFIKASI})
    public static void terimaTolakKlarifikasi(long permId, String alasan, String tipe, String setujuTolak) {
        String msg = "";
        PembaruanStatus pembaruanStatus = PembaruanStatus.getLatestStatusByPermohonanId(permId);
        PermohonanPembaruan permohonanPembaruan = PermohonanPembaruan.findById(permId);
        PembaruanRiwayat pembaruanRiwayat = new PembaruanRiwayat();
        PembaruanStaging pembaruanStaging = PembaruanStaging.getById(permId);// ini maksute adalah get by permohonan_id
//        redirect("katalog.PermohonanPembaruanCtr.listDisposisi", permId);

        pembaruanStatus.status_permohonan = setujuTolak.equalsIgnoreCase(permohonanPembaruan.STATUS_KLARIFIKASI_TERIMA) ?
                pembaruanStatus.STATUS_PERMOHONAN_KLARIFIKASI_DITERIMA : pembaruanStatus.STATUS_PERMOHONAN_KLARIFIKASI_DITOLAK;
        pembaruanStatus.from_status = pembaruanStatus.STATUS_ADMIN;
        pembaruanStatus.to_status = pembaruanStatus.STATUS_ADMIN;

        permohonanPembaruan.klarif_user_id = AktifUser.getAktifUser().user_id;
        permohonanPembaruan.status = permohonanPembaruan.STATUS_HASIL;
        permohonanPembaruan.klarif_terima_tolak = setujuTolak;
        permohonanPembaruan.klarif_terima_tolak_oleh = AktifUser.getAktifUser().user_id;
        permohonanPembaruan.klarif_terima_tolak_alasan = alasan;
        permohonanPembaruan.klarif_terima_tolak_tanggal = new Timestamp(new java.util.Date().getTime());

        int saveData = permohonanPembaruan.save();

        msg = "Proses persetujuan perubahan data";

        if (saveData > 0) {
            if(permohonanPembaruan.tipe_permohonan.equalsIgnoreCase(permohonanPembaruan.TIPE_PERMOHONAN_PROFIL)){
                pembaruanStaging.apakah_disetujui = setujuTolak.equalsIgnoreCase(permohonanPembaruan.STATUS_KLARIFIKASI_TERIMA) ? 1 : 0;
                pembaruanStaging.tgl_disetujui = new Timestamp(new Date().getTime());
                pembaruanStaging.save();
            }

            pembaruanRiwayat.addKlarifikasi(permId, pembaruanStatus.STATUS_ADMIN, setujuTolak, alasan);

            pembaruanStatus.deskripsi = alasan;
            pembaruanStatus.save();

            flash.success(msg + " berhasil disimpan.");
        } else {
            flash.error(msg + " gagal disimpan.");
        }

        redirect("katalog.PermohonanPembaruanCtr.listHasilKlarifikasi", permId);
    }

    public static void selesaikanApprovalProdukAtauDistributor(long permId) {
        String msg = "", tipe = "", alasan = "", setujuTolak = "";
        int dataDiterima = 0, dataTidakDiterima = 0;
        PembaruanStatus pembaruanStatus = PembaruanStatus.getLatestStatusByPermohonanId(permId);
        PermohonanPembaruan permohonanPembaruan = PermohonanPembaruan.findById(permId);
        PembaruanRiwayat pembaruanRiwayat = new PembaruanRiwayat();
        List<PembaruanStaging> pembaruanStaging = PembaruanStaging.getByPermohonanPembaruanId(permId);

//        redirect("katalog.PermohonanPembaruanCtr.listDisposisi", permId);
        if (pembaruanStaging!=null){
            for (PembaruanStaging ps : pembaruanStaging){
                if (ps.apakah_disetujui == 1){
                    dataDiterima = dataDiterima+1;
                } else {
                    dataTidakDiterima = dataTidakDiterima+1;
                    ps.apakah_disetujui = -1;
                    ps.save();
                }
            }
        }

        pembaruanStatus.status_permohonan = dataDiterima > 0 ? pembaruanStatus.STATUS_PERMOHONAN_KLARIFIKASI_DITERIMA
                : pembaruanStatus.STATUS_PERMOHONAN_KLARIFIKASI_DITOLAK;
        pembaruanStatus.from_status = pembaruanStatus.STATUS_ADMIN;
        pembaruanStatus.to_status = pembaruanStatus.STATUS_ADMIN;


        tipe = permohonanPembaruan.tipe_permohonan.equalsIgnoreCase(permohonanPembaruan.TIPE_PERMOHONAN_PRODUK) ? "produk" :"distributor";

        alasan = dataDiterima == pembaruanStaging.size() ? "Semua pembaruan "+
                tipe+" diterima" : (dataDiterima > 0 && dataDiterima < pembaruanStaging.size() ? "Sebagian pembaruan "+tipe+" dari pengajuan permohonan pembaruan":"Daftar pembaruan "+tipe+" yang diajukan tidak diterima");

        setujuTolak = dataDiterima > 0 ? permohonanPembaruan.STATUS_KLARIFIKASI_TERIMA : permohonanPembaruan.STATUS_KLARIFIKASI_TOLAK;

        permohonanPembaruan.klarif_user_id = AktifUser.getAktifUser().user_id;
        permohonanPembaruan.status = permohonanPembaruan.STATUS_HASIL;
        permohonanPembaruan.klarif_terima_tolak = setujuTolak;
        permohonanPembaruan.klarif_terima_tolak_oleh = AktifUser.getAktifUser().user_id;
        permohonanPembaruan.klarif_terima_tolak_alasan = alasan;
        permohonanPembaruan.klarif_terima_tolak_tanggal = new Timestamp(new java.util.Date().getTime());

        int saveData = permohonanPembaruan.save();

        msg = "Proses persetujuan perubahan data";

        if (saveData > 0) {
            pembaruanRiwayat.addKlarifikasi(permId, pembaruanStatus.STATUS_ADMIN, setujuTolak, alasan);

            pembaruanStatus.deskripsi = alasan;
            pembaruanStatus.save();

            flash.success(msg + " berhasil disimpan.");
        } else {
            flash.error(msg + " gagal disimpan.");
        }

        redirect("katalog.PermohonanPembaruanCtr.listHasilKlarifikasi", permId);
    }

    public static void removeDokumenPermohonan(Long permohonanId, Long btId) {
        boolean rmBlob = removeDokumen(btId);
        if (rmBlob) {
            int rmDocument = PermohonanPembaruan.removeDocument(permohonanId);
            renderJSON(rmDocument);
        }
    }

    public static void removeDokumenDisposisi(Long dispoId, Long btId) {
        boolean rmBlob = removeDokumen(btId);
        if (rmBlob) {
            int rmDocument = PembaruanDisposisi.removeDocument(dispoId);
            renderJSON(rmDocument);
        }
    }

    public static void removeDokumenKoordinasi(Long koordId, Long btId) {
        boolean rmBlob = removeDokumen(btId);
        if (rmBlob) {
            int rmDocument = PembaruanKoordinasi.removeDocument(koordId);
            renderJSON(rmDocument);
        }
    }

    public static void removeDokumenBeritaAcara(Long baId, Long btId){
        boolean rmBlob = removeDokumen(btId);
        if(rmBlob) {
            int rmDocument = PembaruanBeritaAcara.removeDocument(baId);
            renderJSON(rmDocument);
        }
    }

    public static void removeDokumenKlarifikasi(Long klarifId, Long btId){
        boolean rmBlob = removeDokumen(btId);
        if (rmBlob) {
            int rmDocument = PembaruanKlarifikasi.removeDocument(klarifId);
            renderJSON(rmDocument);
        }
    }

    public static void removeDokumenHasilKlarifikasi(Long hasilKlarifId, Long btId){
        boolean rmBlob = removeDokumen(btId);
        if(rmBlob) {
            int rmDocument = PembaruanDisposisi.removeDocument(hasilKlarifId);
            renderJSON(rmDocument);
        }
    }

    public static void removeDokumenKontrak(Long kontrakId, Long btId){
        boolean rmBlob = removeDokumen(btId);
        if (rmBlob) {
            int rmDocument = PembaruanKontrak.removeDocument(kontrakId);
            renderJSON(rmDocument);
        }
    }

    public static boolean removeDokumen(Long blob_id) {
        try {
            BlobTable blobTable = BlobTable.findByBlobId(blob_id);
            if (blobTable != null) {
                blobTable.preDelete();
                blobTable.delete();
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_BA_KLARIFIKASI})
    public static void beritaAcaraKlarifikasi(long id){
        boolean showButtonSimpan = false, allowAddBA = false;
        String statusPmb = "", latestStatus = "", jabatan = "";
        int tipeJadwal = 0; //0 belum ada jadwal, 1 sudah ada jadwal namun belum konfirmasi kehadiran, 3 ada jadwal dan setuju, 4 ada jadwal selesai tapi tidak konfirmasi
        PembaruanBeritaAcara pembBeritaAcara = PembaruanBeritaAcara.findByPermohonanId(id);
        PermohonanPembaruan pmb = PermohonanPembaruan.findById(id);
        allowToAccess(pmb);
        PembaruanStatus ps = PembaruanStatus.getLatestStatusByPermohonanId(id);
        PembaruanKlarifikasi pk = PembaruanKlarifikasi.getLatestJadwalByPermohonanId(id);
        AktifUser au = AktifUser.getAktifUser();

        jabatan = au.jabatan != null ? au.jabatan : "";
        statusPmb = pmb.status;
        latestStatus = ps.status_permohonan;
        if ((statusPmb.equalsIgnoreCase(pmb.STATUS_KLARIFIKASI) || statusPmb.equalsIgnoreCase(pmb.STATUS_HASIL)) &&
                (ps.status_permohonan.equalsIgnoreCase(ps.STATUS_PERMOHONAN_KLARIFIKASI) ||
                ps.status_permohonan.equalsIgnoreCase(ps.STATUS_PERMOHONAN_KLARIFIKASI_BA)) &&
            ps.to_status.equalsIgnoreCase(ps.STATUS_ADMIN) && (jabatan.isEmpty() && (au.isAdmin() || au.isAdminLokalSektoral()))){
            showButtonSimpan = true;
        }

        if (pk!=null){
            //jumlahTolakKlarif = jumlahTolakKlarif+1;
            Date nows = new Timestamp(new java.util.Date().getTime());
            String batasJadwal = pk.tanggal_kegiatan + " " + pk.waktu_kegiatan;
            Date dateJadwal = null;
            //2019-11-27	14:45:00
            try {
                dateJadwal = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(batasJadwal);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (pk.setuju_tolak != null && pk.setuju_tolak.equalsIgnoreCase(PermohonanPembaruan.STATUS_SETUJU)){
                allowAddBA = nows.after(dateJadwal);
                if (nows.before(dateJadwal)){
                    tipeJadwal = 2;
                    // ada jadwal dan setuju
                }
            }

            if (pk.setuju_tolak == null && nows.before(dateJadwal)){
                //sudah ada jadwal namun belum konfirmasi kehadiran
                tipeJadwal = 1;
            }

            if (pk.setuju_tolak == null && nows.after(dateJadwal)){
                tipeJadwal = 3; //ada jadwal selesai tapi tidak konfirmasi
            }
        } else {
            //belum ada jadwal klarifikasi
            tipeJadwal = 0;
        }

        renderArgs.put("showButtonSimpan",showButtonSimpan);
        renderArgs.put("allowAddBA",allowAddBA);
        renderArgs.put("tipeJadwal",tipeJadwal);
        render("katalog/ProdukCtr/permohonan/beritaAcaraKlarifikasi.html", pembBeritaAcara, id);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_BA_KLARIFIKASI_ADD})
    public static void beritaAcaraKlarifSubmit(PembaruanBeritaAcara beritaAcara, File file) {
        Long countBA = PembaruanBeritaAcara.countActiveByPermohonan(beritaAcara.permohonan_pembaruan_id);
        boolean msg = false, baHasBeenSaved = false;
        beritaAcara.active = 1;
        PermohonanPembaruan pmb = PermohonanPembaruan.findById(beritaAcara.permohonan_pembaruan_id);
        if(countBA > 0){
            PembaruanBeritaAcara pembBeritaAcara = PembaruanBeritaAcara.findById(beritaAcara.id);
            msg = pembBeritaAcara.beritaAcaraKlarifSubmit(pembBeritaAcara, file);

            if (pmb.butuh_klarifikasi_ulang != 0){
                pmb.butuh_klarifikasi_ulang = 0;
                pmb.status = pmb.STATUS_HASIL;
                pmb.save();
            }
        }else{
            msg = beritaAcara.beritaAcaraKlarifSubmit(beritaAcara, file);
            PembaruanStatus ps = PembaruanStatus.getLatestStatusByPermohonanId(beritaAcara.permohonan_pembaruan_id);
            //cek dulu status sebelumnya agar status terakhir tidak tumpan tindih
            ps.status_permohonan = ps.STATUS_PERMOHONAN_KLARIFIKASI_BA;
            ps.save();

            pmb.status = pmb.STATUS_HASIL;
            pmb.save();
        }

        if (msg) {
            flash.success("Data berhasil disimpan");
        } else {
            flash.error("Data tidak berhasil disimpan");
        }

        redirect("katalog.PermohonanPembaruanCtr.beritaAcaraKlarifikasi", beritaAcara.permohonan_pembaruan_id);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_HASILKLARIFIKASI})
    public static void listHasilKlarifikasi(long id){
        boolean baIsExist = false, showButtonSetuju = false, showButtonTambah = true, shwoButtonUlang = false,
                showButtonAdendum = false, showButtonKirim = false, showButtonSelesaikan = false, keepShowSelesaikan = false;

        List<PembaruanHasilKlarifikasi> hasilKLarif = PembaruanHasilKlarifikasi.findActiveByPermohonan(id);
        PembaruanBeritaAcara pembBeritaAcara = PembaruanBeritaAcara.findByPermohonanId(id);

        baIsExist = null != pembBeritaAcara && pembBeritaAcara.blb_id_content != null ? true : false;

        String jabatan = "", posisi = "", disposisiDari = "", status = "", dispoKe = "";
        Long uid = 0l;

        AktifUser au = AktifUser.getAktifUser();
        PembaruanStatus ps = PembaruanStatus.getLatestStatusByPermohonanId(id);
        PermohonanPembaruan pemb = PermohonanPembaruan.findById(id);
        allowToAccess(pemb);

        boolean status_selesai = (!pemb.status.equalsIgnoreCase(PermohonanPembaruan.STATUS_SELESAI_SETUJU) &&
                                !pemb.status.equalsIgnoreCase(PermohonanPembaruan.STATUS_SELESAI_TOLAK) &&
                                !pemb.status.equalsIgnoreCase(PermohonanPembaruan.STATUS_SELESAI));
        uid = au.user_id;
        jabatan = au.jabatan != null ? au.jabatan : "";
        posisi = ps.to_status;
        disposisiDari = ps.from_status;
        status = ps.status_permohonan;

        if (ps.to_status.equalsIgnoreCase(ps.STATUS_ADMIN) && (jabatan == null || jabatan.isEmpty())){
            showButtonTambah = true;
            if (pemb.klarif_terima_tolak == null || pemb.klarif_terima_tolak.isEmpty()){
                showButtonSetuju = true;
            }
        } else if (!ps.to_status.equalsIgnoreCase(ps.STATUS_ADMIN) && !ps.to_status.equalsIgnoreCase(ps.STATUS_PENYEDIA)){
            showButtonTambah = ps.to_status.equalsIgnoreCase(jabatan) ? true : false;
        } else {
            showButtonTambah = false;
        }

        if (jabatan.equalsIgnoreCase(ps.STATUS_DIREKTUR) && posisi.equalsIgnoreCase(ps.STATUS_DIREKTUR) && pemb.butuh_klarifikasi_ulang==0){
            shwoButtonUlang = true;
        }

        if (jabatan.equalsIgnoreCase(ps.STATUS_KASUBDIT) && posisi.equalsIgnoreCase(ps.STATUS_KASUBDIT) && pemb.butuh_klarifikasi_ulang==0){
            shwoButtonUlang = true;
        }

        for (PembaruanHasilKlarifikasi p : hasilKLarif) {
            if (uid.equals(new Long(p.created_by))) {
                showButtonTambah = false;

                if(status_selesai) {
                    if (!jabatan.isEmpty()) {
                        if (!showButtonTambah && jabatan.equalsIgnoreCase(posisi) && !jabatan.equalsIgnoreCase(PermohonanPembaruan.STATUS_DIREKTUR)
                                && (pemb.klarif_terima_tolak != null || !pemb.klarif_terima_tolak.isEmpty())){
                            if (!pemb.status.equalsIgnoreCase(pemb.STATUS_ADENDUM_KONTRAK)) {
                                showButtonKirim = true;
                            }
                        }
                        if (!showButtonTambah && jabatan.equalsIgnoreCase(PermohonanPembaruan.STATUS_DIREKTUR)) {
                            showButtonSelesaikan = true;
                        }
                    } else {
                        if (!showButtonTambah && posisi.equalsIgnoreCase(PermohonanPembaruan.STATUS_ADMIN)) {
                            if (!pemb.status.equalsIgnoreCase(pemb.STATUS_ADENDUM_KONTRAK)) {
                                showButtonKirim = true;
                            }
                        }
                    }
                }
            }
        }

        if ((status.equalsIgnoreCase(ps.STATUS_PERMOHONAN_KLARIFIKASI_KASUBDIT) || status.equalsIgnoreCase(ps.STATUS_PERMOHONAN_KLARIFIKASI_DITERIMA)) && ps.to_status.equalsIgnoreCase(ps.STATUS_DIREKTUR)
                && !showButtonTambah && (au.isAdmin() || au.isAdminLokalSektoral()) && jabatan.equalsIgnoreCase(ps.STATUS_DIREKTUR)){
            if (pemb.tipe_permohonan.equalsIgnoreCase(pemb.TIPE_PERMOHONAN_PRODUK)) {
                showButtonAdendum = true;
            } else {
                keepShowSelesaikan = true;
            }
        } else if (status.equalsIgnoreCase(ps.STATUS_TANPA_ADENDUM_KONTRAK) && ps.to_status.equalsIgnoreCase(ps.STATUS_DIREKTUR)
                && !showButtonTambah && !showButtonAdendum && (au.isAdmin() || au.isAdminLokalSektoral()) && jabatan.equalsIgnoreCase(ps.STATUS_DIREKTUR)) {
            keepShowSelesaikan = true;
        }

//        if (status.equalsIgnoreCase(ps.STATUS_ADENDUM_KONTRAK) || status.equalsIgnoreCase(ps.STATUS_TANPA_ADENDUM_KONTRAK)){
//            showButtonAdendum = false;
//        } else {
//            keepShowSelesaikan = true;
//        }

//        if (status.equalsIgnoreCase(ps.STATUS_TANPA_ADENDUM_KONTRAK) ){
////            || status.equalsIgnoreCase(ps.STATUS_PERMOHONAN_KLARIFIKASI_DITERIMA)
////                    || status.equalsIgnoreCase(ps.STATUS_PERMOHONAN_KLARIFIKASI_DITOLAK)
//            keepShowSelesaikan = true;
//        }

        List<User> sendToUser = new ArrayList<>();
        //ambil data user
        if (!posisi.equalsIgnoreCase(PermohonanPembaruan.STATUS_DIREKTUR)){
            sendToUser = PermohonanPembaruan.findUserToSend(ps.to_status, pemb.status, pemb.jenis_komoditas, pemb.kldi_id);
        }

        if (posisi.equalsIgnoreCase(PermohonanPembaruan.STATUS_ADMIN)){
            dispoKe = PermohonanPembaruan.STATUS_KASI;
        } else if (posisi.equalsIgnoreCase(PermohonanPembaruan.STATUS_KASI)){
            dispoKe = PermohonanPembaruan.STATUS_KASUBDIT;
        } else if (posisi.equalsIgnoreCase(PermohonanPembaruan.STATUS_KASUBDIT)) {
            dispoKe = PermohonanPembaruan.STATUS_DIREKTUR;
        }

        render("katalog/ProdukCtr/permohonan/listPermohonanHasilKlarifikasi.html",
                hasilKLarif, id, baIsExist, showButtonKirim, showButtonTambah, showButtonSelesaikan,
                showButtonSetuju, showButtonAdendum, keepShowSelesaikan, pemb, shwoButtonUlang, sendToUser, dispoKe);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_HASILKLARIFIKASI})
    public static void kirimHasilKlarifikasi(long permId, String toUser) {
        String toStatus = "", statusPermohonan = "";
        boolean baIsExist = false;
        long uid = 0;

        if (null != toUser){
            uid = Long.valueOf(toUser);
        }

        if (uid==0){
            flash.error("Laporan Klarifikasi gagal dikirim!");
            redirect("katalog.PermohonanPembaruanCtr.listHasilKlarifikasi", permId);
        }

        PembaruanBeritaAcara pembBeritaAcara = PembaruanBeritaAcara.findByPermohonanId(permId);
        baIsExist = null != pembBeritaAcara && pembBeritaAcara.blb_id_content != null ? true : false;

        if (baIsExist) {

            PembaruanStatus pembaruanStatus = PembaruanStatus.getLatestStatusByPermohonanId(permId);
            PermohonanPembaruan pemb = PermohonanPembaruan.findById(permId);
            PembaruanStatus pembaruanStatusTemp = new PembaruanStatus();
            PembaruanRiwayat pembaruanRiwayat = new PembaruanRiwayat();

            pembaruanStatusTemp = pembaruanStatus;
            pembaruanStatusTemp.from_status = pembaruanStatus.to_status;

            if (pembaruanStatus.to_status.equalsIgnoreCase(PembaruanStatus.STATUS_ADMIN)) {
                toStatus = PembaruanStatus.STATUS_KASI;
                statusPermohonan = PembaruanStatus.STATUS_PERMOHONAN_KLARIFIKASI_ADMIN;
            } else if (pembaruanStatus.to_status.equalsIgnoreCase(PembaruanStatus.STATUS_KASI)) {
                toStatus = PembaruanStatus.STATUS_KASUBDIT;
                statusPermohonan = PembaruanStatus.STATUS_PERMOHONAN_KLARIFIKASI_KASI;
            } else if (pembaruanStatus.to_status.equalsIgnoreCase(PembaruanStatus.STATUS_KASUBDIT)) {
                toStatus = PembaruanStatus.STATUS_DIREKTUR;
                statusPermohonan = PembaruanStatus.STATUS_PERMOHONAN_KLARIFIKASI_KASUBDIT;
            }

            pembaruanStatusTemp.to_status = toStatus;
            pembaruanStatusTemp.status_permohonan = statusPermohonan;

            int saveSucces = pembaruanStatusTemp.save();
        
            pemb.klarif_user_id = uid;
            pemb.status = pemb.STATUS_HASIL;
            pemb.save();

            if (saveSucces > 0) {
                pembaruanRiwayat.addKlarifikasiHasilKirim(permId, toStatus, pembaruanStatus.from_status);
                flash.success("Laporan Klarifikasi berhasil dikirim");
            } else {
                flash.error("Laporan Klarifikasi gagal dikirim");
            }
        } else {
            flash.error("Laporan Klarifikasi gagal dikirim karena BA Tidak Ada");
        }

        redirect("katalog.PermohonanPembaruanCtr.listHasilKlarifikasi", permId);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_HASILKLARIFIKASI})
    public static void butuhKlarifikasiUlang(long pmbId, String alasanUlang, String yaTidak) {
        String msg = "";
        int ulangKe = 0;
        String posisiTerakhir = "";
        PembaruanStatus pembaruanStatus = PembaruanStatus.getLatestStatusByPermohonanId(pmbId);
        PermohonanPembaruan permohonanPembaruan = PermohonanPembaruan.findById(pmbId);
        PembaruanRiwayat pembaruanRiwayat = new PembaruanRiwayat();

        if (yaTidak.equalsIgnoreCase(permohonanPembaruan.STATUS_YA)){
            PembaruanStaging.cancelApproveStagingByPermId(pmbId);
            permohonanPembaruan.butuh_klarifikasi_ulang = 1;
            ulangKe = permohonanPembaruan.klarifikasi_ulang_ke;
            permohonanPembaruan.klarifikasi_ulang_ke = ulangKe + 1;
            permohonanPembaruan.status = permohonanPembaruan.STATUS_KLARIFIKASI;
            permohonanPembaruan.klarif_user_id = null;
            permohonanPembaruan.klarif_terima_tolak = null;
            permohonanPembaruan.klarif_terima_tolak_tanggal = null;
            permohonanPembaruan.klarif_terima_tolak_alasan = null;
            permohonanPembaruan.klarif_terima_tolak_oleh = null;
        } else {
            permohonanPembaruan.butuh_klarifikasi_ulang = -1;
            permohonanPembaruan.status = permohonanPembaruan.STATUS_HASIL;
        }

        permohonanPembaruan.klarifikasi_ulang_alasan = alasanUlang;
        int simpanPembaruan = permohonanPembaruan.save();

        posisiTerakhir = pembaruanStatus.to_status.equalsIgnoreCase(pembaruanStatus.STATUS_DIREKTUR)? pembaruanStatus.STATUS_DIREKTUR : pembaruanStatus.STATUS_KASUBDIT;
//        redirect("katalog.PermohonanPembaruanCtr.listDisposisi", permId);

        msg = "Data";
        if (simpanPembaruan>0){
            if (yaTidak.equalsIgnoreCase(permohonanPembaruan.STATUS_YA)) {
                pembaruanStatus.to_status = pembaruanStatus.STATUS_ADMIN;
                pembaruanStatus.from_status = posisiTerakhir;
                pembaruanStatus.status_permohonan = pembaruanStatus.STATUS_PERMOHONAN_KLARIFIKASI;
                pembaruanStatus.deskripsi = "klarifikasi ulang";
            } else {
                pembaruanStatus.to_status = posisiTerakhir;
                pembaruanStatus.from_status = posisiTerakhir;
                pembaruanStatus.status_permohonan = pembaruanStatus.STATUS_PERMOHONAN_KLARIFIKASI_TIDAK_DIULANG;
                pembaruanStatus.deskripsi = "klarifikasi tidak perlu diulang";
            }
            pembaruanStatus.save();

            pembaruanRiwayat.addKlarifikasiUlang(pmbId, posisiTerakhir, yaTidak, alasanUlang);

            flash.success(msg + " berhasil disimpan.");
        } else {
            flash.error(msg + " gagal disimpan.");
        }

        redirect("katalog.PermohonanPembaruanCtr.listhasilklarifikasi",pmbId);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_HASILKLARIFIKASI})
    public static void adendumHasilKlarifikasi(long permId, String alasan, String tipe, String setujuTolak) {
        String msg = "";
        PembaruanStatus pembaruanStatus = PembaruanStatus.getLatestStatusByPermohonanId(permId);
        PermohonanPembaruan permohonanPembaruan = PermohonanPembaruan.findById(permId);
        PembaruanRiwayat pembaruanRiwayat = new PembaruanRiwayat();
//        redirect("katalog.PermohonanPembaruanCtr.listDisposisi", permId);

        if (setujuTolak.equalsIgnoreCase("ya")){
            pembaruanStatus.status_permohonan = pembaruanStatus.STATUS_ADENDUM_KONTRAK;
            pembaruanStatus.to_status = pembaruanStatus.STATUS_ADMIN;
        } else {
            pembaruanStatus.status_permohonan = pembaruanStatus.STATUS_TANPA_ADENDUM_KONTRAK;
            pembaruanStatus.to_status = pembaruanStatus.STATUS_DIREKTUR;
        }

        pembaruanStatus.from_status = pembaruanStatus.STATUS_DIREKTUR;

        permohonanPembaruan.status =  setujuTolak.equalsIgnoreCase("ya") ? permohonanPembaruan.STATUS_ADENDUM_KONTRAK : permohonanPembaruan.STATUS_HASIL;

        int saveData = permohonanPembaruan.save();

        msg = "Data";

        if (saveData > 0) {
            pembaruanRiwayat.addKlarifikasiAdendum(permId, pembaruanStatus.STATUS_DIREKTUR, setujuTolak, alasan);

            pembaruanStatus.deskripsi = alasan;
            pembaruanStatus.save();

            flash.success(msg + " berhasil disimpan.");
        } else {
            flash.error(msg + " gagal disimpan.");
            redirect("katalog.PermohonanPembaruanCtr.listhasilklarifikasi",permId);
        }

        if (setujuTolak.equalsIgnoreCase("ya")){
            redirect("katalog.PermohonanPembaruanCtr.index");
        } else {
            redirect("katalog.PermohonanPembaruanCtr.listhasilklarifikasi",permId);
        }
    }

    public static void selesaikanPermohonan(long permId) {
        String toStatus = "", statusPermohonan = "", eksekutor = "";
        PembaruanStatus pembaruanStatus = PembaruanStatus.getLatestStatusByPermohonanId(permId);
        PermohonanPembaruan pemb = PermohonanPembaruan.findById(permId);
        PembaruanStatus pembaruanStatusTemp = new PembaruanStatus();
        PembaruanRiwayat pembaruanRiwayat = new PembaruanRiwayat();

        if (pemb.klarif_terima_tolak.equalsIgnoreCase(pemb.STATUS_KLARIFIKASI_TERIMA)){
            /*
                Pembaruan datanya bisa dilakukan disini
            */
            pemb.applyStagging();
        }

        eksekutor = pembaruanStatus.to_status.equalsIgnoreCase(pembaruanStatus.STATUS_DIREKTUR) ?
                pembaruanStatus.STATUS_DIREKTUR : pembaruanStatus.STATUS_ADMIN;

        pembaruanStatusTemp = pembaruanStatus;
        pembaruanStatusTemp.from_status = pembaruanStatus.to_status;

        toStatus = PembaruanStatus.STATUS_PENYEDIA;
        statusPermohonan = eksekutor.equalsIgnoreCase(pembaruanStatus.STATUS_DIREKTUR) ?
                PembaruanStatus.STATUS_PERMOHONAN_KLARIFIKASI_DIREKTUR : PembaruanStatus.STATUS_PERMOHONAN_SELESAI;

        pembaruanStatusTemp.to_status = toStatus;
        pembaruanStatusTemp.status_permohonan = statusPermohonan;

        pembaruanStatusTemp.save();

        pemb.dispo_user_id = null;
        pemb.minta_disetujui = 0;
        pemb.klarif_user_id = null;
        pemb.status = pemb.klarif_terima_tolak.equalsIgnoreCase(pemb.STATUS_KLARIFIKASI_TERIMA) ? pemb.STATUS_SELESAI_SETUJU : pemb.STATUS_SELESAI_TOLAK;
        int saveSucces = pemb.save();

        if (saveSucces > 0) {
            pembaruanRiwayat.addSelesaikanHasil(permId, eksekutor);
            flash.success("Permohonan Pembaruan berhasil diselesaikan");
        } else {
            flash.error("Permohonan Pembaruan gagal diselesaikan");
        }

        if (eksekutor.equalsIgnoreCase(PembaruanStatus.STATUS_DIREKTUR)){
            redirect("katalog.PermohonanPembaruanCtr.listHasilKlarifikasi", permId);
        } else{
            redirect("katalog.PermohonanPembaruanCtr.listPenawaranKontrak", permId);
        }
    }

    public static void batalkanPermohonan(long permId, String alasan) {
        String toStatus = "", statusPermohonan = "";
        PembaruanStatus pembaruanStatus = PembaruanStatus.getLatestStatusByPermohonanId(permId);
        PermohonanPembaruan pemb = PermohonanPembaruan.findById(permId);
        PembaruanStatus pembaruanStatusTemp = new PembaruanStatus();
        PembaruanRiwayat pembaruanRiwayat = new PembaruanRiwayat();

        if (pembaruanStatus != null) {
            pembaruanStatusTemp = pembaruanStatus;
        } else {
            pembaruanStatusTemp.permohonan_pembaruan_id = permId;
        }
        pembaruanStatusTemp.from_status = PembaruanStatus.STATUS_PENYEDIA;

        statusPermohonan = PembaruanStatus.STATUS_PERMOHONAN_BATAL;

        pembaruanStatusTemp.to_status = PembaruanStatus.STATUS_PENYEDIA;;
        pembaruanStatusTemp.status_permohonan = statusPermohonan;
        pembaruanStatusTemp.deskripsi = alasan;

        pembaruanStatusTemp.save();

        pemb.status = pemb.STATUS_BATAL;
        int saveSucces = pemb.save();

        if (saveSucces > 0) {
            pembaruanRiwayat.addBatalkanPermohona(permId, pemb.STATUS_PENYEDIA, alasan);
            //PembaruanStaging.deleteStagingByPermId(permId);
            flash.success("Permohonan Pembaruan berhasil diselesaikan");
        } else {
            flash.error("Permohonan Pembaruan gagal diselesaikan");
        }
        redirect("katalog.PermohonanPembaruanCtr.index");
    }

    public static void AdminBatalkanPermohonan(long permIdS, String alasan) {
        String toStatus = "", statusPermohonan = "";
        PembaruanStatus pembaruanStatus = PembaruanStatus.getLatestStatusByPermohonanId(permIdS);
        PermohonanPembaruan pemb = PermohonanPembaruan.findById(permIdS);
        PembaruanStatus pembaruanStatusTemp = new PembaruanStatus();
        PembaruanRiwayat pembaruanRiwayat = new PembaruanRiwayat();

        if (pembaruanStatus != null) {
            pembaruanStatusTemp = pembaruanStatus;
        } else {
            pembaruanStatusTemp.permohonan_pembaruan_id = permIdS;
        }
        pembaruanStatusTemp.from_status = PembaruanStatus.STATUS_ADMIN;

        statusPermohonan = PembaruanStatus.STATUS_PERMOHONAN_BATAL;

        pembaruanStatusTemp.to_status = PembaruanStatus.STATUS_PENYEDIA;;
        pembaruanStatusTemp.status_permohonan = statusPermohonan;
        pembaruanStatusTemp.deskripsi = alasan;

        pembaruanStatusTemp.save();

        pemb.status = pemb.STATUS_DIBATALKAN;
        pemb.butuh_konfirmasi_kehadiran = 0;
        pemb.klarif_user_id = null;
        pemb.dispo_user_id = null;
        int saveSucces = pemb.save();

        if (saveSucces > 0) {
            pembaruanRiwayat.addBatalkanPermohona(permIdS, pemb.STATUS_ADMIN, alasan);
            //PembaruanStaging.deleteStagingByPermId(permId);
            flash.success("Permohonan Pembaruan berhasil diselesaikan");
        } else {
            flash.error("Permohonan Pembaruan gagal diselesaikan");
        }
        redirect("katalog.PermohonanPembaruanCtr.index");
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_HASILKLARIFIKASI_ADD})
    public static void createHasilKlarifikasi(long permId) {
        render("katalog/ProdukCtr/permohonan/createHasilKLarifikasi.html", permId);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_HASILKLARIFIKASI_EDIT})
    public static void editHasilKlarifikasi(long dispId) {
        PembaruanHasilKlarifikasi hasilKlarif = PembaruanHasilKlarifikasi.findById(dispId);
        render("katalog/ProdukCtr/permohonan/editHasilKLarifikasi.html", hasilKlarif);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_HASILKLARIFIKASI_ADD})
    public static void createHasilKlarifSubmit(PembaruanHasilKlarifikasi hasilKlarif, File file) {
        hasilKlarif.active = 1;
        boolean msg = hasilKlarif.hasilKlarifikasiSubmit(hasilKlarif, file);
        if (msg) {
            flash.success("Data berhasil disimpan");
        } else {
            flash.error("Data tidak berhasil disimpan");
        }

        redirect("katalog.PermohonanPembaruanCtr.listHasilKlarifikasi", hasilKlarif.permohonan_pembaruan_id);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_HASILKLARIFIKASI_EDIT})
    public static void editHasilKlarifSubmit(PembaruanHasilKlarifikasi hasilKlarif, File file) {
        PembaruanHasilKlarifikasi hasilKlarifikasi = PembaruanHasilKlarifikasi.findById(hasilKlarif.id);
        hasilKlarifikasi.deskripsi = hasilKlarif.deskripsi;
        hasilKlarifikasi.active = 1;
        boolean msg = hasilKlarifikasi.hasilKlarifikasiSubmit(hasilKlarifikasi, file);
        if (msg) {
            flash.success("Data berhasil diubah");
        } else {
            flash.error("Data tidak berhasil diubah");
        }

        redirect("katalog.PermohonanPembaruanCtr.listHasilKlarifikasi", hasilKlarifikasi.permohonan_pembaruan_id);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK})
    public static void listApprovalProdukPembaruanStaging(Long id, Integer approved) {
        //tidka digunakan
        //boolean showButtonKirim = false;

        if(null == id){
            index();
        }

        String statusPmb = "";

        ProductSearch query = new ProductSearch(params);
        ProductSearchResult model = new ProductSearchResult(query);
        List<Komoditas> komoditas = new ArrayList<>();
        PermohonanPembaruan pmb = PermohonanPembaruan.findById(id);

        statusPmb = pmb.status;

        if (getAktifUser().isProvider()) {
            try {
                Penyedia py = Penyedia.findById(pmb.penyedia_id);
                komoditas = Komoditas.findByPenyedia(pmb.penyedia_id);
                query.penyedia = Penyedia.getProviderByUserId(py.user_id).id.toString();
            } catch (Exception e) {
                Logger.error("error:" + e.getMessage());
            }
        } else {
            // belum define
        }

        if (params.all().size() > 1) {
            model = ProductRepository.searchProduct(query);
        }

//        tidak digunakan
//        if (statusPmb.equalsIgnoreCase(pmb.STATUS_PERSIAPAN)) {
//            showButtonKirim = true;
//        }

        //all permbaruan stagging per permohonan
        List<PembaruanStaging> lstPstg = PembaruanStaging.getByPermohonanPembaruanIdApproved(pmb.id, 1);
        List<PembaruanStaging> lstPstgN = PembaruanStaging.getByPermohonanPembaruanIdApproved(pmb.id, 0);
        Boolean exisApproved = lstPstg.size() > 0;
        Boolean emptyNotAprr = lstPstgN.size() == 0;
        if(null == approved){
            lstPstg = PembaruanStaging.getByPermohonanPembaruanId(pmb.id);
        }else{
            if(approved.equals(0)){
                lstPstg = lstPstgN;
            }
        }
        //filter yang tipenya produk
        lstPstg = lstPstg.stream().filter(f -> f.getPermohonanPembaruan()
                .tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_PRODUK)).collect(Collectors.toList());
        //ambil id as sting
        List<String> sStr = new ArrayList<>();
        try {
            sStr = Arrays.asList(lstPstg.stream().map(PembaruanStaging::getProdukIdIfTypeProduk).collect(Collectors.joining(",")).split(","));
        } catch (Exception e) {
        }
        //convert as long
        List<Long> sLong = new ArrayList<>();
        try {
            sLong = sStr.stream().map(Long::parseLong).collect(Collectors.toList());
        } catch (Exception e) {
        }
        //filter buang produk yang sedang pengajuan
        //List<Long> finalSLong = sLong;
        // search ids
        query.listProdukIdsIn = sLong;

        if(params.all().size() > 1 && sLong.size() > 0){
            model = ProductRepository.searchProduct(query);
        }else{
            model = new ProductSearchResult(query);
        }

        renderArgs.put("paramQuery", query);

        //renderArgs.put("showButtonKirim", showButtonKirim);
        render("katalog/ProdukCtr/listApprovalProdukPembaruanStaging.html", model, komoditas, pmb, approved, lstPstg, exisApproved);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_PRODUK})
    public static void submitProdukApprovalPeritems(long[] idProduk, long pmbid) {
        PermohonanPembaruan pmb = PermohonanPembaruan.findById(pmbid);
        PembaruanStaging.getByPermohonanPembaruanId(pmb.id);
        List<PembaruanStaging> list = PembaruanStaging.getIfProdukStagingByIdProduk(idProduk,pmbid);
        if(list.size()>0){
            for (PembaruanStaging stg : list) {
                stg.apakah_disetujui = 1;
                stg.tgl_disetujui = new Timestamp(new Date().getTime());
                stg.save();
            }
            flash.success("Persetujuan Produk Update disimpan");
            listApprovalProdukPembaruanStaging(pmbid, null);
        }else{
            flash.error("Silahkan memilih produk yang akan di setujui");
            listApprovalProdukPembaruanStaging(pmbid, null);
        }
    }

    public static void riwayatPermohonan(Long permohonan_pembaruan_id){
        JSONObject jsonObject = new JSONObject();
        Gson gson = new Gson();
        List<PembaruanRiwayat> prList = new ArrayList<>();
        List<PembaruanRiwayat> pr = PembaruanRiwayat.getDataRiwayatByPermPembId(permohonan_pembaruan_id);

        for(PembaruanRiwayat prs : pr){
            PembaruanRiwayat p = new PembaruanRiwayat();
            p.tanggal = DateTimeUtils.parseToReadableDateTime(prs.created_date);
            if (prs.nama_role.equalsIgnoreCase(PaketStatus.STATUS_PENYEDIA)){
                p.tipe_user = prs.nama_role;
            } else if ((prs.nama_role.equalsIgnoreCase(PaketStatus.STATUS_ADMIN) || prs.nama_role.equalsIgnoreCase(PaketStatus.STATUS_ADMIN_LOKAL)) && (prs.jabatan == null || prs.jabatan.isEmpty())){
                p.tipe_user = PaketStatus.STATUS_ADMIN;
            } else if ((prs.nama_role.equalsIgnoreCase(PaketStatus.STATUS_ADMIN) || prs.nama_role.equalsIgnoreCase(PaketStatus.STATUS_ADMIN_LOKAL)) && (prs.jabatan != null && !prs.jabatan.isEmpty())){
                p.tipe_user = prs.jabatan;
            }
            p.id = prs.id;
            p.nama_user = prs.user_name;
            p.catatan = prs.deskripsi;
            p.nama_lengkap = prs.nama_lengkap; //+" ("+prs.user_name+")";
            p.status_riwayat = prs.status_permohonan.replaceAll("_"," ");
            prList.add(p);
        }

        jsonObject.put("status",prList.size() > 0 ? "sukses" : "gagal");
        jsonObject.put("data",gson.toJsonTree(prList));

        renderJSON(jsonObject);
    }
    
    public static void detailProdukCompare(Long id) {
        render("katalog/ProdukCtr/web/detailCompare.html",id);
    }

    public static void getDetailProductCenter(Long id, String type, Long location_id, Boolean laporan, Long pmbid){
        String randomID = Codec.UUID();
        Produk produk = Produk.findByIdCheck(id);
        Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
        produk.komoditas = komoditas;
        Map<Long, String> sortedMapAtributHarga = new TreeMap<>();
        List<Kabupaten> kabupatenList = new ArrayList<>();
        List<Provinsi> provinsiList = new ArrayList<>();
        User user_produk = new User();

        //		kondisi produk bisa dibeli
        boolean isCanBeBought = Produk.isCanBeBought(produk);
        renderArgs.put("isCanBeBought", isCanBeBought);


        boolean frontendDetail = true;
        if(!TextUtils.isEmpty(type) &&
                (type.equalsIgnoreCase("province") || type.equalsIgnoreCase("regency"))
                && location_id == null){
            //LogUtil.d(TAG, "redirecting to all detail");
            frontendDetail = false;
        }

        Report rpt = new Report();
        if (getAktifUser() != null) {
            //untuk chat nanti
            rpt.reporter_name = AktifUser.getAktifUser().nama_lengkap;
            rpt.reporter_telp = AktifUser.getAktifUser().no_tlp;
            rpt.reporter_email = AktifUser.getAktifUser().email;
        }

        List<KategoriKonten> listJenisLaporan = KategoriKonten.findAllByKonten(5);
        renderArgs.put("listJenisLaporan", listJenisLaporan);
        List<String> resultProdukGambar = new ArrayList<>();
        List<String> resultProdukGambarC = new ArrayList<>();
        List<ProdukHarga> listProdukHarga = new ArrayList<>();
        List<ProdukHarga> listProdukHargaC = new ArrayList<>();
        if(null != produk){
            List<ProdukGambar> produkGambar = ProdukGambar.findByProduk(id);
            for(ProdukGambar pg : produkGambar){
                if(pg.posisi_file == ProdukGambar._POSISI_FILE_LOKAL){
                    String fileLocal = pg.getLocalImageStorage()+pg.file_sub_location+pg.file_name;
                    if(Play.mode.isProd()){
                        fileLocal = KatalogUtils.cleanForLocalPathImage(fileLocal);
                    }
                    resultProdukGambar.add(fileLocal);
                    Logger.debug("local posisin:"+fileLocal);
                }else {
                    if(Play.mode.isProd()){
                        String localImage = KatalogUtils.cleanForLocalPathImage(pg.getActiveUrl());
                        resultProdukGambar.add(localImage);
                    }else{
                        resultProdukGambar.add(pg.getActiveUrl());
                    }

                }
            }
            if(produk.manufaktur_id != null){
                Manufaktur manufaktur = Manufaktur.findById(produk.manufaktur_id);
                produk.nama_manufaktur = manufaktur.nama_manufaktur;
            }
            if(produk.unit_pengukuran_id != null){
                UnitPengukuran unitPengukuran = UnitPengukuran.findById(produk.unit_pengukuran_id);
                produk.nama_unit_pengukuran = unitPengukuran.nama_unit_pengukuran;
            }
            if(produk.harga_kurs_id != null){
                Kurs kurs = Kurs.findById(produk.harga_kurs_id);
                produk.nama_kurs = kurs.nama_kurs;
            }
            List<KomoditasAtributHarga> atributHargaList = KomoditasAtributHarga.findByKomoditas(produk.komoditas_id);
            renderArgs.put("atributHargaList", atributHargaList);

            Map<Long, String> mapAtributHarga = atributHargaList.stream().collect(
                    Collectors.toMap(x -> x.id, x -> x.label_harga));

            sortedMapAtributHarga = new TreeMap<>(mapAtributHarga);

            List<ProdukHarga> produkHarga = ProdukHarga.findRiwayatByProdukId(id);
            Map<String, List<ProdukHarga>> groupHarga = produkHarga.stream()
                    .collect(
                            Collectors.groupingBy(
                                    x -> x.getStandarDateString()
                            )
                    );
            NavigableMap<String, List<ProdukHarga>> navMapProdukHarga = new TreeMap<>(groupHarga).descendingMap();
            renderArgs.put("produkHarga", navMapProdukHarga);

            String hargaUtama = "";
            if ((!TextUtils.isEmpty(type) && (type.equalsIgnoreCase("province") || type.equalsIgnoreCase("regency")) && location_id != null) && produkHarga.size() > 0) {
                KomoditasAtributHarga kah = atributHargaList.stream().parallel().filter(it -> it.apakah_harga_utama == true).findFirst().orElse(new KomoditasAtributHarga());
                ProdukHarga ph = navMapProdukHarga.firstEntry().getValue().stream().parallel().filter(it -> it.komoditas_harga_atribut_id.equals(kah.id)).findFirst().orElse(new ProdukHarga());
                List<ProdukHargaJson> hargaProdukJson = new Gson().fromJson(ph.harga, new TypeToken<List<ProdukHargaJson>>() {
                }.getType());
                ProdukHargaJson hkj = hargaProdukJson.stream()
                        .filter(it -> (type.equalsIgnoreCase("province") ? it.provinsiId : it.kabupatenId) == location_id)
                        .findFirst().orElse(new ProdukHargaJson());
                //fill harga
                produk.harga_utama = new BigDecimal(hkj.harga);
                hargaUtama = FormatUtils.formatCurrencyRupiah(produk.harga_utama);
                //fill location
                produk.daerah_id = location_id;
            }else{
                hargaUtama = FormatUtils.formatCurrencyRupiah(produk.harga_utama);
            }
            //set param hargaUtama
            renderArgs.put("hargaUtama", hargaUtama);

            List<String> jenisProduk = new ArrayList<>(Arrays.asList(Produk.TYPE_LOCAL, Produk.TYPE_IMPORT));
            if(!komoditas.jenis_produk_override){
                produk.jenis_produk = komoditas.jenis_produk;
            }

            //boolean showBuyButton = AktifUser.isUserAllowedToBuy(komoditas);
            //renderArgs.put("showBuyButton", showBuyButton);
            // by asep 2019-11-28
            // spesifikasi ada di  produk.withSpecifications()
//            List<SpesifikasiProdukJson> spesifikasiProdukJson = new ArrayList<>();
//            try{
//                spesifikasiProdukJson = new Gson().fromJson(produk.spesifikasi, new TypeToken<List<SpesifikasiProdukJson>>(){}.getType());
//            }catch (Exception e){}
//            renderArgs.put("spesifikasiProduk", spesifikasiProdukJson);

            notFoundIfNull(produk);
            produk.withPictures();
            produk.withDocuments(true);
            produk.withProvider();
            produk.withCommodity();
            produk.withSpecifications();
            produk.withPenawaran();
            produk.withPricesEqualsDatePrice();
            if(produk.prices.size() ==0){
                produk.withPricesApproved();
            }
            if (frontendDetail) {
                produk.withProvincePrice(location_id);
                produk.withRegencyPrice(location_id);
            }

            if (produk.getProviderId() != null && produk.apakah_ditayangkan && produk.active) {
                if(null != getAktifUser()) {
                    new JobUpdateViewCounter(produk,getAktifUser()).now();
                }
            }
            //wilayah jual produk
            if (produk.komoditas.isRegency()) {
                //LogUtil.d(TAG, "get regencies");
                kabupatenList = ProdukWilayahJualKabupaten.findWilayahJual(produk.getId());
                renderArgs.put("kabupatenList", kabupatenList);
                if(location_id != null) {
                    Kabupaten kabupaten = Kabupaten.findById(location_id);
                    produk.daerah_name = kabupaten.nama_kabupaten;
                }
            } else if (produk.komoditas.isProvince()) {
                //LogUtil.d(TAG, "get provinces");
                provinsiList = ProdukWilayahJualProvinsi.findWilayahJual(produk.getId());
                renderArgs.put("provinsiList", provinsiList);
                if(location_id != null) {
                    Provinsi provinsi = Provinsi.findById(location_id);
                    produk.daerah_name = provinsi.nama_provinsi;
                }
            } else {
                //LogUtil.d(TAG, "get national");
            }

            //ongkir
            if (produk.komoditas.perlu_ongkir) {
                List<OngkosKirim> listOngkir = new Gson().fromJson(produk.ongkir, new TypeToken<ArrayList<OngkosKirim>>() {
                }.getType());
                if (listOngkir.size() > 0)
                    listOngkir = setKabupatenProvinsi(listOngkir, provinsiList, kabupatenList);
                renderArgs.put("listOngkir", listOngkir);
            }

            renderArgs.put("frontendDetail", frontendDetail);
            renderArgs.put("tabLaporan", laporan);

            Penyedia penyedia = Penyedia.findById(produk.penyedia_id);
            user_produk = User.findById(penyedia.user_id);

        }//end if produk !=null

        // get produk comparer:

        Produk produkC = new Produk();
        produkC = Produk.findByIdCheck(produk.id);
        List<PembaruanStaging> list = produk.getProdukStagging(pmbid);
        Boolean info = false,spec = false,lamp = false,gambar = false,harga = false;
        for (PembaruanStaging stg : list) {
            ProdukStagging prdstg = stg.getIfProdukStagging();
            if (prdstg.info) {
                info = true;
                produkC = stg.getIfProdukStagging().getProduk();
                if(produkC.manufaktur_id != null){
                    Manufaktur manufaktur = Manufaktur.findById(produkC.manufaktur_id);
                    produk.nama_manufaktur = manufaktur.nama_manufaktur;
                }
                if(produkC.unit_pengukuran_id != null){
                    UnitPengukuran unitPengukuran = UnitPengukuran.findById(produkC.unit_pengukuran_id);
                    produkC.nama_unit_pengukuran = unitPengukuran.nama_unit_pengukuran;
                }
                if(!komoditas.jenis_produk_override){
                    produkC.jenis_produk = komoditas.jenis_produk;
                }
            } else if (prdstg.spec) {
                spec = true;
                produkC.specifications = prdstg.getListProdukAtributValue();
            } else if (prdstg.lamp) {
                lamp = true;
                produkC.documents = prdstg.withDocuments();
            } else if (prdstg.gambar) {
                gambar = true;
                produkC.pictures = prdstg.getListProdukImages();
                for(ProdukGambar pg : produkC.pictures){
                    if(pg.posisi_file == ProdukGambar._POSISI_FILE_LOKAL){
                        String fileLocal = pg.getLocalImageStorage()+pg.file_sub_location+pg.file_name;
                        if(Play.mode.isProd()){
                            fileLocal = KatalogUtils.cleanForLocalPathImage(fileLocal);
                        }
                        resultProdukGambarC.add(fileLocal);
                        Logger.debug("local posisin:"+fileLocal);
                    }else {
                        if(Play.mode.isProd()){
                            String localImage = KatalogUtils.cleanForLocalPathImage(pg.getActiveUrl());
                            resultProdukGambarC.add(localImage);
                        }else{
                            resultProdukGambarC.add(pg.getActiveUrl());
                        }

                    }
                }
            } else if (prdstg.harga) {
                harga = true;
                if(produkC.harga_kurs_id != null){
                    Kurs kurs = Kurs.findById(produkC.harga_kurs_id);
                    produkC.nama_kurs = kurs.nama_kurs;
                }

                produkC.harga_tanggal = prdstg.getIfProdukHargaStagging().harga_tanggal;
                
                produkC.prices = prdstg.getIfProdukHargaStagging().produkHargas;
                //if nasional
                if(komoditas.isNational()) {
                    listProdukHargaC = produkC.prices.stream()
                            .sorted(Comparator.comparing(ProdukHarga::getTanggalDibuat)
                                    .reversed()).collect(Collectors.toList());
                    listProdukHargaC.forEach(ProdukHarga::setNama_kursFdb);
                    //end if nasional
                }
                List<Provinsi> provs = Provinsi.findAllActive();
                List<Kabupaten> kabs = Kabupaten.find("active = 1").fetch();
                for(int i=0;i < produkC.prices.size(); i++){
                    produkC.prices.get(i).activeKabs = kabs;
                    produkC.prices.get(i).activeProvs = provs;
                    produk.prices.get(i).activeKabs = kabs;
                    produk.prices.get(i).activeProvs = provs;
                }
                // compare purpose
                List<ProdukHarga> phs = new ArrayList<>();
                for(int i =0; i < produk.prices.size(); i++){
                    ProdukHarga phe = produk.prices.get(i);
                    phs.add(phe);
                    ProdukHarga phr = produkC.prices.stream()
                            .filter(f-> f.komoditas_harga_atribut_id
                            .equals(phe.komoditas_harga_atribut_id)).findAny().orElse(null);
                    if(null != phr){
                        phs.add(phr);
                    }
                }
                //replace for compareing purpose
                produkC.prices = phs;
                produkC.ongkir = prdstg.getIfProdukHargaStagging().ongkir;

                if (komoditas.perlu_ongkir) {
                    List<OngkosKirim> listOngkirC = new Gson().fromJson(produkC.ongkir, new TypeToken<ArrayList<OngkosKirim>>() {
                    }.getType());
                    if (listOngkirC.size() > 0)
                        listOngkirC = setKabupatenProvinsi(listOngkirC, provinsiList, kabupatenList);
                    renderArgs.put("listOngkirC", listOngkirC);
                }

            }
        }
        renderArgs.put("compareInfo", info);
        renderArgs.put("compareSpec", spec);
        renderArgs.put("compareLamp", lamp);
        renderArgs.put("compareGambar", gambar);
        renderArgs.put("compareHarga", harga);

        //if nasional
        if(komoditas.isNational()) {
            listProdukHarga = produk.prices.stream()
                    .sorted(Comparator.comparing(ProdukHarga::getTanggalDibuat)
                            .reversed()).collect(Collectors.toList());
            listProdukHarga.forEach(ProdukHarga::setNama_kursFdb);
            //end if nasional
        }
        renderArgs.put("listProdukHarga", listProdukHarga);
        renderArgs.put("listProdukHargaC", listProdukHargaC);

        render("katalog/ProdukCtr/detailpart/detailCenterCompare.html",
                produk,
                produkC,
                randomID,
                sortedMapAtributHarga,
                user_produk,
                resultProdukGambar,
                resultProdukGambarC);
    }

    private static List<OngkosKirim> setKabupatenProvinsi(List<OngkosKirim> listOngkir, List<Provinsi> listPropinsi, List<Kabupaten> listKabupaten) {
        if (listPropinsi.isEmpty()) {
            List<String> pvIds = listOngkir.stream().map(it -> it.provinsiId.toString()).collect(Collectors.toList());
            if (!pvIds.isEmpty())
                listPropinsi = Provinsi.findByIds(pvIds);
        }
        if (listKabupaten.isEmpty()) {
            List<String> kbIds = listOngkir.stream().map(it -> it.kabupatenId).filter(it -> it != null).map(it -> it.toString()).collect(Collectors.toList());
            if (!kbIds.isEmpty())
                listKabupaten = Kabupaten.findByIds(kbIds);
        }
        for (OngkosKirim ok : listOngkir) {
            ok.provinsi = listPropinsi.stream().filter(it -> it.id.equals(ok.provinsiId)).findFirst().orElse(new Provinsi()).nama_provinsi;
            ok.kabupaten = listKabupaten.stream().filter(it -> it.id.equals(ok.kabupatenId)).findFirst().orElse(new Kabupaten()).nama_kabupaten;
        }
        return listOngkir;
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KOORDINASI})
    public static void listKoordinasi(long id) {
        boolean showButtonTambahKoordinasi = false;
        boolean showButtonKirimKoordinasi = false;

        List<PembaruanKoordinasi> koordinasi = PembaruanKoordinasi.findActiveByPermohonan(id);
        PembaruanStatus ps = PembaruanStatus.getLatestStatusByPermohonanId(id);

        if ((getAktifUser().isAdmin() ||getAktifUser().isAdminLokalSektoral()) && null == getAktifUser().jabatan && ps.status_permohonan.equalsIgnoreCase(PermohonanPembaruan.STATUS_VERIFIKASI_PIHAK_KETIGA)) {
            showButtonTambahKoordinasi = true;
        }

        if(koordinasi.size() > 0 && ps.status_permohonan.equalsIgnoreCase(PermohonanPembaruan.STATUS_VERIFIKASI_PIHAK_KETIGA)){
            showButtonKirimKoordinasi = true;
        }

        renderArgs.put("showButtonTambahKoordinasi", showButtonTambahKoordinasi);
        renderArgs.put("showButtonKirimKoordinasi", showButtonKirimKoordinasi);
        render("katalog/ProdukCtr/permohonan/listPermohonanKoordinasi.html", koordinasi, id);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KOORDINASI_ADD})
    public static void createKoordinasi(long permId) {
        render("katalog/ProdukCtr/permohonan/createKoordinasi.html", permId);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KOORDINASI_ADD})
    public static void createKoordinasiSubmit(PembaruanKoordinasi koord, File file) {
        koord.active = 1;
        boolean msg = koord.koordinasiSubmit(koord, file);
        if (msg) {
            flash.success("Data berhasil disimpan");
        } else {
            flash.error("Data tidak berhasil disimpan");
        }

        redirect("katalog.PermohonanPembaruanCtr.listKoordinasi", koord.permohonan_pembaruan_id);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KOORDINASI_EDIT})
    public static void editKoordinasi(long dispId) {
        PembaruanKoordinasi koord = PembaruanKoordinasi.findById(dispId);
        render("katalog/ProdukCtr/permohonan/editKoordinasi.html", koord);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KOORDINASI_EDIT})
    public static void editKoordinasiSubmit(PembaruanKoordinasi koord, File file) {
        PembaruanKoordinasi pembaruanKoordinasi = PembaruanKoordinasi.findById(koord.id);
        pembaruanKoordinasi.deskripsi = koord.deskripsi;
        pembaruanKoordinasi.active = 1;
        boolean msg = pembaruanKoordinasi.koordinasiSubmit(pembaruanKoordinasi, file);
        if (msg) {
            flash.success("Data berhasil diubah");
        } else {
            flash.error("Data tidak berhasil diubah");
        }

        redirect("katalog.PermohonanPembaruanCtr.listKoordinasi", pembaruanKoordinasi.permohonan_pembaruan_id);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KOORDINASI})
    public static void kirimKoordinasi(long permId) {
        String msg = "";
        PembaruanStatus pembaruanStatus = PembaruanStatus.getLatestStatusByPermohonanId(permId);
        PermohonanPembaruan permohonanPembaruan = PermohonanPembaruan.findById(permId);
        PembaruanRiwayat pembaruanRiwayat = new PembaruanRiwayat();

        pembaruanStatus.status_permohonan = pembaruanStatus.STATUS_PERMOHONAN_KLARIFIKASI;
        pembaruanStatus.from_status = pembaruanStatus.STATUS_ADMIN;
        pembaruanStatus.to_status = pembaruanStatus.STATUS_ADMIN;

        permohonanPembaruan.status = permohonanPembaruan.STATUS_KLARIFIKASI;

        int saveData = permohonanPembaruan.save();

        msg = "Hasil koordinasi";

        if (saveData > 0) {
            pembaruanRiwayat.addSendToAdminKoordinasi(permId);
            pembaruanStatus.save();

            flash.success(msg + " berhasil dikirimkan.");
        } else {
            flash.error(msg + " gagal dikirimkan.");
        }

        redirect("katalog.PermohonanPembaruanCtr.listKoordinasi", permId);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DETAIL})
    public static void formDetailPersetujuan(long permId){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", permId);
        String urlDetailPenyedia = Router.reverse("katalog.PermohonanPembaruanCtr.infoDetailPenyedia", params).url;
        String urlListProduk = Router.reverse("katalog.PermohonanPembaruanCtr.listApprovalProdukPembaruanStaging", params).url;
        String urlListDistributor = Router.reverse("katalog.PermohonanPembaruanCtr.listApprovalDistributorPembaruanStaging", params).url;

        PermohonanPembaruan permohonanPembaruan = PermohonanPembaruan.findById(permId);
        allowToAccess(permohonanPembaruan);

        if (permohonanPembaruan.tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_PROFIL)) {
            redirect(urlDetailPenyedia);
        } else if (permohonanPembaruan.tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_PRODUK)) {
            redirect(urlListProduk);
        } else if (permohonanPembaruan.tipe_permohonan.equalsIgnoreCase(PermohonanPembaruan.TIPE_PERMOHONAN_DISTRIBUTOR)) {
            redirect(urlListDistributor);
        }
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KONTRAK})
    public static void ListPenawaranKontrak(long id){
        boolean isAdmin = false, dapatDiselesaikan = false;
        List<PembaruanKontrak> kontrak = PembaruanKontrak.findActiveByPermohonan(id);
        PermohonanPembaruan permohonanPembaruan = PermohonanPembaruan.findById(id);
        allowToAccess(permohonanPembaruan);
        List<PembaruanStaging> produkStaging = PembaruanStaging.getIdProdukByPermohonanId(id);
        String ids = produkStaging.stream().map(PembaruanStaging::getProdukIdIfTypeProduk).collect(Collectors.joining(","));
        List<Penawaran> penawaran = Penawaran.findPenawaranByProduk(ids);

        AktifUser au = AktifUser.getAktifUser();
        String jabatan = au.jabatan != null ? au.jabatan : "";
        isAdmin = jabatan.isEmpty() && (au.isAdmin() || au.isAdminLokalSektoral()) ? true : false;

        if (null != kontrak && kontrak.size() == penawaran.size() && permohonanPembaruan.status.equalsIgnoreCase(permohonanPembaruan.STATUS_ADENDUM_KONTRAK) && isAdmin) {
            dapatDiselesaikan = true;
        }

        renderArgs.put("dapatDiselesaikan", dapatDiselesaikan);
        render("katalog/ProdukCtr/permohonan/listPenawaranKontrak.html",id, penawaran);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KONTRAK})
    public static void listKontrak(long id, long penawaran_id){
        boolean showButtonTambah = false, isAdmin = false, dapatDiselesaikan = false, sudahAdaKontrak = false;
        List<PembaruanKontrak> kontrak = PembaruanKontrak.findActiveByPenawaran(id, penawaran_id);
        PermohonanPembaruan permohonanPembaruan = PermohonanPembaruan.findById(id);
        PembaruanStatus ps = PembaruanStatus.getLatestStatusByPermohonanId(id);
        String statusPmb = permohonanPembaruan.status;

        AktifUser au = AktifUser.getAktifUser();
        String jabatan = au.jabatan != null ? au.jabatan : "";
        isAdmin = jabatan.isEmpty() && (au.isAdmin() || au.isAdminLokalSektoral()) ? true : false;

        if (statusPmb.equalsIgnoreCase(permohonanPembaruan.STATUS_ADENDUM_KONTRAK)
                && ps.to_status.equalsIgnoreCase(ps.STATUS_ADMIN) && isAdmin){
            showButtonTambah = true;
        }

        sudahAdaKontrak = kontrak.size() > 0 ? true : false;

//        if (null != kontrak && kontrak.size() > 0 && statusPmb.equalsIgnoreCase(permohonanPembaruan.STATUS_ADENDUM_KONTRAK) && isAdmin) {
//            dapatDiselesaikan = true;
//        }

        renderArgs.put("showButtonTambah", showButtonTambah);
        renderArgs.put("isAdmin", isAdmin);
//        renderArgs.put("dapatDiselesaikan", dapatDiselesaikan);
        renderArgs.put("sudahAdaKontrak", sudahAdaKontrak);
        render("katalog/ProdukCtr/permohonan/listPermohonanKontrak.html", kontrak, id, penawaran_id, sudahAdaKontrak);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KONTRAK_ADD})
    public static void createKontrak(long permId, long penawaranId) {
        render("katalog/ProdukCtr/permohonan/createKontrak.html", permId, penawaranId);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KONTRAK_ADD})
    public static void createKontrakSubmit(PembaruanKontrak kontrak, String tgl_mulai, String tgl_selesai, File file) {
        try {
            Date tgl_mulai_format = new SimpleDateFormat("dd-MM-yyyy").parse(tgl_mulai);
            Date tgl_selesai_format = new SimpleDateFormat("dd-MM-yyyy").parse(tgl_selesai);

            kontrak.tgl_masa_berlaku_mulai = tgl_mulai_format;
            kontrak.tgl_masa_berlaku_selesai = tgl_selesai_format;
            kontrak.active = 1;
            boolean msg = kontrak.kontrakSubmit(kontrak, file);
            if (msg) {
                flash.success("Data berhasil disimpan");
            } else {
                flash.error("Data tidak berhasil disimpan");
            }

            redirect("katalog.PermohonanPembaruanCtr.listKontrak", kontrak.permohonan_pembaruan_id, kontrak.penawaran_id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KONTRAK_EDIT})
    public static void editKontrak(long id) {
        PembaruanKontrak kontrak = PembaruanKontrak.findById(id);
        DateFormat format_tanggal = new SimpleDateFormat("dd-MM-yyyy");
        String tgl_kontrak_mulai = format_tanggal.format(kontrak.tgl_masa_berlaku_mulai);
        String tgl_kontrak_selesai = format_tanggal.format(kontrak.tgl_masa_berlaku_selesai);
        render("katalog/ProdukCtr/permohonan/editKontrak.html", kontrak, tgl_kontrak_mulai, tgl_kontrak_selesai);
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_KONTRAK_EDIT})
    public static void editKontrakSubmit(PembaruanKontrak kontrak, String tgl_mulai, String tgl_selesai, File file) {
        try {
            Date tgl_mulai_format = new SimpleDateFormat("dd-MM-yyyy").parse(tgl_mulai);
            Date tgl_selesai_format = new SimpleDateFormat("dd-MM-yyyy").parse(tgl_selesai);

            PembaruanKontrak pembaruanKontrak = PembaruanKontrak.findById(kontrak.id);
            pembaruanKontrak.no_kontrak = kontrak.no_kontrak;
            pembaruanKontrak.deskripsi = kontrak.deskripsi;
            pembaruanKontrak.tgl_masa_berlaku_mulai = tgl_mulai_format;
            pembaruanKontrak.tgl_masa_berlaku_selesai = tgl_selesai_format;
            pembaruanKontrak.active = 1;

            boolean msg = pembaruanKontrak.kontrakSubmit(pembaruanKontrak, file);
            if (msg) {
                flash.success("Data berhasil diubah");
            } else {
                flash.error("Data tidak berhasil diubah");
            }

            redirect("katalog.PermohonanPembaruanCtr.listKontrak", pembaruanKontrak.permohonan_pembaruan_id, pembaruanKontrak.penawaran_id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AllowAccess({Acl.COM_PERMOHONAN_PEMBARUAN_DISTRIBUTOR})
    public static void detailDistributor(long id){
        PembaruanStaging pmb = PembaruanStaging.findById(id);
        renderArgs.put("pmbstagging", pmb);
//        ProdukStagging psg = pmb.getIfProdukStagging();

        DistributorStagging dstg = pmb.getIfDistributorStagging();
        List<PenyediaDistributorRepresentatif> listRepresentatifAwal = new ArrayList<>();
        PenyediaDistributor distributorAwal = PenyediaDistributor.findById(dstg.distributor.id);
        if(null == distributorAwal ){
            listRepresentatifAwal = dstg.listRepresentatif;
            distributorAwal = dstg.distributor;
        }else{
            listRepresentatifAwal = PenyediaDistributorRepresentatif.findByDistributor(distributorAwal.id);
        }

//        Produk produk = Produk.findById(psg.produk_id);
        PenyediaDistributor distributor = dstg.distributor;
        distributor.representatifs = dstg.listRepresentatif;
        render("katalog/ProdukCtr/permohonan/detailDistributor.html",distributor, distributorAwal, listRepresentatifAwal);
    }
}
