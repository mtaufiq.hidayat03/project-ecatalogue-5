package repositories.produkkategori;

import models.BaseKatalogTable;
import models.katalog.Produk;
import models.katalog.ProdukAtributValue;
import models.katalog.ProdukKategoriAtribut;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.db.jdbc.Query;

import java.util.*;
import java.util.stream.Collectors;

import static constants.models.ProdukKategoriAtributConstant.*;

public class ProdukKategoriAtributRepository {

	public static final String TAG = "KategoriAtributRepo";

	/**
	 * @param produk_kategori_id
	 * @param type information type either specification or additional information
	 * @return list of attributes*/
	public static List<ProdukKategoriAtribut> findAllActive(long produk_kategori_id, String type) {
		String query = "SELECT pka." + ID
				+ ", pka." + ATRIBUTE_TYPE_ID
				+ ", pka." + ATRIBUTE_TYPE_LABEL
				+ ", pka." + PRODUK_KATEGORI_ID
				+ ", pka." + PARENT_ID
				+ ", pka." + LABEL_ATRIBUT
				+ ", pka." + DESKRIPSI
				+ ", pka." + TIPE_INPUT
				+ ", pka." + WAJIB_DIISI
				+ " FROM " + TABLE_NAME + " pka"
				+ " WHERE pka." + ACTIVE + " = 1 "
				+ " AND pka." + PRODUK_KATEGORI_ID + " =? " +
				//"AND pka.komoditas_produk_atribut_tipe_label =? " +
				"ORDER BY pka." + ATRIBUTE_TYPE_LABEL + " ASC, pka." + POSISI_ITEM + " ASC, pka." + ID + " ASC ";
		Logger.debug("active categories: " + query);
		return Query.find(query, ProdukKategoriAtribut.class, produk_kategori_id).fetch();
	}

	/**
	 * get nested produk kategori atribut max 2 level
	 * @param pkid produk_komoditas_id
	 * @return ProdukKategoriAtribut Collection
	 * */
	public static List<ProdukKategoriAtribut> getNestedList(long pkid) {
		List<ProdukKategoriAtribut> results = getList(pkid, "");
		Logger.debug(TAG + " total parent: " + results.size());
		setChildren(results, pkid);
		return results;
	}

	public static List<ProdukKategoriAtribut> getList(long pkid, String ids) {
		String parentFilter = " AND pka." + PARENT_ID + " = 0";
		if (!TextUtils.isEmpty(ids)) {
			parentFilter = " AND pka." + PARENT_ID + " IN (" + ids + ")";
		}
		String query = "SELECT pka." + ID
				+ ", pka." + ATRIBUTE_TYPE_ID
				+ ", pka." + ATRIBUTE_TYPE_LABEL
				+ ", pka." + PRODUK_KATEGORI_ID
				+ ", pka." + PARENT_ID
				+ ", pka." + LABEL_ATRIBUT
				+ " FROM " + TABLE_NAME + " pka"
				+ " WHERE pka." + ACTIVE + " = 1 "
				+ " AND pka." + PRODUK_KATEGORI_ID + "=?"
				+ parentFilter
				+ " ORDER BY pka." + POSISI_ITEM + " ASC, pka." + ID + " ASC ";
		return Query.find(query, ProdukKategoriAtribut.class, pkid).fetch();
	}

	public static void setChildren(List<ProdukKategoriAtribut> lists, long pkid) {
		if (!lists.isEmpty()) {
			String parents = lists
					.stream()
					.map(p -> "'" + String.valueOf(p.id) + "'")
					.collect(Collectors.joining(","));
			Logger.debug(TAG + " parent ids: " + parents);
			List<ProdukKategoriAtribut> attributes = getList(pkid, parents);
			if (attributes != null && !attributes.isEmpty()) {
				Map<Long, List<ProdukKategoriAtribut>> results = attributes
						.stream()
						.collect(Collectors
								.groupingBy(ProdukKategoriAtribut::getParent));
				for (ProdukKategoriAtribut model : lists) {
					if (results.containsKey(model.id)) {
						model.children.addAll(results.get(model.id));
					}
				}
			}
		}
	}

	public static List<ProdukAtributValue> findByProduk(Produk produk) {
		String sql = "SELECT komoditas_produk_atribut_tipe_label"
				+ ", label_atribut"
				+ ", atribut_value"
				+ " FROM produk_kategori_atribut pka"
				+ " LEFT JOIN produk_atribut_value pkv ON pka.id=pkv.produk_kategori_atribut_id "
				+ " WHERE produk_kategori_id=? AND produk_id=?"
				+ " ORDER BY posisi_item";
		return Query.find(sql, ProdukAtributValue.class, produk.produk_kategori_id, produk.id).fetch();
	}

	public static Map<Long, ProdukKategoriAtribut> getByIds(String ids) {
		final String query = " SELECT * FROM produk_kategori_atribut" +
				" WHERE id IN ("+ids+")";
		List<ProdukKategoriAtribut> attributes = Query.find(query, ProdukKategoriAtribut.class).fetch();
		Map<Long, ProdukKategoriAtribut> results = new HashMap<>();
		if (!attributes.isEmpty()) {
			for (ProdukKategoriAtribut model : attributes) {
				results.put(model.id, model);
			}
		}
		return results;
	}

	/**
	 * @param id produk kategori id
	 * @return single detail at first element*/
	public static ProdukKategoriAtribut getAtibutByKategori(long id) {
		return ProdukKategoriAtribut.find("produk_kategori_id =?", id).first();
	}

	/**
	 * @param ids reconstructed produk kategori atribut ids
	 * @return a map with produk kategori atribut id as the key*/
	public static Map<Long, ProdukKategoriAtribut> getAtributMap(String ids) {
		List<ProdukKategoriAtribut> list = ProdukKategoriAtribut.find(ID + " IN ("+ids+")").fetch();
		if (!list.isEmpty()) {
			return list.stream().collect(Collectors.toMap(ProdukKategoriAtribut::getId, p -> p));
		}
		return Collections.emptyMap();
	}

	public static Long getTotalCategoriAttributeByAttrId(Long id) {
		return ProdukKategoriAtribut.count("komoditas_produk_atribut_tipe_id =? AND active = ?", id, BaseKatalogTable.AKTIF);
	}

}
