package jobs.JobTrail;

import com.google.gson.GsonBuilder;
import models.elasticsearch.SearchResult;
import models.elasticsearch.query.ElasticsearchUtil;
import models.elasticsearch.query.Sort;
import models.elasticsearch.query.aggregation.InnerTermAggregation;
import models.elasticsearch.query.aggregation.MetricAggregation;
import models.elasticsearch.query.aggregation.TermAggregation;
import models.elasticsearch.query.aggregation.TopHitAggregation;
import models.elasticsearch.response.ResponseProduct;
import play.Logger;
import play.cache.Cache;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import retrofit2.Call;
import retrofit2.Response;
import services.elasticsearch.ElasticsearchConnection;

import static constants.models.ElasticProductConstant.ID_KOMODITAS;
import static repositories.elasticsearch.ElasticsearchRepository.POPULAR_CACHE_KEY;

@NoTransaction
public class JobPopularProduk extends Job {
    String elasticurls = "";
    String modelUrl = "";
    String searchlUrl = "";
    public JobPopularProduk(String _elasticurls, String _modelUrl, String _searchlUrl){
        elasticurls = _elasticurls;
        modelUrl = _modelUrl;
        searchlUrl = searchlUrl;
    }
    public void doJob(){
        try {
            SearchResult result = new SearchResult();
            InnerTermAggregation innerTermAggregation = new InnerTermAggregation();
            innerTermAggregation.setTerm(new MetricAggregation()
                    .setMethod(MetricAggregation.METHOD_MAX)
                    .setAlias("total_purchased")
                    .setField("total_pembelian"))
                    .topHitAggregasion(new TopHitAggregation()
                            .setAlias("hit_list")
                            .size(1)
                            .sort(new Sort("total_pembelian", "desc")));
            ElasticsearchUtil.QueryUtil builder = new ElasticsearchUtil.QueryUtil()
                    .setAggregation(new TermAggregation()
                            .setAlias("commodity")
                            .setField(ID_KOMODITAS)
                            .setSize(12)
                            .setSort(new Sort("total_purchased", "desc"))
                            .setInnerAggregation(innerTermAggregation))
                    .setLimit(0);
            Logger.debug(new GsonBuilder().setPrettyPrinting().create().toJson(builder.buildRequest()));
            Call<ResponseProduct> call = ElasticsearchConnection.open().getProduct(builder.buildRequest(),elasticurls+modelUrl+searchlUrl);
            Response<ResponseProduct> responseCall = call.execute();
            if (ElasticsearchConnection.isSucceed(responseCall.code())) {
                ResponseProduct response = responseCall.body();
                if (response != null) {
                    result = response.getPopularResult();
                    try{
                        Cache.safeDelete(POPULAR_CACHE_KEY);
                    }catch (Exception e){}
                    Cache.set(POPULAR_CACHE_KEY, result, "10min");
                }
            }
        } catch (Throwable t) {
            t.printStackTrace();
            Logger.error("error elastic get popular list: from server");
        }
    }
}
