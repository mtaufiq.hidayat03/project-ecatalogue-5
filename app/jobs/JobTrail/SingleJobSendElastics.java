package jobs.JobTrail;

import com.google.gson.Gson;
import jobs.JobTrail.viewmodels.ResultFeedBackEs;
import jobs.TrackableJob;
import jobs.elasticsearch.ElasticConnection;
import models.util.Config;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.libs.F;
import play.libs.WS;
import services.elasticsearch.ElasticsearchConnection;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static jobs.elasticsearch.ElasticSearchTextLog.*;

@NoTransaction
public class SingleJobSendElastics extends TrackableJob {
    Gson gson = new Gson();
    public int page = 1;
    public int tData = 1000;
    int wait = 10;
    int next=0;
    int countRetry=0;
    int stopRetry = 5;
    public boolean successSend = false;
    String _sb = "";
    ElasticConnection _elasticConnection;
    Config conf = Config.getInstance();
    public SingleJobSendElastics(String sb, int _page, int _tData){
        _sb = sb;
        _elasticConnection = new ElasticConnection(true);
        page = _page;
        tData = _tData;
    }
    public SingleJobSendElastics(String sb, int _tData){
        _sb = sb;
        _elasticConnection = new ElasticConnection(true);
        tData = _tData;
    }

    public void doJob(){
        F.Promise<WS.HttpResponse> response = null;
        String logs = "sending data produk ke elastic: page->"+page+" total:"+tData;
        writeLogGeneral(logs);
        Logger.info(logs);
        String urls = ElasticsearchConnection.getElasticurls()+ElasticConnection.modelUrl+ElasticConnection.bulklUrl;
        WS.HttpResponse exist = WS.url(conf.elasticsearchHost+"/").get();
        if(exist.success()){
            do{
                successSend = false;
                countRetry++;
                String waitTime = (wait+next)+"mn";
                logs = waitTime+" waittime post json page:"+page+", trynumber:"+countRetry;
                Logger.info(logs);
                response = WS.url(urls).timeout(waitTime).body(_sb).postAsync();
                if(null != response){
                    try {
                        //report post
                        String report = response.get().getString();
                        ResultFeedBackEs restObj = gson.fromJson(report, ResultFeedBackEs.class);
                        int okObjekt = restObj.items.stream().filter(f-> f.create.status==201).collect(Collectors.toList()).size();
                        int nobjekt = restObj.items.stream().filter(f-> f.create.status==409).collect(Collectors.toList()).size();
                        logs = "response NOT null sending page :"+page+", containt failed object:"+restObj.errors+", success:"+okObjekt+", failed:"+nobjekt;
                        writeLogGeneral(logs);
                        Logger.info(logs);
                        String directory = conf.aktifUserLogTrail+"/elasticsearch/";
                        directory = createSubDirDate(directory);
                        directory += "/"+page+"_report.txt";
                        File f = new File(directory);
                        writeNioFile(f,report);
                        successSend = true;
                    } catch (InterruptedException e) {
                        logs ="error InterruptedException page:"+page+", desc:"+e.getMessage();
                        writeLogGeneral(logs);
                        Logger.info(logs);
                        response = null;
                        successSend = false;
                    } catch (ExecutionException e) {
                        logs = "error ExecutionException page:"+page+", desc:"+e.getMessage();
                        writeLogGeneral(logs);
                        Logger.info(logs);
                        response = null;
                        successSend = false;
                    }
                }else{
                    logs = "response NULL sending page :"+page;
                    writeLogGeneral(logs);
                    successSend = false;
                }
                if(countRetry > stopRetry){
                    successSend = true;
                    //stop retry
                }
                next++;
            }while (successSend == false);
        }else{
            logs = "elastic host not exist";
            Logger.info(logs);
            writeLogGeneral(logs);
        }
        logs = "done sending data produk ke elastic page:"+page+", success:"+successSend+", retrycount:"+countRetry;
        writeLogGeneral(logs);
        Logger.info(logs);
    }
}
