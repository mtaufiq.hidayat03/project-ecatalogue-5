package models.util.produk;

import java.util.List;

/**
 * Created by dadang on 8/23/17.
 */
public class HargaProvinsiJson {
	public long provinsiId;
	public double harga_utama;
	public double harga_pemerintah;
	public double harga;
	public List<HargaKabupatenJson> kabs;

	public HargaProvinsiJson(long provinsiId, double harga_utama, double harga_pemerintah){
		this.provinsiId = provinsiId;
		this.harga_utama = harga_utama;
		this.harga_pemerintah = harga_pemerintah;
	}

	public HargaProvinsiJson(long provinsiId, double harga_utama, double harga_pemerintah, double harga){
		this.provinsiId = provinsiId;
		this.harga_utama = harga_utama;
		this.harga_pemerintah = harga_pemerintah;
		this.harga = harga;
	}
}
