package controllers;

import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.time.StopWatch;

import controllers.jcommon.http.IfModifiedSince;
import models.katalog.Produk;
import models.user.CentrumUser;
import models.util.Config;
import play.Logger;
import play.cache.Cache;
import play.cache.CacheFor;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.mvc.Before;
import play.mvc.Finally;
import play.mvc.Router;

/**
 * Controller ini berisi fitur-fitur penting yang perlu diterapkan di e-katalog
 * 
 * @author User
 */
public class DevelopmentCtr extends BaseController {

	private static ThreadLocal<StopWatch> tl = new ThreadLocal<>();

//	///// Interceptor untuk mengecek renderTime
//	@Before
//	protected static void beforeRender() {
//		StopWatch sw = new StopWatch();
//		sw.start();
//		tl.set(sw);
//	}
//
//	@Finally
//	protected static void afterRender() {
//		StopWatch sw = tl.get();
//		sw.stop();
//		// Logger.debug("[DevelopmentCtr] Dur: %s, URL %s", sw, request.url);
//	}

	public static void index() {
		render();
	}

	public static void clearCache() {
		play.cache.Cache.clear();
		index();
	}

	/**
	 * Penggunaan @IfModifiedSince/IMS akan menyebabkan setiap Session di
	 * browser menyimpan di Cache selama 10 menit. IMS tidak berlaku untuk
	 * session yang berbeda. Jadi jika komputer A sudah request lalu komputer B
	 * melakukan request maka Play akan mengeksekusi browserCache()
	 * 
	 * Untuk mencegah hal ini maka ditambahkan @CacheFor di maka Play akan
	 * menyimpan di-Cache hasil render dari komputer A lalau mengirim value
	 * Cache ini saat komputer B request.
	 * 
	 * Jadi kedua anotasi ini digunakan secara bersama-sama
	 *
	 * //@param id
	 */
	@IfModifiedSince("10min")
	@CacheFor("5min")
	public static void browserCache() {
		StopWatch sw = new StopWatch();
		sw.start();
		List<Produk> list = Produk.findByProduk_kategori_id(154);
		sw.stop();
		render(list, sw);
	}
	
	public static void cariProdukDataTable()
	{
		render();
	}
	
	/**
	 * Penggunaan @IfModifiedSince akan menyebabkan setiap Session di browser
	 * menyimpan di Cache selama 10 menit
	 * 
	 * @param id
	 */
	@IfModifiedSince("10min")
	@CacheFor("5min")
	public static void produkView(Long id) {
		Produk produk = Produk.findById(id);
		render(produk);
	}

	@IfModifiedSince("10min")
	@CacheFor("5min")
	public static void searchProduk(String nama_produk) {
		StopWatch sw = new StopWatch();
		sw.start();
		List<Produk> list = Produk.findByNama(nama_produk);
		sw.stop();
		render("DevelopmentCtr/browserCache.html", sw, list);
	}

/////////////////////////////////////////	////////////////////////////////////////////////////////////////////////////////////	///////////////////////////////////////////
	/////////////////////////////////////////// action terkait Centrum 	/////////////////////////////////////////// 
	///////////////////////////////////////////	/////////////////////////////////////////// /////////////////////////////////////////	///////////////////////////////////////////
	public static void loginCentrum() {
		/**
		 * String params = new
		 * StringBuilder(active_user.userid).append("|").append(active_user.name).append("|")
		 * .append(active_user.group).append("|").append(ConfigurationDao.getRepoId()).append("|")
		 * .append(ConfigurationDao.isProduction()).append("|").append(FormatUtils.ttsDateFormat.format(newDate()))
		 * .toString();
		 */

		render();
	}

	public static void loginCentrumSubmit(String user_id, String name, String group, Integer repo_id, boolean is_prod) {
		Config conf = Config.getInstance();
		String redirect=Router.reverse("DevelopmentCtr.loginCentrumCallback").url;
		/* staging user id di katalog tdk ada karena tidak punya PK yg integer
		 *  
		 */
		String staging_user_id = String.valueOf(repo_id + (user_id.hashCode() + group.hashCode()) * 1000);
		String url=String.format("%s/StagingCtr/login?"
				+ "user.staging_user_id=%s&user.user_name=%s&user.nama=%s&user.role_id=KATALOG$%s&user.lpse_id=%s&user.app_id=%s"
				+ "&redirect_uri=%s"
			,conf.centrum_url,	 staging_user_id, user_id, name, group, repo_id, "KATALOG", redirect);
		redirect(url);
	}
	
	public static void loginCentrumCallback(String code)
	{
		Config conf = Config.getInstance();
		String redirect=Router.reverse("controllers.DevelopmentCtr.loginCentrumSukses").url;
		String url=String.format("%s/OAuthCtr/token?client_id=%s&client_secret=%s&grant_type=authorization_code&code=%s&redirect_uri=%s",
				conf.centrum_url,conf.centrum_client_id, conf.centrum_client_secret, code, redirect);
		HttpResponse resp= WS.url(url).get();
		String str=resp.getString();
		CentrumUser user=CentrumUser.getInstance(str);
		Cache.set(code+"centrumUser", user,"1min");
		Logger.debug("[loginCentrumCallback] %s: %s", resp.getStatus(), user);
		loginCentrumSukses(code);
	}
	
	public static void loginCentrumSukses(String code)
	{
		CentrumUser user=Cache.get(code+"centrumUser", CentrumUser.class);
		SessionInfo info=new SessionInfo(user.info.nama, 0, user.info.user_id, code, false, false, null, null, null, null);
		info.saveToSession();

		
		/**Jika satu User punya beberapa Role, centrum akan mengembalikan seluruh Role tersebut.
		 */
		flash.success("LOGIN Via Centrum Sukses dilakukan; user_id: %s, roles: %s", 
				user.info.user_name, ArrayUtils.toString(user.info.role_codes));
		index();
	}

}
