package utils.performance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.time.StopWatch;

import play.mvc.Http.Request;

public class ActionMonitor {

	public static Map<String, ActionEntry> actions=new HashMap();
	
	public static synchronized void add(Request request, StopWatch sw)
	{
		String actionName=request.action;
		ActionEntry action=actions.get(actionName);
		if(action==null)
		{
			action=new ActionEntry(actionName);
			actions.put(actionName, action);
		}
		action.count++;
		if(sw.getTime() > action.duration)
			action.duration=sw.getTime();
	}
	
	public static List<ActionEntry> getActionsByCount()
	{
		List<ActionEntry> list=new ArrayList(actions.values());
		Collections.sort(list, new ActionEntry.CountComparator());
		return list;
	}
	
	public static List<ActionEntry> getActionsByDuration()
	{
		List<ActionEntry> list=new ArrayList(actions.values());
		Collections.sort(list, new ActionEntry.DurationComparator());
		return list;
	}
}
