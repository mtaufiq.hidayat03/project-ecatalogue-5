package controllers.katalog;

import ext.contracts.PaginateAble;
import ext.contracts.ParamAble;
import models.elasticsearch.SearchQuery;
import models.katalog.Produk;
import org.apache.http.message.BasicNameValuePair;
import play.mvc.Scope;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 12/22/2017
 */
public class ProductSearchResult implements PaginateAble {

    public List<Produk> products = new ArrayList<>();
    public long total = 0;
    private ParamAble paramAble;

    public ProductSearchResult(ParamAble paramAble) {
        this.paramAble = paramAble;
    }

    public ProductSearchResult(Scope.Params params, List<Produk> _products) {
        products = _products;
        setTotal(new Long(_products.size()));
        paramAble = new SearchQuery(params);
    }
    @Override
    public long getTotal() {
        return total;
    }

    @Override
    public long getTotalCurrentPage() {
        return products.size();
    }

    @Override
    public int getMax() {
        return 10;
    }

    @Override
    public int getPage() {
        return paramAble.getPage();
    }

    @Override
    public List<BasicNameValuePair> getParams() {
        return paramAble.getParams();
    }

    @Override
    public void setTotal(Long total) {
        this.total = total;
    }

}
