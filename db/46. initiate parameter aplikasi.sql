INSERT INTO `parameter_aplikasi` (nama_parameter, isi_parameter, deskripsi, active)
VALUES
("website_lkpp", "http://www.lkpp.go.id", "Website LKPP.", 1),
("faksimili_lkpp", "(021) 299 12451", "Nomor Fax LKPP.", 1),
("lokasi_lkpp", "Gedung  LKPP, Komplek Rasuna Epicentrum, Jl. Epicentrum Tengah Lot 11B, Jakarta Selatan", "Lokasi/Alamat LKPP", 1);