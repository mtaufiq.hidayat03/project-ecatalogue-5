package controllers.admin;

import controllers.BaseController;
import controllers.security.AllowAccess;
import models.jcommon.util.CommonUtil;
import models.katalog.komoditasmodel.KategoriKomoditas;
import models.secman.Acl;
import play.Logger;
import utils.HtmlUtil;

public class KategoriKomoditasCtr extends BaseController {

	@AllowAccess({Acl.COM_KOMODITAS_KATEGORI})
	public static void index() {
		render("admin/KategoriKomoditasCtr/index.html");
	}

	@AllowAccess({Acl.COM_KOMODITAS_KATEGORI_ADD})
	public static void create() {
		/*KategoriKomoditas katkom = new KategoriKomoditas();

		render(katkom);*/
		index();
	}

	@AllowAccess({Acl.COM_KOMODITAS_KATEGORI_ADD})
	public static void createSubmit(KategoriKomoditas katkom){
		//encode decode
		//Logger.info(katkom.deskripsi);
		String decodedText = HtmlUtil.decodeFromTinymce(katkom.deskripsi);
		//Logger.info(decodedText);
		katkom.deskripsi = decodedText;

		if(!validation.required(katkom.nama_kategori).ok)
			validation.addError("katkom.nama_kategori", "Nama Kategori Harus Diisi", "var");

		if (validation.hasErrors()){
			flash.error("Gagal Simpan Kategori Komoditas, cek kembali inputan anda.");
			render("admin/KategoriKomoditasCtr/create.html", katkom);
		}else{
			katkom.active = true;
			katkom.save();
			flash.success("Data berhasil disimpan");
			index();
		}

	}

	@AllowAccess({Acl.COM_KOMODITAS_KATEGORI_EDIT})
	public static void edit(long id){
		KategoriKomoditas katkom = KategoriKomoditas.findById(id);
		render("admin/KategoriKomoditasCtr/edit.html", katkom);
	}

	@AllowAccess({Acl.COM_KOMODITAS_KATEGORI_EDIT})
	public static void editSubmit(long kk_id, String nama_kategori, String deskripsi){
		//encode decode
		//Logger.info(deskripsi);
		String decodedText = HtmlUtil.decodeFromTinymce(deskripsi);
		//Logger.info(decodedText);
		deskripsi = decodedText;

		KategoriKomoditas katkom = KategoriKomoditas.findById(kk_id);
		katkom.nama_kategori = nama_kategori;
		katkom.deskripsi = deskripsi;
		katkom.active = true;

		if (CommonUtil.isEmpty(nama_kategori))
			validation.addError("katkom.nama_kategori", "Nama Kategori Harus Diisi", "var");

		if (validation.hasErrors()){
			flash.error("Gagal Ubah Kategori Komoditas, cek kembali inputan anda.");
			render("admin/KategoriKomoditasCtr/edit.html", katkom, kk_id);
		}

		katkom.save();

		flash.success("Data berhasil disimpan");
		index();
	}

	@AllowAccess({Acl.COM_KOMODITAS_KATEGORI_DELETE})
	public static void delete(long id){
		//KategoriKomoditas.deleteById(id);
		index();
	}

}
