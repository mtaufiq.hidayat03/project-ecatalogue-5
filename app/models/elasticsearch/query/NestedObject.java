package models.elasticsearch.query;

import com.google.gson.JsonObject;

/**
 * @author HanusaCloud on 10/5/2017
 */
public abstract class NestedObject {

    protected String path;
    protected String field;

    public String getPath() {
        return this.path;
    }

    public String getFieldPath() {
        return path + "." + field;
    }

    public abstract JsonObject build();

}
