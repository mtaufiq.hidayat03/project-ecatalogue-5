package controllers.admin;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import controllers.*;
import controllers.security.AllowAccess;
import jobs.JobTrail.JobCekWs;
import models.cms.KontenStatis;
import models.cms.SyaratKetentuanHasil;
import models.common.AktifUser;
import models.jcommon.util.CommonUtil;
import models.katalog.Komoditas;
import models.lpse.PpeSite;
import models.lpse.UserFromLpse;
import models.masterdata.Kldi;
import models.penyedia.Penyedia;
import models.penyedia.PenyediaDistributor;
import models.secman.Acl;
import models.secman.UserLoggedIn;
import models.user.*;
import models.util.Config;
import models.util.Encryption;
import models.util.RekananDetail;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.cache.Cache;
import play.i18n.Messages;
import play.libs.URLs;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.mvc.Router;
import repositories.penyedia.ProviderRepository;
import utils.LogUtil;
import utils.UrlUtil;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class UserCtr extends BaseController {

	public static final String TAG = "UserCtr";

	@AllowAccess({Acl.COM_USER})
	public static void index() {
		List<UserRoleGrup> groupRoles = UserRoleGrup.findAllActive();
		List<RoleBasic> roles = RoleBasic.findAllActive();
		render("admin/UserCtr/index.html", groupRoles, roles);
	}

	@AllowAccess({Acl.COM_USER_ADD})
	public static void create() {
		List<RoleGrup> kategori = RoleGrup.findAllKategori();
		List<RoleGrup> kategori2 = kategori;
		List<RoleGrup> roleGrupList = RoleGrup.findAllActive();
		List<RoleItem> roleItemList = RoleItem.findAllActive();
		List<RoleBasic> roleBasicList = RoleBasic.findAllActive();
		List<Komoditas> komoditasList = Komoditas.findAllActive();
		List<Komoditas> komoditasListNasional = Komoditas.getKomoditasNasional();
		List<Komoditas> komoditasListLokal = Komoditas.getKomoditasLokal();
		List<Komoditas> komoditasListSektoral = Komoditas.getKomoditasSektoral();
		List<UserRoleGrup> userRoleGrups = UserRoleGrup.findAllActive();

		render("admin/UserCtr/create.html", kategori, kategori2,
				roleGrupList, roleItemList, roleBasicList, komoditasList, userRoleGrups,
				komoditasListNasional, komoditasListLokal, komoditasListSektoral);
	}

	@AllowAccess({Acl.COM_USER_EDIT})
	public static void edit(long id) {
		User user = User.findById(id);
		notFoundIfNull(user);
		LogUtil.d(TAG, user);
		List<RoleGrup> kategori = RoleGrup.findAllKategori();
		List<RoleGrup> kategori2 = kategori;
		List<RoleGrup> roleGrupList = RoleGrup.findAllActive();
		List<RoleItem> roleItemList = RoleItem.findAllActive();
		List<RoleBasic> roleBasicList = RoleBasic.findAllActive();
		List<Komoditas> komoditasList = Komoditas.findAllActive();
		List<Komoditas> komoditasListNasional = Komoditas.getKomoditasNasional();
		List<Komoditas> komoditasListLokal = Komoditas.getKomoditasLokal();
		List<Komoditas> komoditasListSektoral = Komoditas.getKomoditasSektoral();
		List<UserRoleGrup> userRoleGrups = UserRoleGrup.findAllActive();

		Set<Long> roleItemSelected = UserRoleItemOverride.findByUserIdMap(id);
		Set<Long> komoditasSelected = UserRoleKomoditasOverride.findByUserIdMap(id);
		Map<Long, String> paketSelected = UserRolePaketOverride.findByUserIdMap(id);
		List<Long> paketIds = new ArrayList<>(paketSelected.keySet());
		List<UserRolePaketOverride> listPaket = UserRolePaketOverride.findByUserId(id);
		render("admin/UserCtr/edit.html", kategori, kategori2, user, paketIds, listPaket,
				roleGrupList, roleItemList, roleBasicList, komoditasList, userRoleGrups, roleItemSelected,
				komoditasListNasional, komoditasListLokal, komoditasListSektoral, komoditasSelected, paketSelected);
	}
	/**Create user juga melakukan aktivitas di Centrum
	 * 1. memanggil WS UndangUser di Centrum
	 * 2. jika (1) sukses maka lanjutkan ke simpan user; jika gagal maka jangan simpan.
	 * 3. Password tidak diisi di sini namun diisi saat user mendaftar di Centrum.
	 * Jika email yang diisikan salah, tentunya user tidak pernah dapat kiriman email. 
	 *  
	 *  //TODO Katalog harus punya mekanisme untuk edit email dan kirim ulang undangan via Centrum
	 * 
	 * @param user
	 * @param role_item_ids
	 * @param komoditas_ids
	 * @param paket_ids
	 */

	@AllowAccess({Acl.COM_USER_ADD})
	public static void createSubmit(User user, Long[] role_item_ids, Long[] komoditas_ids, Long[] paket_ids){
		checkAuthenticity();
		validation.valid(user);
		if (validation.hasErrors()){
			validation.keep();
			String errors = new Gson().toJson(validation.errorsMap());
			Logger.debug(errors);
			params.flash();
			create();
		} else {
			Kldi kldi = Kldi.findById(user.kldi_id);
			UserRoleGrup urg = UserRoleGrup.findById(user.user_role_grup_id);
			user.role_basic_id = urg.role_basic_id;
			user.setPassword();
//			user.user_password = "#VIA_CENTRUM#";
			user.bahasa="id";
			if(kldi != null) {
				user.kldi_jenis = kldi.jenis;
			}
			user.saveUser();

			if(user.override_role_grup && !ArrayUtils.isEmpty(role_item_ids)){
				for(Long id : role_item_ids){
					new UserRoleItemOverride(id, user.id).save();
				}
			}

			if(user.override_role_komoditas && !ArrayUtils.isEmpty(komoditas_ids)){
				for(Long id : komoditas_ids){
					new UserRoleKomoditasOverride(id, user.id).save();
				}
			}

			if(user.override_role_paket && !ArrayUtils.isEmpty(paket_ids)){
				for(Long id : paket_ids){
					new UserRolePaketOverride(id, user.id).save();
				}
			}
			
//			//TODO call webService
//			Config conf=Config.getInstance();
//			String url=String.format("%s/ws/undangUser", conf.centrum_url);
//			WSRequest req= WS.url(url);
//			//String nama, String email, String client_id, String client_secret
//			req.setParameter("nama", user.nama_lengkap);
//			req.setParameter("email", user.user_email);
//			req.setParameter("client_id", conf.centrum_client_id);
//			req.setParameter("client_secret", conf.centrum_client_secret);
//			try
//			{
//				//DEV Sementara dimatikan
//				HttpResponse resp= req.post();
//				if(resp.getStatus()==200)
//					flash.success("User telah berhasil ditambahkan: %s", user.nama_lengkap);
//				else
//					throw new Exception(resp.getString());
//			}
//			catch(Exception e)
//			{
//				flash.error("Error: %s", e);
//			}
		}
		flash.success(Messages.get("notif.success.submit"));
		index();
	}

	@AllowAccess({Acl.COM_USER_DELETE})
	public static void delete(Long userId){

		User user = User.findById(userId);

		if (user != null) {
			user.active = false;
			user.save();
			flash.success(Messages.get("notif.success.delete"));
		}

		index();
	}

	@AllowAccess({Acl.COM_USER_EDIT})
	public static void editSubmit(User user, Long[] role_item_ids, Long[] komoditas_ids, Long[] paket_ids) {
		checkAuthenticity();
		validation.valid(user);
		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
			create();
		} else {
			Kldi kldi = Kldi.findById(user.kldi_id);
			User usr = User.findById(user.id);
			if (user.user_password.isEmpty()){
				user.user_password = usr.user_password;
			} else {
				user.setPassword();
			}
			user.bahasa = "id";
			user.created_date = usr.created_date;
			user.created_by = usr.created_by;
			user.modified_date = new Timestamp(new java.util.Date().getTime());
			user.modified_by = AktifUser.getActiveUserId();
			if(kldi != null) {
				user.kldi_jenis = kldi.jenis;
			}
			user.saveUser();
			UserRoleItemOverride.deleteByUserId(user.id);
			UserRoleKomoditasOverride.deleteByUserId(user.id);
			UserRolePaketOverride.deleteByUserId(user.id);
			if (user.override_role_grup && !ArrayUtils.isEmpty(role_item_ids)) {
				for (Long id : role_item_ids) {
					new UserRoleItemOverride(id, user.id).save();
				}
			}

			if (user.override_role_komoditas && !ArrayUtils.isEmpty(komoditas_ids)) {
				for (Long id : komoditas_ids) {
					new UserRoleKomoditasOverride(id, user.id).save();
				}
			}

			if(user.override_role_paket && !ArrayUtils.isEmpty(paket_ids)) {
				for (Long id : paket_ids) {
					new UserRolePaketOverride(id, user.id).save();
				}
			}
			flash.success(Messages.get("notif.success.update"));
			index();
		}
	}

	public static void logout()
	{
		boolean isCentrum = false;
		AktifUser aktifUser = getAktifUser();
		if(aktifUser !=  null){
			//isCentrum = (aktifUser != null && aktifUser.centrum_code != null && !aktifUser.centrum_code.isEmpty());
			UserLoggedIn userLoggedIn = UserLoggedIn.find("session_id = ?", session.current().getId()).first();
			if (userLoggedIn != null) {
				try {
					userLoggedIn.logoutdate = new Timestamp(new Date().getTime());
					userLoggedIn.save();
					//userLoggedIn.delete();
					//Cache.safeDelete(session.current().getId());
				} catch (Exception e) {
				}
			}
		}
		if(!Config.getInstance().aktifUserDb && aktifUser!= null) {
			try{
				//aktifUser = UserLoggedIn.getAktifUserFile(session.getId());
				//baru sampe sini cekidot ulang

				UserLoggedIn.setAktifUserFileLogout(session.getId(), aktifUser);
				//Cache.delete(session.current().getId());
				//clearUserSession();
			}catch (Exception e){
				Cache.safeDelete(session.current().getId());
			}
		}
		try{
			Cache.safeDelete(session.current().getId());
			clearUserSession();
			session.current().clear();
		}catch (Exception e){}
//		if(Config.getInstance().aktifUserDb) {
//			aktifUser = getAktifUser();
//			isCentrum = (aktifUser != null && aktifUser.centrum_code != null && !aktifUser.centrum_code.isEmpty());
//			UserLoggedIn userLoggedIn = UserLoggedIn.find("session_id = ?", session.current().getId()).first();
//			if (userLoggedIn != null) {
//				try {
//					userLoggedIn.delete();
//					Cache.safeDelete(session.current().getId());
//				} catch (Exception e) {
//				}
//			}
//
//		}else{
//			try{
//				//aktifUser = UserLoggedIn.getAktifUserFile(session.getId());
//				//baru sampe sini cekidot ulang
//				UserLoggedIn.setAktifUserFileLogout(session.getId());
//				isCentrum = (aktifUser != null && aktifUser.centrum_code != null && !aktifUser.centrum_code.isEmpty());
//				//Cache.delete(session.current().getId());
//                clearUserSession();
//			}catch (Exception e){
//				Cache.safeDelete(session.current().getId());
//			}
//		}

		if (isCentrum){
			//TODO Centrum Logout
			String url=String.format("%s/OAuthCtr/logout?code=%s&redirect_uri=%s",
					Config.getInstance().centrum_url, aktifUser.centrum_code, Router.reverse("PublikCtr.index").url);
			redirect(url);
		}
		clearLockUrl();
		HomeCtr.Home();
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////// LOGIN & CENTRUM //////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static void login()
	{
		Config conf=Config.getInstance();
		if(StringUtils.isNotEmpty(conf.centrum_url))
		{
			//https://centrum.lkpp.go.id/OAuthCtr/authenticate?client_id=xxx&redirect_uri=xxx
			String callback=Router.reverse("controllers.admin.UserCtr.loginCentrumCallback").url;
			String url=String.format("%s/OAuthCtr/authenticate?client_id=%s&redirect_uri=%s", 
					conf.centrum_url, conf.centrum_client_id, callback);
			redirect(url);
		}
		else
		render();
	}

	public static void loginCentrumCallback(String code)
	{
		Config conf = Config.getInstance();
		String redirect=Router.reverse("admin.UserCtr.loginCentrumSukses").url;
		String url=String.format("%s/OAuthCtr/token?client_id=%s&client_secret=%s&grant_type=authorization_code&code=%s&redirect_uri=%s",
				conf.centrum_url,conf.centrum_client_id, conf.centrum_client_secret, code, redirect);
		HttpResponse resp= WS.url(url).get();
		if(resp.success())
		{
			String str=resp.getString();
			CentrumUser user=CentrumUser.getInstance(str);
			Cache.set(code+"centrumUser", user,"1min");
			Logger.debug("[loginCentrumCallback] %s: %s", resp.getStatus(), user.info);
			loginCentrumSukses(code);
		}
		else
			forbidden("Otentikasi via centrum gagal");
	}

	public static void loginCentrumSukses(String code)
	{
		CentrumUser centrumUser=Cache.get(code+"centrumUser", CentrumUser.class);
		long userId = User.saveOrUpdateUsrCentrum(centrumUser);
		if(userId != 0){
			//TODO Case belum ditangani: ketika user login dari centrum dan barusan aktivasi.
//			setRoles(User.findById(userId));
		}

		/**Jika satu User punya beberapa Role, centrum akan mengembalikan seluruh Role tersebut.
		 */
		flash.success("LOGIN Via Centrum Sukses dilakukan; user_id: %s, roles: %s",
				centrumUser.info.user_name, ArrayUtils.toString(centrumUser.info.role_codes));
		//TODO Isikan paramater yang tepat sesuai yang dari database.
		SessionInfo sessionInfo = new SessionInfo(centrumUser.info.nama, 0, centrumUser.info.user_id, "",
				false, false, null, null, null, null);
//		sessionInfo.saveToSession();
		DevelopmentCtr.index();
	}

	public static void aktivasiCentrum()
	{
		Logger.info("[AktivasiCentrum]");
	}
	
	public static void loginSubmit(String userId, String password)
	{
		checkAuthenticity();
		User user=User.findFirstByUserNameLoginFromKatalog(userId);
		if(user.isPasswordEquals(password))
		{

			/**Setelah sukses login lokal maka lakukan login centrum
			 */
			Config conf = Config.getInstance();
			String redirect=Router.reverse("DevelopmentCtr.loginCentrumCallback").url;
			/* staging user id di katalog tdk ada karena tidak punya PK yg integer
			 *
			 */
			Long repo_id= user.pps_id==null ? 0L : user.pps_id;
			String staging_user_id = String.valueOf(repo_id + (user.user_name.hashCode() + user.role_basic_id) * 1000);
			String url=String.format("%s/StagingCtr/login?"
					+ "user.staging_user_id=%s&user.user_name=%s&user.nama=%s&user.email=%s&"
					+ "user.role_id=KATALOG$%s&user.lpse_id=%s&user.app_id=%s"
					+ "&redirect_uri=%s"
				,conf.centrum_url
				, staging_user_id, user.user_name, user.nama_lengkap, user.user_email,
				user.getRoleBasic().nama_role,
				repo_id, "KATALOG", redirect);
			redirect(url);
//
//			flash.success("Login berhasil: %s", user.nama_lengkap);
//			SessionInfo info=new SessionInfo(user.nama_lengkap + " - " + user.getRoleBasic().nama_role, user.role_basic_id, user.id);
//			info.saveToSession();
//			DevelopmentCtr.index();
		}
		else
		{
			flash.error("User ID/Passworservices/datatable/PenawaranDataTableServiced salah");
			login();
		}
	}

//    public static void cekBlacklist(RekananDetail rd){
//        try {
//            Config config = Config.getInstance();
//            String urlAuth = config.blacklist_api_bynpwp + "?arg="+rd.npwp;
//            LogUtil.d(TAG, urlAuth);
//            String content = WS.url(urlAuth).timeout("1min").getAsync().get().getString();
//            //LogUtil.d(TAG, content);
//            Logger.info("cek BLACKLIST:"+content);
//        } catch (InterruptedException e) {
//            //LogUtil.d(TAG, "error interrupted exc:"+e.getMessage());
//        } catch (ExecutionException e) {
//            //LogUtil.d(TAG, "error ExecutionException exc:"+e.getMessage());
//        }
//    }

	public static void loginPenyediaSubmit(String username, String password) {
		LogUtil.d(TAG, "login provider");
		try {

			//cek apakah penyedia dan atau distributor
			User userTmp = User.findFirstByUserNameLoginFromKatalog(username);
			if(userTmp!=null){
				userTmp.isProviderAndDistributor();
				RoleBasic roleBasic = RoleBasic.findById(userTmp.role_basic_id);
				if(!(roleBasic.nama_role.equalsIgnoreCase(RoleBasic.PENYEDIA_NAME) ||
						roleBasic.nama_role.equalsIgnoreCase(RoleBasic.DISTRIBUTOR_NAME) ||
						roleBasic.nama_role.equalsIgnoreCase(RoleBasic.PENYEDIA_DISTRIBUTOR_NAME))){
					flash.error(Messages.get("login_gagal.label") + ", " + Messages.get("notif_failed_wrongloginpage") + "!");
					redirectToLoginPage("non-penyedia");
				}
			}

			Config config = Config.getInstance();
			JsonObject params = new JsonObject();
			params.addProperty("username", username.toUpperCase());
			params.addProperty("password", DigestUtils.md5Hex(password));
			params.addProperty("remoteAddress", request.remoteAddress);
			params.addProperty("repoId", config.repo_id);
			//119 lpse lkpp
			//24
			String param = URLs.encodePart(Encryption.encrypt(params.toString().getBytes()));
			String urlAuth = config.adp_login_api + "?q=" + param;
			LogUtil.d(TAG, urlAuth);
			//Logger.info(urlAuth);
			String content = WS.url(urlAuth).timeout("5min").getAsync().get().getString();
			Logger.info(content);
			String response = Encryption.decrypt(content);
			LogUtil.d(TAG, "response: "+ response);
			//Logger.info(response);
			if(!TextUtils.isEmpty(response)){
				long rekananId = CommonUtil.fromJson(response, Long.class);

//				String codeEncript = Encryption.encrypt(String.valueOf(rekananId).getBytes());
//				//save encode url
//				codeEncript = Encryption.encodeUrlSafe(codeEncript);
//				String rkn = "";
//				rkn = config.adp_detail_user + "?q=" + codeEncript;
//				LogUtil.d(TAG, rkn);
//				content = WS.url(rkn).timeout("5min").getAsync().get().getString();
//				response = Encryption.decrypt(content);
//				LogUtil.d(TAG, "response detail: "+ response);
//				RekananDetail rknDetail = CommonUtil.fromJson(response, RekananDetail.class);

				RekananDetail rknDetail = RekananDetail.getIdRekananDefault(rekananId);

				new JobCekWs(rknDetail).now();

				long userId = User.saveOrUpdateUsrRkn(rknDetail);
				User user = User.findById(userId);

				ProviderRepository.checkAndSaveRknIdByUser(userId, rknDetail.rkn_id, rknDetail);
				//PenyediaDistributor.checkAndSaveRknIdByUser(userId, rknDetail.rkn_id);

				if(userId != 0){
					setRoles(user);
					PublikCtr.home();
				}
			} else {
				flash.error(Messages.get("login_gagal.label") + ", "+ Messages.get("notif_failed_username_password.label") + "!");
				// new req: diarahkan pake data lokal
				//redirectToLoginPage("penyedia");
			}
		} catch (Exception e){
			LogUtil.e(TAG, e);
			if (e instanceof ConnectException || e instanceof SocketTimeoutException) {
				flash.error(Messages.get("login_gagal.label") + ", "+ Messages.get("notif_failed_connection.label") + "!");
			} else {
				flash.error(Messages.get("login_gagal.label") + "!");
			}
			// new req: diarahkan pake data lokal
			//redirectToLoginPage("penyedia");
		}
		//coba login dengan lokal
		Logger.info("RDIRECT TO LOGIN LOKAL");
		loginLokalSubmit(username,password, true);
	}

	private static void redirectToLoginPage(String tab) {
		redirect(UrlUtil.builder().setUrl("LoginCtr.index").build() + "#" + tab);
	}

	public static void loginLokalSubmit (String username, String password){
		loginLokalSubmit(username,password,false);
	}

	private static void loginLokalSubmit(String username, String password, Boolean allowPenyedia){
		try {
			User user = User.findFirstByUserNameLoginFromKatalog(username);
			//cek apakah penyedia dan atau distributor
			user.isProviderAndDistributor();
			RoleBasic roleBasic = RoleBasic.findById(user.role_basic_id);
			if(roleBasic.nama_role.equalsIgnoreCase(RoleBasic.PENYEDIA_NAME) ||
					roleBasic.nama_role.equalsIgnoreCase(RoleBasic.DISTRIBUTOR_NAME) ||
					roleBasic.nama_role.equalsIgnoreCase(RoleBasic.PENYEDIA_DISTRIBUTOR_NAME)){
				flash.error(Messages.get("login_gagal.label") + ", " + Messages.get("notif_failed_wrongloginpage") + "!");
				if(!allowPenyedia) {
					redirectToLoginPage("penyedia");
				}
			}

			if(user != null && user.isPasswordMd5Equals(password)){
				setRoles(user);
				KontenStatis syaratKetentuan = KontenStatis.findKontenByKategori("syarat dan ketentuan");
				if (getAktifUser() != null && syaratKetentuan != null) {
					SyaratKetentuanHasil syaratKetentuanHasil = SyaratKetentuanHasil.findByExist(getAktifUser().user_id, syaratKetentuan.id);
					if(syaratKetentuanHasil == null){
						PublikCtr.syaratKetentuan();
					}
				}
                if (getAktifUser().isPenyedia() ^ getAktifUser().isDistributor()){
                    User userPenyedia = User.findFirstByUserName(username);
                    if (userPenyedia.status_penyedia_distributor != null){
                        PublikCtr.home();
                    }else {
                        PublikCtr.statusPenyediaDistributor();
                    }
                }else {
                    PublikCtr.home();
                }

			} else {
				flash.error(Messages.get("login_gagal.label") + ", " + Messages.get("notif_failed_username_password.label") + "!");
				redirectToLoginPage("non-penyedia");
			}
		} catch (Exception e){
			e.printStackTrace();
			flash.error(Messages.get("login_gagal.label") + ", " + Messages.get("notif_failed_username_password.label") + "!");
			redirectToLoginPage("non-penyedia");
		}
	}

	public static void loginLpse(String key) throws ParseException, ExecutionException, InterruptedException {
		Logger.info("header spse: "+request.headers.toString());
		String decrypted = Encryption.decryptInaproc(key);
		if(decrypted == null){
			flash.error(Messages.get("login_gagal.label"));
			redirectToLoginPage("non-penyedia");
		}
		Logger.info("[LOGIN FROM LPSE] - " + decrypted);

		//String[] data = decrypted.replace("|", "-").split("-");

		String[] data = decrypted.split("\\|");
		UserFromLpse loginUser = new UserFromLpse(data[0],
				data[1],
				//data[2].equalsIgnoreCase("REKANAN") ? "PENYEDIA" : data[2],
				RoleBasic.convRoleSpse(data[2]),
				Long.valueOf(data[3]),
				data[4].equalsIgnoreCase("false"),
				DateUtils.parseDate(data[5], new String[] {"ddMMyyyy"}));

		PpeSite ppeSite = PpeSite.findById(loginUser.ppsId);
		if(ppeSite == null){
			ppeSite = new PpeSite();
			ppeSite.pps_id = loginUser.ppsId;
			ppeSite.tag_id = 1;
			ppeSite.stg_id = 1;
			ppeSite.audittype = "C";
			ppeSite.audituser = loginUser.username;
			ppeSite.auditupdate = new Date();
			ppeSite.pps_nama = "LPSE " + loginUser.ppsId;
			ppeSite.pps_alamat = "";
			ppeSite.pps_display = "";
			ppeSite.pps_kontak = "";
			ppeSite.pps_proxy = "";
			ppeSite.pps_public_url = "";
			ppeSite.pps_private_url = "";
			ppeSite.pps_tanggal_pendaftaran = new Date();
			ppeSite.pps_sk = "";
			ppeSite.pps_id = Long.valueOf(ppeSite.save());
		}
		RoleBasic roleBasic = RoleBasic.find("nama_role = ?", loginUser.role).first();
		User user = User.find("user_name = ? AND pps_id = ? AND role_basic_id = ? AND active = 1",
				loginUser.username, ppeSite.pps_id, roleBasic.id).first();
		if(user == null){
			UserRoleGrup userRoleGrup = UserRoleGrup.find("role_basic_id = ?", roleBasic.id).first();
			user = new User();
			user.user_role_grup_id = userRoleGrup.id;
			user.role_basic_id = roleBasic.id;
			user.nama_lengkap = loginUser.name;
			user.user_name = loginUser.username;
			user.user_password = "";
			user.user_email = "";
			user.override_role_grup = false;
			user.override_role_komoditas = false;
			user.override_role_paket = false;
			user.active = true;
			user.pps_id = loginUser.ppsId;
			user.bahasa = "id";
			user.id = Long.valueOf(user.save());
		}
		if(roleBasic.id == RoleBasic.PENYEDIA){
			Config config = Config.getInstance();
			String rkn = config.adp_detail_user_by_username + "?q=" + URLs.encodePart(Encryption.encrypt(String.valueOf(loginUser.username).getBytes()));
			String content = WS.url(rkn).timeout("5min").getAsync().get().getString();
			String res = Encryption.decrypt(content);
			RekananDetail rknDetail = CommonUtil.fromJson(res, RekananDetail.class);
			if(rknDetail != null){
				user.rkn_id = rknDetail.rkn_id;
				user.save();
			}
		}
		setRoles(user);
		Logger.info("[LOGIN FROM LPSE] - Login Success, redirect to home..");
		PublikCtr.home();
	}

	private static void setRoles(User user){
		List<String> roleItems = new ArrayList<>();
		List<String> tmpRoleItems = new ArrayList<>();
		List<Long> komoditasList = new ArrayList<>();
		List<Long> paketList = new ArrayList<>();
		boolean akses_seluruh_komoditas = true;
		boolean akses_seluruh_paket = true;
		if(user.user_role_grup_id != null && user.user_role_grup_id != 0){
			UserRoleGrup userRoleGrup = UserRoleGrup.findById(user.user_role_grup_id);
			List<UserRoleItem> userRoleItems = UserRoleItem.findByUserRoleGrupId(userRoleGrup.id);
			if(user.override_role_grup){
				List<UserRoleItemOverride> userRoleItemOverrides = UserRoleItemOverride.findByUserId(user.id);
				if(userRoleItemOverrides != null && userRoleItemOverrides.size() > 0){
					tmpRoleItems = userRoleItemOverrides.stream().
							map(obj -> Objects.toString(obj.role_item_name)).
							collect(Collectors.toList());
					roleItems.addAll(tmpRoleItems);
				}
			} else {
				if(userRoleItems != null && userRoleItems.size() > 0){
					tmpRoleItems = userRoleItems.stream().
							map(obj -> Objects.toString(obj.role_item_name)).
							collect(Collectors.toList());
					roleItems.addAll(tmpRoleItems);
				}
			}

			if(user.override_role_komoditas){
				List<UserRoleKomoditasOverride> komoditasOverrides = UserRoleKomoditasOverride.findByUserId(user.id);
				if(komoditasOverrides != null && komoditasOverrides.size() > 0){
					komoditasList = komoditasOverrides.stream().
							map(obj -> obj.komoditas_id).
							collect(Collectors.toList());
					akses_seluruh_komoditas = false;
				}

			} else {
				if(!userRoleGrup.akses_seluruh_komoditas){
					List<UserRoleKomoditas> userRoleKomoditas = UserRoleKomoditas.findByUserRoleGrupId(userRoleGrup.id);
					if(userRoleKomoditas != null && userRoleKomoditas.size() > 0){
						komoditasList = userRoleKomoditas.stream().
								map(obj -> obj.komoditas_id).
								collect(Collectors.toList());
						akses_seluruh_komoditas = false;
					}
				}

			}

			if(user.override_role_paket){
				List<UserRolePaketOverride> paketOverrides = UserRolePaketOverride.findByUserId(user.id);
				if(paketOverrides != null && paketOverrides.size() > 0){
					komoditasList = paketOverrides.stream().
							map(obj -> obj.paket_id).
							collect(Collectors.toList());
					akses_seluruh_paket = false;
				}

			} else {
				if(!userRoleGrup.akses_seluruh_paket){
					List<UserRolePaket> userRolePakets = UserRolePaket.findByUserRoleGrupId(userRoleGrup.id);
					if(userRolePakets != null && userRolePakets.size() > 0){
						komoditasList = userRolePakets.stream().
								map(obj -> obj.paket_id).
								collect(Collectors.toList());
						akses_seluruh_paket = false;
					}
				}

			}
		}

		//Scope.Session.current().put(AktifUser.USER_SESSION_ID, user.id);
		RoleBasic roleBasic = RoleBasic.findById(user.role_basic_id);
		AktifUser aktifUser = new AktifUser(session.getId(), user.nama_lengkap, user.role_basic_id, user.id,"",
				akses_seluruh_paket, akses_seluruh_komoditas, roleItems, paketList, komoditasList, user.rkn_id,
				user.nip, user.user_email, user.no_telp, user.nomor_sertifikat_pbj, user.jabatan, roleBasic.nama_role,
				user.user_role_grup_id, user.isProviderAndDistributor(), user.kldi_jenis, user.kldi_id);
		RoleBasic rbc =  RoleBasic.findById(user.role_basic_id);
		if(rbc != null) {
			if (rbc.nama_role.equalsIgnoreCase(RoleBasic.PENYEDIA_DISTRIBUTOR_NAME)) {
				PenyediaDistributor pd = PenyediaDistributor.findByUser(user.id);
				if(pd!= null){
					aktifUser.distributor_id = PenyediaDistributor.findByUser(user.id).id;
				}
				Penyedia pe = Penyedia.getProviderByUserId(user.id);
				if(pe!= null){
					aktifUser.penyedia_id = Penyedia.getProviderByUserId(user.id).id;
				}
			} else {
				if (rbc.nama_role.equalsIgnoreCase(RoleBasic.PENYEDIA_NAME)) {
					Penyedia pe = Penyedia.getProviderByUserId(user.id);
					if(pe!= null){
						aktifUser.penyedia_id = Penyedia.getProviderByUserId(user.id).id;
					}
				}
				if (rbc.nama_role.equalsIgnoreCase(RoleBasic.DISTRIBUTOR_NAME)) {
					PenyediaDistributor pd = PenyediaDistributor.findByUser(user.id);
					if(pd!= null){
						aktifUser.distributor_id = PenyediaDistributor.findByUser(user.id).id;
					}
				}
			}
		}

		AktifUser.saveToCache(aktifUser);

//		UserLoggedIn userLoggedIn = UserLoggedIn.find("user_id = ? and ip_address = ?",
//				user.id.toString(),
//				request.remoteAddress).first();
		//cari berdasarkan session juga, agar tidak melogout user sama pada device yang lain
//		UserLoggedIn userLoggedIn = UserLoggedIn.find("user_id = ? and ip_address = ? and session_id=?",
//				user.id.toString(),
//				request.remoteAddress,
//				session.current().getId()).first();
//		if(userLoggedIn != null){
//			userLoggedIn.session_id = session.current().getId();
//			userLoggedIn.payload = CommonUtil.toJson(aktifUser);
//			userLoggedIn.save();
//		} else {
//			userLoggedIn = new UserLoggedIn();
//			userLoggedIn.ip_address = request.remoteAddress;
//			userLoggedIn.user_id = user.id.toString();
//			userLoggedIn.session_id = session.current().getId();
//			userLoggedIn.payload = CommonUtil.toJson(aktifUser);
//			userLoggedIn.save();
//		}

		boolean sukses = UserLoggedIn.setOrUpdateActiveUser(user.id.toString(), request.remoteAddress, session.current().getId(), aktifUser);
		if(sukses){
			Logger.info("Setted UserLogedIn ");
		}else{
			Logger.info("not setted UserLogedIn");
		}

	}

}
