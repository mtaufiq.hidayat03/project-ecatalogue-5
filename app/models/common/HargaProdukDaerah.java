package models.common;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.collections4.map.LinkedMap;

/**
 * Created by dadang on 10/3/17.
 */
public class HargaProdukDaerah {

	public String tanggal;
	public String nama_kurs;

	public Map<Long, BigDecimal> harga = new LinkedMap<>();

}
