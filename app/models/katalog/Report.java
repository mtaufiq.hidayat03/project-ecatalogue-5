package models.katalog;

import ext.DatetimeBinder;
import models.BaseKatalogTable;
import play.data.binding.As;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.LogUtil;

import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

/**
 * Created by fajar on 5/23/18.
 */
@Table(name = "report")
public class Report extends BaseKatalogTable{
    @Id
    public Long id;

    public Long item_id;

    public String item_type;

    public String status;

    public Long reporter_id;

    public String reporter_name;

    public String reporter_email;

    public String reporter_nik;

    public String reporter_telp;

    public String message;

    public String reply;

    public Long reply_by;

    public String konten_kategori_id;

    @As(binder = DatetimeBinder.class)
    public Date reply_date;

    @Transient
    public String no_produk;
    @Transient
    public String nama_produk;
    @Transient
    public String nama_penyedia;

    public static boolean closeReport(Long id){
        String status = "selesai";
        try{
            String query = "update report set status = '" + status + "' where id = ?";
            return Query.update(query,id) == 1;
        }catch (Exception e) {
            // TODO Auto-generated catch block
            LogUtil.d("***************** error ***************", e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public static List<Report> findReportDate(String start_date, String end_date){
        String query = "SELECT r.id, r.created_date, r.item_id, r.status, p.no_produk, p.nama_produk, r.reporter_name, r.reporter_email, r.reporter_nik, r.message, py.nama_penyedia " +
                "FROM report r " +
                "LEFT JOIN produk p on p.id = r.item_id " +
                "LEFT JOIN penyedia py on p.penyedia_id = py.id " +
                "WHERE date(r.created_date) BETWEEN '" + start_date + "' and '" + end_date + "' " +
                "ORDER BY r.created_date DESC";
        List<Report> result = Query.find(query,Report.class).fetch();
        return result;
    }
}
