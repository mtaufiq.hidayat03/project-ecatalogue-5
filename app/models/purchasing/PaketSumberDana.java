package models.purchasing;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

/**
 * Created by dadang on 11/17/17.
 */
@Table(name = "sumber_dana_paket")
public class PaketSumberDana extends BaseKatalogTable {

	@Id
	public Long id;
	public Long sumber_dana_id;
	public Long paket_id;
	public String kode_anggaran;

	@Transient
	public String nama_sumber_dana;

	public static void deleteByPaketId(long paket_id){
		String query = "DELETE from sumber_dana_paket where paket_id = ?";
		Query.update(query, paket_id);
	}

	public static List<PaketSumberDana> getFundSource(long packageid) {
		String query = "SELECT psd.id, " +
				"psd.sumber_dana_id, " +
				"psd.paket_id, " +
				"psd.kode_anggaran, " +
				"sd.nama_sumber_dana " +
				"FROM sumber_dana_paket psd " +
				"LEFT JOIN sumber_dana sd " +
				"ON sd.id = psd.sumber_dana_id " +
				"WHERE psd.paket_id =?";
		return Query.find(query, PaketSumberDana.class, packageid).fetch();
	}

}
