package jobs.JobTrail;

import models.elasticsearch.NewBase.PenyediaElastic;
import models.penyedia.Penyedia;
import play.cache.Cache;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import java.util.ArrayList;
import java.util.List;

@NoTransaction
public class JobSetPenyediaElastic extends Job {
    String key1;
    public JobSetPenyediaElastic(String _key1){
        key1 = _key1;
    }
    public void doJob(){
        List<PenyediaElastic> result2 = new ArrayList<>();
        try{
            result2 = Penyedia.getAllPenyediaOnElastic();
            if(result2.size() > 0) {
                Cache.set(key1, result2, "1d");
            }
        }catch (Exception e){
        }
    }
}
