package models.katalog.komoditasmodel;

import com.google.gson.Gson;
import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

@Table(name = "komoditas_tahapan_template_jadwal")
public class KomoditasTahapanTemplateJadwal extends BaseKatalogTable {

	@Id
	public Long id;
	public Long tahapan_id;
	public Long template_id;
	public Long form_id;
	public boolean active;

	@Transient
	public String nama_tahapan;

	public KomoditasTahapanTemplateJadwal() {}

	public KomoditasTahapanTemplateJadwal(Long tahapanId, Long templateId) {
		this.tahapan_id = tahapanId;
		this.template_id = templateId;
		this.active = true;
	}

	public static List<KomoditasTahapanTemplateJadwal> findByTemplateId(long templateId){
		String query = "select template_tahapan.tahapan_id as tahapan_id, tahapan.nama_tahapan_alias as nama_tahapan " +
				"from komoditas_tahapan_template_jadwal template_tahapan " +
				"join tahapan on tahapan.id = template_tahapan.tahapan_id " +
				"where template_tahapan.template_id = ? and template_tahapan.active = ? " +
				"order by template_tahapan.id";

		return Query.find(query, KomoditasTahapanTemplateJadwal.class,templateId,1).fetch();
	}

	public static String getSelectedTahapanTemplate(List<KomoditasTahapanTemplateJadwal> templatetahapanList){
		List<Long> selectedTahapan = new ArrayList<Long>();
		for(KomoditasTahapanTemplateJadwal templateTahapan : templatetahapanList){
			selectedTahapan.add(templateTahapan.tahapan_id);
		}

		Gson gson = new Gson();
		return gson.toJson(selectedTahapan);

	}

}
