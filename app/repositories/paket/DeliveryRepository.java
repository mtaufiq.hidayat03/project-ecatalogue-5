package repositories.paket;

import com.google.gson.Gson;
import constants.paket.PaketPenerimaanConstant;
import models.common.AktifUser;
import models.purchasing.pengiriman.LampiranPengiriman;
import models.purchasing.pengiriman.ProdukPengiriman;
import models.purchasing.pengiriman.PaketRiwayatPengiriman;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.db.jdbc.Query;
import utils.DateTimeUtils;
import utils.KatalogUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static constants.paket.PaketPengirimanConstant.*;

/**
 * @author HanusaCloud on 10/20/2017
 */
public class DeliveryRepository {

    public static List<ProdukPengiriman> getProductsByDeliveryIdAndPackage(Long paket_id, Long delivery_id) {
        final String query = "SELECT pp.id, " +
                "pp.deskripsi," +
                "pp.kuantitas, " +
                "ppr.kuantitas as kuantitas_awal, " +
                "ppr.catatan, " +
                "ppr.id as paket_produk_id, " +
                "p.nama_produk, " +
                "p.id as produk_id, " +
                "p.no_produk, " +
                "p.produk_kategori_id, " +
                "up.nama_unit_pengukuran, " +
                "(ppr.kuantitas - COALESCE(pprhis.delivered, 0)) as leftovers " +
                "FROM paket_pengiriman_produk pp " +
                "LEFT JOIN (SELECT id, produk_id, paket_produk_id, SUM(kuantitas) as delivered " +
                "FROM paket_pengiriman_produk " +
                "WHERE paket_id ="+paket_id+" and paket_pengiriman_id <> "+delivery_id+" AND active > 0 " +
                "GROUP BY paket_produk_id) pprhis " +
                "ON pprhis.produk_id = pp.produk_id "+
                "JOIN paket_produk ppr " +
                "ON ppr.id = pp.paket_produk_id AND ppr.paket_id = pp.paket_id " +
                "JOIN produk p " +
                "ON p.id = pp.produk_id " +
                "JOIN unit_pengukuran up " +
                "ON up.id = p.unit_pengukuran_id " +
                "WHERE pp.paket_id =? AND pp.active > 0 AND pp.paket_pengiriman_id =?;";
        Logger.debug(query);
        return Query.find(query, ProdukPengiriman.class, paket_id, delivery_id).fetch();
    }

    public static List<ProdukPengiriman> searchProductsToBeDelivered(Long paket_id, String keyword, String except) {
        StringBuilder sb = new StringBuilder("SELECT " +
                "pp.kuantitas as kuantitas_awal, " +
                "pp.catatan, " +
                "pp.id as paket_produk_id, " +
                "p.nama_produk, " +
                "p.id as produk_id, " +
                "p.unit_pengukuran_id, " +
                "p.no_produk, " +
                "up.nama_unit_pengukuran, " +
                "ppr.delivered, " +
                "(pp.kuantitas - COALESCE(SUM(ppr.delivered), 0)) as leftovers, " +
                "ppr.id " +
                "FROM paket_produk pp " +
                "JOIN produk p " +
                "ON p.id = pp.produk_id " +
                "LEFT JOIN (SELECT id, produk_id, paket_produk_id, SUM(kuantitas) as delivered " +
                "FROM paket_pengiriman_produk " +
                "WHERE paket_id =? AND active > 0 " +
                "GROUP BY paket_produk_id) ppr " +
                "ON ppr.paket_produk_id = pp.id " +
                "JOIN unit_pengukuran up " +
                "ON up.id = p.unit_pengukuran_id " +
                "WHERE pp.paket_id =? " );
        if (!TextUtils.isEmpty(except)) {
            sb.append("AND pp.id NOT IN (");
            sb.append(except);
            sb.append(") ");
        }
        if (!TextUtils.isEmpty(keyword)) {
            sb.append("AND p.nama_produk LIKE '%")
                    .append(keyword)
                    .append("%' ");
        }
        sb.append(" GROUP BY pp.id ");
        sb.append("HAVING leftovers > 0 ");
        if (!TextUtils.isEmpty(keyword)) {
            sb.append("LIMIT 10");
        }
        Logger.debug(sb.toString());
        return Query.find(sb.toString(), ProdukPengiriman.class, paket_id, paket_id).fetch();
    }

    public static List<LampiranPengiriman> getAttachmentByPackageId(Long packageId, Long deliveryId) {
        final String query = "SELECT *" +
                " FROM paket_pengiriman_lampiran" +
                " WHERE paket_id =?" +
                " AND paket_pengiriman_id =?" +
                " AND active > 0";
        return Query.find(query, LampiranPengiriman.class, packageId, deliveryId).fetch();
    }

    public static LampiranPengiriman getLatestAttachment(Long packageId, Long deliveryId) {
        final String query = "SELECT id" +
                ", paket_pengiriman_id" +
                ", paket_id, file_name" +
                ", original_file_name" +
                ", file_sub_location" +
                " FROM paket_pengiriman_lampiran" +
                " WHERE paket_id =? AND paket_pengiriman_id =? AND active > 0" +
                " ORDER BY posisi_file DESC LIMIT 1";
        return Query.find(query, LampiranPengiriman.class, packageId, deliveryId).first();
    }

    public static List<PaketRiwayatPengiriman> getDeliveryHistoriesByPackageId(Long id) {
        final String query = "SELECT pp.id, " +
                "pp.paket_id, " +
                "pp.no_dokumen_sistem, " +
                "pp.no_dokumen, " +
                "pp.tanggal_dokumen, " +
                "pp.deskripsi, " +
                "pp.agr_paket_pengiriman_produk, " +
                "pp.agr_paket_pengiriman_lampiran, " +
                "pp.agr_total_harga_diterima_dan_ongkir, " +
                "ppn.agr_paket_penerimaan_produk, " +
                "pp.ongkir, " +
                "pp.created_date, " +
                "ppl.original_file_name, " +
                "ppl.file_sub_location, " +
                "ppl.file_name, " +
                "ppl.id as file_id " +
                "FROM paket_pengiriman pp " +
                "LEFT JOIN paket_penerimaan ppn " +
                "ON pp.id = ppn.paket_pengiriman_id " +
                "LEFT JOIN paket_pengiriman_lampiran ppl " +
                "ON ppl.paket_pengiriman_id = pp.id AND ppl.active > 0 " +
                "WHERE pp.paket_id = ? AND pp.active > 0 " +
                "GROUP BY pp.id " +
                "ORDER BY pp.created_date ASC";
        Logger.debug(query);
        List<PaketRiwayatPengiriman> results = Query.find(query, PaketRiwayatPengiriman.class, id).fetch();
        if (!results.isEmpty()) {
            Map<Long, LampiranPengiriman> attachments = getAttachments(id);
            Logger.debug(new Gson().toJson(attachments));
            for (PaketRiwayatPengiriman model : results) {
                LampiranPengiriman attachment = attachments.get(model.id);
                if (attachment != null) {
                    model.attachment = attachment;
                }
            }
        }
        return results;
    }

    public static void deactivateAllProducts(Long packageId, Long deliveryId, String ids) {
        StringBuilder sb = new StringBuilder("UPDATE paket_pengiriman_produk" +
                " SET active = 0" +
                ", deleted_by = '" + AktifUser.getActiveUserId() + "'" +
                ", deleted_date = '" + DateTimeUtils.getStandardDateTime() + "'" +
                " WHERE paket_id =?" +
                " AND paket_pengiriman_id =?");
        if (!TextUtils.isEmpty(ids)) {
            sb.append(" AND id IN (").append(ids).append(")");
        }
        Query.update(sb.toString(), packageId, deliveryId);
    }

    public static void deactivateAllAttachments(Long packageId, Long deliveryId) {
        Query.update("UPDATE paket_pengiriman_lampiran" +
                " SET active = 0" +
                ", deleted_by = '" + AktifUser.getActiveUserId() + "'" +
                ", deleted_date = '" + DateTimeUtils.getStandardDateTime() + "'" +
                " WHERE paket_id =?" +
                " AND paket_pengiriman_id =?", packageId, deliveryId);
    }

    public static Map<Long, LampiranPengiriman> getAttachments(Long packageId) {
        final String query = "SELECT id, paket_pengiriman_id, file_name, original_file_name, file_sub_location " +
                "FROM paket_pengiriman_lampiran " +
                "where paket_id = ? AND active > 0 " +
                "GROUP BY paket_pengiriman_id " +
                "ORDER BY posisi_file DESC;";
        List<LampiranPengiriman> attachments = Query.find(query, LampiranPengiriman.class, packageId).fetch();
        return attachments.stream().collect(Collectors.toMap(LampiranPengiriman::getPengirimanId, p -> p));
    }

    /**
     * @return Map product id as the key*/
    public static Map<Long, ProdukPengiriman> getCurrentProducts(Long packageId, Long deliveryId) {
        final String query = "SELECT *" +
                " FROM paket_pengiriman_produk" +
                " WHERE paket_id =? AND paket_pengiriman_id =? AND active > 0;";
        List<ProdukPengiriman> attachments = Query.find(query, ProdukPengiriman.class, packageId, deliveryId).fetch();
        return attachments.stream().collect(Collectors.toMap(ProdukPengiriman::getProdukId, p -> p));
    }

    /**
     * @return Map id as the key*/
    public static Map<Long, ProdukPengiriman> getCurrentProductsIdKey(Long packageId, Long deliveryId) {
        final String query = "SELECT *" +
                " FROM paket_pengiriman_produk" +
                " WHERE paket_id =? AND paket_pengiriman_id =? AND active > 0;";
        List<ProdukPengiriman> attachments = Query.find(query, ProdukPengiriman.class, packageId, deliveryId).fetch();
        return attachments.stream().collect(Collectors.toMap(ProdukPengiriman::getId, p -> p));
    }

    public static void resetVerificationId(Long id, Long packageId) {
        Query.update("UPDATE " + TABLE_NAME +
                " SET " + VERIFICATION_ID + " = null " +
                " , " + MODIFIED_DATE + " = '" + DateTimeUtils.getStandardDateTime() + "'" +
                " , " + MODIFIED_BY + " = '" + AktifUser.getActiveUserId() + "'" +
                " WHERE " + ID + " =?" +
                " AND " + PAKET_ID + " =?", id, packageId);
    }

    public static void setVerificationId(Long id, Long packageId, Long verificationId) {
        Query.update("UPDATE " + TABLE_NAME +
                " SET " + VERIFICATION_ID + " = '" + verificationId + "'" +
                " , " + MODIFIED_DATE + " = '" + DateTimeUtils.getStandardDateTime() + "'" +
                " , " + MODIFIED_BY + " = '" + AktifUser.getActiveUserId() + "'" +
                " WHERE " + ID + " =?" +
                " AND " + PAKET_ID + " =?", id, packageId);
    }

    public static List<PaketRiwayatPengiriman> getByIds(Long packageId, String ids, String[] fields) {
        String fieldPart = KatalogUtils.generateSelectedColumns(fields);
        final String query = "SELECT " + fieldPart + " FROM " + TABLE_NAME +
                " WHERE " + PAKET_ID + " =?" +
                " AND " + ID + " IN (" + ids + ")";
        return Query.find(query, PaketRiwayatPengiriman.class, packageId).fetch();
    }

    public static Map<Long, PaketRiwayatPengiriman> getMapByIds(Long packageId, String ids) {
        final String[] fields = {ID, PAKET_ID};
        return getByIds(packageId, ids, fields).stream().collect(Collectors.toMap(PaketRiwayatPengiriman::getId, p -> p));
    }

    public static List<PaketRiwayatPengiriman> getByPaymentId(Long packageId, Long id) {
        final String query = "SELECT *" +
                " FROM " + TABLE_NAME +
                " WHERE " + PAKET_ID + " =?" +
                " AND " + ID + " =?";
        return Query.find(query, PaketRiwayatPengiriman.class, packageId, id).fetch();
    }

    public static List<PaketRiwayatPengiriman> searchDelivery(Long packageId, String keyword) {
        StringBuilder sb = new StringBuilder("SELECT p."
                + ID
                + ", p." + NO_DOKUMEN
                + ", p." + NO_DOKUMEN_SISTEM
                + ", p." + TANGGAL_DOKUMEN
                + ", p." + TOTAL_PRODUK
                + ", p." + PAKET_ID
                + ", p." + DESKRIPSI
                + ", a." + PaketPenerimaanConstant.ID + " as acceptance"
                + " FROM " + TABLE_NAME + " p"
                + " LEFT JOIN " + PaketPenerimaanConstant.TABLE_NAME + " a"
                + " ON a." + PaketPenerimaanConstant.PAKET_PENGIRIMAN_ID + " = p." + ID
                + " WHERE p." + PAKET_ID + " =?"
                + " AND p.active > 0");
        if (!TextUtils.isEmpty(keyword)) {
            sb.append(" AND (p.").append(NO_DOKUMEN_SISTEM).append(" LIKE '%").append(keyword).append("%'");
            sb.append(" OR p.").append(NO_DOKUMEN).append(" LIKE '%").append(keyword).append("%')");
        }
        sb.append(" HAVING acceptance IS NULL");
        return Query.find(sb.toString(), PaketRiwayatPengiriman.class, packageId).fetch();
    }

}
