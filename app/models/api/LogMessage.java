package models.api;

/**
 * Created by dadang on 12/11/17.
 */
public class LogMessage {

	public String message;
	public boolean isEnd;
	public boolean newLine;

	public LogMessage(){

	}

	public LogMessage(String message, boolean isEnd, boolean newLine) {
		this.message = message;
		this.isEnd = isEnd;
		this.newLine = newLine;
	}
}
