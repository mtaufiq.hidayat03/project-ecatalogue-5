package jobs.wilayah;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import jobs.JobTrail.ExtKabupatenDataJob;
import models.api.DceKab;
import models.jcommon.config.Configuration;
import models.jcommon.http.SimpleHttpRequest;
import models.masterdata.*;
import models.util.Config;
import play.Logger;
import play.jobs.Job;
import utils.KatalogUtils;

import java.io.IOException;
import java.util.*;

public class ExtKabupatenData {
	private Properties propJob;
	public static final String DEFAULT_WAIT_TIME = "job.sirup.waittime";
	public static final String DEFAULT_WAIT_TIME_NEXT = "job.sirup.waittime.next";
	private String jsResult = "[]";
	private Boolean resultGet = false;
	public List<DceKab> kabupatenList = new ArrayList<>();
	private int defaultWaitTime = 0;
	private String kabupatenUrl = Config.getInstance().kabupaten_api;
	private int nextWaitTime = 0;
	private Long propId;
	private int delayTaskJob = 5;// second
	private int delayJob = 10;// second
	//
	public ExtKabupatenData(Long prop_id) {
		propJob = new Properties();
		propJob.put(DEFAULT_WAIT_TIME,
				Configuration.getConfigurationValue(DEFAULT_WAIT_TIME, ""));
		propJob.put(DEFAULT_WAIT_TIME_NEXT, Configuration
				.getConfigurationValue(DEFAULT_WAIT_TIME_NEXT, ""));

		defaultWaitTime = Integer
				.parseInt(propJob.getProperty(DEFAULT_WAIT_TIME));
		nextWaitTime = Integer
				.parseInt(propJob.getProperty(DEFAULT_WAIT_TIME_NEXT));
		propId = prop_id;

	}

	public List<DceKab> getKabupatenData() {
		return getKabupatenData(defaultWaitTime);
	}
	public List<DceKab> getKabupatenData(Integer minuteTimeOut) {
		if (fetchKabupatenData(propId, minuteTimeOut)) {
			this.kabupatenList = parseKabupatenData();
		}
		return this.kabupatenList;
	}

	private Boolean fetchKabupatenData(Long prop_id, Integer minuteTimeout) {
		resultGet = false;
		jsResult = "[]";
		Date tgl_update = new Date();
		String date = KatalogUtils.toDateyyyyMMdd(tgl_update);
		Integer seconds = 0;
		if (minuteTimeout > 0) {
			seconds = (minuteTimeout * 60);
		}
		Logger.info("run with minuteTimeout: " + minuteTimeout + ", and date "
				+ tgl_update.toLocaleString());
		try (SimpleHttpRequest connect = new SimpleHttpRequest(seconds)) {
			String params = prop_id.toString();
			connect.get(kabupatenUrl + params);
			jsResult = connect.getString();
			resultGet = true;
		} catch (InterruptedException e) {
			Logger.error("Intekabupatented Exception:" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Logger.error("IO Exception:" + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		if (resultGet == false) {
			defaultWaitTime++;
			nextWaitTime = defaultWaitTime;
			// Configuration.updateConfigurationValue(DEFAULT_WAIT_TIME_NEXT,
			// String.valueOf(newWait));
			// propJob.replace(DEFAULT_WAIT_TIME_NEXT, String.valueOf(newWait));

			 newJob();
		}
		return resultGet;
	}

	private List<DceKab> parseKabupatenData() {
		try {
			kabupatenList = new Gson().fromJson(jsResult,
					new TypeToken<List<DceKab>>() {
					}.getType());
			Logger.info("total:" + kabupatenList.size());
		} catch (Exception e) {
			Logger.error("error parse data dcekabupaten:" + e.getMessage());
			e.printStackTrace();
		}
		return kabupatenList;
	}

	public void saveDataKabupatenToDb() {
		List<DceKabupaten> failed = new ArrayList<>();
		List<DceKabupaten> success = new ArrayList<>();
		Logger.info("save data kabupaten to dce_kabupaten:");
		Provinsi prop = Provinsi.find("dce_id=?",propId).first();
		for (DceKab s : kabupatenList) {
			DceKabupaten kabupatenObj = DceKabupaten.findById(s.kbp_id);
			Kabupaten kab = Kabupaten.find("dce_id=?",kabupatenObj.id).first();
			try {
				if (kabupatenObj != null) {
					kabupatenObj.dce_provinsi_id = s.prp_id;
					kabupatenObj.nama = s.kbp_nama;
					int result = kabupatenObj.save();
					Logger.info("update:" + result);
				} else {
					kabupatenObj = s.getAsDceKabupaten();
					int result = kabupatenObj.save();
					Logger.info("save:" + result);
				}
				Logger.info("save data kabupaten to kabupaten:");
				if (kab == null) {
					kab = new Kabupaten();
					kab.nama_kabupaten = kabupatenObj.nama;
					kab.dce_id = kabupatenObj.id;
					kab.provinsi_id = prop.id;
					kab.active = true;
					kab.save();
				}else {
					kab.nama_kabupaten = kabupatenObj.nama;
					kab.dce_id = kabupatenObj.id;
					kab.provinsi_id = prop.id;
					kab.active = true;
					kab.save();
				}

				if (kabupatenObj != null) {
					success.add(kabupatenObj);
				}
			} catch (Exception e) {
				if (kabupatenObj != null) {
					failed.add(kabupatenObj);
				}
				Logger.info("error save or update kabupaten:" + e.getMessage());
				e.printStackTrace();
			}
		}
		Logger.info(">>>>>>>>>>save success total:" + success.size());
		for (DceKabupaten s : success) {
			Logger.info("~~~ "+ s.id + " " + s.nama);
		}

		Logger.info("<<<<<<<<<<save failed total:" + failed.size());
		for (DceKabupaten s : failed) {
			Logger.info("~~~ "+ s.id + " " + s.nama);
		}

	}

	private void newJob() {
		ExtKabupatenDataJob job1 = new ExtKabupatenDataJob(this, nextWaitTime);
		job1.in(delayJob);
//		new Job() {
//			String uniqueID = UUID.randomUUID().toString();
//			@Override
//			public void before() {
//				Logger.info("job id:" + uniqueID);
//				super.before();
//			}
//
//			public void doJob() {
//				Logger.info("on job execute, waittime:" + nextWaitTime);
//				getKabupatenData(nextWaitTime);
//				Logger.info("%%%%done getDate, saving now%%%");
//				saveDataKabupatenToDb();
//				Logger.info("done job:" + kabupatenList.size());
//			}
//		}.in(delayJob);

	}

	public void run() {
		newJob();
	}
}
