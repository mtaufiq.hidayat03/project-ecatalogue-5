package controllers.katalog;

import com.google.gson.JsonObject;
import controllers.BaseController;
import models.common.AktifUser;
import models.datatable.ResultsetHandlers;
import models.jcommon.datatable.DatatableQuery;
import models.penyedia.Penyedia;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.util.TextUtils;
import play.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 12/22/2017
 */
public class ProductDataTable extends BaseController {

    public static void cariProduk(Boolean firstLoad, String komoditas_id, String keyword, String jenis_produk, String penyedia_id,
                                  String persetujuan_tayang, String dibeli, String tayang) {
        List<Object> params = new ArrayList<>();
        //initial new result
        if(firstLoad){
            JsonObject json= DatatableQuery.createEmptyResult();
            renderJSON(json);
        }

        String whereString = "p.active = ?";
        params.add(1);
        if (!TextUtils.isEmpty(komoditas_id)) {
            Long komoditasId = Long.parseLong(komoditas_id);
            whereString += " AND p.komoditas_id = ?";
            params.add(komoditasId);
        }
        if(!TextUtils.isEmpty(keyword)){
            keyword = "%" + StringEscapeUtils.escapeSql(keyword) + "%";
            whereString += " AND (p.nama_produk LIKE ? or p.no_produk like ? or pen.nama_penyedia LIKE ? or m.nama_manufaktur LIKE ?)";
            params.add(keyword);
            params.add(keyword);
            params.add(keyword);
            params.add(keyword);
        }

        if(!TextUtils.isEmpty(jenis_produk)){
            whereString += " AND p.jenis_produk = ?";
            params.add(jenis_produk);
        }

        if(!TextUtils.isEmpty(penyedia_id)){
            whereString += " AND p.penyedia_id = ?";
            params.add(penyedia_id);
        }

		if(!TextUtils.isEmpty(persetujuan_tayang)){
		    if(persetujuan_tayang.equalsIgnoreCase("1")){
		        persetujuan_tayang = "";
            }else if (persetujuan_tayang.equalsIgnoreCase("2")){
                persetujuan_tayang = "0";
            }else if (persetujuan_tayang.equalsIgnoreCase("3")){
                persetujuan_tayang = "setuju";
            }else if (persetujuan_tayang.equalsIgnoreCase("4")){
                persetujuan_tayang = "tolak";
            }
			whereString += " AND p.setuju_tolak = ?";
			params.add(persetujuan_tayang);
		}

        if(!TextUtils.isEmpty(dibeli)){
            whereString += " AND p.apakah_dapat_dibeli = ?";
            params.add(dibeli);
        }

        if(!TextUtils.isEmpty(tayang)){
            whereString += " AND p.apakah_ditayangkan = ?";
            params.add(tayang);
        }

        JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_PRODUK)
                .select(
                        "p.id, p.produk_gambar_file_sub_location,p.produk_gambar_file_name,(case when (concat(p.produk_gambar_file_sub_location,p.produk_gambar_file_name) is null) then\n" +
                                "concat(pg.file_sub_location,pg.file_name) else concat(p.produk_gambar_file_sub_location,p.produk_gambar_file_name)\n" +
                                "end) as file_url, m.nama_manufaktur, pen.nama_penyedia, p.no_produk_penyedia, p.apakah_dapat_dibeli, p.apakah_ditayangkan, \n" +
                                "p.status, p.komoditas_id, p.no_produk, p.setuju_tolak, p.jumlah_stok, p.jumlah_stok_inden, pk.nama_kategori, p.nama_produk, k.kelas_harga,\n" +
                                "pen.nama_penyedia, pk.nama_kategori, m.nama_manufaktur, k.kelas_harga "
                )
                .from( "produk p \n" +
                        "join produk_kategori pk on pk.id = p.produk_kategori_id \n" +
                        "join komoditas k on k.id = p.komoditas_id\n" +
                        "right join produk_gambar pg on pg.produk_id = p.id and pg.active=1 and pg.produk_id > 0\n" +
                        "right join penyedia pen on p.penyedia_id = pen.id\n" +
                        "join manufaktur m on m.id = p.manufaktur_id" )
                .where(whereString +" GROUP BY p.id")
                .params(params.toArray(new Object[params.size()]))
                .executeQuery();

        renderJSON(json);
    }

    public static void cariProdukPenyedia(Boolean firstLoad, String komoditas_id, String keyword, String jenis_produk,
                                            String persetujuan_tayang, String dibeli, String tayang) {
        List<Object> params = new ArrayList<>();

        //initial new result
        if(null == firstLoad || firstLoad ){
            Logger.info("first data");
            JsonObject json= DatatableQuery.createEmptyResult();
            renderJSON(json);
        }
        String whereString = "p.active = ? and p.penyedia_id = ?";
        params.add(1);
        Penyedia p = Penyedia.getProviderByUserId(AktifUser.getActiveUserId().longValue());
        if(null != p){
            params.add(p.id);
        }else{
            params.add(0);
        }

        if (!TextUtils.isEmpty(komoditas_id)) {
            Long komoditasId = Long.parseLong(komoditas_id);
            whereString += " AND p.komoditas_id = ?";
            params.add(komoditasId);
        }

        if(!TextUtils.isEmpty(keyword)){
            keyword = "%" + StringEscapeUtils.escapeSql(keyword) + "%";
            whereString += " AND (p.nama_produk LIKE ? or p.no_produk like ? or m.nama_manufaktur LIKE ?)";
            params.add(keyword);
            params.add(keyword);
            params.add(keyword);
        }

        if(!TextUtils.isEmpty(jenis_produk)){
            whereString += " AND p.jenis_produk = ?";
            params.add(jenis_produk);
        }

        if(!TextUtils.isEmpty(persetujuan_tayang)){
            whereString += " AND p.setuju_tolak = ?";
            params.add(persetujuan_tayang);
        }

        if(!TextUtils.isEmpty(dibeli)){
            whereString += " AND p.apakah_dapat_dibeli = ?";
            params.add(dibeli);
        }

        if(!TextUtils.isEmpty(tayang)){
            whereString += " AND p.apakah_ditayangkan = ?";
            params.add(tayang);
        }

        JsonObject json= new DatatableQuery(ResultsetHandlers.DAFTAR_PRODUK_PENYEDIA)
                .select("p.*, \n" +
//                        "pn.status as status_penawaran, \n" +
                        "pen.nama_penyedia, pk.nama_kategori, m.nama_manufaktur, k.kelas_harga")
                .from("produk p " +
                        "join produk_kategori pk on pk.id = p.produk_kategori_id " +
                        "join komoditas k on k.id = p.komoditas_id " +
//                        "join penawaran pn ON pn.id = p.penawaran_id " +
                        "left join penyedia pen on p.penyedia_id = pen.id " +
                        "join manufaktur m on m.id = p.manufaktur_id ")
                .where(whereString)
                .params(params.toArray(new Object[params.size()]))
                .executeQuery();

        renderJSON(json);
    }

}
