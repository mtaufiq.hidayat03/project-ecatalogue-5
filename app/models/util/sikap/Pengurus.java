package models.util.sikap;

import play.data.validation.Required;

/**
 * Created by dadang on 7/11/17.
 */
public class Pengurus {

	@Required
	public long rkn_id;
	@Required
	public long pgr_id;
	@Required
	public String pgr_ktp;
	@Required
	public String pgr_npwp;
	@Required
	public String pgr_jabatan;
	@Required
	public String pgr_nama;
	@Required
	public String pgr_alamat;
	public String status_verifikasi;
	public String auditupdate;
	public String source_data;
	public String createdat;
	public boolean is_verified = false;

	public boolean deleted = false;
	public String alasan_pembatalan = "";
	public String keterangan_pembuktian = "";
}
