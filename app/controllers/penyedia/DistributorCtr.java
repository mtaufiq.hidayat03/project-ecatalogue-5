package controllers.penyedia;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.common.AktifUser;
import models.penyedia.Penyedia;
import models.penyedia.PenyediaDistributor;
import models.penyedia.PenyediaDistributorRepresentatif;
import models.penyedia.form.UserJson;
import models.secman.Acl;
import models.user.User;
import models.util.Config;
import models.util.Encryption;
import models.util.RekananDetail;
import org.sql2o.Sql2oException;
import play.Logger;
import play.i18n.Messages;
import play.libs.URLs;
import play.libs.WS;
import utils.DateTimeUtils;
import utils.LogUtil;

import java.lang.reflect.Type;
import java.util.*;

public class DistributorCtr extends BaseController {

	private static final String formHtml = "penyedia/DistributorCtr/form.html";

	@AllowAccess({Acl.COM_PENYEDIA_DISTRIBUTOR})
	public static void index(long pid){
		if(getAktifUser().isProvider() || getAktifUser().isDistributor()){
			redirect("penyedia.PenyediaDistributorCtr.index");
		}
		render(pid);
	}

	@AllowAccess({Acl.COM_PENYEDIA_DISTRIBUTOR_ADD})
	public static void create(long pid){
		PenyediaDistributor distributor = new PenyediaDistributor();
		render(formHtml, distributor, pid);
	}

	@AllowAccess({Acl.COM_PENYEDIA_DISTRIBUTOR_EDIT})
	public static void edit(long pid, Long id){
		PenyediaDistributor distributor  = PenyediaDistributor.findById(id);
		List<PenyediaDistributorRepresentatif> representatifList = PenyediaDistributorRepresentatif.findByDistributor(id);
		render(formHtml, distributor, pid, representatifList);
	}

	@AllowAccess({Acl.COM_PENYEDIA_DISTRIBUTOR, Acl.COM_PENYEDIA_DISTRIBUTOR_ADD,Acl.COM_PENYEDIA_DISTRIBUTOR_EDIT})
	public static void formSubmit(PenyediaDistributor distributor, String[] nama_representatif,
								  String[] telp_representatif, String[] email_representatif,
								  String username, Long userId, String rknDetailJson){

		List<PenyediaDistributorRepresentatif> representatifList = PenyediaDistributorRepresentatif.setList(nama_representatif, telp_representatif, email_representatif);
		long pid = distributor.penyedia_id;

		if(distributor.id == null && username.isEmpty()){
			params.flash();
			flash.error("Anda belum memilih user untuk menjadi distributor.");

			render(formHtml, distributor, pid, username, representatifList);
		}else{
			validation.valid(distributor);
			if(validation.hasErrors()) {
				params.flash();
				flash.error("Simpan Penyedia gagal. Silakan cek kembali isian Anda di tab Informasi Distributor.");

				render(formHtml, distributor, pid, username, representatifList);
			}else{

				if(distributor.user_id == null){
					if(userId != null){
						distributor.user_id = userId;
					}else{
						RekananDetail rkn = new Gson().fromJson(rknDetailJson,RekananDetail.class);
						long user_id = User.saveOrUpdateUsrRkn(rkn);
						distributor.user_id = user_id;
					}
				}

				distributor.active = true;
				// Admin langsung menyetujui distributor
				distributor.minta_disetujui = 0L;
				distributor.setuju_tolak_oleh = AktifUser.getAktifUser().user_id;
				distributor.setuju_tolak_tanggal = new Date();
				distributor.setuju_tolak = "Setuju";
				long distributor_id = 0L;

				if(distributor.id != null){
					PenyediaDistributorRepresentatif.deleteByDistributor(distributor.id);
					distributor.save();
					distributor_id = distributor.id;
				}else{
					distributor_id = distributor.save();
				}

				for(PenyediaDistributorRepresentatif rep : representatifList){
					rep.penyedia_distributor_id = distributor_id;
					rep.active = true;
					rep.save();
				}

				flash.success("Data Distributor berhasil ditambahkan");
				index(pid);
			}
		}

	}

	@AllowAccess({Acl.COM_PENYEDIA_DISTRIBUTOR_ADD,Acl.COM_PENYEDIA_DISTRIBUTOR_EDIT})
	public static void findUser(String searchType, String keyword){
		JsonObject params = new JsonObject();
		params.addProperty("username","");
		params.addProperty("nama","");
		params.addProperty("npwp","");
		params.addProperty("email","");

		params.addProperty(searchType,keyword);

		Logger.debug(params.toString());

		try{

			Config config = Config.getInstance();
			String param = URLs.encodePart(Encryption.encrypt(params.toString().getBytes()));
			String urlAuth = config.adp_rekanan_search + "?q=" + param;

			String content = WS.url(urlAuth).timeout("5min").getAsync().get().getString();
			String response = Encryption.decrypt(content);

			Logger.debug("response cari user :"+response);

			if(response != null && !response.isEmpty()){
				Type type = new TypeToken<List<RekananDetail>>(){}.getType();
				List<RekananDetail> adpUsers = new Gson().fromJson(response,type);
				Logger.debug(adpUsers.size()+"");
				UserJson results = new UserJson();
				if(adpUsers.size() > Penyedia.MAX_SEARCH_USER_FROM_ADP){
					results.message = "Hasil pencarian terlalu banyak. Berikan keyword yang lebih spesifik.";
				}else{
					results.message = "success";
					List<RekananDetail> rknResults = new ArrayList<RekananDetail>();

					for(RekananDetail adp : adpUsers){
						Logger.debug("rkn_username: "+adp.rkn_namauser);
						rknResults = RekananDetail.populateIfNotDistributor(adp, rknResults);
					}
					results.results = rknResults;
				}

				renderJSON(results);
			}

		}catch (Exception e){
			e.printStackTrace();
			UserJson results = new UserJson();
			results.message = "Terjadi Kesalahan. Silakan mencoba untuk memberikan keyword yang spesifik.";
			renderJSON(results);
		}

	}

	@AllowAccess({Acl.COM_PENYEDIA_DISTRIBUTOR})
	public static void detail(long pid, long id){
		PenyediaDistributor distributor = PenyediaDistributor.findDetailById(id);
		render(distributor, pid);
	}

	@AllowAccess({Acl.ALL_AUTHORIZED_USERS})
	public static void infoDistributor(long pid, long id){
		PenyediaDistributor distributor = PenyediaDistributor.findDetailById(id);
		render("penyedia/DistributorCtr/infoDistributor.html",distributor, pid);
	}

	@AllowAccess({Acl.COM_PENYEDIA_DISTRIBUTOR_DELETE})
	public static void delete(long id){
		PenyediaDistributor model= new PenyediaDistributor().findByIdActive(id, PenyediaDistributor.class);
		notFoundIfNull(model);
		if (model.isAllowToBeDeleted()) {
			model.softDelete();
			flash.success(Messages.get("notif.success.delete"));
		} else {
			flash.error(Messages.get("notif.failed.delete.relation"));
		}
		index(model.penyedia_id);
	}


	public static void persetujuanDistributorSubmit(String alasan, long[] idDistributor, String setujuTolakFp, Long penyedia_id){
		LogUtil.d("Approval", idDistributor);
		AktifUser aktifUser = getAktifUser();

		for(long id : idDistributor){
			try{
				PenyediaDistributor penyediaDistributor = PenyediaDistributor.findById(id);

				if(setujuTolakFp.equalsIgnoreCase("tolak") && alasan != null){
					penyediaDistributor.persetujuanDistributor(id, alasan, "Tolak", aktifUser.user_id);
				}else if(setujuTolakFp.equalsIgnoreCase("setuju")){
					penyediaDistributor.persetujuanDistributor(id, alasan, "Setuju", aktifUser.user_id);
				}
			}catch (Sql2oException e){
				e.printStackTrace();
				throw new UnsupportedOperationException();
			}
		}

		index(penyedia_id);
	}

//	tes commit
}
