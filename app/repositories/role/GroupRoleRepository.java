package repositories.role;

import com.google.gson.GsonBuilder;
import models.user.RoleGroupKategori;
import models.user.RoleGrup;
import models.user.RoleItem;
import play.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 11/24/2017
 */
public class GroupRoleRepository {

    public static final String TAG = "GroupRoleRepository";

    public List<RoleGroupKategori> getRolesCategories() {
        List<RoleGroupKategori> categories = new ArrayList<>();
        categories.add(new RoleGroupKategori(RoleGroupKategori.CMS));
        categories.add(new RoleGroupKategori(RoleGroupKategori.KOMODITAS));
        categories.add(new RoleGroupKategori(RoleGroupKategori.SERVICES));
        categories.add(new RoleGroupKategori(RoleGroupKategori.PRAKATALOG));
        categories.add(new RoleGroupKategori(RoleGroupKategori.Akses));
        categories.add(new RoleGroupKategori(RoleGroupKategori.MASTERDATA));
        List<RoleGrup> roleGrupList = RoleGrup.findAllActive();

        if (!roleGrupList.isEmpty()) {
            Map<Long, List<RoleItem>> map = RoleItem.getRoleItemMapGroupByRoleGroup();
            for (RoleGrup model : roleGrupList) {
                if (map.containsKey(model.id)) {
                    model.items.addAll(map.get(model.id));
                }
                for (RoleGroupKategori cat : categories) {
                    if (model.getTrimmedKategori().equalsIgnoreCase(cat.name)) {
                        cat.items.add(model);
                    }
                }
            }
        }
        return categories;
    }

}
