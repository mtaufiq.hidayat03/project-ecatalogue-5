INSERT INTO role_grup (nama_grup, kategori, active, created_date) VALUES
("Berita", "cms", 1, CURDATE());

INSERT INTO role_item (role_grup_id, nama_role, deskripsi, active, created_date) VALUES
  ((SELECT id FROM role_grup WHERE nama_grup = "Berita"), "comBerita", "Akses Manajemen Berita", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Berita"), "comBeritaAdd", "Akses Tambah Berita", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Berita"), "comBeritaEdit", "Akses Edit Berita", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Berita"), "comBeritaDelete", "Akses Hapus Berita", 1, CURDATE());
