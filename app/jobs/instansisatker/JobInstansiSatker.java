package jobs.instansisatker;

import jobs.TrackableJob;
import models.jcommon.config.Configuration;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.masterdata.Kldi;
import play.Logger;
import play.jobs.Job;

import java.util.List;
import java.util.UUID;

//@On("0 0 23 * * ?") 
// daily at 08 : 39
//@On("0 39 08 * * ?")
public class JobInstansiSatker extends TrackableJob {

	public void doJob() {
		pullData();
//		new Job() {
//			String uniqueID = UUID.randomUUID().toString();
//			@Override
//			public void before() {
//				Logger.info("job id:" + uniqueID);
//				super.before();
//			}
//
//			public void doJob() {
//				pullData();
//			}
//		}.in(1);
	}

	private void pullData() {
		Logger.info("Job Instansi Start");
		ExtInstansiData ext = new ExtInstansiData();
		ext.getInstansiData();
		ext.jalankan();

		//List<Instansi> instansiList = Instansi.findAll();
        List<Kldi> instansiList = Kldi.findAll();
		for (Kldi p : instansiList) {
			ExtSatkerData ext2 = new ExtSatkerData(p.id);
			ext2.instansiId = p.id;
			ext2.getSatkerData();
			ext2.jalankan();
		}

		String reportText = "Import data Instansi selesai";
		try {
			String report = Configuration
					.getConfigurationValue(MailQueue.SMTP_MAIL_ADMIN, "");
			String[] ary = report.split(";");
			for (String to : ary) {
				Logger.info("create mail prop job report to: " + to);
				//MailQueue mq = EmailManager.createEmail(to, reportText,
				//		"Report Job Dce InstansiSatker");
				//mq.save();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error("error create mail:" + e.getMessage());
		}
	}
}
