package controllers.admin;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.katalog.Komoditas;
import models.katalog.komoditasmodel.KomoditasProdukAtributTipe;
import models.katalog.komoditasmodel.form.AttributeForm;
import models.katalog.komoditasmodel.form.AttributePosition;
import models.secman.Acl;
import org.apache.http.util.TextUtils;
import play.i18n.Messages;
import utils.LogUtil;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by raihaniqbal on 7/19/17.
 */
public class KomoditasProdukAtributTipeCtr extends BaseController {


    @AllowAccess({Acl.COM_KOMODITAS_PRODUK_ATRIBUT_TIPE})
    public static void index(long komoditas_id){
        Komoditas komoditas = Komoditas.findById(komoditas_id);
        render("admin/KomoditasProdukAtributTipeCtr/index.html", komoditas);
    }

    @AllowAccess({Acl.COM_KOMODITAS_PRODUK_ATRIBUT_TIPE_ADD})
    public static void create(Long komoditas_id){
        Komoditas komoditas = Komoditas.findById(komoditas_id);
        render("admin/KomoditasProdukAtributTipeCtr/create.html", komoditas);
    }

    @AllowAccess({Acl.COM_KOMODITAS_PRODUK_ATRIBUT_TIPE_ADD})
    public static void createPosition(Long komoditas_id) {
        Komoditas komoditas = Komoditas.findById(komoditas_id);
        if (komoditas != null) {
            komoditas.withAttributesProduct();
        }
        render("admin/KomoditasProdukAtributTipeCtr/position.html", komoditas);
    }

    @AllowAccess({Acl.COM_KOMODITAS_PRODUK_ATRIBUT_TIPE_ADD})
    public static void createSubmit(AttributeForm form){
        checkAuthenticity();
        if (form.action.isEmpty()){
            form.action = "create";
        }
        validation.valid(form);
        if (validation.hasErrors()){
            validation.keep();
            params.flash();
            create(form.commodityId);
        }else{
            KomoditasProdukAtributTipe atributTipe;
            if (form.isCreateMode()) {
                atributTipe = new KomoditasProdukAtributTipe()
                        .setDataForm(form)
                        .setPosition(0);
            } else {
                atributTipe = KomoditasProdukAtributTipe.findById(form.id);
                atributTipe.setDataForm(form);
            }
            atributTipe.saveAtribut();
            index(form.commodityId);
        }
    }

    @AllowAccess({Acl.COM_KOMODITAS_PRODUK_ATRIBUT_TIPE_EDIT})
    public static void storePosition(Long id, String positions) {
        if (TextUtils.isEmpty(positions)) {
            positions = "[]";
        }
        Type collectionType = new TypeToken<Collection<AttributePosition>>() {
        }.getType();
        List<AttributePosition> products = new Gson().fromJson(positions, collectionType);
        if (products != null && !products.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            String glue = "";
            for (AttributePosition model : products) {
                if (model != null && model.id > 0) {
                    sb.append(glue).append("'").append(model.id).append("'");
                    glue = ",";
                }
            }
            Map<Long, KomoditasProdukAtributTipe> types = KomoditasProdukAtributTipe
                    .getAllByCommodityMapByIds(sb.toString());
            if (!types.isEmpty()) {
                int position = 0;
                for (AttributePosition model : products) {
                    KomoditasProdukAtributTipe type = types.get(model.id);
                    if (model.id > 0 && type != null) {
                        if (!TextUtils.isEmpty(model.name)) {
                            type.tipe_atribut = model.name;
                        }
                        type.active = true;
                        type.setPosition(position);
                        type.saveAtribut();
                        position++;
                    }
                }
            }
        }
        index(id);
    }

    @AllowAccess({Acl.COM_KOMODITAS_PRODUK_ATRIBUT_TIPE_EDIT})
    public static void edit(long id){
        KomoditasProdukAtributTipe kpat = KomoditasProdukAtributTipe.findById(id);
        Komoditas komoditas = Komoditas.findById(kpat.komoditas_id);
        renderArgs.put("action", "update");
        renderArgs.put("form", new AttributeForm(kpat));
        render("admin/KomoditasProdukAtributTipeCtr/create.html", komoditas);
    }

    @AllowAccess({Acl.COM_KOMODITAS_PRODUK_ATRIBUT_TIPE_EDIT})
    public static void editSubmit(long kpat_id, String tipe_atribut, String deskripsi){
        KomoditasProdukAtributTipe kpat = KomoditasProdukAtributTipe.findById(kpat_id);
        kpat.tipe_atribut = tipe_atribut;
        kpat.deskripsi = deskripsi;
        kpat.saveAtribut();

        index(kpat.komoditas_id);
    }

    @AllowAccess({Acl.COM_KOMODITAS_PRODUK_ATRIBUT_TIPE_DELETE})
    public static void delete(long id, long komoditas_id){
        KomoditasProdukAtributTipe model = KomoditasProdukAtributTipe.findById(id);
        notFoundIfNull(model);
        if (model.isAllowedToBeDeleted()) {
            model.softDelete();
            flash.success(Messages.get("notif.success.delete"));
        } else {
            flash.error(Messages.get("notif.failed.delete.relation"));
        }
        index(komoditas_id);
    }



}
