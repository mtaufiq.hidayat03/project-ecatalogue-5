DROP TABLE IF EXISTS `agenda_kegiatan`;
CREATE TABLE `agenda_kegiatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usulan_id` int(11) DEFAULT NULL,
  `tanggal_kegiatan` date DEFAULT NULL,
  `waktu_kegiatan` time DEFAULT NULL,
  `judul_kegiatan` varchar(255) DEFAULT NULL,
  `lokasi` varchar(255) DEFAULT NULL,
  `keterangan` text,
  `nama_file` varchar(255) DEFAULT NULL,
  `blb_id_content` int(11) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `deleted_date` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;