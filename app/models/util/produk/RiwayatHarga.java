package models.util.produk;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * Created by dadang on 8/28/17.
 */
public class RiwayatHarga {

	public Date tanggal;
	public String kurs;
	public Map<Long, BigDecimal> harga;

}
