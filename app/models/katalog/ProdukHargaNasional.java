package models.katalog;

import models.BaseKatalogTable;
import play.Logger;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.Date;
import java.util.List;

/**
 * Created by dadang on 11/27/17.
 */
@Table(name = "produk_harga_nasional")
public class ProdukHargaNasional extends BaseKatalogTable {

	@Id
	public Long id;
	public Long produk_id;
	public Long kurs_id;
	public Double harga;
	public Date harga_tanggal;
	public boolean active;
	public boolean approved;
	public Long komoditas_harga_atribut_id;
	public Date approved_date;
	public Long approved_by;

	public ProdukHargaNasional(){}
	public ProdukHargaNasional(Long produk_id, Long kurs_id, Double harga, Long komoditas_harga_atribut_id) {
		this.produk_id = produk_id;
		this.kurs_id = kurs_id;
		this.harga = harga;
		this.komoditas_harga_atribut_id = komoditas_harga_atribut_id;

		// default
		// this.harga_tanggal = new Date();
		this.active = true;
	}

	public static ProdukHargaNasional getAktifByProdukId(Long produk_id){
		ProdukHargaNasional phn = Query.find("select hn.* from produk_harga_nasional hn where hn.produk_id=? and hn.active=1 order by hn.modified_date desc",ProdukHargaNasional.class, produk_id).first();
		return phn;
	}

	public static List<ProdukHargaNasional> getByProductIdAndAtributid(long produk_id, long komoditas_harga_atribut_id){
		return find("produk_id=? and komoditas_harga_atribut_id=? and active = ?",produk_id, komoditas_harga_atribut_id, AKTIF).fetch();
	}

}
