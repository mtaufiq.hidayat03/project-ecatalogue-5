package models.prakatalog;

import com.google.gson.Gson;
import models.BaseKatalogTable;
import models.common.AktifUser;
import models.jcommon.blob.BlobTable;
import models.katalog.Komoditas;
import models.katalog.UsulanPokja;
import models.masterdata.Kldi;
import models.masterdata.Tahapan;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;
import play.mvc.Router;
import utils.DateTimeUtils;
import utils.KatalogUtils;

import javax.persistence.Transient;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * Created by dadang on 7/20/17.
 */
@Table(name = "usulan")
public class Usulan extends BaseKatalogTable {
	
	public static int LIMIT_CHAR_TITLE = 50;
	public static int LIMIT_LESS_CHAR_TITLE = 30;

	@Id
	public Long id;
	@Required
	public String nama_usulan;
	@Required
	public String kldi_id;
	public String pengusul_alamat;
	@Required
	public String pengusul_nama;
	public String pengusul_email;
	@Required
	public String pengusul_nik;
	public String pengusul_no_telp;
	public String status;

	public Long komoditas_id;

	public String doc_review;
	public Long doc_review_id;

	public boolean active;

	@Required
	public Long template_jadwal_id;

	public Long pokja_id;

	public String judul_pengumuman;

	@Transient
	public String komoditas;

	@Transient
	public String nama_komoditas;

	@Transient
	public String komoditas_kategori;

	@Transient
	public String kldi;

	@Transient
	public Date started_date;
	@Transient
	public Date finished_date;

	@Transient
	public long total_penawaran = 0;
	
	@Transient
	public long[] pokja_ids = new long[0];
	@Transient
    public long[] kategori_ids = new long[0];

	public static final String STATUS_BARU = "baru";
	public static final String STATUS_DRAFT = "draft";
	public static final String STATUS_PENAWARAN_DIBUKA = "penawaran_dibuka";
	public static final String STATUS_TUTUP_PENAWARAN = "tutup_penawaran";

	public static final String[] STATUSES = {STATUS_BARU, STATUS_PENAWARAN_DIBUKA, STATUS_TUTUP_PENAWARAN};

	public static Map<String,String> statusMaps(){
		Map<String,String> map = new HashMap<String,String>();
		map.put(STATUS_BARU, KatalogUtils.getStatusLabel("Baru"));
		map.put(STATUS_DRAFT, KatalogUtils.getStatusLabel("Draft"));
		map.put(STATUS_PENAWARAN_DIBUKA,KatalogUtils.getStatusLabel("Telah Buat Pengumuman"));
		map.put(STATUS_TUTUP_PENAWARAN,KatalogUtils.getStatusLabel("Penawaran Telah Ditutup"));
		return map;
	}

	public void withPokja() {
		if(this.id != null) {
			Logger.info(" ~~=== usulan id === ~~~ " + this);
			List<UsulanPokja> listPokja = UsulanPokja.find("usulan_id = ? and active = 1", this.id).fetch();
			pokja_ids = listPokja.stream().map(x -> x.pokja_id.longValue()).mapToLong(l->l).toArray();
		}
		else
			pokja_ids = new long[0];
	}

	public void withKategori() {
		if(this.id != null) {
			List<UsulanKategoriProduk> lisKategori = UsulanKategoriProduk.find("usulan_id = ? and active = 1", this.id).fetch();
			kategori_ids = lisKategori.stream().map(x -> x.produk_kategori_id.longValue()).mapToLong(l->l).toArray();
		}
		else
			kategori_ids = new long[0];
	}

	public boolean isPokjaIdExist(Long ids){
//		Logger.info(" ===========   isPokjaIdExist   ======= " + ids);
		return LongStream.of(pokja_ids).anyMatch(x -> x == ids);
	}
	public void savePokja() {
	    Logger.info("pokja ids" + pokja_ids);
		List<UsulanPokja> listPokjaAktif = UsulanPokja.find("usulan_id = ? and active = 1", this.id).fetch();

		List<UsulanPokja> listPokjaNonAktif = UsulanPokja.find("usulan_id = ? and not active = 1", this.id).fetch();
		//idpokjaaktif
		List<Long> listPokjaIdAktif = listPokjaAktif.stream().map(x->x.pokja_id).collect(Collectors.toList());
		//idpokjanonaktif
		List<Long> listPokjaIdNonAktif = listPokjaNonAktif.stream().map(x->x.pokja_id).collect(Collectors.toList());
		//pokaid_dariform
		List<Long> listPokjaIdBaru = Arrays.stream(pokja_ids).boxed().collect(Collectors.toList());

		//id yang akan dihapus, karena gak ada di listform submit
		List<Long> listPokjaIdToDelete = listPokjaIdAktif.stream().filter(l->!listPokjaIdBaru.contains(l)).collect(Collectors.toList());

		//enable yang gak aktif, karena ada di listfom submit
		List<Long> listPokjaIdToRevive = listPokjaIdNonAktif.stream().filter(l->listPokjaIdBaru.contains(l)).collect(Collectors.toList());

		//baru karena gak ada di yang aktif, dan yang akan kembali diaktifkan karena not active
		List<Long> listPokjaIdToAdd = listPokjaIdBaru.stream().filter(l->!listPokjaIdAktif.contains(l) && !listPokjaIdToRevive.contains(l)).collect(Collectors.toList());


		listPokjaIdToAdd.forEach(l->{
			UsulanPokja up = new UsulanPokja();
			up.usulan_id = this.id;
			up.pokja_id = l;
			up.active = true;
			Long idup = new Long(up.save());
		});

		listPokjaIdToRevive.forEach(l->{
			final Optional<UsulanPokja> oup = listPokjaNonAktif.stream().filter(u -> u.pokja_id.equals(l)).findFirst();
			if(oup.isPresent()) {
				UsulanPokja up = oup.get();
				up.reactivate();
			}
		});

		listPokjaIdToDelete.forEach(l->{
			final Optional<UsulanPokja> oup = listPokjaAktif.stream().filter(u -> u.pokja_id.equals(l)).findFirst();
			if(oup.isPresent()) {
				UsulanPokja up = oup.get();
				up.delete();
			}
		});
	}

	public boolean isKategoriIdExist(Long idk){
	    Logger.info("== idk UUU " + idk);
		return LongStream.of(kategori_ids).anyMatch(x -> x == idk);
	}
    public void saveKategori() {
        Logger.info("kategori ids" + kategori_ids);
		Logger.info("this id " + this.id);
//		List<Long> listKategoriUpdate = Arrays.stream(kategori).boxed().collect(Collectors.toList());

        List<UsulanKategoriProduk> listKategoriAktif = UsulanKategoriProduk.find("usulan_id = ? and active = 1", this.id).fetch();
        Logger.info("listKategoriAktif " + Arrays.toString(listKategoriAktif.toArray()));
        List<UsulanKategoriProduk> listKategoriNonAktif = UsulanKategoriProduk.find("usulan_id = ? and not active = 1", this.id).fetch();
        Logger.info("listKategoriNonAktif " + Arrays.toString(listKategoriNonAktif.toArray()));
        List<Long> listKategoriIdAktif = listKategoriAktif.stream().map(x->x.produk_kategori_id).collect(Collectors.toList());

        List<Long> listKategoriIdNonAktif = listKategoriNonAktif.stream().map(x->x.produk_kategori_id).collect(Collectors.toList());

        List<Long> listKategoriIdBaru = Arrays.stream(kategori_ids).boxed().collect(Collectors.toList());
        Logger.info("listKategoriIdBaru " + Arrays.toString(listKategoriIdBaru.toArray()));

        List<Long> listKategoriIdToDelete = listKategoriIdAktif.stream().filter(l->!listKategoriIdBaru.contains(l)).collect(Collectors.toList());

        List<Long> listKategoriIdToRevive = listKategoriIdNonAktif.stream().filter(l->listKategoriIdBaru.contains(l)).collect(Collectors.toList());

        List<Long> listKategoriIdToAdd = listKategoriIdBaru.stream().filter(l->!listKategoriIdAktif.contains(l) && !listKategoriIdToRevive.contains(l)).collect(Collectors.toList());
        Logger.info("listKategoriIdToAdd " + Arrays.toString(listKategoriIdToAdd.toArray()));

        listKategoriIdToAdd.forEach(l->{
            UsulanKategoriProduk ukp = new UsulanKategoriProduk();
            ukp.usulan_id = this.id;
            ukp.produk_kategori_id = l;
            ukp.active = true;
            Long idup = new Long(ukp.save());
        });

		listKategoriIdToRevive.forEach(l->{
            final Optional<UsulanKategoriProduk> oup = listKategoriNonAktif.stream().filter(u -> u.produk_kategori_id.equals(l)).findFirst();
            if(oup.isPresent()) {
				UsulanKategoriProduk ukp = oup.get();
                ukp.reactivate();
            }
        });

		listKategoriIdToDelete.forEach(l->{
            final Optional<UsulanKategoriProduk> oup = listKategoriAktif.stream().filter(u -> u.produk_kategori_id.equals(l)).findFirst();
            if(oup.isPresent()) {
				UsulanKategoriProduk ukp = oup.get();
                ukp.delete();
            }
        });
    }

	public void setDataForm(Usulan model) {
		this.nama_usulan = model.nama_usulan;
		this.kldi_id = model.kldi_id;
		this.pengusul_nama = model.pengusul_nama;
		this.pengusul_alamat = model.pengusul_alamat;
		this.pengusul_email = model.pengusul_email;
		this.pengusul_nik = model.pengusul_nik;
		this.pengusul_no_telp = model.pengusul_no_telp;
	}

	public static List<Usulan> findAllByStatus(String status){

		String query = "select u.id, u.nama_usulan, u.komoditas_id, k.nama_komoditas as komoditas, u.status, u.judul_pengumuman " +
				"from usulan u " +
				"join komoditas k on u.komoditas_id = k.id " +
				"where u.status = ? and u.active = ? " +
				"order by u.modified_date desc";

		return Query.find(query, Usulan.class,status, AKTIF).fetch();
	}

	public static Usulan getDetail(long id){
		String query = "select u.id, u.komoditas_id, k.nama_komoditas as komoditas, u.nama_usulan, kldi.nama as kldi, " +
				"u.pengusul_nama, u.pengusul_nik, u.pengusul_email, u.`pengusul_no_telp`, u.pengusul_alamat, u.status, u.pokja_id, u.template_jadwal_id, u.doc_review, u.doc_review_id " +
				"from usulan u " +
				"join komoditas k on u.`komoditas_id` = k.id " +
				"join kldi on u.kldi_id = kldi.id " +
				"where u.id = ?";

		return Query.find(query, Usulan.class,id).first();
	}
	
	public String getTrimmedTitle() {
		String title = !TextUtils.isEmpty(judul_pengumuman) ? judul_pengumuman : nama_usulan;

		if(title.length() < LIMIT_CHAR_TITLE) {
			return title;
		}
		return title.substring(0, LIMIT_CHAR_TITLE) + "...";
	}

	public boolean isTenderingOpen(Long usulan_id){
		Usulan usulan = Usulan.findById(usulan_id);
		Tahapan tahapanPenawaran = Tahapan.getTahapanPenawaran(usulan.template_jadwal_id);

		Date today = new Date();
		Date tanggalAwal = UsulanJadwal.getTanggalAwal(usulan_id,tahapanPenawaran.id);

		if(today.compareTo(tanggalAwal) < 0){
			return false;
		}
		return true;
	}

	public String getStartDateTendering(Long usulan_id){
		Usulan usulan = Usulan.findById(usulan_id);
		Tahapan tahapanPenawaran = Tahapan.getTahapanPenawaran(usulan.template_jadwal_id);

		Date tanggalAwal = UsulanJadwal.getTanggalAwal(usulan_id,tahapanPenawaran.id);

		return DateTimeUtils.formatDateToString(tanggalAwal,"dd MMMM yyyy");
	}

	public String getEndDateTendering(Long usulan_id){
		Usulan usulan = Usulan.findById(usulan_id);
		Tahapan tahapanPenawaran = Tahapan.getTahapanPenawaran(usulan.template_jadwal_id);

		Date tanggalAkhir = UsulanJadwal.getTanggalAkhir(usulan_id,tahapanPenawaran.id);

		return DateTimeUtils.formatDateToString(tanggalAkhir,"dd MMMM yyyy");
	}

	public static Long jumlahPenawaranByUsulanId(Long usulan_id){
		return Penawaran.getJumlahPenawaran(usulan_id);
	}

	public static void deleteById(long id){
		Usulan usulan = Usulan.findById(id);
		usulan.active = false;
		usulan.save();
	}

	public void softDelete() {
		this.active = false;
		setDeleted();
		save();
	}
	

	public static Integer getTotalUsulanByTemplate(Long id) {
		return Query.find("SELECT COUNT(*) FROM usulan " +
				"WHERE template_jadwal_id =? AND active =?", Integer.class, id, AKTIF).first();
	}

	public boolean isOpen() {
		return this.status.equalsIgnoreCase(STATUS_PENAWARAN_DIBUKA);
	}

	public boolean isAllowedToBeDeleted() {
		return this.status.equalsIgnoreCase(STATUS_BARU);
	}

	public String getReadableStartedDate() {
		return DateTimeUtils.parseToReadableDate(this.started_date);
	}

	public String getReadableFinishedDate() {
		return DateTimeUtils.parseToReadableDate(this.finished_date);
	}

	public String getNamaKomoditas(){
		Usulan usulan = Usulan.findById(this.id);
		Komoditas komoditas = Komoditas.findById(usulan.komoditas_id);
		return this.komoditas=komoditas.nama_komoditas;
	}

	public String getTrimmedKomoditas() {
		Usulan usulan = Usulan.findById(this.id);
		Komoditas komoditas = Komoditas.findById(usulan.komoditas_id);

		if(komoditas.nama_komoditas.length() < LIMIT_LESS_CHAR_TITLE) {
			return this.komoditas = komoditas.nama_komoditas;
		}
		return this.komoditas = komoditas.nama_komoditas.substring(0, LIMIT_LESS_CHAR_TITLE) + "...";
	}

	public String getTitle() {
		return !TextUtils.isEmpty(judul_pengumuman) ? judul_pengumuman : nama_usulan;
	}

	public String getDownloadReviewDoc(){
		try{
			return BlobTable.findByBlobId(this.doc_review_id).getDownloadUrl(null);
		}catch (Exception e){
			return "#";
		}
	}

	public Komoditas getKomoditas(){
		return Komoditas.findById(this.komoditas_id);
	}

	public boolean isValidKomoditasForAktifUserByOverideRole(){
		boolean result = false;
		try{
			AktifUser aktifuser = AktifUser.getAktifUser();
			Logger.info(this.getKomoditas().isKomoditasLokal()+" lokal");
			Logger.info(this.getKomoditas().isKomodtiasSektoral()+" sektoral");
			//cek sektoral atau lokal
			boolean apakahLokalAtauSektoral = (this.getKomoditas().isKomoditasLokal() ^ this.getKomoditas().isKomodtiasSektoral());
			if(apakahLokalAtauSektoral && aktifuser.getUser().override_role_komoditas == true){
				for (Long ids:aktifuser.komoditas_list ) {
					Logger.info(ids +"-"+this.komoditas_id);
					if(ids.equals(this.komoditas_id)){
						return true;
					}
				}

			}else if(apakahLokalAtauSektoral == false && aktifuser.getUser().override_role_komoditas){
				Logger.info("nonsektoral dan akses seluruh komoditas");
				return true;
			}else if(apakahLokalAtauSektoral){
				Boolean sama = this.getKomoditas().kldi_id.equals(aktifuser.getUser().kldi_id);
				if(sama){
					//next samakan lokasi
					Kldi kldi= Kldi.findById(this.getKomoditas().kldi_id);
					Logger.debug("kldinya:"+ new Gson().toJson(kldi));
				}

				return sama;
			}
		}catch (Exception e){}
		return result;
	}

	public boolean isValidKomoditasForAktifUser(){
		try{
			AktifUser aktifuser = AktifUser.getAktifUser();
			Logger.info(this.getKomoditas().isKomoditasLokal()+" lokal");
			Logger.info(this.getKomoditas().isKomodtiasSektoral()+" sektoral");
			//cek sektoral atau lokal
			boolean apakahLokalAtauSektoral = (this.getKomoditas().isKomoditasLokal() ^ this.getKomoditas().isKomodtiasSektoral());
			if(apakahLokalAtauSektoral){
//				Logger.debug("Usulan lokalatausektoral:"+apakahLokalAtauSektoral);
				//lokasi ke kldi komoditas
//				List<PenyediaWilayah> list = aktifuser.getUser().getAsPenyediaIfPenyedia().getPenyediaWilayah();
//				for (PenyediaWilayah pw: list) {
//					if(pw.kldi_id.equals(this.getKomoditas().kldi_id)){
//						return true;
//					}
//				}

				//edited by Fajar : 31-01-2020
				//Pengecekan Assign Penyedia Pengajuan Penawaran dinonaktifkan
//				List<String>locKldiuser =aktifuser.getUser().getAsPenyediaIfPenyedia().locationSelected();
//				List<String> locSek = aktifuser.getUser().getAsPenyediaIfPenyedia().locationSektoralSelected();
//				locKldiuser.addAll(locSek);
//				List<String>locKldikomo = this.getKomoditas().locationSelected();
//
//				Logger.info("lokasipenyedia:"+new Gson().toJson(locKldiuser));
//				Logger.info("lokasikomoditas:"+new Gson().toJson(locKldikomo));
				//remove when not in those content
//				locKldikomo.retainAll(locKldiuser);
//				Logger.info("resultcompare:"+new Gson().toJson(locKldikomo));
//				if(locKldikomo.size() > 0){
//					return true;
//				}
				return true;

			}else if(apakahLokalAtauSektoral == false && !aktifuser.getUser().override_role_komoditas){
				Logger.info("nonsektoral dan akses seluruh komoditas");
				return true;
			}else{
				//selain lokal atau sectoral
				if(aktifuser.getUser().override_role_komoditas == true){
					for (Long ids:aktifuser.komoditas_list ) {
						Logger.info(ids +"-"+this.komoditas_id);
						if(ids.equals(this.komoditas_id)){
							return true;
						}
					}

				}
			}

		}catch (Exception e){}
		Logger.debug("usulan model isValidKomoditasForAktifUser :"+false);
		return false;
	}

	public String getReadableStatus(){
		Map<String,String> statusMaps = statusMaps();
		String result = "";

		for (Object key : statusMaps.keySet()) {
			if(key.toString().equalsIgnoreCase(status)){
				result = statusMaps.get(key);
			}
		}
		return result;
	}

	public StringBuilder getMakeButtonGroups() {
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("id", id);

			Map<String, Object> paramrev = new HashMap<String, Object>();
			paramrev.put("usulan_id", id);

			String urlDetail = Router.reverse("controllers.admin.UsulanCtr.detail", param).url;
			String urlBukaPenawaran = Router.reverse("controllers.admin.UsulanCtr.bukaPenawaran", param).url;

			StringBuilder htmlBuilder = new StringBuilder("<ul class='dropdown-menu' aria-labelledby='dropdownMenu4'>");
			boolean allow = AktifUser.getAktifUser().isAuthorized("comPenawaranBuka");
			if (status.equalsIgnoreCase(STATUS_BARU) && allow) {
				htmlBuilder.append("<li><a target='_blank' href='")
						.append(urlBukaPenawaran).append("'>")
						.append(Messages.get("buat.label") + " " + Messages.get("pengumuman.label"))
						.append("</a></li>");
			}

			if (status.equalsIgnoreCase(STATUS_DRAFT) && allow) {
				htmlBuilder.append("<li><a target='_blank' href='")
						.append(urlBukaPenawaran).append("'>")
						.append(Messages.get("update") + " " + Messages.get("pengumuman.label"))
						.append("</a></li>");
			}

			String docReviewid = (null == doc_review_id ? "" : doc_review_id.toString());
			Boolean belumReview = docReviewid.isEmpty();
			String urlReviewUsulan = Router.reverse("controllers.admin.UsulanCtr.reviewUsulan", paramrev).url;
//			String urlDetilReviewUsulan = Router.reverse("controllers.admin.UsulanCtr.DetailReviewUsulan",new HashMap<String, Object>()).url;

			if (belumReview) {
				if (AktifUser.getAktifUser().isAuthorized("comUsulanReview")) {
					htmlBuilder.append("<li><a target='_blank' href='")
							.append(urlReviewUsulan).append("'>")
							.append(Messages.get("review_usulan.label"))
							.append("</a></li>");
				}
			}

			htmlBuilder.append("<li><a target='_blank' href='")
					.append(urlDetail).append("'>")
					.append(Messages.get("detail_usulan.label"))
					.append("</a></li>");
			htmlBuilder.append("</ul>");
			return htmlBuilder;
		} catch (Exception e) {
			Logger.error("Error usulandatatableservice buttongroup:", e);
			e.printStackTrace();
			throw new UnsupportedOperationException(e);
		}
	}

	public Boolean isNew() {
		return !TextUtils.isEmpty(status) && status.equalsIgnoreCase(STATUS_BARU);
	}

	public Boolean isPenawaranDibuka() {
		return !TextUtils.isEmpty(status) && status.equalsIgnoreCase(STATUS_PENAWARAN_DIBUKA);
	}

}
