package models.permohonan;

import models.katalog.Produk;
import models.katalog.ProdukAtributValue;
import models.katalog.ProdukHarga;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProdukHargaStagging {
    public Long produk_id;

    //information only, do not apply on spplystaging
    public List<ProdukAtributValue> produkAtributValues = new ArrayList<>();

    public List<ProdukHarga> produkHargas = new ArrayList<>();
    public String ongkir = "";
    public Date harga_tanggal;
    public boolean isNasional(){
        boolean result = false;
        try{
            Produk p = Produk.findByIdCheck(produk_id);
            result = p.getCommodity().isNational();
        }catch (Exception e){}
        return result;
    }

    public boolean isRegency(){
        boolean result = false;
        try{
            Produk p = Produk.findByIdCheck(produk_id);
            result = p.getCommodity().isRegency();
        }catch (Exception e){}
        return result;
    }

    public boolean isProvince(){
        boolean result = false;
        try{
            Produk p = Produk.findByIdCheck(produk_id);
            result = p.getCommodity().isProvince();
        }catch (Exception e){}
        return result;
    }
}

/*
catatan stagging:
ongkir berada di field ongkir
harga tanggal berada di field harga tanggal untuk diterapkan di produk dan produk_harga

 */
