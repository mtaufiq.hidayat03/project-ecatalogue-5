package models.prakatalog;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import models.jcommon.blob.BlobTable;

import javax.persistence.Transient;

import java.util.Date;
import java.util.List;


@Table(name = "agenda_kegiatan")
public class AgendaKegiatan extends BaseKatalogTable {

    @Id
    public Long id;

    public Long usulan_id;

    public String judul_kegiatan;

    public String lokasi;

    public String keterangan;

    public String nama_file;

    public Long  blb_id_content;

    public Date tanggal_kegiatan;

    public Date waktu_kegiatan;

    public Boolean active;

    @Transient
    public String download_url;
    

    public static List<AgendaKegiatan> findByUsulan(long usulan_id){
        List<AgendaKegiatan> results = AgendaKegiatan.find("usulan_id = ? order by tanggal_kegiatan",usulan_id).fetch();

        for(AgendaKegiatan ak : results){
            if(ak.blb_id_content!=null) {
                BlobTable blobTable = BlobTable.findByBlobId(ak.blb_id_content);
                if(blobTable != null){
                    ak.download_url = blobTable.getDownloadUrl(null);
                }
            }
        }
        return results;

    }

    public static int removeDocument(long agendaId){
        String query = "UPDATE agenda_kegiatan SET nama_file = null, blb_id_content = null WHERE id = ?";
        return Query.update(query,agendaId);
    }



}
