package models.katalog;

import models.BaseKatalogTable;
import models.katalog.payload.ProdukKategoriAtributItem;
import models.katalog.payload.ProdukKategoriAtributPosisi;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

@Table(name = "produk_kategori_atribut")
public class ProdukKategoriAtribut extends BaseKatalogTable {

	@Transient
	public static final String TYPE_SPECIFICATION = "Spesifikasi";
	@Transient
	public static final String TYPE_ADDITIONAL_INFORMATION = "Keterangan Tambahan";
	@Transient
	public static final String INPUT_TYPE_TEXT = "text";
	@Transient
	public static final String INPUT_TYPE_TEXTAREA = "textarea";
	@Transient
	public static final String INPUT_LABEL = "label";
	@Transient
	public static final String[] INPUT_TYPES = {INPUT_TYPE_TEXT, INPUT_TYPE_TEXTAREA, INPUT_LABEL};
	@Transient
	public static final String[] INFORMATION_TYPES = {TYPE_SPECIFICATION, TYPE_ADDITIONAL_INFORMATION};

    @Id
    public Long id;
    public Long komoditas_id;
    public Long komoditas_produk_atribut_tipe_id;
    public String komoditas_produk_atribut_tipe_label;
    public Long produk_kategori_id;
    public Long parent_id;
    public Integer posisi_item;
    public String label_atribut;
    public String deskripsi;
    public String tipe_input;
    public boolean wajib_diisi;
    public boolean active;
    public long level_hirarki;

    @Transient
    public List<ProdukKategoriAtribut> children = new ArrayList<>();

	@Transient
	public String tipe_atribut;

    public ProdukKategoriAtribut() {}

    public ProdukKategoriAtribut(ProdukKategoriAtributItem model) {
    	this.komoditas_id = model.commodityId;
    	this.produk_kategori_id = model.categoryProductId;
    	this.label_atribut = model.label;
    	this.deskripsi = model.desc;
    	this.tipe_input = model.type;
    	this.wajib_diisi = model.isRequired;
    	this.komoditas_produk_atribut_tipe_id = model.informationType;
    	this.komoditas_produk_atribut_tipe_label = model.informationLabel;
    	this.parent_id = model.parent;
    	this.posisi_item = 0;
    	this.active = true;
	}

	public void setUpdatePosisi(ProdukKategoriAtributPosisi model) {
    	this.parent_id = model.parentId;
    	this.level_hirarki = model.depth;
    	this.active = true;
	}

	public void setUpdateValue(ProdukKategoriAtributItem model) {
		this.label_atribut = model.label;
		this.deskripsi = model.desc;
		this.tipe_input = model.type;
		this.wajib_diisi = model.isRequired;
		this.komoditas_produk_atribut_tipe_id = model.informationType;
		this.komoditas_produk_atribut_tipe_label = model.informationLabel;
		this.active = true;
	}

	public long getParent() {
    	return parent_id;
	}

	public void softDelete() {
		this.active = false;
		setDeleted();
		save();
	}

	public Long getId() {
    	return this.id;
	}

}
