ALTER TABLE produk_harga ADD komoditas_harga_atribut_id INT NULL;
ALTER TABLE produk_harga ADD kelas_harga VARCHAR(255) NULL;

UPDATE produk_harga_nasional SET harga_tanggal = NULL WHERE CAST(harga_tanggal  AS CHAR(20)) = '0000-00-00';
UPDATE produk_harga_nasional SET created_date = NULL WHERE CAST(created_date  AS CHAR(20)) = '0000-00-00 00:00:00';
UPDATE produk_harga_nasional SET modified_date = NULL WHERE CAST(modified_date  AS CHAR(20)) = '0000-00-00 00:00:00';
UPDATE produk_harga_nasional SET deleted_date = NULL WHERE CAST(deleted_date  AS CHAR(20)) = '0000-00-00 00:00:00';
UPDATE produk_harga_nasional SET approved_date = NULL WHERE CAST(approved_date  AS CHAR(20)) = '0000-00-00 00:00:00';

CREATE INDEX nasional_produk_id_harga_atribut_id_tanggal_index ON produk_harga_nasional (produk_id, komoditas_harga_atribut_id, harga_tanggal);

CREATE INDEX migration_idx ON produk_harga_kabupaten (produk_id, komoditas_harga_atribut_id, created_date);