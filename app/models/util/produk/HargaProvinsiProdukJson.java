package models.util.produk;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dadang on 8/23/17.
 */
public class HargaProvinsiProdukJson {
	public BigDecimal harga;
	public Long provinsiId;
}
