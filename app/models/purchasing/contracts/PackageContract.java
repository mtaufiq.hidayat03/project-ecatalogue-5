package models.purchasing.contracts;

import models.common.AktifUser;
import models.masterdata.Kldi;
import models.masterdata.Kurs;
import models.masterdata.Satker;
import models.penyedia.Penyedia;
import models.penyedia.PenyediaDistributor;
import models.purchasing.PaketAnggotaUlp;
import models.purchasing.PaketSumberDana;
import models.purchasing.negosiasi.PaketProdukHeader;
import models.purchasing.kontrak.PaketKontrak;
import models.purchasing.PaketProduk;
import models.purchasing.pembayaran.Pembayaran;
import models.purchasing.pembayaran.PembayaranLampiran;
import models.purchasing.penerimaan.PaketRiwayatPenerimaan;
import models.purchasing.penerimaan.ProdukPenerimaan;
import models.purchasing.pengiriman.PaketRiwayatPengiriman;
import models.purchasing.pengiriman.ProdukPengiriman;
import models.purchasing.riwayat.PaketRiwayat;
import models.purchasing.riwayat.PaketRiwayatNegosiasi;
import models.purchasing.status.PaketStatus;
import models.purchasing.status.StatusMessage;
import play.Logger;
import repositories.paket.*;

import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 11/2/2017
 */
public interface PackageContract extends BaseContract {

    String TAG = "PackageContract";

    void setPayment(List<Pembayaran> payments);

    void setDeliveries(List<PaketRiwayatPengiriman> deliveries);

    void setAcceptances(List<PaketRiwayatPenerimaan> acceptances);

    void setPackageHistories(List<PaketRiwayat> histories);

    void setSatker(Satker satker);

    void setKldi(Kldi kldi);

    void setTotalNegotiations(List<PaketRiwayatNegosiasi> list);

    void setNegotiationHistory(List<PaketRiwayatNegosiasi> list);

    void setProducts(List<PaketProduk> list);

    void setContracts(List<PaketKontrak> list);

    void setContractValue(Double value);

    void setStatusMessage(StatusMessage model);

    void setKurs(Kurs kurs);

    void setNegoHeader(PaketProdukHeader paketProdukHeader);

    void setPaketStatus(PaketStatus paketStatus);

    void setPenyedia(Penyedia penyedia);

    void setSumberDana(List<PaketSumberDana> sumberDana);

    void setUlPMembers(List<PaketAnggotaUlp> list);

    Long getPenyediaId();

    String getSatketId();

    String getKldiId();

    List<ProdukPengiriman> getDeliveryProducts();

    List<ProdukPenerimaan> getAcceptanceProducts();

    List<Pembayaran> getPayments();

    PaketRiwayatPenerimaan getAcceptanceHistory();

    Long getKursId();

    Kurs getKurs();

    PaketProdukHeader getProdukNegoHeader();

    PaketStatus getPaketStatus();

    List<PaketSumberDana> getPaketSumberDana();

    default void withPayments(AktifUser aktifUser) {
        if (getId() != null) {
            Logger.debug(TAG + " get payments");
            List<Pembayaran> payments = PaymentRepository.getPaymentHistoriesByPackageId(getId());
            if (payments != null && !payments.isEmpty()) {
                for (Pembayaran model : payments) {
                    model.setEnabledMenus(aktifUser);
                }
                setPayment(payments);
                Map<Long, PembayaranLampiran> results = PaymentRepository.getPaymentAttachmentsMapModelId(getId(), getPaymentIds());
                if (!results.isEmpty()) {
                    for (Pembayaran model : getPayments()) {
                        model.setAttachment(results.get(model.getId()));
                    }
                }
            }
        }
    }

    default String getPaymentIds() {
        StringBuilder sb = new StringBuilder();
        if (getPayments() != null && !getPayments().isEmpty()) {
            String glue = "";
            for (Pembayaran model : getPayments()) {
                sb.append(glue).append("'").append(model.getId()).append("'");
                glue = ",";
            }
        }
        return sb.toString();
    }

    default void withAcceptances() {
        if (getId() != null) {
            setAcceptances(AcceptanceRepository.getHistoriesByPackageId(getId()));
        }
    }

    default void withDeliveries() {
        if (getId() != null) {
            setDeliveries(DeliveryRepository.getDeliveryHistoriesByPackageId(getId()));
        }
    }

    default void withSatker() {
        if (getSatketId() != null) {
            setSatker(Satker.findById(getSatketId()));
        }
    }

    default void withKldi() {
        if (getKldiId() != null) {
            setKldi(Kldi.findById(getKldiId()));
        }
    }

    default void withPackageHistories() {
        if (getId() != null) {
            setPackageHistories(PaketRiwayat.getHistoriesByPackageId(getId()));
        }
    }

    default PenyediaDistributor checkDistributor(Long distributorId) {
        PenyediaDistributor result = null;
        withProvider(true);
        if (getProvider() != null && !getProvider().distributors.isEmpty()) {
            for (PenyediaDistributor model : getProvider().distributors) {
                if (model.id.equals(distributorId)) {
                    result = model;
                    break;
                }
            }
        }
        return result;
    }

    default void withTotalNegotiations() {
        if (getId() != null) {
            setTotalNegotiations(PaketRiwayatNegosiasi.getNegotiationTotalByPackageId(getId()));
        }
    }

    default void withNegotiationHistory() {
        if (getId() != null) {
            setNegotiationHistory(PaketRiwayatNegosiasi.getNegotiationHistoryByPackageId(getId()));
        }
    }

    default String getAcceptanceDeliveryJSON() {
        if (getAcceptanceHistory() != null) {
            return getAcceptanceHistory().getDeliveryJSON();
        }
        return "{}";
    }

    default void withProducts() {
        if (getId() != null) {
            List<PaketProduk> products = PackageProductRepository.getProductsFront(getId());
            if (!products.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                String glue = "";
                for (PaketProduk model : products) {
                    sb.append(glue).append("'").append(model.getProdukId()).append("'");
                    glue = ",";
                }
                Map<Long, PaketProduk> mapPrice = PackageProductRepository.getBestPrice(sb.toString());
                for (PaketProduk model : products) {
                    PaketProduk paketProduk = mapPrice.get(model.getProdukId());
                    if (paketProduk != null) {
                        model.best_price = paketProduk.best_price;
                        model.conversion_best_price = paketProduk.conversion_best_price;
                    }
                }
                setProducts(products);
            }
        }
    }

    default void withProductsHistory() {
        if (getId() != null) {
            List<PaketProduk> products = PackageProductRepository.getProductsFrontHistory(getId());
            if (!products.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                String glue = "";
                for (PaketProduk model : products) {
                    sb.append(glue).append("'").append(model.getProdukId()).append("'");
                    glue = ",";
                }
                Map<Long, PaketProduk> mapPrice = PackageProductRepository.getBestPriceHistory(sb.toString());
                for (PaketProduk model : products) {
                    PaketProduk paketProduk = mapPrice.get(model.getProdukId());
                    if (paketProduk != null) {
                        model.best_price = paketProduk.best_price;
                        model.conversion_best_price = paketProduk.conversion_best_price;
                    }
                }
                setProducts(products);
            }
        }
    }

    default void withContracts() {
        if (getId() != null) {
            setContracts(PackageContractRepository.getByPackageIdFront(getId()));
        }
    }

    default void withContractValue() {
        if (getId() != null) {
            setContractValue(PackageContractRepository.getContractValue(getId()));
        }
    }

    default void withKurs() {
        if (getKursId() != null) {
            setKurs(Kurs.findById(getKursId()));
        }
    }

    default void withNegoHeader(){
        if(getId() != null){
            setNegoHeader(PaketProdukHeader.find("paket_id = ? order by created_date DESC", getId()).first());
        }
    }

    default void withPaketStatus(){
        if(getId() != null){
            setPaketStatus(PaketStatus.getLatestStatusByPackageId(getId()));
        }
    }

    default void withPenyedia(){
        if(getPenyediaId() != null){
            setPenyedia(Penyedia.findById(getPenyediaId()));
        }
    }

    default void withSumberDana(){
        if(getId() != null){
            setSumberDana(PaketSumberDana.getFundSource(getId()));
        }
    }

    default void withUlpMembers() {
        if (getId() != null) {
            setUlPMembers(PaketAnggotaUlp.getMembersByPackageIdFront(getId()));
        }
    }
}
