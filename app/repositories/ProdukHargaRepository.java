package repositories;

import models.elasticsearch.product.ProductPriceElastic;

import models.elasticsearch.TotalPurchased;
import models.katalog.ProdukWilayahJualKabupaten;
import models.katalog.ProdukWilayahJualProvinsi;
import play.db.jdbc.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 10/2/2017
 */
public class ProdukHargaRepository {

    /**
     * @param productIds reconstructed produk id with "," delimiter
     * @return collection of ProductPriceElastic
     */
    public static Map<Long, List<ProductPriceElastic>> getProductPriceRegency(String productIds) {
        Map<Long, List<ProductPriceElastic>> results = new HashMap<>();
        final String query = "SELECT pk.produk_id" +
                ", pk.kabupaten_id" +
                ", pk.harga_utama" +
                " (CASE WHEN margin_harga > 0 THEN" +
                " ROUND((harga_utama + (harga_utama * margin_harga / 100 )), 2)" +
                " ELSE" +
                " harga_utama" +
                " END) as harga_pemerintah" +
                " FROM `produk_wilayah_jual_kabupaten` pk" +
                ", k.nama_kabupaten as nama_lokasi" +
                " JOIN kabupaten k" +
                " ON k.id = pk.provinsi_id" +
                " WHERE pk.approved = 1 " +
                " AND p.produk_id IN ("+ productIds +")" +
                " AND pk.active = 1" +
                " ORDER BY pk.produk_id ASC";
        List<ProdukWilayahJualKabupaten> priceList = Query.find(query, ProdukWilayahJualKabupaten.class).fetch();
        if (priceList != null && !priceList.isEmpty()) {
            for (ProdukWilayahJualKabupaten model : priceList) {
                ProductPriceElastic price = new ProductPriceElastic(model);
                if (results.containsKey(model.produk_id)) {
                    results.get(price.id).add(price);
                } else {
                    List<ProductPriceElastic> list = new ArrayList<>();
                    list.add(price);
                    results.put(price.id, list);
                }
            }
        }
        return results;
    }

    /**
     * @param productIds reconstructed produk id with "," delimiter
     * @return collection of ProductPriceElastic
     */
    public static Map<Long, List<ProductPriceElastic>> getProductPriceProvince(String productIds) {
        Map<Long, List<ProductPriceElastic>> results = new HashMap<>();
        final String query = "SELECT p.produk_id" +
                ", p.provinsi_id" +
                ", p.harga_utama" +
                " (CASE WHEN margin_harga > 0 THEN" +
                " ROUND((harga_utama + (harga_utama * margin_harga / 100 )), 2)" +
                " ELSE" +
                " harga_utama" +
                " END) as harga_pemerintah" +
                " FROM `produk_wilayah_jual_provinsi` p" +
                ", pr.nama_provinsi as nama_lokasi" +
                " JOIN provinsi pr" +
                " ON pr.id = p.provinsi_id" +
                " WHERE p.active = 1" +
                " AND p.produk_id IN ("+ productIds +")" +
                " ORDER BY p.produk_id ASC";
        List<ProdukWilayahJualProvinsi> priceList = Query.find(query, ProdukWilayahJualProvinsi.class).fetch();
        if (priceList != null && !priceList.isEmpty()) {
            for (ProdukWilayahJualProvinsi model : priceList) {
                ProductPriceElastic price = new ProductPriceElastic(model);
                if (results.containsKey(model.produk_id)) {
                    results.get(price.id).add(price);
                } else {
                    List<ProductPriceElastic> list = new ArrayList<>();
                    list.add(price);
                    results.put(price.id, list);
                }
            }
        }
        return results;
    }

    public static Map<Long, Long> getTotalPurchased(String ids) {
        final String query = "SELECT SUM(pp.kuantitas) as total, pp.produk_id\n" +
                "FROM paket_produk pp\n" +
                "JOIN paket p\n" +
                "ON p.id = pp.paket_id\n" +
                "WHERE p.active = 1 AND pp.active\n AND " +
                "pp.produk_id IN ("+ids+")\n" +
                "GROUP BY pp.produk_id";
        Map<Long, Long> results = new HashMap<>();
        List<TotalPurchased> list = Query.find(query, TotalPurchased.class).fetch();
        if (!list.isEmpty()) {
            for (TotalPurchased model : list) {
                results.put(model.produk_id, model.total);
            }
        }
        return results;
    }

}
