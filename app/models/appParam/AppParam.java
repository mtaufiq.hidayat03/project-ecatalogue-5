package models.appParam;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.Date;

/**
 * Created by dadang on 12/11/17.
 */
@Table(name = "app_param")
public class AppParam extends BaseTable {

	@Id
	public Long id;
	public String param_type;
	public String param_key;
	public String param_text;
	public Date param_date;

	public static final String API_RUP = "api_rup_sync_last_run";
	public static final String API_ADP = "api_adp_sync_last_run";
	public static final String API_PRODUK = "api_produk_sync_last_run_penyedia_id";
	public static final String API_DCE = "api_dce_sync_last_run";

}
