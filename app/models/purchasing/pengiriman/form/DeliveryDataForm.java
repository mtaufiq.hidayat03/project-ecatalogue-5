package models.purchasing.pengiriman.form;

import models.purchasing.base.BaseDataForm;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.util.TextUtils;
import utils.DateTimeUtils;
import utils.KatalogUtils;

import java.util.Date;

/**
 * @author HanusaCloud on 10/19/2017
 */
public class DeliveryDataForm extends BaseDataForm {

    public String docNumber;
    public String docDate;
    public String note;
    public Long packageId;
    public String ongkir;
    public long totalProduct = 0;

    public String getDocNumber() {
        return StringEscapeUtils.escapeSql(docNumber);
    }

    public String generateSystemNumber() {
        return KatalogUtils.generaRandomId(String.valueOf(packageId));
    }

    public Date getDocumentDate() {
        return DateTimeUtils.parseDdMmYyyy(docDate);
    }

    public String getNote() {
        return StringEscapeUtils.escapeSql(note);
    }

    public Double getOngkosKirimDouble() {
        if (!TextUtils.isEmpty(ongkir)) {
            return Double.parseDouble(ongkir.replaceAll("[^0-9.]",""));
        }
        return 0D;
    }

}
