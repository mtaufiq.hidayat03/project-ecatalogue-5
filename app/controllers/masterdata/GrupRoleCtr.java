package controllers.masterdata;

import java.util.*;

import com.google.gson.*;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.jcommon.util.CommonUtil;
import models.katalog.Komoditas;
import models.purchasing.Paket;
import models.secman.Acl;
import models.user.*;
import play.Logger;
import play.i18n.Messages;
import repositories.role.GroupRoleRepository;
import services.grouprole.GroupRoleService;

import java.util.List;
import java.util.stream.Collectors;

public class GrupRoleCtr extends BaseController {

	public static final String TAG = "GrupRoleCtr";

	@AllowAccess({Acl.COM_USER_GRUP_ROLE})
	public static void index() {
		render("masterdata/GrupRoleCtr/index.html");
	}

	@AllowAccess({Acl.COM_USER_GRUP_ROLE_ADD})
	public static void create() {
		int active = 1;
		List<RoleGroupKategori> categories = new GroupRoleRepository().getRolesCategories();
		List<RoleBasic> roleBasicList = RoleBasic.findAllActive();
		List<Komoditas> komoditasList = Komoditas.findAllActive();

		render("masterdata/GrupRoleCtr/create.html", categories, roleBasicList, komoditasList, active);
	}

	@AllowAccess({Acl.COM_USER_GRUP_ROLE_EDIT})
	public static void edit(long id) {
		final int active = 1;
		List<RoleGroupKategori> categories = new GroupRoleRepository().getRolesCategories();
		List<RoleBasic> roleBasicList = RoleBasic.findAllActive();
		List<Komoditas> komoditasList = Komoditas.findAllActive();
		UserRoleGrup userRoleGrup = UserRoleGrup.findById(id);

		Set<Long> roleItemSelected = UserRoleItem.findByUserRoleGrupIdSet(id);
		Set<Long> komoditasSelected = UserRoleKomoditas.findByUserRoleGrupIdMap(id);
		Map<Long, String> paketSelected = UserRolePaket.findByUserRoleGrupIdMap(id);
        List<Long> paketIds = new ArrayList<>(paketSelected.keySet());

        List<UserRolePaket> listPaket = UserRolePaket.findByUserRoleGrupId(id);
		Logger.debug( "results: "+ new Gson().toJson(komoditasSelected));
		render("masterdata/GrupRoleCtr/edit.html", categories, roleBasicList, komoditasList, userRoleGrup, roleItemSelected,
				komoditasSelected, paketIds, listPaket, active);
	}

	@AllowAccess({Acl.COM_USER_GRUP_ROLE_ADD})
	public static void createSubmit(UserRoleGrup userRoleGrup, Long[] role_item_ids, Long[] komoditas_ids, Long[] paket_ids){
		validation.valid(userRoleGrup);
		GroupRoleService service = new GroupRoleService(true);
		int active = validate(userRoleGrup, role_item_ids, komoditas_ids, paket_ids);
		if (validation.hasErrors()){
			setArgs(userRoleGrup, role_item_ids, komoditas_ids, paket_ids);
			render("masterdata/GrupRoleCtr/create.html", active);
		} else {
			userRoleGrup.saveRole();
			service.storeRoles(userRoleGrup, role_item_ids, komoditas_ids, paket_ids);
			flash.success("Data berhasil disimpan");
			edit(userRoleGrup.id);
		}
	}

	@AllowAccess({Acl.COM_USER_GRUP_ROLE_EDIT})
	public static void editSubmit(UserRoleGrup userRoleGrup, Long[] role_item_ids, Long[] komoditas_ids, Long[] paket_ids){

		validation.valid(userRoleGrup);
		GroupRoleService service = new GroupRoleService(false);
		Logger.debug(new Gson().toJson(komoditas_ids));

		int active = validate(userRoleGrup, role_item_ids, komoditas_ids, paket_ids);

		if (validation.hasErrors()) {
			setArgs(userRoleGrup, role_item_ids, komoditas_ids, paket_ids);
			render("masterdata/GrupRoleCtr/edit.html", active);
		} else {
			userRoleGrup.save();
			service.storeRoles(userRoleGrup, role_item_ids, komoditas_ids, paket_ids);
			flash.success("Data berhasil disimpan");
			edit(userRoleGrup.id);
		}
	}

	private static List<Map<String, String>> generateSelectedPackage(Long[] paket_ids) {
		List<Map<String, String>> results = new ArrayList<>();
		if (!CommonUtil.isEmpty(paket_ids)) {
			for (Long id : paket_ids) {
				Paket paket = Paket.findById(id);
				Map<String, String> paketMap = new HashMap<>();
				paketMap.put("id", paket.id.toString());
				paketMap.put("nama_paket", paket.nama_paket);
				results.add(paketMap);
			}
		}
		return results;
	}

	@AllowAccess({Acl.COM_USER_GRUP_ROLE_DELETE})
	public static void delete(long id){
		UserRoleGrup model = new UserRoleGrup().findByIdActive(id, UserRoleGrup.class);
		Logger.debug(CommonUtil.toJson(model));
		notFoundIfNull(model);
		if (model.isAllowedToBeDeleted()) {
			UserRoleItem.deleteByUserRoleGrupId(id);
			UserRoleKomoditas.deleteByUserRoleGrupId(id);
			UserRolePaket.deleteByUserRoleGrupId(id);
			model.softDelete();
			flash.success(Messages.get("notif.success.delete"));
		} else {
			flash.error(Messages.get("notif.failed.delete.relation"));
		}
		index();
	}

	private static Map<Long, String> convertArrToMap(List<UserRoleItem> userRoleItems){

		Map<Long, String> result = userRoleItems.stream().collect(
				Collectors.toMap(x -> x.role_item_id, x -> x.role_item_name));

		return result;
	}

	private static int validate(UserRoleGrup userRoleGrup, Long[] role_item_ids, Long[] komoditas_ids, Long[] paket_ids) {
		if (userRoleGrup.role_basic_id == null) {
			validation.addError("userRoleGrup.role_basic_id", "LKPP Role harus di pilih.", "var");
		}

		if (CommonUtil.isEmpty(role_item_ids)) {
			validation.addError("groupAkses", "<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Anda belum memilih Group Akses, silahkan pilih group akses terlebih dahulu.", "var");
		}

		if (CommonUtil.isEmpty(komoditas_ids) && !userRoleGrup.akses_seluruh_komoditas) {
			validation.addError("komoditas", "<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Anda belum memilih Komoditas, silahkan pilih komoditas terlebih dahulu.", "var");
		}

		if (CommonUtil.isEmpty(paket_ids) && !userRoleGrup.akses_seluruh_paket) {
			validation.addError("paket", "<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Anda belum memilih Paket, silahkan pilih paket terlebih dahulu.", "var");
		}

		if ((CommonUtil.isEmpty(komoditas_ids) && !userRoleGrup.akses_seluruh_komoditas) && userRoleGrup.role_basic_id != null
				&& !CommonUtil.isEmpty(role_item_ids)) {
			return 2;
		}

		if ((CommonUtil.isEmpty(paket_ids) && !userRoleGrup.akses_seluruh_paket) && userRoleGrup.role_basic_id != null
				&& !CommonUtil.isEmpty(role_item_ids)) {
			return 3;
		}

		if ((CommonUtil.isEmpty(paket_ids) && !userRoleGrup.akses_seluruh_komoditas)
				&& (CommonUtil.isEmpty(komoditas_ids) && !userRoleGrup.akses_seluruh_paket)
				&& userRoleGrup.role_basic_id != null && !CommonUtil.isEmpty(role_item_ids)) {
			return 2;
		}
		return 1;
	}

	private static void setArgs(UserRoleGrup userRoleGrup, Long[] role_item_ids, Long[] komoditas_ids, Long[] paket_ids) {
		params.flash();
		flash.error("Gagal Simpan Grup Role, silahkan cek kembali inputan anda.");
		List<RoleGroupKategori> categories = new GroupRoleRepository().getRolesCategories();
		List<RoleBasic> roleBasicList = RoleBasic.findAllActive();
		List<Komoditas> komoditasList = Komoditas.findAllActive();
		Set<Long> roleItemSelected = new HashSet<>();
		Set<Long> komoditasSelected = new HashSet<>();
		List<Map<String, String>> paketSelected = generateSelectedPackage(paket_ids);

		if (!CommonUtil.isEmpty(role_item_ids)) {
			roleItemSelected.addAll(Arrays.asList(role_item_ids));
		}

		if (!CommonUtil.isEmpty(komoditas_ids)) {
			komoditasSelected.addAll(Arrays.asList(komoditas_ids));
		}

		Logger.debug(TAG + " commodity: " + Arrays.toString(komoditas_ids));
		Logger.debug(TAG + " commodity: " + new Gson().toJson(komoditasSelected));
		renderArgs.put("categories", categories);
		renderArgs.put("roleBasicList", roleBasicList);
		renderArgs.put("komoditasList", komoditasList);
		renderArgs.put("userRoleGrup", userRoleGrup);
		renderArgs.put("roleItemSelected", roleItemSelected);
		renderArgs.put("komoditasSelected", komoditasSelected);
		renderArgs.put("paketSelected", paketSelected);
	}

}
