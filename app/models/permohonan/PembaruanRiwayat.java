package models.permohonan;

import models.BaseKatalogTable;
import models.purchasing.PaketProduk;
import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.*;

@Table(name = "permohonan_pembaruan_riwayat")
public class PembaruanRiwayat extends BaseKatalogTable {

    @Id
    public Long id;
    public Long permohonan_pembaruan_id;
    public String deskripsi;
    public String status_permohonan;
    public Integer active = 1;

    @Transient
    public String nama_lengkap;
    @Transient
    public String user_name	;
    @Transient
    public String jabatan;
    @Transient
    public String nama_role	;

    @Transient
    public String tanggal;
    @Transient
    public String tipe_user;
    @Transient
    public String nama_user;
    @Transient
    public String catatan;
    @Transient
    public String status_riwayat;

    public static final String STATUS_PERMOHONAN_DIBUAT = "permohonan_dibuat";
    public static final String STATUS_PERMOHONAN_DIUBAH = "permohonan_diubah";
    public static final String STATUS_PERMOHONAN_PERSETUJUAN = "permohonan_persetujuan";
    public static final String STATUS_DISPOSISI_DIBUAT = "disposisi_dibuat";
    public static final String STATUS_DISPOSISI_DIUBAH = "disposisi_diubah";
    public static final String STATUS_KOORDINASI_DIBUAT = "koordinasi_dibuat";
    public static final String STATUS_KOORDINASI_DIUBAH = "koordinasi_diubah";
    public static final String STATUS_KLARIFIKASI_DIBUAT = "klarifikasi_dibuat";
    public static final String STATUS_KLARIFIKASI_DIUBAH = "klarifikasi_diubah";
    public static final String STATUS_BA_KLARIFIKASI_DIBUAT = "ba_klarifikasi_dibuat";
    public static final String STATUS_BA_KLARIFIKASI_DIUBAH = "ba_klarifikasi_diubah";
    public static final String STATUS_KONTRAK_DIBUAT = "kontrak_dibuat";
    public static final String STATUS_KONTRAK_DIUBAH = "kontrak_diubah";
    public static final String STATUS_VERIFIKASI_PIHAK_KETIGA = "verifikasi_pihak_ketiga";
    public static final String STATUS_KLARIFIKASI = "klarifikasi";
    public static final String STATUS_KLARIFIKASI_ULANG = "klarifikasi_ulang";
    public static final String STATUS_KLARIFIKASI_HASIL = "hasil_klarifikasi";
    public static final String STATUS_HASIL_KLARIFIKASI_DIBUAT = "hasil_klarifikasi_dibuat";
    public static final String STATUS_HASIL_ADENDUM = "adendum_kontrak";
    public static final String STATUS_HASIL_TANPA_ADENDUM = "tanpa_adendum_kontrak";
    public static final String STATUS_HASIL_KLARIFIKASI_DIUBAH = "hasil_klarifikasi_diubah";
    public static final String STATUS_HASIL_KIRIM_KE_KASUBDIT = "hasil_klarifikasi_dikirimkan_ke_kasubdit";
    public static final String STATUS_HASIL_KIRIM_KE_KASI = "hasil_klarifikasi_dikirimkan_ke_kasi";
    public static final String STATUS_HASIL_KIRIM_KE_DIREKTUR = "hasil_klarifikasi_dikirimkan_ke_direktur";
    public static final String STATUS_ADMIN_TERIMA_PROSES = "admin_menerima_proses_permohonan_pembaruan";
    public static final String STATUS_ADMIN_TOLAK_PROSES = "admin_menolak_proses_permohonan_pembaruan";
    public static final String STATUS_DISPOSISI_KIRIM_KE_KASUBDIT = "disposisi_dikirimkan_ke_kasubdit";
    public static final String STATUS_DISPOSISI_KIRIM_KE_KASI = "disposisi_dikirimkan_ke_kasi";
    public static final String STATUS_DISPOSISI_KASI_SETUJU = "kasi_setuju_disposisi";
    public static final String STATUS_DISPOSISI_KASI_TOLAK = "kasi_tolak_disposisi";
    public static final String STATUS_PROSES_ADMIN_SETUJU = "admin_setuju_permohonan";
    public static final String STATUS_PROSES_ADMIN_TOLAK = "admin_tolak_permohonan";
    public static final String STATUS_DATA_KASI_SETUJU = "kasi_setuju_data";
    public static final String STATUS_DATA_KASI_TOLAK = "kasi_tolak_data";
    public static final String STATUS_DATA_ADMIN_SETUJU = "admin_setuju_data";
    public static final String STATUS_DATA_ADMIN_TOLAK = "admin_tolak_data";
    public static final String STATUS_SELESAI = "permohonan_selesai";
    public static final String STATUS_BATAL = "permohonan_dibatalkan";
    public static final String STATUS_PENYEDIA_HADIR = "penyedia_bisa_hadir_klarifikasi";
    public static final String STATUS_PENYEDIA_TIDAK_HADIR = "penyedia_tidak_bisa_hadir_klarifikasi";

    public void set(Long permohonan_pembaruan_id, String message) {
        this.permohonan_pembaruan_id = permohonan_pembaruan_id;
        this.deskripsi = message;
    }

    private void createHistory() {
        this.active = 1;
        save();
    }

    public void addPermohonanPembaruan(Long permId, String message) {
        set(permId, message);
        this.status_permohonan = STATUS_PERMOHONAN_DIBUAT;
        createHistory();
    }

    public void editPermohonanPembaruan(Long permId, String message) {
        set(permId, message);
        this.status_permohonan = STATUS_PERMOHONAN_DIUBAH;
        createHistory();
    }

    public void addPembaruanDispo(Long permId, String message) {
        set(permId, message);
        this.status_permohonan = STATUS_DISPOSISI_DIBUAT;
        createHistory();
    }

    public void editPembaruanDispo(Long permId, String message) {
        set(permId, message);
        this.status_permohonan = STATUS_DISPOSISI_DIUBAH;
        createHistory();
    }

    public void addPembaruanKoor(Long permId, String message) {
        set(permId, message);
        this.status_permohonan = STATUS_KOORDINASI_DIBUAT;
        createHistory();
    }

    public void editPembaruanKoor(Long permId, String message) {
        set(permId, message);
        this.status_permohonan = STATUS_KOORDINASI_DIUBAH;
        createHistory();
    }

    public void addPembaruanKlarif(Long permId, String message) {
        set(permId, message);
        this.status_permohonan = STATUS_KLARIFIKASI_DIBUAT;
        createHistory();
    }

    public void editPembaruanKlarif(Long permId, String message) {
        set(permId, message);
        this.status_permohonan = STATUS_KLARIFIKASI_DIUBAH;
        createHistory();
    }

    public void addPembaruanHasilKlarif(Long permId, String message) {
        set(permId, message);
        this.status_permohonan = STATUS_HASIL_KLARIFIKASI_DIBUAT;
        createHistory();
    }

    public void editPembaruanHasilKlarif(Long permId, String message) {
        set(permId, message);
        this.status_permohonan = STATUS_HASIL_KLARIFIKASI_DIUBAH;
        createHistory();
    }

    public void addBeritaAcaraKlarif(Long permId, String message) {
        set(permId, message);
        this.status_permohonan = STATUS_BA_KLARIFIKASI_DIBUAT;
        createHistory();
    }

    public void editBeritaAcaraKlarif(Long permId, String message) {
        set(permId, message);
        this.status_permohonan = STATUS_BA_KLARIFIKASI_DIUBAH;
        createHistory();
    }

    public void addPembaruanKontrak(Long permId, String message) {
        set(permId, message);
        this.status_permohonan = STATUS_KONTRAK_DIBUAT;
        createHistory();
    }

    public void editPembaruanKontrak(Long permId, String message) {
        set(permId, message);
        this.status_permohonan = STATUS_KONTRAK_DIUBAH;
        createHistory();
    }

    public void addSendToDirektur(Long permId) {
        set(permId, "Penyedia mengirimkan permohonan ke Admin untuk persetujuan.");
        this.status_permohonan = STATUS_PERMOHONAN_PERSETUJUAN;
        createHistory();
    }

    public void addSendToAdminKoordinasi(Long permId) {
        set(permId, "Admin mengirimkan hasil koordinasi dengan pihak ketiga");
        this.status_permohonan = STATUS_KLARIFIKASI;
        createHistory();
    }

    public void addDisposisiSendTo(Long permId, String disposisiTo, String disposisiFrom) {
        set(permId, disposisiFrom + " mengirimkan disposisi ke "+disposisiTo+" untuk ditindaklanjuti.");
        this.status_permohonan = disposisiTo.equalsIgnoreCase(PembaruanStatus.STATUS_KASUBDIT) ? STATUS_DISPOSISI_KIRIM_KE_KASUBDIT : STATUS_DISPOSISI_KIRIM_KE_KASI;
        createHistory();
    }

    public void addDisposisiProses(Long permId, String disposisiTo, String setujuTolak, String alasan) {
//        set(permId, disposisiTo + (setujuTolak.equalsIgnoreCase("setuju_proses") ? " menyetujui ":" menolak ")+"permohonan pembaruan untuk dilakukan disposisi.");
        set(permId, alasan);
        this.status_permohonan = setujuTolak.equalsIgnoreCase("setuju_proses") ? STATUS_PROSES_ADMIN_SETUJU: STATUS_PROSES_ADMIN_TOLAK;
        createHistory();
    }

    public void addDisposisiData(Long permId, String disposisiTo, String setujuTolak, String alasan) {
//        set(permId, disposisiTo + (setujuTolak.equalsIgnoreCase("setuju_data") ? " menyetujui ":" menolak ")+"data permohonan pembaruan.");
        set(permId, alasan);
        this.status_permohonan = setujuTolak.equalsIgnoreCase("setuju_data") ? STATUS_DATA_ADMIN_SETUJU: STATUS_DATA_ADMIN_TOLAK;
        createHistory();
    }

    public void addKlarifikasi(Long permId, String disposisiTo, String setujuTolak, String alasan) {
        set(permId, disposisiTo + (setujuTolak.equalsIgnoreCase(PermohonanPembaruan.STATUS_KLARIFIKASI_TERIMA) ? " menerima ":" menolak ")+"proses permohonan pembaruan data.\nAlasan: "+alasan);
        this.status_permohonan = setujuTolak.equalsIgnoreCase(PermohonanPembaruan.STATUS_KLARIFIKASI_TERIMA) ? STATUS_ADMIN_TERIMA_PROSES: STATUS_ADMIN_TOLAK_PROSES;
        createHistory();
    }

    public void addKlarifikasiKetiga(Long permId, String disposisiTo, String setujuTolak, String alasan) {
//        set(permId, disposisiTo + " memutuskan permohonan ini" + (setujuTolak.equalsIgnoreCase("ya") ? " membutuhkan " : " tidak membutuhkan ") + "verifikasi pihak ketiga.");
        set(permId, alasan);
        this.status_permohonan = setujuTolak.equalsIgnoreCase("ya") ? STATUS_VERIFIKASI_PIHAK_KETIGA : STATUS_KLARIFIKASI;
        createHistory();
    }

    public void addKlarifikasiAdendum(Long permId, String disposisiTo, String setujuTolak, String alasan) {
        set(permId, disposisiTo + " memutuskan permohonan ini" + (setujuTolak.equalsIgnoreCase("ya") ? " membutuhkan " : " tidak membutuhkan ") + "adendum kontrak.\nAlasan: "+alasan);
//        set(permId, alasan);
        this.status_permohonan = setujuTolak.equalsIgnoreCase("ya") ? STATUS_HASIL_ADENDUM : STATUS_HASIL_TANPA_ADENDUM;
        createHistory();
    }

    public void addKlarifikasiUlang(Long permId, String disposisiTo, String setujuTolak, String alasan) {
        set(permId, disposisiTo + " memutuskan permohonan ini" + (setujuTolak.equalsIgnoreCase("ya") ? " membutuhkan " : " tidak membutuhkan ") + "klarifikasi ulang.\nAlasan: "+alasan);
//        set(permId, alasan);
        this.status_permohonan = setujuTolak.equalsIgnoreCase("ya") ? STATUS_KLARIFIKASI_ULANG : STATUS_KLARIFIKASI_HASIL;
        createHistory();
    }

    public void addKlarifikasiHasilKirim(Long permId, String disposisiTo, String disposisiFrom) {
        set(permId, disposisiFrom + " mengirimkan hasil klarifikasi ke "+disposisiTo+" untuk ditindaklanjuti.");
        if (disposisiTo.equalsIgnoreCase(PembaruanStatus.STATUS_KASI)){
            this.status_permohonan = STATUS_HASIL_KIRIM_KE_KASI;
        } else if (disposisiTo.equalsIgnoreCase(PembaruanStatus.STATUS_KASUBDIT)){
            this.status_permohonan = STATUS_HASIL_KIRIM_KE_KASUBDIT;
        } else if (disposisiTo.equalsIgnoreCase(PembaruanStatus.STATUS_DIREKTUR)){
            this.status_permohonan = STATUS_HASIL_KIRIM_KE_DIREKTUR;
        }
        createHistory();
    }

    public void addSelesaikanHasil(Long permId, String eksekutor){
        set(permId, eksekutor +" telah menyelesaikan permohonan pembaruan dari penyedia.");
        this.status_permohonan = STATUS_SELESAI;
        createHistory();
    }

    public void addBatalkanPermohona(Long permId, String eksekutor, String alasan){
        set(permId, eksekutor +" telah membatalkan permohonan pembaruan ini.\nAlasan: "+alasan);
        this.status_permohonan = STATUS_BATAL;
        createHistory();
    }

    public void addBatalkanPermohonanKarnaTidakKlarif(Long permId){
        set(permId, "Paket dibatalkan karena penyedia tidak bisa hadir dalam proses klarifikasi.");
        this.status_permohonan = STATUS_BATAL;
        createHistory();
    }

    public void addKonfirmasiKehadiranKlarif(Long permId, String eksekutor, String setujuTolak){
        String msg = setujuTolak.equalsIgnoreCase(PermohonanPembaruan.STATUS_TOLAK) ? " menolak " : " menyetujui ";
        set(permId, eksekutor +msg+"untuk hadir dalam proses klarifikasi permohonan pembaruan ini.");
        this.status_permohonan = setujuTolak.equalsIgnoreCase(PermohonanPembaruan.STATUS_TOLAK) ? STATUS_PENYEDIA_TIDAK_HADIR : STATUS_PENYEDIA_HADIR;
        createHistory();
    }

    public static PembaruanRiwayat findLatestPembaruanRiwayat(Long idPembaruan){
        return PembaruanRiwayat.find("permohonan_pembaruan_id = ? and active = 1 ORDER BY created_date desc LIMIT 1", idPembaruan).first();
    }

    public static List<PembaruanRiwayat> getDataRiwayatByPermPembId(Long idPembaruan){
        List<PembaruanRiwayat> pr = new ArrayList<>();

        StringBuilder sb = new StringBuilder("select ppr.id, u.nama_lengkap, u.user_name, u.jabatan, " +
                "rb.nama_role, ppr.created_date, ppr.permohonan_pembaruan_id, ppr.deskripsi, ppr.status_permohonan " +
                "from permohonan_pembaruan_riwayat ppr " +
                "LEFT JOIN user u on ppr.created_by = u.id " +
                "LEFT JOIN role_basic rb on u.role_basic_id = rb.id " +
                "where ppr.permohonan_pembaruan_id = ?");
        pr = Query.find(sb.toString(), PembaruanRiwayat.class, idPembaruan).fetch();

        return pr;
    }
}
