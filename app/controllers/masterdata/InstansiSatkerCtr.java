package controllers.masterdata;

import controllers.BaseController;
import controllers.security.AllowAccess;
import jobs.instansisatker.JobInstansiSatker;
import models.jcommon.util.CommonUtil;
import models.masterdata.Instansi;
import models.masterdata.Kldi;
import models.masterdata.Satker;
import models.secman.Acl;
import play.Logger;
import play.i18n.Messages;

public class InstansiSatkerCtr extends BaseController {

	@AllowAccess({Acl.COM_INSTANSI})
	public static void index() {
		render();
	}

	@AllowAccess({Acl.COM_INSTANSI})
	public static void manualFetch() {
		JobInstansiSatker jw = new JobInstansiSatker();
		jw.doJob();
		index();
	}

	@AllowAccess({Acl.COM_INSTANSI_ADD})
	public static void create() {
		render();
	}

	@AllowAccess({Acl.COM_INSTANSI_ADD})
	public static void createSubmit(String[] nama_instansi_arr) {
		// checkAuthenticity();
		// if(nama_instansi_arr != null){
		//
		// //validasi instansi
		// boolean validate = validateInstansi(nama_instansi_arr);
		//
		// if(!validate){
		//
		// if (validation.hasErrors())
		// flash.error("Gagal Simpan Instansi, periksa kembali inputan anda.");
		//
		// List<String> instansiList = Arrays.asList(nama_instansi_arr);
		//
		// render("masterdata/InstansiCtr/create.html", instansiList);
		//
		// }
		//
		//
		// for(int i = 0; i < nama_instansi_arr.length; i++){
		// Instansi sd = new Instansi();
		// sd.nama_instansi = nama_instansi_arr[i];
		// sd.active = true;
		// sd.save();
		// }
		//
		// flash.success("Data berhasil disimpan");
		// index();
		// }

	}

	public static void detail(String id) {
		Kldi kldi = Kldi.find("id like ?",id).first();
		notFoundIfNull(kldi);
		render("masterdata/InstansiSatkerCtr/detail.html", kldi);
	}

	@AllowAccess({Acl.COM_INSTANSI_EDIT})
	public static void editSubmit(long id_instansi, String nama_instansi) {
		checkAuthenticity();
		if (CommonUtil.isEmpty(nama_instansi)) {
			flash.error(
					"Gagal Ubah instansi, nama instansi tidak boleh kosong");
		} else {
			Instansi instansi = Instansi.findById(id_instansi);
			instansi.nama = nama_instansi;
//			instansi.active = true;
			instansi.save();
		}
		flash.success("Data berhasil disimpan");
		index();
	}

	@AllowAccess({Acl.COM_INSTANSI_EDIT})
	public static void editNilaiSubmit(long id, long id_instansi, String nama,
			Double nilai_jual, Double nilai_beli) {
		checkAuthenticity();
		Logger.debug("Instansi ID = %s", id_instansi);
		if (id == 0) {
			Satker satker = new Satker();
			satker.instansi_id = id_instansi;
			satker.nama = nama;
			satker.save();
			flash.success("Data berhasil disimpan");
			//detail(satker.instansi_id);
		} else {
			Satker satker = Satker.findById(id);
			satker.nama = nama;
			satker.save();
			flash.success("Data berhasil disimpan");
			//detail(satker.instansi_id);
		}
	}

	@AllowAccess({Acl.COM_INSTANSI_DELETE})
	public static void delete(long id) {
//		Instansi model = Instansi.findById(id);
//		notFoundIfNull(model);
//		if (!instansiIsInUse(id)) {
//			model.softDelete();
//			flash.success(Messages.get("notif.success.delete"));
//		} else {
//			flash.error(Messages.get("notif.failed.delete.relation"));
//		}
//		index();
	}

	private static boolean instansiIsInUse(long id) {
		return false;
	}

	@AllowAccess({Acl.COM_INSTANSI_EDIT})
	public static void setInstansiAktif(long id) {
		Instansi instansi = Instansi.findById(id);
		notFoundIfNull(instansi);
		instansi.setAktif();
		flash.success(
				Messages.get("record_update_aktifnonaktif_relation_success"));
		index();
	}

	@AllowAccess({Acl.COM_INSTANSI_EDIT})
	public static void setInstansiNonAktif(long id) {
		Instansi instansi = Instansi.findById(id);
		notFoundIfNull(instansi);
		if (!instansiIsInUse(id)) {
			instansi.setNonAktif();
			flash.success(Messages
					.get("record_update_aktifnonaktif_relation_success"));
		} else {
			flash.error(Messages
					.get("record_update_aktifnonaktif_relation_failed"));
		}
		index();
	}
	
	@AllowAccess({Acl.COM_INSTANSI_EDIT})
	public static void setSatkerAktif(long id) {
		Satker satker = Satker.findById(id);
		notFoundIfNull(satker);
		satker.setAktif();
		flash.success(
				Messages.get("record_update_aktifnonaktif_relation_success"));
		index();
	}

	@AllowAccess({Acl.COM_INSTANSI_EDIT})
	public static void setSatkerNonAktif(long id) {
		Satker satker = Satker.findById(id);
		notFoundIfNull(satker);
		if (!instansiIsInUse(id)) {
			satker.setNonAktif();
			flash.success(Messages
					.get("record_update_aktifnonaktif_relation_success"));
		} else {
			flash.error(Messages
					.get("record_update_aktifnonaktif_relation_failed"));
		}
		index();
	}


}
