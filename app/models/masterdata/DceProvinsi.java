package models.masterdata;

import models.BaseKatalogTable;
import play.db.jdbc.BaseTable;
import play.db.jdbc.CacheMerdeka;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by dadang on 8/7/17.
 */

@Table(name = "dce_provinsi")
public class DceProvinsi extends BaseTable {

	@Id
	public Long id;

	public String nama;


	@Transient
	public List<DceKabupaten> kabupatens;


	public Long getId() {return id;}
	public String getNama() {return nama;}

	public static DceProvinsi findByName(String name){
		return find("nama = ?",name).first();
	}

	public static List<DceProvinsi> findByIds(List<String> ids){
		return DceProvinsi.find("id in (" + String.join(",", ids) + ")").fetch();
	}

}