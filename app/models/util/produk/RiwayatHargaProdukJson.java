package models.util.produk;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by reza on 8/6/18.
 */
public class RiwayatHargaProdukJson extends HargaProdukJson {
	public Date getTanggalDibuat(){
		return tanggal_dibuat;
	}
	public Date tanggal_dibuat;
	public String tanggal;
	public String kurs;
	public Long komoditas_harga_atribut_id;
}
