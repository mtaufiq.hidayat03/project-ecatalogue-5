package elastics;

import jobs.elasticsearch.ElasticsearchLog;
import org.junit.Test;
import play.Logger;
import play.test.UnitTest;

public class NaikanProdukElastics extends UnitTest {

    @Test
    public void run(){
        //to execute, into dev server
        Logger.info("naikan produk elastics");
        try {
            ElasticsearchLog elasticsearchLog = new ElasticsearchLog();
            elasticsearchLog.process();
        } catch (Exception e) {
            Logger.info("error test elastic test up produk:"+e.getMessage());
            e.printStackTrace();
        }
    }
}
