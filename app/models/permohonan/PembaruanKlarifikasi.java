package models.permohonan;

import models.BaseKatalogTable;
import models.common.DokumenInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.DateTimeUtils;
import utils.LogUtil;

import javax.persistence.Transient;
import java.io.File;
import java.util.Date;
import java.util.List;

@Table(name = "permohonan_pembaruan_klarifikasi")
public class PembaruanKlarifikasi extends BaseKatalogTable {
    @Id
    public Long id;
    public Long permohonan_pembaruan_id;
    public Date tanggal_kegiatan;
    public Date waktu_kegiatan;
    public String judul_kegiatan;
    public String lokasi;
    public String keterangan;
    public String file_hash;
    public String nama_file;
    public Long  blb_id_content;
    public int active;
    public Date setuju_tolak_tanggal;
    public Long setuju_tolak_oleh;
    public String setuju_tolak;
    @Transient
    public int status_jadwal; //-1 belum expired, 0 expired dan tidak konfirmasi, 1 konfirmasi bisa hadir, 2 konfirmasi tidak hadir

    public static List<PembaruanKlarifikasi> findByPermohonanId(long permohonanId){
        String query = "select klarif.* from permohonan_pembaruan_klarifikasi klarif " +
                "where klarif.active = 1 and klarif.permohonan_pembaruan_id = ? ";
        return Query.find(query, PembaruanKlarifikasi.class, permohonanId).fetch();
    }

    public static PembaruanKlarifikasi findById(long klarifId){
        String query = "select klarif.* from permohonan_pembaruan_klarifikasi klarif " +
                "where klarif.active = 1 and klarif.id = ? ";
        return Query.find(query, PembaruanKlarifikasi.class, klarifId).first();
    }

    public static PembaruanKlarifikasi getLatestJadwalByPermohonanId(long pembId) {
        String query = "select * from permohonan_pembaruan_klarifikasi where permohonan_pembaruan_id = ? and active = 1 ORDER BY id desc limit 1";
        return Query.find(query, PembaruanKlarifikasi.class, pembId).first();
    }

    public boolean klarifikasiSubmit(PembaruanKlarifikasi klarif, File file){
        try {
            BlobTable blob = null;
            if(file != null) {
//                if(file.length() <= 5242880) {
                    blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, this.blb_id_content, PembaruanKlarifikasi.class.getSimpleName());
                    this.nama_file = blob.blb_nama_file;
                    this.file_hash = blob.blb_hash;
                    this.blb_id_content = blob.id;
//                }else{
//                    return false;
//                }
            }
            this.permohonan_pembaruan_id = klarif.permohonan_pembaruan_id;
            this.tanggal_kegiatan = klarif.tanggal_kegiatan;
            this.waktu_kegiatan = klarif.waktu_kegiatan;
            this.judul_kegiatan = klarif.judul_kegiatan;
            this.lokasi = klarif.lokasi;
            this.keterangan = klarif.keterangan;
            this.active = klarif.active;

            Long idr = new Long(this.saveKlarifAndRiwayat());
            if(null != blob){
                blob.blb_id_content = idr;
                blob.save();
            }

            return true;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            LogUtil.d("***************** error ***************", e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public int saveKlarifAndRiwayat(){
        PembaruanRiwayat rw = new PembaruanRiwayat();
        int ret = this.save();
        if(null == this.id){
            rw.addPembaruanKlarif(this.permohonan_pembaruan_id,this.judul_kegiatan);
        }else{
            rw.editPembaruanKlarif(this.permohonan_pembaruan_id,this.judul_kegiatan);
        }
        return ret;
    }

    public String getReadableDate() {
        return DateTimeUtils.parseToReadableDate(tanggal_kegiatan);
    }

    public String getFileUrl() {
        String url = "";

        if(blb_id_content != null && blb_id_content.toString() != ""){
            BlobTable blobTable = BlobTable.findByBlobId(blb_id_content);
            if(blobTable != null){
                DokumenInfo dokumenInfo = DokumenInfo.findDokumenKontrak(blobTable);
                url = dokumenInfo.download_url;
            }
        }

        return url;
    }

    public static int removeDocument(long klarifId){
        String query = "update permohonan_pembaruan_klarifikasi set file_hash = null, nama_file = null, blb_id_content = null where id = ?";
        return Query.update(query,klarifId);
    }
}
