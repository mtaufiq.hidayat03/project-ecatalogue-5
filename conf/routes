# Routes
# This file defines all application routes (Higher priority routes first)
# ~~~~

# Home page
GET     /                                                   HomeCtr.Home
GET     /development                                        PublikCtr.index
GET     /pollingpilihan                                     PublikCtr.pollingPilihan
GET     /hub-kami                                           PublikCtr.hubungiKami
GET     /katalog-lokal-sektoral                             PublikCtr.katalogLokalSektoral
GET     /produk-gambar                                      FileDanGambarCtr.gambar1
GET     /produk-gambar/{th}/{bl}/{tgl}/{nama}               FileDanGambarCtr.gambar1
# Ignore favicon requests
GET     /favicon.ico                                        404

# Map static resources from the /app/public folder to the /public path
GET     /public/                                            staticDir:public

# language
GET    /language/change                                     PublikCtr.changeLang

# Login
GET     /user/login                                         LoginCtr.index
*     /lpse/{key}                                           admin.UserCtr.loginLpse

# Dashboard
GET     /dashboard/penyedia                                 dashboard.DashboardCtr.dashboardPenyedia
GET     /dashboard/ppk                                      dashboard.DashboardCtr.dashboardPpk
GET     /dashboard/pp                                       dashboard.DashboardCtr.dashboardPp
GET     /dashboard/pokja                                    dashboard.DashboardCtr.dashboardPokja

# Kategori Komoditas
GET     /admin/kategori-komoditas                           admin.KategoriKomoditasCtr.index
GET     /admin/kategori-komoditas/create                    admin.KategoriKomoditasCtr.create
GET     /admin/kategori-komoditas/edit/{id}                 admin.KategoriKomoditasCtr.edit
GET     /admin/kategori-komoditas/delete/{id}               admin.KategoriKomoditasCtr.delete


# Komoditas
GET     /admin/komoditas                                            admin.KomoditasCtr.index
POST    /admin/komoditas/generate-kode                              admin.KomoditasCtr.generateKode
POST    /admin/komoditas/validate-name                              admin.KomoditasCtr.validateName
POST    /admin/komoditas/validate-kode                              admin.KomoditasCtr.validateKode
GET     /admin/komoditas/create                                     admin.KomoditasCtr.create
GET     /admin/komoditas/edit/{id}                                  admin.KomoditasCtr.edit
GET     /admin/komoditas/delete/{id}                                admin.KomoditasCtr.delete
GET     /admin/komoditas/setNonAktifKomoditas/{id}                  admin.KomoditasCtr.setNonAktifKomoditas
GET     /admin/komoditas/setAktifKomoditas/{id}                     admin.KomoditasCtr.setAktifKomoditas
GET     /admin/komoditas/detail/{id}                                admin.KomoditasCtr.detail
GET     /admin/komoditas/template-jadwal/{id}/create                admin.KomoditasCtr.createTemplateJadwal
GET     /admin/komoditas/template-jadwal/{id}/edit/{template_id}    admin.KomoditasCtr.editTemplateJadwal
GET     /admin/komoditas/template-jadwal/{id}/delete/{template_id}  admin.KomoditasCtr.deleteTemplateJadwal

GET     /admin/komoditas/{komoditas_id}/atribut-harga               admin.KomoditasAtributHargaCtr.index
GET     /admin/komoditas/{komoditas_id}/atribut-harga/create        admin.KomoditasAtributHargaCtr.create


GET     /katalog/komoditas                                  PublikCtr.allKomoditas
GET     /katalog/produk/perbandingan                        PublikCtr.bandingkanProduk

# Komoditas List by kldi
GET     /katalog/komoditas-kldi/{id}                        PublikCtr.komoditasKldiIndex


# Komoditas Produk Atribut Tipe
GET     /admin/komoditas-atribut/{komoditas_id}             admin.KomoditasProdukAtributTipeCtr.index
GET     /admin/komoditas-atribut/{komoditas_id}/create      admin.KomoditasProdukAtributTipeCtr.create
GET     /admin/komoditas-atribut/{komoditas_id}/edit/{id}   admin.KomoditasProdukAtributTipeCtr.edit
GET     /admin/komoditas-atribut/{komoditas_id}/delete/{id} admin.KomoditasProdukAtributTipeCtr.delete
GET     /admin/komoditas-atribut/{komoditas_id}/hirarki     admin.KomoditasProdukAtributTipeCtr.createPosition

# komoditas by Type
GET     /katalog/komoditas-nasional                         admin.KomoditasCtr.nasional
GET     /katalog/komoditas-lokal                            admin.KomoditasCtr.lokal
GET     /katalog/komoditas-sektoral                         admin.KomoditasCtr.sektoral


# Produk Kategori Atribut
GET     /admin/produk-atribut/{kid}/input/{pkid}            admin.ProdukKategoriAtributCtr.create
POST    /admin/produk-atribut-delete                        admin.ProdukKategoriAtributCtr.delete
GET     /admin/produk-kategori-atribut/hirarki/{kid}/{pkid} admin.ProdukKategoriAtributCtr.createPosisi

# Produk Kategori
GET     /admin/produk-kategori/{id}/tree                    admin.ProdukKategoriCtr.index
GET     /admin/produk-kategori/{kid}/create                 admin.ProdukKategoriCtr.create
GET     /admin/produk-kategori/{kid}/delete/{pkid}          admin.ProdukKategoriCtr.delete
GET     /admin/produk-kategori/{kid}/edit/{pkid}            admin.ProdukKategoriCtr.edit
GET     /admin/produk-kategori/hirarki/{id}                 admin.ProdukKategoriCtr.createPosisi

# User
GET     /admin/user                                         admin.UserCtr.index
GET     /admin/user/create                                  admin.UserCtr.create
GET     /admin/user/edit                                    admin.UserCtr.edit

# User
GET     /admin/user-lokal                                   admin.UserLokalCtr.index
GET     /{language}/admin/user-lokal/create/{komoditas_id}  admin.UserLokalCtr.createUserKomoditas

# Role Item
GET     /admin/role-item                                    admin.RoleItemCtr.index
GET     /admin/role-item/create                             admin.RoleItemCtr.create
GET     /admin/role-item/edit/{id}                          admin.RoleItemCtr.edit

# Usulan
GET     /admin/usulan                                       admin.UsulanCtr.index
GET     /admin/usulan/create/{komoditas_id}                 admin.UsulanCtr.create
GET     /admin/usulan/edit/{id}/{kid}                       admin.UsulanCtr.edit
GET     /admin/usulan/detail/{id}                           admin.UsulanCtr.detail
GET     /admin/usulan/detail-pengumuman/{id}                admin.UsulanCtr.detailPengumuman
GET     /admin/usulan/aanwijzing/{produk_id}                admin.UsulanCtr.aanwijzing
GET     /admin/usulan/aanwijzing/detail/{produk_id}/{id}    admin.UsulanCtr.aanwijzingDetail
GET     /admin/usulan/edit-jadwal/{uId}/{tId}               admin.UsulanCtr.editJadwal
GET     /admin/usulan/riwayat-jadwal/{uId}/{tId}            admin.UsulanCtr.riwayatPerubahanJadwal
GET     /admin/usulan/buat-pengumuman/{id}                  admin.UsulanCtr.bukaPenawaran
GET     /admin/usulan/create-agenda/{usulanId}              admin.UsulanCtr.addAgenda
GET     /admin/usulan/edit-agenda/{agendaId}                admin.UsulanCtr.editAgenda
GET     /admin/usulan/list-pendaftar/{kid}/{id}             admin.UsulanCtr.listPendaftar

GET     /pengumuman                                         PublikCtr.listUsulan

# Produk
GET     /katalog/produk                                     katalog.ProdukCtr.index
GET     /katalog/produk-penyedia                            katalog.ProdukCtr.productListProvider
GET     /katalog/produk/buka-fitur-edit                     katalog.ProductPermissionCtr.productListProvider
GET     /katalog/produk/create/{pid}                        katalog.ProdukCtr.create
GET     /katalog/produk/detail/{id}                         katalog.ProdukCtr.detail
GET     /katalog/produk/detail/{id}/{location_id}           katalog.ProdukCtr.detail
GET     /captcha                                            katalog.ProdukCtr.captcha
# GET   /katalog/produk/all-detail/{id}                     katalog.ProdukCtr.allDetail
GET     /katalog/produk/edit/{produk_id}                    katalog.ProdukCtr.edit
GET     /katalog/produk/edit-produk/{produk_id}             katalog.ProdukCtr.editProduk
GET     /katalog/produk/perbandingan/spek/{id}              katalog.ProdukCtr.spesifikasi
GET     /katalog/produk/perbandingan/harga/{id}             katalog.ProdukCtr.harga
GET     /katalog/produk/permintaan-detail/{id}              katalog.ProdukCtr.lihatPermintaan
POST    /katalog/produk/report/create                       katalog.ProdukCtr.reportSubmit
POST    /katalog/produk/update-status-umkm                  katalog.ProdukCtr.updateStatusUmkm
POST    /katalog/produk/automated-saving                    katalog.ProdukCtr.automatedSaving
# Laporan Produk
GET     /katalog/produk/report                              katalog.LaporanProdukCtr.index
GET     /katalog/produk/report/detail/{id}                  katalog.LaporanProdukCtr.detail

# Kategori Produk
*       /{language}/katalog/produk/{komoditas_slug}/{id}    katalog.ProdukCtr.listProduk

# Search Produk
*       /{language}/search-produk                           katalog.SearchResultCtr.index

# Permohonan Pembaruan
GET     /katalog/produk/permohonan-pembaruan                katalog.PermohonanPembaruanCtr.index
GET     /katalog/produk/permohonan-pembaruan/create         katalog.PermohonanPembaruanCtr.create
GET     /katalog/produk/permohonan-pembaruan/edit/{id}      katalog.PermohonanPembaruanCtr.edit
GET     /katalog/produk/form-permohonan/{id}                katalog.PermohonanPembaruanCtr.formPermohonan
GET     /katalog/produk/detail-permohonan/{id}              katalog.PermohonanPembaruanCtr.infoDetailPenyedia
#GET     /katalog/produk/daftar-disposisi/{id}              katalog.PermohonanPembaruanCtr.listDisposisi
GET     /katalog/produk/disposisi/create                    katalog.PermohonanPembaruanCtr.createDispo
GET     /katalog/produk/disposisi/edit/{id}                 katalog.PermohonanPembaruanCtr.editDispo
GET     /katalog/distributor/permohonan-pembaruan/edit/{id} katalog.PermohonanPembaruanCtr.editDistributor
#GET     /katalog/produk/daftar-klarifikasi/{id}            katalog.PermohonanPembaruanCtr.listKlarifikasi
GET     /katalog/produk/jadwal-klarifikasi/create           katalog.PermohonanPembaruanCtr.createKlarifikasi
GET     /katalog/produk/jadwal-klarifikasi/edit/{id}        katalog.PermohonanPembaruanCtr.editKlarifikasi


# Distributor
GET     /masterdata/distributor                             masterdata.DistributorCtr.index
GET     /masterdata/distributor/create                      masterdata.DistributorCtr.create
GET     /masterdata/distributor/view                        masterdata.DistributorCtr.view

# Group Role
GET     /masterdata/group-role                              masterdata.GrupRoleCtr.index
GET     /masterdata/group-role/create                       masterdata.GrupRoleCtr.create
GET     /masterdata/group-role/edit/{id}                    masterdata.GrupRoleCtr.edit
GET     /masterdata/group-role/delete/{id}                  masterdata.GrupRoleCtr.delete

# Kurs
GET     /masterdata/kurs                                    masterdata.KursCtr.index
GET     /masterdata/kurs/create                             masterdata.KursCtr.create
GET     /masterdata/kurs/delete/{id}                        masterdata.KursCtr.delete

# Sumber Kurs
GET     /masterdata/sumberkurs                              masterdata.SumberKursCtr.index
GET     /masterdata/sumberkurs/create                       masterdata.SumberKursCtr.create
GET     /masterdata/sumberkurs/delete/{id}                  masterdata.SumberKursCtr.delete

# Manufaktur
GET     /masterdata/manufaktur                              masterdata.ManufakturCtr.index
GET     /masterdata/manufaktur/create                       masterdata.ManufakturCtr.create
GET     /masterdata/manufaktur/delete/{id}                  masterdata.ManufakturCtr.delete
GET     /masterdata/manufaktur/importExcel                  masterdata.ManufakturCtr.importExcel

# Banner
GET     /cms/banner                                         cms.BannerCtr.index
GET     /cms/banner/edit                                    cms.BannerCtr.edit
GET     /cms/banner/create                                  cms.BannerCtr.create
GET     /cms/banner/delete                                  cms.BannerCtr.delete
GET     /cms/banner/detail/{id}                             cms.BannerCtr.detail

# Berita
GET     /cms/berita                                         cms.BeritaCtr.index
GET     /cms/berita/edit                                    cms.BeritaCtr.edit
GET     /cms/berita/create                                  cms.BeritaCtr.create
GET     /cms/berita/delete                                  cms.BeritaCtr.delete
GET     /cms/berita/detail/{id}                             cms.BeritaCtr.detail

GET     /berita                                             PublikCtr.listBerita
GET     /berita/baca-berita/{slug}                          PublikCtr.detailBerita
GET     /berita/kategori-berita/{kategori}                  PublikCtr.listBeritaFromKategori
GET     /berita/cari-berita/{keyword}                       PublikCtr.searchingBerita

# Status Penyedia Distributor
GET     /status-penyedia-distributor                        PublikCtr.statusPenyediaDistributor

# Unduh
GET     /cms/unduh                                          cms.UnduhCtr.index
GET     /cms/unduh/edit                                     cms.UnduhCtr.edit
GET     /cms/unduh/create                                   cms.UnduhCtr.create
GET     /cms/unduh/delete                                   cms.UnduhCtr.delete

GET     /unduh                                              PublikCtr.listUnduh
GET     /unduh/kategori-berita/{kategori}                   PublikCtr.listUnduhFromKategori
GET     /unduh/download-file/{id}                           PublikCtr.getFileUnduh
GET     /unduh/cari-unduh/{keyword}                         PublikCtr.searchingUnduh

# FAQ
GET     /cms/faq                                            cms.FaqCtr.index
GET     /cms/faq/create                                     cms.FaqCtr.create
GET     /cms/faq/edit                                       cms.FaqCtr.edit
GET     /cms/faq/delete                                     cms.FaqCtr.delete
GET     /cms/faq/detail                                     cms.FaqCtr.detail

GET     /faq                                                PublikCtr.faq
GET     /faq/kategori-faq/{kategori}                        PublikCtr.kategorifaq
GET     /faq/detail-faq/{slug}                              PublikCtr.detailFaq
GET     /faq/cari-faq/{keyword}                             PublikCtr.searchingFaq

# Kelola Notifikasi
GET     /cms/notifikasi                                     cms.NotifikasiCtr.edit

# Kategori Konten
GET     /cms/kategori-konten                                cms.KategoriKontenCtr.index
GET     /cms/kategori-konten/create                         cms.KategoriKontenCtr.create
GET     /cms/kategori-konten/edit                           cms.KategoriKontenCtr.edit
GET     /cms/kategori-konten/delete                         cms.KategoriKontenCtr.delete

# Kategori Utama
GET     /cms/kategori-utama                                 cms.KategoriKontenCtr.listKategoriUtama
GET     /cms/kategori-utama/create                          cms.KategoriKontenCtr.createKategoriUtama

# Kategori Faq
GET     /cms/kategori-faq                                   cms.KategoriFaqCtr.index
GET     /cms/kategori-faq/create                            cms.KategoriFaqCtr.create
GET     /cms/kategori-faq/edit                              cms.KategoriFaqCtr.edit

# Kategori Konten Statis
GET     /cms/kategori-konten-statis                         cms.KategoriKontenStatisCtr.index
GET     /cms/kategori-konten-statis/create                  cms.KategoriKontenStatisCtr.create
GET     /cms/kategori-konten-statis/edit                    cms.KategoriKontenStatisCtr.edit

# Konten Statis
GET     /cms/konten-statis                                  cms.KontenStatisCtr.index
GET     /cms/konten-statis/create                           cms.KontenStatisCtr.create
GET     /cms/konten-statis/edit                             cms.KontenStatisCtr.edit
GET     /cms/konten-statis/delete                           cms.KontenStatisCtr.delete
GET     /cms/konten-statis/detail                           cms.KontenStatisCtr.detail

# Syarat Ketentuan
GET     /syarat-ketentuan                                   PublikCtr.syaratKetentuan

# History Datafeed
GET     /history-datafeed                                   PublikCtr.historyDatafeed

# Polling
GET     /cms/polling                                        cms.PollingCtr.index
GET     /cms/polling/create                                 cms.PollingCtr.create
GET     /cms/polling/edit                                   cms.PollingCtr.edit
GET     /cms/polling/delete                                 cms.PollingCtr.delete

# Parameter Aplikasi
GET     /masterdata/parameter-aplikasi                      masterdata.ParameterAplikasiCtr.index

# Penyedia
GET     /admin/penyedia-lokal                               admin.PenyediaLokalCtr.index
GET     /{language}/admin/penyedia-lokal/create/{kid}       admin.PenyediaLokalCtr.createPenyediaLokal
GET     /masterdata/penyedia                                masterdata.PenyediaCtr.index
GET     /masterdata/penyedia/create                         masterdata.PenyediaCtr.create
GET     /masterdata/penyedia/edit/{id}                      masterdata.PenyediaCtr.edit
GET     /masterdata/penyedia/detail/{id}                    masterdata.PenyediaCtr.detail

# Penyedia Distributor
GET     /penyedia/distributor                               penyedia.PenyediaDistributorCtr.index
GET     /penyedia/distributor/create/{pid}                  penyedia.PenyediaDistributorCtr.create
GET     /penyedia/distributor/edit/{pid}/{id}               penyedia.PenyediaDistributorCtr.edit
GET     /penyedia/distributor/detail/{pid}/{id}             penyedia.PenyediaDistributorCtr.detail
GET     /penyedia/distributor/delete/{id}                   penyedia.PenyediaDistributorCtr.delete

# Distributor
GET     /penyedia/distributor/{pid}                         penyedia.DistributorCtr.index
GET     /penyedia/distributor/create/{pid}                  penyedia.DistributorCtr.create
GET     /penyedia/distributor/edit/{pid}/{id}               penyedia.DistributorCtr.edit
GET     /penyedia/distributor/detail/{pid}/{id}             penyedia.DistributorCtr.detail
GET     /penyedia/distributor/delete/{id}                   penyedia.DistributorCtr.delete
GET     /info/penyedia/{id}                                 masterdata.PenyediaCtr.infoPenyedia
GET     /info/distributor/{id}                              penyedia.DistributorCtr.infoDistributor

# Sumber Dana
GET     /masterdata/sumber-dana                             masterdata.SumberDanaCtr.index
GET     /masterdata/sumber-dana/create                      masterdata.SumberDanaCtr.create
GET     /masterdata/sumber-dana/delete/{id}                 masterdata.SumberDanaCtr.delete

# Tahapan
GET     /masterdata/tahapan                                 masterdata.TahapanCtr.index
GET     /masterdata/tahapan/create                          masterdata.TahapanCtr.create
GET     /masterdata/tahapan/delete/{id}                     masterdata.TahapanCtr.delete

# Bidang
GET     /masterdata/bidang                                 masterdata.TahapanCtr.index
GET     /masterdata/bidang/create                          masterdata.TahapanCtr.create
GET     /masterdata/bidang/delete/{id}                     masterdata.TahapanCtr.delete

# Unit Pengukuran
GET     /masterdata/unit-pengukuran                         masterdata.UnitPengukuranCtr.index
GET     /masterdata/unit-pengukuran/create                  masterdata.UnitPengukuranCtr.create
GET     /masterdata/unit-pengukuran/delete/{id}             masterdata.UnitPengukuranCtr.delete
GET     /masterdata/unit-pengukuran/editaktif/{id}          masterdata.UnitPengukuranCtr.setNonAktifUnit
GET     /masterdata/unit-pengukuran/editnonaktif/{id}       masterdata.UnitPengukuranCtr.setAktifUnit
GET     /masterdata/unit-pengukuran/importexcel             masterdata.UnitPengukuranCtr.importExcel

# Point Rating
GET     /masterdata/point-rating                            masterdata.PointRatingCtr.index
GET     /masterdata/point-rating/create                     masterdata.PointRatingCtr.create
GET     /masterdata/point-rating/edit/{id}                  masterdata.PointRatingCtr.edit
GET     /masterdata/point-rating/delete/{id}                masterdata.PointRatingCtr.delete

# Kontrak
GET     /penyedia/kontrak/{pid}                                 penyedia.KontrakCtr.index
GET     /penyedia/kontrak/list-kontrak-penawaran/{pid}/{id}     penyedia.KontrakCtr.listKontrakPenawaran
GET     /penyedia/kontrak/create/{pid}/{type}                   penyedia.KontrakCtr.create
GET     /penyedia/kontrak/edit/{id}                             penyedia.KontrakCtr.edit
GET     /penyedia/kontrak/detail                                penyedia.KontrakCtr.detail
GET     /penyedia/kontrak/cetak-kontrak/{pid}/{type}            penyedia.KontrakCtr.cetakKontrak
GET     /penyedia/kontrak/list-dokumen/{pid}/{type}             penyedia.KontrakCtr.listDokumenUpload
GET     /penyedia/kontrak/cetak-penetapan-produk/{pid}          penyedia.KontrakCtr.cetakPenetapanProduk
GET     /penyedia/kontrak/list-lampiran-kontrak/{pid}           penyedia.KontrakCtr.listLampiranKontrak

# Penawaran
GET     /prakatalog/penawaran                               prakatalog.PenawaranCtr.index
GET     /prakatalog/penawaran/penyedia                      prakatalog.PenawaranCtr.penawaranPenyedia
GET     /prakatalog/penawaran/create/{usulan_id}            prakatalog.PenawaranCtr.create
GET     /prakatalog/penawaran/edit/{id}                     prakatalog.PenawaranCtr.edit
GET     /prakatalog/penawaran/detail/{id}                   prakatalog.PenawaranCtr.detail
GET     /prakatalog/penawaran/undangan-penawaran            prakatalog.PenawaranCtr.undanganPenawaran
GET     /prakatalog/penawaran/daftar-produk/{id}            prakatalog.PenawaranCtr.daftarProduk
GET     /prakatalog/penawaran/verification/{id}             prakatalog.PenawaranCtr.verification
GET     /prakatalog/persetujuan/produk                      prakatalog.PersetujuanCtr.persetujuanProduk
GET     /prakatalog/penawaran/cetak-hasil-evaluasi/{id}     prakatalog.PenawaranCtr.cetakHasilEvaluasiForm
GET     /prakatalog/persetujuan/selesai-negosiasi/{id}      prakatalog.PersetujuanCtr.selesaiNegosiasi
GET     /prakatalog/penawaran/berita-acara/{id}             prakatalog.PenawaranCtr.beritaAcara

# Verifikasi Penawaran
GET     /prakatalog/verifikasi-penawaran                    prakatalog.VerifikasiPenawaranCtr.index
GET     /prakatalog/verifikasi-penawaran/detail-penawaran   prakatalog.VerifikasiPenawaranCtr.detailPenawaran

# Perpanjang Penawaran
GET     /prakatalog/perpanjang-penawaran/edit/{id}          prakatalog.PerpanjangPenawaranCtr.edit

# Perpanjang Produk
GET     /katalog/perpanjang-produk/edit/{produk_id}         katalog.PerpanjangProdukCtr.edit

# Dokumen Template
GET     /masterdata/dokumen-template                        masterdata.DokumenTemplateCtr.index
GET     /masterdata/dokumen-template/edit                   masterdata.DokumenTemplateCtr.edit

# Instansi/Satker
GET     /masterdata/instansi                                masterdata.InstansiSatkerCtr.index
GET     /masterdata/instansi/satker/{id}                    masterdata.InstansiSatkerCtr.detail

# Wilayah
GET     /masterdata/provinsi                                masterdata.ProvinsiCtr.index
#GET     /masterdata/provinsi/create                        masterdata.ProvinsiCtr.create
#GET     /masterdata/provinsi/delete/{id}                   masterdata.ProvinsiCtr.delete

# Wilayah Kota/Kab/Prov
GET     /masterdata/wilayah/provinsi                        masterdata.WilayahCtr.index
GET     /masterdata/wilayah/provinsi/kab-kota/{id}          masterdata.WilayahCtr.detail

# Master Data - RUP
GET     /masterdata/rup                                     masterdata.RupCtr.index
POST    /masterdata/rup/tarikrup/{auditdate}/{idrup}        masterdata.RupCtr.getRupByDate
POST    /masterdata/rup/simpanrup/{idrup}                   masterdata.RupCtr.simpanDataRup
GET     /masterdata/rup/detail/{id}                         masterdata.RupCtr.detail

# Master Data - Daftar Hitam
GET     /masterdata/daftar-hitam                            masterdata.BlacklistCtr.index

# Profile
GET     /masterdata/profil-penyedia                         masterdata.ProfilPenyediaCtr.index
GET     /masterdata/penyedia-ukm                            masterdata.ProfilPenyediaCtr.penyediaUkm
GET     /masterdata/profil-satker                           masterdata.ProfilSatkerCtr.index

# Keranjang Belanja
GET     /purchasing/keranjang-belanja                       purchasing.KeranjangBelanjaCtr.index
GET     /purchasing/keranjang-belanja/delete-provider/{id}  purchasing.KeranjangBelanjaCtr.deleteAllProducts

# Paket
GET     /purchasing/paket                                               purchasing.PaketCtr.index
POST    /purchasing/paket/automated-saving                              purchasing.PaketCtr.automatedSaving
GET     /purchasing/paket/fromcart/create                               purchasing.KeranjangBelanjaCtr.buatPaket
GET     /purchasing/paket/create                                        purchasing.PaketCtr.create
*       /{language}/purchasing/paket/detail/{paket_id}                  purchasing.PaketCtr.detail
*       /{language}/purchasing/paket/riwayat/{paket_id}                 purchasing.PaketCtr.riwayatPaket
*       /{language}/purchasing/paket/{paket_id}/daftar-produk           purchasing.PaketCtr.productList
POST    /purchasing/paket/{paket_id}/approval-setujui/{from}/{to}       purchasing.PaketCtr.approve
POST    /purchasing/paket/{paket_id}/approval-tolak/{from}/{to}         purchasing.PaketCtr.reject
*       /{language}/purchasing/paket/{paket_id}/negosiasi               purchasing.PaketCtr.negosiasi
*       /{language}/purchasing/paket/riwayat-negosiasi-produk/{paket_id}       purchasing.PaketCtr.riwayatNegosiasiProduk
GET     /purchasing/paket/{paket_id}/kirim-ppk                          purchasing.PaketCtr.sendToPpk
*       /{language}/purchasing/paket/{paket_id}/tetapkan-distributor    purchasing.PaketCtr.setDistributor
POST    /purchasing/paket/{paket_id}/paket-selesai                      purchasing.PaketCtr.finishingPackage
*       /{language}/purchasing/paket/{paket_id}/daftar-ulp              purchasing.PaketCtr.ulpList
GET     /{language}/purchasing/paket/{paket_id}/batal-paket             purchasing.PaketCtr.cancelPackage

*       /{language}/purchasing/paket/{paket_id}/daftar-kontrak          purchasing.PaketKontrakCtr.contractList
*       /{language}/purchasing/paket/{paket_id}/create-kontrak          purchasing.PaketKontrakCtr.createContract
GET     /{language}/purchasing/paket/{paket_id}/hapus-paket/{id}        purchasing.PaketKontrakCtr.deleteContract
GET     /purchasing/paket/kontrak/download-lampiran/{id}                purchasing.PaketKontrakCtr.downloadContract
GET     /purchasing/paket/kontrak/download-file-kontrak/{id}            purchasing.PaketKontrakCtr.downloadUplContract

*       /{language}/purchasing/paket/{paket_id}/riwayat-negosiasi/{id}         purchasing.PaketCtr.detailRiwayatNegosiasi
*       /{language}/purchasing/paket/{paket_id}/info-penyedia/{id}             purchasing.PaketCtr.infoPenyedia
*       /{language}/purchasing/paket/{paket_id}/info-distributor/{id}          purchasing.PaketCtr.infoDistributor
*       /{language}/purchasing/paket/{paket_id}/penyedia-representatif/{id}    purchasing.PaketCtr.representative
*       /{language}/purchasing/paket/{paket_id}/distributor-representatif/{id} purchasing.PaketCtr.distributorRepresentative

*       /{language}/purchasing/paket/{paket_id}/buat-pengiriman         purchasing.PengirimanCtr.createPengiriman
*       /{language}/purchasing/paket/{paket_id}/riwayat-pengiriman      purchasing.PengirimanCtr.riwayatPengiriman
*       /{language}/purchasing/paket/{paket_id}/riwayat-pengiriman/{id} purchasing.PengirimanCtr.detailRiwayatPengiriman
*       /{language}/purchasing/paket/{paket_id}/hapus-pengiriman/{id}   purchasing.PengirimanCtr.deletePengiriman
*       /{language}/purchasing/paket/{paket_id}/ubah-pengiriman/{id}    purchasing.PengirimanCtr.updatePengiriman
POST    /purchasing/paket/{paket_id}/search-pengiriman                  purchasing.PengirimanCtr.searchDelivery
POST    /purchasing/paket/{paket_id}/get-produk-pengiriman              purchasing.PengirimanCtr.getDeliveryProductsApi
POST    /purchasing/paket/{paket_id}/search-product/pengiriman          purchasing.PengirimanCtr.searchProduct
GET     /purchasing/paket/pengiriman/download-lampiran/{id}             purchasing.PengirimanCtr.downloadLampiran

*       /{language}/purchasing/paket/{paket_id}/buat-penerimaan         purchasing.PenerimaanCtr.createPenerimaan
*       /{language}/purchasing/paket/{paket_id}/riwayat-penerimaan      purchasing.PenerimaanCtr.riwayatPenerimaan
*       /{language}/purchasing/paket/{paket_id}/riwayat-penerimaan/{id} purchasing.PenerimaanCtr.detailRiwayatPenerimaan
GET     /purchasing/paket/{paket_id}/hapus-penerimaan/{id}              purchasing.PenerimaanCtr.deletePenerimaan
*       /{language}/purchasing/paket/{paket_id}/ubah-penerimaan/{id}    purchasing.PenerimaanCtr.updatePenerimaan
GET     /purchasing/paket/penerimaan/download-lampiran/{id}             purchasing.PenerimaanCtr.downloadLampiran
POST    /purchasing/paket/{paket_id}/search-product/penerimaan          purchasing.PenerimaanCtr.searchProduct

*       /{language}/purchasing/paket/{paket_id}/riwayat-pembayaran      purchasing.PembayaranCtr.riwayatPembayaran
*       /{language}/purchasing/paket/{paket_id}/buat-pembayaran         purchasing.PembayaranCtr.createPembayaran
*       /{language}/purchasing/paket/{paket_id}/ubah-pembayaran/{id}    purchasing.PembayaranCtr.updatePembayaran
GET     /purchasing/paket/{paket_id}/hapus-pembayaran/{id}              purchasing.PembayaranCtr.deletePembayaran
POST    /purchasing/paket/{paket_id}/pembayaran/search-pengiriman       purchasing.PembayaranCtr.searchPengiriman
POST    /purchasing/paket/{paket_id}/pembayaran/search-penerimaan       purchasing.PembayaranCtr.searchPenerimaan

*       /{language}/purchasing/paket/{paket_id}/detil-verifikasi-pembayaran/{id}    purchasing.PembayaranCtr.detailVerification
*       /{language}/purchasing/paket/{paket_id}/buat-verifikasi-pembayaran          purchasing.PembayaranCtr.createVerification
GET     /purchasing/paket/{paket_id}/hapus-verifikasi-pembayaran/{id}               purchasing.PembayaranCtr.deleteVerification
*       /{language}/purchasing/paket/{paket_id}/ubah-verifikasi-pembayaran/{id}     purchasing.PembayaranCtr.updateVerification
*       /{language}/purchasing/paket/edit/{paket_id}                                purchasing.PaketCtr.edit

GET     /purchasing/paket/pembayaran/download-lampiran/{id}             purchasing.PembayaranCtr.downloadLampiran

*       /katalog-api                                                    api.KatalogApiCtr.index
GET     /service/rup                                                    api.KatalogApiCtr.formRup


*       /aktivasiCentrum                                                admin.UserCtr.aktivasiCentrum

# others
GET     /test/template-dokumen                                          PublikCtr.bikinHtmlTemplateDokumen

# Chat
GET     /all-message                                                    chat.ChatCtr.allMessage

# Catch all
*       /{controller}/{action}                                          {controller}.{action}
