jQuery.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
});

jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || value == value.match(/^[0-9a-zA-Z.,+_()%&*-:;@/\s]+$/);
});

jQuery.validator.addMethod("emailAddress", function(value, element) {
    return this.optional(element) || value == value.match(/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i);
});

jQuery.validator.addMethod("email", function(value, element) {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
});

jQuery.validator.addMethod("dateformat", function(value, element) {
    return this.optional(element) || value.match(/^(\d{2})-(\d{2})-(\d{4})$/);
});

jQuery.validator.addMethod("price", function(value, element) {
    value = value === undefined ? '' : value.replace(/\D/gi, '');
    return this.optional(element) || value == value.match(/^[0-9]+$/);
});

jQuery.validator.addMethod("phone", function(value, element) {
    value = value.replace( /\(|\)|\s+|-/g, "" );
	return this.optional( element ) || value.length > 9 &&
    value.match( /^(1[ \-\+]{0,3}|\+1[ -\+]{0,3}|\+1|\+)?((\(\+?1-[2-9][0-9]{1,2}\))|(\(\+?[2-8][0-9][0-9]\))|(\(\+?[1-9][0-9]\))|(\(\+?[17]\))|(\([2-9][2-9]\))|([ \-\.]{0,3}[0-9]{2,4}))?([ \-\.][0-9])?([ \-\.]{0,3}[0-9]{2,4}){2,3}$/ );
});
