DROP TABLE IF EXISTS `blob_table`;
CREATE TABLE `blob_table` (
  `blb_id_content` int(11) NOT NULL,
  `blb_versi` int(11) DEFAULT NULL,
  `blb_date_time` datetime DEFAULT NULL,
  `blb_path` varchar(500) DEFAULT NULL,
  `blb_engine` varchar(255) DEFAULT NULL,
  `blb_mirrored` int(11) DEFAULT NULL,
  `blb_ukuran` int(11) DEFAULT NULL,
  `cdn_host_id` int(255) DEFAULT NULL,
  `audittype` varchar(255) DEFAULT NULL,
  `auditupdate` timestamp NULL DEFAULT NULL,
  `audituser` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`blb_id_content`),
  UNIQUE KEY `blob_table_blb_id_content_uindex` (`blb_id_content`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;