package models.common;


import models.jcommon.config.Configuration;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.jcommon.util.CommonUtil;
import models.katalog.Komoditas;
import models.secman.Acl;
import models.secman.UserLoggedIn;
import models.user.RoleBasic;
import models.user.RoleGrup;
import models.user.User;
import models.user.UserRoleKomoditasOverride;
import models.util.Config;
import models.util.RekananDetail;
import org.apache.commons.lang.ArrayUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import play.Logger;
import play.cache.Cache;
import play.libs.WS;
import play.mvc.Scope;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;


/**
 * Created by dadang on 9/11/17.
 */
public class AktifUser implements Serializable {

	public Long user_role_grup_id;

	public Long user_id;

	public String session_id;

	public String nama_lengkap;

	public Long role_basic_id;

	public String centrum_code;

	public List<String> roleItems;

	public boolean akses_seluruh_paket;

	public boolean akses_seluruh_komoditas;

	public List<Long> paket_list;

	public List<Long> komoditas_list;

	public Long rkn_id;

	public String nip;

	public String email;

	public String no_tlp;

	public String jabatan;

	public String no_sert_pbj;

	public String role_basic_name;

	public String kldi_jenis;

	public String kldi_id;

	public boolean is_penyedia;

	public boolean is_panitia_ulp;

	public boolean is_panitia_pp;

	public boolean is_provider_distributor;
	public Long distributor_id;

	public Long penyedia_id;

	public static final String USER_SESSION_ID = "session_key";

	public AktifUser(String session_id, String nama, Long role_basic_id, Long user_id, String centrum_code,
					   boolean akses_seluruh_paket, boolean akses_seluruh_komoditas,
					   List<String> roleItems, List<Long> paketList, List<Long> komoditasList, Long rkn_id,
					 String nip, String email, String no_tlp, String no_sert_pbj, String jabatan,
					 String role_basic_name, Long user_role_grup_id, boolean is_provider_distributor, String kldi_jenis, String kldi_id) {
		super();
		this.session_id = session_id;
		this.nama_lengkap = nama;
		this.role_basic_id = role_basic_id;
		this.user_role_grup_id = user_role_grup_id;
		this.user_id = user_id;
		this.centrum_code = centrum_code;
		this.akses_seluruh_komoditas = akses_seluruh_komoditas;
		this.akses_seluruh_paket = akses_seluruh_paket;
		this.roleItems = roleItems;
		this.paket_list = paketList;
		this.komoditas_list = komoditasList;
		this.rkn_id = rkn_id;
		this.nip = nip;
		this.email = email;
		this.no_tlp = no_tlp;
		this.no_sert_pbj = no_sert_pbj;
		this.role_basic_name = role_basic_name;
		this.jabatan = jabatan;
		this.kldi_jenis = kldi_jenis;
		this.kldi_id = kldi_id;
		this.is_penyedia = (user_role_grup_id.longValue() == RoleGrup.PENYEDIA);
		this.is_panitia_ulp = (user_role_grup_id.longValue() == RoleGrup.PANITIA_ULP);
		this.is_panitia_pp = (user_role_grup_id.longValue() == RoleGrup.PANITIA_PP);
		this.is_provider_distributor = is_provider_distributor;
	}

//	public void save(){
//		Cache.set(session_id, this, "3h");
//	}

	public static void saveToCache(AktifUser obj){
		Cache.set(obj.session_id, obj, "3d");
		Logger.debug("AktifUser set -->> Cache");
	}
	public static AktifUser get(String sessionId){
		AktifUser result = null;
		try{
			result = Cache.get(sessionId, AktifUser.class);
			Logger.debug("AktifUser get --> Cache null"+(result==null));
			return result;
		}catch (Throwable t){
			Logger.error("cant get Aktif user from Cache");
		}
		return result;
	}

	public static void delete(String session_id){

		try{
			Cache.delete(session_id);
		}catch (Throwable t){
			Logger.error("can't delete session from cache");
		}
	}

	public boolean isAuthorized(Acl[] acls){
		try{
			if(ArrayUtils.contains(acls, Acl.ALL_AUTHORIZED_USERS)){
				return true;
			}

			for (Acl a : acls){
				for (String acl : roleItems){
					if(a.namaRole.equals(acl)){
						return true;
					}
				}
			}
		}catch (Exception e){
			Logger.error("error isAuthorized:"+e.getMessage());
		}
		return false;
	}

	public Boolean isAuthorized(String aclCode){
		if(Acl.ALL_AUTHORIZED_USERS.namaRole.equals(aclCode)){
			return true;
		}
		if(this.roleItems.indexOf(aclCode)>= 0){
			return true;
		}
		return false;
	}
	public static AktifUser getAktifUser() {
		try{
			Scope.Session session = Scope.Session.current();
			if(null == session) {
				return null;
			}
			AktifUser aktifUser = AktifUser.get(session.getId());
			if(aktifUser == null){
				// cek session mode
				if(Config.getInstance().aktifUserDb){
					UserLoggedIn userLoggedIn = UserLoggedIn.find("session_id = ? and logoutdate is null", session.getId()).first();
					aktifUser = userLoggedIn != null ? CommonUtil.fromJson(userLoggedIn.payload, AktifUser.class) : null;
					//restore ke sessinya kembali
					AktifUser.saveToCache(aktifUser);
				}else{
					//restore ke sessinya kembali
					aktifUser = UserLoggedIn.getAktifUserFile(session.getId());
				}
			}
			if(aktifUser == null){
				//Scope.Flash.current().error("Sessi habis . . . .");
			}
			return aktifUser;
		}catch (Exception e){
			Logger.info("session cache, aktif user null");
		}
		return null;
	}

	public static Integer getActiveUserId() {
		try{
			AktifUser aktifUser = getAktifUser();
			return aktifUser != null && aktifUser.user_id != null ? aktifUser.user_id.intValue() : -99;
		}catch (Exception e){}
		return -99;
	}

	public static void cekBlacklist(RekananDetail rd){
		try {
			Config config = Config.getInstance();
			String urlAuth = config.blacklist_api_bynpwp + "?arg="+rd.npwp;
			//LogUtil.d(TAG, urlAuth);
			String content = WS.url(urlAuth).timeout("1min").getAsync().get().getString();
			//LogUtil.d(TAG, content);
			Logger.info("cek BLACKLIST:"+content);

			JSONParser parser = new JSONParser();
			JSONObject jsonObject =  (JSONObject) parser.parse(content);

			Boolean isBlacklist = (Boolean) jsonObject.get("status");
			if(isBlacklist){
				String report = Configuration.getConfigurationValue(MailQueue.SMTP_MAIL_ADMIN, "");
				String[] ary = report.split(";");
				for (String to : ary) {
					MailQueue mq = new MailQueue();
					try {
						mq = EmailManager.createEmail(to, "Blacklist rekanan:"+rd.rkn_nama+" rknid:"+rd.rkn_id, "Warning penyedia blacklist");
					} catch (Exception e) {
						Logger.error("error create mail:"+e.getMessage());
					}
					Logger.info(mq.save()+"");
				}
			}

		} catch (InterruptedException e) {
			//LogUtil.d(TAG, "error interrupted exc:"+e.getMessage());
			Logger.info("interupted cek blacklist");
		} catch (ExecutionException e) {
			//LogUtil.d(TAG, "error ExecutionException exc:"+e.getMessage());
			Logger.info("ExecutionException cek blacklist");
		} catch (ParseException e) {
			Logger.info("parse exception cek blacklist");
		}
	}

	public static boolean isUserAllowedToBuy(Komoditas komoditas){
		boolean isAllowedToBuy = getAktifUser() != null && (getAktifUser().isPp() || getAktifUser().isPpk()) && getAktifUser().isAuthorized("comProdukBelibtnBuy");
		if(!isAllowedToBuy || komoditas.apakah_iklan)
			return false;

//		Hardcode untuk komoditas nasional
		if(komoditas.komoditas_kategori_id != 5 && komoditas.komoditas_kategori_id != 6)
			return true;

		long user_id = getAktifUser().user_id;
//		if(getAktifUser().akses_seluruh_komoditas)
//			return true;
		User user = User.findById(user_id);

		if(user.override_role_komoditas)
//			return true;
		{
			List<UserRoleKomoditasOverride> komoditasOverrides = UserRoleKomoditasOverride.findByUserId(user.id);
			if (komoditasOverrides != null && komoditasOverrides.size() > 0) {
				List<Long> komoditasList = komoditasOverrides.stream().
						map(obj -> obj.komoditas_id).
						collect(Collectors.toList());
				if (komoditasList.contains(komoditas.id))
					return true;
			}
		}
		return false;
	}

	public boolean isUlp() {
		return this.role_basic_name.equalsIgnoreCase("ulp") || is_panitia_ulp;
	}

	public boolean isPp() {
		return this.role_basic_name.equalsIgnoreCase("Pejabat Pengadaan") || is_panitia_pp;
	}

	public boolean isPpk() {
		return this.role_basic_name.equalsIgnoreCase("PPK");
	}

	public boolean isProvider() {
		Boolean retVal = this.role_basic_name.equalsIgnoreCase(RoleBasic.PENYEDIA_NAME.toLowerCase()) || this.role_basic_name.equalsIgnoreCase(RoleBasic.PENYEDIA_DISTRIBUTOR_NAME);
		Logger.debug("isProvider()"+retVal);
		return retVal;
	}

	public boolean isDistributor() {
		Boolean retVal=  this.role_basic_name.equalsIgnoreCase(RoleBasic.DISTRIBUTOR_NAME)
				|| this.role_basic_name.equalsIgnoreCase(RoleBasic.PENYEDIA_DISTRIBUTOR_NAME);
		Logger.debug("isDistributor():"+retVal);
		return retVal;
	}

	public boolean allowProviderOrDistributor() {
		Boolean retVal = (isDistributor() || this.is_provider_distributor)
				|| this.role_basic_name.equalsIgnoreCase(RoleBasic.PENYEDIA_DISTRIBUTOR_NAME);
		Logger.debug("allowProviderOrDistributor():"+retVal);
		return retVal;
	}
	
	public boolean isPokja() {
		return this.role_basic_name.equalsIgnoreCase("pokja");
	}

	public boolean isPenyedia() {
		return this.role_basic_name.equalsIgnoreCase("penyedia");
	}

	public boolean isAllowedProviderOrDistributor() {
		return (isProvider() || isDistributor());
	}

	public boolean isAdmin() { return this.role_basic_name.equalsIgnoreCase(RoleBasic.ADMIN_NAME);}

	public boolean isAdminLokalSektoral() { return this.role_basic_name.equalsIgnoreCase(RoleBasic.ADMIN_LOKAL_SEKTORAL_NAME);}

	public User getUser(){
		return User.findById(this.user_id);
	}


}
