package jobs.JobTrail;

import jobs.TrackableJob;
import models.common.AktifUser;
import models.katalog.Produk;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import repositories.elasticsearch.ElasticsearchRepository;

@NoTransaction
public class JobUpdateViewCounter extends TrackableJob {
    Produk produk = new Produk();
    AktifUser aktifUser;
    public JobUpdateViewCounter(Produk _produk, AktifUser _aktifUser){
        produk = _produk;
        aktifUser =_aktifUser;
    }
    public void doJob(){
        if(null != aktifUser) {
            new ElasticsearchRepository().inputViewCounter(produk, aktifUser);
            Logger.info("input view counter");
        }else{
            Logger.info("aktif user kosong pada update view counter");
        }
    }
}
