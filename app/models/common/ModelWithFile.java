package models.common;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.MappedSuperclass;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import models.util.Config;
import play.data.FileUpload;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Query;

 /** Manajemen file dilakukan langsung pada setiap tabel yang menggunakannya; berbeda dengan jcommon yang menyatu 
 * pada class BlobTable
 * 
 * Contoh: ProdukGambar merupakan Model yang memiliki File harus extends dari ModelWithFile
 * 
 * @author Andik Yulianto<andikyulianto@yahoo.com>
 */
@MappedSuperclass
public  class ModelWithFile extends BaseTable {

	//INi di-copy dari BaseKatalogTable karena ternyata kalau extends dari BaseKatalogTable tidak dikenali (BUG)
	public Timestamp created_date;
    public int created_by;
    public Timestamp modified_date;
    public int modified_by;
    public Timestamp deleted_date;
    public int deleted_by;
	
    ///field terkait File
	public String file_name;
	public String original_file_name;
	public Long file_size;
	public String file_sub_location;
	public Integer posisi_file;
	
	/**GUnakan ini untuk menyimpan file hasil upload.
	 * Setelah ini dipanggil 
	 * 1. field-field dari FileDanGambar terisi, namun belum disimpan di database.
	 * 2. File sudah di-copy ke FILE_STORAGE
	 * @param file
	 * @return
	 * @throws IOException 
	 */
	public ModelWithFile saveFile(FileUpload file) throws IOException
	{
		return saveFile(file, 1);
	}
	public ModelWithFile saveFile(FileUpload file, Integer posisi_file) throws IOException
	{
		String root=Config.getInstance().fileStorageDir;
		original_file_name= file.getFileName();
		String ext=FilenameUtils.getExtension(original_file_name);
		file_name=System.currentTimeMillis() + "." + ext;
		file_size=file.getSize();
		file_sub_location=String.format("%1$tY/%1$tm/%1$td", new Date());
		//Copy to fileStorage AND delete original file
		String fullFilePath=String.format("%s/%s/%s", root, file_sub_location, file_name);
		File fileTarget=new File(fullFilePath);
		fileTarget.getParentFile().mkdirs();
		this.posisi_file=posisi_file;
		File fileSrc=file.asFile();
		FileUtils.copyFile(fileSrc, fileTarget);
		fileSrc.delete();
		return this;
	}
}
