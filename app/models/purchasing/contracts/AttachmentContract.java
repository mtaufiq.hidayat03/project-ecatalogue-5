package models.purchasing.contracts;

import com.google.gson.Gson;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.config.Configuration;
import models.util.Config;
import org.apache.commons.io.FilenameUtils;
import play.Logger;
import utils.UrlUtil;

import java.io.File;

/**
 * @author HanusaCloud on 10/31/2017
 */
public interface AttachmentContract<T> {

    T setFile(File file);

    T position(int position);

    String getDownloadUrl();

    Long getId();

    Long getPackageId();

    String getFileName();

    String getSubLocation();

    String getOriginalFileName();

    Integer getCreatedBy();

    Long getModelId();

    Long getBlobId();

    void saveAttachment();

    void setBlobId(Long blobId);

    default File getFile() {
        File dir = getDirectory();
        return new File(dir.getAbsolutePath() + "/" + getFileName());
    }

    default File renameTo(File file) {
        final File newFile = getFile();
        if (file.renameTo(newFile)) {
            Logger.debug("rename file");
            blobbing(newFile);
        }
        return newFile;
    }

    default File getDirectory() {
//        Config config = Config.getInstance();
//        File dir = new File( config.fileStorageDir + config.pengirimanLampiranDir + "/" + getSubLocation());
        String storage = BlobTable.getFileStorageDir();
        File dir = new File(storage);
        if (!dir.exists() && dir.mkdirs()) {
            Logger.debug("dir not exists, creating..");
        }
        return dir;
    }

    default String regenerateFileName() {
        return (getCreatedBy() != null ? String.valueOf(getCreatedBy()) : "" ) + System.currentTimeMillis() + "." + getExtension();
    }

    default String getExtension() {
        return FilenameUtils.getExtension(this.getOriginalFileName());
    }

    default String generateDownloadUrl(String path) {
        return UrlUtil.builder()
                .setUrl(path)
                .setParam("id", getId())
                .build();
    }

    default void blobbing(File file) {
        if (file != null && file.exists()) {
            try {
                BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, getId(), AttachmentContract.class.getSimpleName());
                Logger.debug("AttachmentContract class:"+new Gson().toJson(blob));
                setBlobId(blob.id);
                //setBlobId(blob.blb_id_content);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    default BlobTable withBlobContent() {
        if (getBlobId() != null) {
            Logger.info(getBlobId().toString()+" blobid");
            BlobTable blobTable = BlobTable.find("id =?", getBlobId()).first();
            blobTable.postLoad();
            return blobTable;
        }
        return null;
    }

     default BlobTable withBlobId() {
        if (getBlobId() != null) {
            Logger.info(getBlobId().toString()+" blobid");
            BlobTable blobTable = BlobTable.find("id =?", getBlobId()).first();
            blobTable.postLoad();
            return blobTable;
        }
        return null;
    }
}
