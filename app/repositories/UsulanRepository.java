package repositories;

import models.common.AktifUser;
import models.prakatalog.SearchUsulan;
import models.prakatalog.Usulan;
import models.prakatalog.UsulanSearchParams;
import models.prakatalog.search.UsulanSearch;
import models.prakatalog.search.UsulanSearchResult;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.util.TextUtils;
import play.cache.CacheFor;
import play.db.jdbc.Query;
import utils.LogUtil;

import java.util.List;

/**
 * @author HanusaCloud on 12/18/2017
 */
public class UsulanRepository {

    public SearchUsulan searchUsulan(UsulanSearchParams params) {
        SearchUsulan model = new SearchUsulan(params);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT u.id, u.judul_pengumuman, s.started_date, s.finished_date, km.nama_komoditas as komoditas, kk.nama_kategori as komoditas_kategori  ") //p.total_penawaran
                .append("FROM usulan u ")
                .append("JOIN komoditas km ")
                .append("ON km.id = u.komoditas_id ")
                .append("JOIN komoditas_kategori kk on km.komoditas_kategori_id = kk.id ")
                .append("LEFT JOIN (SELECT usulan_id, MIN(tanggal_mulai) as started_date, MAX(tanggal_selesai) as finished_date ")
                .append("FROM usulan_jadwal GROUP BY usulan_id) s ")
                .append("ON s.usulan_id = u.id ");
//                .append("LEFT JOIN (SELECT usulan_id, COUNT(*) as total_penawaran FROM penawaran WHERE active = 1 GROUP BY usulan_id) p ")
//                .append("ON p.usulan_id = u.id");
        sb.append(" WHERE u.active = 1")
                .append(" AND u.status = '").append(Usulan.STATUS_PENAWARAN_DIBUKA).append("'");

        if (!TextUtils.isEmpty(params.getKeyword())) {
            sb.append(" AND (nama_usulan LIKE '%").append(params.getKeyword()).append("%'")
                    .append(" OR pengusul_nama LIKE '%").append(params.getKeyword()).append("%')");
        }
        final String ids = params.getIds();
        if (!TextUtils.isEmpty(ids)) {
            sb.append(" AND u.komoditas_id IN (").append(ids).append(")");
        }

        if (!TextUtils.isEmpty(params.startDate)) {
            sb.append(" AND s.started_date >= '").append(params.parseStartDate()).append("'");
        }

        if (!TextUtils.isEmpty(params.finishDate)) {
            sb.append(" AND s.finished_date <= '").append(params.parseFinishDate()).append("'");
        }

       /* if (!TextUtils.isEmpty(params.startDate)) {
            sb.append(" ORDER BY s.started_date ASC");
        } else if (!TextUtils.isEmpty(params.finishDate)) {
            sb.append(" ORDER BY s.finished_date DESC");
        } else {
            sb.append(" ORDER BY u.modified_date DESC");
        }*/

        sb.append(" ORDER BY u.modified_date DESC");

        model.generateTotal(sb.toString());
        LogUtil.d("usulan", "total all: " + model.getTotal());
        if (!model.isEmpty()) {
            sb.append(" LIMIT ").append(model.getMax()).append(" OFFSET ").append(model.getFrom());
            LogUtil.d("usulan", sb.toString());
            model.items = Query.find(sb.toString(), Usulan.class).fetch();
        }
        return model;
    }

    @CacheFor("5mn")
    public static   List<Usulan> homePage(){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT u.id, u.judul_pengumuman, s.started_date, s.finished_date, km.nama_komoditas as komoditas, p.total_penawaran, kk.nama_kategori as komoditas_kategori ")
                .append("FROM usulan u ")
                .append("JOIN komoditas km ")
                .append("ON km.id = u.komoditas_id ")
                .append("JOIN komoditas_kategori kk on km.komoditas_kategori_id = kk.id ")
                .append("LEFT JOIN (SELECT usulan_id, MIN(tanggal_mulai) as started_date, MAX(tanggal_selesai) as finished_date ")
                .append("FROM usulan_jadwal GROUP BY usulan_id) s ")
                .append("ON s.usulan_id = u.id ")
                .append("LEFT JOIN (SELECT usulan_id, COUNT(*) as total_penawaran FROM penawaran WHERE active = 1 GROUP BY usulan_id) p ")
                .append("ON p.usulan_id = u.id");
        sb.append(" WHERE u.active = 1")
                .append(" AND u.status = '").append(Usulan.STATUS_PENAWARAN_DIBUKA).append("'");
        sb.append(" ORDER BY u.modified_date DESC").append(" LIMIT 8 ");

        return Query.find(sb.toString(), Usulan.class ).fetch();
    }

    public static UsulanSearchResult searchDataUsulan(UsulanSearch search) {
        UsulanSearchResult model = new UsulanSearchResult(search);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT DISTINCT u.id " +
                ", u.komoditas_id " +
                ", u.nama_usulan " +
                ", u.status " +
                ", u.pengusul_nama " +
                ", kldi.nama kldi " +
                ", k.nama_komoditas " +
                ", u.doc_review_id ")
                .append("FROM usulan u ")
                .append("JOIN kldi on u.kldi_id = kldi.id ")
                .append("JOIN komoditas k on u.komoditas_id = k.id and k.active = 1 ")
                .append("LEFT JOIN usulan_pokja up on up.usulan_id = u.id and up.active = 1 ");

        sb.append(" WHERE u.active = 1 ");
        if (!TextUtils.isEmpty(search.keyword)) {
            sb.append(" AND (u.nama_usulan LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%'");
            sb.append(" OR kldi.nama LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%')");
        }

        if (search.uid > 0) {
            if(AktifUser.getAktifUser().isPokja()){
                sb.append(" AND up.pokja_id = ").append(search.uid);
            }else if(AktifUser.getAktifUser().isAdminLokalSektoral()){
                sb.append(" AND u.kldi_id = '").append(AktifUser.getAktifUser().kldi_id).append("' ");
            }
        }

        if (search.komoditas > 0) {
            sb.append(" AND u.komoditas_id = ").append(search.komoditas);
        }

        if(!TextUtils.isEmpty(search.status)){
            sb.append(" AND u.status = '").append(search.status).append("'");
        }

        Long total = UsulanRepository.generateTotal(sb.toString());
        model.setTotal(total);

        if(!TextUtils.isEmpty(search.urutkan)){
            if(search.urutkan.equalsIgnoreCase("1")){
                sb.append(" ORDER BY k.nama_komoditas ASC ");
            }else if(search.urutkan.equalsIgnoreCase("2")){
                sb.append(" ORDER BY k.nama_komoditas DESC ");
            }else if(search.urutkan.equalsIgnoreCase("3")){
                sb.append(" ORDER BY u.nama_usulan ASC ");
            }else if(search.urutkan.equalsIgnoreCase("4")){
                sb.append(" ORDER BY u.nama_usulan DESC ");
            }else if(search.urutkan.equalsIgnoreCase("5")){
                sb.append(" ORDER BY kldi.nama ASC ");
            }else if(search.urutkan.equalsIgnoreCase("6")){
                sb.append(" ORDER BY kldi.nama DESC ");
            }else if(search.urutkan.equalsIgnoreCase("7")){
                sb.append(" ORDER BY u.created_date ASC ");
            }else if(search.urutkan.equalsIgnoreCase("8")){
                sb.append(" ORDER BY u.created_date DESC ");
            }
        }

        if (!model.isEmpty()) {
            sb.append(" LIMIT ").append(search.per_page).append(" OFFSET ").append(model.getFrom());
            LogUtil.debug("TEST", sb.toString());
            model.items = Query.find(sb.toString(), Usulan.class).fetch();
        }
        return model;
    }

    private static Long generateTotal(String query) {
        Long total = Query.find("SELECT COUNT(id) FROM (" + query + ") p", Long.class).first();
        return total;
    }

}
