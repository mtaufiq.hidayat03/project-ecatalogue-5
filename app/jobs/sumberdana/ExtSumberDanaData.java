package jobs.sumberdana;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import jobs.TrackableJob;
import models.api.DceSatker;
import models.api.DceSumberDana;
import models.jcommon.config.Configuration;
import models.jcommon.http.SimpleHttpRequest;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.masterdata.SumberDana;
import models.util.Config;
import play.Logger;
import play.jobs.Job;
import play.libs.URLs;
import utils.KatalogUtils;

public class ExtSumberDanaData {
	private Properties propJob;
	public static final String DEFAULT_WAIT_TIME = "job.sirup.waittime";
	public static final String DEFAULT_WAIT_TIME_NEXT = "job.sirup.waittime.next";
	private String jsResult = "[]";
	private Boolean resultGet = false;
	public List<DceSumberDana> instansiList = new ArrayList<>();
	private int defaultWaitTime = 0;
	private String sumberDanaUrl = Config.getInstance().sumber_dana_api;
	private int nextWaitTime = 0;
	private int delayTaskJob = 5;// second
	private int delayJob = 10;// second
	//
	public ExtSumberDanaData() {
		propJob = new Properties();
		propJob.put(DEFAULT_WAIT_TIME,
				Configuration.getConfigurationValue(DEFAULT_WAIT_TIME, ""));
		propJob.put(DEFAULT_WAIT_TIME_NEXT, Configuration
				.getConfigurationValue(DEFAULT_WAIT_TIME_NEXT, ""));

		defaultWaitTime = Integer
				.parseInt(propJob.getProperty(DEFAULT_WAIT_TIME));
		nextWaitTime = Integer
				.parseInt(propJob.getProperty(DEFAULT_WAIT_TIME_NEXT));

	}

	public List<DceSumberDana> getSumberDanaData() {
		return getSumberDanaData(defaultWaitTime);
	}
	public List<DceSumberDana> getSumberDanaData(int minuteTimeOut) {
		if (fetchSumberDanaData(minuteTimeOut)) {
			this.instansiList = parseSumberDanaData();
		}
		return this.instansiList;
	}

	private Boolean fetchSumberDanaData(Integer minuteTimeout) {
		resultGet = false;
		jsResult = "[]";
		Date tgl_update = new Date();
		String date = KatalogUtils.toDateyyyyMMdd(tgl_update);
		Integer seconds = 0;
		if (minuteTimeout > 0) {
			seconds = (minuteTimeout * 60);
		}
		Logger.info("run with minuteTimeout: " + minuteTimeout + ", and date "
				+ tgl_update.toLocaleString());
		try (SimpleHttpRequest connect = new SimpleHttpRequest(seconds)) {
			String params = "";
			connect.get(sumberDanaUrl + params);
			jsResult = connect.getString();
			resultGet = true;
		} catch (InterruptedException e) {
			Logger.error("Interupted Exception:" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Logger.error("IO Exception:" + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		if (resultGet == false) {
			defaultWaitTime++;
			nextWaitTime = defaultWaitTime;
			// Configuration.updateConfigurationValue(DEFAULT_WAIT_TIME_NEXT,
			// String.valueOf(newWait));
			// propJob.replace(DEFAULT_WAIT_TIME_NEXT, String.valueOf(newWait));

			// newJob(tgl_update);
		}
		return resultGet;
	}

	private List<DceSumberDana> parseSumberDanaData() {
		try {
			instansiList = new Gson().fromJson(jsResult,
					new TypeToken<List<DceSumberDana>>() {
					}.getType());
			Logger.info("total:" + instansiList.size());
		} catch (Exception e) {
			Logger.error("error parse datasisumberDana:" + e.getMessage());
			e.printStackTrace();
		}
		return instansiList;
	}

	public void saveDataSumberDanaToDb() {
		List<SumberDana> failed = new ArrayList<>();
		List<SumberDana> success = new ArrayList<>();
		Logger.info("save data sumberDana:");
		for (DceSumberDana s : instansiList) {
			SumberDana sumberDanaObj = SumberDana.findById(s.id);
			try {
				if (sumberDanaObj != null) {
					sumberDanaObj.nama_sumber_dana = s.nama;

					int result = sumberDanaObj.save();
					Logger.info("update:" + result);
				} else {
					sumberDanaObj = s.getAsSumberDana();
					int result = sumberDanaObj.save();
					Logger.info("save:" + result);
				}
				if (sumberDanaObj != null) {
					success.add(sumberDanaObj);
				}
			} catch (Exception e) {
				if (sumberDanaObj != null) {
					failed.add(sumberDanaObj);
				}
				Logger.info("error save or update sumberDana:" + e.getMessage());
				e.printStackTrace();
			}
		}

	}

//	private void newJob() {
//		new Job() {
//			String uniqueID = UUID.randomUUID().toString();
//			@Override
//			public void before() {
//				Logger.info("job id:" + uniqueID);
//				super.before();
//			}
//
//			public void doJob() {
//				jalankan();
//			}
//		}.in(delayJob);
//	}

	public void jalankan() {
		Logger.info("on job execute, waittime:" + nextWaitTime);
		getSumberDanaData(nextWaitTime);
		Logger.info("%%%%done getDate, saving now%%%");
		saveDataSumberDanaToDb();
		Logger.info("done job:" + instansiList.size());
	}
}
