package export;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;
import org.junit.Test;

import play.Logger;
import play.test.UnitTest;

public class MysqlGrepTest extends UnitTest {

	//@Test
	public void doTest() throws FileNotFoundException, IOException
	{
		File fileSource=new File("test/data/lkpp_katalog_17082016.sql.gz");
		
		InputStream is=new GZIPInputStream(new BufferedInputStream(new FileInputStream(fileSource)));
		
		int i=0;
		Pattern p=Pattern.compile("INSERT INTO `(\\w+)`.*");
		Set<String> excludes=new HashSet<>();
		excludes.add("cisessionsportalkatalog");
		excludes.add("api_log_adp");
		excludes.add("ex_dapodik_detail_paket");
		excludes.add("event_log");
		String folderOut="test/data/FilePerTabel";
		StopWatch sw=new StopWatch();
		sw.start();
		Map<String, PrintWriter> writers=new HashMap<>();
		//DDL
		PrintWriter writerPre=new PrintWriter(new FileOutputStream(folderOut + "/000-ddl.sql"));
		writers.put("PRE", writerPre);
		PrintWriter writerPost=new PrintWriter(new FileOutputStream(folderOut + "/999-ddl.sql"));
		writers.put("POST", writerPre);
		long byteRead=0l;
		List<String> tables=new ArrayList<>();
		boolean insertFound=false;
		try(LineNumberReader reader=new LineNumberReader(new InputStreamReader(is));
			)
		{
			while(true)
			{
				String st=reader.readLine();
				if(st==null)
					break;
				byteRead+=st.length()+2;

				String stHeader=StringUtils.left(st, 100);
					//jika insert into maka lakukan filter
				Matcher m=p.matcher(stHeader);
				if(m.matches())
				{
					insertFound=true;
					String tableName=m.group(1);
					if(!tables.contains(tableName))
						tables.add(tableName);
					int tableIndex=tables.indexOf(tableName);
					PrintWriter writer=writers.get(tableName);
					if(writer==null)
					{
						 writer=new PrintWriter(new FileOutputStream(String.format("%s/%03d-%s.sql", folderOut, tableIndex, tableName)));
						 writers.put(tableName, writer);
					}
					writer.println(st);
				}
				else
					if(insertFound)
						writers.get("POST").println(st);
					else
						writers.get("PRE").println(st);

				i++;
				if(i%200==0)
				{
					Logger.debug("[ROW] #%,9d, bytes: %,15d", i, byteRead);
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		//close all writer
		for(PrintWriter w: writers.values())
			w.close();
		sw.stop();
		Logger.debug("[DONE], dur: %s", sw);

		
	}
}
