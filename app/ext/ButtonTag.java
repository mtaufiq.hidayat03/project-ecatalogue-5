package ext;

import groovy.lang.Closure;
import org.apache.commons.lang.StringUtils;
import play.i18n.Messages;
import play.mvc.Http;
import play.mvc.Router;
import play.mvc.Router.ActionDefinition;
import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;

import java.io.PrintWriter;
import java.util.Map;

public class ButtonTag extends FastTags {
	
	/**
	 * tag saveButton digunakan untuk aksi simpan model
	 * default label='simpan'
	 * boleh diberi id
	 **/
	public static void _saveButton(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		String label = (String)args.get("title");
		if(StringUtils.isEmpty(label)) {
			label = Messages.get("simpan.label");
		}
		String name = (String)args.get("name");
		if(StringUtils.isEmpty(name)) {
			name = "simpan";
		}
		String id = (String)args.get("id");
		if(!StringUtils.isEmpty(id))
			id=" id=\"" + id + "\" ";
		else id="";
		String confirm = (String)args.get("confirm");
		if(!StringUtils.isEmpty(confirm)) {
			confirm = "onClick=\"return confirm('"+confirm+"');\"";
		}
		out.print("<button type=\"submit\" name=\""+name+"\" class=\"btn btn-md btn-primary\""+id+' '+confirm+"> <i class=\"fa fa-save\"></i> "+label+"</button>");
	}
	
	/**
	 * tag addButton digunakan untuk masuk ke halaman create data
	 * default label = 'Tambah'
	 */
	public static void _addButton(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		ActionDefinition actionDef = (ActionDefinition) args.get("action");
		String label = (String)args.get("title");
		if(StringUtils.isEmpty(label)) {
			label = Messages.get("tambah.label");
		}
		out.print("<a class=\"btn btn-primary btn-md\" href=\""+actionDef.url+"\""+serialize(args, "action")+" ><i class=\"fa fa-plus\"></i> "+label+"</a>");
	}
	
	/**
	 * tag deleteButton digunakan untuk menghapus data 
	 * dengan menampilkan confirmation popup terlebih dahulu
	 * default label = 'Hapus'
	 **/
	public static void _deleteButton(Map<?,?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		ActionDefinition actionDef = (ActionDefinition) args.get("action");
		String label = (String)args.get("title");
		if (StringUtils.isEmpty(label)) {
			label = Messages.get("hapus.label");
		}
		String name = (String)args.get("name");
		if(StringUtils.isEmpty(name)) {
			name = "hapus";
		}
		String disabled = (String)args.get("disabled");
		String eventMsg = "onclick=\"return confirm('"+Messages.get("notif.delete_button.confirm")+"');\"";
		if(actionDef == null)
			out.print("<button class=\"btn btn-danger btn-md\" name=\""+name+"\" type=\"submit\" "+eventMsg+" "+disabled+"><i class=\"fa fa-trash\"></i> "+label+"</button>");
		else
			out.print("<a class=\"btn btn-danger btn-md\" href=\""+actionDef.url+"\""+serialize(args, "action") + ' ' +eventMsg+" "+disabled+"><i class=\"fa fa-trash\"></i> "+label+"</a>");
	}
	
	/**
	 * tag backButton digunakan untuk kembali ke page sebelumnya
	 * default label='kembali'
	 **/
	public static void _backButton(Map<?,?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		ActionDefinition actionDef = (ActionDefinition) args.get("action");
		String label = (String)args.get("title");
		if(StringUtils.isEmpty(label)) {
			label = Messages.get("kembali.label");
		}
		String url = Router.reverse("PublikCtr.home").url;
		if(actionDef == null) {
			try{
				url = Http.Request.current().headers.get("referer").value();
			}catch (Exception e){

			}
			out.println("<a class=\"btn btn-default btn-md\" href=\""+url+"\"><i class=\"fa fa-arrow-circle-left\"></i> "+label+"</a>&nbsp;");
		}else{
			out.print("<a class=\"btn btn-default btn-md\" href=\""+actionDef.url+"\""+serialize(args, "href")+"><i class=\"fa fa-arrow-circle-left\"></i> "+label+"</a>&nbsp;");
		}

	}
	
	/**
	 * tag link digunakan untuk link yang berbentuk tombol
	 * dengan icon yang bisa disesuaikan
	 * default label='Klik'
	 **/
	public static void _link(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		ActionDefinition actionDef = (ActionDefinition) args.get("action");
		String label = (String)args.get("title");
		if(StringUtils.isEmpty(label)) {
			label = Messages.get("klik.label");
		}
		String cssClass = (String)args.get("cssClass");
		if(StringUtils.isEmpty(cssClass)) {
			cssClass = "btn btn-primary btn-md";
		}
		String iconClass = (String)args.get("iconClass");
		if(StringUtils.isEmpty(iconClass)) {
			iconClass = "";
		}
		String newTab = (String)args.get("newTab");
		String target = "";
		if(!StringUtils.isEmpty(newTab)) {
			if(Boolean.parseBoolean(newTab)) {
				target = " target='_blank' ";
			}
		}
		String confirm = (String)args.get("confirm");
		if(!StringUtils.isEmpty(confirm)) {
			confirm = "onClick=\"return confirm('"+confirm+"');\"";
		}
		out.print("<a class=\""+cssClass+"\" href=\""+actionDef.url+"\""+serialize(args, "action")+target+' '+confirm+"> <i class=\"fa "+iconClass+"\"></i> "+label+"</a>");
	}
}
