package controllers;

import models.elasticsearch.SearchResult;
import play.Logger;
import play.jobs.OnApplicationStart;
import play.mvc.Controller;
import repositories.elasticsearch.ElasticsearchRepository;


public class FirstStart extends Controller{

    public static void index(){
        Logger.debug("on first run page");
        play.cache.Cache.clear();
        render("PublikCtr/web/firstStart.html");
    }

    public static void maintenancemode(){
        play.cache.Cache.clear();
        render("PublikCtr/web/maintenancemode.html");
    }
}
