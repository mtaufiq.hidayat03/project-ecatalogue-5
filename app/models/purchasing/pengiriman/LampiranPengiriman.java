package models.purchasing.pengiriman;

import models.BaseKatalogTable;
import models.purchasing.base.BaseDataForm;
import models.purchasing.contracts.AttachmentContract;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import utils.DateTimeUtils;

import java.io.File;

/**
 * @author HanusaCloud on 10/18/2017
 */
@Table(name = "paket_pengiriman_lampiran")
public class LampiranPengiriman extends BaseKatalogTable implements AttachmentContract {

    @Id
    public Long id;
    public String file_sub_location;
    public String file_name;
    public String original_file_name;
    public Long file_size;
    public Long paket_id;
    public Integer posisi_file;
    public Long paket_pengiriman_id;
    public Long blb_id;

    public LampiranPengiriman paketPengiriman(PaketRiwayatPengiriman model) {
        this.paket_id = model.paket_id;
        this.paket_pengiriman_id = model.id;
        return this;
    }

    public Long getPengirimanId() {
        return paket_pengiriman_id;
    }

    @Override
    public LampiranPengiriman setFile(File file) {
        this.original_file_name = file.getName();
        this.file_size = file.length();
        this.file_sub_location = DateTimeUtils.generateDirFormattedDate();
        this.file_name = regenerateFileName();
        renameTo(file);
        return this;
    }

    public LampiranPengiriman dataForm(BaseDataForm model) {
        if (model.attachment != null) {
            setFile(model.attachment);
        }
        return this;
    }

    @Override
    public String getDownloadUrl() {
        return generateDownloadUrl("purchasing.PengirimanCtr.downloadLampiran");
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Long getPackageId() {
        return paket_id;
    }

    @Override
    public String getFileName() {
        return file_name;
    }

    @Override
    public String getSubLocation() {
        return file_sub_location;
    }

    @Override
    public String getOriginalFileName() {
        return original_file_name;
    }

    @Override
    public Integer getCreatedBy() {
        return created_by;
    }

    @Override
    public Long getModelId() {
        return id;
    }

    @Override
    public Long getBlobId() {
        return this.blb_id;
    }

    @Override
    public void saveAttachment() {
        this.id = (long) save();
    }

    @Override
    public void setBlobId(Long blobId) {
        this.blb_id = blobId;
    }

    public LampiranPengiriman position(int position) {
        this.posisi_file = position;
        return this;
    }

}
