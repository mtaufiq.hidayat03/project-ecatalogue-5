function getProdukKategoriByParent(url,value, level){
	$.ajax({
		url: url,
		type: "post",
		dataType: "json",
		data: {
			value: value
		},
		success: function (data) {
			if(data.length > 0){
				generateDropDown(data,level);
				have_child = true;
			}else{
				have_child = false;
			}

		}
	});
}

function getKategoriAtributByKatId(url,value){
	$.ajax({
		url: url,
		type: "post",
		dataType: "json",
		data: {
			kategori_id: value
		},
		success: function (data) {
			if(data.length > 0){
				var content="";
				$.each(data, function(idx, formAtr){
					if(formAtr.tipe_input == "label"){
						content += "<div class='panel panel-default'>" +
							"<div class='panel-heading'><span>"+formAtr.label_atribut+"</span></div>" +
							"<div class='panel-body'>";
						$.each(data, function (index, child) {
							if(child.parent_id === formAtr.id){
								var required = (child.wajib_diisi == 1) ? false : false;
								//var name = child.label_atribut.replace(/\W+/g, "_");
								var idKat = parseInt(child.id);
								content += "<div class='form-group'><label class='col-sm-2 control-label'>"+
									formAtr.label_atribut+"</label>" +
									"<div class='col-sm-4'><input type='"+child.tipe_input+"' " +
									"required='"+required+"' class='form-control' " +
									"name='infoTambahan' placeholder='"+child.label_atribut+"'/>" +
									"<input type='hidden' name='infoTambahanId' value='"+idKat+"'/></div></div>";
							}
						});
						content += "</div></div>";
					}
				});
				$.each(data, function(idx, formAtr){
					if(formAtr.tipe_input == "textarea" && formAtr.parent_id == 0) {
						var required = (formAtr.wajib_diisi == 1) ? false : false;
//                            var name = formAtr.label_atribut.replace(/\W+/g, "_");
						var idKat = parseInt(formAtr.id);
						content += "<div class='form-group'><label class='col-sm-2 control-label'>"+
							formAtr.label_atribut+"</label>" +
							"<div class='col-sm-4'><"+formAtr.tipe_input+" required='"+required+"' " +
							"class='form-control' " +
							"name='infoTambahan' placeholder='"+formAtr.label_atribut+"'/>" +
							"<input type='hidden' name='infoTambahanId' value='"+idKat+"'/></div></div>";
					} else if(formAtr.parent_id == 0 && formAtr.tipe_input != "label") {
						var required = (formAtr.wajib_diisi == 1) ? false : false;
//                            var name = formAtr.label_atribut.replace(/\W+/g, "_");
						var idKat = parseInt(formAtr.id);
						content += "<div class='form-group'><label class='col-sm-2 control-label'>"+
							formAtr.label_atribut+"</label>" +
							"<div class='col-sm-4'><input type='"+formAtr.tipe_input+"' " +
							"required='"+required+"' class='form-control' " +
							"name='infoTambahan' placeholder='"+formAtr.label_atribut+"'/>" +
							"<input type='hidden' name='infoTambahanId' value='"+idKat+"'/></div></div>";
					}

				});
				$("#dynamic-content").html("");
				$("#dynamic-content").append(content);
			}
		}
	});
}

function generateDropDown(data,level){
	var next_level = parseInt(level) + 1;

	if(level == current_last_level){
		current_last_level = next_level;
	}

	var html = "<option>Pilih Sub-Kategori </option>";
	for ( var key in data) {
		var obj = data[key];
		html += "<option value='"+obj["id"]+"'>"+obj["nama_kategori"]+"</option>";
	}

	for(var i = next_level; i <= current_last_level; i++){
		$("#level"+i).select2('destroy');
		$("#level"+i).remove();
	}

	// create new element
	html = "<select level='"+next_level+"' class='form-control pilih_kategori' id='level"+next_level+"'>" + html + "</select>";
	$("#form_kategori").append(html);
}