ALTER TABLE `penyedia_kontrak`
ADD `komoditas_id` Int(11) NULL;

ALTER TABLE `penyedia_kontrak`
ADD `file_hash` VARCHAR (255) NULL;

ALTER TABLE `penyedia_kontrak`
ADD `file_signature` VARCHAR(255) NULL;

ALTER TABLE `penyedia_kontrak`
ADD `dok_id_attachment` Int(11) NULL;