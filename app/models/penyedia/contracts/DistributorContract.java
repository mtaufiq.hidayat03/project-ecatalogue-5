package models.penyedia.contracts;

import models.penyedia.PenyediaDistributorRepresentatif;

import java.util.List;

/**
 * @author HanusaCloud on 11/8/2017
 */
public interface DistributorContract {

    Long getId();

    void setRepresentatives(List<PenyediaDistributorRepresentatif> list);

    default void withRepresentatives() {
        if (getId() != null) {
            setRepresentatives(PenyediaDistributorRepresentatif.findByDistributor(getId()));
        }
    }

}
