package models.purchasing.contracts;

import com.google.gson.Gson;
import models.purchasing.base.ProductJson;
import models.purchasing.pengiriman.LampiranPengiriman;
import models.purchasing.pengiriman.ProdukPengiriman;
import repositories.paket.DeliveryRepository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 11/2/2017
 */
public interface DeliveryContract extends HistoryContract {

    void setProducts(List<ProdukPengiriman> list);
    void setAttachment(LampiranPengiriman model);

    List<ProdukPengiriman> getProducts();

    default void withProducts() {
        if (getId() != null && getPackageId() != null) {
            List<ProdukPengiriman> list = DeliveryRepository.getProductsByDeliveryIdAndPackage(getPackageId(), getId());
            if (!list.isEmpty()) {
                for (ProdukPengiriman model : list) {
                    model.setImage();
                }
                setProducts(list);
            }
        }
    }

    default void withAttachment() {
        if (getId() != null && getPackageId() != null) {
            setAttachment(DeliveryRepository.getLatestAttachment(getPackageId(), getId()));
        }
    }

    default String getProductsJSON() {
        if (getProducts() != null && !getProducts().isEmpty()) {
            Map<Long, ProductJson> result = getProducts().stream()
                    .map(p -> new ProductJson(p)).collect(Collectors.toMap(ProductJson::getPackageProductId, p -> p));
            return new Gson().toJson(result);
        }
        return "{}";
    }

}
