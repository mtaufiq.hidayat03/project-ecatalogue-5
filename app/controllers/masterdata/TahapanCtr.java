package controllers.masterdata;

import controllers.BaseController;
import controllers.security.AllowAccess;
import models.jcommon.util.CommonUtil;
import models.masterdata.Tahapan;
import models.secman.Acl;
import play.i18n.Messages;

import java.util.Arrays;
import java.util.List;

public class TahapanCtr extends BaseController {

	@AllowAccess({Acl.COM_TAHAPAN})
	public static void index() {
		render();
	}

	@AllowAccess({Acl.COM_TAHAPAN_ADD})
	public static void create() { render(); }

	@AllowAccess({Acl.COM_TAHAPAN_ADD})
	public static void createSubmit(String[] nama_tahapan_arr, Integer[] urutan_tahapan_arr) {
		checkAuthenticity();
		if(nama_tahapan_arr != null){

			//validasi sumber dana
			boolean validate = validateTahapan(nama_tahapan_arr);
            boolean validateUrutan = validateUrutan(urutan_tahapan_arr);

			if(!validate || !validateUrutan){

				if (validation.hasErrors())
					flash.error(Messages.get("notif.failed.submit"));

				List<String> tahapanList = Arrays.asList(nama_tahapan_arr);
                List<Integer> urutanList = Arrays.asList(urutan_tahapan_arr);

				render("masterdata/TahapanCtr/create.html", tahapanList, urutanList);

			}

			Tahapan.saveTahapan(nama_tahapan_arr, urutan_tahapan_arr);

			flash.success(Messages.get("notif.success.submit"));
			index();
		}

	}

	@AllowAccess({Acl.COM_TAHAPAN_EDIT})
	public static void editSubmit(long id_tahapan, String nama_tahapan, Integer urutan_tahapan) {
		checkAuthenticity();
		Tahapan tahapan = Tahapan.findById(id_tahapan);
		if (CommonUtil.isEmpty(nama_tahapan)) {
			flash.error(Messages.get("notif.failed.update"));
		}
		else if (Tahapan.checkNamaTahapan(nama_tahapan) && !Tahapan.checkEditUrutan(urutan_tahapan)) {
			Tahapan.updateTahapan(id_tahapan, nama_tahapan, urutan_tahapan);
			flash.success(Messages.get("notif.success.update"));
		}
        else if (tahapan.urutan_tahapan.equals(urutan_tahapan) && Tahapan.checkNamaTahapan(nama_tahapan) && Tahapan.checkEditUrutan(urutan_tahapan)) {
			flash.error(Messages.get("urutan.nama.failed.update"));
        }
		else if (!tahapan.urutan_tahapan.equals(urutan_tahapan) && Tahapan.checkEditUrutan(urutan_tahapan)) {
			flash.error(Messages.get("urutan.failed.update"));
		}
		else if (tahapan.urutan_tahapan.equals(urutan_tahapan) && !tahapan.nama_tahapan_alias.equals(nama_tahapan)){
			Tahapan.updateTahapan(id_tahapan, nama_tahapan, urutan_tahapan);
			flash.success(Messages.get("notif.success.update"));
		}
        else {
            Tahapan.updateTahapan(id_tahapan, nama_tahapan, urutan_tahapan);
            flash.success(Messages.get("notif.success.update"));
        }
		index();
	}

    @AllowAccess({Acl.COM_TAHAPAN_EDIT})
    public static void flaggingSubmit(long id_tahapanflag, String flagging) {
        checkAuthenticity();
        if (CommonUtil.isEmpty(flagging)) {
            flash.error(Messages.get("notif.failed.update"));
        } else {
            Tahapan.updateFlagging(id_tahapanflag, flagging);
        }
        flash.success(Messages.get("notif.success.update"));
        index();
    }

	@AllowAccess({Acl.COM_TAHAPAN_DELETE})
	public static void delete(long id) {
		try {
			Tahapan.deleteTahapan(id);
			flash.success(Messages.get("notif.success.delete"));
		}catch (Exception e){
			flash.error(Messages.get("notif.failed.delete"));
		}

		index();
	}

	private static boolean validateTahapan(String[] nama_tahapan_arr){

		boolean validate = true;

		for(int i = 0; i < nama_tahapan_arr.length; i++){

			if(CommonUtil.isEmpty(nama_tahapan_arr[i])) {

				validate = false;

				validation.addError("tahapan_"+(i+1), Messages.get("nama_tahapan_harus_diisi"), "var");

			}
            if (Tahapan.checkNamaTahapan(nama_tahapan_arr[i])) {
                validate = false;
                validation.addError("tahapan_"+(i+1), Messages.get("nama_tahapan_tidak_boleh_sama"),"var");
            }
		}

		return validate;
	}

    private static boolean validateUrutan(Integer[] urutan_tahapan_arr){

        boolean validate = true;

        for(int i = 0; i < urutan_tahapan_arr.length; i++){

            if (Tahapan.checkEditUrutan(urutan_tahapan_arr[i])) {
                validate = false;
                validation.addError("urutan_"+(i+1), Messages.get("urutan.failed.update"),"var");
            }

        }

        return validate;
    }

	public static void getTahapan(){
		List<Tahapan> tahapanList = Tahapan.find("active = 1").fetch();

		renderJSON(tahapanList);
	}
}
