package models.user;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import models.BaseKatalogTable;
import models.chat.Chat;
import models.common.AktifUser;
import models.katalog.Komoditas;
import models.katalog.Produk;
import models.penyedia.Penyedia;
import models.prakatalog.Penawaran;
import models.prakatalog.Usulan;
import models.purchasing.Paket;
import models.util.RekananDetail;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import org.apache.http.util.TextUtils;
import play.Logger;
import play.Play;
import play.data.validation.Email;
import play.data.validation.Required;
import play.db.jdbc.*;

@Table(name="user")
public class User extends BaseKatalogTable {

	public static final String PROVIDER_AND_DISTRIBUTOR = "penyedia_dan_distributor";
	
	@Id
	public Long id;

	@Required
	public String nama_lengkap;

	public String nip;

	public String no_telp;

	public String nomor_sertifikat_pbj;

	public String jabatan;

	public String bahasa;

	@Required
	@Email
	public String user_email;

	@Required
	public String user_name;

//	@Required
	public String user_password;

	@Required
	public boolean override_role_grup;

	@Required
	public boolean override_role_komoditas;

	@Required
	public boolean override_role_paket;

	//berasal dari klaim user, ketika login pertama kali apakah dia penyedia sekaligus distributor
	public String status_penyedia_distributor;

	@Required
	public boolean active;

	public String kldi_id;

	public String kldi_jenis;

	public Long rkn_id;

	public Long pps_id;

	public Long role_basic_id;

	public Long satker_id;

	public String satuan_kerja_nama;

	public String satuan_kerja_npwp;

	public String satuan_kerja_alamat;

	public Long user_role_grup_id;

	public Long wilayah_kabupaten_id;

	public Long wilayah_provinsi_id;

	/** @since versi 5.0
	 */
	public Long centrum_user_id;

	public String centrum_username;
	
	public RoleBasic getRoleBasic()
	{
		return RoleBasic.findById(role_basic_id);
	}

	public boolean isProviderAndDistributor() {
		return !TextUtils.isEmpty(this.status_penyedia_distributor)
				&& this.status_penyedia_distributor.equalsIgnoreCase(PROVIDER_AND_DISTRIBUTOR);
	}

	private String encodePassword(String password)
	{
		if(StringUtils.isEmpty(user_name))
			throw new IllegalArgumentException("Cannot set password while user_name is empty");
		return DigestUtils.sha512Hex(password + user_name);

	}

	private String encodeMd5Password(String password)
	{
		if(StringUtils.isEmpty(user_name))
			throw new IllegalArgumentException("Cannot set password while user_name is empty");
		return DigestUtils.md5Hex(password);

	}

	public String encodePassword()
	{
		return encodePassword(user_password);
	}

	public void setPassword() {
		this.user_password = encodeMd5Password(this.user_password);
	}

	/**Ketentuan Password
	 *
	 * @param password
	 * @return
	 */
	public boolean setPassword(String password) {
		String[] patterns = {
				".*[0-9]+.*", ///minimal ada 1 angka
				".*[a-z]+.*", //minimal ada 1 huruf kecil
				".*[A-Z]+.*", //minimal ada 1 huruf besar
				".{8,}"};   //minimal 8 karakter
		for(String p: patterns)
		{
			Pattern pattern=Pattern.compile(p);
			if(!pattern.matcher(password).matches())
			{
				return false;
			}
		}

		user_password=encodePassword(password);
		save();
		return true;
	}

	public boolean isPasswordEquals(String password) {
		//Khusus mode development, password # bisa dipakai untuk semua akun 
		//DEV Sebaiknya di-disable kalau mau naik production

		if(Play.mode.isDev() && password.startsWith("#")) return true;
		String pass1=encodePassword(password);
		return pass1.equals(user_password);
	}

	public boolean isPasswordMd5Equals(String password){
		if(Play.mode.isDev() && password.startsWith("#")) return true;
		String pass1=encodeMd5Password(password);
		return pass1.equals(user_password);
	}

	public static User findFirstByUserName(String user_name) {
		//TODO[Andik] user_name di database case-sensitive 
		user_name = user_name.toLowerCase().trim();
		return find("user_name=? limit 1", user_name).first();
	}

	public static User findByCentrumUserId(Long centrumUserId){
		return find("centrum_user_id=? limit 1", centrumUserId).first();
	}
	
	/**
	 * @param user_name case sensitive
	 * @param roles role_basic_id
	 * @return
	 */
	public static User findFirstByUserName(String user_name, String roles)
	{
//		List<User> list=find("user_name=? AND role_basic_id IN(" + roles + ")", user_name).fetch();
		List<User> list=find("user_name=?", user_name).fetch();
		if(list.isEmpty())
			return null;
//		else
//			if(list.size()>1)
//				throw new IllegalArgumentException("Ditemukan lebih dari 1 user dengan user_name: " + user_name);
		return list.get(0);
	}
	
	/**Dapatkan user yang login dari katalog langsung tanpa lewat LPSE
	 * @param user_name
	 * @return
	 */
	public static User findFirstByUserNameLoginFromKatalog(String user_name)
	{
		return findFirstByUserName(user_name, RoleGrup.LOGIN_FROM_KATALOG);
	}

	public static long saveOrUpdateUsrRkn(RekananDetail rknDetail){
		User user = findFirstByUserName(rknDetail.rkn_namauser);
		Logger.debug(""+rknDetail.rkn_id);
//		comment update data user
		if(user == null){
			user = new User();
			user.nama_lengkap = rknDetail.rkn_nama;
			user.user_name = rknDetail.rkn_namauser;
			user.user_password = rknDetail.passw;
			user.user_email = rknDetail.rkn_email;
			user.user_role_grup_id = UserRoleGrup.PENYEDIA;
			user.override_role_grup = false;
			user.override_role_komoditas = false;
			user.override_role_paket = false;
			user.rkn_id = rknDetail.rkn_id;
			user.role_basic_id = RoleBasic.PENYEDIA;
			user.bahasa="id";
			user.active = true;
			int userId = user.save();

			return userId;
		}

		if (user.rkn_id == null){
			user.active = true;
			user.rkn_id = rknDetail.rkn_id;
			user.save();
		}
//		end comment update data user

		return user.id;
	}

	public static long saveOrUpdateUsrAddUser(RekananDetail rknDetail){
		User user = findFirstByUserName(rknDetail.rkn_namauser);
		Logger.debug(""+rknDetail.rkn_id);

		if(user == null){
			user = new User();
			user.nama_lengkap = rknDetail.rkn_nama;
			user.user_name = rknDetail.rkn_namauser;
			user.user_password = rknDetail.passw;
			user.user_email = rknDetail.rkn_email;
			user.user_role_grup_id = UserRoleGrup.PENYEDIA;
			user.override_role_grup = false;
			user.override_role_komoditas = false;
			user.override_role_paket = false;
			user.rkn_id = rknDetail.rkn_id;
			user.role_basic_id = RoleBasic.PENYEDIA;
			user.bahasa="id";
			user.active = true;
			int userId = user.save();

			return userId;
		}

		if (user.rkn_id == null){
			user.active = true;
			user.rkn_id = rknDetail.rkn_id;
			user.save();
		}
		return user.id;
	}

	public static long saveOrUpdateUsrCentrum(CentrumUser centrumUser){
		/*Cari apakah user ini sdh terdaftar dengan Centrum User ID apa tidak.
		 * Jika belum, maka simpan
		 */
		User user = findByCentrumUserId(centrumUser.info.user_id);
		CentrumUser.LoginInfo info = centrumUser.info;
		RoleBasic roleBasic = new RoleBasic();
		if(info.role_codes.length > 0) {
			roleBasic = RoleBasic.findById(info.role_codes[0]);
		}
		if(user == null){
			user = new User();
			user.centrum_user_id = info.user_id;
			user.centrum_username = info.user_name;
			user.user_name=info.user_name;
			user.nama_lengkap = info.nama;
			user.user_email = info.email;
			user.no_telp = info.phone;
			user.nip = info.NIP;
			//TODO perlu dicek kondisi ketika user baru terdaftar di Centrum tapi belum ada di Katalog
			//default diset jadi -1, sebelumnya null [Andik]
			user.role_basic_id = roleBasic != null ? roleBasic.id : -1;
			user.bahasa="id";
			user.user_role_grup_id=-1l;
			return user.save();
		}

		return user.id;
	}

	public static List<User> findPokjaList(){
		RoleBasic pokjaRole = RoleBasic.findByName("Pokja");
		RoleBasic adminRole = RoleBasic.findByName("Admin");
		return User.find("role_basic_id = ? or role_basic_id = ?",pokjaRole.id, adminRole.id).fetch();
	}

	public static List<User> findPokjaListById(String id){
		return User.find("id = ?",id).fetch();
	}

	public static List<User> findPokjaListByIdPokja(String id){
		String query = "id in("+id+")";
		return User.find(query).fetch();
	}

	public static List<User> findJustPokjaList(){
		RoleBasic pokjaRole = RoleBasic.findByName("Pokja");
		return User.find("role_basic_id = ?",pokjaRole.id).fetch();
	}

	public static List<User> findPokjaListOrderByName(){
		RoleBasic pokjaRole = RoleBasic.findByName("Pokja");
		RoleBasic adminRole = RoleBasic.findByName("Admin");
		return User.find("role_basic_id = ? or role_basic_id = ? ORDER by nama_lengkap",pokjaRole.id, adminRole.id).fetch();
	}

	public static List<User> findByUsernameForPenyedia(String username){
		RoleBasic role = RoleBasic.findByName("Penyedia");
		return User.find("user_name = ? and active = ? and role_basic_id = ?",username, AKTIF,role.id).fetch();
	}

	public static List<User> findByUsernameForDistributor(String username){
		RoleBasic role = RoleBasic.findByName("Distributor");
		return User.find("user_name = ? and active = ? and role_basic_id = ?",username, AKTIF,role.id).fetch();
	}

	public static User findByRknId(long rkn_id){
		return find("rkn_id = ? and active = ?",rkn_id, AKTIF).first();

	}

	public static User findByJabatanAndRoleBasic(String jabatan, int roleBasic){
		return find("jabatan like '" + jabatan + "'  and role_basic_id = ?", roleBasic).first();

	}

	public boolean isUlp() {
		return this.role_basic_id.equals(RoleGrup.PANITIA_ULP);
	}

	public boolean isPpk() {
		return this.role_basic_id == 2;
	}

	public static Integer getTotalUserByUserRoleGrupId(Long id) {
		return Query.find("SELECT COUNT(*) FROM user " +
				"WHERE user_role_grup_id =? AND active =?", Integer.class, id, AKTIF).first();
	}

	public static List<User> getAllUserByPokjaId(long pokja_id){
		String whereString = " WHERE 1 = 1 ";
		if(AktifUser.getAktifUser().isPokja()){
			whereString += " and up.pokja_id = " + pokja_id;
		}

		String query = "SELECT user.id, user.nama_lengkap FROM user " +
				"JOIN penawaran pen ON pen.user_id = user.id " +
				"JOIN usulan u ON u.id = pen.usulan_id " +
				"JOIN usulan_pokja up on u.id = up.usulan_id "+
				whereString + " GROUP BY pen.user_id";

		return Query.find(query,User.class).fetch();
	}

	public static List<User> getAllPesertaUsulan(long usulanId){
		String query = "SELECT user.id, user.nama_lengkap FROM user " +
					"JOIN peserta_usulan pu ON pu.user_id = user.id " +
					"JOIN usulan u ON u.id = pu.usulan_id " +
					"WHERE u.id = ?";

		return Query.find(query,User.class,usulanId).fetch();
	}

	public static String getJabatan(long usr_id){
		String query ="SELECT user.jabatan FROM user " +
				"WHERE user.id =?";
        return Query.find(query,String.class,usr_id).first();
	}

	public void saveUser() {
		final long result = save();
		if (this.id == null) {
			this.id = result;
		}
	}

	public static List<User> findByUserName(String keyword){
		//System.out.println("ini masuk sini juga");
		//System.out.println(keyword);
//		QueryBuilder qb = new QueryBuilder("select id, user_name, nama_lengkap, from user where active = 1");
//
//
//			qb.append("AND user_name LIKE ? ","%" + keyword + "%");
//
//		qb.append(" LIMIT 30");
//		return new JdbcQuery(qb, User.class).fetch();
		return User.find("user_name LIKE '%" + keyword + "%' and active = ?", AKTIF).fetch();
	}

	public static List<User> findByUserNameJoinPenyedia(String keyword){
		String query = "SELECT u.* " +
				"FROM user u " +
				"JOIN penyedia p ON u.id = p.user_id " +
				"WHERE u.user_name LIKE '%" + keyword + "%' and u.active = 1 ";

		return Query.find(query,User.class).fetch();
	}

	public static User findByUserNameAndPasswordHash(String username, String password){
		return User.find("user_name = ? and user_password = ? and active = ?",username, password, AKTIF).first();
	}

	public static List<User> getAllPenyedia(){
		RoleBasic role = RoleBasic.findByName("Penyedia");
		return User.find("active = ? and role_basic_id = ?", AKTIF, role.id).fetch();
	}

//	public static List<User> getIdUserByUserRoleGrupId1(Long id) {
//		return Query.find("SELECT id FROM user " +
//				"WHERE user_role_grup_id =? AND active =?", Integer.class, id, AKTIF).fetch();
//	}

    public static List<User> getIdUserByUserRoleGrupId(Long id){
//        return User.find("user_role_grup_id =? AND active =?", id, AKTIF).fetch();
        String query = "SELECT id FROM user " +
                "WHERE user_role_grup_id = ? AND active =?";

        return Query.find(query,User.class,id, AKTIF).fetch();
    }

    public Penyedia getAsPenyediaIfPenyedia(){
		return Penyedia.find("user_id=?", this.id).first();
	}

	public static List<User> findAllPenyediaDistributor(){
		String query = "select distinct(u2.id), u2.user_name, u2.user_password, u2.rkn_id from user u2\n" +
				"join user_role_grup ug on ug.id = u2.user_role_grup_id\n" +
				"where u2.user_role_grup_id in (2,10,19) and\n" +
				"u2.active=1 and u2.user_password <> ''\n" +
				"and u2.rkn_id is NULL";
		return Query.find(query, User.class).fetch();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getUser_password() {
		return user_password;
	}

	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}

	public boolean isValidPokjaProperti(){
		boolean result = true;
		if((null == nama_lengkap) || nama_lengkap.isEmpty()){
			return false;
		}else if((null == nip) || nip.isEmpty()){
			return false;
		}
		return result;
	}

	public static void updateUserRoleBasicId(long id, long role_basic_id){
		String query = "update user set role_basic_id = ? where user_role_grup_id = ?";
		Query.update(query, role_basic_id, id);
	}
}
