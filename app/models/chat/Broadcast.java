package models.chat;


import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;

@Table(name = "broadcast")

public class Broadcast extends BaseKatalogTable {
    @Id
    public Long id;
    public int filter_id;
    public Long user_role_grup_id;
    public Long komoditas_id;
    public String judul;
    public String deskripsi;
    public Boolean active;

    public static List<Broadcast> findBroadcastByUserRoleGrupId(Long user_role_grup_id){
        String query = "SELECT * " +
                "FROM broadcast " +
                "WHERE user_role_grup_id = "+user_role_grup_id+" " +
                "ORDER BY created_date desc";
        return Query.find(query,Broadcast.class).fetch();
    }
}