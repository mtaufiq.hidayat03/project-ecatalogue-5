package models.prakatalog;

import models.BaseKatalogTable;
import models.prakatalog.form.FormBukaPenawaran;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import utils.LogUtil;

import java.util.List;

/**
 * Created by raihaniqbal on 8/4/2017.
 */
@Table(name = "usulan_dokumen_penawaran")
public class UsulanDokumenPenawaran extends BaseKatalogTable {

	public static final String TAG = "UsulanDokumenPenawaran";

	@Id
	public Long id;

	@Required
	public Long usulan_id;

	@Required
	public String nama_dokumen;

	@Required
	public String jenis_evaluasi;

	public boolean active;

	public static void simpanUsulanDokumenPenawaran(FormBukaPenawaran form) {
		UsulanDokumenPenawaran.delete("usulan_id = ? ", form.usulan_id);
		if (form.dokumen_arr == null || form.dokumen_arr.length == 0) {
			LogUtil.debug(TAG, "dokumen arr is empty");
			return;
		}
		for (int i = 0; i < form.dokumen_arr.length; i++) {

			UsulanDokumenPenawaran udp = new UsulanDokumenPenawaran();
			udp.jenis_evaluasi = form.jenis_evaluasi[i];
			udp.nama_dokumen = form.dokumen_arr[i];
			udp.usulan_id = form.usulan_id;
			udp.active = true;

			udp.save();
		}
	}

	public static List<UsulanDokumenPenawaran> findBy(Long usulanId) {
		return UsulanDokumenPenawaran
				.find("usulan_id = ? AND active = 1", usulanId)
				.fetch();
	}

}
