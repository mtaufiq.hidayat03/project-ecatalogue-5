package controllers.masterdata;

import controllers.BaseController;
import controllers.security.AllowAccess;
import models.jcommon.util.CommonUtil;
import models.masterdata.Kurs;
import models.masterdata.KursNilai;
import models.purchasing.Paket;
import models.secman.Acl;
import play.Logger;
import play.i18n.Messages;
import services.produk.ProdukService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

public class KursCtr extends BaseController{

    @AllowAccess({Acl.COM_KURS})
    public static void index(){
        render();
    }

    @AllowAccess({Acl.COM_KURS_ADD})
    public static void create(){
        render();
    }

    @AllowAccess({Acl.COM_KURS_ADD})
    public static void createSubmit(String[] nama_kurs_arr) {
        checkAuthenticity();
        if(nama_kurs_arr != null){

            //validasi kurs
            boolean validate = validateKurs(nama_kurs_arr);

            if(!validate){

                if (validation.hasErrors())
                    flash.error("Gagal Simpan Kurs, periksa kembali inputan anda.");

                List<String> kursList = Arrays.asList(nama_kurs_arr);

                render("masterdata/KursCtr/create.html", kursList);

            }


            for(int i = 0; i < nama_kurs_arr.length; i++){
                Kurs sd = new Kurs();
                sd.nama_kurs = nama_kurs_arr[i];
                sd.active = true;
                sd.save();
            }

            flash.success("Data berhasil disimpan");
            index();
        }

    }

    public static void detail(long id) {
        Kurs model = Kurs.findById(id);
        notFoundIfNull(model);
        render("masterdata/KursCtr/detail.html", model);
    }

    @AllowAccess({Acl.COM_KURS_EDIT})
    public static void editSubmit(long id_kurs, String nama_kurs) {
        checkAuthenticity();
        if (CommonUtil.isEmpty(nama_kurs)) {
            flash.error("Gagal Ubah kurs, nama kurs tidak boleh kosong");
        } else {
            Kurs kurs = Kurs.findById(id_kurs);
            kurs.nama_kurs = nama_kurs;
            kurs.active = true;
            kurs.save();
        }
        flash.success("Data berhasil disimpan");
        index();
    }

    @AllowAccess({Acl.COM_KURS_EDIT})
    public static void editNilaiSubmit(long id, long id_kurs, String tanggal_kurs, Double nilai_jual, Double nilai_beli) {
        checkAuthenticity();
        Logger.debug("Kurs ID = %s", id_kurs);
        if(id==0) {
            KursNilai kursNilai = new KursNilai();
            try {
                kursNilai.kurs_id_from = id_kurs;
                kursNilai.kurs_sumber_id = 1L;
                kursNilai.tanggal_kurs = (new SimpleDateFormat("dd-MM-yyyy")).parse(tanggal_kurs);
                kursNilai.nilai_jual = nilai_jual;
                kursNilai.nilai_beli = nilai_beli;
                kursNilai.nilai_tengah = (nilai_jual + nilai_beli)/2;
                kursNilai.save();
                flash.success("Data berhasil disimpan");
                detail(kursNilai.kurs_id_from);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else {
            KursNilai kursNilai = KursNilai.findById(id);
            kursNilai.nilai_jual = nilai_jual;
            kursNilai.nilai_beli = nilai_beli;
            kursNilai.nilai_tengah = (nilai_jual + nilai_beli)/2;
            kursNilai.save();
            flash.success("Data berhasil disimpan");
            detail(kursNilai.kurs_id_from);
        }
    }

    @AllowAccess({Acl.COM_KURS_DELETE})
    public static void delete(long id) {
        Kurs model = Kurs.findById(id);
        notFoundIfNull(model);
        if (kursIsInUse(id)) {
            model.softDelete();
            flash.success(Messages.get("notif.success.delete"));
        } else {
            flash.error(Messages.get("notif.failed.delete.relation"));
        }
        index();
    }

    private static boolean kursIsInUse(long id) {
        return ProdukService.getTotalProductByKurs(id) > 0 && Paket.getTotalPackageByKurs(id) > 0;
    }

    private static boolean validateKurs(String[] nama_kurs_arr){

        boolean validate = true;

        for(int i = 0; i < nama_kurs_arr.length; i++){

            if(CommonUtil.isEmpty(nama_kurs_arr[i])) {

                validate = false;

                validation.addError("kurs_"+(i+1), "Nama Kurs Harus Diisi", "var");

            }

        }

        return validate;

    }

    @AllowAccess({Acl.COM_KURS_EDIT})
    public static void setAktif(long id) {
        Kurs kurs = Kurs.findById(id);
        notFoundIfNull(kurs);
        kurs.setAktif();
        flash.success(Messages.get("record_update_aktifnonaktif_relation_success"));
        index();
    }

    @AllowAccess({Acl.COM_KURS_EDIT})
    public static void setNonAktif(long id) {
        Kurs kurs = Kurs.findById(id);
        notFoundIfNull(kurs);
        if(!kursIsInUse(id)){
            kurs.setNonAktif();
            flash.success(Messages.get("record_update_aktifnonaktif_relation_success"));
        }else{
            flash.error(Messages.get("record_update_aktifnonaktif_relation_failed"));
        }
        index();
    }

}
