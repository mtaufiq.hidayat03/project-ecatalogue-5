CREATE TABLE `parameter_aplikasi`(
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`nama_parameter` VARCHAR(255) NOT NULL,
	`isi_parameter` VARCHAR(255) NOT NULL,
	`deskripsi` VARCHAR(255) NOT NULL,
	`created_date` DateTime NULL,
	`created_by` Int( 5 ) NULL,
	`modified_date` DateTime NULL,
	`modified_by` Int( 5 ) NULL,
	`deleted_date` DateTime NULL,
	`deleted_by` Int( 5 ) NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`auditdate` Timestamp NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`active` Int(1) NULL,
	PRIMARY KEY (`id`)
)
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;