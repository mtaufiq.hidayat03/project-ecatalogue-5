package jobs.ExtProduct;

import models.penyedia.Penyedia;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import play.jobs.On;

import java.util.List;

/** job di eksekusi setiap jam 22:00 **/
//@On("0 0 22 * * ?")
@NoTransaction
public class JobCrawlVendor extends Job {

    public void doJob(){
        ExtCrawlVendor varc = new ExtCrawlVendor();
        List<Penyedia> listPenyedia = Penyedia.getVendorAPI();
        for (Penyedia py: listPenyedia) {
            varc.CrawlVendor(py.komoditas_id, py.id);
        }
    }
}
