package services.Login;

import models.user.User;
import models.user.User2;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;

@NoTransaction
public class JobUpdateRkn_Id extends Job {
    public Long userid = -1l;
    public String username = "";
    public String password = "";
    public Long rkn_id = null;
    int isUpdate = 0;
    @Override
    public void doJob() {
        Long rknid = LoginServices.loginAdpGetRknidNoHash(username,password);
        Logger.info("> > > > > > > > > > > > > > > > > > > > > > > Rekananid user: %s",rknid);
        Long idDefault = LoginServices.getRkn_IdActionDefault(rknid);
        if( idDefault != -1l ) {
            Logger.info("< < < < < < < < < < < < < < < < < < < < < < < Default Rekananid user: %s", idDefault);
            User u = User.findFirstByUserName(username);
            if( idDefault != -1l ) {
                u.rkn_id = idDefault;
                rkn_id = idDefault;
            }
             isUpdate = u.save();
        }
    }

    @Override
    public void _finally() {
        System.out.println("####### @@@@@@@@@@@@ "+userid + " / " + rkn_id + " isupdate="+isUpdate);
    }
}
