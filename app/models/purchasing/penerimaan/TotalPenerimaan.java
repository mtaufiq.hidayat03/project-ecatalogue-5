package models.purchasing.penerimaan;

/**
 * @author HanusaCloud on 10/25/2017
 */
public class TotalPenerimaan {

    public Long total_delivered = 0L;
    public Long total_requested = 0L;

    public boolean isCompleted(Long currentDeliverAmount) {
        return (total_requested - (total_delivered + currentDeliverAmount)) == 0;
    }

}
