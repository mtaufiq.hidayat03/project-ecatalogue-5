package models.purchasing.pembayaran;

import ext.FormatUtils;
import utils.DateTimeUtils;
import utils.KatalogUtils;
import utils.UrlUtil;

import java.util.Date;

/**
 * @author HanusaCloud on 11/2/2017
 */
public class DeliveryItem {

    public Long id;
    public Long packageId;
    public Date documentDate;
    public String documentNumber;
    public String systemDocumentNumber;
    public Double total;
    public String description;
    public Long paymentVerificationId;

    public String getReadableDocumentDate() {
        return DateTimeUtils.parseToReadableDate(documentDate);
    }

    public String getFormattedTotal() {
        return FormatUtils.formatCurrencyRupiah(total);
    }

    public String getDeliveryDetailUrl() {
        return UrlUtil.builder()
                .setUrl("purchasing.PenerimaanCtr.detailRiwayatPengiriman")
                .setParam("id", id)
                .setParam("paket_id", packageId)
                .build();
    }

    public String getFormattedDocNumber() {
        return KatalogUtils.getFormattedDocNumber(documentNumber);
    }

    public String getFormattedSystemDocNumber() {
        return KatalogUtils.getFormattedDocNumber(systemDocumentNumber);
    }

}
