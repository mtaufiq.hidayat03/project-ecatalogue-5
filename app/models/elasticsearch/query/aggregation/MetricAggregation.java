package models.elasticsearch.query.aggregation;

import com.google.gson.JsonObject;

/**
 * @author HanusaCloud on 10/10/2017
 */
public class MetricAggregation {

    public static final String METHOD_MAX = "max";
    public static final String METHOD_MIN = "min";

    private String field;
    private String method;
    private String alias;

    public MetricAggregation setField(String field) {
        this.field = field;
        return this;
    }

    public MetricAggregation setMethod(String method) {
        this.method = method;
        return this;
    }

    public MetricAggregation setAlias(String alias) {
        this.alias = alias;
        return this;
    }

    public String getAlias() {
        return alias;
    }

    public JsonObject build() {
        JsonObject result = new JsonObject();
        JsonObject query = new JsonObject();
        query.addProperty("field", field);
        result.add(method, query);
        return result;
    }


}
