package models.elasticsearch.product;

import com.google.gson.annotations.SerializedName;
import models.katalog.Produk;
import models.masterdata.Kurs;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 9/29/2017
 */
public class ProductElastic implements Serializable {

    @SerializedName("produk_id")
    public long id = 0;
    @SerializedName("nama_produk")
    public String productName = "";
    @SerializedName("image_url")
    public String imageUrl;
    @SerializedName("penyedia_id")
    public long providerId = 0;
    @SerializedName("nama_penyedia")
    public String providerName = "";
    @SerializedName("status")
    public int status = 0;
    @SerializedName("nama_komoditas")
    public String commodityName = "";
    @SerializedName("nama_manufaktur")
    public String manufactureName = "";
    //ternyata lokal dan import
    @SerializedName("jenis_produk")
    public String productType = "";
    @SerializedName("komoditas_id")
    public Long commodityId;
    @SerializedName("apakah_iklan")
    public boolean apakahIklan;
    @SerializedName("categories")
    public List<ProductCategoryElastic> categories = new ArrayList<>();
    @SerializedName("priceList")
    public List<ProductPriceElastic> priceList = new ArrayList<>();
    @SerializedName("stock")
    public double stock = 0;
    @SerializedName("is_umkm")
    public boolean isUmkm;

    public String getProductName() {
        return productName;
    }

    public String getManufactureName() {
        return manufactureName;
    }


    public static String formatCurrencyKurs(Number number, String money) {
        String hasil = money+" ";
        DecimalFormat df = new DecimalFormat("#,###,##0.00");
        return hasil+ df.format(number);
    }
    public String getPrice() {
        double min = getMinPrice();
        String namaKurs = "IDR";//default IDR
        try{
            Produk produk = Produk.findById(this.id);
            Kurs kurs = Kurs.findById(produk.harga_kurs_id);
            namaKurs =  kurs.nama_kurs;
        }catch (Exception e){}
        //final String minPrice = FormatUtils.formatCurrencyRupiah(min);//formatDesimal2US
        final String minPrice = formatCurrencyKurs(min, namaKurs);//formatDesimal2US
        if (priceList.size() > 1) {
            double max = getMaxPrice();
            if (min == max) {
                return minPrice;
            }
            //final String maxPrice = FormatUtils.formatCurrencyRupiah(max);
            final String maxPrice = formatCurrencyKurs(max,namaKurs);
            return minPrice + " - " + maxPrice;
        }
        return minPrice;
    }

    public double getMinPrice() {
        if (!priceList.isEmpty()) {
            return priceList.get(0).hargaUtama;
        }
        return 0;
    }

    public double getMaxPrice() {
        final int size = priceList.size();
        if (size > 0) {
            return priceList.get(size > 1 ? size - 1 : 0).hargaUtama;
        }
        return 0;
    }

    public double getGovernmentPrice() {
        if (!priceList.isEmpty()) {
            return priceList.get(0).hargaPemerintah;
        }
        return 0;
    }

    public String getCategory() {
        if (!categories.isEmpty()) {
            final int size = categories.size();
            return categories.get(size > 1 ? size - 1 : 0).name;
        }
        return "";
    }

}
