package models.purchasing.base;

import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 10/23/2017
 */
public class BaseProductForm {

    @SerializedName("productId")
    public Long productId;
    @SerializedName("isChecked")
    public boolean isChecked;
    @SerializedName("productQuantity")
    public Long productQuantity;
    @SerializedName("productNote")
    public String productNote;
    @SerializedName("modelId")
    public Long modelId;
    @SerializedName("packageProductId")
    public Long packageProductId;

    public boolean isCreatedMode() {
        return modelId == null;
    }

}
