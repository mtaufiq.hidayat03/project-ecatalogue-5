$("#search_user").click(function () {
    var keyword = $("#keyword").val();
    var komoditas = $("select#komoditas").val();
    var loader = Ladda.create(document.querySelector('#search_user'));
    loader.start();

    $.ajax({
        type:'post',
        url: params.searchUrl,
        data: {
            keyword: keyword
        },
        dataType:'json',
        success:function (result) {
            loader.stop();
            if(result.status){
                var data = result.data;
                var tr;

                $("#tblPaket").find("tr:gt(0)").remove();

                for(var i=0; i<data.length;i++){
                    var disabled = "";

                    var checkArray = user_ids.indexOf(data[i].id);
                    if(checkArray > -1){
                        disabled = "disabled='disabled'";
                    }

                    tr = $('<tr/>');
                    tr.append("<td>" + data[i].id + "</td>");
                    tr.append("<td>" + data[i].user_name + "</td>");
                    tr.append("<td>" + data[i].nama_lengkap + "</td>");
                    tr.append("<td>" + data[i].pps_id + "</td>");
                    tr.append("<td>" +
                        "<button type='button' class='btn btn-primary choose paket-"+data[i].id+"' " +
                        "userId='"+data[i].id+"' "+disabled+" userUserName='"+data[i].user_name+"' userNamaLengkap='"+data[i].nama_lengkap+"' userPpsId='"+data[i].pps_id+"'>Pilih" +
                        "</button></td>");

                    $("#tblPaket").append(tr);
                }
            }

        },
        complete: function () {

        }
    });

});

$("body").on("click",".choose", function () {
    var idUser = $(this).attr("userId");
    var userName = $(this).attr("userUserName");
    var namaLengkap = $(this).attr("userNamaLengkap");
    var idPps = $(this).attr("userPpsId");

    //add input
    var input = "<input type='hidden' name='user_ids' value='"+idUser+"'>";
    $("#added_package").append(input);

    //add label view
    var row = $('<tr/>');
    row.append("<td>" + idUser + "</td>");
    row.append("<td>" + userName + "</td>");
    row.append("<td>" + namaLengkap + "</td>");
    row.append("<td>" + idPps + "</td>");
    row.append("<td><button type='button' class='remove_package btn btn-danger' data-value='"+idUser+"'>x</button>" );

    $("#selected_package").append(row);

    user_ids.push(parseInt(idUser));
    $(this).attr('disabled','disabled');

});

$(document).on("click",".remove_package", function () {
    var idPaket = $(this).data("value");

    //remove from array
    var index = user_ids.indexOf(idPaket);
    if (index > -1) {
        user_ids.splice(index, 1);
    }

    //remove input
    $("#added_package :input[value='"+idPaket+"']").remove();

    //remove label view
    $(this).parent().parent().remove();

    //enable button
    var button = $("#tblPaket").find(".paket-"+idPaket);
    if(button){
        button.prop('disabled',false);
    }

});