package jobs.elasticsearch;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import jobs.ExtProduct.ExtProduct;
import jobs.JobTrail.SingleJobSendElastics;
import jobs.TrackableJob;
import models.elasticsearch.KategoriElastic;
import models.elasticsearch.log.ProductLogJson;
import models.elasticsearch.product.ProductCategoryElastic;
import models.elasticsearch.product.ProductPriceElastic;
import models.jcommon.util.CommonUtil;
import models.katalog.ProdukGambar;
import models.katalog.ProdukHarga;
import models.util.Config;
import models.util.produk.HargaKabupatenJson;
import models.util.produk.ProdukHargaJson;
import org.jsoup.parser.Parser;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.jobs.Job;
import play.mvc.Scope;
import repositories.elasticsearch.ElasticsearchRepository;
import repositories.produkkategori.ProdukKategoriRepository;
import services.produk.ProdukService;
import utils.LogUtil;

import java.io.*;
import java.math.BigDecimal;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static constants.models.ProdukConstant.*;

public class ElasticsearchLog {

    public static final String TAG = "ElasticsearchLog";
    private boolean isLogged = true;
    private String filePath;
    private String indexFilePath;
    private Connection connection;
    public String report = "";
    public String elasticsearchIndex = Config.getInstance().elasticsearchIndex;
    public static void main(String[] args) throws Exception {
        //ElasticsearchLog elasticsearchLog = new ElasticsearchLog();
        //to execute, into dev server
        //elasticsearchLog.process();
        //elasticsearchLog.addRemoveByKomoditas(24L, true, true);
        //local asep
        //elasticsearchLog.createDmp();
        //elasticsearchLog.addRemoveProduk(706288l);
    }

    public void preparation() throws Exception {
        Play.configuration = new Properties();
        Play.applicationPath = new File(System.getProperty("application.path", "."));
        Play.configuration.load(new FileInputStream(Play.applicationPath+"/conf/application.conf"));
        String dbUrl = Play.configuration.getProperty("db.default.url");
        String user = Play.configuration.getProperty("db.default.user");
        String pass = Play.configuration.getProperty("db.default.pass");
        final String mode = Play.configuration.getProperty("application.mode");
        isLogged = mode.equalsIgnoreCase("dev") || mode.equalsIgnoreCase("debug");
        connection = DriverManager.getConnection(dbUrl, user, pass);
        filePath = Play.applicationPath + "/logstash/elastics.dump";
        indexFilePath = Play.applicationPath + "/logstash/index.json";
        elasticsearchIndex = Play.configuration.getProperty("elasticsearchIndex");
        File file = new File(filePath);
        if (file.exists() && file.delete()) {
            logging("delete file: " + filePath);
        }
    }

    private List<ProductLogJson> getProductbyKomoditas(Long komoditas_id) throws SQLException {
        List<ProductLogJson> results = new ArrayList<>();
        if (isConnected()) {
            String query = ProdukService.Q_DAFTAR_PRODUK_GENERAL + " and k.id="+komoditas_id;
            Logger.info(query);
            if (isLogged) {
                //query += " LIMIT 5000 OFFSET 6000;";
            }
            ResultSet resultSet = connection.createStatement().executeQuery(query);
            while (resultSet.next()) {
                ProductLogJson model = new ProductLogJson();
                model.id = resultSet.getLong(ID);
                String namaProduk = Parser.unescapeEntities(resultSet.getString(NAMA_PRODUK), true);
                model.nama_produk = null!=namaProduk?namaProduk.trim():namaProduk;
                model.komoditas_id = resultSet.getLong(KOMODITAS_ID);
                String namaKom = resultSet.getString("nama_komoditas").trim();
                model.nama_komoditas = null!=namaKom?namaKom.trim():namaKom;
                model.kelas_harga = resultSet.getString("kelas_harga");
                model.penyedia_id = resultSet.getLong("penyedia_id");
                String namaPenyedia = resultSet.getString(NAMA_PENYEDIA).trim();
                model.nama_penyedia = null!=namaPenyedia?namaPenyedia.trim():namaPenyedia;
                model.manufaktur_id = resultSet.getLong(MANUFAKTUR_ID);
                String nama_manufaktur = resultSet.getString(NAMA_MANUFAKTUR);
                model.nama_manufaktur = null!=nama_manufaktur?nama_manufaktur.trim():nama_manufaktur;
                model.produk_kategori_id = resultSet.getLong("produk_kategori_id");
                model.harga_utama = resultSet.getLong(HARGA_UTAMA);
                model.harga_pemerintah = resultSet.getLong(HARGA_PEMERINTAH);
                String no_produk = resultSet.getString("no_produk");
                model.no_produk = null!=no_produk?no_produk.trim():no_produk;
                String no_produk_penyedia =  resultSet.getString("no_produk_penyedia");
                model.no_produk_penyedia = null != no_produk_penyedia? no_produk_penyedia.trim():no_produk_penyedia;
                model.stock = resultSet.getDouble(JUMLAH_STOK);
                model.jenis_produk = resultSet.getString(JENIS_PRODUK);
                java.sql.Date date = resultSet.getDate(BERLAKU_SAMPAI);
                if (date != null) {
                    model.expired = new SimpleDateFormat("yyyy-MM-dd").format(date);
                }
                Timestamp createStamp = resultSet.getTimestamp(CREATED_DATE);
                if (createStamp != null) {
                    java.util.Date createdDate = new java.util.Date();
                    createdDate.setTime(createStamp.getTime());
                    model.created_at = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZ")
                            .format(createdDate);
                }
                Timestamp modifiedStamp = resultSet.getTimestamp(MODIFIED_DATE);
                if (modifiedStamp != null) {
                    java.util.Date updatedDate = new java.util.Date();
                    updatedDate.setTime(modifiedStamp.getTime());
                    model.updated_at = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZ").format(updatedDate);
                }
                results.add(model);
            }
            resultSet.close();
            String message = "total product: " + results.size()+ " ";
            logging(message);
            report+= message;
        }
        return results;
    }
    private List<ProductLogJson> getProducts() throws SQLException {
        List<ProductLogJson> results = new ArrayList<>();
        if (isConnected()) {
            String query = ProdukService.Q_DAFTAR_PRODUK_GENERAL + " limit 1";
            Logger.info(query);
            if (isLogged) {
                //query += " LIMIT 5000 OFFSET 6000;";
            }
            ResultSet resultSet = connection.createStatement().executeQuery(query);
            while (resultSet.next()) {
                ProductLogJson model = new ProductLogJson();
                model.id = resultSet.getLong(ID);
                String namaProduk = Parser.unescapeEntities(resultSet.getString(NAMA_PRODUK), true);
                model.nama_produk = null!=namaProduk?namaProduk.trim():namaProduk;
                model.komoditas_id = resultSet.getLong(KOMODITAS_ID);
                String namaKom = resultSet.getString("nama_komoditas").trim();
                model.nama_komoditas = null!=namaKom?namaKom.trim():namaKom;
                model.kelas_harga = resultSet.getString("kelas_harga");
                model.penyedia_id = resultSet.getLong("penyedia_id");
                String namaPenyedia = resultSet.getString(NAMA_PENYEDIA).trim();
                model.nama_penyedia = null!=namaPenyedia?namaPenyedia.trim():namaPenyedia;
                model.manufaktur_id = resultSet.getLong(MANUFAKTUR_ID);
                String nama_manufaktur = resultSet.getString(NAMA_MANUFAKTUR);
                model.nama_manufaktur = null!=nama_manufaktur?nama_manufaktur.trim():nama_manufaktur;
                model.produk_kategori_id = resultSet.getLong("produk_kategori_id");
                model.harga_utama = resultSet.getLong(HARGA_UTAMA);
                model.harga_pemerintah = resultSet.getLong(HARGA_PEMERINTAH);
                String no_produk = resultSet.getString("no_produk");
                model.no_produk = null!=no_produk?no_produk.trim():no_produk;
                String no_produk_penyedia =  resultSet.getString("no_produk_penyedia");
                model.no_produk_penyedia = null != no_produk_penyedia? no_produk_penyedia.trim():no_produk_penyedia;
                model.stock = resultSet.getDouble(JUMLAH_STOK);
                model.jenis_produk = resultSet.getString(JENIS_PRODUK);
                model.is_umkm = resultSet.getBoolean(IS_UMKM);
                java.sql.Date date = resultSet.getDate(BERLAKU_SAMPAI);
                if (date != null) {
                    model.expired = new SimpleDateFormat("yyyy-MM-dd").format(date);
                }
                Timestamp createStamp = resultSet.getTimestamp(CREATED_DATE);
                if (createStamp != null) {
                    java.util.Date createdDate = new java.util.Date();
                    createdDate.setTime(createStamp.getTime());
                    model.created_at = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZ")
                            .format(createdDate);
                }
                Timestamp modifiedStamp = resultSet.getTimestamp(MODIFIED_DATE);
                if (modifiedStamp != null) {
                    java.util.Date updatedDate = new java.util.Date();
                    updatedDate.setTime(modifiedStamp.getTime());
                    model.updated_at = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZ").format(updatedDate);
                }
                results.add(model);
            }
            resultSet.close();
            String message = "total product: " + results.size()+ " ";
            logging(message);
            report+= message;
        }
        return results;
    }

    public void logging(String message) {
        if (isLogged) {
            System.out.println(TAG + " " + message);
        }
    }

    public Map<Long, List<ProductCategoryElastic>> getCategories() throws SQLException {
        Map<Long, List<ProductCategoryElastic>> results = new HashMap<>();
        if (isConnected()) {
            Logger.debug("produk kategori sql:"+ProdukKategoriRepository.getCategoriesSql());
            ResultSet c = connection.createStatement().executeQuery(ProdukKategoriRepository.getCategoriesSql());
            while (c.next()) {
                KategoriElastic model = new KategoriElastic();
                model.currentId = c.getLong("current_id");
                model.levelOneId = c.getLong("one_id");
                model.levelOneName = c.getString("one_name");
                model.levelTwoId = c.getLong("two_id");
                model.levelTwoName = c.getString("two_name");
                model.levelThreeId = c.getLong("th_id");
                model.levelThreeName = c.getString("th_name");
                model.levelFourId = c.getLong("ft_id");
                model.levelFourName = c.getString("ft_name");
                model.levelFiveId = c.getLong("fi_id");
                model.levelFiveName = c.getString("fi_name");
                results.put(model.currentId, model.getCategories());
            }
            c.close();
            String message = "total categories: " + results.size()+ " ";
            logging(message);
            report += message;
        }
        return results;
    }

    public Map<Long, List<ProductPriceElastic>> getProductPriceProvince(String productIds) throws SQLException {
        Map<Long, List<ProductPriceElastic>> results = new HashMap<>();

        List<ProdukHarga> produkHarga = this.findProdukHargaByProdukIds(productIds);
        Map<Long, List<ProdukHarga>> grPh = produkHarga.stream()
                .collect(
                        Collectors.groupingBy(
                                x -> x.produk_id
                        )
                );

        for (Map.Entry<Long, List<ProdukHarga>> entry : grPh.entrySet()) {

            NavigableMap<String, List<ProdukHarga>> grDate = (new TreeMap(entry.getValue().stream()
                    .collect(
                            Collectors.groupingBy(
                                    x -> x.getStandarDateString()
                            )
                    ))).descendingMap();

            ProdukHarga ph_utama = grDate.firstEntry().getValue().stream().parallel().filter(it -> it.harga_utama == true).findFirst().orElse(null);
            ProdukHarga ph_pemerintah = grDate.firstEntry().getValue().stream().parallel().filter(it -> it.harga_pemerintah == true).findFirst().orElse(null);

            List<ProdukHargaJson> harga_utama = new ArrayList<>();
            List<ProdukHargaJson> harga_pemerintah = new ArrayList<>();

            if(null!=ph_utama ){
                harga_utama = new Gson().fromJson(ph_utama.harga, new TypeToken<List<ProdukHargaJson>>(){}.getType());
            }
            if(null != ph_pemerintah) {
                harga_pemerintah = new Gson().fromJson(ph_pemerintah.harga, new TypeToken<List<ProdukHargaJson>>() {
                }.getType());
            }
            JsonObject jsObj = CommonUtil.fromJson(ph_utama.harga_xls_json, JsonObject.class);

            //handling data kotor
            if(null == harga_utama){
                harga_utama = new ArrayList<>();
            }if(null == harga_pemerintah){
                harga_pemerintah = new ArrayList<>();
            }if(null == jsObj){
                jsObj = new JsonObject();
            }

            for(int i = 0; i < harga_utama.size(); i++){
                ProdukHargaJson phu = harga_utama.get(i);
                ProdukHargaJson php = harga_pemerintah.get(i);

                ProductPriceElastic price = new ProductPriceElastic();
                price.id = phu.provinsiId;
                price.type = ProductPriceElastic.TYPE_PROVINCE;
                price.hargaUtama = new BigDecimal(phu.harga).longValue();
                price.hargaPemerintah = new BigDecimal(php.harga).longValue();
                //Logger.info("json :"+ph_utama.harga_xls_json);
                //price.locationName = jsObj != null ? ((JsonObject)jsObj.getAsJsonArray("cost").get(i)).get("provinsi").getAsString() : "";
                //handle property ada yang Provinsi ada yang provinsi
                if( null != jsObj){
                    //((JsonObject)jsObj.getAsJsonArray("cost").get(i)).get("provinsi").getAsString()
                    try{
                        if(jsObj.getAsJsonArray("cost").size() > 0){
                            JsonObject jso = ((JsonObject)jsObj.getAsJsonArray("cost").get(i));
                            if(null != jso && jso.has("provinsi")){
                                price.locationName = jso.get("provinsi").getAsString();
                            }else if(null != jso && jso.has("Provinsi")){
                                price.locationName = jso.get("Provinsi").getAsString();
                            }else{
                                price.locationName = "";
                            }
                        }
                    }catch (Exception e){
                        LogUtil.d("Elastics dump",e);
                        price.locationName = "";
                    }
                }else{
                    price.locationName = "";
                }
                if (results.containsKey(entry.getKey())) {
                    results.get(entry.getKey()).add(price);
                } else {
                    List<ProductPriceElastic> list = new ArrayList<>();
                    list.add(price);
                    results.put(entry.getKey(), list);
                }
            }
        }
        logging("total province: " + results.size());
        return results;
    }

    public Map<Long, List<ProductPriceElastic>> getProductPriceRegency(String productIds) throws SQLException {
        Map<Long, List<ProductPriceElastic>> results = new HashMap<>();
        
        List<ProdukHarga> produkHarga = this.findProdukHargaByProdukIds(productIds);
        Map<Long, List<ProdukHarga>> grPh = produkHarga.stream()
        .collect(
            Collectors.groupingBy(
                x -> x.produk_id
            )
        );

        for (Map.Entry<Long, List<ProdukHarga>> entry : grPh.entrySet()) {

            NavigableMap<String, List<ProdukHarga>> grDate = (new TreeMap(entry.getValue().stream()
                    .collect(
                            Collectors.groupingBy(
                                    x -> x.getStandarDateString()
                            )
                    ))).descendingMap();
            
            ProdukHarga ph_utama = grDate.firstEntry().getValue().stream().parallel().filter(it -> it.harga_utama == true).findFirst().orElse(null);
            ProdukHarga ph_pemerintah = grDate.firstEntry().getValue().stream().parallel().filter(it -> it.harga_pemerintah == true).findFirst().orElse(null);
            List<HargaKabupatenJson> harga_utama = new ArrayList<>();
            List<HargaKabupatenJson> harga_pemerintah = new ArrayList<>();
            if(null != ph_utama){
                harga_utama = new Gson().fromJson(ph_utama.harga, new TypeToken<List<HargaKabupatenJson>>(){}.getType());
            }
            if(null != ph_pemerintah){
                harga_pemerintah = new Gson().fromJson(ph_pemerintah.harga, new TypeToken<List<HargaKabupatenJson>>(){}.getType());
            }

            JsonObject jsObj = CommonUtil.fromJson(ph_utama.harga_xls_json, JsonObject.class);

            if(null == harga_utama){
                harga_utama = new ArrayList<>();
            }if (null == harga_pemerintah){
                harga_pemerintah = new ArrayList<>();
            }if(null == jsObj){
                jsObj = new JsonObject();
            }

            for(int i = 0; i < harga_utama.size(); i++){
                HargaKabupatenJson hkj = harga_utama.get(i);
                HargaKabupatenJson hkjp = harga_pemerintah.get(i);

                ProductPriceElastic price = new ProductPriceElastic();
                price.id = hkj.kabupatenId;
                price.type = ProductPriceElastic.TYPE_REGENCY;
                price.hargaUtama = new BigDecimal(hkj.harga).longValue();
                price.hargaPemerintah = new BigDecimal(hkjp.harga).longValue();
                try {
                    price.locationName = jsObj != null ? ((JsonObject) (jsObj.getAsJsonArray("cost") == null ?  jsObj.getAsJsonArray("price") :  jsObj.getAsJsonArray("cost")).get(i)).get("Kabupaten").getAsString() : "";
                }catch(Exception e){
                }
                if (results.containsKey(entry.getKey())) {
                    results.get(entry.getKey()).add(price);
                } else {
                    List<ProductPriceElastic> list = new ArrayList<>();
                    list.add(price);
                    results.put(entry.getKey(), list);
                }
            }
        }

        logging("total regency: " + results.size());
        return results;
    }

    public  List<ProdukHarga> findProdukHargaByProdukIds(String produkIds) throws SQLException {
        String query =
                "Select ph.id, ph.produk_id, ph.tanggal_dibuat, ph.komoditas_harga_atribut_id, ph.harga, ph.harga_xls_json, kha.apakah_harga_utama harga_utama, kha.apakah_harga_pemerintah harga_pemerintah " +
                    "from produk_harga ph " +
                    "join komoditas_harga_atribut kha on ph.komoditas_harga_atribut_id = kha.id and (kha.apakah_harga_utama = 1 or kha.apakah_harga_pemerintah = 1) " +
                    "where ph.active = 1 and ph.produk_id in (" + (produkIds.isEmpty() ? "0" : produkIds) + ")";
        List<ProdukHarga> result = new ArrayList<>();
        if(!produkIds.isEmpty()){
            if(isConnected()) {
                ResultSet c = connection.createStatement().executeQuery(query);
                int line = 0;
                while (c.next()) {
                    line++;
                    ProdukHarga rec = new ProdukHarga();
                    rec.id = c.getLong("id");
                    rec.produk_id = c.getLong("produk_id");
                    rec.tanggal_dibuat = c.getDate("tanggal_dibuat");
                    rec.komoditas_harga_atribut_id = c.getLong("komoditas_harga_atribut_id");
                    rec.harga = c.getString("harga");
                    rec.harga_xls_json = c.getString("harga_xls_json");
                    rec.harga_utama = c.getBoolean("harga_utama");
                    rec.harga_pemerintah = c.getBoolean("harga_pemerintah");
                    result.add(rec);
                }
                c.close();
            }
        }


        return result;
    }

    public Map<Long, Long> getTotalPurchased(String ids) throws SQLException {
        final String query = "SELECT SUM(pp.kuantitas) as total, pp.produk_id" +
                " FROM paket_produk pp" +
                " JOIN paket p" +
                " ON p.id = pp.paket_id" +
                " WHERE p.active > 0 AND pp.active > 0" +
                " AND pp.produk_id IN ("+ids+")" +
                " GROUP BY pp.produk_id";
        Map<Long, Long> results = new HashMap<>();
        try{
            if (isConnected()) {
                ResultSet c = connection.createStatement().executeQuery(query);
                while (c.next()) {
                    Long produkId = c.getLong("produk_id");
                    Long total = c.getLong("total");
                    results.put(produkId, total);
                }
                c.close();
            }
        }catch (Exception e){
            Logger.info("error getTotalPurchased:"+e.getMessage());e.printStackTrace();
        }
        return results;
    }

    private boolean isConnected() throws SQLException {
        return connection != null && !connection.isClosed();
    }

    public void process() throws Exception {
        preparation();
        System.out.println("working on it");
        final long begin = System.currentTimeMillis();
        List<ProductLogJson> products = getProducts();
        ElasticConnection elasticConnection = new ElasticConnection(isLogged);
        if (!products.isEmpty() && products.size() > 0) {
            //elasticConnection.delete();
            elasticConnection.indexing(new File(indexFilePath));
//            Logger.debug(TAG + "total: " + products.size());
            Gson gson = new Gson();
            final int perPage = 1000;
            final int size = products.size();
            Map<Long, List<ProductCategoryElastic>> categories = getCategories();
            for (int start = 0; start < size; start += perPage) {
                int end = Math.min(start + perPage, size);
                //all_send
                StringBuilder sb = new StringBuilder();
                List<ProductLogJson> part = products.subList(start, end);
                final String productIds = getProductIds(part);
                final String productProvinsiIds = getProductAreaIds(part, "provinsi");
                final String productRegencyIds = getProductAreaIds(part, "kabupaten");
                Logger.info("provinsi ID:"+productProvinsiIds);
                Map<Long, List<ProductPriceElastic>> province = getProductPriceProvince(productProvinsiIds);
                Map<Long, List<ProductPriceElastic>> regencies = getProductPriceRegency(productRegencyIds);
                Map<Long, ProdukGambar> images = getProductImage(productIds);
                Map<Long, Long> totals = getTotalPurchased(productIds);
                for (ProductLogJson model : part) {
                    //single send
                    //StringBuilder sb = new StringBuilder();
                    model.total_pembelian = totals.getOrDefault(model.id, 0L);
                    if (model.isAllowedToSetGeneralPrice()) {
                        model.priceList.add(new ProductPriceElastic(model));
                    }
                    if (province != null && province.containsKey(model.id)) {
                        model.priceList.addAll(province.get(model.id));
                    }
                    if (regencies != null && regencies.containsKey(model.id)) {
                        model.priceList.addAll(regencies.get(model.id));
                    }
                    if (categories.containsKey(model.produk_kategori_id)) {
                        model.categories.addAll(categories.get(model.produk_kategori_id));
                        model.setDefaultCategoryPosition();
                    }
                    if (images.containsKey(model.id)) {
                        model.image_url = images.get(model.id).getActiveUrl();
                    }
                    model.timestamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZ")
                            .format(new java.util.Date());
                    if (!model.priceList.isEmpty()) {
                       // logging(gson.toJson(model));
                        sb.append("{ \"create\":  { \"_index\": \""+ elasticsearchIndex +"\", \"_type\": \"produk\", \"_id\": \"")
                                .append(model.id).append("\" }}");
                        sb.append("\n").append(gson.toJson(model)).append("\n");
                        //writeToFile(result);
                        //single send
                        //elasticConnection.bulkSend(sb.toString());
                        //Logger.info("send data single:"+sb.toString());
                    }
                }
                //all send
                elasticConnection.bulkSend(sb.toString());
                Logger.info("send data perpage+"+sb.toString());
            }
        }
        if (isConnected()) {
            connection.close();
        }
        final long finish = System.currentTimeMillis();
        System.out.println("done Update elastics, required: " + (TimeUnit.MILLISECONDS.toSeconds(finish - begin)) + " seconds");

        //see KatalogApiCtr.formProdukSubmit
        if( Cache.class == null){
            Cache.set("end_jobp",new Date());
        }
    }

    public void processWithJob() throws Exception {
        preparation();
        System.out.println("working on it");
        Cache.set("msg_jobp","get all produk on db");
        final long begin = System.currentTimeMillis();
        List<ProductLogJson> products = getProducts();
        Cache.set("msg_jobp","total produk:"+products.size());
        ElasticConnection elasticConnection = new ElasticConnection(isLogged);
        if (!products.isEmpty() && products.size() > 0) {
            elasticConnection.delete();
            elasticConnection.indexing(new File(indexFilePath));
            Logger.debug(TAG + "total: " + products.size());
            Cache.set("msg_jobp","delete index, and set model done");
            Gson gson = new Gson();
            final int perPage = 1000;
            final int size = products.size();
            Map<Long, List<ProductCategoryElastic>> categories = getCategories();
            int delaySecond = 1;
            for (int start = 0; start < size; start += perPage) {
                int end = Math.min(start + perPage, size);
                Cache.set("msg_jobp","proses produk from :" +start+" to "+end);
                //all_send
                StringBuilder sb = new StringBuilder();
                List<ProductLogJson> part = products.subList(start, end);
                final String productIds = getProductIds(part);
//                Cache.set("msg_jobp","get produk provinsi, regency ... " );
                final String productProvinsiIds = getProductAreaIds(part, "provinsi");
                final String productRegencyIds = getProductAreaIds(part, "kabupaten");

                Map<Long, List<ProductPriceElastic>> province = getProductPriceProvince(productProvinsiIds);
                Map<Long, List<ProductPriceElastic>> regencies = getProductPriceRegency(productRegencyIds);
                Map<Long, ProdukGambar> images = getProductImage(productIds);

//                Cache.set("msg_jobp","get produk images, and collect data ... " );
                Map<Long, Long> totals = getTotalPurchased(productIds);
                for (ProductLogJson model : part) {
                    //single send
                    //StringBuilder sb = new StringBuilder();
                    model.total_pembelian = totals.getOrDefault(model.id, 0L);
                    if (model.isAllowedToSetGeneralPrice()) {
                        model.priceList.add(new ProductPriceElastic(model));
                    }
                    if (province != null && province.containsKey(model.id)) {
                        model.priceList.addAll(province.get(model.id));
                    }
                    if (regencies != null && regencies.containsKey(model.id)) {
                        model.priceList.addAll(regencies.get(model.id));
                    }
                    if (categories.containsKey(model.produk_kategori_id)) {
                        model.categories.addAll(categories.get(model.produk_kategori_id));
                        model.setDefaultCategoryPosition();
                    }
                    if (images.containsKey(model.id)) {
                        model.image_url = images.get(model.id).getActiveUrl();
                    }
                    model.timestamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZ")
                            .format(new java.util.Date());
                    if (!model.priceList.isEmpty()) {
                        // logging(gson.toJson(model));
                        sb.append("{ \"create\":  { \"_index\": \""+elasticsearchIndex +"\", \"_type\": \"produk\", \"_id\": \"")
                                .append(model.id).append("\" }}");
                        sb.append("\n").append(gson.toJson(model)).append("\n");
                        //writeToFile(result);
                        //single send
                        //elasticConnection.bulkSend(sb.toString());
                    }
                    System.out.println(sb.toString());
                }
                //all send
                //elasticConnection.bulkSend(sb.toString());
//                Cache.set("msg_jobp","send to elastic "+ start +" end"+end );
                SingleJobSendElastics job1 = new SingleJobSendElastics(sb.toString(), part.size());
                job1.now();
                Cache.set("msg_jobp","sending data to elasticsearch");
//                new Job(){
//                    //UUID idjobs = UUID.randomUUID();
//                    String _sb = sb.toString();
//                    ElasticConnection _elasticConnection = elasticConnection;
//                    public void doJob(){
//                        _elasticConnection.bulkSend(_sb);
//                    }
//                }.now();

//                new Job(){
//                    public void doJob(){
//                        try{
//                            Cache.delete(ElasticsearchRepository.POPULAR_CACHE_KEY);
//                            Cache.delete(ElasticsearchRepository.RECOMMENDATION_CACHE_KEY);
//
//                        }catch (Exception e){
//                            try{
//                                Cache.safeDelete(ElasticsearchRepository.POPULAR_CACHE_KEY);
//                                Cache.safeDelete(ElasticsearchRepository.RECOMMENDATION_CACHE_KEY);
//                            }catch (Exception ex){}
//                        }
//                    }
//                }.now();
                //ganti karena delay terlalu lama
                //.in(delaySecond++);
            }
        }
        if (isConnected()) {
            connection.close();
        }
        final long finish = System.currentTimeMillis();
        Logger.info("done Update elastics, required: " + (TimeUnit.MILLISECONDS.toSeconds(finish - begin)) + " seconds");

        //see KatalogApiCtr.formProdukSubmit
        Cache.set("end_jobp",new Date());
    }
    private String getProductIds(List<ProductLogJson> list) {
        if (!list.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            String glue = "";
            for (ProductLogJson model : list) {
                sb.append(glue).append("'").append(model.id).append("'");
                glue = ",";
            }
            return sb.toString();
        }
        return "";
    }

    private String getProductAreaIds(List<ProductLogJson> list, String area) {
        if (!list.isEmpty()) {
            List<String> listIds = list.stream().parallel().filter(x -> x.kelas_harga.equals(area)).map(ProductLogJson::getIdToString).collect(Collectors.toList());
            return String.join(",", listIds);
        }
        return "";
    }

    private Map<Long, ProdukGambar> getProductImage(String ids) throws SQLException {
        Map<Long, ProdukGambar> results = new HashMap<>();
        if (isConnected()) {
            ResultSet c = connection.createStatement()
                    .executeQuery("SELECT " +
                            " pg.produk_id" +
                            ", pg.dok_id_attachment" +
                            ", pg.file_name"+
                            ", pg.file_sub_location"+
                            ", pg.file_url"+
                            " FROM produk_gambar pg" +
                            " LEFT JOIN blob_table bb" +
                            " ON bb.blb_id_content = pg.dok_id_attachment" +
                            " WHERE pg.produk_id IN ("+ids+")" +
                            " AND pg.active > 0" +
                            " GROUP BY pg.produk_id"+
                            " ORDER BY pg.created_date ASC");
            StringBuilder sb = new StringBuilder();
            String glue = "";
            while (c.next()) {
                ProdukGambar model = new ProdukGambar();
                model.produk_id = c.getLong("produk_id");
                model.dok_id_attachment = c.getLong("dok_id_attachment");
                model.file_name = c.getString("file_name");
                model.file_sub_location = c.getString("file_sub_location");
                model.file_url = c.getString("file_url");
                results.put(model.produk_id, model);
                sb.append(glue).append("'").append(model.dok_id_attachment).append("'");
                glue = ",";
            }
            c.close();
            logging("total image: " + results.size());
        }
        return results;
    }

    public File writeToFile(String json) {
        File file = new File(filePath);
        try {
            if (!file.exists()) {
                if (file.createNewFile()) {
                    Logger.debug(TAG + " new file's created");
                }
            }
            FileWriter fileWriter = new FileWriter(file, true);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            writer.append(json);
            writer.append("\n");
            writer.flush();
            writer.close();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }
    public void createDmp() throws Exception{
        preparation();
        System.out.println("working on dump");
        final long begin = System.currentTimeMillis();
        List<ProductLogJson> products = getProducts();
        ElasticConnection elasticConnection = new ElasticConnection(isLogged);
        if (!products.isEmpty() && products.size() > 0) {
            elasticConnection.delete();
            elasticConnection.indexing(new File(indexFilePath));
            Logger.debug(TAG + "total: " + products.size());
            Gson gson = new Gson();
            final int perPage = 1000;
            final int size = products.size();
            StringBuilder sb = new StringBuilder();
            Map<Long, List<ProductCategoryElastic>> categories = getCategories();
            for (int start = 0; start < size; start += perPage) {
                int end = Math.min(start + perPage, size);

                List<ProductLogJson> part = products.subList(start, end);
                final String productIds = getProductIds(part);
                final String productProvinsiIds = getProductAreaIds(part, "provinsi");
                final String productRegencyIds = getProductAreaIds(part, "kabupaten");
                Map<Long, List<ProductPriceElastic>> province = getProductPriceProvince(productProvinsiIds);
                Map<Long, List<ProductPriceElastic>> regencies = getProductPriceRegency(productRegencyIds);
                Map<Long, ProdukGambar> images = getProductImage(productIds);
                Map<Long, Long> totals = getTotalPurchased(productIds);
                for (ProductLogJson model : part) {
                    Logger.info("on loop data:");
                    model.total_pembelian = totals.getOrDefault(model.id, 0L);
                    if (model.isAllowedToSetGeneralPrice()) {
                        model.priceList.add(new ProductPriceElastic(model));
                    }
                    if (province != null && province.containsKey(model.id)) {
                        model.priceList.addAll(province.get(model.id));
                    }
                    if (regencies != null && regencies.containsKey(model.id)) {
                        model.priceList.addAll(regencies.get(model.id));
                    }
                    if (categories.containsKey(model.produk_kategori_id)) {
                        model.categories.addAll(categories.get(model.produk_kategori_id));
                        model.setDefaultCategoryPosition();
                    }
                    if (images.containsKey(model.id)) {
                        model.image_url = images.get(model.id).getActiveUrl();
                    }
                    model.timestamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZ")
                            .format(new java.util.Date());
                    if (!model.priceList.isEmpty()) {
                        // logging(gson.toJson(model));
                        sb.append("{ \"create\":  { \"_index\": \""+elasticsearchIndex+"\", \"_type\": \"produk\", \"_id\": \"")
                                .append(model.id).append("\" }}");
                        sb.append("\n");
                        sb.append(gson.toJson(model));
                        sb.append("\n");
                        System.out.println(sb.toString());
                        //writeToFile(sb.toString());
                        elasticConnection.bulkSend(sb.toString());
                    }
                }
                //System.out.println(sb.toString());
                //writeToFile(sb.toString());
                elasticConnection.bulkSend(sb.toString());
            }
        }
    }
    private ProductLogJson getProductById(Long idp) throws SQLException, Exception {
        ProductLogJson result = new ProductLogJson();
        if(!isConnected()){
            preparation();
        }
        String query = ProdukService.Q_DAFTAR_PRODUK_GENERAL + " AND p.ID =" + idp.toString();
        Logger.debug("Q_DAFTAR_PRODUK_GENERAL with idproduk: "+ query);
        if (isConnected()) {
            ResultSet resultSet = connection.createStatement().executeQuery(query);
            while (resultSet.next()) {
                ProductLogJson model = new ProductLogJson();
                model.id = resultSet.getLong(ID);
                model.nama_produk = Parser.unescapeEntities(resultSet.getString(NAMA_PRODUK), true);
                model.komoditas_id = resultSet.getLong(KOMODITAS_ID);
                model.nama_komoditas = resultSet.getString("nama_komoditas");
                model.kelas_harga = resultSet.getString("kelas_harga");
                model.penyedia_id = resultSet.getLong("penyedia_id");
                model.nama_penyedia = resultSet.getString(NAMA_PENYEDIA);
                model.manufaktur_id = resultSet.getLong(MANUFAKTUR_ID);
                model.nama_manufaktur = resultSet.getString(NAMA_MANUFAKTUR);
                model.produk_kategori_id = resultSet.getLong("produk_kategori_id");
                model.harga_utama = resultSet.getLong(HARGA_UTAMA);
                model.harga_pemerintah = resultSet.getLong(HARGA_PEMERINTAH);
                model.no_produk = resultSet.getString("no_produk");
                model.no_produk_penyedia = resultSet.getString("no_produk_penyedia");
                model.stock = resultSet.getDouble(JUMLAH_STOK);
                model.jenis_produk = resultSet.getString(JENIS_PRODUK);
                model.produk_api_url= resultSet.getString("produk_api_url");
                model.is_umkm = resultSet.getBoolean(IS_UMKM);
                java.sql.Date date = resultSet.getDate(BERLAKU_SAMPAI);
                if (date != null) {
                    model.expired = new SimpleDateFormat("yyyy-MM-dd").format(date);
                }
                Timestamp createStamp = resultSet.getTimestamp(CREATED_DATE);
                if (createStamp != null) {
                    java.util.Date createdDate = new java.util.Date();
                    createdDate.setTime(createStamp.getTime());
                    model.created_at = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZ")
                            .format(createdDate);
                }
                Timestamp modifiedStamp = resultSet.getTimestamp(MODIFIED_DATE);
                if (modifiedStamp != null) {
                    java.util.Date updatedDate = new java.util.Date();
                    updatedDate.setTime(modifiedStamp.getTime());
                    model.updated_at = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZ").format(updatedDate);
                }
                result=model;
            }
        }
        return result;
    }

    public void addRemoveProduk(Long ids) throws Exception {
        List<ProductLogJson> part = new ArrayList<>();
        ProductLogJson prd = getProductById(ids);
        if(prd!=null){
            part.add(prd);
        }
        executeToElastics(part, true);
    }

    public void addRemoveByKomoditas(Long komoditas_id, boolean useLocal, boolean pisahKoneksi) throws Exception {
        List<ProductLogJson> part = new ArrayList<>();
        if(pisahKoneksi){
            preparation();
        }
        part = getProductbyKomoditas(komoditas_id);

        executeToElastics(part, useLocal);
    }
    private void executeToElastics(List<ProductLogJson> products, boolean useLocal) throws SQLException {
        Map<Long, List<ProductCategoryElastic>> categories = getCategories();
        StringBuilder sbAdd = new StringBuilder();
        StringBuilder sbDell = new StringBuilder();

        final int perPage = 25;
        final int size = products.size();
        String productProvinsiIds = "";
        String productIds = "";
        String productRegencyIds = "";
        Map<Long, List<ProductPriceElastic>> province = new HashMap<>();
        Map<Long, List<ProductPriceElastic>> regencies = new HashMap<>();
        Map<Long, ProdukGambar> images = new HashMap<>();
        Map<Long, Long> totals = new HashMap<>();
        String message = "";
        for (int start = 0; start < size; start += perPage) {
            int end = Math.min(start + perPage, size);
            List<ProductLogJson> part = products.subList(start, end);
            productIds = getProductIds(products);
            productProvinsiIds = getProductAreaIds(products, "provinsi");
            productRegencyIds = getProductAreaIds(products, "kabupaten");
            province = getProductPriceProvince(productProvinsiIds);
            regencies = getProductPriceRegency(productRegencyIds);
            images = getProductImage(productIds);
            totals = getTotalPurchased(productIds);
            Gson gson = new Gson();
            ElasticConnection elasticConnection = new ElasticConnection(isLogged);
            for (ProductLogJson model : part) {
                Logger.info("on loop data:" +start +"-"+end);
                model.total_pembelian = totals.getOrDefault(model.id, 0L);
                if (model.isAllowedToSetGeneralPrice()) {
                    model.priceList.add(new ProductPriceElastic(model));
                }
                if (province != null && province.containsKey(model.id)) {
                    model.priceList.addAll(province.get(model.id));
                }
                if (regencies != null && regencies.containsKey(model.id)) {
                    model.priceList.addAll(regencies.get(model.id));
                }
                if (categories.containsKey(model.produk_kategori_id)) {
                    model.categories.addAll(categories.get(model.produk_kategori_id));
                    model.setDefaultCategoryPosition();
                }
                if (images.containsKey(model.id)) {
                    model.image_url = images.get(model.id).getActiveUrl();
                    if(useLocal){
                        model.image_url = model.image_url.replace(Config.getInstance().cdnServerImage300, "");
                    }

                }
                model.timestamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZ")
                        .format(new java.util.Date());
                if (!model.priceList.isEmpty()) {
                    // logging(gson.toJson(model));
                    sbAdd.append("{ \"create\":  { \"_index\": \""+elasticsearchIndex+"\", \"_type\": \"produk\", \"_id\": \"")
                            .append(model.id).append("\" }}");
                    sbAdd.append("\n");
                    sbAdd.append(gson.toJson(model));
                    sbAdd.append("\n");

                    sbDell.append("{ \"delete\":  { \"_index\": \""+ elasticsearchIndex +"\", \"_type\": \"produk\", \"_id\": \"")
                            .append(model.id).append("\" }}").append("\n");
                    //ElasticConnection elasticConnection = new ElasticConnection(isLogged);
                    //writeToFile(sb.toString());
                    Boolean isDeleted = false;
                    //try{
                    isDeleted = elasticConnection.bulkDeleteProduk(sbDell.toString());
                    //}catch (Exception e){}
                    //if(isDeleted){
                    //elasticConnection.bulkSend(sbAdd.toString());
                    //}
                    message +="nama produk:"+model.nama_produk+"("+model.id+"), ditayangkan; ";
                }else{
                    message +="nama produk:"+model.nama_produk+"("+model.id+"), gagal ditayangkan; ";
                }
            }
            elasticConnection.bulkSend(sbAdd.toString());
            sbAdd = new StringBuilder();
        }
        try{
            Scope.Flash.current().success(message);
        }catch (Exception e){}
    }


    public void createJobUpdateCrawl(Long idp){
        new Job(){

            public void doJob(){
                Logger.debug("populate from url");
                new ExtProduct().executeSingle(idp);

            }
            public void after(){
                Logger.info("after job 1st get url");
                new TrackableJob(){
                    public void doJob(){
                        ElasticsearchProdukAdd obj = new ElasticsearchProdukAdd();
                        try {
                            obj.preparation();
                            obj.addRemoveProduk(idp);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.in("5mn");//
            }
        }.in(3);//3seconds
    }

    public void createJobUpdateElastic(Long idp){
        new Job(){
            public void doJob(){
                ElasticsearchProdukAdd obj = new ElasticsearchProdukAdd();
                try {
                    obj.preparation();
                    obj.addRemoveProduk(idp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.now();
        new Job(){
            public void doJob(){
                try{
                    Cache.delete(ElasticsearchRepository.POPULAR_CACHE_KEY);
                    Cache.delete(ElasticsearchRepository.RECOMMENDATION_CACHE_KEY);

                }catch (Exception e){
                    try{
                        Cache.safeDelete(ElasticsearchRepository.POPULAR_CACHE_KEY);
                        Cache.safeDelete(ElasticsearchRepository.RECOMMENDATION_CACHE_KEY);
                    }catch (Exception ex){}
                }
            }
        }.now();

    }

    //ids is id produk
    public boolean deleteProdukbyId(Long ids){
        StringBuilder sbDell = new StringBuilder();
        Boolean isDeleted = false;

        try{
            sbDell.append("{ \"delete\":  { \"_index\": \""+ elasticsearchIndex +"\", \"_type\": \"produk\", \"_id\": \"")
                    .append(ids).append("\" }}").append("\n");
            ElasticConnection elasticConnection = new ElasticConnection(true);

            isDeleted = elasticConnection.bulkDeleteProduk(sbDell.toString());
        }catch (Exception e){
            Logger.info("error send delete ke elastics:"+e.getMessage());
        }

        new Job(){
            public void doJob(){
                try{
                    Cache.delete(ElasticsearchRepository.POPULAR_CACHE_KEY);
                    Cache.delete(ElasticsearchRepository.RECOMMENDATION_CACHE_KEY);

                }catch (Exception e){
                    try{
                        Cache.safeDelete(ElasticsearchRepository.POPULAR_CACHE_KEY);
                        Cache.safeDelete(ElasticsearchRepository.RECOMMENDATION_CACHE_KEY);
                    }catch (Exception ex){}
                }
            }
        }.now();
        return isDeleted;
    }
}
