package models.cms;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.List;

@Table(name = "konten_kategori_utama")
public class KategoriUtama extends BaseKatalogTable {
    @Id
    public Long id;
    public String nama_kategori_utama;
    public String deskripsi;
    public boolean active;

    public static List<KategoriUtama> findAllActive(){
        return KategoriUtama.find("active = 1").fetch();
    }

    public void setAktif() {
        this.active = true;
        prePersist();
        save();
    }

    public void setNonAktif() {
        this.active = false;
        prePersist();
        save();
    }
}
