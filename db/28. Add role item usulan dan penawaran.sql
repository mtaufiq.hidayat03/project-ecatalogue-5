INSERT INTO role_item (role_grup_id, nama_role, deskripsi, active, created_date) VALUES
  ((select id from role_grup where nama_grup = "Usulan"), "comUsulan", "Akses usulan", 1, CURDATE()),
  ((select id from role_grup where nama_grup = "Usulan"), "comUsulanAdd", "Tambah Penawaran", 1, CURDATE()),
  ((select id from role_grup where nama_grup = "Usulan"), "comUsulanEdit", "Edit usulan", 1, CURDATE()),
  ((select id from role_grup where nama_grup = "Usulan"), "comUsulanDelete", "Delete usulan", 1, CURDATE()),

  ((select id from role_grup where nama_grup = "Penawaran"), "comPenawaran", "Akses penawaran", 1, CURDATE()),
  ((select id from role_grup where nama_grup = "Penawaran"), "comPenawaranAdd", "Tambah penawaran", 1, CURDATE()),
  ((select id from role_grup where nama_grup = "Penawaran"), "comPenawaranEdit", "Edit penawaran", 1, CURDATE()),
  ((select id from role_grup where nama_grup = "Penawaran"), "comPenawaranDelete", "Delete penawaran", 1, CURDATE()),
  ((select id from role_grup where nama_grup = "Penawaran"), "comPenawaranBuka", "Buka penawaran", 1, CURDATE()),
  ((select id from role_grup where nama_grup = "Penawaran"), "comPenawaranListTemplateJadwal", "List template jadwal penawaran", 1, CURDATE()),
  ((select id from role_grup where nama_grup = "Penawaran"), "comPenawaranTemplateJadwalAdd", "Tambah template jadwal penawaran", 1, CURDATE()),
  ((select id from role_grup where nama_grup = "Penawaran"), "comPenawaranTemplateJadwalEdit", "Edit template jadwal penawaran", 1, CURDATE()),
  ((select id from role_grup where nama_grup = "Penawaran"), "comPenawaranTemplateJadwalDelete", "Delete template jadwal penawaran", 1, CURDATE()),
  ((select id from role_grup where nama_grup = "Penawaran"), "comPenawaranProdukList", "Akses daftar produk penawaran", 1, CURDATE()),
  ((select id from role_grup where nama_grup = "Penawaran"), "comPenawaranProdukAdd", "Tambah produk penawaran", 1, CURDATE()),
  ((select id from role_grup where nama_grup = "Penawaran"), "comPenawaranApproval", "Approval penawaran", 1, CURDATE()),

  ((select id from role_grup where nama_grup = "Penawaran"), "comCetakDokumenBaNegosiasi", "Cetak BA Negosiasi", 1, CURDATE()),
  ((select id from role_grup where nama_grup = "Penawaran"), "comCetakDokumenBAEvaluasi", "Cetak BA Evaluasi", 1, CURDATE()),
  ((select id from role_grup where nama_grup = "Penawaran"), "comCetakDokumenSkPenetapanProduk", "Cetak Dokumen SK Penetapan Produk", 1, CURDATE()),

  ((select id from role_grup where nama_grup = "Penawaran"), "comVerifikasiKualifikasiPenyedia", "Verifikasi kualifikasi penyedia", 1, CURDATE())