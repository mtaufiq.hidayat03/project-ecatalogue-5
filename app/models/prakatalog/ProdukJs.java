package models.prakatalog;

import com.google.gson.annotations.Expose;
import ext.DateBinder;
import models.common.OngkosKirim;
import play.data.binding.As;
import play.db.jdbc.Id;

import javax.persistence.Transient;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public class ProdukJs {

    @Id
    public Long id;
    public Integer komoditas_id;
    public Integer produk_kategori_id;
    public Integer penawaran_id;
    public Long penyedia_id;
    public Long manufaktur_id;
    public Long unspsc_id;
    public Long unit_pengukuran_id;
    public String no_produk;
    public String no_produk_penyedia;
    public String nama_produk;
    public String jenis_produk;
    public String url_produk;
    public BigDecimal jumlah_stok = new BigDecimal(0);
    public String tkdn_produk;

    @Transient
    public Integer jumlah_stok_form = 0;
    @Transient
    public Integer jumlah_stok_inden_form = 0;

    public BigInteger jumlah_stok_inden = BigInteger.valueOf(0);
    public boolean minta_disetujui;
    public Long minta_disetujui_oleh;
    public String minta_disetujui_tanggal;
    public String minta_disetujui_alasan;
    public String setuju_tolak;
    public String setuju_tolak_tanggal;
    public Long setuju_tolak_oleh;
    public String setuju_tolak_alasan;
    public BigDecimal harga_utama;
    public BigDecimal harga_ongkir;

//    @Expose()
    public String ongkir;

    public String harga_tanggal;
    public Long harga_kurs_id;
    public Double margin_harga;
    public Integer agr_total_gambar;
    public Integer agr_total_lampiran;
    public Integer agr_total_wilayah_jual;
    public boolean apakah_dapat_dibeli;
    public String spesifikasi;
    public boolean active;

    //@As(binder = DateBinder.class)
   // public Date berlaku_sampai;

    public String status;

    public String image_800x800;

    public Integer agr_total_dibeli;

//	public boolean editable;

    public String edit_able;

    public boolean apakah_ditayangkan;

    public Long kontrak_id;

    public String produk_gambar_file_name;
    public String produk_gambar_file_sub_location;
    public String produk_gambar_file_url;
}
