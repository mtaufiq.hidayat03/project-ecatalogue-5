package constants.models;

import constants.models.BaseConstant;

/**
 * this class contains static field related to the fields inside produk_kategori_atribut table*/
public class ProdukKategoriAtributConstant extends BaseConstant {

	public static final String TABLE_NAME = "produk_kategori_atribut";

	public static final String ID = "id";
	public static final String LABEL_ATRIBUT = "label_atribut";
	public static final String ATRIBUTE_TYPE_ID = "komoditas_produk_atribut_tipe_id";
	public static final String ATRIBUTE_TYPE_LABEL = "komoditas_produk_atribut_tipe_label";
	public static final String PRODUK_KATEGORI_ID = "produk_kategori_id";
	public static final String PARENT_ID = "parent_id";
	public static final String WAJIB_DIISI = "wajib_diisi";
	public static final String POSISI_ITEM = "posisi_item";
	public static final String TIPE_INPUT = "tipe_input";
	public static final String DESKRIPSI = "deskripsi";


}
