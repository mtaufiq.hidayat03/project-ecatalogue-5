package models.purchasing.search;

import ext.contracts.ParamAble;
import org.apache.http.message.BasicNameValuePair;
import play.mvc.Scope;

import java.util.ArrayList;
import java.util.List;

public class DistributorSearch implements ParamAble {
    public String keyword = "";
    public long pktid = 0;
    public long pid = 0;
    public int page = 0;

    public DistributorSearch(Scope.Params params) {
        this.keyword = !isNull(params,"keyword") ? params.get("keyword") : "";
        this.pktid = !isNull(params, "pktid") ? params.get("pktid", Long.class) : 0;
        this.pid = !isNull(params, "pid") ? params.get("pid", Long.class) : 0;
        this.page = getCurrentPage(params);
    }

    @Override
    public List<BasicNameValuePair> getParams() {
        List<BasicNameValuePair> results = new ArrayList<>();
        results.add(new BasicNameValuePair("keyword", this.keyword));
        results.add(new BasicNameValuePair("pktid", String.valueOf(this.pktid)));
        results.add(new BasicNameValuePair("pid", String.valueOf(this.pid)));
        return results;
    }

    @Override
    public int getPage() {
        return page;
    }
}
