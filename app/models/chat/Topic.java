package models.chat;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.PersistenceUnit;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**Penamaan kolom ikut konvensi di e-katalog
 * @author Andik Yulianto<andikyulianto@yahoo.com>
 */
@Table(name="topic", schema="chat")
@PersistenceUnit(name="logAndChat")
public class Topic extends BaseTable implements Serializable {
	@Id
	public Long id;
	public String judul;
	@javax.persistence.Transient //tidak save di DB
	//harus pakai Synchronized List
	private List<Chat> chats= new ArrayList<>();
	
//	public synchronized void addChat(Chat chat)
//	{
//		chat.tanggal=new Date();
//		chat.id=RandomUtils.nextLong();
//		chats.add(chat);
//		//add, save in other thread
//		/*JIka diperlukan, simpan juga chat ini di file txt untuk jaga-jaga jika sampai gagal save ke DB
//		 */
//		new Job()
//		{
//			public void doJob()
//			{
//				chat.save();
//			}
//		}.now();
//	}
	
	/**Untuk menghindari: Exception raised was ConcurrentModificationException : 
	 * @return
	 */
	public List<Chat> getChats()
	{
		List<Chat> chatsCopy=new ArrayList<>();
		chatsCopy.addAll(chats);
		return chatsCopy;
	}
	
	public void postLoad()
	{
		chats=Chat.find("topic_id=? ORDER BY tanggal", id).fetch();
		chats=  new Vector(chats);
	}
}
