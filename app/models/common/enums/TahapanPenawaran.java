package models.common.enums;

public enum TahapanPenawaran
{
    KLARIFIKASI("Klarifikasi"),
    NEGOSIASI("Negosiasi");

    private String label;

    TahapanPenawaran(String label) {
        this.label = label;
    }

    public String label() {
        return label;
    }
}