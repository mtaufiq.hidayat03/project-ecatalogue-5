package models.elasticsearch.product;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import models.katalog.Produk;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author HanusaCloud on 10/14/2017
 */
public class ViewHitCounter {

    @SerializedName("product_id")
    public long productId;
    @SerializedName("created_at")
    public String createdAt;
    @SerializedName("owned_by")
    public long owner;
    @SerializedName("visited_by")
    public Long visitedBy = 0L;

    public ViewHitCounter(Produk model) {
        this.productId = model.id;
        this.owner = model.penyedia_id;
        setCreatedAt();
    }

    public ViewHitCounter(ProductElastic model) {
        this.productId = model.id;
        this.owner = model.providerId;
        setCreatedAt();
    }

    public ViewHitCounter setVisitedBy(Long userId) {
        this.visitedBy = userId;
        return this;
    }

    public void setCreatedAt() {
        this.createdAt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }

    public String generatePayload() {
        final String index = "{ \"index\":  { \"_index\": \"ecatalogue-view-counter\", \"_type\": \"produk-counter\" }}\n";
        return index + new Gson().toJson(this) + "\n";
    }

}
