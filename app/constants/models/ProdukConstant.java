package constants.models;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class ProdukConstant {

    public static final String TABLE_NAME = "produk";

    public static final String ID = "id";
    public static final String NAMA_PRODUK = "nama_produk";
    public static final String KOMODITAS_ID = "komoditas_id";
    public static final String HARGA_UTAMA = "harga_utama";
    public static final String NO_PRODUK = "no_produk";
    public static final String HARGA_PEMERINTAH = "harga_pemerintah";
    public static final String BERLAKU_SAMPAI = "berlaku_sampai";
    public static final String JENIS_PRODUK = "jenis_produk";
    public static final String MANUFAKTUR_ID = "manufaktur_id";
    public static final String NAMA_MANUFAKTUR = "nama_manufaktur";
    public static final String NAMA_PENYEDIA = "nama_Penyedia";
    public static final String JUMLAH_STOK = "jumlah_stok";
    public static final String IS_UMKM = "is_umkm";

    public static final String CREATED_DATE = "created_date";
    public static final String MODIFIED_DATE = "modified_date";

}
