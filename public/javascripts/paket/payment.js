var tempProducts = {};
var deletedProducts = {};
var modalTableList = jQuery('.modal-table-list');
var modalSearchButton = jQuery('.modal-search-button');
var modalSearchView = jQuery('.modal-search-view');
var frontTableBody = jQuery('.front-list');
var invoiceDate = jQuery('.invoice-date');
var paymentDate = jQuery('.payment-date');
jQuery('.item-check').on('change', function () {
    var view = jQuery(this);
    var id = view.data('id');
    if (this.checked) {
        if (!checkedItems.hasOwnProperty(id)) {
            var data = {};
            data['id'] = id;
            data['total'] = Number(view.data('value'));
            checkedItems[id] = data;
        }
    } else {
        delete checkedItems[id];
    }
    getFormattedTotal();
});

jQuery('.checked-all').on('change', function () {
    jQuery('.item-check').prop('checked', this.checked).change();
});

jQuery('.input-form').validate({
    highlight: function(element) {
        $(element).parents('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).parents('.form-group').removeClass('has-error');
    }
});

function getTotalPrice() {
    var total = Number(0);
    if (Object.keys(checkedItems).length > 0) {
        for (var i in checkedItems) {
            total += checkedItems[i].total;
        }
    }
    return total;
}

function getFormattedTotal() {
    var total = getTotalPrice();
    return formattingCurrency(total);
}

function formattingCurrency(total) {
    return  "Rp " + (total).toLocaleString(params.lang);
}

function showToast(message) {
    jQuery.toast({
        heading: 'Oops',
        text: message,
        position: 'bottom-center',
        stack: false,
        icon: 'error',
        hideAfter: 6000
    })
}

function generateArray() {
    var arr = [];
    if (Object.keys(checkedItems).length > 0) {
        for (var i in checkedItems) {
            arr.push(checkedItems[i]);
        }
    }
    return JSON.stringify(arr);
}

jQuery('#rootwizard').bootstrapWizard({
    onTabShow : function(tab, navigation, index) {
        var next = jQuery('.next');
        next.show();
        var $total = navigation.find('li').length;
        var $current = index + 1;
        var $percent = ($current / $total) * 100;
        $('#rootwizard .progress-bar').css({
            width : $percent + '%'
        });
        if($current === $total) {
            next.addClass('simpan');
            next.removeClass('disabled');
            next.find('a').addClass('btn btn-md btn-primary').html(params.button.save);
        } else {
            next.removeClass('simpan');
            next.find('a').removeClass('btn btn-md btn-primary').html(params.button.next);
        }
    },
    onTabClick: function(tab, navigation, index) {
        alert('on tab click disabled');
        return false;
    },
    onNext: function (tab, navigation, index) {
        if (index === 1) {
            if (Object.keys(checkedItems).length === 0) {
                showToast(params.messages.acceptance_empty);
                return false;
            }
            jQuery('.total-price').val(getFormattedTotal());
            jQuery('.selected-items').val(generateArray());
        }
        if (index === 2) {
            var totalInvoice = $('.total-invoice').val().replace(/\./g,'');
            
            // if (totalInvoice != getTotalPrice()) {
            //     showToast(params.messages.total_invoice_not_equal);
            //     return false;
            // }

            $('input[name="model.invoiceNumber"]').rules('add', {
                required: true,
                alphanumeric: true
            });

            $('input[name="model.totalInvoice"]').rules('add', {
                required: true
            });

            $('input[name="model.invoiceDate"]').rules('add', {
                required: true,
                dateformat: true
            });

            $('input[name="model.paymentDate"]').rules('add', {
                required: true,
                dateformat: true
            });

            if (jQuery('.input-form').valid()) {
                jQuery('.input-form').submit();
            }
        }
        return true;
    }
});

function isDateValid(date) {
    return date.match(/^(\d{2})-(\d{2})-(\d{4})$/) === null;
}

/*=================================== DELETE ITEM ==============================*/

jQuery(document).on('click', '.delete-button', function () {
    deleteItem(jQuery(this));
});

function deleteItem(view) {
    var id = view.data('id');
    if (checkedItems.hasOwnProperty(id)) {
        var model = checkedItems[id];
        delete checkedItems[id];
        deletedProducts[id] = model;
        generatedDeletedItems();
    }
    deleteView(view);
    view.add('disabled');
}

function generatedDeletedItems() {
    if (Object.keys(deletedProducts).length > 0) {
        var array = [];
        for (var i in deletedProducts) {
            var data = deletedProducts[i];
            array.push({'modelId': data.id});
        }
        jQuery('.deleted-items').val(JSON.stringify(array));
    }
}

function deleteView(view) {
    view.parent().parent().remove();
}

/*======================================= MODAL ========================================*/

jQuery(".add-button").click(function () {
    jQuery('#myModal').modal({
        backdrop: 'static',
        keyboard: false
    });
});

jQuery('#myModal').on('show.bs.modal', function (e) {
    modalTableList.html('');
    tempProducts = {};
    modalSearchView.val('');
}).on('shown.bs.modal', function(){
    modalSearchButton.trigger('click');
});

modalSearchButton.click(function () {
    getProducts();
});

function getProducts() {
    var loader = Ladda.create(document.querySelector('.modal-search-button'));
    loader.start();
    jQuery.ajax({
        url: params.queryAction,
        type: 'POST',
        data: {
            keyword: modalSearchView.val(),
            exception: generateException(),
            authenticityToken: params.token
        }
    }).success(function (s) {
        // console.log(s);
        loader.stop();
        generateObject(s);
    }).fail(function (e) {
        // console.log(e);
        loader.stop();
        showToast("Terjadi kesalahan");
    });
}

function generateTemplateListItem(data, isFrontpage) {
    return '<tr>' +
        '<td><a href="' + data.acceptanceUrl + '" target="_blank">' + data.documentNumber + '</a></td>' +
        '<td>' + data.documentDate + '</td>' +
        '<td>' + data.acceptanceDate + '</td>' +
        '<td>' + formattingCurrency(Number(data.total)) + '</td>' +
        '<td>' + data.description + '</td>' +
        '<td>' +
        '            <button class="btn btn-sm btn-primary ' + (isFrontpage ? 'delete-button' : 'add-to-front') + '" type="button" data-value="" data-id="' + data.id + '">' +
        '                <i class="fa fa-'+(isFrontpage ? 'remove' : 'plus')+'"></i>' +
        '            </button>' +
        '</td>' +
        '</tr>';
}

jQuery(document).on('click', '.add-to-front', function () {
   var view = jQuery(this);
   var id = view.data('id');
   if (tempProducts.hasOwnProperty(id) && !checkedItems.hasOwnProperty(id)) {
       var model = tempProducts[id];
       checkedItems[id] = model;
       frontTableBody.append(generateTemplateListItem(model, true));
   }
   view.addClass('disabled');
});

function generateObject(data) {
    tempProducts = {};
    var template = '';
    if (data.length > 0) {
        for (var i = 0; i < data.length; i++) {
            var model = data[i];
            if (!tempProducts.hasOwnProperty(model.id)) {
                tempProducts[model.id] = model;
                template += generateTemplateListItem(model, false);
            }
        }
    }
    modalTableList.html(template);
}

function generateException() {
    if (Object.keys(checkedItems).length > 0) {
        var array = [];
        for (var i in checkedItems) {
            array.push(i);
        }
        return JSON.stringify(array);
    }
    return '[]';
}

