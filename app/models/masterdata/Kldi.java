package models.masterdata;

import models.BaseKatalogTable;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;


@Table(name = "kldi")
public class Kldi extends BaseTable {

	@Id
	public String id;
	public String nama;
	public String jenis;
	public String website;
	public boolean is2014;
	public boolean is2015;

	public Long prp_id;
	public Long kbp_id;

	public static List<Kldi> findLokal(){
		return Query.find("SELECT * FROM kldi WHERE jenis in ('KABUPATEN', 'KOTA', 'PROVINSI')",Kldi.class).fetch();
	}

	public static List<Kldi> findLokalLocation(){
		return Query.find("SELECT distinct (kbp_id),prp_id, jenis, k.nama, k.id FROM kldi k WHERE k.jenis in ('KABUPATEN', 'KOTA', 'PROVINSI') ",Kldi.class).fetch();
	}

	public static List<Kldi> findSektoral(){
		return Query.find("SELECT * FROM kldi WHERE jenis in ('KEMENTERIAN', 'LEMBAGA', 'INSTANSI')",Kldi.class).fetch();
	}

	public static List<Kldi> findJenis(){
		return Query.find("SELECT DISTINCT jenis FROM kldi WHERE jenis != '' && jenis is not null",Kldi.class).fetch();
	}

	public static List<Kldi> findIdKldi(String id){
		return Query.find("SELECT * FROM kldi WHERE id = '" + id + "'",Kldi.class).fetch();
	}

	public static Kldi findbyIdKldi(String id){
		return Query.find("SELECT * FROM kldi WHERE id = '" + id + "'",Kldi.class).first();
	}

	public static List<Kldi> findByJenis(String jenis){
		return find("jenis = ?", jenis).fetch();
	}
}
