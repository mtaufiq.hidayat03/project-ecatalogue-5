package models.prakatalog;

import models.BaseKatalogTable;
import models.common.DokumenInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import play.Logger;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.io.File;
import java.util.List;

/**
 * Created by dadang on 7/21/17.
 */
@Table(name = "dokumen_perpanjangan")
public class DokumenPerpanjangan extends BaseKatalogTable {

	@Id
	public Long id;

	public Long perp_id;

	public Long usulan_dok_perp_id;

	public String dok_judul;

	public String keterangan;

	public String dok_hash;

	public Long dok_id_attachment;

	public boolean active;

	public static DokumenInfo simpanDokPerpanjangan(Long perp_id, Long usulan_dok_perp_id, File file, String nama_dokumen, String keterangan) throws Exception {

		DokumenPerpanjangan dokumenPerpanjangan = new DokumenPerpanjangan();
		BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, dokumenPerpanjangan.dok_id_attachment, DokumenPerpanjangan.class.getSimpleName());
        dokumenPerpanjangan.perp_id = perp_id;
        dokumenPerpanjangan.usulan_dok_perp_id=usulan_dok_perp_id;
        dokumenPerpanjangan.dok_id_attachment = blob.id;
        dokumenPerpanjangan.dok_judul = blob.blb_nama_file;
        dokumenPerpanjangan.dok_hash = blob.blb_hash;
        dokumenPerpanjangan.active = true;
        dokumenPerpanjangan.keterangan = keterangan;
        Long ids = new Long(dokumenPerpanjangan.save());

        DokumenInfo doki = new DokumenInfo(blob);
        if(null != dokumenPerpanjangan.id)
            doki.id = dokumenPerpanjangan.id;
        else
            doki.id = ids;
        return doki;
	}

    public static DokumenPerpanjangan setNewDokPerpanjangan(Long perp_id, Long usulan_dok_perp_id, File file, String nama_dokumen, String keterangan) throws Exception {
		DokumenPerpanjangan dokumenPerpanjangan = new DokumenPerpanjangan();

			BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, dokumenPerpanjangan.dok_id_attachment, DokumenPerpanjangan.class.getSimpleName());
			dokumenPerpanjangan.perp_id = perp_id;
			dokumenPerpanjangan.usulan_dok_perp_id=usulan_dok_perp_id;
			dokumenPerpanjangan.dok_id_attachment = blob.id;
			dokumenPerpanjangan.dok_judul = blob.blb_nama_file;
			dokumenPerpanjangan.dok_hash = blob.blb_hash;
			dokumenPerpanjangan.active = true;
			dokumenPerpanjangan.keterangan = keterangan;

        try{
            Long iddp = new Long(dokumenPerpanjangan.save());
//            if(iddp >0){
//                blob.blb_id_content = iddp;
//                blob.save();
//            }
        }catch (Exception e){}
        return dokumenPerpanjangan;
    }

	public static List<DokumenPerpanjangan> listDokumenPerpanjangan(long usulan_id){
		//String query ="SELECT dp.id, dp.perp_id, dp.usulan_dok_perp_id, dp.dok_judul, dp.dok_hash, dp.dok_id_attachment FROM dokumen_perpanjangan dp LEFT JOIN usulan_dokumen_penawaran udp ON udp.nama_dokumen = dp.dok_judul WHERE dp.penawaran_id="+penawaran_id+" and dp.active=1 GROUP BY dp.dok_id_attachment";
		String query = "SELECT dp.id, dp.perp_id,u.nama_usulan,udp.dok_label, dp.usulan_dok_perp_id, dp.dok_judul, dp.dok_hash, dp.keterangan ,dp.dok_id_attachment\n" +
				"FROM dokumen_perpanjangan dp\n" +
				"       LEFT JOIN usulan_dokumen_perpanjangan udp ON dp.usulan_dok_perp_id=udp.id\n" +
				"       LEFT JOIN perpanjangan_kontrak pk ON dp.perp_id = pk.id\n" +
				"       LEFT JOIN usulan u on udp.usulan_id = u.id\n" +
				"WHERE pk.usulan_id="+usulan_id+" and dp.active=1 GROUP BY dp.dok_id_attachment;";
		return Query.find(query, DokumenPerpanjangan.class).fetch();
	}

}
