package controllers.masterdata;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.common.AktifUser;
import models.common.DokumenInfo;
import models.katalog.Komoditas;
import models.masterdata.Kldi;
import models.masterdata.PointRating;
import models.masterdata.Rating;
import models.penyedia.Penyedia;
import models.penyedia.PenyediaKomoditas;
import models.penyedia.PenyediaRepresentatif;
import models.penyedia.PenyediaWilayah;
import models.penyedia.PenyediaSektoral;
import models.penyedia.form.UserDistributorJson;
import models.penyedia.form.UserJson;
import models.prakatalog.Penawaran;
import models.secman.Acl;
import models.user.User;
import models.util.Config;
import models.util.Encryption;
import models.util.RekananDetail;
import models.viewmodels.RatingVM;
import play.Logger;
import play.data.binding.As;
import play.libs.URLs;
import play.libs.WS;
import repositories.penyedia.ProviderRepository;

import javax.validation.Validation;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class PenyediaCtr extends BaseController {

	public static final String TAG = "PenyediaCtr";

    private static final String formUrl = "masterdata/PenyediaCtr/form.html";

	@AllowAccess({Acl.COM_PENYEDIA})
	public static void index() {
		List<Komoditas> commodities = Komoditas.getForDropDown();
		AktifUser aktifUser = getAktifUser();
		render(commodities, aktifUser);
	}

	@AllowAccess({Acl.COM_PENYEDIA_ADD})
	public static void create(){
		List<Komoditas> komoditasList = Komoditas.findAllActive();
        Penyedia penyedia = new Penyedia();
        List<Kldi> lokasiKldi = Kldi.findLokalLocation();
        List<Kldi> lokasiKldiSektoral = Kldi.findSektoral();
		render(formUrl,komoditasList, penyedia, lokasiKldi,lokasiKldiSektoral);
	}

	@AllowAccess({Acl.COM_PENYEDIA_ADD, Acl.COM_PENYEDIA_EDIT})
	public static void formSubmit(Penyedia penyedia, String[] nama_representatif,
									String[] telp_representatif, String[] email_representatif,
									Long[] komoditas, String username, String rknDetailJson, String[] wilayah, String[] sektoral){

		List<PenyediaRepresentatif> representatifList = PenyediaRepresentatif.setList(nama_representatif, telp_representatif, email_representatif);
		List<Long> selectedKomoditas = Arrays.asList(komoditas);

		List<String> selectedlokasiKldi = null == wilayah ? new ArrayList<>() : Arrays.asList(wilayah);
		List<String> selectedlokasiKldiSektoral = null == sektoral ? new ArrayList<>() : Arrays.asList(sektoral);

		List<Kldi> lokasiKldi = Kldi.findLokalLocation();
		List<Kldi> lokasiKldiSektoral = Kldi.findSektoral();
		List<Komoditas> komoditasList = Komoditas.findAllActive();

		if(penyedia.id == null && username.isEmpty()){
			params.flash();
			flash.error("Anda belum memilih user untuk menjadi penyedia.");

			render(formUrl, komoditasList, penyedia, komoditas, username, representatifList, selectedKomoditas, selectedlokasiKldi, selectedlokasiKldiSektoral, lokasiKldi, lokasiKldiSektoral);
		}else if (komoditas.length < 1){
			params.flash();
			flash.error("Anda belum memilih komoditas.");

			render(formUrl, komoditasList, penyedia, komoditas, username, representatifList, selectedKomoditas, selectedlokasiKldi, selectedlokasiKldiSektoral, lokasiKldi, lokasiKldiSektoral);
		}else if (null == representatifList){
			params.flash();
			flash.error("Representatif belum diisi.");

			render(formUrl, komoditasList, penyedia, komoditas, username, representatifList, selectedKomoditas, selectedlokasiKldi, selectedlokasiKldiSektoral, lokasiKldi, lokasiKldiSektoral);
		}else{
//			validation.valid(penyedia);
//			Logger.info("errornya " + String.valueOf(validation.hasErrors()));
//			if(validation.hasErrors()) {
//				params.flash();
//				flash.error("Simpan Penyedia gagal. Silakan cek kembali isian Anda di tab Informasi Penyedia.");
//				List<Komoditas> komoditasList = Komoditas.findAllActive();
//
//				render(formUrl, komoditasList, penyedia, komoditas, username, representatifList, selectedKomoditas);
//			}else{
				RekananDetail rkn = new Gson().fromJson(rknDetailJson,RekananDetail.class);
				User user = User.findFirstByUserName(rkn.rkn_namauser);


				//check existing penyedia
				Penyedia existPenyedia;
				if (user != null) {
					existPenyedia = ProviderRepository.findByUserId(user.id);
				}else{
					existPenyedia = null;
				}

				if(existPenyedia != null){
					if(existPenyedia.rkn_id == null){
						existPenyedia.nama_penyedia = rkn.rkn_nama;
						existPenyedia.email = rkn.rkn_email;
						existPenyedia.rkn_id = rkn.rkn_id;
						existPenyedia.active = true;
						existPenyedia.save();
					}

					params.flash();
					flash.error("Simpan Penyedia gagal. User yang dipilih sudah menjadi penyedia di E-Katalog. Silakan pilih user yang lain.");

					render(formUrl, komoditasList, penyedia, komoditas, username, representatifList, selectedKomoditas, selectedlokasiKldi, selectedlokasiKldiSektoral, lokasiKldi, lokasiKldiSektoral);
				}

                Penyedia finalPenyedia = ProviderRepository.setByForm(penyedia);
				finalPenyedia.rkn_id = rkn.rkn_id;

				// check user.
                // If user is null, insert new user from rknDetailJson and get a new userId
                // set userId to finalPenyedia.user_id
//				if(user != null){
//					finalPenyedia.user_id = user.id;
//
//				}else{
				finalPenyedia.user_id = User.saveOrUpdateUsrAddUser(rkn);
				//}

                if(finalPenyedia.id != null){
                    PenyediaRepresentatif.deleteByPenyedia(penyedia.id);
                    PenyediaKomoditas.deleteByPenyedia(penyedia.id);
                }

				long penyedia_id = finalPenyedia.save();

				for(int counter = 0;counter < komoditas.length; counter++){
					PenyediaKomoditas pk = new PenyediaKomoditas();
					pk.penyedia_id = penyedia_id;
					pk.komoditas_id = komoditas[counter];
					pk.active = true;
					pk.save();
				}

				//wilayah isinya kldi id
				if(null != wilayah && wilayah.length > 0){
					for (String kldiwilayah: wilayah) {
						PenyediaWilayah.saveNew(kldiwilayah, penyedia_id);
					}
				}

				//Save Wilayah Sektoral
				if(null != sektoral && sektoral.length > 0){
					for (String kldiSektoral: sektoral) {
						PenyediaSektoral.saveNew(kldiSektoral, penyedia_id);
					}
				}

				for(PenyediaRepresentatif rep : representatifList){
					rep.penyedia_id = penyedia_id;
					rep.active = true;
					rep.save();
				}

				flash.success("Data berhasil disimpan");
				index();
//			}
		}


	}

	@AllowAccess({Acl.COM_PENYEDIA_EDIT})
	public static void edit(long id) {
		Penyedia penyedia = Penyedia.findById(id);
		penyedia.lokasiKldi = penyedia.locationSelected();
		penyedia.lokasiKldiSektoral = penyedia.locationSektoralSelected();
		List<PenyediaRepresentatif> representatifList = PenyediaRepresentatif.find("penyedia_id = ?",id).fetch();
        List<Long> pkList = PenyediaKomoditas.findIdsByPenyedia(id);
        List<Komoditas> komoditasList = Komoditas.findAllActive();
		List<Kldi> lokasiKldi = Kldi.findLokalLocation();
		List<Kldi> lokasiKldiSektoral = Kldi.findSektoral();
		render(penyedia, representatifList, pkList, komoditasList,lokasiKldi,lokasiKldiSektoral);
	}

	@AllowAccess({Acl.COM_PENYEDIA_EDIT})
	public static void editSubmit(long penyedia_id, Penyedia penyedia, String[] nama_representatif,
								  String[] telp_representatif, String[] email_representatif, Long[] komoditas, String[] wilayah, String[] wilayahSektoral){

		if(komoditas==null)
			komoditas = new Long[]{};
		if(nama_representatif==null)
			nama_representatif = new String[]{};
		if(telp_representatif==null)
			telp_representatif = new String[]{};
		if(email_representatif==null)
			email_representatif = new String[]{};
		
        for(int i = 0; i < komoditas.length; i++){
            Logger.debug(komoditas[i]+"");
        }
        penyedia.id = penyedia_id;

        List<PenyediaRepresentatif> representatifList = PenyediaRepresentatif.setList(nama_representatif,telp_representatif,email_representatif);

        if(komoditas.length < 1){
            params.flash();
            flash.error("Komoditas tidak boleh kosong.");
            List<Long> pkList = Arrays.asList(komoditas);
            List<Komoditas> komoditasList = Komoditas.findAllActive();
            render("masterdata/PenyediaCtr/edit.html",penyedia_id,penyedia,representatifList, pkList, komoditasList);
        }else{
            validation.valid(penyedia);
            if (validation.hasErrors()){
                params.flash();
                flash.error("Gagal menyimpan Penyedia. Silakan cek kembali isian informasi penyedia.");

                List<Long> pkList = Arrays.asList(komoditas);
                List<Komoditas> komoditasList = Komoditas.findAllActive();
                render("masterdata/PenyediaCtr/edit.html",penyedia_id,penyedia,representatifList, pkList, komoditasList);
            } else {
                PenyediaRepresentatif.deleteByPenyedia(penyedia_id);
                PenyediaKomoditas.deleteByPenyedia(penyedia_id);

                Penyedia penyediaDb = Penyedia.findById(penyedia_id);
                penyediaDb.nama_penyedia = penyedia.nama_penyedia;
                penyediaDb.npwp = penyedia.npwp;
                penyediaDb.alamat = penyedia.alamat;
                penyediaDb.kode_pos = penyedia.kode_pos;
                penyediaDb.email = penyedia.email;
                penyediaDb.website = penyedia.website;
                penyediaDb.no_telp = penyedia.no_telp;
                penyediaDb.no_hp = penyedia.no_hp;
                penyediaDb.no_fax = penyedia.no_fax;
                penyediaDb.pkp = penyedia.pkp;
                penyediaDb.produk_api_url = penyedia.produk_api_url;
                penyediaDb.active = true;

                penyediaDb.save();

                for(int counter = 0;counter < komoditas.length; counter++){
                    PenyediaKomoditas pk = new PenyediaKomoditas();
                    pk.penyedia_id = penyedia_id;
                    pk.komoditas_id = komoditas[counter];
                    pk.active = true;
                    pk.save();
                }

                for(PenyediaRepresentatif rep : representatifList){
                    rep.penyedia_id = penyedia_id;
                    rep.active = true;
                    rep.save();
                }

				//wilayah isinya kldi id
				if(null != penyedia.lokasiKldi && !penyedia.lokasiKldi.isEmpty()){
					List<String> existingDb = penyedia.locationSelected();
					List<String> newselected = penyedia.lokasiKldi;

					PenyediaWilayah.deleteNotIn(penyedia,penyedia.lokasiKldi);

					newselected.removeAll(existingDb);//clean yang sama, yang gak sama disimpan
					for (String kldiwilayah: newselected) {
						PenyediaWilayah.saveNew(kldiwilayah, penyedia_id);
					}



				}

				//Save Instansi
				if(null != penyedia.lokasiKldiSektoral && !penyedia.lokasiKldiSektoral.isEmpty()){
					List<String> existingDbSektoral = penyedia.locationSektoralSelected();
					List<String> newselectedSektoral = penyedia.lokasiKldiSektoral;

					PenyediaSektoral.deleteNotIn(penyedia,penyedia.lokasiKldiSektoral);

					newselectedSektoral.removeAll(existingDbSektoral);
					for (String kldiSektoral: newselectedSektoral) {
						PenyediaSektoral.saveNew(kldiSektoral, penyedia_id);
					}
				}

                flash.success("Data berhasil disimpan");
                index();
            }

        }

	}

	@AllowAccess({Acl.COM_PENYEDIA, Acl.COM_PENYEDIA_EDIT})
	public static void detail(long id) {
		List<DokumenInfo> dokumenUkmList = new ArrayList<>();
		List<Penawaran> komoditasList = new ArrayList<>();

		Penyedia penyedia = ProviderRepository.findDetailById(id);
		dokumenUkmList = DokumenInfo.listDokumenUkm(id);
		komoditasList = Penawaran.infoKomoditasPenyedia(id);
		render(penyedia,dokumenUkmList,komoditasList);
	}

	@AllowAccess({Acl.ALL_AUTHORIZED_USERS})
	public static void infoPenyedia(long id){
		List<PointRating> points = new ArrayList<>();
		List<RatingVM> ratingResult = new ArrayList<>();
		List<DokumenInfo> dokumenUkmList = new ArrayList<>();
		List<Penawaran> komoditasList = new ArrayList<>();
		Penyedia penyedia = new Penyedia();
		try{
			penyedia = ProviderRepository.findDetailById(id);
			points = PointRating.findAll();
			ratingResult = Rating.getRatingResult();
			dokumenUkmList = DokumenInfo.listDokumenUkm(id);
			komoditasList = Penawaran.infoKomoditasPenyedia(penyedia.id);
		}catch (Exception e){}

		render("masterdata/PenyediaCtr/infoPenyedia.html",penyedia, points,ratingResult,dokumenUkmList,komoditasList);
	}

	@AllowAccess({Acl.COM_RATING_INPUT})
	//@AllowAccess({Acl.ALL_AUTHORIZED_USERS})
	public static void saveRatePenyedia(long idPenyedia, Rating[] pt){
		params.flash();
		for (Rating rating : pt)
		{

			rating.created_by = AktifUser.getActiveUserId();
			rating.created_date = new Timestamp(new java.util.Date().getTime());
			rating.save();
		}
		flash.success("Data berhasil disimpan");
		infoPenyedia(idPenyedia);
	}

//	@AllowAccess({Acl.COM_PENYEDIA_ADD,Acl.COM_PENYEDIA_EDIT})
	public static void findUser(String searchType, String keyword){
		JsonObject params = new JsonObject();
		params.addProperty("username","");
		params.addProperty("nama","");
		params.addProperty("npwp","");
		params.addProperty("email","");
		params.addProperty("rkn_id","");

		params.addProperty(searchType,keyword);

		Logger.debug(params.toString());

		try{

			Config config = Config.getInstance();
			String param = URLs.encodePart(Encryption.encrypt(params.toString().getBytes()));
			String urlAuth = config.adp_rekanan_search + "?q=" + param;

            Logger.debug(urlAuth);

			String content = WS.url(urlAuth).timeout("5min").getAsync().get().getString();
			String response = Encryption.decrypt(content);

			if(response != null && !response.isEmpty()){
				Type type = new TypeToken<List<RekananDetail>>(){}.getType();
				List<RekananDetail> adpUsers = new Gson().fromJson(response,type);
				Logger.debug( TAG +" total: "+ adpUsers.size()+"");
				UserJson results = new UserJson();
				List<RekananDetail> subs = new ArrayList<>();
				if(adpUsers.size() > Penyedia.MAX_SEARCH_USER_FROM_ADP){
					Logger.debug(TAG + " result is too big, cut it...");
					results.message = "Hasil pencarian terlalu banyak. Berikan keyword yang lebih spesifik.";
					subs = adpUsers.subList(0, Penyedia.MAX_SEARCH_USER_FROM_ADP);
				} else {
					results.message = "success";
					subs.addAll(adpUsers);
				}
				results.results = new ArrayList<>();
				if (!subs.isEmpty()) {

					StringBuilder sb = new StringBuilder();
					String glue = "";
					for (RekananDetail model : subs) {
						sb.append(glue).append("'").append(model.rkn_namauser).append("'");
						glue = ",";
					}

//					Map<Long, Penyedia> penyediaMap = ProviderRepository.checkProviderByRknId(sb.toString());
					Map<String, User> penyediaMap = ProviderRepository.checkProviderByUserName(sb.toString());
					for (RekananDetail adp : subs) {
//						Logger.debug("rkn_username: " + adp.rkn_namauser);
//						Logger.debug("rkn_username db : " + penyediaMap.keySet());
//						if (penyediaMap.containsKey(adp.rkn_namauser)) {
//							adp.user_id = penyediaMap.get(adp.rkn_namauser).id;
							results.results.add(adp);
//						}
					}
				}

				if (results.results.isEmpty()){
					results.message = "Data not found!";
				}

				renderJSON(results);
			}

		}catch (Exception e){
			e.printStackTrace();
			UserJson results = new UserJson();
			results.message = "Terjadi Kesalahan. Silakan mencoba untuk memberikan keyword yang spesifik.";
			renderJSON(results);
		}
	}

	@AllowAccess({Acl.COM_PENAWARAN_BUKA})
	public static void findPenyedia(String searchType, String keyword){

		List<Penyedia> searchResult = ProviderRepository.findPenyediaBySearch(keyword,searchType);

		UserJson result = new UserJson();
		result.penyediaList = searchResult;
		result.message = "success";
		renderJSON(result);

	}

	public static void findUserInPenyedia(String searchType, String keyword){
		List<UserDistributorJson> searchResult = ProviderRepository.findPenyediaJoinUserBySearch(keyword,searchType);

		UserJson result = new UserJson();
		result.distributorJsonList = searchResult;
		if (!result.distributorJsonList.isEmpty()) {
			result.message = "Success";
		}else{
			result.message = "Data not found!";
		}
		renderJSON(result);
	}

}
