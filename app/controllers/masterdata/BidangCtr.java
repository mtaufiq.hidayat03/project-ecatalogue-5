package controllers.masterdata;

import controllers.BaseController;
import controllers.security.AllowAccess;
import models.jcommon.util.CommonUtil;
import models.masterdata.Bidang;
import models.masterdata.Tahapan;
import models.secman.Acl;
import play.i18n.Messages;

import java.util.Arrays;
import java.util.List;

public class BidangCtr extends BaseController {

    @AllowAccess({Acl.COM_BIDANG})
    public static void index() {
        render();
    }

    @AllowAccess({Acl.COM_BIDANG_ADD})
    public static void create() { render(); }

    @AllowAccess({Acl.COM_BIDANG_ADD})
    public static void createSubmit(String[] nama_bidang_arr) {
        checkAuthenticity();
        if(nama_bidang_arr != null){

            //validasi bidang
            boolean validate = validateTahapan(nama_bidang_arr);

            if(!validate){

                if (validation.hasErrors())
                    flash.error(Messages.get("notif.failed.submit"));

                List<String> bidangList = Arrays.asList(nama_bidang_arr);

                render("masterdata/TahapanCtr/create.html", bidangList);

            }

            Bidang.saveBidang(nama_bidang_arr);

            flash.success(Messages.get("notif.success.submit"));
            index();
        }

    }

    @AllowAccess({Acl.COM_BIDANG_EDIT})
    public static void editSubmit(long id_bidang, String nama_bidang) {
        checkAuthenticity();
        if (CommonUtil.isEmpty(nama_bidang)) {
            flash.error(Messages.get("notif.failed.update"));
        } else {
            Bidang.updateBidang(id_bidang,nama_bidang);
        }
        flash.success(Messages.get("notif.success.update"));
        index();
    }

    @AllowAccess({Acl.COM_BIDANG_DELETE})
    public static void delete(long id) {
        try {
            Bidang.deleteBidang(id);
            flash.success(Messages.get("notif.success.delete"));
        }catch (Exception e){
            flash.error(Messages.get("notif.failed.delete"));
        }

        index();
    }

    private static boolean validateTahapan(String[] nama_tahapan_arr){

        boolean validate = true;

        for(int i = 0; i < nama_tahapan_arr.length; i++){

            if(CommonUtil.isEmpty(nama_tahapan_arr[i])) {

                validate = false;

                validation.addError("tahapan_"+(i+1), "Nama Tahapan Harus Diisi", "var");

            }

        }

        return validate;
    }

    public static void getTahapan(){
        List<Tahapan> tahapanList = Tahapan.find("active = 1").fetch();

        renderJSON(tahapanList);
    }
}
