DROP TABLE IF EXISTS `peserta_usulan`;
CREATE TABLE `peserta_usulan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usulan_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `deleted_date` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY peserta_usulan_usulan_fk(usulan_id) REFERENCES usulan(id),
  FOREIGN KEY peserta_usulan_user_fk(user_id) REFERENCES user(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;