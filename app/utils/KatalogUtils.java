package utils;


import com.google.gson.reflect.TypeToken;
import models.jcommon.util.CommonUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.Play;

import java.io.File;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.*;
import java.util.*;

public class KatalogUtils {

    public static BigDecimal parseKursCurrency(String value) throws ParseException {
        NumberFormat fmt = NumberFormat.getNumberInstance(Locale.ENGLISH);
        ((DecimalFormat)fmt).setParseBigDecimal(true);

        return (BigDecimal)fmt.parse(value);
    }

    public static String getLabel(String status){
        String result = status.replace("_"," ");
        return result;
    }

    public static String getStatusLabel(String status){
        String result = status.replace("_"," ");
        return result.toUpperCase();
    }

    public static String generateSlug(String text){
        String result = text.replace(" ","-");
        return result.toLowerCase();
    }

    public static String generaRandomId(String id) {
        if (id != null) {
            final int length = id.length();
            Random random = new Random();
            long nextLong = Math.abs(random.nextLong());
            return  id + "" + String.valueOf(nextLong).substring(0, 10 - length);
        }
        return "";
    }

    public static String getFormattedDocNumber(String no_dokumen) {
        if (!TextUtils.isEmpty(no_dokumen)) {
            StringBuilder sb = new StringBuilder(no_dokumen);
            if (no_dokumen.length() > 4) {
                sb.insert(4, "-");
            }
            if (no_dokumen.length() > 9) {
                sb.insert(9, "-");
            }
            return sb.toString();
        }
        return "";
    }

    public static String generateSelectedColumns(String[] fields) {
        if (fields != null && fields.length > 0) {
            StringBuilder sb = new StringBuilder();
            String glue = "";
            for (String s : fields) {
                sb.append(glue).append(" ").append(s);
                glue = ",";
            }
            return sb.toString();
        }
        return "*";
    }

    public static String ucfirst(String s) {
        if (TextUtils.isEmpty(s)) return "";
        return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
    }

    public static String generateExclude(String excluded) {
        if (TextUtils.isEmpty(excluded)) {
            excluded = "[]";
        }
        Type collectionType = new TypeToken<Collection<String>>() {
        }.getType();
        List<String> items = CommonUtil.fromJson(excluded, collectionType);
        StringBuilder sb = new StringBuilder();
        if (items != null && !items.isEmpty()) {
            String glue = "";
            for (String s : items) {
                sb.append(glue).append("'").append(s).append("'");
                glue = ",";
            }
        }
        return sb.toString();
    }

    public static String getProductBasketCookie(Long userId) {
        return "produk_basket_" + DigestUtils.md5Hex(String.valueOf(userId));
    }

    public static Date toDateMMMddYYYY(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, YYYY");
        try {
            if(date != null){
                return sdf.parse(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String toDateyyyyMMdd(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
        if(date != null){
            return sdf.format(date);
        }

        return null;
    }

    public static Boolean fileExistInPathApps(String path){
        try{
            String longpath = Play.applicationPath + path;
            return new File(longpath).exists();
        }catch (Exception e){}
        return false;
    }

    public static String cleanForLocalPathImage(String imageUrl){
        String pathImage = imageUrl.replace("//","/");
        pathImage = pathImage.replace(Play.configuration.getProperty("application.baseUrl"),"");
        if(!KatalogUtils.fileExistInPathApps(pathImage)){
            pathImage = pathImage.replace("/public/files/image/","/public/files/upload/");
        }
        Logger.debug(pathImage);
        return pathImage;
    }
    public static String cleanForLocalPathKontrak(String imageUrl){
        String pathImage = imageUrl.replace("//","/");
        pathImage = pathImage.replace(Play.configuration.getProperty("application.baseUrl"),"");
        if(!KatalogUtils.fileExistInPathApps(pathImage)){
            //lokasi v4 /public/files/upload/penyedia_kontrak/
            pathImage = pathImage.replace("/public/files/","/public/files/upload/");
        }
        Logger.debug(pathImage);
        return pathImage;
    }
    public static String escapeSql(String str) {
        if (str == null) {
            return null;
        }
        return StringUtils.replace(str, "'", "''");
    }

    public static String replaceHostInUrl(String url, String newHost)
    {
        if (url == null || newHost == null)
        {
            return url;
        }

        try
        {
            URL originalURL = new URL(url);

            boolean hostHasPort = newHost.indexOf(":") != -1;
            int newPort = originalURL.getPort();
            if (hostHasPort)
            {
                URL hostURL = new URL("http://" + newHost);
                newHost = hostURL.getHost();
                newPort = hostURL.getPort();
            }
            else
            {
                newPort = -1;
            }

            // Use implicit port if it's a default port
            boolean isHttps = originalURL.getProtocol().equals("https");
            boolean useDefaultPort = (newPort == 443 && isHttps) || (newPort == 80 && !isHttps);
            newPort = useDefaultPort ? -1 : newPort;

            URL newURL = new URL(originalURL.getProtocol(), newHost, newPort, originalURL.getFile());
            String result = newURL.toString();

            return result;
        }
        catch (MalformedURLException e)
        {
            throw new RuntimeException("Couldnt replace host in url, originalUrl=" + url + ", newHost=" + newHost);
        }
    }

    public static String removeProtocol(String url) {
        URL urls = null;
        String result = url;
        try {
            urls = new URL(url);
        } catch (MalformedURLException e) {
            Logger.info("Erro buang protokol url:"+e.getMessage());
        }
        result = urls.toString().replace(urls.getProtocol(),"").replace(":","");
        return result;
    }

    public static String cleanRootUrlImage(String pathImage){
        String result = "";
        result = replaceHostInUrl(pathImage,"");
        result = removeProtocol(result);
        return result.replace("//","/");
    }
}
