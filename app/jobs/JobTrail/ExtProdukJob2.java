package jobs.JobTrail;

import jobs.ExtProduct.ExtProduct;
import jobs.ExtProduct.PenyediaKomoditasApi;
import models.api.produk.HistoryDatafeed;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;

import java.util.UUID;

@NoTransaction
public class ExtProdukJob2 extends Job {
    UUID id = UUID.randomUUID();
    int _defaultWaitime = 0;
    PenyediaKomoditasApi _pk = new PenyediaKomoditasApi();
    int _nextWaitime = 0;
    String _newurls = "";
    HistoryDatafeed _hdf = new HistoryDatafeed();

    public ExtProdukJob2(int defaultWaitime, PenyediaKomoditasApi pk, int nextWaitime, String newurls, HistoryDatafeed _hdf){
        _defaultWaitime = defaultWaitime;
        _pk = pk;
        _nextWaitime = nextWaitime;
        _newurls = newurls;
        _hdf = _hdf;
    }

    @Override
    public void doJob() {
        ExtProduct ext = new ExtProduct();
        ext.penyediaKomoditasApi = _pk;
        ext.defaultWaitime = _defaultWaitime;
        ext.nextWaitime = _nextWaitime;
        ext.historyDatafeed = _hdf;
        ext.getDatas(ext.defaultWaitime, _newurls,ext.historyDatafeed);
    }
    public void before(){
        //Logger.info("on continue next page, running job id:"+id);
        Logger.info("on continue next page, penyedia:"+_pk.nama_penyedia);
    }
}
