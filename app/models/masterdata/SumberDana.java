package models.masterdata;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

@Table(name = "sumber_dana")
public class SumberDana extends BaseKatalogTable {

	@Id
	public Long id;
	public String nama_sumber_dana;
	public String deskripsi;
	public boolean active;


	public static void deleteById(long id){
		SumberDana sd = SumberDana.findById(id);
		sd.active = false;
		sd.save();
	}

	public static Integer checkChildByFundSourceId(long id) {
		final String query = "SELECT COUNT(*) as total FROM sumber_dana_paket WHERE sumber_dana_id =? ";
		return Query.find(query, Integer.class, id).first();
	}

	public void softDelete() {
		this.active = false;
		setDeleted();
		save();
	}

}
