package models.katalog.komoditasmodel;

import models.BaseKatalogTable;
import models.katalog.komoditasmodel.contracts.AttributeContract;
import models.katalog.komoditasmodel.form.AttributeForm;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Column;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 12/5/2017
 */
@Table(name = "komoditas_produk_atribut_tipe")
public class KomoditasProdukAtributTipe extends BaseKatalogTable implements AttributeContract {

    @Id
    public Long id;
    public Long komoditas_id;
    @Column(length = 50)
    public String tipe_atribut;
    public String deskripsi;
    public Boolean active;
    public int posisi_item = 0;

    public KomoditasProdukAtributTipe() {}

    public KomoditasProdukAtributTipe setDataForm(AttributeForm model) {
        if (!model.isCreateMode() && this.id == null) {
            this.id = model.id;
        }
        this.komoditas_id = model.commodityId;
        this.tipe_atribut = model.name;
        this.deskripsi = model.description;
        this.active = true;
        return this;
    }

    public KomoditasProdukAtributTipe setPosition(int position) {
        this.posisi_item = position;
        return this;
    }

    public void saveAtribut() {
        final Long id = (long) save();
        if (this.id == null) {
            this.id = id;
        }
    }

    public static List<KomoditasProdukAtributTipe> getAllByCommodity(Long commodityId) {
        return find("komoditas_id =? AND active = ? ORDER BY posisi_item ASC", commodityId, AKTIF).fetch();
    }

    public static Map<Long, KomoditasProdukAtributTipe> getAllByCommodityMap(Long commodityId) {
        List<KomoditasProdukAtributTipe> results = Query.find("SELECT id, tipe_atribut FROM komoditas_produk_atribut_tipe" +
                " WHERE komoditas_id =? AND active = ?", KomoditasProdukAtributTipe.class, commodityId, AKTIF).fetch();
        return results.stream().collect(Collectors.toMap(KomoditasProdukAtributTipe::getId, k -> k));
    }

    public static Map<Long, KomoditasProdukAtributTipe> getAllByCommodityMapByIds(String ids) {
        List<KomoditasProdukAtributTipe> results = Query.find("SELECT * FROM komoditas_produk_atribut_tipe" +
                " WHERE id IN ("+ids+") AND active = ?", KomoditasProdukAtributTipe.class, AKTIF).fetch();
        return results.stream().collect(Collectors.toMap(KomoditasProdukAtributTipe::getId, k -> k));
    }

    public void softDelete() {
        this.active = false;
        setDeleted();
        save();
    }

    @Override
    public Long getId() {
        return this.id;
    }

}
