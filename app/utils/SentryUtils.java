package utils;

import io.sentry.SentryClient;
import io.sentry.SentryClientFactory;
import io.sentry.context.Context;
import io.sentry.event.Breadcrumb;
import io.sentry.event.BreadcrumbBuilder;
import io.sentry.event.UserBuilder;
import models.common.AktifUser;
import org.apache.commons.lang3.StringUtils;
import play.Play;
import play.mvc.Http;
import play.mvc.Scope;

import java.util.Date;
import java.util.Map;

/**
 * @author HanusaCloud on 5/28/2020 9:30 AM
 */
public class SentryUtils {

    public static final String TAG = "SentryUtil";

    private static SentryClient sentryClient;
    private static final String TYPE_UNCAUGHT = "UNCAUGHT";
    private static final String TYPE_HANDLED = "HANDLED";

    static  {
        LogUtil.debug(TAG, "init sentry util");
        final String dsn = Play.configuration.getProperty("sentry.dsn");
        LogUtil.debug(TAG, "sentry dsn: " + dsn);
        if (!StringUtils.isEmpty(dsn)) {
            sentryClient = SentryClientFactory.sentryClient(dsn);
        }
    }

    public static void uncaught(Throwable t, AktifUser aktifUser, String request) {
        catchError(t, TYPE_UNCAUGHT, aktifUser, request);
    }

    private static void catchError(Throwable e, String type, AktifUser user, String request) {
        if (sentryClient == null) {
            LogUtil.debug(TAG, "Oops.. you don't configure your sentry");
            return;
        }
        try {
            LogUtil.debug(TAG, "send error to sentry.io, type: " + type);

            Context context = sentryClient.getContext();
            // Record a breadcrumb in the current context. By default the last 100 breadcrumbs are kept.
            final String env = Play.mode.isDev() ? "Development" : "Production";
            context.recordBreadcrumb(new BreadcrumbBuilder()
                    .setCategory(env)
                    .setMessage("Catch unhandled error")
                    .setTimestamp(new Date())
                    .setLevel(Breadcrumb.Level.ERROR)
                    .build());
            context.addExtra("environment", env);
            context.addExtra("request", request);
            context.addTag("environment", env);
            final String name = user == null ? "non-authorized-user" : user.nama_lengkap;
            context.addTag("user", name);
            context.addTag("type", type);
            // Set the user in the current context.
            final String emailAddress = user == null ? "non-authorized-user" : user.email;
            final long userId = user == null ? 0 : user.user_id;
            context.setUser(new UserBuilder()
                    .setEmail(emailAddress)
                    .setUsername(name)
                    .setId(String.valueOf(userId))
                    .build());
            sentryClient.sendException(e);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static class SentryRequest {

        public final String action;
        public final String method;
        public final String actionMethod;
        public final Scope.Params params;
        public final Map<String, Object> args;
        public final String contentType;
        public final String controller;
        public final Map<String, Http.Cookie> cookies;
        public final Map<String, Http.Header> headers;
        public final String remoteAddress;

        public SentryRequest(Http.Request request) {
            this.action = request.action;
            this.method = request.method;
            this.actionMethod = request.actionMethod;
            this.params = request.params;
            this.args = request.args;
            this.contentType = request.contentType;
            this.controller = request.controller;
            this.cookies = request.cookies;
            this.headers = request.headers;
            this.remoteAddress = request.remoteAddress;
        }

    }

}
