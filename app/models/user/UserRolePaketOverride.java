package models.user;

import models.BaseKatalogTable;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Table(name="user_role_paket_override")
public class UserRolePaketOverride extends BaseKatalogTable {

	@Id
	public Long id;
	public boolean active;
	public Long paket_id;
	public Long user_id;

	@Transient
	public String paket_name;

	public UserRolePaketOverride() {}

	public UserRolePaketOverride(Long itemId, Long userId) {
		this.paket_id = itemId;
		this.user_id = userId;
		this.active = true;
	}

	public static List<UserRolePaketOverride> findByUserId(long id){
		String query = "select uri.id, uri.paket_id, uri.user_id, ri.nama_paket as paket_name from user_role_paket_override uri " +
				"inner join paket ri on uri.paket_id = ri.id where uri.active = 1 and uri.user_id = ?";

		return Query.find(query, UserRolePaketOverride.class, id).fetch();
	}

	public static Map<Long, String> findByUserIdMap(long id){
		List<UserRolePaketOverride> userPakets = findByUserId(id);
		return userPakets.stream().collect(
				Collectors.toMap(i -> i.paket_id, i -> i.paket_name));
	}

	public static void deleteByUserId(long userId) {
		String query = "delete from user_role_paket_override where user_id = ?";
		Query.update(query, userId);
	}
}
