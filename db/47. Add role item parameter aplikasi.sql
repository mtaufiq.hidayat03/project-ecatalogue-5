INSERT INTO role_grup (nama_grup, kategori, active, created_date) VALUES
("Parameter Aplikasi", "master_data", 1, CURDATE());

INSERT INTO role_item (role_grup_id, nama_role, deskripsi, active, created_date) VALUES
  ((SELECT id FROM role_grup WHERE nama_grup = "Parameter Aplikasi"), "comParameterAplikasi", "Akses Manajemen Parameter Aplikasi", 1, CURDATE()),
  ((SELECT id FROM role_grup WHERE nama_grup = "Parameter Aplikasi"), "comParameterAplikasiEdit", "Akses Edit Parameter Aplikasi", 1, CURDATE());
