package elastics;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Test;

import models.elasticsearch.SearchQuery;
import models.elasticsearch.SearchResult;
import models.elasticsearch.response.MetaProduct;
import play.Logger;
import play.test.UnitTest;
import repositories.elasticsearch.ElasticsearchRepository;

public class SearchProductTest extends UnitTest {
	
	////@Test
	public void testOrderByNameAscending() throws Exception {
		SearchQuery sq = new SearchQuery();
		sq.order = "nameAsc";
		ElasticsearchRepository repo = new ElasticsearchRepository();
		SearchResult sr = repo.searchProduct(sq);
		List<MetaProduct> products = sr.products;
		List<String> productNames = products.stream().map(p->p.product.productName).collect(Collectors.toList());
		assertFalse("list is empty",products.isEmpty());
		assertFalse("list is empty",productNames.isEmpty());
		List<String> productNamesSorted = productNames.stream().sorted((a,b)->a.compareTo(b)).collect(Collectors.toList());
		assertEquals(productNamesSorted, productNames);
	}

	//@Test
	public void testOrderByNameDescending() throws Exception {
		SearchQuery sq = new SearchQuery();
		sq.order = "nameDesc";
		ElasticsearchRepository repo = new ElasticsearchRepository();
		SearchResult sr = repo.searchProduct(sq);
		List<MetaProduct> products = sr.products;
		List<String> productNames = products.stream().map(p->p.product.productName).collect(Collectors.toList());
		assertFalse("list is empty",products.isEmpty());
		assertFalse("list is empty",productNames.isEmpty());
		List<String> productNamesSorted = productNames.stream().sorted((a,b)->b.compareTo(a)).collect(Collectors.toList());
		assertEquals(productNamesSorted, productNames);
	}

	//@Test
	public void testOrderByMinPrice() throws Exception {
		SearchQuery sq = new SearchQuery();
		sq.order = "minPrice";
		ElasticsearchRepository repo = new ElasticsearchRepository();
		SearchResult sr = repo.searchProduct(sq);
		List<MetaProduct> products = sr.products;
		List<Long> productMinPrices = products.stream().map(p->Math.round(p.product.getMinPrice()*10)).collect(Collectors.toList());
		assertFalse("list is empty",products.isEmpty());
		assertFalse("list is empty",productMinPrices.isEmpty());
		List<Long> productMinPricesSorted = productMinPrices.stream().sorted((a,b)->a.compareTo(b)).collect(Collectors.toList());
		List<String> differences=IntStream.range(0,products.size())
				.mapToObj(i->productMinPricesSorted.get(i).equals(productMinPrices.get(i))?
						null:
						
								productMinPricesSorted.get(i).toString()+":"+
								productMinPrices.get(i).toString()
							
						)
				.collect(Collectors.toList());
		assertTrue(differences.toString(), productMinPricesSorted.equals(productMinPrices));
	}


	//@Test
	public void testOrderByMaxPrice() throws Exception {
		SearchQuery sq = new SearchQuery();
		sq.order = "maxPrice";
		ElasticsearchRepository repo = new ElasticsearchRepository();
		SearchResult sr = repo.searchProduct(sq);
		List<MetaProduct> products = sr.products;
		List<Long> productMaxPrices = products.stream().map(p->Math.round(p.product.getMaxPrice()*10)).collect(Collectors.toList());
		assertFalse("list is empty",products.isEmpty());
		assertFalse("list is empty",productMaxPrices.isEmpty());
		List<Long> productMaxPricesSorted = productMaxPrices.stream().sorted((a,b)->a.compareTo(b)).collect(Collectors.toList());
		assertEquals(productMaxPricesSorted, productMaxPrices);
	}

	@Test
	public void replaceWord(){
		//
		String reserved = "+ - = && || > < ! ( ) { } [ ] ^ \" ~ * ? : \\ /".trim();
		Logger.info(reserved);
		String kata = "Epirubisin/Epirubisin+]".replaceAll("([\\/\\+~*?:{}^\\[\\]\\-|=&><!\"()])","\\\\$1");
		Logger.info(kata);
	}

}
