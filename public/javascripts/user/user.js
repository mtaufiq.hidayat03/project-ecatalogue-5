var messageContainer = jQuery('.message-container');

jQuery('.form-horizontal').validate({
    rules: {
        'user.user_email': {
            required: true,
            emailAddress: true
        }
    },
    highlight: function(element) {
        $(element).parents('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).parents('.form-group').removeClass('has-error');
    }
});


jQuery('.form-horizontal').cancelSubmit = false;

jQuery('.form-horizontal').submit(function (e) {
    var data = validate();
    if (jQuery(this).valid() && data.status) {
    } else {
        showAlert(true, data.message === undefined ? "Periksa kembali!" : data.message);
        e.preventDefault();
    }
});

function validate() {
    var data = {};
    data.status = true;
    if (canOverride('.can-role') && !isRoleItemsChecked()) {
        data.status = false;
        data.message = "Anda belum memilih role user!";
        return data;
    }

    if (canOverride('.can-commodity')
        && canOverride('.can-selected-commodity')
        && !isCommodityItemsChecked()) {
        console.log('commodity level two open');
        data.status = false;
        data.message = "Anda belum memilih akses komoditas!";
        return data;
    }

    if (canOverride('.can-package')
        && canOverride('.can-selected-package')
        && jQuery('#added_package').children().length === 0) {
        data.status = false;
        data.message = "Anda belum memilih akses paket!";
        return data;
    }
    return data;
}

function canOverride(id) {
    return jQuery(id).is(':checked');
}

function isRoleItemsChecked() {
    var total = 0;
    $(".role-item-check:checked").each(function(){
        if (jQuery(this).val() !== '') {
            total++;
        }
    });
    return total > 0;
}

function isCommodityItemsChecked() {
    var total = 0;
    $(".commodity-item-check:checked").each(function(){
        total++;
    });
    return total > 0;
}

function showAlert(status, message) {
    if (status) {
        messageContainer.html('<div class="alert alert-danger fade in">' + message + '</div>');
    } else {
        messageContainer.html('');
    }
}
