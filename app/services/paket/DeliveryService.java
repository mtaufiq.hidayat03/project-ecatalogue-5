package services.paket;

import controllers.purchasing.PengirimanCtr;
import models.common.AktifUser;
import models.purchasing.base.BaseProductForm;
import models.purchasing.base.ProductJson;
import models.purchasing.pengiriman.LampiranPengiriman;
import models.purchasing.PaketProduk;
import models.purchasing.pengiriman.ProdukPengiriman;
import models.purchasing.pengiriman.form.DeliveryDataForm;
import models.purchasing.pengiriman.PaketRiwayatPengiriman;
import models.purchasing.riwayat.PaketRiwayat;
import models.purchasing.status.PaketStatus;
import org.apache.commons.io.FilenameUtils;
import play.Logger;
import play.i18n.Messages;
import play.mvc.Scope;
import repositories.paket.DeliveryRepository;
import repositories.paket.PackageProductRepository;
import utils.LogUtil;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 10/19/2017
 */
public class DeliveryService extends BaseService {

    public static final String TAG = "DeliveryService";

    public static DeliveryService instance() {
        return new DeliveryService();
    }

    public void store(DeliveryDataForm dataForm) {
        if (dataForm != null) {
            List<BaseProductForm> productDelivers = dataForm.getSavedProducts();
            PaketRiwayatPengiriman model;
            if (dataForm.isCreateMode()) {
                 model = new PaketRiwayatPengiriman(dataForm);
            } else {
                model = PaketRiwayatPengiriman.findById(dataForm.model);
                model.dataForm(dataForm);
            }
            model.saveHistory();
            new PaketRiwayat().addDeliveryHistory(model);
            saveAttachment(model, dataForm);
            saveProducts(model, dataForm, productDelivers);
            PaketStatus paketStatus = PaketStatus.getLatestStatusByPackageId(model.getPackageId());
            if (paketStatus != null) {
                paketStatus.updateStatusToDelivery();
            }
        }
    }

    private void saveProducts(PaketRiwayatPengiriman model,
                                     DeliveryDataForm dataForm, List<BaseProductForm> productDelivers) {
        Map<Long, PaketProduk> packageProducts = PackageProductRepository
                .getProducts(dataForm.packageId, dataForm.getIds(productDelivers));
        LogUtil.d(TAG, "packageProducts: " + packageProducts.size());
        if (!productDelivers.isEmpty()) {
            deleteProductsOnUpdate(dataForm);
            Map<Long, ProdukPengiriman> saved = null;
            if (!dataForm.isCreateMode()) {
                saved = DeliveryRepository
                        .getCurrentProductsIdKey(dataForm.packageId, dataForm.model);
            }
            for (BaseProductForm productForm : productDelivers) {
                PaketProduk paketProduk = packageProducts.get(productForm.packageProductId);
                if (paketProduk != null) {
                    if(productForm.productQuantity.longValue() > paketProduk.kuantitas.longValue()){
                        Scope.Flash.current().error("Jumlah Pengiriman tidak boleh lebih besar dari jumlah Pembelian");
                    }else {
                        if (productForm.isCreatedMode()) {
                            new ProdukPengiriman()
                                    .dataForm(paketProduk)
                                    .productDelivery(productForm)
                                    .pengiriman(model)
                                    .saveProduct();
                        } else {
                            if (saved != null && saved.containsKey(productForm.modelId)) {
                                ProdukPengiriman produkPengiriman = saved.get(productForm.modelId);
                                produkPengiriman.productDelivery(productForm).saveProduct();
                            }
                        }
                    }
                }
            }
        }
    }

    private void saveAttachment(PaketRiwayatPengiriman model, DeliveryDataForm dataForm) {
        if (dataForm.attachment != null) {
            int position = 1;
            if (!dataForm.isCreateMode()) {
                position = DeliveryRepository
                        .getAttachmentByPackageId(dataForm.packageId, dataForm.model).size() + 1;
                DeliveryRepository.deactivateAllAttachments(dataForm.packageId, dataForm.model);
            }
            new LampiranPengiriman()
                    .paketPengiriman(model)
                    .dataForm(dataForm)
                    .position(position)
                    .saveAttachment();
        }
    }

    private void deleteProductsOnUpdate(DeliveryDataForm model) {
        if (!model.isCreateMode()) {
            List<BaseProductForm> deleted = model.getDeletedProducts();
            if (!deleted.isEmpty()) {
                DeliveryRepository
                        .deactivateAllProducts(model.packageId,
                                model.model,
                                model.getIds(deleted)
                        );
            }
        }
    }

    public void updateAttachment(File attachment, Long deliveryId) {
        if (attachment != null) {
            LampiranPengiriman model = LampiranPengiriman.findById(deliveryId);
            if (model != null) {
                model.setFile(attachment);
                model.save();
            }
        }
    }

    public boolean deleteDelivery(Long deliveryId, AktifUser aktifUser) {
        Logger.debug(TAG + " delete delivery");
        PaketRiwayatPengiriman model = PaketRiwayatPengiriman.findById(deliveryId);
        if (model != null && model.isUserAllowed(aktifUser)) {
            model.softDelete();
            DeliveryRepository.deactivateAllProducts(model.paket_id, model.id, null);
            DeliveryRepository.deactivateAllAttachments(model.paket_id, model.id);
            return true;
        } else {
            Logger.debug(TAG + " model null or user not allowed");
            return false;
        }
    }

    public boolean deleteProduct(Long productId) {
        ProdukPengiriman model = ProdukPengiriman.findById(productId);
        if (model != null) {
            model.active = 0;
            model.save();
            return true;
        } else {
            return false;
        }
    }

    public List<ProductJson> searchProduct(Long packageId, String keyword, String excepted) {
        List<ProdukPengiriman> products = DeliveryRepository
                .searchProductsToBeDelivered(packageId, keyword, generateExcludeIds(excepted));
        return products.stream().map(p -> new ProductJson(p)).collect(Collectors.toList());
    }

    public List<ProductJson> getProductsById(Long packageId, Long id) {
        List<ProdukPengiriman> products = DeliveryRepository
                .getProductsByDeliveryIdAndPackage(packageId, id);
        return products.stream().map(p -> new ProductJson(p)).collect(Collectors.toList());
    }

}
