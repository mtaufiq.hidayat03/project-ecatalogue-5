package jobs;

import models.jcommon.config.Configuration;
import models.jcommon.util.CommonUtil;
import models.masterdata.Kurs;
import models.masterdata.KursNilai;

import org.apache.commons.lang.time.StopWatch;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import play.jobs.On;
import utils.KatalogUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

/** job di eksekusi setiap jam 08:39 pagi **/
// @On("0 39 08 * * ?")
@NoTransaction
public class UpdateKursJob extends Job {

	//TODO sebaiknya disimpan di DB->configuration
    private static final String biUrl = Configuration.getConfigurationValue("kurs.source.url");

    private static final int maxRetryCounter = Configuration.getConfigurationValue("kurs.max.retry") == null ? 0 :
            Integer.valueOf(Configuration.getConfigurationValue("kurs.max.retry")) ;

    private static final int retryDelay = Configuration.getConfigurationValue("kurs.retry.delay") == null ? 5 :
            Integer.valueOf(Configuration.getConfigurationValue("kurs.retry.delay")) ;

    private static final String userAgent = "Mozilla/5.0 (X11; U; Linux i586; en-US; rv:1.7.3) Gecko/20040924 Epiphany/1.4.4 (Ubuntu)";

    public void doJob() throws IOException, ParseException, InterruptedException {
    	//TODO Tambahkan mekanisme 'retry' jika gagal ambil dara
        int retryCounter = 0;
        // mekanisme retry
        while (retryCounter < maxRetryCounter) {
            Logger.info("[UpdateKursJob] Start job kurs..");
            try {
                Logger.debug("starting");
                Logger.debug("maxRetryCounter = %s", maxRetryCounter);
                Logger.debug("biUrl = %s", biUrl);
                Logger.debug("userAgent = %s", userAgent);
                final int timeoutInMillis = 10 * 1000;
                final Connection connection = Jsoup.connect(biUrl.replace("http:","https:")).userAgent(userAgent).timeout(timeoutInMillis);
                Document doc = connection.get();
                Logger.debug("getting");
//                Logger.debug("doc = %s", doc);
                Element table = doc.select("#ctl00_PlaceHolderMain_biWebKursTransaksiBI_GridView1").first();
                Logger.debug("table = %s", table);
                Logger.debug("kursBaruList = ");
                List<Elements> kursBaruList = validateKursBaru(table);
                for(Elements e:kursBaruList) Logger.debug("e = %s", e.toString());
                Long jumlahNilaiKurs = KursNilai.getJumlahKursNilaiByTanggal(new Date());
                Logger.debug("jumlah Nilai Kurs = %s", jumlahNilaiKurs.toString());

                //check apakah kurs sudah di update hari ini?
                if (kursBaruList.size() > jumlahNilaiKurs) {
                    Logger.info("[UpdateKursJob] Sedang melakukan update kurs..");
                    for (Elements kursBaru : kursBaruList) {
                        final String kursBaruText = kursBaru.get(0).text().trim();
                        Logger.debug("kursBaruText = %s", kursBaruText);
                        Kurs kurs = Kurs.findByNamaKurs(kursBaruText);
                        Logger.debug("kurs = %s", kurs.nama_kurs);
                        Logger.debug("kursBaru = %s", kursBaru.toString());
                        final String nilaiJualString = kursBaru.get(2).text().trim();
                        final String nilaiBeliString = kursBaru.get(3).text().trim();
                        Logger.debug("kursBaru(2) = %s", nilaiJualString);
                        Logger.debug("kursBaru(3) = %s", nilaiBeliString);
                        final BigDecimal nilaiJualDecimal = KatalogUtils.parseKursCurrency(nilaiJualString);
                        final BigDecimal nilaiBeliDecimal = KatalogUtils.parseKursCurrency(nilaiBeliString);
                        final Double nilai_jual = nilaiJualDecimal.doubleValue();
                        final Double nilai_beli = nilaiBeliDecimal.doubleValue();
                        Logger.debug("nilai_jual = %s", nilai_jual.toString());
                        Logger.debug("nilai_beli = %s", nilai_beli.toString());

                        KursNilai kursNilai = KursNilai.getByTanggalAndKurs(new Date(), kurs.id);
                        if (kursNilai == null) {
                            kursNilai = new KursNilai();
                            kursNilai.kurs_id_from = kurs.id;
                            kursNilai.tanggal_kurs = new Date();
                        }
                        Logger.debug("KursNilai fetched");
                        kursNilai.kurs_sumber_id = 1l;
                        kursNilai.nilai_jual = nilai_jual;
                        kursNilai.nilai_beli = nilai_beli;
                        kursNilai.nilai_tengah = (kursNilai.nilai_beli + kursNilai.nilai_jual) / 2;
                        Logger.debug("KursNilai saving");
                        long id = kursNilai.save();
                        Logger.debug("KursNilai saved");
                        Logger.info("[UpdateKursJob] Saved/Updated Kurs : " + id + " - " + kurs.nama_kurs);
                    }

                    Logger.info("[UpdateKursJob] Update kurs selesai.. ");
                }

            } catch (Exception e) {
                Logger.error("[UpdateKursJob] Terjadi Kesalahan update Kurs : %s", e.getLocalizedMessage());
//                if (maxRetryCounter != 0) {
//                    retryCounter++;
//                    Logger.error("[UpdateKursJob] Retry Job " + retryCounter + " of " + maxRetryCounter);
//                    if (retryCounter > maxRetryCounter)
//                        break;
//                    // delay setiap retry, default 5 detik
//                    Thread.sleep(retryDelay*1000);
//                }
                throw(e);
            }
            Logger.info("[UpdateKursJob] Job kurs selesai..");
            break;
        }

    }

    private static List<Elements> validateKursBaru(Element table) throws ParseException {

        List<Elements> elementsList = new ArrayList<>();

        for (Element row : table.select("tr")) {

            Elements cols = row.select("td");

            if (CommonUtil.isEmpty(cols))
                continue;

            String currency = cols.get(0).text().trim();

            if (!CommonUtil.isEmpty(currency)) {

                Kurs kurs = Kurs.findByNamaKurs(currency);

                if (kurs != null)
                    elementsList.add(cols);

            }

        }

        return elementsList;

    }

}
