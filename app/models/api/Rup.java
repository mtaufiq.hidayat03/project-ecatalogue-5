package models.api;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.sql.Timestamp;
import java.util.Date;

@Table(name = "rup")
public class Rup extends BaseTable {

    @Id
    public Long id;

    public String nama;
    public String kegiatan;
    public int jenis_belanja;
    public int jenis_pengadaan;
    public String volume;
    public int metode_pengadaan;
    public Date tanggal_awal_pengadaan;
    public Date tanggal_akhir_pengadaan;
    public Date tanggal_awal_pekerjaan;
    public Date tanggal_akhir_pekerjaan;
    public String lokasi;
    public String keterangan;
    public int tahun_anggaran;
    public Long id_sat_ker;
    public String kode_kldi;
    public boolean aktif;
    public boolean is_delete;
    public Timestamp audit_update;
    public String kode_anggaran;
    public String sumber_dana;

    @Transient
    public String nama_kldi;
    @Transient
    public String nama_satker;
    @Transient
    public String jenis_kldi;
    @Transient
    public int prp_id;
    @Transient
    public int kbp_id;

    public void setRupFromSirup(Sirup sirup){
        id = sirup.id;
        nama = sirup.nama;
        kegiatan = sirup.kegiatan;
        jenis_belanja = null == sirup.jenisBelanja ?0:sirup.jenisBelanja ;
        jenis_pengadaan = null == sirup.jenisPengadaan?0:sirup.jenisPengadaan;
        volume = sirup.volume;
        metode_pengadaan = null == sirup.metodePengadaan?0:sirup.metodePengadaan;
        tanggal_awal_pengadaan= new Timestamp(sirup.getTanggalAwalPengadaan().getTime());
        tanggal_akhir_pengadaan = new Timestamp(sirup.getTanggalAkhirPengadaan().getTime());
        tanggal_awal_pekerjaan = new Timestamp(sirup.getTanggalAwalPekerjaan().getTime());
        tanggal_akhir_pekerjaan = new Timestamp(sirup.getTanggalAkhirPekerjaan().getTime());
        lokasi = sirup.lokasi;
        keterangan = sirup.keterangan;
        tahun_anggaran = sirup.tahunAnggaran;
        id_sat_ker = sirup.idSatker;
        kode_kldi = sirup.kodeKldi;
        aktif = sirup.aktif;
        is_delete = sirup.is_delete;
        audit_update = new Timestamp(sirup.auditupdate.getTime());
        kode_anggaran = sirup.kodeAnggaran;
        sumber_dana = sirup.sumberDanaString;

    }

    public void setRupFromSirupById(SirupById sirup){
        id = sirup.id;
        nama = sirup.nama;
        kegiatan = sirup.kegiatan;
        //jenis_belanja = null == sirup.jenis_pengadaan  ?0:sirup.jenisBelanja ;
        jenis_pengadaan = null == sirup.jenis_pengadaan?0:sirup.jenis_pengadaan;
        volume = sirup.volume;
        metode_pengadaan = null == sirup.metode_pengadaan?0:sirup.metode_pengadaan;
        tanggal_awal_pengadaan= new Timestamp(sirup.getTanggalAwalPengadaan().getTime());
        tanggal_akhir_pengadaan = new Timestamp(sirup.getTanggalAkhirPengadaan().getTime());
        tanggal_awal_pekerjaan = new Timestamp(sirup.getTanggalAwalPekerjaan().getTime());
        tanggal_akhir_pekerjaan = new Timestamp(sirup.getTanggalAkhirPekerjaan().getTime());
        //lokasi = sirup.lokasi;
        keterangan = sirup.keterangan;
        tahun_anggaran = sirup.tahun_anggaran;
        id_sat_ker = sirup.id_satker;
        kode_kldi = sirup.kode_kldi;
        aktif = sirup.aktif;
        is_delete = sirup.is_delete;
        audit_update = new Timestamp(sirup.auditupdate.getTime());
        //kode_anggaran = sirup.kode_;
        sumber_dana = sirup.sumber_dana_string;

    }

    public static Rup findByIdRup(Long id){
        String query = "SELECT rup.*, kldi.nama nama_kldi, ks.nama nama_satker " +
                "FROM rup " +
                "LEFT JOIN kldi ON kldi.id = rup.kode_kldi " +
                "LEFT JOIN kldi_satker ks ON ks.id = rup.id_sat_ker " +
                "WHERE rup.id = ? ";
        return Query.find(query,Rup.class, id).first();
    }

    public static Rup getRupForPaket(Long id){
        String query = "SELECT r.id, r.nama, r.tahun_anggaran, r.id_sat_ker, " +
                " k.id kode_kldi, k.nama nama_kldi, k.jenis jenis_kldi, k.prp_id, k.kbp_id " +
                " FROM rup r " +
                " JOIN kldi k on r.kode_kldi = k.id " +
                " WHERE r.id = ? ";
        return Query.find(query, Rup.class, id).first();
    }
}
