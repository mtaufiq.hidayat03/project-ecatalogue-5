package models.prakatalog;

import models.BaseKatalogTable;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by raihaniqbal on 8/4/2017.
 */
@Table(name = "usulan_kategori_produk")
public class UsulanKategoriProduk extends BaseKatalogTable {

	@Id
	public Long id;

	@Required
	public Long usulan_id;

	@Required
	public Long produk_kategori_id;

	@Transient
	public String nama_kategori;

	public boolean active;

	public UsulanKategoriProduk() {}

	public UsulanKategoriProduk(Long usulanId, Long categoryId) {
		this.usulan_id = usulanId;
		this.produk_kategori_id = categoryId;
		this.active = true;
	}

	public static List<UsulanKategoriProduk> findByUsulan(Long usulan_id){
		String query = "SELECT ukp.produk_kategori_id, pk.nama_kategori " +
				"FROM usulan_kategori_produk ukp JOIN produk_kategori pk ON ukp.produk_kategori_id = pk.id " +
				"WHERE ukp.usulan_id = ? AND ukp.active = ? ";

		return Query.find(query, UsulanKategoriProduk.class, usulan_id, AKTIF).fetch();
	}

	public static Set<Long> getCategoryIdByUsulan(Long id) {
		List<UsulanKategoriProduk> list = findByUsulan(id);
		if (!list.isEmpty()) {
			return list.stream().map(u -> u.produk_kategori_id).collect(Collectors.toSet());
		}
		return Collections.emptySet();
	}

    public void reactivate() {
        active = true;
        deleted_by = null;
        deleted_date = null;
        save();
    }


}
