package features.komoditas;

import features.komoditas.repositories.KomoditasRepositoryContract;
import models.katalog.Komoditas;
import play.db.jdbc.Query;

import java.util.List;

/**
 * @author HanusaCloud on 3/27/2020 9:13 AM
 */
public class KomoditasRepository implements KomoditasRepositoryContract {

    private static KomoditasRepository komoditasRepository;

    public static KomoditasRepository getInstance() {
        if (komoditasRepository == null) {
            komoditasRepository = new KomoditasRepository();
        }
        return komoditasRepository;
    }

    @Override
    public List<Komoditas> getUmkms() {
        return Query.find("SELECT k.id, k.nama_komoditas FROM `produk` p " +
                "LEFT JOIN komoditas k ON p.komoditas_id = k.id " +
                "WHERE p.is_umkm = true " +
                "GROUP BY k.id, k.nama_komoditas", Komoditas.class).fetch();
    }

}
