package models.elasticsearch.product;

import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class PriceListHit {

    @SerializedName("hits")
    public PriceHit priceHit;

}
