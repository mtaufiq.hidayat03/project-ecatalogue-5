package models.prakatalog;

import models.BaseKatalogTable;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import play.Logger;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import java.util.List;

import javax.persistence.Transient;
import java.io.File;

/**
 * Created by dadang on 7/21/17.
 */
@Table(name = "dokumen_penawaran")
public class DokumenPenawaran extends BaseKatalogTable {

	@Id
	public Long id;

	public Long penawaran_id;

	public String dok_judul;

	public String dok_keterangan;

	public String dok_file_name;

	public String dok_hash;

	public String dok_signature;

	@Transient
	public String jenis_evaluasi;

	public Long dok_id_attachment;

	public boolean active;

	public static void simpanDokPenawaran(Long penawaran_id, File file, String dok_keterangan, String nama_dokumen) throws Exception {

		DokumenPenawaran dokumenPenawaran = new DokumenPenawaran();
		BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, dokumenPenawaran.dok_id_attachment, DokumenPenawaran.class.getSimpleName());
		dokumenPenawaran.dok_id_attachment = blob.id;
		dokumenPenawaran.dok_judul = nama_dokumen;
		dokumenPenawaran.dok_file_name = blob.blb_nama_file;
		dokumenPenawaran.dok_hash = blob.blb_hash;
		dokumenPenawaran.dok_keterangan = dok_keterangan;
		dokumenPenawaran.penawaran_id = penawaran_id;
		dokumenPenawaran.active = true;

		try{
			Long iddp = new Long(dokumenPenawaran.save());
			if(iddp >0){
				blob.blb_id_content = iddp;
				blob.save();
			}
		}catch (Exception e){}

	}

    public static DokumenPenawaran setNewDokPenawaran(Long penawaran_id, File file, String dok_keterangan, String nama_dokumen) throws Exception {
		DokumenPenawaran dokumenPenawaran = new DokumenPenawaran();

			BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, dokumenPenawaran.dok_id_attachment, DokumenPenawaran.class.getSimpleName());
			dokumenPenawaran.dok_id_attachment = blob.id;
			dokumenPenawaran.dok_judul = nama_dokumen;
			dokumenPenawaran.dok_file_name = blob.blb_nama_file;
			dokumenPenawaran.dok_hash = blob.blb_hash;
			dokumenPenawaran.dok_keterangan = dok_keterangan;
			dokumenPenawaran.penawaran_id = penawaran_id;
			dokumenPenawaran.active = true;

//        try{
//            Long iddp = new Long(dokumenPenawaran.save());
//            if(iddp >0){
//                blob.blb_id_content = iddp;
//                blob.save();
//            }
//        }catch (Exception e){}
        return dokumenPenawaran;
    }

	public static void editDokPenawaran(Long penawaran_id, File file, String dok_keterangan, String nama_dokumen, Long dok_id){
		Logger.debug("penawaran_id : "+penawaran_id);
		DokumenPenawaran oldDokPenawaran = DokumenPenawaran.findById(dok_id);
		oldDokPenawaran.active = false;
		oldDokPenawaran.save();

		try{
			simpanDokPenawaran(penawaran_id,file,dok_keterangan,nama_dokumen);
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	public static DokumenPenawaran findByDokId(long dok_id_attachment){

		return DokumenPenawaran.find("dok_id_attachment = ?", dok_id_attachment).first();
	}

	public static List<DokumenPenawaran> listDokumenPenawaran(long penawaran_id){

		String query ="SELECT dp.id, dp.penawaran_id, dp.dok_judul, dp.dok_keterangan, dp.dok_file_name, dp.dok_hash, dp.dok_id_attachment, udp.jenis_evaluasi FROM dokumen_penawaran dp LEFT JOIN usulan_dokumen_penawaran udp ON udp.nama_dokumen = dp.dok_judul WHERE dp.penawaran_id="+penawaran_id+" and dp.active=1 GROUP BY dp.dok_id_attachment";
		return Query.find(query,DokumenPenawaran.class).fetch();
	}

}
