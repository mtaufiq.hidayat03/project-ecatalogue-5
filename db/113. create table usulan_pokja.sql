CREATE TABLE `usulan_pokja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usulan_id` int(11) NOT NULL,
  `pokja_id` int(11) NOT NULL,
  `active` int(2) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `modified_date` timestamp NULL DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `deleted_date` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_usulan_pokja_id` (`usulan_id`,`pokja_id`, `active`)
) ENGINE=InnoDB AUTO_INCREMENT=22229 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

insert into usulan_pokja(usulan_id, pokja_id, active, created_date, created_by)
select id, pokja_id, active, NOW(), -99 from usulan where pokja_id is not null;

commit;