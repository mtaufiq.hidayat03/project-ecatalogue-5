package model;

import static org.junit.Assert.*;

import models.katalog.*;
import org.junit.Test;
import play.test.*;

public class ProdukTest extends UnitTest {

	//@Test
	public void produkNasionalHarusPunyaHarga() {
		Integer idProdukNasional=348664;
		Produk p = Produk.findById(idProdukNasional);
		p.withPrice();
		assertNotNull(p.produkHarga);
		assertNotNull(p.produkHarga.harga_xls_json);
	}

	//@Test
	public void produkKabupatenHarusPunyaHarga() {
		Integer idProdukKabupaten=348667;
		Produk p = Produk.findById(idProdukKabupaten);
		p.withPrice();
		assertNotNull(p.produkHarga);
		assertNotNull(p.produkHarga.harga_xls_json);
	}

}
