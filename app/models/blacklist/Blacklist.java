package models.blacklist;

import java.util.Date;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

/**
 * @author Reza
 */
@Table(name = "blacklist")
public class Blacklist extends BaseTable{

	@Id
	public Long id;
	public Long blacklist_source_id;
	public String rkn_id;
	public String repo_id;
	public String sk;
	public String nama_penyedia;
	public String npwp;
	public String alamat;
	public String alamat_tambahan;
	public String direktur;
	public String npwp_direktur;
	public Integer tahun_anggaran;
	public Date created_at;
	public String sk_pencabutan;
	public Long provinsi_id;
	public String provinsi_nama;
	public Long kabupaten_id;
	public String kabupaten_nama;
	public Long satker_id;
	public String satker_nama;
}
