package models.purchasing.pengiriman;

import models.purchasing.PaketProduk;
import models.purchasing.base.BaseProductForm;
import models.purchasing.base.BaseProduk;
import play.db.jdbc.*;

/**
 * @author HanusaCloud on 10/18/2017
 */
@Table(name = "paket_pengiriman_produk")
public class ProdukPengiriman extends BaseProduk {

    public Long paket_pengiriman_id;

    public ProdukPengiriman() {}

    public ProdukPengiriman productDelivery(BaseProductForm model) {
        this.kuantitas = model.productQuantity;
        this.deskripsi = model.productNote;
        return this;
    }

    public ProdukPengiriman dataForm(PaketProduk model) {
        this.produk_id = model.produk_id;
        this.paket_produk_id = model.id;
        this.paket_id = model.paket_id;
        return this;
    }

    public ProdukPengiriman pengiriman(PaketRiwayatPengiriman model) {
        this.paket_pengiriman_id = model.id;
        return this;
    }

    public void saveProduct() {
        final long result = (long) save();
        if (this.id == null) {
            this.id = result;
        }
    }

}
