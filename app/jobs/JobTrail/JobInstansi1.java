package jobs.JobTrail;

import jobs.instansisatker.ExtInstansiData;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;

import java.util.UUID;

@NoTransaction
public class JobInstansi1 extends Job {
    String uniqueID = UUID.randomUUID().toString();
    ExtInstansiData ext = new ExtInstansiData();

    @Override
    public void before() {
        Logger.info("job id:" + uniqueID);
        super.before();
    }

    public void doJob() {
        ext.jalankan();
    }
}
