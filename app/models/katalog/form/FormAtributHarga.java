package models.katalog.form;

import com.google.gson.Gson;
import models.katalog.komoditasmodel.KomoditasAtributHarga;
import utils.LogUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by raihaniqbal on 9/7/17.
 */
public class FormAtributHarga implements Serializable {

    public List<Long> atribut_id;
    public long komoditas_id;
    public List<String> nama_atribut;
    public Integer ongkir;
    public Integer harga_utama;
    public Integer harga_pemerintah;
    public Integer harga_retail;

    public FormAtributHarga(){
        this.atribut_id = new ArrayList<>();
        this.nama_atribut = new ArrayList<>();
    }

    public FormAtributHarga(List<KomoditasAtributHarga> hargaList, long komoditas_id){
        this.atribut_id = new ArrayList<>();
        this.nama_atribut = new ArrayList<>();
        this.komoditas_id = komoditas_id;

        int counter = 0;
        for(KomoditasAtributHarga harga : hargaList){
            LogUtil.d("harga",new Gson().toJson(harga));
            this.atribut_id.add(harga.id);
            this.nama_atribut.add(harga.label_harga);

            if(this.ongkir == null){
                this.ongkir = harga.apakah_ongkir ? counter : null;
            }

            if(this.harga_utama == null){
                this.harga_utama = harga.apakah_harga_utama ? counter : null;
            }

            if(this.harga_pemerintah == null){
                this.harga_pemerintah = harga.apakah_harga_pemerintah ? counter : null;
            }

            if(this.harga_retail == null){
                this.harga_retail = harga.apakah_harga_retail ? counter : null;
            }

            counter++;
        }
    }

}
