package models.util;

import play.Logger;
import play.Play;

import java.lang.reflect.Field;

/**Ini untuk load konfigurasi yang didapat dari
 * 1. application.conf
 * 2. System.getProperty(): -Dconfig=xxxx
 * System.getProperty akan meng-override application.conf
 * 
 * @author Andik Yulianto
 *
 */
public class Config {
	private static Config config=null;
	private Config()
	{
		
	}
	 /************* Ketentuan ***********
	  * Field harus bertipe 
	  * 1. primitif (int, boolean, long)
	 *  2. String
	 */
	
	/**Ini penting saat development agar fungsi Cache di controller dinonaktifkan; sedangkan saat production
	 * Cache harus aktif untuk meningkatkan performa
	 */
	public boolean playCacheDisabled;
	
	/**JIka kosong maka image dan file akan di-download dari server utama
	 * , jika tidak maka URL akan mengarah ke server yang dimaksud
	 * contoh: cdnServerPrefix=https://e-katalog.lkpp.go.id/thumb/300x300/files/upload
	 */
	public String cdnServerImage300;
	public String cdnServerImage100;
	
	//lokasi penyimpanan image/file
	public String fileStorageDir;
	
	/**Secara default, semua contentType='text/*' akan diproses dengan gzipEncoding.
	 * Namun untuk kondisi tertentu mungkin tidak diinginkan
	 */
	public boolean gzipEncodingDisabled;
	
	//centrum
	public String centrum_url;
	public String centrum_client_id;
	public String centrum_client_secret;
	//datatable CACHE untuk COUNT() 
	public String datatableCacheExpires;

	//ADP
	public String adp_login_api;
	public String adp_detail_user;
	public String adp_rekanan_search;
	public String adp_aktivasi_rekanan;
	public String adp_rekanan_terpilih;
	public String adp_detail_user_by_username;
	public String adp_url ;

	public String elasticsearchHost;
	public String elasticsearchIndex;
	public String pengirimanLampiranDir;

	//RUP
	public String sirup_api;
	public int repo_id;
	
	//WILAYAH
	public String propinsi_api;
	public String kabupaten_api;
	//BLACKLIST
	public String blacklist_api;
	public String blacklist_api_bynpwp;

	//INSTANSI SATKER
	public String instansi_api;
	public String satker_api;

	//INSTANSI SUMBER DANA
	public String sumber_dana_api;

	// SIKAP
	public String sikap_api;

	public boolean monitoringEnable;

	public String fileImageGallery;
	public String storageChat;

	public boolean prodDev;
	public boolean aktifUserDb;
	public String aktifUserFileLocation;
	public String aktifUserLogTrail;
	public String dataFeedLogPath;
	public boolean writeLogDataFeed;
	public boolean writeUserLogActivity;
	public boolean writeLogActivity;
	public boolean maintenanceMode = false;
	public String allowUserInmaintenance ="";

	public static Config getInstance()
	{
		if(config==null)
		{
			config=new Config();
			//read from application.conf first, then System.
			Class clz=Config.class;
			Field[] fields=clz.getFields();
			for(Field field: fields)
			{
				String fieldType=field.getType().getName();
				String value=Play.configuration.getProperty(field.getName(), null);
				if(value==null)
					value=System.getProperty(field.getName(), null);
				try {
					if(value!=null)
					{
						if(fieldType.equals("boolean"))
							field.set(config, Boolean.valueOf(value));
						else
						if(fieldType.equals("int"))
							field.set(config, Integer.valueOf(value));
						else
						if(fieldType.equals("long"))
							field.set(config, Long.valueOf(value));
						else //string
							field.set(config, value);

					}
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		return config;
	}

	public String getElasticurl(){
		return this.elasticsearchHost+"/"+this.elasticsearchIndex;
	}
	

}
