package TestStagging;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import models.katalog.Produk;
import models.permohonan.ISO8601DateAdapter;
import models.permohonan.PembaruanStaging;
import models.permohonan.ProdukStagging;
import org.junit.Test;
import play.Logger;
import play.test.UnitTest;

import java.util.Date;

public class TestStagging extends UnitTest {

    @Test
    public void test(){

        //converter harus didefinisikan dari converter yang sama dan type yang sama
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new ISO8601DateAdapter())
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .create();

        Produk produk = Produk.findById(762773l);
        ProdukStagging pstg = new ProdukStagging();
        pstg.info = true;
        pstg.permohonan_pembaruan_id = 0l;
        pstg.produk_id = produk.id;
        pstg.penyedia_id = produk.penyedia_id;
        pstg.object_json = gson.toJson(produk);


        PembaruanStaging stg = new PembaruanStaging();
        stg.permohonan_pembaruan_id = 0l;
        stg.class_name = pstg.getClass().getCanonicalName();
        stg.isList = false;
        stg.object_json = gson.toJson(pstg);
        stg.active=true;



        ProdukStagging psg = gson.fromJson(stg.object_json, ProdukStagging.class );

        //String jstr = new Gson().toJson(psg.singleObjek);
        //Produk pnew = Produk.class.cast(psg.singleObjek);
        //Produk pnew = gson.fromJson(psg.object_json, Produk.class);

        //contoh convert objek dengan custom class dari nama class
        try {
            Produk p = (Produk) gson.fromJson(stg.object_json, Class.forName(Produk.class.getCanonicalName()));
            Logger.info(gson.toJson(p));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }



}
