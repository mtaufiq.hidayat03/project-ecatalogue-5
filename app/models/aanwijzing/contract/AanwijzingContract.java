package models.aanwijzing.contract;

import org.apache.commons.lang3.StringUtils;

public interface AanwijzingContract {

    String TAG = "AanwijzingContract";

    String getQuestion();
    String getAnswer();

    default String cleanContentFromTags(String value) {
        if (StringUtils.isEmpty(value)) {
            return value;
        }
        return value.replaceAll("\\<.*?\\>", "")
                .replaceAll("&nbsp;", " ");
    }

    default String cleanAndAbbreviateQuestion() {
        return cleanContentFromTags(getQuestion());
    }

    default String cleanAndAbbreviateAnswer() {
        return cleanContentFromTags(getAnswer());
    }

}
