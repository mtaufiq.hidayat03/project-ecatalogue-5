package models.util.produk;

/**
 * Created by dadang on 8/30/17.
 */
public class PersetujuanProduk {

	public long id_permintaan;
	public long id_produk;
	public String kode_produk;
	public String nama_produk;
	public long id_komoditas;
	public String nama_komoditas;
	public String nama_penyedia;
	public long id_penyedia;

}
