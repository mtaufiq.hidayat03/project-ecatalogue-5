package models.katalog.komoditasmodel;

import com.google.gson.Gson;
import models.BaseKatalogTable;
import models.katalog.ProdukHarga;
import models.katalog.form.FormAtributHarga;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Table(name = "komoditas_harga_atribut")
public class KomoditasAtributHarga extends BaseKatalogTable {

	@Id
	public Long id;

	public Long komoditas_id;

	@Required
	public String label_harga;

	public boolean apakah_ongkir;
	public boolean apakah_harga_utama;
	public boolean apakah_harga_retail;
	public boolean apakah_harga_pemerintah;
	public boolean active;

	public boolean isOngkir(){
		return this.apakah_ongkir;
	}

	public static void saveAtributHarga(FormAtributHarga atributHarga){
		if (atributHarga.atribut_id != null && !atributHarga.atribut_id.isEmpty()
				&& atributHarga.nama_atribut != null && !atributHarga.nama_atribut.isEmpty()) {
			Iterator<String> stringIterator = atributHarga.nama_atribut.iterator();
			Iterator<Long> longIterator = atributHarga.atribut_id.iterator();
			int i = 0;
			while (stringIterator.hasNext() && longIterator.hasNext()) {
				String name = stringIterator.next();
				Logger.info("=== name ==== " + name);
				Long atributId = longIterator.next();
				if (!TextUtils.isEmpty(name) && atributId != null) {
					KomoditasAtributHarga kah = new KomoditasAtributHarga();
					if (atributId != null && atributId != 0) {
						kah = KomoditasAtributHarga.findById(atributId);
					}

					kah.label_harga = name;
					kah.komoditas_id = atributHarga.komoditas_id;

					kah.apakah_ongkir = atributHarga.ongkir != null && atributHarga.ongkir == i;
					kah.apakah_harga_retail = atributHarga.harga_retail != null && atributHarga.harga_retail == i;
					kah.apakah_harga_utama = atributHarga.harga_utama != null && atributHarga.harga_utama == i;
					kah.apakah_harga_pemerintah = atributHarga.harga_pemerintah != null && atributHarga.harga_pemerintah == i;

					kah.active = true;

					kah.save();
				}
				i++;
			}
		}
	}

	public static List<KomoditasAtributHarga> findByKomoditas(long komoditas_id){
//		return find("komoditas_id = ? and active = ? and (apakah_ongkir = ? || label_harga not like '%ongkos kirim%')",komoditas_id, AKTIF, false).fetch();
		return find("komoditas_id = ? and active = ?",komoditas_id, AKTIF).fetch();
	}

	public static long countByKomoditas(long komoditas_id){
		return count("komoditas_id = ? and active = ?",komoditas_id, AKTIF);
	}

	public static List<String> findNamaNotOngkirByKomoditas(long komoditas_id){
		String query = "select label_harga from komoditas_harga_atribut where komoditas_id = ? and (apakah_ongkir = ? || label_harga not like '%ongkos kirim%') and active = ?";
		return Query.find(query,String.class,komoditas_id,false, AKTIF).fetch();
	}

	public static KomoditasAtributHarga findHargaUtamaByKomoditas(long komoditas_id){
		String query = "select * from komoditas_harga_atribut where komoditas_id = ? and apakah_harga_utama = ? and active = ?";
		return Query.find(query,KomoditasAtributHarga.class,komoditas_id,true, AKTIF).first();
	}

	public static List<KomoditasAtributHarga> findHargaNonOngkirByKomoditas(long komoditas_id){
		String query = "komoditas_id = ? and (apakah_ongkir = ? || label_harga not like '%ongkos kirim%') and active = ?";
		return find(query,komoditas_id,false, AKTIF).fetch();
	}

	public static String findNamaOngkirByKomoditas(long komoditas_id){
		String query = "select label_harga from komoditas_harga_atribut where komoditas_id = ? and apakah_ongkir = ? and active = ?";
		return Query.find(query,String.class,komoditas_id,true, AKTIF).first();
	}

	public static void softDelete(long id){
		Query.update("update komoditas_harga_atribut set active = ? where id = ?", NOT_AKTIF,id);
	}

	public static String getJsonUsedAtributFromList(List<Long> list){
		List<Long> result = new ArrayList<>();
		for(Long id : list){
			Long countProdukHarga = ProdukHarga.countByHargaAtribut(id);
			if(countProdukHarga > 0){
				result.add(id);
			}
		}

		return new Gson().toJson(result);
	}

}

