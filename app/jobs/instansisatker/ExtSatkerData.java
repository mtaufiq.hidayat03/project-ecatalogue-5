package jobs.instansisatker;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import jobs.JobTrail.JobSatker1;
import models.api.DceSatker;
import models.jcommon.config.Configuration;
import models.jcommon.http.SimpleHttpRequest;
import models.masterdata.Satker;
import models.util.Config;
import play.Logger;
import play.jobs.Job;
import utils.KatalogUtils;

import java.io.IOException;
import java.util.*;

public class ExtSatkerData {
	private Properties propJob;
	public static final String DEFAULT_WAIT_TIME = "job.sirup.waittime";
	public static final String DEFAULT_WAIT_TIME_NEXT = "job.sirup.waittime.next";
	private String jsResult = "[]";
	private Boolean resultGet = false;
	public List<DceSatker> satkerList = new ArrayList<>();
	private int defaultWaitTime = 0;
	private String satkerUrl = Config.getInstance().satker_api;
	private int nextWaitTime = 0;
	public String instansiId = "";
	private int delayTaskJob = 5;// second
	private int delayJob = 10;// second
	//
	public ExtSatkerData(String instansiId) {
		propJob = new Properties();
		propJob.put(DEFAULT_WAIT_TIME,
				Configuration.getConfigurationValue(DEFAULT_WAIT_TIME, ""));
		propJob.put(DEFAULT_WAIT_TIME_NEXT, Configuration
				.getConfigurationValue(DEFAULT_WAIT_TIME_NEXT, ""));

		defaultWaitTime = Integer
				.parseInt(propJob.getProperty(DEFAULT_WAIT_TIME));
		nextWaitTime = Integer
				.parseInt(propJob.getProperty(DEFAULT_WAIT_TIME_NEXT));
		instansiId = instansiId;

	}

	public List<DceSatker> getSatkerData() {
		return getSatkerData(defaultWaitTime);
	}
	public List<DceSatker> getSatkerData(Integer minuteTimeOut) {
		if (fetchSatkerData(this.instansiId, minuteTimeOut)) {
			this.satkerList = parseSatkerData();
		}
		return this.satkerList;
	}

	private Boolean fetchSatkerData(String instansiId, Integer minuteTimeout) {
		resultGet = false;
		jsResult = "[]";
		Date tgl_update = new Date();
		String date = KatalogUtils.toDateyyyyMMdd(tgl_update);
		Integer seconds = 0;
		if (minuteTimeout > 0) {
			seconds = (minuteTimeout * 60);
		}
		Logger.info("run with minuteTimeout: " + minuteTimeout + ", and date "
				+ tgl_update.toLocaleString());
		try (SimpleHttpRequest connect = new SimpleHttpRequest(seconds)) {
			String params = instansiId;
			connect.get(satkerUrl + params);
			jsResult = connect.getString();
			resultGet = true;
		} catch (InterruptedException e) {
			Logger.error("Intesatkerted Exception:" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Logger.error("IO Exception:" + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		if (resultGet == false) {
			defaultWaitTime++;
			nextWaitTime = defaultWaitTime;
			// Configuration.updateConfigurationValue(DEFAULT_WAIT_TIME_NEXT,
			// String.valueOf(newWait));
			// propJob.replace(DEFAULT_WAIT_TIME_NEXT, String.valueOf(newWait));

			 newJob();
		}
		return resultGet;
	}

	private List<DceSatker> parseSatkerData() {
		try {
			satkerList = new Gson().fromJson(jsResult,
					new TypeToken<List<DceSatker>>() {
					}.getType());
			Logger.info("total:" + satkerList.size());
		} catch (Exception e) {
			Logger.error("error parse data dcesatker:" + e.getMessage());
			e.printStackTrace();
		}
		return satkerList;
	}

	public void saveDataSatkerToDb() {
		List<Satker> failed = new ArrayList<>();
		List<Satker> success = new ArrayList<>();
		Logger.info("save data satker:");
		for (DceSatker s : satkerList) {
			if(s.isDeleted)
				continue;
			Satker satkerObj = Satker.findById(s.id);
			try {
				if (satkerObj != null) {
					satkerObj.nama = s.nama;
					satkerObj.idKldi = s.idKldi;
					satkerObj.idSatker = s.idSatker;
					satkerObj.statusPerubahan = s.statusPerubahan;
					int result = satkerObj.save();
					Logger.info("update:" + result);
				} else {
					satkerObj = new Satker();
					satkerObj.id = s.id.intValue();
					satkerObj.nama = s.nama;
					satkerObj.idKldi = s.idKldi;
					satkerObj.idSatker = s.idSatker;
					satkerObj.statusPerubahan = s.statusPerubahan;
					satkerObj.active = true;
					int result = satkerObj.save();
					Logger.info("save:" + result);
				}
				if (satkerObj != null ) {
					success.add(satkerObj);
				}
			} catch (Exception e) {
				if (satkerObj != null) {
					failed.add(satkerObj);
				}
				Logger.info("error save or update satker:" + e.getMessage());
				e.printStackTrace();
			}
		}
		Logger.info(">>>>>>>>>>save success total:" + success.size());
		for (Satker s : success) {
			Logger.info("~~~ "+ s.id + " " + s.nama);
		}

		Logger.info("<<<<<<<<<<save failed total:" + failed.size());
		for (Satker s : failed) {
			Logger.info("~~~ "+ s.id + " " + s.nama);
		}

	}

	private void newJob() {

		JobSatker1 job1 = new JobSatker1(this);
		job1.in(delayJob);
//		new Job() {
//			String uniqueID = UUID.randomUUID().toString();
//			@Override
//			public void before() {
//				Logger.info("job id:" + uniqueID);
//				super.before();
//			}
//
//			public void doJob() {
//				jalankan();
//			}
//		}.in(delayJob);

	}

	public void jalankan() {
//		newJob();
		Logger.info("on job execute, waittime:" + nextWaitTime);
//		getSatkerData(nextWaitTime);
		Logger.info("%%%%done getDate, saving now%%%");
		saveDataSatkerToDb();
		Logger.info("done job:" + satkerList.size());

	}

}
