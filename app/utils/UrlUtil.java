package utils;

import play.mvc.Router;

import java.util.HashMap;
import java.util.Map;

public class UrlUtil {

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private Map<String, Object> map = new HashMap<>();
		private String url = "";

		public Builder setParam(String key, Object object) {
			this.map.put(key, object);
			return this;
		}

		public Builder setUrl(String url) {
			this.url = url;
			return this;
		}

		public String build() {
			return Router.reverse(url, map).url;

		}
	}

}
