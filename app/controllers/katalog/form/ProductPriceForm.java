package controllers.katalog.form;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import org.apache.commons.lang.ArrayUtils;
import org.apache.http.util.TextUtils;
import utils.DateTimeUtils;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author HanusaCloud on 12/10/2017
 */
public class ProductPriceForm {

    public long atribut_id;
    public String value;
    public boolean isMain;

    public BigDecimal getValue(){
        return new BigDecimal(this.value.replaceAll("[^0-9.]",""));
    }
}
