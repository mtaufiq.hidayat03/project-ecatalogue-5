package models.lpse;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.Date;

/**
 * Created by dadang on 12/8/17.
 */
@Table(name = "ppe_site")
public class PpeSite extends BaseTable {

	@Id
	public Long pps_id;
	public Integer tag_id;
	public Integer stg_id;
	public String audittype;
	public String audituser;
	public Date auditupdate;
	public String pps_nama;
	public String pps_alamat;
	public String pps_display;
	public String pps_kontak;
	public String pps_proxy;
	public String pps_public_url;
	public String pps_private_url;
	public Date pps_tanggal_pendaftaran;
	public String pps_sk;

}
