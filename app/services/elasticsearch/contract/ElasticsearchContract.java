package services.elasticsearch.contract;

import com.google.gson.JsonElement;
import models.elasticsearch.request.RequestCount;
import models.elasticsearch.request.RequestSearch;
import models.elasticsearch.response.ResponseCount;
import models.elasticsearch.response.ResponseInputElastic;
import models.elasticsearch.response.ResponseProduct;
import models.elasticsearch.response.ResponseStandard;
import models.util.Config;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.*;

/**
 * @author HanusaCloud on 10/4/2017
 */
public interface ElasticsearchContract {

    //@POST("/ecatalogue/produk/_search")
    @POST
    Call<ResponseProduct> getProduct(@Body RequestSearch request, @Url String url);
    //@POST("/produk/_bulk")
    @POST
    Call<ResponseInputElastic> bulkInsertProduct(@Body RequestBody requestBody, @Url String urls);
    //@POST("/produk/_bulk")
    @POST
    Call<ResponseInputElastic> deleteProduk(@Body RequestBody requestBody, @Url String urls);
    //@POST("/produk/_count")
    @POST
    Call<ResponseCount> getTotalProduct(@Body RequestCount request, @Url String url);
    @POST("/ecatalogue-view-counter/_bulk")
    Call<ResponseInputElastic> bulkInputViewCounter(@Body RequestBody request);
    @DELETE
    Call<JsonElement> deleteCatalogue(@Url String url);
    @PUT
    Call<JsonElement> indexing(@Body RequestBody request, @Url String url);

    /*audit trail*/
    @PUT("/ecatalogue-audit-trail")
    Call<JsonElement> indexingAuditTrail(@Body RequestBody request);
    @POST("/ecatalogue-audit-trail/_bulk")
    Call<JsonElement> bulkAuditTrail(@Body RequestBody request);
    @DELETE("/ecatalogue-audit-trail")
    Call<JsonElement> deleteAuditTrail();

}
