package jobs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.time.StopWatch;

import com.google.gson.Gson;

import models.masterdata.Instansi;
import models.masterdata.Satker;
import play.Logger;
import play.libs.WS;

/**Job untuk mengambil data-data dari Sirup
 * Coding ini belum selesai, masih Proof Of Concept
 * @see https://gitlab.lkpp.go.id/eproc/e-katalog-5/issues/57
 * @author Andik Yulianto<andikyulianto@yahoo.com>
 */	
public class SirupApiJob extends TrackableJob {

	/**Konstanta ini di-copy dari SPSE4*/
	private static final String URL_API_PROPINSI = "http://report-lpse.lkpp.go.id/dce/aplikasi/getjsonprov";
	private static final String URL_API_KABUPATEN = "http://report-lpse.lkpp.go.id/dce/aplikasi/getjsonkab";
	private static final String URL_API_INSTANSI = "https://sirup.lkpp.go.id/sirup/service/daftarKLDI";
	private static final String URL_API_SATKER = "https://sirup.lkpp.go.id/sirup/service/daftarSatkerByKLDI?kldi=";
	private static final String URL_API_PAKET_SIRUP = "https://sirup.lkpp.go.id/sirup/service/paketPenyediaTampilEpurchasing?auditupdate=";

	public void doJob()
	{
		
		//Step #1: ambil daftar instansi
		StopWatch sw=new StopWatch();
		sw.start();
		String url="";
		try
		{
			url=URL_API_INSTANSI;
			String strInstansi=WS.url(url).get().getString();
			sw.stop();
			
			Gson gson =new Gson();
			Instansi[] aryInstansi=gson.fromJson(strInstansi, new Instansi[0].getClass());
			Logger.debug("\t[SirupApiJob] %s, Instansi: %,d, size: %,d, duration: %s", 
					URL_API_INSTANSI, aryInstansi.length, strInstansi.length(), sw);
			sw.reset();
			
			//Step #2: ambil daftar satker
			List<Satker> listSatker=new ArrayList<>();
			int i=0;
			for(Instansi instansi: aryInstansi)
			{	
				if(true) break; //DEV
				String strSatker=WS.url(URL_API_SATKER + instansi.id).get().getString();
				Satker[] arySatker=gson.fromJson(strSatker, Satker[].class);
				i++;
				if(arySatker!=null)
					listSatker.addAll(Arrays.asList(arySatker));
				Thread.sleep(500); //beri kesempatan Sirup 'bernafas'
				Logger.debug("\t[SirupApiJob] downloadSatker(%s): #%s, count: %,d",instansi.nama,  i, strSatker.length());
			}
		
			//Step #3: Dapatkan RUP e-purchasing
			url=URL_API_PAKET_SIRUP+"2017-07-19";
			String strRUP=WS.url(url).get().getString();
			Logger.debug("\t[SirupApiJob] downloadSirup, size: %,d",strRUP.length());
			
			//Step #4: Simpan hasilnya 
			/**Simpan ke DB HARUS dilakukan di akhir JOB supaya tidak blocking
			 * Kalau dilakukan di dalam for() {....} maka bisa jadi bloking karena WS.url bisa sangat lama
			 */
			for(Satker satker: listSatker)
				satker.save();
		}
		catch(Exception e)
		{
			Logger.error("\t[SirupApiJob] %s\nURL: %s", e, url);
		}
	}
}
