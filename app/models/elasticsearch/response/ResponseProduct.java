package models.elasticsearch.response;

import com.google.gson.annotations.SerializedName;
import models.elasticsearch.ProductCommodity;
import models.elasticsearch.SearchResult;
import models.elasticsearch.response.bucket.AggregationChild;
import models.elasticsearch.response.bucket.Bucket;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class ResponseProduct {

    @SerializedName("took")
    public int took;
    @SerializedName("time_out")
    public boolean timeOut = false;

    @SerializedName("hits")
    public ResponseHit hits = new ResponseHit();

    @SerializedName("aggregations")
    public Map<String, AggregationChild> aggregations = new HashMap<>();

    public SearchResult getPopularResult() {
        SearchResult result = new SearchResult();
        result.set(this);
        if (!aggregations.isEmpty()) {
            for (Map.Entry<String, AggregationChild> map : aggregations.entrySet()) {
                AggregationChild child = map.getValue();
                if (child != null) {
                    if (child.buckets != null && !child.buckets.isEmpty()) {
                        for (Bucket bucket : child.buckets) {
                            if (bucket.hitList != null) {
                                if (bucket.hitList.hits != null) {
                                    List<MetaProduct> hits = bucket.hitList.hits.hits;
                                    if (hits != null && !hits.isEmpty()) {
                                        for (MetaProduct metaProduct : hits ) {
                                            if (metaProduct.product != null) {
                                                ProductCommodity productCommodity = new ProductCommodity(metaProduct.product);
                                                productCommodity.total = bucket.doc_count;
                                                result.commodities.add(productCommodity);
                                                if (result.aggregateProducts.size() < 8) {
                                                    result.aggregateProducts.add(metaProduct);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

}
