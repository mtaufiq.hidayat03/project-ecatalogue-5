package models.prakatalog.search;

import ext.contracts.PaginateAble;
import ext.contracts.ParamAble;
import models.prakatalog.Penawaran;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class PenawaranSearchResult implements PaginateAble {

    public long total = 0;
    public List<Penawaran> items = new ArrayList<>();
    public ParamAble contract;


    public PenawaranSearchResult(ParamAble paramAble) {
        this.contract = paramAble;
    }

    @Override
    public long getTotal() {
        return total;
    }

    @Override
    public long getTotalCurrentPage() {
        return items.size();
    }

    @Override
    public int getMax() {
        return 20;
    }

    @Override
    public int getPage() {
        return contract.getPage();
    }

    @Override
    public List<BasicNameValuePair> getParams() {
        return contract.getParams();
    }

    @Override
    public void setTotal(Long total) {
        this.total = total;
    }
}
