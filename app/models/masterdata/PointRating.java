package models.masterdata;

import com.google.gson.JsonObject;
import models.BaseKatalogTable;
import models.common.ButtonGroup;
import models.jcommon.datatable.DatatableQuery;
import models.jcommon.datatable.DatatableResultsetHandler;
import play.Logger;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.i18n.Messages;
import play.mvc.Router;
import services.datatable.DataTableService;


import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Table(name = "point_rating")
public class PointRating extends BaseKatalogTable {

    @Id
    public Long id;

    @Required
    public String nama_point;

    public String deskripsi;

    public boolean active;

    public PointRating(){
        active = true;
    }
    @Transient
    private static String qField = " pr.id, pr.nama_point, pr.active ,pr.deskripsi";
    @Transient
    private static String qFrom = " point_rating pr ";

    public static JsonObject dataSrc(){
        JsonObject result = new JsonObject();
        try {

            DatatableQuery query =
                    new DatatableQuery(dataPointRating)
                            .select(qField)
                            .from(qFrom);
         result = query.executeQuery();
        }catch (Exception e){
            Logger.error("Error getDataSrc datatable PointRating:"+ e.getMessage());
        }
        return result;
    }

    //result set
    private static DatatableResultsetHandler<String[]> dataPointRating =
            new DatatableResultsetHandler<String[]>(qField) {
        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            Map<String, Object> params = new HashMap<String, Object>();
            final String id = rs.getString("pr.id");
            params.put("id", id);
            String urlEdit =Router.reverse("controllers.masterdata.pointRatingCtr.edit",params).url;
//          String urlDetail =Router.reverse("controllers.masterdata.pointRatingCtr.detail",params);
            String urlHapus =Router.reverse("controllers.masterdata.pointRatingCtr.delete",params).url;
            boolean active = rs.getInt("pr.active")==1;
            String classButton = active ? "class='btn btn-success btn-xs disabled'":"class='btn btn-danger btn-xs disabled'";

            List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();
            buttonGroups.add(new ButtonGroup(Messages.get("ubah.label"),urlEdit));
//          buttonGroups.add(new ButtonGroup(Messages.get("detail.label"),urlDetail));
            buttonGroups.add(new ButtonGroup(Messages.get("hapus.label"),urlHapus));
            String[] results=new String[columns.length+1];
            results[0] = rs.getString("pr.id");
            results[1] = rs.getString("pr.nama_point");
            results[2] = rs.getString("pr.deskripsi");
            results[3] = String.format("<a href='#' %s style='width:50px'>%s</a>", classButton, active ? Messages.get("ya.label"):Messages.get("tidak.label"));
            results[4] = DataTableService.generateButtonGroup(buttonGroups);

            return results;
        }
    };

}
