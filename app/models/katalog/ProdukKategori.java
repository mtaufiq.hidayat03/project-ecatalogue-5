package models.katalog;

import models.BaseKatalogTable;
import models.katalog.payload.ProdukKategoriPayload;
import play.cache.Cache;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.mvc.Router;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Table(name = "produk_kategori")
public class ProdukKategori extends BaseKatalogTable {
    @Id
    public Long id;
    public Long komoditas_id;
    public Long parent_id;
    public Long posisi_item;
    public String nama_kategori;
    public String deskripsi;
    public boolean active;
    public int level_kategori;
    @Transient
    public Long total_kategori_atribut = 0L;
    @Transient
    public Long total_produk_kategori = 0L;

    @Transient
    public List<ProdukKategori> children = new ArrayList<ProdukKategori>();

    @Transient
    public static final String CACHE_KEY = "cache_category_key_";

    public ProdukKategori() {}

    public ProdukKategori(long id, String name) {
        this.id = id;
        this.nama_kategori = name;
    }

    public ProdukKategori(ProdukPosisi model) {
        this.id = model.id;
        this.level_kategori = model.depth;
        this.parent_id = model.parentId;
        this.nama_kategori = model.name;
        this.posisi_item = 0L;
        this.active = true;

    }

    public ProdukKategori(ProdukKategoriPayload model) {
        setFromPayload(model);
        this.komoditas_id = model.kid;
        this.posisi_item = 0L;
        this.active = true;
        if (model.id > 0) {
            this.id = model.id;
        }
    }

    public void setFromPosisi(ProdukPosisi model){
        this.level_kategori = model.depth;
        this.parent_id = model.parentId;
        this.nama_kategori = model.name;
        this.posisi_item = 0L;
        this.active = true;
    }

    public void setFromPayload(ProdukKategoriPayload model) {
        this.parent_id = model.parent;
        this.nama_kategori = model.name;
        this.deskripsi = model.description;
        this.active = true;
    }

    public Map<String, Object> getParams() {
        Map<String, Object> params = new HashMap<>();
        params.put("kid", komoditas_id);
        params.put("pkid", id);
        return params;
    }

    public String getCreateSpecificationUrl() {
        return Router.reverse("admin.ProdukKategoriAtributCtr.create", getParams()).url;
    }

    @Transient
    private static String key1 = "ProdukKategoriByKomoditas";
    @Transient
    private static String key2 = "ProdukKategoriByKomoditasHashmap";
    public static Map<String, ProdukKategori> getKategoriMapByKomId(Long komoditas_id){
        List<ProdukKategori> produkKategoriList = Cache.get(key1+komoditas_id,List.class);
        if(null == produkKategoriList){
            produkKategoriList = ProdukKategori.find("komoditas_id = ? and active = 1", komoditas_id).fetch();
            if (produkKategoriList != null && !produkKategoriList.isEmpty()) {
                Cache.set(key1 + komoditas_id, produkKategoriList);
            }
        }

        Map<String, ProdukKategori> results = Cache.get(key2+komoditas_id,Map.class);
        if(null == results && null != produkKategoriList) {
            results = new HashMap<>();
            for (ProdukKategori pk : produkKategoriList) {
                if (pk.parent_id == null || pk.parent_id == 0) {
                    results.put(pk.id + "-0", pk);
                } else {
                    results.put(pk.id + "-" + pk.parent_id, pk);
                }
            }
            Cache.set(key2+komoditas_id,results,"30mn");
        }
        return results;
    }
    public static List<ProdukKategori> getByKomoditasId(Long komoditas_id){
        return ProdukKategori.find("komoditas_id = ? and active = 1", komoditas_id).fetch();
    }
    public String getDeleteUrl() {
        return Router.reverse("admin.ProdukKategoriCtr.delete", getParams()).url;
    }

    public String getEditUrl() {
        return Router.reverse("admin.ProdukKategoriCtr.edit", getParams()).url;
    }

    public String getHiarchy() {
        return Router.reverse("admin.ProdukKategoriAtributCtr.createPosisi", getParams()).url;
    }

    public void softDelete() {
        this.active = false;
        setDeleted();
        save();
    }

}
