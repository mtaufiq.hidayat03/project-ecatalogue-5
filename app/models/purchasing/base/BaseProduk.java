package models.purchasing.base;

import models.common.AktifUser;
import models.katalog.Produk;
import models.katalog.ProdukGambar;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import utils.UrlUtil;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author HanusaCloud on 10/24/2017
 */
@MappedSuperclass
public class BaseProduk extends BaseTable {

    @Id
    public Long id;
    public Long kuantitas;
    public String deskripsi;
    public Long produk_id;
    public Long paket_produk_id;
    public Long paket_id;
    public Integer active = 1;

    @Transient
    public String nama_produk;
    @Transient
    public Long leftovers;
    @Transient
    public Long kuantitas_awal;
    @Transient
    public String nama_unit_pengukuran;
    @Transient
    public String produk_kategori_id;
    @Transient
    public String catatan;
    @Transient
    public String no_produk;
    @Transient
    public String imageUrl;

    public Timestamp created_date = new Timestamp(new Date().getTime());
    public Integer created_by;
    public Timestamp modified_date;
    public Integer modified_by;
    public Timestamp deleted_date;
    public Integer deleted_by;

    public static final int ACTIVE = 1;
    public static final int NOT_ACTIVE = 0;

    protected void prePersist() {
        if (id == null) {
            created_by = AktifUser.getActiveUserId();
        } else {
            modified_date = new Timestamp(System.currentTimeMillis());
            modified_by = AktifUser.getActiveUserId();
        }
    }

    public String getProductDetailUrl() {
        return UrlUtil.builder().setUrl("katalog.ProdukCtr.allDetail").setParam("id", produk_id).build();
    }

    public Long getId() {
        return id;
    }

    public Long getProdukId() {
        return produk_id;
    }

    public String setImage() {
        if (this.produk_id != null) {
            ProdukGambar model = ProdukGambar.findFirstByProduk(this.produk_id);
            if (model != null) {
                this.imageUrl = model.getActiveUrl();
            }
        }
        return this.imageUrl;
    }

}
