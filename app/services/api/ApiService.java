package services.api;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import models.api.Sirup;
import models.jcommon.http.SimpleHttpRequest;
import models.util.Config;
import play.libs.URLs;
import utils.KatalogUtils;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by dadang on 12/12/17.
 */
public class ApiService {

	public static long tarikRup(Date tanggalUpdate) throws Exception {
		Config conf = Config.getInstance();
		String date = KatalogUtils.toDateyyyyMMdd(tanggalUpdate);
		String url = conf.sirup_api + "?auditupdate=" + URLs.encodePart(date);
		try (SimpleHttpRequest connect = new SimpleHttpRequest()) {
			connect.get(url);
			String content = connect.getString();
			List<Sirup> sirupList = new Gson().fromJson(content, new TypeToken<List<Sirup>>(){}.getType());

			return sirupList.size();
		}
	}
}
