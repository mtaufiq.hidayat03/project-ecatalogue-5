ALTER TABLE blob_table add COLUMN id INT not null primary key auto_increment;
alter table blob_table drop index blob_table_blb_id_content_uindex;
ALTER TABLE blob_table DROP INDEX `blob_table_blb_id_content_uindex`;
ALTER TABLE blob_table MODIFY COLUMN blb_id_content INT not null ;
alter table blob_table add column classUsed varchar(255) not null;
alter table blob_table MODIFY column classUsed varchar(255) null;