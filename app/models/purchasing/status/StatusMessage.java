package models.purchasing.status;

import constants.paket.PaketStatusConstant;
import models.purchasing.PaketRating;
import play.Logger;
import play.i18n.Messages;
import utils.UrlUtil;

import static constants.paket.PaketStatusConstant.*;

/**
 * @author HanusaCloud on 11/8/2017
 */
public class StatusMessage {

    public static final int SHOW_APPROVAL = 1;
    public static final int SHOW_MESSAGE = 2;
    public static final int SHOW_DISTRIBUTOR_URL = 3;
    public static final int SEND_TO_PPK = 4;
    public static final int SEND_TO_DISTRIBUTOR = 5;
    public static final int SHOW_FINISHED_BUTTON = 6;

    public String message = "";
    public String actionCode = "";
    public String position = "";
    public String userAction;
    public Long userId;
    public String to = "";
    public Long packageId;

    public String approveUrl;
    public String rejectUrl;

    public String sendToPpkUrl;

    public PaketRating rating;

    public int allowed = 0;

    public boolean allowAddDelivery = false;

    public boolean allowAddAcceptance = false;

    public boolean allowAddVerification = false;

    public boolean allowAddPayment = false;

    public boolean allowAddContract = false;

    public boolean allowNegotiation = false;

    public boolean showButtonsAfterNegotiation = false;

    public boolean showButtonPilihDistributor = false;

    public boolean showButtonBatalPaket = false;

    public boolean showContractMessage = false;

    public boolean showMoreOptionItem = false;

    public boolean showUlpUrl = false;

    public boolean isSetDistributor() {
        return actionCode.equalsIgnoreCase("PROVIDER_SET_DISTRIBUTOR");
    }

    public void setToProviderApproval() {
        this.actionCode = "PROVIDER_ACTION";
        setProviderPosition();
        this.to = USER_PANITIA;
        this.message = Messages.get("paket.waiting.for.provider");
    }

    private String generateUrl(String path, boolean isApprove) {
        return UrlUtil.builder()
                .setUrl(path)
                .setParam("paket_id", this.packageId)
                .setParam("from", this.position)
                .setParam("to", isApprove ? this.to : USER_PANITIA)
                .build();
    }

    public void setPreparation() {
        this.actionCode = "PP_ACTION_START";
        setBuyerPosition();
        this.to = USER_PROVIDER;
        this.allowed = 0;
        this.message = "Paket dalam status draft";
    }

    public void setPreparationNonNegotiation() {
        this.actionCode = "PP_ACTION_START_NON_NEGOTIATION";
        setBuyerPosition();
        this.to = USER_PROVIDER;
        this.allowed = 1;
        this.message = Messages.get("paket.waiting.for.committee");
    }

    public void setActionToPpkNonNegotiation() {
        this.actionCode = "PP_ACTION_TO_PPK";
        setBuyerPosition();
        this.message = Messages.get("paket.committee.send.to.ppk");
        this.to = USER_PPK;
    }

    public void setDistributorAction() {
        this.actionCode = "PROVIDER_SET_DISTRIBUTOR";
        setProviderPosition();
        this.message = Messages.get("paket.provider.set.distributor");
        this.to = USER_PANITIA;
    }

    public void setPpkActionApproval() {
        this.actionCode = "PPK_ACTION_APPROVAL";
        this.position = USER_PPK;
        this.message = Messages.get("paket.waiting.for.ppk");
        this.to = USER_DISTRIBUTOR;
    }

    public void setBuyerActionNonNegotiation() {
        this.actionCode = "PP_ACTION_NO_NEGOTIATION";
        setBuyerPosition();
        this.to = USER_PROVIDER;
    }

    public void setPpkSendDistributor() {
        this.actionCode = "PPK_SEND_TO_DISTRIBUTOR";
        this.position = USER_PPK;
        this.to = USER_DISTRIBUTOR;
        this.message = "PPK belum mengirimkan paket ke distributor";
    }

    public void setRejected(String name, String username, String date, String reason) {
        this.actionCode = "REJECTED";
        this.position = USER_PANITIA;
        this.message = Messages.get("paket.rejected.message", name, username, date, reason);
        this.allowed = StatusMessage.SHOW_MESSAGE;
    }

    public boolean isAllowedToShow() {
        return allowed == 1;
    }

    public void build() {
        this.approveUrl = generateUrl("purchasing.PaketCtr.approve", true);
        this.rejectUrl = generateUrl("purchasing.PaketCtr.reject", false);
    }

    public boolean showApproval() {
        return allowed == SHOW_APPROVAL;
    }

    public boolean isFinishingPackage() {
        return allowed == SHOW_FINISHED_BUTTON;
    }

    public boolean showMessage() {
        return allowed == SHOW_MESSAGE;
    }

    public boolean showDistributorFormUrl() {
        return allowed == SHOW_DISTRIBUTOR_URL;
    }

    public boolean sendToPpk() {
        return allowed == SEND_TO_PPK;
    }

    public boolean showFinishButton() {
        return allowed == SHOW_FINISHED_BUTTON;
    }

    public String getPositionCapitalized() {
        return PaketStatusConstant.getLocalizedUser(this.position);
    }

    public String getSendToPpkUrl() {
        return UrlUtil.builder()
                .setUrl("purchasing.PaketCtr.sendToPpk")
                .setParam("paket_id", packageId)
                .build();
    }

    public void setApproval(boolean value) {
        this.allowed = value ? SHOW_APPROVAL : SHOW_MESSAGE;
    }

    public void setSendToPpk (boolean value) {
        this.allowed = value ? SEND_TO_PPK : SHOW_MESSAGE;
    }

    public void setDistributorUrl(boolean value) {
        this.allowed = value ? SHOW_DISTRIBUTOR_URL : SHOW_MESSAGE;
    }

    public void setFinishedButton(boolean value) {
        Logger.debug("set finished button: " + value);
        this.allowed = value ? SHOW_FINISHED_BUTTON : 0;
    }

    public void setBuyerPosition() {
        this.position = USER_PANITIA;
    }

    public void setProviderPosition() {
        this.position = USER_PROVIDER;
    }

    public boolean isAllowNegotiation() {
        return allowNegotiation;
    }
}
