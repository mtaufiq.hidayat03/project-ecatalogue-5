$('#dokLabel').keyup(function () {
    var label = $(this).val();
    if(label != "" && label != null){
        $('#fileupload').attr("disabled", false);
    } else {
        $('#fileupload').attr("disabled", true);
    }
});

$(document).on('click', '#fileupload', function(){
    console.log("upload..");
    $('#fileupload').fileupload({
        url: params.uploadDocUrl,
        formData: {
            'authenticityToken': $('input[name=authenticityToken]').val(),
            dok_label: $('#dokLabel').val(),
            'usulan_id': $('#usulan-id').val()
        },
        dataType: 'json',
        acceptFileTypes: /(\.|\/)(txt|doc|docx|xls|xlsx|pdf|gif|jpe?g|png|zip|rar|rtf)$/i,
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                //file.id adalah doc.id
                var content = file.file_label + ' - ' + file.name
                    + ' <a href="javascript:void(0)" class="removeDok" versi="'+file.versi+'" fileId="'+file.id+'">' +
                    '<i class="fa fa-trash-o" ></i></a><input type="hidden" name="file_id" value="' + file.id + '">';
                $('<li/>').html(content).appendTo('#files');
                totalUploaded++;
            });
            //$('#dokLabel').val('');
            $('#dokLabel').change();
            $('#progress').hide();
            $('#alert-upload').hide();
            $('#file-counter').val(totalUploaded);
            $('#fileupload').attr('disabled', true).val('');
            validate();

            if (data.result.errorx != null){
                //$('#alert-upload').attr('class', 'alert alert-danger').html("&{'paket.upload_failed'}");
                //alert("&{'paket.upload_failed'}");
                showToast("failed upload dok");
            }
        },
        error: function (jqXHR, exception) {
            //alert(jqXHR.status);
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            alert(msg);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress').show();
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).on('fileuploadprocessalways', function (e, data) {
        var file = data.files[data.index];
        if (data.files.error && file.error) {
            var msg = file.name+' - '+file.error
            $('#alert-upload').attr('class', 'alert alert-danger').html(msg)
        }
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
});

$(document).on('click', '.removeDok', function(){
    var versi = $(this).attr('versi');
    var parent = $(this).parent();
    var fileId = $(this).attr('fileId');
    $.ajax({
        type: 'POST',
        url: params.deleteDocUrl,
        //data : {'fileId': fileId, 'versi': versi, 'authenticityToken': ('input[name=authenticityToken]').val()},
        data : {'docId': fileId, 'authenticityToken': $('input[name=authenticityToken]').val()},
        success : function(){
            $('#alert-upload').html('<div class="alert alert-success" role="alert">File telah terhapus</div>');
            parent.remove();
            totalUploaded--;
            $('#file-counter').val(totalUploaded);
            validate();
        },
        error : function(){
            $('#alert-upload').html('<div class="alert alert-danger" role="alert">File tidak bisa dihapus</div>');
            validate();
        }
    })
});




// BEGIN POKJA

$('#dokPokjaLabel').keyup(function () {
    var label = $(this).val();
    if(label != "" && label != null){
        $('#filePokjaupload').attr("disabled", false);
    } else {
        $('#filePokjaupload').attr("disabled", true);
    }
});

$(document).on('click', '#filePokjaupload', function(){
    console.log("upload..");
    $('#filePokjaupload').fileupload({
        url: params.uploadDocPokjaUrl,
        formData: {
            'authenticityToken': $('input[name=authenticityToken]').val(),
            dok_label: $('#dokPokjaLabel').val(),
            'usulan_id': $('#usulan-id').val()
        },
        dataType: 'json',
        acceptFileTypes: /(\.|\/)(txt|doc|docx|xls|xlsx|pdf|gif|jpe?g|png|zip|rar|rtf)$/i,
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                //file.id adalah doc.id
                var content = file.file_label + ' - ' + file.name
                    + ' <a href="javascript:void(0)" class="removePokjaDok" versi="'+file.versi+'" fileId="'+file.id+'">' +
                    '<i class="fa fa-trash-o" ></i></a><input type="hidden" name="file_pokja_id" value="' + file.id + '">';
                $('<li/>').html(content).appendTo('#pokja-files');
                totalPokjaUploaded++;

            });
            //$('#dokPokjaLabel').val('');
            $('#dokPokjaLabel').change();
            $('#progressPokja').hide();
            $('#alert-uploadPokja').hide();
            $('#file-counterPokja').val(totalPokjaUploaded);
            $('#filePokjaupload').attr('disabled', true).val('');
            validate();

            if (data.result.errorx != null){
                //$('#alert-upload').attr('class', 'alert alert-danger').html("&{'paket.upload_failed'}");
                //alert("&{'paket.upload_failed'}");
                showToast("upload failed");
            }
        },
        error: function (jqXHR, exception) {
            //alert(jqXHR.status);
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            alert(msg);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progressPokja').show();
            $('#progressPokja .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).on('fileuploadprocessalways', function (e, data) {
        var file = data.files[data.index];
        if (data.files.error && file.error) {
            var msg = file.name+' - '+file.error
            $('#alert-uploadPokja').attr('class', 'alert alert-danger').html(msg)
        }
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
});

$(document).on('click', '.removePokjaDok', function(){
    var versi = $(this).attr('versi');
    var parent = $(this).parent();
    var fileId = $(this).attr('fileId');
    $.ajax({
        type: 'POST',
        url: params.deleteDocUrl,
        //data : {'fileId': fileId, 'versi': versi, 'authenticityToken': ('input[name=authenticityToken]').val()},
        data : {'docId': fileId, 'authenticityToken': $('input[name=authenticityToken]').val()},
        success : function(){
            $('#alert-uploadPokja').html('<div class="alert alert-success" role="alert">File telah terhapus</div>');
            parent.remove();
            totalPokjaUploaded--;
            $('#file-counterPokja').val(totalPokjaUploaded);
            validate();
        },
        error : function(){
            $('#alert-uploadPokja').html('<div class="alert alert-danger" role="alert">File tidak bisa dihapus</div>');
            validate();
        }
    })
});




// END POKJA

function validate() {
    $('.form_create').valid();
}

$('.form_create').validate({
    rules: {
        'pokja[]': {
            required: true
        },
        'usulan.nama_usulan': {
            required: true,
            alphanumeric: true
        },
        'usulan.pengusul_nama': {
            required: true,
            alphanumeric: true
        },
        'usulan.pengusul_nik': {
            required: true,
            number: true
        },
        'usulan.kldi_id': {required: true},
        'usulan.template_jadwal_id': {required: true},
        'usulan.pengusul_email': {
            required: true,
            emailAddress: true
        },
        'file-pokja-counter': {required: true, min: 1},
        'usulan.pokja_id': {required: true},
        'kategori[]': {required: true},
        'file-counter': {required: true, min: 1},
        'usulan.pengusul_no_telp': {number: true}
    }, messages: {
        'pokja[]':{
            required: "Belum memilih Pokja"
        },
        'usulan.nama_usulan': {
            required: "Judul usulan belum diisi!",
            alphanumeric: "Judul usalan hanya berupa alphanumeric!"
        },
        'usulan.pengusul_nama': {
            required: "Nama pengusul belum diisi!",
            alphanumeric: "Nama pengusul hanya berupa alphanumeric!"
        },
        'usulan.pengusul_nik': {
            required: "NIK/NIP pengusul belum diisi!",
            number: "NIK/NIP hanya bisa berupa angka!"
        },
        'usulan.kldi_id': 'Belum memilih K/L/PD !',
        'usulan.pengusul_email': {
            required: "Belum mengisi alamat email!",
            emailAddress: "Format email tidak tepat"
        },
        'usulan.pengusul_no_telp': {
            number: "No. telp hanya bisa berupa angka!"
        },
        'usulan.pokja_id': "Belum memilih Pokja",
        'kategori[]': "Belum memilih kategori produk!",
        'usulan.template_jadwal_id': 'Belum memilih template jadwal!',
        'file-counter': "Belum mengupload file Dokumen",
        'file-pokja-counter': "Belum mengupload file pokja"
    },
    highlight: function(element) {
        $(element).parents('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).parents('.form-group').removeClass('has-error');
    }
});

function showToast(message) {
    jQuery.toast({
        heading: 'Oops',
        text: message,
        position: 'bottom-center',
        stack: false,
        icon: 'error',
        hideAfter: 6000
    })
}