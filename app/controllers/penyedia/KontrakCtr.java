package controllers.penyedia;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import controllers.BaseController;
import controllers.prakatalog.PenawaranCtr;
import controllers.security.AllowAccess;
import ext.FormatUtils;
import jobs.JobTrail.JobTurnTayang;
import jobs.elasticsearch.ElasticsearchProdukAdd;
import models.api.produk.detail.Lampiran;
import models.common.AktifUser;
import models.common.DokumenInfo;
import models.common.DokumenKontrak;
import models.common.DokumenPenetapanProduk;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.util.CommonUtil;
import models.katalog.*;
import models.katalog.komoditasmodel.KomoditasTemplateKontrak;
import models.masterdata.*;
import models.penyedia.Penyedia;
import models.penyedia.PenyediaKomoditas;
import models.penyedia.PenyediaKontrak;
import models.prakatalog.*;
import models.secman.Acl;
import models.user.User;
import models.util.RekananDetail;
import models.util.produk.HargaProdukJson;
import play.Logger;
import play.Play;
import play.i18n.Messages;
import play.jobs.Job;
import repositories.penyedia.ProviderRepository;

import services.dokumen.DokumenService;
import services.prakatalog.SuratKeputusanService;
import utils.DateTimeUtils;
import utils.HtmlUtil;
import utils.LogUtil;

import java.io.*;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class KontrakCtr extends BaseController {

	@AllowAccess({Acl.COM_PENYEDIA_FILE_KONTRAK_KELOLA, Acl.COM_PENYEDIA_FILE_KONTRAK_DOWNLOAD})
	public static void index(long pid){
		render(pid);
	}

	@AllowAccess({Acl.COM_PENYEDIA_FILE_KONTRAK_KELOLA, Acl.COM_PENYEDIA_FILE_KONTRAK_DOWNLOAD})
	public static void listKontrakPenawaran(long id, long pid){
		render(pid, id);
	}

	@AllowAccess({Acl.COM_PENYEDIA_FILE_KONTRAK_KELOLA})
	public static void create(long pid, String type){
		Komoditas komoditas = null;
		List<Komoditas> komoditasList = new ArrayList<>();
		if(null == type){
			type= "";
		}
		Penyedia penyedia = null;
		if(type.equals("penawaran")){
			Penawaran penawaran = Penawaran.findById(pid);
			komoditas = Komoditas.findById(penawaran.komoditas_id);
			penyedia = Penyedia.findById(penawaran.penyedia_id);

		}else if(type.equals("penyedia")){
			komoditasList = Komoditas.findByPenyedia(pid);
		}else{
			//dari menu user manjemen create
			penyedia = Penyedia.findById(pid);
		}
		renderArgs.put("objpenyedia", penyedia);
		renderArgs.put("action", "insert");

		render(pid, komoditasList, komoditas, type);
	}

	@AllowAccess({Acl.COM_PENYEDIA_FILE_KONTRAK_KELOLA})
	public static void edit(long id){
		PenyediaKontrak model = PenyediaKontrak.findById(id);
		notFoundIfNull(model);
		model.withAttachment();
		List<Komoditas> komoditasList = Komoditas.findByPenyedia(model.penyedia_id);
		try {
			Penawaran penawaran = Penawaran.findById(model.penawaran_id);
			Komoditas komoditas = Komoditas.findById(penawaran.komoditas_id);
			Penyedia penyedia = Penyedia.getProviderByUserId(penawaran.user_id);
			renderArgs.put("komoditas", komoditas);
			renderArgs.put("penyedia", penyedia);
			renderArgs.put("penawaran", penawaran);
			if( null == komoditas || null == penyedia || null == penawaran) {
				flash.error(Messages.get("edit_kontrak_penyedia_3.errorvalidasi"));
				params.flash();
			}
		}catch (Exception e){
			flash.error(Messages.get("edit_kontrak_penyedia_3.errorvalidasi"));
			params.flash();
		}

		renderArgs.put("action", "update");
		renderArgs.put("kontrak", model);
		renderArgs.put("commodities", komoditasList);
		renderArgs.put("type", "penyedia");
		renderArgs.put("penyedia_id", model.penyedia_id);

		render();
	}

	@AllowAccess({Acl.COM_CETAK_DOKUMEN_SK_PENETAPAN_PRODUK})
	public static void listLampiranKontrak(long pid){
		Penawaran penawaran = Penawaran.findById(pid);
		notFoundIfNull(penawaran);
		if(penawaran != null) {
			List<LampiranKontrak> lampiranKontrak = LampiranKontrak.getByPenawaranId(pid);
			renderArgs.put("kontrak", lampiranKontrak);
		}
		render(pid);
	}

	@AllowAccess({Acl.COM_CETAK_DOKUMEN_SK_PENETAPAN_PRODUK})
	public static void cetakPenetapanProduk(long pid){
		Penawaran penawaran = Penawaran.findById(pid);
		Komoditas komoditas = Komoditas.findById(penawaran.komoditas_id);
		Penyedia penyedia = Penyedia.findById(penawaran.penyedia_id);
		LampiranKontrak lampiranKontrak = new LampiranKontrak();

		DokumenPenetapanProduk dokumenPenetapanProduk = new DokumenPenetapanProduk();
		dokumenPenetapanProduk.namaPihakPertama = ParameterAplikasi.findByNamaParameter(ParameterAplikasi.PARAMETER_KEPALA_LKPP);
		dokumenPenetapanProduk.namaBadanUsaha = penyedia.nama_penyedia;
		dokumenPenetapanProduk.alamatBadanUsaha = penyedia.alamat.toLowerCase();
		dokumenPenetapanProduk.namaKomoditas = komoditas.nama_komoditas;

		List<ProdukHarga> listDataSK = new ArrayList<>();

		if(komoditas.kelas_harga.equals("nasional")){
			List<Produk> produkNasionalList = Produk.findProdukByPenawaran(pid);

			for(Produk produkNasional : produkNasionalList){
				ProdukHarga dataSK = new ProdukHarga();
				dataSK.nama_produk = produkNasional.nama_produk;
				dataSK.nama_manufaktur = produkNasional.nama_manufaktur;
				dataSK.nama_unit_pengukuran = produkNasional.nama_unit_pengukuran;
				dataSK.wilayah_jual = "Nasional";
				dataSK.harga = FormatUtils.formatCurrencyRupiah(produkNasional.harga_utama);

				listDataSK.add(dataSK);
			}

		}else if(komoditas.kelas_harga.equals("provinsi")){
			List<ProdukWilayahJualProvinsi> produkWilayahJualProvinsiList = ProdukWilayahJualProvinsi.findProdukByPenawaran(pid);

			for(ProdukWilayahJualProvinsi produkProvinsi : produkWilayahJualProvinsiList){
				ProdukHarga dataSK = new ProdukHarga();
				dataSK.nama_produk = produkProvinsi.nama_produk;
				dataSK.nama_manufaktur = produkProvinsi.nama_manufaktur;
				dataSK.nama_unit_pengukuran = produkProvinsi.nama_unit_pengukuran;
				dataSK.wilayah_jual = produkProvinsi.nama_provinsi;
				dataSK.harga = FormatUtils.formatCurrencyRupiah(produkProvinsi.harga_utama);

				listDataSK.add(dataSK);
			}
		}else{
			List<ProdukWilayahJualKabupaten> produkWilayahJualKabupatenList = ProdukWilayahJualKabupaten.findProdukByPenawaran(pid);

			for(ProdukWilayahJualKabupaten produkKabupaten : produkWilayahJualKabupatenList){
				ProdukHarga dataSK = new ProdukHarga();
				dataSK.nama_produk = produkKabupaten.nama_produk;
				dataSK.nama_manufaktur = produkKabupaten.nama_manufaktur;
				dataSK.nama_unit_pengukuran = produkKabupaten.nama_unit_pengukuran;
				dataSK.wilayah_jual = produkKabupaten.nama_kabupaten;
				dataSK.harga = FormatUtils.formatCurrencyRupiah(produkKabupaten.harga_utama);

				listDataSK.add(dataSK);
			}
		}

		DokumenTemplate dokumenTemplate = DokumenTemplate.findById(4); //Hardcode
		Map<String, Object> params = new HashMap<>();
		params.put("tableProduk", SuratKeputusanService.populateDataForDocument(dokumenPenetapanProduk, listDataSK));
		params.put("dokumenPenetapanProduk", dokumenPenetapanProduk);
		String templates = HtmlUtil.renderHtml(dokumenTemplate.template, params);
		dokumenTemplate.template = templates;

		render(pid, dokumenPenetapanProduk, dokumenTemplate, lampiranKontrak);
	}

	public static void downloadDoc(Long id) {
		//Logger.info(isi_text+" isinya");

		LampiranKontrak lampiranKontrak = LampiranKontrak.findById(id);
		Penawaran penawaran = Penawaran.findById(lampiranKontrak.penawaran_id);
		Usulan usulan = Usulan.findById(penawaran.usulan_id);
		if (null == lampiranKontrak.dokumen_lampiran || lampiranKontrak.dokumen_lampiran.isEmpty()) {
			renderArgs.put("isi_text", "<p>Dokumen kosong</p>");
		} else {
			renderArgs.put("isi_text", lampiranKontrak.dokumen_lampiran);
		}
		renderArgs.put("nama_file", "dokumen_lampiran_kontrak_" + usulan.nama_usulan);
		render("MediaManager/create.html");
	}

	public static void downloadLampiran(Long id) {
		LampiranKontrak model = LampiranKontrak.findById(id);
		if (model != null) {
			BlobTable blobTable = model.withBlobContent();
			if (blobTable != null && blobTable.getFile().exists()) {
				renderBinary(blobTable.getFile(), model.original_file_name);
			} else if (model.getFile().exists()) {
				renderBinary(model.getFile(), model.original_file_name);
			} else {
				error("File's not found!");
			}
		} else {
			error("File's not found!");
		}
	}

	public static void downloadUplLampiran(Long id) {
		LampiranKontrak model = LampiranKontrak.findById(id);

		if (model != null) {
			BlobTable blobTable = model.withBlobId();
			if (blobTable != null && blobTable.getFile().exists()) {
				renderBinary(blobTable.getFile(), model.file_name);
			} else if (model.getFile().exists()) {
				renderBinary(model.getFile(), model.file_name);
			} else {
				error("File's not found!");
			}
		} else {
			error("File's not found!");
		}
	}

	public static void uploadLampiran(File file, Long lampiran_id) {
		try {
			LampiranKontrak model = LampiranKontrak.findById(lampiran_id);
			if (model != null) {
				BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, model.id, LampiranKontrak.class.getName());
				blob.blb_nama_file = file.getName();
				model.file_size = blob.blb_ukuran;
				model.blb_id = blob.id;
				model.file_name = blob.blb_nama_file;
				model.save();
			}
			redirect("penyedia.KontrakCtr.listLampiranKontrak", model.penawaran_id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AllowAccess({Acl.COM_CETAK_DOKUMEN_SK_PENETAPAN_PRODUK})
	public static void cetakPenetapanProdukSubmit(DokumenPenetapanProduk dokumenPenetapanProduk, long pid, long id) {
		//decode and replace
		String decodedText = HtmlUtil.decodeFromTinymce(dokumenPenetapanProduk.dokumenLampiran);
		dokumenPenetapanProduk.dokumenLampiran = decodedText;
		//end
		if (id != new Long(0)) {
			LampiranKontrak lampiranKontrak = LampiranKontrak.findById(id);
			lampiranKontrak.penawaran_id = pid;
			lampiranKontrak.no_surat = dokumenPenetapanProduk.nomorSurat;
			lampiranKontrak.perihal_surat = dokumenPenetapanProduk.perihalSurat;
			lampiranKontrak.tanggal_surat = DateTimeUtils.parseDdMmYyyy(dokumenPenetapanProduk.tanggalSurat);
			lampiranKontrak.no_berita_acara = dokumenPenetapanProduk.nomorBeritaAcaraNego;
			lampiranKontrak.tanggal_berita_acara = DateTimeUtils.parseDdMmYyyy(dokumenPenetapanProduk.tanggalBeritaAcaraNego);
			lampiranKontrak.dokumen_lampiran = dokumenPenetapanProduk.dokumenLampiran;
			lampiranKontrak.file_size = new Long(0);
			lampiranKontrak.active = 1;
			lampiranKontrak.save();
			flash.success(Messages.get("notif.success.update"));
		} else {
			LampiranKontrak lampiranKontrak = new LampiranKontrak();
			lampiranKontrak.penawaran_id = pid;
			lampiranKontrak.no_surat = dokumenPenetapanProduk.nomorSurat;
			lampiranKontrak.perihal_surat = dokumenPenetapanProduk.perihalSurat;
			lampiranKontrak.tanggal_surat = DateTimeUtils.parseDdMmYyyy(dokumenPenetapanProduk.tanggalSurat);
			lampiranKontrak.no_berita_acara = dokumenPenetapanProduk.nomorBeritaAcaraNego;
			lampiranKontrak.tanggal_berita_acara = DateTimeUtils.parseDdMmYyyy(dokumenPenetapanProduk.tanggalBeritaAcaraNego);
			lampiranKontrak.dokumen_lampiran = dokumenPenetapanProduk.dokumenLampiran;
			lampiranKontrak.file_size = new Long(0);
			lampiranKontrak.active = 1;
			lampiranKontrak.save();
			flash.success(Messages.get("notif.success.submit"));
		}
		listLampiranKontrak(pid);
	}

	public static void editPenetapanProduk(long id){
		LampiranKontrak lampiranKontrak = LampiranKontrak.findById(id);
		Long pid = lampiranKontrak.penawaran_id;
		SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
		DokumenTemplate dokumenTemplate = new DokumenTemplate();
		dokumenTemplate.template = lampiranKontrak.dokumen_lampiran;
		String tanggal_surat = dateformat.format(lampiranKontrak.tanggal_surat);
		String tanggal_berita_acara = dateformat.format(lampiranKontrak.tanggal_berita_acara);

		render("penyedia/KontrakCtr/cetakPenetapanProduk.html", pid, lampiranKontrak, dokumenTemplate, tanggal_surat, tanggal_berita_acara);
	}

	@AllowAccess({Acl.COM_PENYEDIA_FILE_KONTRAK_DOWNLOAD})
	public static void listDokumenUpload(long pid, String type, String kategori){
		Penawaran penawaran = Penawaran.findById(pid);
		DokumenKategori dokumenKategori = DokumenKategori.findByKategori(kategori);
		long kategori_id = dokumenKategori.id;
		render(pid,kategori_id,type);
	}

	@AllowAccess({Acl.COM_PENYEDIA_FILE_KONTRAK_DOWNLOAD})
	public static void cetakKontrak(long pid, String type){
		Komoditas komoditas = null;
		List<Komoditas> komoditasList = null;

		if(type != null) {
			if (type.equals("penawaran")) {
				Penawaran penawaran = Penawaran.findById(pid);
				komoditas = Komoditas.findById(penawaran.komoditas_id);
			}

			if (type.equals("penyedia")) {
				komoditasList = Komoditas.findByPenyedia(pid);
			}
		}

		List<ParameterAplikasi> parameterList = ParameterAplikasi.findAll();
		DokumenKontrak dokumenKontrak = new DokumenKontrak();

		//pihak pertama
		dokumenKontrak.namaPihakPertama = ParameterAplikasi.findByNamaParameter(ParameterAplikasi.PARAMETER_KEPALA_LKPP);
		dokumenKontrak.jabatanPihakPertama = "Kepala LKPP";
		dokumenKontrak.alamatLKPP = ParameterAplikasi.findByNamaParameter(ParameterAplikasi.PARAMETER_LOKASI_LKPP);

		//pihak kedua
		Penawaran penawaran = Penawaran.findById(pid);
		if (penawaran.data_kontrak==null) {
			if(type != null){
			if (type.equals("penawaran")) {
				User user = User.findById(penawaran.user_id);
				RekananDetail rkn = null;
				try {
					if (user.rkn_id != null) {
						rkn = RekananDetail.getIdRekananDefault(user.rkn_id);
					} else {
						params.flash();
						flash.error(Messages.get("notif.error.null.rkn_id"));
						PenawaranCtr.index();
					}
				} catch (Exception e) {
					// Handle null of rekanan
					Logger.info("Error get rekanan detail: %s", e.getMessage());
				}
				rkn = rkn == null ? new RekananDetail() : rkn;

				dokumenKontrak.namaBadanUsaha = rkn.rkn_nama;
				dokumenKontrak.alamatBadanUsaha = rkn.rkn_alamat;

				dokumenKontrak.namaPenyedia = rkn.rkn_nama;
				dokumenKontrak.alamatPenyedia = rkn.rkn_alamat;
				dokumenKontrak.teleponPenyedia = rkn.rkn_telepon;
				dokumenKontrak.websitePenyedia = rkn.rkn_website;
				dokumenKontrak.faksPenyedia = rkn.rkn_fax;
				dokumenKontrak.emailPenyedia = rkn.rkn_email;

			}
			} else {
				Penyedia penyedia = Penyedia.findById(pid);
				dokumenKontrak.namaBadanUsaha = penyedia.nama_penyedia;
				dokumenKontrak.alamatBadanUsaha = penyedia.alamat;

				dokumenKontrak.namaPenyedia = penyedia.nama_penyedia;
				dokumenKontrak.alamatPenyedia = penyedia.alamat;
				dokumenKontrak.teleponPenyedia = penyedia.no_telp;
				dokumenKontrak.websitePenyedia = penyedia.website;
				dokumenKontrak.faksPenyedia = penyedia.no_fax;
				dokumenKontrak.emailPenyedia = penyedia.email;
			}
		}else{
			DokumenKontrak dataKontrak = new Gson().fromJson(penawaran.data_kontrak, DokumenKontrak.class);
			dokumenKontrak=dataKontrak;
		}


		//korespondensi
		dokumenKontrak.namaLKPP = "Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah";
		dokumenKontrak.teleponLKPP = ParameterAplikasi.findByNamaParameter(ParameterAplikasi.PARAMETER_TELEPON_LKPP);
		dokumenKontrak.websiteLKPP = ParameterAplikasi.findByNamaParameter(ParameterAplikasi.PARAMETER_WEBSITE_LKPP);
		dokumenKontrak.faksLKPP = ParameterAplikasi.findByNamaParameter(ParameterAplikasi.PARAMETER_FAKSIMILI_LKPP);


		render(pid, komoditasList, dokumenKontrak, komoditas, type);
	}

	@AllowAccess({Acl.COM_PENYEDIA_FILE_KONTRAK_DOWNLOAD})
	public static void cetakKontrakSubmit(DokumenKontrak dokumenKontrak, String type, long pid){
		Komoditas komoditas = Komoditas.findById(dokumenKontrak.komoditas);
		dokumenKontrak.namaKomoditas = komoditas.nama_komoditas;
		dokumenKontrak.alamatLKPP = ParameterAplikasi.findByNamaParameter(ParameterAplikasi.PARAMETER_LOKASI_LKPP);

		Date today = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(today);

		dokumenKontrak.tanggalTerbilang = DateTimeUtils.formatDateToString(today,"dd/MM/YYYY");
		dokumenKontrak.bulan = DateTimeUtils.getReadableMonth(cal.get(Calendar.MONTH));
		dokumenKontrak.tanggal = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
		dokumenKontrak.tahun = String.valueOf(cal.get(Calendar.YEAR));
		dokumenKontrak.hari = DateTimeUtils.getReadableDay(cal.get(Calendar.DAY_OF_WEEK) - 1); //jika menggunakan day_of_week maka harus dikurangi 1

		Map<String, Object> params = new HashMap<>();
		params.put("dokumenKontrak", dokumenKontrak);

		List<KomoditasTemplateKontrak> templateKontrak = KomoditasTemplateKontrak.findByKomoditas(komoditas.id);

		Map<String,InputStream> map = new HashMap<>();

		for (KomoditasTemplateKontrak template : templateKontrak){
			InputStream is = HtmlUtil.generatePDF(template.template, params);
			map.put(template.jenis_dokumen+".pdf",is);
		}

		if(type.equals("penawaran")){
			Penawaran penawaran = Penawaran.findById(pid);
			penawaran.status = Penawaran.STATUS_KONTRAK_DIPROSES;
			penawaran.active = true;
			penawaran.save();
		}

		OutputStream outputStream = null;
		LocalDate localDate = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddLLyyyy");
		String formattedString = localDate.format(formatter);

		try{
			byte[] zip = DokumenService.zipFiles(map);
			InputStream result = new ByteArrayInputStream(zip);

			Penawaran penawaran = Penawaran.findById(pid);
			String filePath = "/public/files/dokumen/";
			String fileName = penawaran.user_id.toString()+pid+formattedString+"_"+ PenawaranCtr.getRandom()+".zip";

			outputStream = new FileOutputStream(new File(Play.applicationPath+filePath+fileName));
			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = result.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}

			DokumenKategori dokumenKategori = DokumenKategori.findByKategori("Kontrak Katalog");
			DokumenUpload previousData = DokumenUpload.getPreviousData(pid,penawaran.user_id,dokumenKategori.id);
			if(previousData != null){
				DokumenUpload.updateToNonActive(previousData.id);
			}
			DokumenUpload dokumenUpload = new DokumenUpload();
			dokumenUpload.dokumen_kategori_id = dokumenKategori.id;
			dokumenUpload.penyedia_id = penawaran.penyedia_id;
			dokumenUpload.penawaran_id = pid;
			dokumenUpload.nama_dokumen = fileName;
			dokumenUpload.file_path = filePath;
			dokumenUpload.active = true;
			dokumenUpload.save();

			//renderBinary(result,fileName);
			listDokumenUpload(pid,type,"kontrak katalog");
		}catch (Exception e){
			e.printStackTrace();
		} finally {

			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}
	}

	@AllowAccess({Acl.COM_PENYEDIA_FILE_KONTRAK_KELOLA})
	public static void editSubmit(PenyediaKontrak kontrak, File file, int file_id, String type, long pid){
		validation.valid(kontrak);
		if(null ==  kontrak.penyedia_id || null == kontrak.komoditas_id || null == kontrak.penawaran_id) {
			flash.error(Messages.get("edit_kontrak_penyedia_3.errorvalidasi"));
			params.flash();
			edit(kontrak.id);
		}else if (validation.hasErrors()){
			params.flash();
			edit(kontrak.id);
		} else {
			try{
				PenyediaKontrak kontrakEksisting = PenyediaKontrak.findById(kontrak.id);

				// file kontrak dinonaktifkan semua
				PenyediaKontrak.updateApakahBerlaku(kontrakEksisting.penyedia_id, kontrakEksisting.penawaran_id);

				PenyediaKontrak pknewupload = PenyediaKontrak.findById(file_id);
				//tidak ada file yang diganti, maka pakai yang lama
				if(pknewupload == null){
					pknewupload = new PenyediaKontrak();
					pknewupload.posisi_file = kontrakEksisting.posisi_file;
					pknewupload.file_name = kontrakEksisting.file_name;
					pknewupload.original_file_name = kontrakEksisting.original_file_name;
					pknewupload.file_size= kontrakEksisting.file_size;
					pknewupload.file_sub_location = kontrakEksisting.file_sub_location;
					pknewupload.file_hash = kontrakEksisting.file_hash;
					pknewupload.dok_id_attachment = kontrakEksisting.dok_id_attachment;
					pknewupload.active = true;
//					pknewupload.created_by = AktifUser.getActiveUserId();
				}

				//jika file_id adalah upload_membalikan blob table id
//				PenyediaKontrak pk = PenyediaKontrak.getByDokIdAndKomoditas(file_id);
//				Penawaran penawaran = Penawaran.findById(kontrakEksisting.penawaran_id);

				//update penyedia_kontrak
				pknewupload.penawaran_id = kontrakEksisting.penawaran_id;
				pknewupload.penyedia_id = kontrak.penyedia_id;
				pknewupload.tgl_masa_berlaku_mulai = kontrak.tgl_masa_berlaku_mulai;
				pknewupload.tgl_masa_berlaku_selesai = kontrak.tgl_masa_berlaku_selesai;
				pknewupload.deskripsi = kontrak.deskripsi;
				//penyesuaian dengan kontrak status yang aktif nonaktifkan dari depan
				pknewupload.apakah_berlaku = true;
				pknewupload.komoditas_id = kontrak.komoditas_id;
				pknewupload.no_kontrak = kontrak.no_kontrak;

				long penyediaKontrakId = 0;
				if(pknewupload.id == null) {
					penyediaKontrakId = pknewupload.save();
				}else{
					pknewupload.save();
					penyediaKontrakId = pknewupload.id;
				}

				Penawaran.updateKontrak(penyediaKontrakId, kontrakEksisting.penawaran_id);

				// tidak di apply, karena produk akan tayang sesuai dengan penawaran dan kontrak yang berlaku, fahri : 08 okt 2019
//				Produk.updatePenyediaProdukByPenawaran(penawaran,penyediaId,pknewupload.id,kontrak.apakah_berlaku);
//				Produk.updateTayangByKontrak(pknewupload);

				kontrak = pknewupload;
				Logger.info("sukses edit kontrak");
			}catch (Exception e){
				Logger.info("gagal edit kontrak");
			}
		}

		if(type.equals("penawaran")){
			PenawaranCtr.index();
		}else{
//			index(kontrak.penyedia_id);
			listKontrakPenawaran(kontrak.penawaran_id, kontrak.penyedia_id);
		}
	}

	@AllowAccess({Acl.COM_PENYEDIA_FILE_KONTRAK_KELOLA})
	public static void createSubmit(PenyediaKontrak kontrak, File file, int file_id, String type, long pid){
		validation.valid(kontrak);
		if (validation.hasErrors()){
			params.flash();
			create(pid, type);
		} else {

			try{
				Long penyediaId = kontrak.penyedia_id;
				//jika file_id adalah upload_membalikan blob table id
				//PenyediaKontrak pk = PenyediaKontrak.getByDokIdAndKomoditas(file_id);
				//
				PenyediaKontrak pk = PenyediaKontrak.findById(file_id);
				if(type.equals("penawaran")){
					Penawaran penawaran = Penawaran.findById(pid);

					//Buat Penyedia Baru atau Mengambil Data Penyedia
					User user = User.findById(penawaran.user_id);
					Penyedia penyedia = ProviderRepository.findByUserId(user.id);

					if(penyedia == null){
						penyediaId = ProviderRepository.saveNewByRknId(user.rkn_id, user.id);
					}else{
						penyediaId = penyedia.id;
					}

					PenyediaKomoditas.addNew(penyediaId,penawaran.komoditas_id);
					Produk.updatePenyediaProdukByPenawaran(penawaran,penyediaId,pk.id,kontrak.apakah_berlaku);
					penawaran.changeStatus(Penawaran.STATUS_SELESAI);
//					kontrak ngiket ke penawaran
//					kontrak id di produk dibiarkan ada untuk eksistensi saja
					penawaran.kontrak_id = pk.id;
					int idp = penawaran.save();
					pk.penawaran_id = pid;//penawaran id

					List<Produk> produk = Produk.findAllProdukByPenawaran(pid);
					for (Produk produkExecute : produk){
                        long idProduk = produkExecute.id;
						new JobTurnTayang(idProduk).now();
					}

				}else{
					Produk.updateTayangByKontrak(pk);
				}

				pk.updateKontrakPenyedia(penyediaId,kontrak,pid);

			}catch (Exception e){
				Logger.info("gagal save kontrak");
			}
		}

		if(type.equals("penawaran")){
			PenawaranCtr.index();
		}else{
			index(kontrak.penyedia_id);
		}
	}

	@AllowAccess({Acl.COM_PENYEDIA_FILE_KONTRAK_KELOLA, Acl.COM_PENYEDIA_FILE_KONTRAK_DOWNLOAD})
	public static void detail() {
		render();
	}

	@AllowAccess({Acl.COM_PENYEDIA_FILE_KONTRAK_KELOLA})
	public static void upload(File file, String action, Long id, Long idp){
		Map<String, Object> result = new HashMap<>(1);
		try {
			List<DokumenInfo> files = new ArrayList<>();
			if (action.equalsIgnoreCase("update")) {
				PenyediaKontrak model = PenyediaKontrak.findById(id);
				if (model != null) {
					BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, id, DokumenKontrak.class.getSimpleName());
					files.add(new DokumenInfo(blob));
				}
			} else {
				DokumenInfo dokumenInfo = PenyediaKontrak.simpanKontrakPenyedia(file, idp);
				files.add(dokumenInfo);
			}
			result.put("files", files);
		} catch (Exception e) {
			result.put("errorx", "upload failed");
			e.printStackTrace();

		}
		if (result.get("files")==null){
			result.clear();
			result.put("errorx", "upload failed");
		}

		renderJSON(result);
	}

	//@AllowAccess({Acl.COM_PENYEDIA_FILE_KONTRAK_KELOLA, Acl.COM_PENYEDIA_FILE_KONTRAK_DOWNLOAD})
	//disable dulu sampe ada kelanjutannya
//	public static void listKontrakAll(){
//
//		Long penyedida_id = new Long(0);
//		//kontrak kategori, id dokumen kategorinya idnya 3
//		Long kategori_id = new Long(3);
//		try{
//			penyedida_id = Penyedia.getProviderByUserId(AktifUser.getAktifUser().user_id).id;
//		}catch (Exception e){}
//
//		render(penyedida_id, kategori_id);
//	}

	@AllowAccess({Acl.COM_INPUT_DATA_KONTRAK})
	public static void renderInputDataKontrak(Long pid, String type){
        Komoditas komoditas = null;
        List<Komoditas> komoditasList = null;

        if(type.equals("penawaran")){
            Penawaran penawaran = Penawaran.findById(pid);
            komoditas = Komoditas.findById(penawaran.komoditas_id);
        }

        if(type.equals("penyedia")){
            komoditasList = Komoditas.findByPenyedia(pid);
        }

        List<ParameterAplikasi> parameterList = ParameterAplikasi.findAll();
        DokumenKontrak dokumenKontrak = new DokumenKontrak();
        Penawaran penawaran = Penawaran.findById(pid);
		if (penawaran.data_kontrak==null){
			if(type.equals("penawaran")){
				User user = User.findById(penawaran.user_id);
				RekananDetail rkn = RekananDetail.getIdRekananDefault(user.rkn_id);
				// Handle null of rekanan
				rkn = rkn == null ? new RekananDetail() : rkn;
				dokumenKontrak.namaBadanUsaha = rkn.rkn_nama;
				dokumenKontrak.alamatBadanUsaha = rkn.rkn_alamat;
				dokumenKontrak.namaPenyedia = rkn.rkn_nama;
				dokumenKontrak.alamatPenyedia = rkn.rkn_alamat;
				dokumenKontrak.teleponPenyedia = rkn.rkn_telepon;
				dokumenKontrak.websitePenyedia = rkn.rkn_website;
				dokumenKontrak.faksPenyedia = rkn.rkn_fax;
				dokumenKontrak.emailPenyedia = rkn.rkn_email;

			}else{
				Penyedia penyedia = Penyedia.findById(pid);
				dokumenKontrak.namaBadanUsaha = penyedia.nama_penyedia;
				dokumenKontrak.alamatBadanUsaha = penyedia.alamat;
				dokumenKontrak.namaPenyedia = penyedia.nama_penyedia;
				dokumenKontrak.alamatPenyedia = penyedia.alamat;
				dokumenKontrak.teleponPenyedia = penyedia.no_telp;
				dokumenKontrak.websitePenyedia = penyedia.website;
				dokumenKontrak.faksPenyedia = penyedia.no_fax;
				dokumenKontrak.emailPenyedia = penyedia.email;
			}
		}else{
			DokumenKontrak dataKontrak = new Gson().fromJson(penawaran.data_kontrak, DokumenKontrak.class);
			dokumenKontrak=dataKontrak;
		}

		render("penyedia/KontrakCtr/inputDataKontrak.html",pid, komoditasList,dokumenKontrak,komoditas, type);
	}

	@AllowAccess({Acl.COM_INPUT_DATA_KONTRAK})
	public static void submitInputKontrak(DokumenKontrak dokumenKontrak, Long pid, String type) {
		Penawaran penawaran = Penawaran.findById(pid);
		Gson g = new Gson();
		try {
			if (penawaran != null){
				String dataKontrak = g.toJson(dokumenKontrak);
				penawaran.data_kontrak= dataKontrak;
				penawaran.save();
				flash.success(Messages.get("notif.success.submit"));
				renderInputDataKontrak(pid,type);
			}
		}catch (Exception e){
			flash.error(Messages.get("notif.failed.submit"));
			renderInputDataKontrak(pid,type);
		}

	}


	public static void setKontrakActive(long id){
		PenyediaKontrak pk = PenyediaKontrak.findById(id);
		notFoundIfNull(pk);
		List<PenyediaKontrak> penyediaKontrak = PenyediaKontrak.getByPenyediaPenawaran(pk.penyedia_id, pk.penawaran_id);
		for (PenyediaKontrak p : penyediaKontrak) {
			p.setNonActive();
		}
		pk.setActive();
		Penawaran.updateKontrak(id, pk.penawaran_id);
		Logger.info("=== " + pk.penyedia_id);
		flash.success(Messages.get("record_update_aktifnonaktif_relation_success"));
//		index(pk.penyedia_id);
		listKontrakPenawaran(pk.penawaran_id, pk.penyedia_id);
	}

	public static void setKontrakNonActive(long id){
		PenyediaKontrak pk = PenyediaKontrak.findById(id);
		notFoundIfNull(pk);
		pk.setNonActive();
		flash.success(Messages.get("record_update_aktifnonaktif_relation_success"));
//		index(pk.penyedia_id);
		listKontrakPenawaran(pk.penawaran_id, pk.penyedia_id);
	}
}
