$("#search_packages").click(function () {
    var keyword = $("#keyword").val();
    var komoditas = $("select#komoditas").val();
    // console.log(JSON.stringify(paket_ids));
    $.ajax({
        type:'post',
        url: params.searchUrl,
        data: {
            keyword: keyword,
            komoditas: komoditas,
            excludes: JSON.stringify(paket_ids)
        },
        dataType:'json',
        success:function (result) {
            if(result.status){
                var data = result.data;
                var tr;

                $("#tblPaket").find("tr:gt(0)").remove();

                for(var i=0; i<data.length;i++){
                    var disabled = "";

                    var checkArray = paket_ids.indexOf(data[i].id);
                    if(checkArray > -1){
                        disabled = "disabled='disabled'";
                    }

                    tr = $('<tr/>');
                    tr.append("<td>" + data[i].id + "</td>");
                    tr.append("<td>" + data[i].name + "</td>");
                    tr.append("<td>" +
                        "<button type='button' class='btn btn-primary choose paket-"+data[i].id+"' " +
                        "paketId='"+data[i].id+"' "+disabled+" paketName='"+data[i].name+"'>Pilih" +
                        "</button></td>");

                    $("#tblPaket").append(tr);
                }
            }

        },
        complete: function () {

        }
    });

});

$("body").on("click",".choose", function () {
    var idPaket = $(this).attr("paketId");
    var namaPaket = $(this).attr("paketName");

    //add input
    var input = "<input type='hidden' name='paket_ids' value='"+idPaket+"'>";
    $("#added_package").append(input);

    //add label view
    var row = $('<tr/>');
    row.append("<td>" + idPaket + "</td>");
    row.append("<td>" + namaPaket + "</td>");
    row.append("<td><button type='button' class='remove_package btn btn-danger' data-value='"+idPaket+"'>x</button>" );

    $("#selected_package").append(row);

    paket_ids.push(parseInt(idPaket));
    $(this).attr('disabled','disabled');

});

$(document).on("click",".remove_package", function () {
    var idPaket = $(this).data("value");

    //remove from array
    var index = paket_ids.indexOf(idPaket);
    if (index > -1) {
        paket_ids.splice(index, 1);
    }

    //remove input
    $("#added_package :input[value='"+idPaket+"']").remove();

    //remove label view
    $(this).parent().parent().remove();

    //enable button
    var button = $("#tblPaket").find(".paket-"+idPaket);
    if(button){
        button.prop('disabled',false);
    }

});
