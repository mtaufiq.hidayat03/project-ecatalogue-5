package models.purchasing;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

/**
 * Created by dadang on 11/21/17.
 */
@Table(name = "paket_anggota_ulp")
public class PaketAnggotaUlp extends BaseKatalogTable {

	@Id
	public Long id;
	public Long paket_id;
	public Long anggota_user_id;
	public String anggota_nip;
	public boolean active;

	@Transient
	public String nama_lengkap;
	@Transient
	public String user_email;

	public static void deleteByPaketId(long paket_id){
		String query = "DELETE from paket_anggota_ulp where paket_id = ?";
		Query.update(query, paket_id);
	}

	public static List<PaketAnggotaUlp> getMembersByPackageIdFront(Long packageId) {
		final String query = "SELECT p.anggota_nip, p.anggota_user_id, s.nama_lengkap, s.user_email " +
				"FROM paket_anggota_ulp p " +
				"JOIN user s " +
				"ON s.id = p.anggota_user_id " +
				"WHERE paket_id =? " +
				"AND p.active =? " +
				"AND s.active =?";
		return Query.find(query, PaketAnggotaUlp.class, packageId, AKTIF, AKTIF).fetch();
	}

}
