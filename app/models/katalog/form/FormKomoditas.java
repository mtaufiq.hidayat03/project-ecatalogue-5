package models.katalog.form;

import models.katalog.Komoditas;
import models.katalog.komoditasmodel.KomoditasTemplateJadwal;
import models.prakatalog.Usulan;

import java.io.Serializable;
import java.util.List;

/**
 * Created by raihaniqbal on 9/7/17.
 */
public class FormKomoditas implements Serializable {

    public Komoditas komoditas;
    public Usulan usulan;
    public KomoditasTemplateJadwal templateJadwal;
    public Long[] tahapan_arr;
    public Long penawaran;
    public Long negosiasi;
    public String[] manufaktur;
    public String positions;
    public String kategoriHtml;
    public List<Long> jadwal;
    public List<Long> lokasiKldi;
    public String action = "create";
    public String[] tipeAtribut;
    public String[] deskripsiTipeAtribut;

    public FormKomoditas() {}

    public FormKomoditas(Long templateId) {
        this.templateJadwal = KomoditasTemplateJadwal.findById(templateId);
        this.komoditas = Komoditas.getDetailById(templateJadwal.komoditas_id);
        this.penawaran = this.templateJadwal.tahapan_penawaran;
        this.negosiasi = this.templateJadwal.tahapan_negosiasi;
    }

    public boolean isCreateMode() {
        return action.equalsIgnoreCase("create");
    }

}
