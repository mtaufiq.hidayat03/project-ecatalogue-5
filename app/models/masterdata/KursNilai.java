package models.masterdata;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import utils.DateTimeUtils;

import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

@Table(name = "kurs_nilai")
public class KursNilai extends BaseKatalogTable {

    @Id
    public Long id;
    public Date tanggal_kurs;
    public Long kurs_id_from;
    public Long kurs_sumber_id;
    public Double nilai_beli;
    public Double nilai_jual;
    public Double nilai_tengah;

    @Transient
    public Kurs getKurs() {
        return Kurs.findById(kurs_id_from);
    }
    

    public static Long getJumlahKursNilaiByTanggal(Date tanggalKurs) {
        return KursNilai.count("tanggal_kurs=?", DateTimeUtils.formatDateToString(tanggalKurs,"yyyy-MM-dd"));
    }

    public static KursNilai getByTanggalAndKurs(Date tanggalKurs, Long kursId){
        return find("kurs_id_from = ? AND tanggal_kurs = ?", kursId, DateTimeUtils.formatDateToString(tanggalKurs,"yyyy-MM-dd")).first();
    }

    public static KursNilai getNewestKursByKursId(long kurs_id){
        return find("kurs_id_from = ? order by tanggal_kurs DESC", kurs_id).first();
    }

    public static List<KursNilai> getByKursId(long kurs_id){
        return find("kurs_id_from = ? order by tanggal_kurs DESC", kurs_id).fetch();
    }

}
