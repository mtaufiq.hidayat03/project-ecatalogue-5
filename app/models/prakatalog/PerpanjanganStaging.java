package models.prakatalog;

import com.google.gson.Gson;
import models.BaseKatalogTable;
import models.katalog.Produk;
import play.Logger;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Table(name = "perpanjangan_staging")
public class PerpanjanganStaging extends BaseKatalogTable {
    @Id
    public Long id;
    public Long perpanjangan_id;
    //botes important: object.getClass().getName();
    public String class_name;
    public boolean isList;
    public String object_json;

    public Object getRealObjec(){
        Gson gson = new Gson();
        Object target = new Object();
        Class c2 = null;
        try {
            c2 = Class.forName(class_name);
            target = gson.fromJson(object_json,c2);
        } catch (ClassNotFoundException e) {
            Logger.debug("failed get Objek");
        }
        return target;
    }

    public static PerpanjanganStaging getObjectOfClass(String className, Long perpanjangan_id){
        return PerpanjanganStaging.find("class_name=? and perpanjangan_id=?",className,perpanjangan_id).first();
    }

    public List<Object> getListData(Type type){
        return new Gson().fromJson(object_json,type);
    }

    public List<Produk> getProdukPerpanjangan(Long id){
        List<Produk> listProduk = new ArrayList<>();
        try{
            listProduk = PerpanjanganStaging.find("class_name=? and perpanjangan_id=?",Produk.class.getName() ,perpanjangan_id).fetch();
        }catch (Exception e){};
        return listProduk;
    }
}
