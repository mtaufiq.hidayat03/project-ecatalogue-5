package models.permohonan;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

@Table(name = "permohonan_pembaruan_status")
public class PembaruanStatus extends BaseKatalogTable {

    @Id
    public Long id;
    public Long permohonan_pembaruan_id;
    public String status_permohonan;
    public String from_status;
    public String to_status;
    public String deskripsi;
    public Integer active = 1;

    // status permohonan
    public static final String STATUS_PERMOHONAN_PERSIAPAN = "persiapan";
    public static final String STATUS_PERMOHONAN_SETUJU_DIREKTUR = "direktur_setuju";
    public static final String STATUS_PERMOHONAN_SETUJU_KASUBDIT = "kasubdit_setuju";
    public static final String STATUS_PERMOHONAN_LENGKAP = "data_lengkap";
    public static final String STATUS_PERMOHONAN_TIDAK_LENGKAP = "data_tidak_lengkap";
    public static final String STATUS_PERMOHONAN_SETUJU_KASI = "kasi_setuju";
    public static final String STATUS_PERMOHONAN_TOLAK_KASI = "kasi_tolak";
    public static final String STATUS_PERMOHONAN_SETUJU_ADMIN = "admin_setuju";
    public static final String STATUS_PERMOHONAN_TOLAK_ADMIN = "admin_tolak";
    public static final String STATUS_PERMOHONAN_KLARIFIKASI_DITERIMA = "klarifikasi_diterima";
    public static final String STATUS_PERMOHONAN_KLARIFIKASI_DITOLAK = "klarifikasi_ditolak";
    public static final String STATUS_PERMOHONAN_KLARIFIKASI_BA = "berita_acara_klarifikasi";
    public static final String STATUS_PERMOHONAN_KLARIFIKASI_ADMIN = "admin_lapor_klarifikasi";
    public static final String STATUS_PERMOHONAN_KLARIFIKASI_KASI = "kasi_lapor_klarifikasi";
    public static final String STATUS_PERMOHONAN_KLARIFIKASI_KASUBDIT = "kasubdit_lapor_klarifikasi";
    public static final String STATUS_PERMOHONAN_KLARIFIKASI_DIREKTUR = "direktur_terima_klarifikasi";
    public static final String STATUS_PERMOHONAN_VERIFIKASI_PIHAK_KETIGA = "verifikasi_pihak_ketiga";
    public static final String STATUS_PERMOHONAN_KLARIFIKASI = "klarifikasi";
    public static final String STATUS_PERMOHONAN_KLARIFIKASI_TIDAK_DIULANG = "klarifikasi_diterima";
    public static final String STATUS_ADENDUM_KONTRAK = "adendum_kontrak";
    public static final String STATUS_TANPA_ADENDUM_KONTRAK = "tanpa_adendum_kontrak";
    public static final String STATUS_PERMOHONAN_SELESAI = "selesai";
    public static final String STATUS_PERMOHONAN_BATAL = "batal";

    // from / to status
    public static final String STATUS_DIREKTUR = "direktur";
    public static final String STATUS_KASUBDIT = "kasubdit";
    public static final String STATUS_KASI = "kasi";
    public static final String STATUS_ADMIN = "admin";
    public static final String STATUS_PENYEDIA = "penyedia";


    public static PembaruanStatus getLatestStatusByPermohonanId(Long id) {
        return find("permohonan_pembaruan_id =? AND active =?", id, AKTIF).first();
    }
}
