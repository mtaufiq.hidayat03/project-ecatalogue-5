package models.katalog;

import controllers.katalog.contracts.ProductContract;
import controllers.katalog.form.ProductForm;
import ext.DateBinder;
import ext.FormatUtils;
import ext.StringFormat;
import jobs.ExtProduct.PenyediaKomoditasApi;
import models.AutoSequence;
import models.BaseKatalogTable;
import models.common.AktifUser;
import models.elasticsearch.product.ProductPriceElastic;
import models.katalog.komoditasmodel.KomoditasManufaktur;
import models.masterdata.Kabupaten;
import models.masterdata.Manufaktur;
import models.masterdata.Provinsi;
import models.penyedia.Penyedia;
import models.penyedia.PenyediaKontrak;
import models.permohonan.PembaruanStaging;
import models.prakatalog.Penawaran;
import models.prakatalog.Usulan;
import models.prakatalog.contracts.PenawaranStatusContract;
import models.util.Config;
import models.util.produk.HargaProdukJson;
import models.util.produk.RiwayatHargaProdukJson;
import org.apache.commons.lang.StringUtils;
import org.apache.http.util.TextUtils;
import org.sql2o.ResultSetHandler;
import play.Logger;
import play.cache.Cache;
import play.data.binding.As;
import play.db.jdbc.CacheMerdeka;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.mvc.Router;
import utils.DateTimeUtils;
import utils.LogUtil;
import utils.UrlUtil;

import javax.persistence.Transient;
import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

//import org.omg.PortableInterceptor.ACTIVE;

@Table
@CacheMerdeka
public class Produk extends BaseKatalogTable implements ProductContract, PenawaranStatusContract {

	public static final String TAG = "Produk";

	public static int LIMIT_POP_PRODUK = 8;
	public static int LIMIT_CHAR_TITLE = 40;

	public static final String TYPE_LOCAL = "lokal";
	public static final String TYPE_IMPORT = "import";

	@Id
	public Long id;
	public Integer komoditas_id;
	public Integer produk_kategori_id;
	public Integer penawaran_id;
	public Long penyedia_id;
	public Long manufaktur_id;
	public Long unspsc_id;
	public Long unit_pengukuran_id;
	public String no_produk;
	public String no_produk_penyedia;
	public String nama_produk;
	public String jenis_produk;
	public String url_produk;
	public BigDecimal jumlah_stok = new BigDecimal(0);
	public String tkdn_produk;
	public Integer apakah_dapat_diedit = 0;

	@Transient
	public Integer jumlah_stok_form = 0;
	@Transient
	public Integer jumlah_stok_inden_form = 0;
	@Transient
	public String ongkirJson = "";
	public BigInteger jumlah_stok_inden = BigInteger.valueOf(0);
	public boolean minta_disetujui;
	public Long minta_disetujui_oleh;
	public Date minta_disetujui_tanggal;
	public String minta_disetujui_alasan;
	public String setuju_tolak;
	public Date setuju_tolak_tanggal;
	public Long setuju_tolak_oleh;
	public String setuju_tolak_alasan;
	public BigDecimal harga_utama;
	public BigDecimal harga_ongkir;
	public String ongkir;
	public Date harga_tanggal;
	public Long harga_kurs_id;
	public Double margin_harga;
	public Integer agr_total_gambar;
	public Integer agr_total_lampiran;
	public Integer agr_total_wilayah_jual;
	public boolean apakah_dapat_dibeli;
	public String spesifikasi;
	public boolean active;

	@As(binder = DateBinder.class)
	public Date berlaku_sampai;

	public String status;

	public String image_50x50;
	public String image_100x100;
	public String image_300x300;
	public String image_800x800;

	public Integer agr_total_dibeli;

	public Boolean is_umkm;

//	public boolean editable;

	public Date edit_able;

	public boolean apakah_ditayangkan;

	public Long kontrak_id;

	public String produk_gambar_thumb_url;
	public String produk_gambar_file_name;
	public String produk_gambar_file_sub_location;
	public String produk_gambar_file_url;

	@Transient
	public String kelas_harga;

	@Transient
	public String nama_penyedia;

	@Transient
	public Double harga_utama_daerah;

	@Transient String nama_komoditas;

	@Transient
	public String url_gambar_utama;
	@Transient
	public String status_penawaran;

	@Transient
	public List<ProdukAtributValue> produkAtributValues;

	@Transient
	public String nama_manufaktur;

	@Transient
	public String nama_kurs;

	@Transient
	public Long kurs_id;

	@Transient
	public boolean perlu_negosiasi;

	@Transient
	public Long daerah_id;

	@Transient
	public String daerah_name;

	@Transient
	public String nama_minta_disetujui;

	@Transient
	public String nama_setuju_tolak;

	@Transient
	public String nama_unit_pengukuran;

	@Transient
	public transient ProdukHarga produkHarga;

	@Transient
	public transient List<ProdukHarga> prices;

	@Transient
	public transient Penyedia penyedia;
	@Transient
	public transient Komoditas komoditas;
	@Transient
	public transient ProdukKategori produkKategori;
	@Transient
	public transient Manufaktur manufaktur;

	@Transient
	public transient List<ProdukGambar> pictures = new ArrayList<>();
	@Transient
	public transient List<ProdukLampiran> documents = new ArrayList<>();
	@Transient
	public transient List<ProdukAtributValue> specifications = new ArrayList<>();
	@Transient
	public transient List<ProdukRiwayat> histories = new ArrayList<>();
	@Transient
	public transient ProdukWilayahJualProvinsi provincePrices;
	@Transient
	public transient ProdukWilayahJualKabupaten regencyPrices;
	@Transient
	public transient Penawaran penawaran;
	@Transient
	public String sudah_tayang;
	@Transient
	public String manufaktur_check;
	@Transient
	public String manufaktur_input;

	public static final String STATUS_MENUNGGU_PERSETUJUAN = "menunggu_persetujuan";
	public static final String STATUS_REVISI = "revisi";
	public static final String STATUS_TERIMA = "diterima";
	public static final String STATUS_TOLAK = "ditolak";
	public static final String STATUS_TAYANG = "tayang";
	public static final String STATUS_TURUN_TAYANG = "turun_tayang";
	public static final String STATUS_MENUNGGU_TAYANG = "menunggu_tayang";
	public static final String STATUS_DRAFT = "draft";

	public static final String SETUJU = "setuju";
	public static final String TOLAK = "tolak";

	public String getOngkir(){
		return this.ongkir == null ? "[]" : this.ongkir;
	}

	public Produk() {}

	@Transient
	private static String key1 = "findManufakturByKomoditas";
	public Produk setData(Produk produk, ProductForm model, Integer stok, Integer stok_inden) {
		this.komoditas_id = produk.komoditas_id;

		Komoditas komoditasCek = Komoditas.findById(produk.komoditas_id);

		if (model.isCreatedMode() || model.isDefaultDraft()){
			if(komoditasCek.penyedia_input_merk) {
				if (produk.manufaktur_check.equalsIgnoreCase("1")) {
					this.manufaktur_id = produk.getManufakturIdWithDefault();
				} else {
					Manufaktur getManufaktur = Manufaktur.findManufakturByNamaManufaktur(produk.manufaktur_input);
					if (getManufaktur == null) {
						Manufaktur man = new Manufaktur();
						man.nama_manufaktur = produk.manufaktur_input;
						man.active = true;
						long manufaktur_id = man.save();

						KomoditasManufaktur km = new KomoditasManufaktur();
						km.komoditas_id = produk.komoditas_id.longValue();
						km.manufaktur_id = manufaktur_id;
						km.active = true;
						km.save();

						this.manufaktur_id = manufaktur_id;
					} else {
						KomoditasManufaktur kmExisting = KomoditasManufaktur.findByKomoditasManufaktur(getManufaktur.id, produk.komoditas_id.longValue());

						if (kmExisting == null) {
							KomoditasManufaktur km = new KomoditasManufaktur();
							km.komoditas_id = produk.komoditas_id.longValue();
							km.manufaktur_id = getManufaktur.id;
							km.active = true;
							km.save();
						}

						this.manufaktur_id = getManufaktur.id;
					}
				}
			}else {
				this.manufaktur_id = produk.getManufakturIdWithDefault();
			}
			//update baru 24 feb 2020
			List<Manufaktur> results;
			String query = "select m.nama_manufaktur, m.id \n" +
					"from manufaktur m \n" +
					"join komoditas_manufaktur km on m.id = km.manufaktur_id \n" +
					"where km.komoditas_id = ? and m.active = ? ";
			results = Query.find(query, Manufaktur.class, produk.komoditas_id.longValue(), AKTIF).fetch();
			Cache.set(key1+produk.komoditas_id.longValue(), results, "30mn");
		}
		if (this.manufaktur_id == null) {
			this.manufaktur_id = 0L;
		}
		this.no_produk_penyedia = produk.no_produk_penyedia;
		this.jenis_produk = produk.jenis_produk;
		this.unit_pengukuran_id = produk.unit_pengukuran_id == null ? 0 : produk.unit_pengukuran_id;
		this.nama_produk = produk.nama_produk;
		this.berlaku_sampai = produk.berlaku_sampai;

		//this.jumlah_stok = this.jumlah_stok != null ? this.jumlah_stok : new BigDecimal(0);
		//this.jumlah_stok_inden = this.jumlah_stok_inden != null ? this.jumlah_stok_inden : BigInteger.valueOf(0);

		//bgs2018 bugsfixing
		this.jumlah_stok = null == this.jumlah_stok
				? new BigDecimal(0)
				: new BigDecimal(stok == null ? 0 : stok);
		this.jumlah_stok_inden = null == this.jumlah_stok_inden
				? new BigInteger("0")
				: new BigInteger(String.valueOf(stok_inden == null ? 0 : stok_inden));
		//bgs2018 bugsfixing

		this.produk_kategori_id = produk.produk_kategori_id == null ? 0 : produk.produk_kategori_id;
		this.harga_kurs_id = produk.harga_kurs_id == null ? 0 : produk.harga_kurs_id;
		this.penawaran_id = produk.penawaran_id;
		this.unspsc_id = produk.unspsc_id == null ? 0 : produk.unspsc_id;
		setProductNumber(model.kode_komoditas, this.unspsc_id);
		this.agr_total_gambar = model.getTotalPicture();
		this.agr_total_lampiran = model.getTotalAttachment();
		this.agr_total_wilayah_jual = model.getAgrWilayahJual();
		this.apakah_dapat_dibeli = false;
		this.apakah_ditayangkan = false;
		this.active = true;
		// this.harga_utama = model.getMainPrice();
		 this.harga_tanggal = model.parseKursDate();//mengacu pada model.tanggal_harga
		 // notes: nanti pas save akan ada overide, waktu kurs date
		// dari input form
		this.ongkir = produk.ongkir == null ? "" : produk.ongkir;
		this.tkdn_produk = produk.tkdn_produk;
		return this;
	}

	public Long getManufakturIdWithDefault() {
		return manufaktur_id == null ? 0 : manufaktur_id;
	}

	public String getPrice(){
		return FormatUtils.formatCurrencyRupiah(harga_utama);
	}

	public void setProductNumber(String commodityCode, Long unspscId) {
		this.no_produk = generateProductNumber(
				this.no_produk,
				commodityCode,
				unspscId,
				new AutoSequence().getSequence()
		);
	}

	public static String generateProductNumber(
			String currentNoProduct,
			String commodityCode,
			Long unspscId,
			String sequence
	) {
		if (unspscId == null || unspscId == 0) {
			return currentNoProduct;
		}
		LogUtil.debug(TAG, "unsp : " + unspscId + " commodityCode : " + commodityCode);
		String newNoProduct = currentNoProduct;
		String unspscFromNoProduct = null;
		if (!TextUtils.isEmpty(currentNoProduct)) {
			String[] result = currentNoProduct.split("-");
			unspscFromNoProduct = result.length > 1 ? result[0] : null;
		}
		if (TextUtils.isEmpty(currentNoProduct)) {
			newNoProduct = String.format("%1d-%2s-%3s", unspscId, commodityCode, sequence);
		} else if (!TextUtils.isEmpty(unspscFromNoProduct)
				&& !unspscFromNoProduct.equalsIgnoreCase(unspscId.toString())) {
			newNoProduct = String.format("%1s%2s", unspscId, currentNoProduct.substring(currentNoProduct.indexOf("-")));
		}
		LogUtil.debug(TAG, newNoProduct);
		return newNoProduct;
	}


	public String getLastUniqueNumber() {
		try {
			if (!TextUtils.isEmpty(this.no_produk)) {
				return this.no_produk.substring(this.no_produk.lastIndexOf("-") + 1);
			}
		} catch (StringIndexOutOfBoundsException e) {
			LogUtil.e("Produk", e);
		}
		return "XXXXX";
	}
	
	@CacheMerdeka
	public static List<Produk> findByProduk_kategori_id(Integer produk_kategori_id)
	{
		return find("produk_kategori_id=?", produk_kategori_id).fetch();
	}
	
	public String getTrimmedTitle() {
		if(this.nama_produk.length() < LIMIT_CHAR_TITLE) {
			return this.nama_produk;
		}
		return this.nama_produk.substring(0, LIMIT_CHAR_TITLE) + "...";
	}
	
	public ProdukGambar getGambarUtama()
	{
		return ProdukGambar.findFirstByProduk(id);
	}

	public static List<Produk> findByNama(String nama_produk) {
		return find("MATCH(nama_produk) AGAINST(?)", Produk.class, nama_produk).fetch();
	}
	
	public static final ResultSetHandler<String[]> resultset= new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[3];
			String url=Router.reverse("controllers.DevelopmentCtr.produkView").url ;
			tmp[0] = rs.getString("no_produk");
			tmp[1] =  
					String.format("<a href='%s?id=%s' class='popup'>%s</a>", url, rs.getLong("id"), rs.getString("nama_produk"));
			Config conf=Config.getInstance();
			tmp[2] = StringUtils.isEmpty(rs.getString("gambar"))? "" :
				String.format("<img src='%s/produk_gambar/%s'>", conf.cdnServerImage100, rs.getString("gambar"));
			return tmp;
		}
	};

	public static List<Produk> findAllActive(long komoditas_id){
		return find("active = ? and komoditas_id = ?", Produk.class,1, komoditas_id).fetch();
	}

	public static List<Produk> findAllActives(){
		return find("active = 1", Produk.class).fetch();
	}

	public static int tolakSetujuProduk(long produk_id, int setujuTolak){
		String query = "Update produk set setuju_tolak = ? where id = ?";

		return Query.update(query, produk_id, setujuTolak);
	}

	public static Produk getProdukKomoditas(long produk_id){
		String query = "SELECT p.*, k.kelas_harga as kelas_harga from produk p join komoditas k on p.komoditas_id = k.id " +
				"where p.id = ?";

		return Query.find(query, Produk.class, produk_id).first();
	}

	public static boolean isExistByNameAndNoProduct(Produk produk, long pid){
		boolean result = false;
		String qry = "SELECT * FROM produk p WHERE RTRIM(LTRIM(LOWER(p.nama_produk)))= " +
				"LTRIM(RTRIM(LOWER('" + produk.nama_produk + "'))) AND " +
				"TRIM(LOWER(p.no_produk_penyedia)) = TRIM(LOWER('" + produk.no_produk_penyedia + "')) AND p.penawaran_id = '" + pid + "' ";
		Logger.debug("cek nama,noprodukp:"+qry);
		List<Produk> list = new ArrayList<>();
		try {
			list = Query.find(qry, Produk.class).fetch();
			// cek duplikat
			result = list.size() > 0;
			if(produk.id != null){
				Produk produkOld = produk.findById(produk.id);
				if(produkOld.nama_produk.equalsIgnoreCase(produk.nama_produk) && produkOld.no_produk_penyedia.equalsIgnoreCase(produk.no_produk_penyedia)){
					result = false;
				}
			}
		} catch (Exception e) {
			LogUtil.error(TAG, e);
		}
		return result;
	}

	public static boolean isExistNoProduct(Produk produk, long pid){
		boolean result = false;
		String qry = "SELECT * FROM produk p WHERE TRIM(LOWER(p.no_produk_penyedia)) = TRIM(LOWER('" + produk.no_produk_penyedia + "')) AND p.penawaran_id = '" + pid + "' ";
		Logger.debug("cek nama,noprodukp:"+qry);
		List<Produk> list = new ArrayList<>();
		try{
			list = Query.find(qry, Produk.class).fetch();
			// cek duplikat
			result = list.size() > 0;
			if(produk.id != null){
				Produk produkOld = produk.findById(produk.id);
				if(produkOld.no_produk_penyedia.equalsIgnoreCase(produk.no_produk_penyedia)){
					result = false;
				}
			}
		}catch (Exception e){}
		return result;
	}
	public static boolean isExistByName(Produk produk, long pid){
		boolean result = false;
		String qry = "SELECT * FROM produk p WHERE RTRIM(LTRIM(LOWER(p.nama_produk)))= " +
				"LTRIM(RTRIM(LOWER('" + produk.nama_produk + "'))) AND p.penawaran_id = '" + pid + "' " ;
		List<Produk> list = new ArrayList<>();
		try{
			list = Query.find(qry, Produk.class).fetch();
			// cek duplikat
			result = list.size() > 0;
			if(produk.id != null){
				Produk produkOld = produk.findById(produk.id);
				if(produkOld.nama_produk.equalsIgnoreCase(produk.nama_produk)){
					result = false;
				}
			}
		}catch (Exception e){}
		return result;
	}
	public static Produk getProdukByKategori(long id) {
		return find("produk_kategori_id =?", id).first();
	}

	public static List<Produk> getPopulkarProduk() {
		String query = "select p.id, p.nama_produk, pn.nama_penyedia as nama_penyedia, p.harga_utama, p.image_800x800 " +
				"from produk p " +
				"join penyedia pn on p.penyedia_id = pn.id join penyedia_kontrak pk on pn.id = pk.penyedia_id " +
				"and p.komoditas_id = pk.komoditas_id join komoditas k on p.komoditas_id = k.id " +
				"where (p.harga_utama != null or p.harga_utama != 0) and p.active = 1 and p.apakah_ditayangkan = 1 " +
				"and p.minta_disetujui = 0 and berlaku_sampai >= curdate() and p.setuju_tolak = 'setuju' " +
				"and pk.tgl_masa_berlaku_mulai <= curdate() and pk.tgl_masa_berlaku_selesai >= curdate() and pk.active = 1 AND pk.apakah_berlaku = 1 " +
				"and (k.apakah_stok_unlimited = 1 or (k.apakah_stok_unlimited = 0 and p.jumlah_stok > 0)) " +
				"order by p.agr_total_dibeli DESC limit ?";

		return Query.find(query, Produk.class, LIMIT_POP_PRODUK).fetch();
	}

	public Penyedia getPenyedia(){
		return Penyedia.findById(penyedia_id);
	}

	public String getGambarUrl() {
		Config conf = Config.getInstance();
		String gambar = produk_gambar_file_sub_location + produk_gambar_file_name;
		return StringUtils.isEmpty(gambar) ? "" : conf.cdnServerImage100 + File.separator + "produk_gambar" + File.separator + gambar;
	}

	public static void approveProduct(long id, String alasan){
		String query = "UPDATE produk SET " +
				"status = ?, " +
				"setuju_tolak = ?, " +
				"minta_disetujui = ?, " +
				"apakah_dapat_dibeli = ?, " +
				"apakah_ditayangkan = ?, " +
				"apakah_dapat_diedit = ?, " +
				"setuju_tolak_tanggal = ?, " +
				"setuju_tolak_oleh = ?, " +
				"setuju_tolak_alasan = ?, " +
				"modified_date =?, " +
				"modified_by =? " +
				"WHERE id =?";
		final String date = DateTimeUtils.getStandardDateTime();
		final Integer user = AktifUser.getActiveUserId();
		Query.update(query,STATUS_TERIMA, "setuju", "0", "1", "1", "0", date, user, alasan, date, user, id);
	}

	public static void rejectProduct(long id, String alasan){
		String query = "UPDATE produk SET " +
				"status = ?, " +
				"setuju_tolak = ?, " +
				"minta_disetujui = ?, " +
				"apakah_dapat_dibeli = ?, " +
				"apakah_ditayangkan = ?, " +
				"apakah_dapat_diedit = ?, " +
				"setuju_tolak_tanggal = ?, " +
				"setuju_tolak_oleh = ?, " +
				"setuju_tolak_alasan = ?, " +
				"modified_date =?, " +
				"modified_by =? " +
				"WHERE id =?";
		final String date = DateTimeUtils.getStandardDateTime();
		final Integer user = AktifUser.getActiveUserId();
		Query.update(query,STATUS_TOLAK, "tolak", "0", "0", "0", "1", date, user, alasan, date, user, id);
	}

	public static void setRejectedAllUnapprovedProductByPenawaran(long penawaranId){
		String query = "UPDATE produk SET " +
				"setuju_tolak = ?," + 
				"setuju_tolak_oleh = ?," + 
				"setuju_tolak_tanggal = ?, " +
				"minta_disetujui = 0, " +
				"status = ? " +
				"WHERE penawaran_id = ? and status = ?";
		final String date = DateTimeUtils.getStandardDateTime();
		final Integer user = AktifUser.getActiveUserId();
		Query.update(query, TOLAK, user, date, STATUS_TOLAK, penawaranId, STATUS_MENUNGGU_PERSETUJUAN);
	}

//	public static Integer kurangiStok(long produk_id, int jumlah){
//		Produk produk = find("id = ?", produk_id).first();
//		if(produk.jumlah_stok < jumlah){
//			return -1;
//		}
//		produk.jumlah_stok = produk.jumlah_stok - jumlah;
//		return produk.save();
//	}

	public static BigDecimal cekStok(long produk_id, BigDecimal jumlah){
		Produk produk = find("id = ?", produk_id).first();

		return produk.jumlah_stok;
	}

	public static void updatePenyediaProdukByPenawaran(Penawaran penawaran, long penyedia_id, long kontrak_id, boolean apakah_berlaku){
		String query = "UPDATE produk " +
				"SET penyedia_id = ?, " +
				"apakah_ditayangkan = ?, " +
				"apakah_dapat_dibeli = ?, " +
				"kontrak_id = ?, " +
				"active = ?, " +
				"modified_date = ?, " +
				"modified_by = ?," +
				"status = ?  " +
				"WHERE penawaran_id = ? AND status = ?";

		LogUtil.d("apakah_berlaku", apakah_berlaku);
		int berlaku = apakah_berlaku ? 1 : 0;

		LogUtil.d("berlaku",berlaku);
		LogUtil.d("id kontrak",kontrak_id);
		LogUtil.d("penawaran_id",penawaran.id);

		Query.update(
				query,
				penyedia_id,
				berlaku,
				berlaku,
				kontrak_id, AKTIF,
				DateTimeUtils.getStandardDateTime(),
				AktifUser.getActiveUserId(),
				STATUS_TAYANG,
				penawaran.id,
				STATUS_TERIMA
		);

	}

	public static void updateTayangByKontrak(PenyediaKontrak kontrak){
		String query = "UPDATE produk " +
				"SET status = ?," +
				" apakah_ditayangkan = ?," +
				" apakah_dapat_dibeli = ?, " +
				" modified_date = ?, " +
				" modified_by = ? " +
				"WHERE kontrak_id = ? AND active = ?";

		String status = Produk.STATUS_TURUN_TAYANG;
		if(kontrak.apakah_berlaku){
			status = Produk.STATUS_TAYANG;
		}

		int berlaku = kontrak.apakah_berlaku ? 1 : 0;
		int userId = AktifUser.getActiveUserId();
		String date = DateTimeUtils.getStandardDateTime();
		Query.update(query,status,berlaku,berlaku, date, userId, kontrak.id, AKTIF);

	}

	public String getExpiredString() {
		return DateTimeUtils.convertToDdMmYyyy(this.berlaku_sampai);
	}

	public String getPriceDateString() {
		return DateTimeUtils.convertToDdMmYyyy(this.harga_tanggal);
	}

	public void saveProduct() {
		final long result = save();
		if (this.id == null) {
			this.id = result;
		}
	}

	public static List<Produk> findByPenawaranAndMenungguPersetujuan(long penawaran_id){
		return find("penawaran_id = ? and status = ?", penawaran_id, STATUS_MENUNGGU_PERSETUJUAN).fetch();
	}

	public static List<Produk> findByPenawaran(long penawaran_id){
		return find("penawaran_id = ?", penawaran_id).fetch();
	}

	public static List<Produk> findAllProdukByPenawaran(Long penawaran_id){
		String query = "select " +
				"p.id" +
				",p.komoditas_id" +
				",k.nama_komoditas" +
				",p.produk_kategori_id" +
				",pk.nama_kategori" +
				",p.penyedia_id" +
				",py.nama_penyedia" +
				",p.manufaktur_id" +
				",m.nama_manufaktur" +
				",p.unspsc_id" +
				",p.unit_pengukuran_id" +
				",up.nama_unit_pengukuran" +
				",p.no_produk" +
				",p.no_produk_penyedia" +
				",p.nama_produk" +
				",p.jenis_produk" +
				",p.setuju_tolak" +
				",p.setuju_tolak_tanggal" +
				",p.setuju_tolak_oleh" +
				",p.setuju_tolak_alasan" +
				",p.harga_utama" +
				",p.harga_tanggal" +
				",p.harga_kurs_id" +
				",p.margin_harga" +
				",p.berlaku_sampai" +
				",p.kontrak_id" +
				",p.ongkir " +
				"from produk p " +
				"left join penyedia py on p.penyedia_id = py.id " +
				"left join komoditas k on p.komoditas_id = k.id " +
				"left join produk_kategori pk on p.produk_kategori_id = pk.id " +
				"left join manufaktur m on p.manufaktur_id = m.id " +
				"left join unit_pengukuran up on p.unit_pengukuran_id = up.id " +
				"where penawaran_id = ?";

		return Query.find(query, Produk.class, penawaran_id).fetch();
	}


	public static List<Produk> findProdukByPenawaran(long penawaran_id){
		String query = "select " +
				"p.id" +
				",p.komoditas_id" +
				",k.nama_komoditas" +
				",p.produk_kategori_id" +
				",pk.nama_kategori" +
				",p.penyedia_id" +
				",py.nama_penyedia" +
				",p.manufaktur_id" +
				",m.nama_manufaktur" +
				",p.unspsc_id" +
				",p.unit_pengukuran_id" +
				",up.nama_unit_pengukuran" +
				",p.no_produk" +
				",p.no_produk_penyedia" +
				",p.nama_produk" +
				",p.jenis_produk" +
				",p.setuju_tolak" +
				",p.setuju_tolak_tanggal" +
				",p.setuju_tolak_oleh" +
				",p.setuju_tolak_alasan" +
				",p.harga_utama" +
				",p.harga_tanggal" +
				",p.harga_kurs_id" +
				",p.margin_harga" +
				",p.berlaku_sampai" +
				",p.kontrak_id" +
				",p.ongkir " +
				"from produk p " +
				"left join penyedia py on p.penyedia_id = py.id " +
				"left join komoditas k on p.komoditas_id = k.id " +
				"left join produk_kategori pk on p.produk_kategori_id = pk.id " +
				"left join manufaktur m on p.manufaktur_id = m.id " +
				"left join unit_pengukuran up on p.unit_pengukuran_id = up.id " +
				"where p.active = " + AKTIF + " and p.status in ('" + Produk.STATUS_TERIMA + "','" + Produk.STATUS_TAYANG + "') " +
				"and penawaran_id = ?";

		return Query.find(query, Produk.class, penawaran_id).fetch();
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public Integer getCreatedBy() {
		return this.created_by;
	}

	@Override
	public Long getProviderId() {
		return penyedia_id;
	}

	@Override
	public Integer getCommodityId() {
		return this.komoditas_id;
	}

	@Override
	public List<ProdukGambar> getPictures() {
		return this.pictures;
	}

	@Override
	public Long getManufactureId() {
		return this.manufaktur_id;
	}

	@Override
	public Integer getCategoryId() {
		return this.produk_kategori_id;
	}

	@Override
	public Komoditas getCommodity() {
		return this.komoditas;
	}

	@Override
	public Penyedia getProvider() {
		return this.penyedia;
	}

	@Override
	public Integer getPenawaranId() {
		return penawaran_id;
	}

	@Override
	public void setPictures(List<ProdukGambar> pictures) {
		this.pictures = pictures;
	}

	@Override
	public void setDocuments(List<ProdukLampiran> documents) {
		this.documents = documents;
	}

	@Override
	public void setPrice(ProdukHarga price) {
		this.produkHarga = price;
	}

	public void setPrices(List<ProdukHarga> _price) {
		this.prices = _price;
	}

	@Override
	public void setProvider(Penyedia penyedia) {
		this.penyedia = penyedia;
	}

	@Override
	public void setCommodity(Komoditas komoditas) {
		this.komoditas = komoditas;
	}

	@Override
	public void setSpecifications(List<ProdukAtributValue> specifications) {
		this.specifications = specifications;
	}

	@Override
	public void setProductHistory(List<ProdukRiwayat> histories) {
		this.histories = histories;
	}

	@Override
	public void setCategory(ProdukKategori category) {
		this.produkKategori = category;
	}

	@Override
	public void setManufacture(Manufaktur manufacture) {
		this.manufaktur = manufacture;
	}

	@Override
	public void setRegencyPrices(ProdukWilayahJualKabupaten model) {
		this.regencyPrices = model;
	}

	@Override
	public void setProvincePrices(ProdukWilayahJualProvinsi model) {
		this.provincePrices = model;
	}

	@Override
	public void setPenawaran(Penawaran model) {
		this.penawaran = model;
	}

	public Double getMainPrice() {
		LogUtil.d("produk", "get main price");
		if (this.regencyPrices != null) {
			return this.regencyPrices.harga_utama.doubleValue();
		} else if (this.provincePrices != null) {
			return this.provincePrices.harga_utama.doubleValue();
		} else if (this.harga_utama_daerah != null) {
			return this.harga_utama_daerah;
		} else if (this.harga_utama != null) {
			return this.harga_utama.doubleValue();
		} else {
			return 0D;
		}
	}

	public String getMainPriceString() {
		return FormatUtils.formatCurrencyRupiah(getMainPrice());
	}

	public boolean isEditAble() {
		final Date now = new Date();
		//Logger.info("tanggal editable " + edit_able);
		//return (edit_able != null && edit_able.after(now) && now.before(edit_able)) || allowToAddProduct();
		return apakah_dapat_diedit == 1 || allowToAddProduct();
	}

	public boolean userAllowEdit(AktifUser aktifUser) {
		return isEditAble() && isItMyProduct(aktifUser);
	}

	public boolean isItMyProduct(AktifUser aktifUser) {
		if (aktifUser == null) {
			return false;
		}
		if (penyedia == null) {
			withProvider();
		}
		return (penyedia_id != null && penyedia.user_id.equals(aktifUser.user_id))
				|| this.created_by == aktifUser.user_id.intValue();
	}

	public String getDetailUrl() {
		return UrlUtil.builder()
				.setUrl("katalog.produkctr.detail")
				.setParam("id", this.id)
				.setParam("type", ProductPriceElastic.getElasticPriceType(kelas_harga))
				.build();
	}

	public String getEditUrl() {
		return UrlUtil.builder()
				.setUrl("katalog.produkctr.edit")
				.setParam("produk_id", this.id)
				.build();
	}

	@Override
	public String getStatusPenawaran() {
		if (penawaran != null) {
			return penawaran.status;
		}
		return this.status_penawaran;
	}

	public String getActiveImageUrl() {
		if (pictures != null && !pictures.isEmpty()) {
            if(getPictures().get(0).posisi_file == ProdukGambar._POSISI_FILE_LOKAL){
                ProdukGambar pg =  getPictures().get(0);
                return pg.file_url = pg.getLocalImageStorage()+pg.file_sub_location+pg.file_name;
            }else {
                return pictures.get(0).getActiveUrl();
            }
		}
		return Router.getBaseUrl() + "/public/images/image-not-found.png";
	}

	public String getHargaByAttr(Long attrId, List<ProdukHarga> items){
		ProdukHarga item = items.stream().filter(x -> x.komoditas_harga_atribut_id.equals(attrId))
			.sorted(Comparator.comparing(ProdukHarga::getTanggalDibuat).reversed()).findFirst().orElse(null);
		if(item == null) return "0";
		else{
			item.setPriceJson();
			return StringFormat.formatCurrency(item.priceJson.harga);
		} 
	}

	public String getHargaByAttrAndAllLocation(Long attrId, List<ProdukHarga> items){
		ProdukHarga item = items.stream().filter(x -> x.komoditas_harga_atribut_id.equals(attrId))
			.sorted(Comparator.comparing(ProdukHarga::getTanggalDibuat).reversed()).findFirst().orElse(null);
		if(item == null) return "0";
		item.setPricesJson();
		List<String> hp = item.pricesJson.stream()
				.map(o -> {
					return getNamaLokasi(o) + " : " + StringFormat.formatCurrency(o.harga);
				}).collect(Collectors.toList());
		return hp.isEmpty() ? "-": String.join("; ", hp);
	}

	public String getHargaByAttrAndLocation(Long attrId, String tanggal, List<RiwayatHargaProdukJson> items){
		RiwayatHargaProdukJson item = items.stream().filter(x -> x.komoditas_harga_atribut_id.equals(attrId) && tanggal.equals(x.tanggal))
			.findFirst().orElse(null);
		if(item == null) return "0";
		return StringFormat.formatCurrency(item.harga);
	}

	String getNamaLokasi(HargaProdukJson hp) {
		Kabupaten kab = Kabupaten.findById(hp.kabupatenId);
		if(kab!=null)
			return kab.nama_kabupaten;
		Provinsi prov = Provinsi.findById(hp.provinsiId);
		if(prov!=null)
			return prov.nama_provinsi;
		return "";
	}

	public String getHargaByAttrAndLocation(Long attrId, Long locationId, List<ProdukHarga> items){
		ProdukHarga item = items.stream().filter(x -> x.komoditas_harga_atribut_id.equals(attrId))
			.sorted(Comparator.comparing(ProdukHarga::getTanggalDibuat).reversed()).findFirst().orElse(null);
		if(item == null) return "0";
		item.setPricesJson();
		HargaProdukJson hp = item.pricesJson.stream()
		        .filter(x -> x.kabupatenId.equals(locationId) || x.provinsiId.equals(locationId) )
		        .findFirst()
		        .orElse(null);
		if(hp == null)
			return "-";
		return getNamaLokasi(hp) + " : " + StringFormat.formatCurrency(hp.harga);
	}

	public static Produk getCurentProdukForCrawl(models.api.produk.Produk produkApi, PenyediaKomoditasApi peKomApi){
        Produk result = null;
        String sql = "select p.* from produk p where p.no_produk_penyedia = ? and p.penyedia_id=? and komoditas_id=?";
	    result = Query.find(sql, Produk.class,produkApi.informasi.no_produk_penyedia,peKomApi.penyediaid,peKomApi.komoditasid).first();
	    return result;
    }

    public Boolean isNeedAprooval(){

		try{
			String query = "select p.* from produk_tunggu_setuju pm join produk p on pm.produk_id = p.id join penawaran pen on p.penawaran_id = pen.id " +
					"join usulan us on pen.usulan_id = us.id " +
					"join user on p.created_by = user.id " +
					"join komoditas k on k.id = p.komoditas_id " +
					"where p.id=? and us.status=? and not ( p.status=? and pen.status=? ) ";
			Produk p = Query.find(query,Produk.class,
					id,
					Usulan.STATUS_PENAWARAN_DIBUKA,
					Produk.STATUS_MENUNGGU_PERSETUJUAN,
					Penawaran.STATUS_MENUNGGU_VERIFIKASI).first();
			return null != p;
		}catch (Exception e){}
		//if null tidak butuh persetujuan
		return false;
	}

	public static List<Produk> getProdukByPenyedia(Long penyediaId) {
		String query = "select p.* from produk p where penyedia_id=?";
		List<Produk> lp = Query.find(query, Produk.class, penyediaId).fetch();
		return lp;
	}

	public static Produk getProdukInfoTayang(long produk_id){
		String query = "SELECT p.id, p.produk_kategori_id, p.no_produk, p.nama_produk, m.nama_manufaktur," +
				"p.minta_disetujui, " +
				"usr1.nama_lengkap as nama_minta_disetujui, " +
				"p.minta_disetujui_tanggal, p.minta_disetujui_alasan," +
				"p.setuju_tolak, " +
				"usr2.nama_lengkap as nama_setuju_tolak, " +
				"p.setuju_tolak_tanggal, p.setuju_tolak_alasan " +
				"FROM produk p " +
				"LEFT JOIN manufaktur m on p.manufaktur_id = m.id " +
				"LEFT JOIN `user` usr1 on p.minta_disetujui_oleh = usr1.id " +
				"LEFT JOIN `user` usr2 on p.setuju_tolak_oleh = usr2.id " +
				"WHERE p.id = ?";

		return Query.find(query, Produk.class, produk_id).first();
	}

	public static List<Produk> getProdukForPerpanjangan(Long penawaran_id, Long penyedia_id, Long kontrak_id){
		String sql = "SELECT p.*\n" +
				"FROM produk p\n" +
				"       JOIN produk_kategori cat on cat.id = p.produk_kategori_id\n" +
				"       JOIN penawaran pw on pw.id = p.penawaran_id\n" +
				"       JOIN penyedia_kontrak pk on pk.id = pw.kontrak_id\n" +
				"       JOIN penyedia pn on pn.id = pk.penyedia_id\n" +
				"       JOIN komoditas k ON p.komoditas_id = k.id\n" +
				"       JOIN manufaktur m ON m.id = p.manufaktur_id\n" +
				"WHERE\n" +
				"    p.active = 1 AND\n" +
				"    p.apakah_ditayangkan = 1 AND\n" +
				"    k.active = 1\n" +
				"    AND (p.berlaku_sampai OR p.berlaku_sampai is not null)\n" +
				"    AND p.setuju_tolak = 'setuju'\n" +
				"    AND pk.tgl_masa_berlaku_mulai <= curdate()\n" +
				"    AND pk.tgl_masa_berlaku_selesai >= curdate()\n" +
				"    AND pk.active = 1\n" +
				"	 AND pk.apakah_berlaku = 1\n" +
				"    AND pw.id=? " + //1032
				"    AND pk.id = ? " + //2422
				"    AND pn.id = ?"; //410111
		return Query.find(sql, Produk.class, penawaran_id, kontrak_id, penyedia_id).fetch();
	}

	public static Produk findByIdCheck(Long id)
	{
		return find("id=?", id).first();
	}

	public static void historyProduct(String aksi, String alasan, Long id){
		ProdukRiwayat produkRiwayat = new ProdukRiwayat();
		produkRiwayat.produk_id = id;
		produkRiwayat.aksi = aksi;
		produkRiwayat.deskripsi = alasan;
		produkRiwayat.created_by = AktifUser.getActiveUserId();
		produkRiwayat.created_date = new Timestamp(new java.util.Date().getTime());
		produkRiwayat.save();
	}

	public static int updateStokProduk(BigDecimal stok, long produk_id, String operator){
		//update produk set jumlah_stok = (jumlah_stok-10) where id = 6905;
		String query = "update produk " +
				" set jumlah_stok = (jumlah_stok "+operator+" "+stok+")" +
				" where id = "+produk_id;

		return Query.update(query);
	}

	//			kondisi tombol beli produk (by omen req dari mas fajar 18/10/2019)
	public static boolean isCanBeBought(Produk produk){
		boolean canBeBought = false;

		try{
			List<Produk> prod = Query.find("SELECT p.id " +
					"FROM produk p " +
					"JOIN penawaran pw on pw.id = p.penawaran_id " +
					"JOIN penyedia_kontrak pk on pk.id = pw.kontrak_id " +
					"JOIN komoditas k on p.komoditas_id = k.id " +
					"WHERE p.active = 1 AND p.apakah_ditayangkan = 1 " +
					"AND k.active = 1 AND p.setuju_tolak = 'setuju' " +
					"AND ((p.berlaku_sampai >= CURDATE() AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1) " +
					"OR (p.berlaku_sampai is null AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1)) " +
					"AND (k.apakah_stok_unlimited = 1 OR (k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0)) AND p.id = ?", Produk.class, produk.id).fetch();
			canBeBought = (prod.size() > 0);
		}catch (Exception e){}
		return canBeBought;
	}

	public List<PembaruanStaging> getProdukStagging(Long pmb_id){
		List<PembaruanStaging> pmbs = PembaruanStaging.getProdukStaggingByPmbIdAndPrdId(pmb_id, id);
		return pmbs;
	}

	public boolean isDraft() {
		return !StringUtils.isEmpty(status) && status.equalsIgnoreCase(STATUS_DRAFT);
	}

	public static void updateIsUmkmbyPenyediaId(Long penyediaId, int isUmkm){
		String query = "UPDATE produk set is_umkm = ? WHERE penyedia_id = ?";
		Query.update(query,isUmkm, penyediaId );
	}
}
/*
notes by: asep
perjalanan menelusuri harga utama dan harga dengan lokasi,
* jika harga nasional maka harga ada di dua tempat yang bisa diambil
	- di produk->harga utama
	- di produkharga -> harga(single json)-> field harga
* jika harga provinsi
 */