package models.common;

import java.io.Serializable;

/**
 * model untuk informasi saat generate dokumen SK Penetapan Produk
 * @author lkpp
 *
 */

public class DokumenPenetapanProduk implements Serializable {

    public Long komoditas;

    public String namaKomoditas;

    public String namaKomoditasDokumen;

    public String tblProdukHarga;

    //pihak pertama - LKPP
    public String namaPihakPertama;

    public String jabatanPihakPertama;

    //pihak kedua - Penyedia
    public String namaBadanUsaha;

    public String alamatBadanUsaha;

    public String namaPihakKedua;

    public String jabatanPihakKedua;

    public String nomorSurat;

    public String tanggalSurat;

    public String perihalSurat;

    public String nomorBeritaAcaraNego;

    public String tanggalBeritaAcaraNego;

    public String dokumenLampiran;
}
