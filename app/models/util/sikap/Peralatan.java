package models.util.sikap;

import play.data.validation.Required;

/**
 * Created by dadang on 7/11/17.
 */
public class Peralatan {

	@Required
	public long rkn_id;
	@Required
	public long alt_id;
	@Required
	public long skp_id;
	@Required
	public String alt_jenis;
	@Required
	public String alt_kapasitas;
	@Required
	public long alt_jumlah;
	@Required
	public String alt_merktipe;
	@Required
	public String alt_thpembuatan;
	@Required
	public long alt_kondisi;
	@Required
	public String alt_lokasi;
	@Required
	public String alt_kepemilikan;
	public boolean is_verified = false;

	public String status_verifikasi;
	public String auditupdate;
	public String source_data;
	public String createdat;

	public boolean deleted = false;
	public String alasan_pembatalan = "";
	public String keterangan_pembuktian = "";
}
