package utils;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import models.common.AktifUser;
import models.prakatalog.UsulanKualifikasi;
import models.user.User;
import models.util.Config;
import models.util.Encryption;
import models.util.sikap.*;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.i18n.Messages;
import play.libs.URLs;
import play.libs.WS;
import play.mvc.Scope;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;

/**
 * Created by raihaniqbal on 8/21/17.
 */

public class SikapUtils {

    public static final String TAG = "SikapUtils";

    public static final String URL_SIKAP = Config.getInstance().sikap_api;

    //13 mei 2019,
    public static final int REPO_ID = Config.getInstance().repo_id;
    //notes static harus disamakan dari variabel sikap(mas ghoni)
    public static final String KETERANGAN_VERIFIKASI_SIKAP = "verified by katalog";

    public static final String URL_GET_IDENTITAS                = URL_SIKAP+"services/getRekananIdentitas?q=";
    public static final String URL_GET_IZINUSAHA                = URL_SIKAP+"services/getRekananIzinUsaha?q=";
    public static final String URL_GET_AKTA                     = URL_SIKAP+"services/getRekananAkta?q=";
    public static final String URL_GET_PEMILIK                  = URL_SIKAP+"services/getRekananPemilik?q=";
    public static final String URL_GET_PENGURUS                 = URL_SIKAP+"services/getRekananPengurus?q=";
    public static final String URL_GET_TENAGAAHLI               = URL_SIKAP+"services/getRekananTenagaAhli?q=";
    public static final String URL_GET_PENGALAMAN               = URL_SIKAP+"services/getRekananPengalaman?q=";
    public static final String URL_GET_PENGALAMANKLASIFIKASI    = URL_SIKAP+"services/getRekananPengalamanKlasifikasi?q=";
    public static final String URL_GET_PAJAK                    = URL_SIKAP+"services/getRekananPajak?q=";
    public static final String URL_GET_PERALATAN                = URL_SIKAP+"services/getRekananPeralatan?q=";
    public static final String URL_GET_PREFERENSI               = URL_SIKAP+"services/getRekananPreferensi?q=";
    public static final String URL_SEND_VERIFIKASI              = URL_SIKAP+"services/kirimverifikasi?q=";

    public static final String JENIS_AKTA_PENDIRIAN             = "AKTA PENDIRIAN";
    public static final String JENIS_AKTA_PERUBAHAN             = "AKTA PERUBAHAN";

    private static final int DATA_TIPE_IDENTITAS                = 1;
    private static final int DATA_TIPE_IJIN_USAHA               = 3;
    private static final int DATA_TIPE_PEMILIK                  = 4;
    private static final int DATA_TIPE_PENGURUS                 = 5;
    private static final int DATA_TIPE_TENAGA_AHLI              = 6;
    private static final int DATA_TIPE_PERALATAN                = 7;
    private static final int DATA_TIPE_PENGALAMAN               = 8;
    private static final int DATA_TIPE_PAJAK                    = 9;
    private static final int DATA_TIPE_AKTA_PENDIRIAN           = 20;
    private static final int DATA_TIPE_AKTA_PERUBAHAN           = 21;


    private static Gson getGson() {
        GsonBuilder builder = new GsonBuilder();

        TypeAdapter<String> stringTypeAdapter = stringTypeAdapter();
        builder.registerTypeAdapter(String.class, stringTypeAdapter);
        return builder.create();

    }

    private static TypeAdapter<String> stringTypeAdapter() {
        return new TypeAdapter<String>() {

            @Override
            public void write(JsonWriter out, String s) throws IOException {
                if (s == null) {
                    out.nullValue();
                    return;
                }
                out.value(s);
            }

            @Override
            public String read(JsonReader in) throws IOException {
                if (in.peek() == JsonToken.NULL) {
                    in.nextNull();
                    return null;
                }

                try {
                    String stringValue = in.nextString();

                    if ("".equals(stringValue) || "null".equalsIgnoreCase(stringValue)) {
                        return null;
                    }
                    return stringValue;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
    }

    public static Sikap getDataSikap(String key, String jenis_kualifikasi, long rkn_id){
        Sikap dataSikap;

        if(isExistCachedData(key))
        {
            dataSikap = Cache.get(key, Sikap.class);
        }
        else
        {
            dataSikap = new Sikap();
        }

        if(jenis_kualifikasi.equals("izin_usaha") || jenis_kualifikasi.equals("all"))
        {
            if (dataSikap.izin_usaha == null)
            {
                getRekananIzinUsaha(dataSikap, rkn_id);
            }
        }
        else if(jenis_kualifikasi.equals("akta") || jenis_kualifikasi.equals("all"))
        {
            if(dataSikap.akta == null)
            {
                getRekananAkta(dataSikap, rkn_id);
            }
        }
        else if(jenis_kualifikasi.equals("pemilik") || jenis_kualifikasi.equals("all"))
        {
            if(dataSikap.pemilik == null)
            {
                getRekananPemilik(dataSikap, rkn_id);
            }
        }
        else if(jenis_kualifikasi.equals("pengurus") || jenis_kualifikasi.equals("all"))
        {
            if(dataSikap.pengurus == null)
            {
                getRekananPengurus(dataSikap, rkn_id);
            }
        }
        else if(jenis_kualifikasi.equals("tenaga_ahli") || jenis_kualifikasi.equals("all"))
        {
            if(dataSikap.tenaga_ahli == null)
            {
                getRekananTenagaAhli(dataSikap, rkn_id);
            }
        }
        else if(jenis_kualifikasi.equals("pengalaman") || jenis_kualifikasi.equals("all"))
        {
            if(dataSikap.pengalaman == null)
            {
                getRekananPengalaman(dataSikap, rkn_id);
            }
        }
        else if(jenis_kualifikasi.equals("pajak") || jenis_kualifikasi.equals("all"))
        {
            if(dataSikap.pajak == null)
            {
                getRekananPajak(dataSikap, rkn_id);
            }
        }
        else if(jenis_kualifikasi.equals("peralatan") || jenis_kualifikasi.equals("all"))
        {
            if(dataSikap.peralatan == null)
            {
                getRekananPeralatan(dataSikap, rkn_id);
            }
        }

        return dataSikap;
    }

    public static Identitas getRekananIdentitas(long rkn_id){
        String content;

        if(rkn_id != 0){
            try {
                String url = SikapUtils.URL_GET_IDENTITAS;
                String param = Encryption.encryptInaproc(String.valueOf(rkn_id));
                Logger.debug("param : "+param);
                content = WS.url(url+param).timeout("5min").getAsync().get().getString();


                if(!TextUtils.isEmpty(content)){

                    Type collectionType = new TypeToken<Collection<Identitas>>() {
                    }.getType();

                    List<Identitas> identitasList = SikapUtils.getGson().fromJson(content, collectionType);

                    if(!identitasList.isEmpty()){
                        Identitas result = identitasList.get(0);

                        Sikap cacheData = new Sikap();
                        String key = String.valueOf(rkn_id);

                        if(SikapUtils.isExistCachedData(key)){
                            cacheData = Cache.get(key,Sikap.class);
                        }

                        cacheData.identitas = result;
                        Cache.set(key,cacheData);

                        if(result.nm_kec == null){
                            Logger.debug("result "+result.id_kec);
                        }else{
                            Logger.debug("fail :"+result.nm_kec);
                        }

                        return result;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }

        return null;
    }

    public static void getRekananIzinUsaha(Sikap sikap, long rkn_id){
        String content;

        if(rkn_id != 0){
            try {
                String url = SikapUtils.URL_GET_IZINUSAHA;
                String param = Encryption.encryptInaproc(String.valueOf(rkn_id));
                content = WS.url(url+param).timeout("5min").getAsync().get().getString();

                if(!TextUtils.isEmpty(content)){
                    Type collectionType = new TypeToken<Collection<IzinUsaha>>() {
                    }.getType();

                    List<IzinUsaha>  response = new Gson().fromJson(content, collectionType);

                    if(!response.isEmpty()){

                        sikap.izin_usaha = response;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public static void getRekananAkta(Sikap sikap, long rkn_id){
        String content;

        if(rkn_id != 0){
            try {
                String url = SikapUtils.URL_GET_AKTA;
                String param = Encryption.encryptInaproc(String.valueOf(rkn_id));
                content = WS.url(url+param).timeout("5min").getAsync().get().getString();

                if(!TextUtils.isEmpty(content)){

                    Type collectionType = new TypeToken<Collection<Akta>>() {
                    }.getType();

                    List<Akta> response = new Gson().fromJson(content, collectionType);

                    if(!response.isEmpty()){
                        List<Akta> aktaPendirian = new ArrayList<Akta>();
                        List<Akta> aktaPerubahan = new ArrayList<Akta>();

                        for (Akta fromJsonAkta : response){
                            if(fromJsonAkta.jenis_akta.equals(JENIS_AKTA_PENDIRIAN)){
                                Logger.debug("pendirian : "+fromJsonAkta.jenis_akta);
                                aktaPendirian.add(fromJsonAkta);
                            }else if(fromJsonAkta.jenis_akta.equals(JENIS_AKTA_PERUBAHAN)){
                                Logger.debug("perubahan : "+fromJsonAkta.jenis_akta);
                                aktaPerubahan.add(fromJsonAkta);
                            }
                        }

                        Akta akta = new Akta();
                        akta.akta_pendirian = aktaPendirian;
                        akta.akta_perubahan = aktaPerubahan;

                        sikap.akta = akta;

                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public static void getRekananPemilik(Sikap sikap, long rkn_id){
        String content;

        if(rkn_id != 0){
            try {
                String url = SikapUtils.URL_GET_PEMILIK;
                String param = Encryption.encryptInaproc(String.valueOf(rkn_id));
                content = WS.url(url+param).timeout("5min").getAsync().get().getString();

                if(!TextUtils.isEmpty(content)){

                    Type collectionType = new TypeToken<Collection<Pemilik>>() {
                    }.getType();

                    List<Pemilik>  response = new Gson().fromJson(content, collectionType);

                    if(!response.isEmpty()){
                        sikap.pemilik = response;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public static void getRekananPengurus(Sikap sikap, long rkn_id){
        String content;

        if(rkn_id != 0){
            try {
                String url = SikapUtils.URL_GET_PENGURUS;
                String param = Encryption.encryptInaproc(String.valueOf(rkn_id));
                content = WS.url(url+param).timeout("5min").getAsync().get().getString();

                if(!TextUtils.isEmpty(content)){

                    Type collectionType = new TypeToken<Collection<Pengurus>>() {
                    }.getType();

                    List<Pengurus>  response = new Gson().fromJson(content, collectionType);

                    if(!response.isEmpty()){
                        sikap.pengurus = response;

                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public static void getRekananTenagaAhli(Sikap sikap, long rkn_id){
        String content;

        if(rkn_id != 0){
            try {
                String url = SikapUtils.URL_GET_TENAGAAHLI;
                String param = Encryption.encryptInaproc(String.valueOf(rkn_id));
                content = WS.url(url+param).timeout("5min").getAsync().get().getString();

                if(!TextUtils.isEmpty(content)){

                    Type collectionType = new TypeToken<Collection<TenagaAhli>>() {
                    }.getType();

                    List<TenagaAhli>  response = new Gson().fromJson(content, collectionType);

                    if(!response.isEmpty()){
                        sikap.tenaga_ahli = response;

                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public static void getRekananPeralatan(Sikap sikap, long rkn_id){
        String content;

        if(rkn_id != 0){
            try {
                String url = SikapUtils.URL_GET_PERALATAN;
                String param = Encryption.encryptInaproc(String.valueOf(rkn_id));
                content = WS.url(url+param).timeout("5min").getAsync().get().getString();

                if(!TextUtils.isEmpty(content)){

                    Type collectionType = new TypeToken<Collection<Peralatan>>() {
                    }.getType();

                    List<Peralatan>  response = new Gson().fromJson(content, collectionType);

                    if(!response.isEmpty()){
                        sikap.peralatan = response;

                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public static void getRekananPengalaman(Sikap sikap, long rkn_id){
        String content;

        if(rkn_id != 0){
            try {
                String url = SikapUtils.URL_GET_PENGALAMAN;
                String param = Encryption.encryptInaproc(String.valueOf(rkn_id));
                content = WS.url(url+param).timeout("5min").getAsync().get().getString();

                if(!TextUtils.isEmpty(content)){
                    Sikap tempSikap = new Gson().fromJson(content, Sikap.class);

                    if(tempSikap.pengalaman != null && !tempSikap.pengalaman.isEmpty()){
                        sikap.pengalaman = tempSikap.pengalaman;

                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public static void getRekananPajak(Sikap sikap, long rkn_id){
        String content;

        if(rkn_id != 0){
            try {
                String url = SikapUtils.URL_GET_PAJAK;
                String param = Encryption.encryptInaproc(String.valueOf(rkn_id));
                content = WS.url(url+param).timeout("5min").getAsync().get().getString();

                if(!TextUtils.isEmpty(content)){

                    Type collectionType = new TypeToken<Collection<Pajak>>() {
                    }.getType();

                    List<Pajak>  response = new Gson().fromJson(content, collectionType);

                    if(!response.isEmpty()){
                        sikap.pajak = response;


                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public static boolean isExistCachedData(String key){
        Sikap cachedData = Cache.get(key,Sikap.class);

        if(cachedData != null){
            return true;
        }

        return false;
    }

    public static Sikap updateVerifikasiDataSikap(Sikap dataSikap, User user, Long rkn_id, VerifikasiSikap verifikasi,UnVerifikasiSikap unverifikasisikap){

        LogUtil.d(TAG, dataSikap);
        LogUtil.d(TAG, verifikasi);
        
        if(verifikasi.izin_usaha != null){
            for(int i = 0; i < verifikasi.izin_usaha.length; i++){
                for(IzinUsaha dbIu : dataSikap.izin_usaha){
                    if(verifikasi.izin_usaha[i] == dbIu.ius_id){
                        Boolean deleted =  dbIu.is_verified;
                        String alasan = "";
                        if(deleted) {
                            alasan = unverifikasisikap.izin_usaha[i];
                        }
//                        if(Play.mode.isProd()){
                            boolean verified = kirimVerifikasi(DATA_TIPE_IJIN_USAHA,user,rkn_id, dbIu.ius_id, deleted, alasan);
                            if(verified){
                                if(deleted){
                                    dbIu.alasan_pembatalan = alasan;
                                    dbIu.is_verified = false;
                                }else {
                                    dbIu.keterangan_pembuktian = SikapUtils.KETERANGAN_VERIFIKASI_SIKAP;
                                    dbIu.is_verified = true;
                                }
                            }else{
                                String msg = Messages.get("verify.sikap.notif-failed");
                                Scope.Flash.current().error(msg);
                            }
//                        }else{
//                            if(deleted){
//                                dbIu.alasan_pembatalan = alasan;
//                                dbIu.is_verified = false;
//                            }else {
//                                dbIu.keterangan_pembuktian = SikapUtils.KETERANGAN_VERIFIKASI_SIKAP;
//                                dbIu.is_verified = true;
//                            }
//                        }
                    }
                }
            }
        }

        if(verifikasi.akta_pendirian != null){
            for(int i = 0; i < verifikasi.akta_pendirian.length; i++){
                for(Akta dbAkta : dataSikap.akta.akta_pendirian){
                    if(verifikasi.akta_pendirian[i] == dbAkta.lhk_id){
                        Boolean deleted =  dbAkta.is_verified;
                        String alasan = "";
                        if(deleted) {
                            alasan = unverifikasisikap.akta_pendirian[i];
                        }
//                        if(Play.mode.isProd()){
                            boolean verified = kirimVerifikasi(DATA_TIPE_AKTA_PENDIRIAN,user,rkn_id, dbAkta.lhk_id, deleted, alasan);
                            if(verified){
                                if(deleted){
                                    dbAkta.alasan_pembatalan = alasan;
                                    dbAkta.is_verified = false;
                                }else {
                                    dbAkta.keterangan_pembuktian = SikapUtils.KETERANGAN_VERIFIKASI_SIKAP;
                                    dbAkta.is_verified = true;
                                }
                            }else{
                                String msg = Messages.get("verify.sikap.notif-failed");
                                Scope.Flash.current().error(msg);
                            }
//                        }else{
//                            if(deleted){
//                                dbAkta.alasan_pembatalan = alasan;
//                                dbAkta.is_verified = false;
//                            }else {
//                                dbAkta.keterangan_pembuktian = SikapUtils.KETERANGAN_VERIFIKASI_SIKAP;
//                                dbAkta.is_verified = true;
//                            }
//                        }
                    }
                }
            }
        }

        if(verifikasi.akta_perubahan != null){
            for(int i = 0; i < verifikasi.akta_perubahan.length; i++){
                for(Akta dbAkta : dataSikap.akta.akta_perubahan){
                    if(verifikasi.akta_perubahan[i] == dbAkta.lhk_id){
                        Boolean deleted =  dbAkta.is_verified;
                        String alasan = "";
                        if(deleted) {
                            alasan = unverifikasisikap.akta_perubahan[i];
                        }
//                        if(Play.mode.isProd()){
                            boolean verified = kirimVerifikasi(DATA_TIPE_AKTA_PERUBAHAN,user,rkn_id, dbAkta.lhk_id, deleted, alasan);
                            if(verified){
                                if(deleted){
                                    dbAkta.alasan_pembatalan = alasan;
                                    dbAkta.is_verified = false;
                                }else {
                                    dbAkta.keterangan_pembuktian = SikapUtils.KETERANGAN_VERIFIKASI_SIKAP;
                                    dbAkta.is_verified = true;
                                }
                            }else{
                                String msg = Messages.get("verify.sikap.notif-failed");
                                Scope.Flash.current().error(msg);
                            }
//                        }else{
//                            if(deleted){
//                                dbAkta.alasan_pembatalan = alasan;
//                                dbAkta.is_verified = false;
//                            }else {
//                                dbAkta.keterangan_pembuktian = SikapUtils.KETERANGAN_VERIFIKASI_SIKAP;
//                                dbAkta.is_verified = true;
//                            }
//                        }
                    }
                }
            }
        }

        if(verifikasi.pemilik != null){
            for(int i = 0; i < verifikasi.pemilik.length; i++){
                for(Pemilik dbPml : dataSikap.pemilik){
                    if(verifikasi.pemilik[i] == dbPml.pml_id){
                        Boolean deleted =  dbPml.is_verified;
                        String alasan = "";
                        if(deleted) {
                            alasan = unverifikasisikap.pemilik[i];
                        }
//                        if(Play.mode.isProd()){
                            boolean verified = kirimVerifikasi(DATA_TIPE_PEMILIK,user,rkn_id, dbPml.pml_id, deleted, alasan);
                            if(verified){
                                if(deleted){
                                    dbPml.alasan_pembatalan = alasan;
                                    dbPml.is_verified = false;
                                }else {
                                    dbPml.keterangan_pembuktian = SikapUtils.KETERANGAN_VERIFIKASI_SIKAP;
                                    dbPml.is_verified = true;
                                }
                            }else{
                                String msg = Messages.get("verify.sikap.notif-failed");
                                Scope.Flash.current().error(msg);
                            }
//                        }else{
//                            if(deleted){
//                                dbPml.alasan_pembatalan = alasan;
//                                dbPml.is_verified = false;
//                            }else {
//                                dbPml.keterangan_pembuktian = SikapUtils.KETERANGAN_VERIFIKASI_SIKAP;
//                                dbPml.is_verified = true;
//                            }
//                        }
                    }
                }
            }
        }

        if(verifikasi.pengurus != null){
            for(int i = 0; i < verifikasi.pengurus.length; i++){
                for(Pengurus dbPgu : dataSikap.pengurus){
                    if(verifikasi.pengurus[i] == dbPgu.pgr_id){
                        Boolean deleted =  dbPgu.is_verified;
                        String alasan = "";
                        if(deleted) {
                            alasan = unverifikasisikap.pengurus[i];
                        }
//                        if(Play.mode.isProd()){
                            boolean verified = kirimVerifikasi(DATA_TIPE_PENGURUS,user,rkn_id, dbPgu.pgr_id, deleted, alasan);
                            if(verified){
                                if(deleted){
                                    dbPgu.alasan_pembatalan = alasan;
                                    dbPgu.is_verified = false;
                                }else {
                                    dbPgu.keterangan_pembuktian = SikapUtils.KETERANGAN_VERIFIKASI_SIKAP;
                                    dbPgu.is_verified = true;
                                }
                            }else{
                                String msg = Messages.get("verify.sikap.notif-failed");
                                Scope.Flash.current().error(msg);
                            }
//                        }else{
//                            if(deleted){
//                                dbPgu.alasan_pembatalan = alasan;
//                                dbPgu.is_verified = false;
//                            }else {
//                                dbPgu.keterangan_pembuktian = SikapUtils.KETERANGAN_VERIFIKASI_SIKAP;
//                                dbPgu.is_verified = true;
//                            }
//                        }
                    }
                }
            }
        }

        if(verifikasi.tenaga_ahli != null){
            for(int i = 0; i < verifikasi.tenaga_ahli.length; i++){
                for(TenagaAhli dbTa : dataSikap.tenaga_ahli){
                    if(verifikasi.tenaga_ahli[i] == dbTa.sta_id){
                        Boolean deleted =  dbTa.is_verified;
                        String alasan = "";
                        if(deleted) {
                            alasan = unverifikasisikap.tenaga_ahli[i];
                        }
//                        if(Play.mode.isProd()){
                            boolean verified = kirimVerifikasi(DATA_TIPE_TENAGA_AHLI,user,rkn_id, dbTa.sta_id, deleted, alasan);
                            if(verified){
                                if(deleted){
                                    dbTa.alasan_pembatalan = alasan;
                                    dbTa.is_verified = false;
                                }else {
                                    dbTa.keterangan_pembuktian = SikapUtils.KETERANGAN_VERIFIKASI_SIKAP;
                                    dbTa.is_verified = true;
                                }
                            }else{
                                String msg = Messages.get("verify.sikap.notif-failed");
                                Scope.Flash.current().error(msg);
                            }
//                        }else{
//                            if(deleted){
//                                dbTa.alasan_pembatalan = alasan;
//                                dbTa.is_verified = false;
//                            }else {
//                                dbTa.keterangan_pembuktian = SikapUtils.KETERANGAN_VERIFIKASI_SIKAP;
//                                dbTa.is_verified = true;
//                            }
//                        }
                    }
                }
            }
        }

        if(verifikasi.peralatan != null){
            for(int i = 0; i < verifikasi.peralatan.length; i++){
                for(Peralatan dbPlt : dataSikap.peralatan){
                    if(verifikasi.peralatan[i] == dbPlt.alt_id){
                        Boolean deleted =  dbPlt.is_verified;
                        String alasan = "";
                        if(deleted) {
                            alasan = unverifikasisikap.peralatan[i];
                        }
//                        if(Play.mode.isProd()){
                            boolean verified = kirimVerifikasi(DATA_TIPE_PERALATAN,user,rkn_id, dbPlt.alt_id, deleted, alasan);
                            if(verified){
                                if(deleted){
                                    dbPlt.alasan_pembatalan = alasan;
                                    dbPlt.is_verified = false;
                                }else {
                                    dbPlt.keterangan_pembuktian = SikapUtils.KETERANGAN_VERIFIKASI_SIKAP;
                                    dbPlt.is_verified = true;
                                }
                            }else{
                                String msg = Messages.get("verify.sikap.notif-failed");
                                Scope.Flash.current().error(msg);
                            }
//                        }else {
//                            if(deleted){
//                                dbPlt.alasan_pembatalan = alasan;
//                                dbPlt.is_verified = false;
//                            }else {
//                                dbPlt.keterangan_pembuktian = SikapUtils.KETERANGAN_VERIFIKASI_SIKAP;
//                                dbPlt.is_verified = true;
//                            }
//                        }
                    }
                }
            }
        }

        if(verifikasi.pengalaman != null){
            for(int i = 0; i < verifikasi.pengalaman.length; i++){
                for(Pengalaman dbPgl : dataSikap.pengalaman){
                    if(verifikasi.pengalaman[i] == dbPgl.pgl_id){
                        Boolean deleted =  dbPgl.is_verified;
                        String alasan = "";
                        if(deleted) {
                            alasan = unverifikasisikap.pengalaman[i];
                        }
//                        if(Play.mode.isProd()){
                            boolean verified = kirimVerifikasi(DATA_TIPE_PENGALAMAN,user,rkn_id, dbPgl.pgl_id, deleted, alasan);
                            if(verified){
                                if(deleted){
                                    dbPgl.alasan_pembatalan = alasan;
                                    dbPgl.is_verified = false;
                                }else {
                                    dbPgl.keterangan_pembuktian = SikapUtils.KETERANGAN_VERIFIKASI_SIKAP;
                                    dbPgl.is_verified = true;
                                }
                            }else{
                                String msg = Messages.get("verify.sikap.notif-failed");
                                Scope.Flash.current().error(msg);
                            }
//                        }else{
//                            if(deleted){
//                                dbPgl.alasan_pembatalan = alasan;
//                                dbPgl.is_verified = false;
//                            }else {
//                                dbPgl.keterangan_pembuktian = SikapUtils.KETERANGAN_VERIFIKASI_SIKAP;
//                                dbPgl.is_verified = true;
//                            }
//                        }
                    }
                }
            }
        }

        if(verifikasi.pajak != null){
            for(int i = 0; i < verifikasi.pajak.length; i++){
                for(Pajak dbPjk : dataSikap.pajak){
                    if(verifikasi.pajak[i] == dbPjk.pjk_id){
                        //jika datanya sudah di verifikasi(true) maka akan deleted true
                        Boolean deleted =  dbPjk.is_verified;
                        String alasan = "";
                        if(deleted) {
                            alasan = unverifikasisikap.pajak[i];
                        }
//                        if(Play.mode.isProd()){
                            boolean verified = kirimVerifikasi(DATA_TIPE_PAJAK,user,rkn_id, dbPjk.pjk_id, deleted, alasan);
                            if(verified){
                                if(deleted){
                                    dbPjk.alasan_pembatalan = alasan;
                                    dbPjk.is_verified = false;
                                }else {
                                    dbPjk.keterangan_pembuktian = SikapUtils.KETERANGAN_VERIFIKASI_SIKAP;
                                    dbPjk.is_verified = true;
                                }
                            }else{
                                String msg = Messages.get("verify.sikap.notif-failed");
                                Scope.Flash.current().error(msg);
                            }
//                        }else {
//                            if(deleted){
//                                dbPjk.alasan_pembatalan = alasan;
//                                dbPjk.is_verified = false;
//                            }else {
//                                dbPjk.keterangan_pembuktian = SikapUtils.KETERANGAN_VERIFIKASI_SIKAP;
//                                dbPjk.is_verified = true;
//                            }
//                        }
                    }
                }
            }
        }

        return dataSikap;
    }

    private static boolean kirimVerifikasi(Integer dataTipe, User user, Long rkn_id, Long data_id, boolean deleted,String alasanbatal){
        //String peg_nama = user.nama_lengkap;
        //String peg_nip = user.nip;
        //String peg_namauser = user.user_name;
        String peg_role = "POKJA";
        //Long repo_id = user.pps_id;
        if(alasanbatal==null){
            alasanbatal="";
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("peg_nama",user.nama_lengkap);
        params.put("peg_nip",user.nip);
        params.put("peg_namauser",user.user_name);
        params.put("peg_role",peg_role);
        params.put("repo_id",REPO_ID);
        params.put("data_tipe",dataTipe);
        params.put("rkn_id",rkn_id);
        params.put("data_id",data_id);//
        params.put("deleted",deleted);
        params.put("alasan_pembatalan", alasanbatal);//
        params.put("keterangan_pembuktian",KETERANGAN_VERIFIKASI_SIKAP);

        String json = new Gson().toJson(params);
        //Logger.info("json : "+json);
        String url = URL_SEND_VERIFIKASI;
        Logger.info(json);
        String param = Encryption.encryptInaproc(json);
        Logger.debug("param : "+param);

        String content;
        try{
            Logger.info(url+param);
            content = WS.url(url+param).timeout("5min").postAsync().get().getString();
            if(!TextUtils.isEmpty(content)){
                Logger.debug("kirimVerifikasi() - "+content);
                JsonObject response = new Gson().fromJson(content,JsonObject.class);
                JsonElement element = response.get("msg");
                String msg = element.getAsString();

                Logger.debug(msg);
                if(msg.equals("Success")){
                    return true;
                }else{
                    Logger.info(" error verifikasi sikap:"+content);
                    return false;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

        return false;


    }



}
