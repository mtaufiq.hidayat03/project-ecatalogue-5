package models.elasticsearch.product;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author HanusaCloud on 9/29/2017
 */
public class ProductCategoryElastic implements Serializable {

    @SerializedName("id")
    public long id;
    @SerializedName("name")
    public String name;
    @SerializedName("current")
    public String current;

    public ProductCategoryElastic(long id, String name) {
        this.id = id;
        this.name = name;
    }

}
