# eCatalogue / ePurchasing

* Deployment: e-katalog.lkpp.go.id
* PIC: A. Priyo Utomo
* Database: MySQL (core e-katalog) & PostgreSQL (logging, chat, non-core)
* Lang: Java (Play-1.4.x-lkpp)

## Manifesto
1. Website harus cepat
2. Website harus aman
3. Website harus mobile friendly
4. Website harus menarik dan informatif
5. Website mendukung dua bahasa (Indonesia dan English)
6. Untuk setiap URL yang bisa diakses minimal ada satu script test
7. Readme.md harus selalu diupdate dan menjadi acuan semua developer

## Kebutuhan Server

### Wajib
* Java >= 8.x.x
* Composer (opsional)
* Elasticsearch (dipertimbangkan)

### Opsional
* Memcached (https://memcached.org) untuk caching

## Kebutuhan SDM

### Backend Developer
* Menguasai Java dan OOP
* Menguasai dasar-dasar framework Play
* Memahami konsep API

### Frontend Developer
* Pernah memakai Semantic-UI atau Bootstrap CSS
* Pernah menggunakan SCSS atau LESS
* Memahami less (http://lesscss.org) atau sass (http://sass-lang.com)
* Mengikuti trend desain web terbaru

## Coding Convention
* Action yang menerima submit dari sebuah FORM selalu diakhiri _submit()_
    
    #{form @admin.GrupRoleCtr.createSubmit(), method:'POST', class:'form-horizontal'} 
* Logger sebaiknya diawali dengan nama class-nya. Pada contoh di bawah ini: _[SirupApiJob]_

    Logger.debug("[SirupApiJob] Downloading Sirup, URL %s: ", url);

## Model
* Model silakan extends dari ``models.BaseKatalogTable`` untuk yang memiliki kesamaan field.

## Jobs
* Model extends dari ``jobs.TrackableJob`` supaya bisa dimonitor.
* Untuk job yang bisa berdurasi panjang seperti penarikan data dari Sirup, proses _save()_ harus dilakukan di bagian akhir job.
  Ini untuk mencegah deadlock koneksi DB. Lihat ``jobs.SirupApiJob`` sebagai contoh.
  
## ElasticSearch
### Mapping
* Package App/jobs/elasticsearch/ElasticsearchIndexing.java
* Run it for the first time ``<ekatalog instance> elasticmapping``.
### Indexing
* Package App/jobs/elasticsearch/ElasticsearchLog.java
* Run it using cron ``<ekatalog instance> stash`` everyday at night.

### Access of ElasticSearch URL 
* ES URL: http://192.168.11.15:9201

## Persiapan Database

CREATE TABLE mail_queue (
  id int(11) NOT NULL AUTO_INCREMENT,
  exception text,
  to_addresses varchar(1000) DEFAULT NULL,
  cc_addresses varchar(1000) DEFAULT NULL,
  bcc_addresses varchar(1000) DEFAULT NULL,
  subject varchar(1000) DEFAULT NULL,
  from_address varchar(255) DEFAULT NULL,
  body text,
  mime decimal(1,0) DEFAULT '0',
  retry decimal(2,0) DEFAULT NULL,
  enqueue_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  send_date datetime DEFAULT NULL,
  status smallint(6) DEFAULT '0',
  noemail text,
  lls_id decimal(19,0) NOT NULL DEFAULT '0',
  jenis int(11) NOT NULL DEFAULT '0',
  rkn_id decimal(19,0) DEFAULT '0',
  bcstat decimal(1,0) NOT NULL DEFAULT '1',
  prioritas int(11) DEFAULT '2',
  audittype varchar(1) DEFAULT NULL,
  auditupdate datetime DEFAULT NULL,
  audituser varchar(100) DEFAULT NULL,
  engine_version varchar(50) DEFAULT NULL,
  ref_1 bigint(20) DEFAULT NULL,
  ref_2 bigint(20) DEFAULT NULL,
  ref_3 bigint(20) DEFAULT NULL,
  ref_a varchar(100) DEFAULT NULL,
  ref_b varchar(100) DEFAULT NULL,
  ref_c varchar(100) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE configuration (
  cfg_category varchar(80) NOT NULL,
  cfg_sub_category varchar(80) NOT NULL,
  audittype char(1) NOT NULL DEFAULT 'C',
  audituser varchar(100) NOT NULL DEFAULT 'ADMIN',
  auditupdate datetime DEFAULT CURRENT_TIMESTAMP,
  cfg_value varchar(2000) DEFAULT NULL,
  cfg_comment varchar(255) DEFAULT NULL,
  PRIMARY KEY (cfg_category,cfg_sub_category)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
insert into configuration(cfg_category,cfg_sub_category, cfg_value) values('CONFIG','file.storage-dir','D:\temp');

ALTER TABLE `ekatalog4_1`.`user` ADD COLUMN `centrum_username` VARCHAR(45) NULL ;
ALTER TABLE `ekatalog4_1`.`user` ADD COLUMN `centrum_user_id` int NULL ;

## Credits
* Coding Reference LKPP: https://gitlab.lkpp.go.id/docs/perpustakaan

## Setup aplikasi 

1. Clone semua git, dan masukkan ke dalam satu folder, misalnya project-ekatalog:
    1.  Katalog play = ssh://git@gitlab.javan.co.id:2229/e-katalog/katalog-play.git dan pastikan dalam branch netty4katalog5
    2.  Local repository = ssh://git@gitlab.javan.co.id:2229/e-katalog/local-repository.git
    3.  jcommon = ssh://git@gitlab.javan.co.id:2229/e-katalog/jcommon.git dan pastikan berada dalam branch netty4katalog5
    4.  katalog5 = ssh://git@gitlab.javan.co.id:2229/e-katalog/katalog-5.git


2. Ubah application.conf pada folder katalog-5 sesuaikan dengan konfigurasi ke databasenya. Contoh konfigurasi database ke cloud javan :
    1.  db.default.url=jdbc:mysql://10.10.10.204:3306/katalog5?zeroDateTimeBehavior=convertToNull&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Asia/Jakarta&allowPublicKeyRetrieval=true
    2.  db.default.user=root
    3.  db.default.pass=asdf1234


3. Melalui terminal jalankan perintah : 
    1.  cd project-ekatalog/katalog-5/ -> untuk masuk ke dalam folder katalog-5, kemudian jalankan 
    2.  ../unmodified-play/play deps --clearcache -> untuk mendownload dependecies dan menghapus cache kemudian jalankan perintah 
    3.  ../unmodified-play/play run untuk menjalankan aplikasi

## Database Connection
1. Aktifkan vpn lkpp : epic-lkpp2
2. Buka database tool management : misalnya navicat, datagrip atau dbweaver
3. Buat koneksi baru, setting username: adminkatalog, host: 192.168.11.15, port: 3306
4. Tes koneksi terlebih dahulu, kemudian klik OK
5. Note: akan ada error time pada datagrip, cara memperbaiki pada bagian connection pilih Advanced -> cari name serverTimeZone kemudian set value dengan UTC

