package controllers.katalog;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import controllers.BaseController;
import controllers.katalog.form.ProductForm;
import controllers.prakatalog.PenawaranCtr;
import controllers.security.AllowAccess;
import ext.FormatUtils;
import jobs.JobTrail.JobUpdateViewCounter;
import jobs.elasticsearch.ElasticsearchLog;
import models.ServiceResult;
import models.chat.Chat;
import models.cms.KategoriKonten;
import models.common.AktifUser;
import models.common.DokumenInfo;
import models.common.HargaProdukDaerah;
import models.common.OngkosKirim;
import models.elasticsearch.SearchQuery;
import models.elasticsearch.SearchResult;
import models.jcommon.blob.BlobTable;
import models.jcommon.util.CommonUtil;
import models.katalog.*;
import models.katalog.komoditasmodel.KomoditasAtributHarga;
import models.masterdata.*;
import models.penyedia.Penyedia;
import models.prakatalog.Penawaran;
import models.prakatalog.Usulan;
import models.prakatalog.UsulanKategoriProduk;
import models.product.search.ProductSearch;
import models.product.search.ProductSearchResult;
import models.secman.Acl;
import models.user.User;
import models.util.produk.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.util.TextUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.data.validation.Required;
import play.db.jdbc.Query;
import play.libs.Codec;
import play.libs.Files;
import play.libs.Images;
import repositories.WilayahRepository;
import repositories.elasticsearch.ElasticsearchRepository;
import repositories.penyedia.ProviderRepository;
import repositories.produk.ProductRepository;
import repositories.produkkategori.ProdukKategoriRepository;
import services.prakatalog.UsulanService;
import services.produk.ProdukService;
import utils.DateTimeUtils;
import utils.KatalogUtils;
import utils.LogUtil;

import java.io.*;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static models.secman.Acl.COM_PRODUK_BUKA_EDIT_FITUR;

public class ProdukCtr extends BaseController {

	public static final String TAG = "ProdukCtr";
	//public static Map<Long, models.product.Produk> modelTayangListDefault = new HashMap<>();

	@AllowAccess({Acl.COM_PRODUK})
	public static void index() {
		ProductSearch query = new ProductSearch(params);

		List<Komoditas> komoditas = new ArrayList<>();
		List<Penyedia> penyedia = new ArrayList<>();
		boolean aksesBtnEditDapatDibeli = false;
		boolean aksesBtnEditDapatDiTayangkan = false;
		boolean aksesBtnEditMintaPersetujuan = false;
		boolean aksesBtnBukaEditProduk = false;

		if(getAktifUser().isProvider()){
			try{
				komoditas = Komoditas.findByPenyedia(Penyedia.getProviderByUserId(AktifUser.getActiveUserId().longValue()).id);
				penyedia = Penyedia.getPenyedia(Penyedia.getProviderByUserId(AktifUser.getActiveUserId().longValue()).id);
				query.penyedia = Penyedia.getProviderByUserId(AktifUser.getActiveUserId().longValue()).id.toString();
				aksesBtnEditMintaPersetujuan = true;
			}catch (Exception e){}
		}else{
			komoditas = Komoditas.getForDropDown();

			if(query.komoditas != 0){
				penyedia = Penyedia.getPenyediaKomoditas(query.komoditas);
			}

			aksesBtnEditDapatDibeli = getAktifUser().isAuthorized("comProdukEditDapatDibeli");
			aksesBtnEditDapatDiTayangkan = getAktifUser().isAuthorized("comProdukEditDapatDitayangkan");
			aksesBtnBukaEditProduk = getAktifUser().isAuthorized("comProdukBukaEditFitur");
		}

		ProductSearchResult model = new ProductSearchResult(query);
		//ProductSearchResult modelTayang = new ProductSearchResult(query);
		if(params.all().size() > 1){
			model = ProductRepository.searchProduct(query);
			//modelTayang = ProductRepository.searchProductTayang(query);
		}

//		Map<Long, models.product.Produk> modelTayangList = new HashMap<>();
//
//		if (modelTayang != null){
//			for (models.product.Produk psr: modelTayang.items){
//				modelTayangList.put(psr.id,psr);
//			}
//		}
		final Boolean allowSetUkm = getAktifUser().isAdmin();
		renderArgs.put("paramQuery", query);
		render(
				"katalog/ProdukCtr/index.html", model, komoditas,
				penyedia, aksesBtnEditDapatDibeli, aksesBtnEditDapatDiTayangkan,
				aksesBtnEditMintaPersetujuan, aksesBtnBukaEditProduk, allowSetUkm);
	}

    @AllowAccess({Acl.COM_PRODUK_EDIT_DAPAT_DIBELI})
    public static void persetujuanProdukDibeliSubmit(String alasan, long[] idProduk, boolean yaTidak, String type){
        //TODO : Pengecekan yang berwenang
       AktifUser aktifUser = getAktifUser();
        if (type.equals("dapat_ditayangkan")){
            for(long id : idProduk){
                Produk produk = Produk.findById(id);
                produk.apakah_ditayangkan = yaTidak;
                produk.save();

                ProdukRiwayat produkRiwayat = new ProdukRiwayat();
                produkRiwayat.produk_id = id;
                produkRiwayat.aksi = "produk_riwayat_update_apakah_dapat_ditayangkan";
                produkRiwayat.deskripsi = alasan;
                produkRiwayat.created_by = AktifUser.getActiveUserId();
                produkRiwayat.created_date = new Timestamp(new java.util.Date().getTime());
                produkRiwayat.save();

                /* create job elastic dan, crawl data dan update ke elastic
                 * **/
                ElasticsearchLog obj = new ElasticsearchLog();
                if (yaTidak){
                    obj.createJobUpdateElastic(id);
                }else{
                    obj.deleteProdukbyId(id);
                }
				try{

					flash.success("Berhasil eksekusi tayang/turun produk");
					//hapus cache list produk by komoditas, firs request true
					final String keyKomo = "elasticsFirsByKomoditasid";
					Cache.delete(keyKomo+produk.komoditas_id);
				}catch (Exception e){
					LogUtil.debug(TAG, e);
				}
            }
        }else {
            for(long id : idProduk){
                Produk produk = Produk.findById(id);
                produk.apakah_dapat_dibeli = yaTidak;
                produk.save();

                ProdukRiwayat produkRiwayat = new ProdukRiwayat();
                produkRiwayat.produk_id = id;
                produkRiwayat.aksi = "produk_riwayat_update_apakah_dapat_dibeli";
                produkRiwayat.deskripsi = alasan;
                produkRiwayat.created_by = AktifUser.getActiveUserId();
                produkRiwayat.created_date = new Timestamp(new java.util.Date().getTime());
                produkRiwayat.save();
            }
        }
        ProdukCtr.index();
    }

	@AllowAccess({Acl.COM_PRODUK_MINTA_PERSUTUJUAN_TAYANG})
	public static void mintaPersetujuanTayang(String alasan, long[] idProduk, boolean yaTidakMintaPersetujuan, String type){
		AktifUser aktifUser = getAktifUser();
		if (type.equals("minta_persetujuan")) {
			for (long id : idProduk){
				Produk produk = Produk.findById(id);
				produk.minta_disetujui = true;
				produk.minta_disetujui_oleh = aktifUser.user_id;
				produk.minta_disetujui_alasan = alasan;
				produk.minta_disetujui_tanggal = new Timestamp(new java.util.Date().getTime());
				produk.apakah_ditayangkan = false;
				produk.apakah_dapat_dibeli = false;
				produk.setuju_tolak = null;
                produk.apakah_dapat_diedit = 1;
				produk.status = Produk.STATUS_MENUNGGU_PERSETUJUAN;
				produk.save();

				ProdukRiwayat produkRiwayat = new ProdukRiwayat();
				produkRiwayat.produk_id = id;
				produkRiwayat.aksi = "produk_riwayat_meminta_persetujuan_tayang";
				produkRiwayat.deskripsi = alasan;
				produkRiwayat.created_by = AktifUser.getActiveUserId();
				produkRiwayat.created_date = new Timestamp(new java.util.Date().getTime());
				produkRiwayat.save();

				if (ProdukTungguSetuju.countByProdukId(id) == 0) {
					new ProdukTungguSetuju().setApproval(id).save();
				}
				Logger.info("count by " +ProdukTungguSetuju.countByProdukId(produk.id));
			}
		}
		JsonObject result = new JsonObject();
		result.addProperty("message","Berhasil, Silahkan tunggu");
		result.addProperty("isSuccess",true);

		renderJSON(result);
	}

	@AllowAccess(COM_PRODUK_BUKA_EDIT_FITUR)
	public static void openByAdmin(String alasan, long[] idProduk, int bukaTutup){
		Logger.info("buka " + bukaTutup);
		for (long id : idProduk) {
			Produk produk = Produk.findById(id);
			produk.apakah_dapat_diedit = bukaTutup;
			produk.save();

			ProdukRiwayat produkRiwayat = new ProdukRiwayat();
			produkRiwayat.produk_id = id;
			if(bukaTutup == 1){
				produkRiwayat.aksi = "produk_riwayat_buka_tombol_edit";
			}
			produkRiwayat.deskripsi = alasan;
			produkRiwayat.created_by = AktifUser.getActiveUserId();
			produkRiwayat.created_date = new Timestamp(new java.util.Date().getTime());
			produkRiwayat.save();
		}

		JsonObject result = new JsonObject();
		result.addProperty("message","Berhasil, Silahkan tunggu");
		result.addProperty("isSuccess",true);

		renderJSON(result);
	}

	@AllowAccess({Acl.COM_PRODUK_ADD})
	public static void create(long pid) {
		setArgs(true, pid);
		renderArgs.put("action", "create");
		render("katalog/ProdukCtr/create.html");
	}

	@AllowAccess({Acl.COM_PRODUK_EDIT})
	public static void edit(long produk_id) {
		Komoditas komoditas = setArgs(false, produk_id);
		List<ProdukAtributValue> produkAtributValues = ProdukAtributValue.findByProdukId(produk_id);
		Map<String, String> attributeMap = produkAtributValues.stream()
				.collect(Collectors.toMap(ProdukAtributValue::getCategoriAttrId, a -> a.atribut_value));
		Long[] wilJualId = new Long[0];
		List<Long> wilJualIds = new ArrayList<>();
		if (komoditas.isRegency()) {
			List<ProdukWilayahJualKabupaten> wilJual = ProdukWilayahJualKabupaten.find("produk_id = ?", produk_id).fetch();
			wilJualIds = new ArrayList<>();
			for (ProdukWilayahJualKabupaten p : wilJual) {
				wilJualIds.add(p.id);
			}
			wilJualId = wilJualIds.toArray(new Long[wilJualIds.size()]);
		} else if (komoditas.isProvince()) {
			List<ProdukWilayahJualProvinsi> wilJual = ProdukWilayahJualProvinsi.find("produk_id = ?", produk_id).fetch();
			wilJualIds = new ArrayList<>();
			for (ProdukWilayahJualProvinsi p : wilJual) {
				wilJualIds.add(p.id);
			}
			wilJualId = wilJualIds.toArray(new Long[wilJualIds.size()]);
		}

		List<ProdukHarga> listProdukHarga = ProdukHarga.findRiwayatByProdukId(produk_id);
		Map<String, List<ProdukHarga>> groupHarga = listProdukHarga.stream()
		.collect(
			Collectors.groupingBy(
				x -> x.getStandarDateString()
			)
		);
		NavigableMap<String, List<ProdukHarga>> riwayatProdukHarga = new TreeMap<>(groupHarga).descendingMap();
		Map<Long, List<RiwayatHargaProdukJson>> groupRiwayat = komoditas.isNational() ? null : listProdukHarga.stream()
				.map(x -> x.riwayatHarga())
				.flatMap(List::stream)
				.collect(
						Collectors.groupingBy(
								x -> komoditas.isProvince() ? x.provinsiId : x.kabupatenId
								)
						);
		NavigableMap<Long, List<RiwayatHargaProdukJson>> riwayatProdukHargaPerLokasi = groupRiwayat==null ? null : new TreeMap<>(groupRiwayat).descendingMap();

		// todo:
		/// nanti cek ulang, mekanisme edit harganya pegimana.?

		ProdukHarga produkHarga = ProdukHarga.getByProductId(produk_id);
		String checkedLocationsJson = "{}";
		LogUtil.debug(TAG, komoditas);
		if(!komoditas.isNational()){
			LogUtil.debug(TAG, "it's national");
			checkedLocationsJson = produkHarga.checked_locations_json;
		}
		LinkedTreeMap checkedLocationMap = (new Gson()).fromJson(checkedLocationsJson, LinkedTreeMap.class);
		LogUtil.debug(TAG, checkedLocationsJson);
		List<String> checkedLocationList = new ArrayList<String>();
		Map<Long, String> checkedLocations = new HashMap<Long, String>();
		if(komoditas.isRegency() && checkedLocationMap != null) {
			LogUtil.debug(TAG, "regency");
			checkedLocationList = new ArrayList<String>(((Map)(checkedLocationMap.get("regency"))).keySet());
			List<Kabupaten> kabupatenList = Kabupaten.findByIds(checkedLocationList);
			checkedLocations = kabupatenList.stream().collect(Collectors.toMap(Kabupaten::getId, Kabupaten::getNama));
		}
		else if(komoditas.isProvince() && checkedLocationMap != null) {
			LogUtil.debug(TAG, "province");
			checkedLocationList = new ArrayList<String>(((Map)(checkedLocationMap.get("province"))).keySet());
			List<Provinsi> provinsiList = Provinsi.findByIds(checkedLocationList);
			checkedLocations = provinsiList.stream().collect(Collectors.toMap(Provinsi::getId, Provinsi::getNama));
		}

		renderArgs.put("checkedLocations", checkedLocations);
		renderArgs.put("riwayatProdukHarga", riwayatProdukHarga);
		renderArgs.put("riwayatProdukHargaPerLokasi", riwayatProdukHargaPerLokasi);
		renderArgs.put("produkHarga", produkHarga);
		renderArgs.put("listProdukHarga", listProdukHarga);
		renderArgs.put("action", "update");
		renderArgs.put("attrMapJson", CommonUtil.toJson(attributeMap));
		render("katalog/ProdukCtr/create.html", produkAtributValues, wilJualId, wilJualIds);
	}

    @AllowAccess({Acl.COM_PRODUK_EDIT})
    public static void editProduk(long produk_id) {
        Komoditas komoditas = setArgs(false, produk_id);
        List<ProdukAtributValue> produkAtributValues = ProdukAtributValue.findByProdukId(produk_id);
        Map<String, String> attributeMap = produkAtributValues.stream()
                .collect(Collectors.toMap(ProdukAtributValue::getCategoriAttrId, a -> a.atribut_value));
        Long[] wilJualId = new Long[0];
        List<Long> wilJualIds = new ArrayList<>();
        if (komoditas.isRegency()) {
            List<ProdukWilayahJualKabupaten> wilJual = ProdukWilayahJualKabupaten.find("produk_id = ?", produk_id).fetch();
            wilJualIds = new ArrayList<>();
            for (ProdukWilayahJualKabupaten p : wilJual) {
                wilJualIds.add(p.id);
            }
            wilJualId = wilJualIds.toArray(new Long[wilJualIds.size()]);
        } else if (komoditas.isProvince()) {
            List<ProdukWilayahJualProvinsi> wilJual = ProdukWilayahJualProvinsi.find("produk_id = ?", produk_id).fetch();
            wilJualIds = new ArrayList<>();
            for (ProdukWilayahJualProvinsi p : wilJual) {
                wilJualIds.add(p.id);
            }
            wilJualId = wilJualIds.toArray(new Long[wilJualIds.size()]);
        }

        List<ProdukHarga> listProdukHarga = ProdukHarga.findRiwayatByProdukId(produk_id);
        Map<String, List<ProdukHarga>> groupHarga = listProdukHarga.stream()
                .collect(
                        Collectors.groupingBy(
                                x -> x.getStandarDateString()
                        )
                );
        NavigableMap<String, List<ProdukHarga>> riwayatProdukHarga = new TreeMap<>(groupHarga).descendingMap();
        Map<Long, List<RiwayatHargaProdukJson>> groupRiwayat = komoditas.isNational() ? null : listProdukHarga.stream()
                .map(x -> x.riwayatHarga())
                .flatMap(List::stream)
                .collect(
                        Collectors.groupingBy(
                                x -> komoditas.isProvince() ? x.provinsiId : x.kabupatenId
                        )
                );
        NavigableMap<Long, List<RiwayatHargaProdukJson>> riwayatProdukHargaPerLokasi = groupRiwayat==null ? null : new TreeMap<>(groupRiwayat).descendingMap();

        // todo:
        /// nanti cek ulang, mekanisme edit harganya pegimana.?

        ProdukHarga produkHarga = ProdukHarga.getByProductId(produk_id);
        String checkedLocationsJson = "{}";
        if(!komoditas.isNational()){
            checkedLocationsJson = produkHarga.checked_locations_json;
        }
        LinkedTreeMap checkedLocationMap = (new Gson()).fromJson(checkedLocationsJson, LinkedTreeMap.class);
        List<String> checkedLocationList = new ArrayList<String>();
        Map<Long, String> checkedLocations = new HashMap<Long, String>();
        if(komoditas.isRegency()) {
            checkedLocationList = new ArrayList<String>(((Map)(checkedLocationMap.get("regency"))).keySet());
            List<Kabupaten> kabupatenList = Kabupaten.findByIds(checkedLocationList);
            checkedLocations = kabupatenList.stream().collect(Collectors.toMap(Kabupaten::getId, Kabupaten::getNama));
        }
        else if(komoditas.isProvince()) {
            checkedLocationList = new ArrayList<String>(((Map)(checkedLocationMap.get("province"))).keySet());
            List<Provinsi> provinsiList = Provinsi.findByIds(checkedLocationList);
            checkedLocations = provinsiList.stream().collect(Collectors.toMap(Provinsi::getId, Provinsi::getNama));
        }

        renderArgs.put("checkedLocations", checkedLocations);
        renderArgs.put("riwayatProdukHarga", riwayatProdukHarga);
        renderArgs.put("riwayatProdukHargaPerLokasi", riwayatProdukHargaPerLokasi);
        renderArgs.put("produkHarga", produkHarga);
        renderArgs.put("listProdukHarga", listProdukHarga);
        renderArgs.put("action", "update");
        renderArgs.put("attrMapJson", CommonUtil.toJson(attributeMap));
        render("katalog/ProdukCtr/editByProduct.html", produkAtributValues, wilJualId, wilJualIds);
    }

	private static Komoditas setArgs(boolean isCreated, long id) {
		//di public static void create(long pid) { penawaran_id
		AktifUser aktifUser = getAktifUser();
		User user = User.findById(aktifUser.user_id);

		Penawaran penawaran;
		Usulan usulan = null;
		Produk produk;
		Komoditas komoditas;

		if (isCreated) {
			produk = new Produk();
			penawaran = Penawaran.findById(id);
			notFoundIfNull(penawaran);
			allowToAddProduct(penawaran);
			usulan = Usulan.findById(penawaran.usulan_id);
			komoditas = Komoditas.findById(penawaran.komoditas_id);
		} else {
			produk = Produk.findById(id);
			notFoundIfNull(produk);
			produk.withPictures();
			produk.withDocuments(false);
			komoditas = Komoditas.findById(produk.komoditas_id);
			if(komoditas.isNational())
				produk.withPrice();
			if (!produk.allowToUpdate(getAktifUser().user_id)) {
				notFound();
			}
			
			penawaran = Penawaran.findById(produk.penawaran_id);
			if (penawaran != null && !produk.isEditAble()) {
//				allowToAddProduct(penawaran);
				allowToProduct(penawaran);
			}
			if (penawaran != null) {
				usulan = Usulan.findById(penawaran.usulan_id);
			}
		}

		List<UnitPengukuran> unitPengukuranList = UnitPengukuran.findAllActive();
		List<Manufaktur> manufakturList = Manufaktur.findManufakturByKomoditas(komoditas.id);

		List<ProdukKategori> produkKategoriList = ProdukKategoriRepository.getProdukKategoriList(komoditas.id);

		List<UsulanKategoriProduk> usulanKategoriProdukList = null;
		if (usulan != null) {
			usulanKategoriProdukList = UsulanKategoriProduk.findByUsulan(usulan.id);
		}
//		List<AllLevelProdukKategori> kategoriList = ProdukKategoriService
//				.generateSingleRowKategoriByUsulan(produkKategoriList, usulanKategoriProdukList);
		List<ProdukKategori> kategoriList = ProdukKategoriRepository.getProdukKategoriList(komoditas.id);

		List<Kurs> kursList = Kurs.findAll();
		Kurs kur = kursList.stream().filter(i->"IDR".equals(i.nama_kurs)).findAny().orElse(null);
		List<String> jenisProduk = new ArrayList<>(Arrays.asList(Produk.TYPE_LOCAL, Produk.TYPE_IMPORT));
		LogUtil.d(TAG, "unlimited: " + komoditas.apakah_stok_unlimited);

		List<String> atributHargaNamaList = KomoditasAtributHarga.findNamaNotOngkirByKomoditas(komoditas.id);
		if(komoditas.isRegency() && komoditas.perlu_ongkir){
			// atributHargaNamaList.add(KomoditasAtributHarga.findNamaOngkirByKomoditas(komoditas.id));
		}

		String atributHargaJson = new Gson().toJson(atributHargaNamaList);
		List<KomoditasAtributHarga> atributHargaList = KomoditasAtributHarga.findHargaNonOngkirByKomoditas(komoditas.id);

		//edit purpose
		if(produk != null){
			produk.jumlah_stok_form = produk.jumlah_stok.intValue();
			produk.jumlah_stok_inden_form = produk.jumlah_stok_inden.intValue();
		}

		renderArgs.put("user", user);
		renderArgs.put("komoditas", komoditas);
		renderArgs.put("unitPengukuranList", unitPengukuranList);
		renderArgs.put("manufakturList", manufakturList);
		renderArgs.put("kategoriList", kategoriList);
		renderArgs.put("kursList", kursList);
		renderArgs.put("kur", kur);
		renderArgs.put("jenisProduk", jenisProduk);
		if (penawaran != null) {
			renderArgs.put("pid", penawaran.id);
		}

		renderArgs.put("produk", produk);
		renderArgs.put("atributHargaJson", atributHargaJson);
		renderArgs.put("atributHargaList", atributHargaList);
		renderArgs.put("provKab", WilayahRepository.instance.getAllWilayah());
		return komoditas;
	}

	private static void allowToAddProduct(Penawaran penawaran) {
		if (!penawaran.allowToAddProduct()) {
			notFound();
		}
	}

	private static void allowToProduct(Penawaran penawaran) {
		if (!penawaran.allowToProduct()) {
			notFound();
		}
	}

	//@AllowAccess({Acl.COM_PRODUK_ADD})
	public static void validasiProduk(){
		String nama =params.get("nama") != null ? params.get("nama", String.class) : "";
		String noprodukp =params.get("noprodukp") != null ? params.get("noprodukp", String.class) : "";
		String pids = params.get("pid") != null ? params.get("pid", String.class) : "";
		Long ids = params.get("id") != null ? params.get("id", Long.class) : null;

		Map<String, Object> result = new HashMap<>(1);
		Produk temp = new Produk();
		Long penawaranIds = Long.valueOf(pids);

		temp.id = ids;
		temp.nama_produk = nama;
		//Logger.debug("namaproduk: "+nama);
		temp.no_produk_penyedia = noprodukp;
		Boolean isOk = false;
		if(nama.isEmpty()){
			isOk = false;
			result.put("message","Nama Produk tidak boleh kosong");
		}else if(!nama.isEmpty() && !noprodukp.isEmpty()){
			isOk = (!Produk.isExistByNameAndNoProduct(temp, penawaranIds) && !Produk.isExistByName(temp, penawaranIds) && !Produk.isExistNoProduct(temp, penawaranIds));
			if(Produk.isExistByNameAndNoProduct(temp, penawaranIds)){
				result.put("message","Nama Produk : "+temp.nama_produk+" atau No Produk Penyedia: "+temp.no_produk_penyedia+" sudah digunakan");
			}else if(Produk.isExistNoProduct(temp, penawaranIds)){
				result.put("message","No Produk Penyedia:"+temp.no_produk_penyedia+" sudah digunakan");
			}else if(Produk.isExistByName(temp, penawaranIds)){
				result.put("message","Nama Produk : "+temp.nama_produk+" sudah digunakan");
			}else{
				result.put("message","Nama Produk atau No Produk Penyedia valid digunakan");
			}
		}else if(!nama.isEmpty() && noprodukp.isEmpty()){
			isOk = !Produk.isExistByName(temp, penawaranIds);
			if(Produk.isExistByName(temp, penawaranIds)){
				result.put("message","Nama Produk : "+temp.nama_produk+" sudah digunakan");
			}else{
				result.put("message","Nama Produk valid digunakan");
			}
		}else if(nama.isEmpty() && !noprodukp.isEmpty()){
			isOk = !Produk.isExistNoProduct(temp, penawaranIds);
			if(Produk.isExistNoProduct(temp, penawaranIds)){
				result.put("message","No Produk Penyedia:"+temp.no_produk_penyedia+" sudah digunakan");
			}else{
				result.put("message","No Produk Penyedia valid digunakan");
			}
		}
		result.put("valid",isOk);

		renderJSON(result);
	}

	public static void validasiNamaProduk(){
		String nama =params.get("nama") != null ? params.get("nama", String.class) : "";
		String pids = params.get("pid") != null ? params.get("pid", String.class) : "";
		Long ids = params.get("id") != null ? params.get("id", Long.class) : null;

		Map<String, Object> result = new HashMap<>(1);
		Produk temp = new Produk();
		Long penawaranIds = Long.valueOf(pids);

		temp.id = ids;
		temp.nama_produk = nama;
		Boolean isOk = false;
		if(nama.isEmpty()){
			isOk = false;
			result.put("message","Nama Produk tidak boleh kosong");
		}else{
			isOk = !Produk.isExistByName(temp, penawaranIds);
			if(Produk.isExistByName(temp, penawaranIds)){
				result.put("message","Nama Produk : "+temp.nama_produk+" sudah digunakan");
			}else{
				result.put("message","Nama Produk valid digunakan");
			}
		}
		result.put("valid",isOk);

		renderJSON(result);
	}

	@AllowAccess({Acl.COM_PRODUK_ADD})
	public static void createSubmit(Produk produk, ProductForm model) throws ParseException {
		checkAuthenticity();

		validation.valid(produk);
		if (validation.hasErrors()) {
			params.flash();
			flash.error("sempurnakan isian");
			Logger.debug(new Gson().toJson(validation.errorsMap()));
			create(produk.penawaran_id);

		} else {

			if(model.isCreatedMode() && Produk.isExistByNameAndNoProduct(produk,produk.penawaran_id)){
				params.flash();
				flash.error("Produk dengan nama: "+produk.nama_produk+" dan NO.Produk Penyedia="+produk.no_produk_penyedia+" sudah ada.");
				setArgs(true, produk.penawaran_id);
				//replace again with produk
				renderArgs.put("produk", produk);
				renderArgs.put("action", "create");
				render("katalog/ProdukCtr/create.html");
			}else{
				Penawaran penawaran = Penawaran.findById(produk.penawaran_id);
				Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
                if(komoditas.jenis_produk_override == false){
                    produk.jenis_produk = komoditas.jenis_produk;
                }
				Produk prodSave = ProdukService.saveProduk(produk, getAktifUser(), model);
				if(Penawaran.STATUS_MENUNGGU_VERIFIKASI.equalsIgnoreCase(penawaran.status)
					&& !prodSave.isDraft()) {
					prodSave.status = Produk.STATUS_MENUNGGU_PERSETUJUAN;
					prodSave.save();
				}
				else if(!penawaran.status.equals(Penawaran.STATUS_NEGOSIASI)){
					penawaran.changeStatus(Penawaran.STATUS_NEGOSIASI);
				}
				produk = prodSave;
				PenawaranCtr.daftarProduk(produk.penawaran_id);
			}
		}
	}

	//@AllowAccess({Acl.COM_PRODUK_ADD})
	public static void automatedSaving(Produk produk, ProductForm model) {
		LogUtil.debug(TAG, model);
		validation.valid(produk);
		if (validation.hasErrors()
				|| produk == null
				|| StringUtils.isEmpty(produk.nama_produk)
				|| StringUtils.isEmpty(produk.no_produk_penyedia)
		) {
			LogUtil.debug(TAG, "validation error");
			LogUtil.debug(TAG, validation.errorsMap());
			renderJSON(new ServiceResult<>("Sempurnakan isian"));
		}
		Penawaran penawaran = Penawaran.findById(produk.penawaran_id);
		Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
		if(!komoditas.jenis_produk_override){
			produk.jenis_produk = komoditas.jenis_produk;
		}
		Produk prodSave = ProdukService.saveProduk(produk, getAktifUser(), model);
		if (Penawaran.STATUS_MENUNGGU_VERIFIKASI.equalsIgnoreCase(penawaran.status)
				&& !model.isDefaultDraft()) {
			LogUtil.debug(TAG, "update product status to " + Penawaran.STATUS_MENUNGGU_VERIFIKASI);
			if(komoditas.perlu_approval_produk == true) {
				prodSave.status = Produk.STATUS_MENUNGGU_PERSETUJUAN;
				prodSave.save();
			}
		} else if (!penawaran.status.equals(Penawaran.STATUS_NEGOSIASI) && !penawaran.status.equals(Penawaran.STATUS_MENUNGGU_VERIFIKASI)) {
			penawaran.changeStatus(Penawaran.STATUS_NEGOSIASI);
		}
		produk = prodSave;
		renderJSON(new ServiceResult<>(true, "success", produk));
	}

    public static void submitEditProduk(Produk produk, ProductForm model) {
        checkAuthenticity();

        validation.valid(produk);
        if (validation.hasErrors()) {
            params.flash();
            flash.error("sempurnakan isian");
            Logger.debug(new Gson().toJson(validation.errorsMap()));
            create(produk.penawaran_id);

        } else {
            Produk prodSave = ProdukService.saveEditProduk(produk, getAktifUser(), model);
//            produk = prodSave;

			ProdukRiwayat produkRiwayat = new ProdukRiwayat();
			produkRiwayat.produk_id = produk.id;
			produkRiwayat.aksi = "produk_riwayat_simpan_edit_produk";
			produkRiwayat.created_by = AktifUser.getActiveUserId();
			produkRiwayat.created_date = new Timestamp(new java.util.Date().getTime());
			produkRiwayat.save();

//			update elastic
			ElasticsearchLog obj = new ElasticsearchLog();
			obj.deleteProdukbyId(produk.id);

        }
        ProdukCtr.index();
    }

	@AllowAccess({Acl.COM_PRODUK_ADD, Acl.COM_PRODUK_EDIT})
	public static void upload(File file, Long produkId){
		Map<String, Object> result = new HashMap<>(1);
		try {
			DokumenInfo dokumenInfo = ProdukLampiran.simpanLampiran(file, produkId, 0);//notaktif samakan kek image
			List<DokumenInfo> files = new ArrayList<>();
			files.add(dokumenInfo);
			result.put("files", files);
		} catch (Exception e) {
			result.put("errorx", "upload failed");
			e.printStackTrace();

		}
		if (result.get("files")==null){
			result.clear();
			result.put("errorx", "upload failed");
		}

		renderJSON(result);
	}

	@AllowAccess({Acl.COM_PRODUK_ADD, Acl.COM_PRODUK_EDIT})
	public static void imageUpload(File imageFile, Long produkId){
		LogUtil.d(TAG, "product id: " + produkId);
		Map<String, Object> result = new HashMap<String, Object>(1);
		try {
			ProdukGambar gambar = new ProdukGambar(produkId);
			gambar.simpanGambarProduk(imageFile);
			//khawatir akan gagagl simpan gambar , dan menjadi sampah,
			//terlebih ketika tidak disimpan produknya, defaultnya adalah non aktif
			//baru aktif=1 setelah di simpan produk dan dikaitkan ke produk image
			gambar.active = 0;
			gambar.save();

			if(gambar.posisi_file==ProdukGambar._POSISI_FILE_LOKAL){
				gambar.file_url = gambar.getLocalImageStorage()+gambar.file_sub_location+gambar.file_name;
			}else {
				gambar.file_url = gambar.getActiveUrl();
			}
			List<ProdukGambar> files = new ArrayList<>();
			files.add(gambar);
			result.put("files", files);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Logger.debug(result.toString());
		renderJSON(result);
	}

	@AllowAccess({Acl.COM_PRODUK_ADD, Acl.COM_PRODUK_EDIT})
	public static void hapusLampiran(Long fileId, Integer versi){

		BlobTable blobTable = BlobTable.findByBlobId(fileId);
		if(blobTable != null){
			blobTable.delete();
		}
	}

	@AllowAccess({Acl.COM_PRODUK_ADD, Acl.COM_PRODUK_EDIT})
	public static void hapusProdukLampiran(Long fileId){

		// ProdukLampiran pl = ProdukLampiran.findById(fileId);
		// if(pl != null){
		// 	BlobTable blobTable = BlobTable.findById(pl.dok_id_attachment, 0);
		// 	if(blobTable != null){
		// 		blobTable.delete();
		// 	}
		// 	pl.delete();
		// }
		Map<String, Object> rjs = new HashMap<String, Object>(1);
		rjs.put("status", 0);
		try{
			BlobTable blobTable = BlobTable.findByBlobId(fileId) ;//.findById(fileId, 0);
			ProdukLampiran pl = ProdukLampiran.find("dok_id_attachment = ?",blobTable.id).first();
			if(blobTable!=null){
				blobTable.preDelete();
				blobTable.delete();
			}
			if(pl != null){
				pl.delete();
			}
			rjs.put("status", 1);
		}catch (Exception e){}
		renderJSON(rjs);
	}

	@AllowAccess({Acl.COM_PRODUK_ADD,Acl.COM_PRODUK_EDIT})
	public static void hapusGambar(Long fileId){
		Map<String, Object> result = new HashMap<String, Object>(1);
		result.put("status", 0);
        try{
            ProdukGambar gambar = ProdukGambar.findById(fileId);
            if(null != gambar){
                String path = Play.applicationPath+"/public/files/image/produk_gambar/"+gambar.file_sub_location+gambar.file_name;
                File file = new File(path);
                Files.delete(file);
                gambar.delete();
                result.put("status", 1);
            }
        }catch (Exception e){}
		renderJSON(result);
	}

	public static void detPartProdukImage(long id){
		List<ProdukGambar> produkGambar = ProdukGambar.findByProduk(id);
		List<String> resultProdukGambar = new ArrayList<>();
		for(ProdukGambar pg : produkGambar){
			if(pg.posisi_file == ProdukGambar._POSISI_FILE_LOKAL){
				String fileLocal = pg.getLocalImageStorage()+pg.file_sub_location+pg.file_name;
				if(Play.mode.isProd()){
					fileLocal = KatalogUtils.cleanForLocalPathImage(fileLocal);
				}
				resultProdukGambar.add(fileLocal);
				Logger.debug("local posisin:"+fileLocal);
			}else {
				if(Play.mode.isProd()){
					String localImage = KatalogUtils.cleanForLocalPathImage(pg.getActiveUrl());
					resultProdukGambar.add(localImage);
				}else{
					resultProdukGambar.add(pg.getActiveUrl());
				}
			}
		}
	}

	public static void getDetailProductCenter(Long id, String type, Long location_id, Boolean laporan) throws ParseException {
		String randomID = Codec.UUID();
		Produk produk = Produk.findByIdCheck(id);
		Map<Long, String> sortedMapAtributHarga = new TreeMap<>();
		List<Kabupaten> kabupatenList = new ArrayList<>();
		List<Provinsi> provinsiList = new ArrayList<>();
		User user_produk = new User();
		String valueUser = "0";

		//		kondisi produk bisa dibeli
		boolean isCanBeBought = Produk.isCanBeBought(produk);
		renderArgs.put("isCanBeBought", isCanBeBought);


		boolean frontendDetail = true;
		if(!TextUtils.isEmpty(type) &&
				(type.equalsIgnoreCase("province") || type.equalsIgnoreCase("regency"))
					&& location_id == null){
			LogUtil.d(TAG, "redirecting to all detail");
			frontendDetail = false;
		}

		Report rpt = new Report();
		if (getAktifUser() != null) {
			//untuk chat nanti
			valueUser = getAktifUser().user_id.toString();
			rpt.reporter_name = AktifUser.getAktifUser().nama_lengkap;
			rpt.reporter_telp = AktifUser.getAktifUser().no_tlp;
			rpt.reporter_email = AktifUser.getAktifUser().email;
		}

		List<KategoriKonten> listJenisLaporan = KategoriKonten.findAllByKonten(5);
		renderArgs.put("listJenisLaporan", listJenisLaporan);
		List<String> resultProdukGambar = new ArrayList<>();
		if(null != produk){
			List<ProdukGambar> produkGambar = ProdukGambar.findByProduk(id);
			for(ProdukGambar pg : produkGambar){
				if(pg.posisi_file == ProdukGambar._POSISI_FILE_LOKAL){
					String fileLocal = pg.getLocalImageStorage()+pg.file_sub_location+pg.file_name;
					if(Play.mode.isProd()){
						fileLocal = KatalogUtils.cleanForLocalPathImage(fileLocal);
					}
					resultProdukGambar.add(fileLocal);
					Logger.debug("local posisin:"+fileLocal);
				}else {
					if(Play.mode.isProd()){
						String localImage = KatalogUtils.cleanForLocalPathImage(pg.getActiveUrl());
						resultProdukGambar.add(localImage);
					}else{
						resultProdukGambar.add(pg.getActiveUrl());
					}

				}
			}
			if(produk.manufaktur_id != null){
				Manufaktur manufaktur = Manufaktur.findById(produk.manufaktur_id);
				produk.nama_manufaktur = manufaktur.nama_manufaktur;
			}
			if(produk.unit_pengukuran_id != null){
				UnitPengukuran unitPengukuran = UnitPengukuran.findById(produk.unit_pengukuran_id);
				produk.nama_unit_pengukuran = unitPengukuran.nama_unit_pengukuran;
			}
			if(produk.harga_kurs_id != null){
				Kurs kurs = Kurs.findById(produk.harga_kurs_id);
				produk.nama_kurs = kurs.nama_kurs;
			}
			List<KomoditasAtributHarga> atributHargaList = KomoditasAtributHarga.findByKomoditas(produk.komoditas_id);
			Map<Long, String> mapAtributHarga = atributHargaList.stream().collect(
					Collectors.toMap(x -> x.id, x -> x.label_harga));

			sortedMapAtributHarga = new TreeMap<>(mapAtributHarga);

			List<ProdukHarga> produkHarga = ProdukHarga.findRiwayatByProdukId(id);
			Map<String, List<ProdukHarga>> groupHarga = produkHarga.stream()
					.collect(
							Collectors.groupingBy(
									x -> x.getStandarDateString()
							)
					);
			NavigableMap<String, List<ProdukHarga>> navMapProdukHarga = new TreeMap<>(groupHarga).descendingMap();
			renderArgs.put("produkHarga", navMapProdukHarga);

			String hargaUtama = "";
			if ((!TextUtils.isEmpty(type) && (type.equalsIgnoreCase("province") || type.equalsIgnoreCase("regency")) && location_id != null) && produkHarga.size() > 0) {
				KomoditasAtributHarga kah = atributHargaList.stream().parallel().filter(it -> it.apakah_harga_utama == true).findFirst().orElse(new KomoditasAtributHarga());
				ProdukHarga ph = navMapProdukHarga.firstEntry().getValue().stream().parallel().filter(it -> it.komoditas_harga_atribut_id.equals(kah.id)).findFirst().orElse(new ProdukHarga());
				List<ProdukHargaJson> hargaProdukJson = new Gson().fromJson(ph.harga, new TypeToken<List<ProdukHargaJson>>() {
				}.getType());
				ProdukHargaJson hkj = hargaProdukJson.stream()
						.filter(it -> (type.equalsIgnoreCase("province") ? it.provinsiId : it.kabupatenId) == location_id)
						.findFirst().orElse(new ProdukHargaJson());
				//fill harga
				produk.harga_utama = new BigDecimal(hkj.harga);
				hargaUtama = FormatUtils.formatCurrencyRupiah(produk.harga_utama);
				//fill location
				produk.daerah_id = location_id;
			}else{
				hargaUtama = FormatUtils.formatCurrencyRupiah(produk.harga_utama);
			}
			//set param hargaUtama
			renderArgs.put("hargaUtama", hargaUtama);

			Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
			produk.komoditas = komoditas;
			boolean showBuyButton = AktifUser.isUserAllowedToBuy(komoditas);
			renderArgs.put("showBuyButton", showBuyButton);

			List<SpesifikasiProdukJson> spesifikasiProdukJson = new ArrayList<>();
			try{
				spesifikasiProdukJson = new Gson().fromJson(produk.spesifikasi, new TypeToken<List<SpesifikasiProdukJson>>(){}.getType());
			}catch (Exception e){}
			renderArgs.put("spesifikasiProduk", spesifikasiProdukJson);

			notFoundIfNull(produk);
			produk.withPictures();
			produk.withDocuments(true);
			produk.withProvider();
			produk.withCommodity();
			produk.withSpecifications();
			produk.withPenawaran();

			if (frontendDetail) {
				produk.withProvincePrice(location_id);
				produk.withRegencyPrice(location_id);
			}

			if (produk.getProviderId() != null && produk.apakah_ditayangkan && produk.active) {
				if(null != getAktifUser()) {
					new JobUpdateViewCounter(produk,getAktifUser()).now();
				}
			}
			//wilayah jual produk
			if (produk.komoditas.isRegency()) {
				LogUtil.d(TAG, "get regencies");
				kabupatenList = ProdukWilayahJualKabupaten.findWilayahJual(produk.getId());
				renderArgs.put("kabupatenList", kabupatenList);
				if(location_id != null) {
					Kabupaten kabupaten = Kabupaten.findById(location_id);
					produk.daerah_name = kabupaten.nama_kabupaten;
				}
			} else if (produk.komoditas.isProvince()) {
				LogUtil.d(TAG, "get provinces");
				provinsiList = ProdukWilayahJualProvinsi.findWilayahJual(produk.getId());
				renderArgs.put("provinsiList", provinsiList);
				if(location_id != null) {
					Provinsi provinsi = Provinsi.findById(location_id);
					produk.daerah_name = provinsi.nama_provinsi;
				}
			} else {
				LogUtil.d(TAG, "get national");
			}

			//ongkir
			if (produk.komoditas.perlu_ongkir) {
				List<OngkosKirim> listOngkir = new Gson().fromJson(produk.ongkir, new TypeToken<ArrayList<OngkosKirim>>() {
				}.getType());
				if (listOngkir.size() > 0)
					listOngkir = setKabupatenProvinsi(listOngkir, provinsiList, kabupatenList);
				renderArgs.put("listOngkir", listOngkir);
			}

			renderArgs.put("frontendDetail", frontendDetail);
			renderArgs.put("tabLaporan", laporan);

			Penyedia penyedia = Penyedia.findById(produk.penyedia_id);
			user_produk = User.findById(penyedia.user_id);

		}//end if produk !=null

		/* get list produk diskusi */
		List<ProdukDiskusi> listGetProdukDiskusiByIdProduk = ProdukDiskusi.findProdukDiskusiByIdProduk(id);

		/* get list produk diskusi balas */
		List<ProdukDiskusiBalas> listGetProdukDiskusiBalasByIdProduk = ProdukDiskusiBalas.findProdukDiskusiBalasByIdProduk(id);

		AktifUser aktifUser = AktifUser.getAktifUser();
		rpt = new Report();
		if(aktifUser != null){
			rpt.reporter_name = aktifUser.nama_lengkap;
			rpt.reporter_nik = aktifUser.nip;
			rpt.reporter_email = aktifUser.email;
			rpt.reporter_telp = aktifUser.no_tlp;

			renderArgs.put("rpt", rpt);
		}

		if (produk.penawaran_id != null) {
			Penawaran penawaran = Penawaran.findById(produk.penawaran_id);
			Usulan usulan = Usulan.findById(penawaran.usulan_id);
			LogUtil.debug(TAG, "usulanId: " + usulan.id);
			renderArgs.put("allowProductApproval", usulan != null && UsulanService.isNegotiation(usulan));
		}
		render("katalog/ProdukCtr/detailpart/detailCenter.html",
				listGetProdukDiskusiByIdProduk,
				listGetProdukDiskusiBalasByIdProduk,
				produk,
				randomID,
				sortedMapAtributHarga,
				user_produk,
				resultProdukGambar);
	}

	public static void detail(Long id, Long location_id, String type, Boolean laporan, Report rpt, String vUser) {
		Produk produk = Produk.findByIdCheck(id);
		render("katalog/ProdukCtr/web/detail.html", produk,id,location_id,type,laporan,rpt,vUser);
		//, listGetProdukDiskusiBalasByIdProduk, resultProdukGambar, listGetProdukDiskusiByIdProduk, valueUser, user_produk, id, chat, chatCheck, produk, sortedMapAtributHarga, randomID, rpt
	}

	public static void getHargaProdukChangeDetail(Long id,Long location_id, String type){
		Produk produk = Produk.findByIdCheck(id);
		produk.komoditas = Komoditas.findById(produk.komoditas_id);
		List<KomoditasAtributHarga> atributHargaList = KomoditasAtributHarga.findByKomoditas(produk.komoditas_id);
		Map<Long, String> mapAtributHarga = atributHargaList.stream().collect(
				Collectors.toMap(x -> x.id, x -> x.label_harga));

		//sortedMapAtributHarga = new TreeMap<>(mapAtributHarga);

		List<ProdukHarga> produkHarga = ProdukHarga.findRiwayatByProdukId(id);
		Map<String, List<ProdukHarga>> groupHarga = produkHarga.stream()
				.collect(
						Collectors.groupingBy(
								x -> x.getStandarDateString()
						)
				);
		NavigableMap<String, List<ProdukHarga>> navMapProdukHarga = new TreeMap<>(groupHarga).descendingMap();
		//renderArgs.put("produkHarga", navMapProdukHarga);

		String hargaUtama = "";
		if ((!TextUtils.isEmpty(type) && (type.equalsIgnoreCase("province") || type.equalsIgnoreCase("regency")) && location_id != null) && produkHarga.size() > 0) {
			KomoditasAtributHarga kah = atributHargaList.stream().parallel().filter(it -> it.apakah_harga_utama == true).findFirst().orElse(new KomoditasAtributHarga());
			ProdukHarga ph = navMapProdukHarga.firstEntry().getValue().stream().parallel().filter(it -> it.komoditas_harga_atribut_id.equals(kah.id)).findFirst().orElse(new ProdukHarga());
			List<ProdukHargaJson> hargaProdukJson = new Gson().fromJson(ph.harga, new TypeToken<List<ProdukHargaJson>>() {
			}.getType());
			ProdukHargaJson hkj = hargaProdukJson.stream()
					.filter(it -> (type.equalsIgnoreCase("province") ? it.provinsiId : it.kabupatenId) == location_id)
					.findFirst().orElse(new ProdukHargaJson());
			//fill harga
			produk.harga_utama = new BigDecimal(hkj.harga);
			hargaUtama = FormatUtils.formatCurrencyRupiah(produk.harga_utama);
			//fill location
			produk.daerah_id = location_id;
		}else{
			hargaUtama = FormatUtils.formatCurrencyRupiah(produk.harga_utama);
		}
		if (produk.komoditas.isRegency()) {
			LogUtil.d(TAG, "get regencies");

			if(location_id != null) {
				Kabupaten kabupaten = Kabupaten.findById(location_id);
				produk.daerah_name = kabupaten.nama_kabupaten;
			}
		} else if (produk.komoditas.isProvince()) {
			LogUtil.d(TAG, "get provinces");
			if(location_id != null) {
				Provinsi provinsi = Provinsi.findById(location_id);
				produk.daerah_name = provinsi.nama_provinsi;
			}
		} else {
			LogUtil.d(TAG, "get national");
		}

		//set param hargaUtama
		renderArgs.put("hargaUtama", hargaUtama);
		JSONObject jso = new JSONObject();
		jso.put("harga_utama", hargaUtama);
		jso.put("daerah_name", produk.daerah_name);
		jso.put("harga_tanggal", produk.getPriceDateString());
		renderJSON(jso);
	}
	public static void detail_(Long id, Long location_id, String type, Boolean laporan, Report rpt, String vUser) {

        String randomID = Codec.UUID();

        List<KategoriKonten> listJenisLaporan = KategoriKonten.findAllByKonten(5);
        renderArgs.put("listJenisLaporan", listJenisLaporan);

//        try{

            boolean frontendDetail = true;
            if(!TextUtils.isEmpty(type) && (type.equalsIgnoreCase("province") || type.equalsIgnoreCase("regency"))
					&& location_id == null){
                LogUtil.d(TAG, "redirecting to all detail");

                frontendDetail = false;
            }

            LogUtil.d("frontendDetail",frontendDetail);
			Produk produk = Produk.findByIdCheck(id);
			List<ProdukGambar> produkGambar = ProdukGambar.findByProduk(id);

			List<String> resultProdukGambar = new ArrayList<>();
			for(ProdukGambar pg : produkGambar){
				if(pg.posisi_file == ProdukGambar._POSISI_FILE_LOKAL){
					String fileLocal = pg.getLocalImageStorage()+pg.file_sub_location+pg.file_name;
					if(Play.mode.isProd()){
						fileLocal = KatalogUtils.cleanForLocalPathImage(fileLocal);
					}
					resultProdukGambar.add(fileLocal);
					Logger.debug("local posisin:"+fileLocal);
				}else {
					if(Play.mode.isProd()){
						String localImage = KatalogUtils.cleanForLocalPathImage(pg.getActiveUrl());
						resultProdukGambar.add(localImage);
					}else{
						resultProdukGambar.add(pg.getActiveUrl());
					}

				}
			}

			Map<Long, String> sortedMapAtributHarga = new TreeMap<>();
			String valueUser = "";
			User user_produk = new User();
		    Chat chat = new Chat();
		    boolean chatCheck = false;
			if (produk!=null) {
				if(produk.manufaktur_id != null){
					Manufaktur manufaktur = Manufaktur.findById(produk.manufaktur_id);
					produk.nama_manufaktur = manufaktur.nama_manufaktur;
				}
				if(produk.unit_pengukuran_id != null){
					UnitPengukuran unitPengukuran = UnitPengukuran.findById(produk.unit_pengukuran_id);
					produk.nama_unit_pengukuran = unitPengukuran.nama_unit_pengukuran;
				}
				if(produk.harga_kurs_id != null){
					Kurs kurs = Kurs.findById(produk.harga_kurs_id);
					produk.nama_kurs = kurs.nama_kurs;
				}

				List<KomoditasAtributHarga> atributHargaList = KomoditasAtributHarga.findByKomoditas(produk.komoditas_id);
				Map<Long, String> mapAtributHarga = atributHargaList.stream().collect(
						Collectors.toMap(x -> x.id, x -> x.label_harga));

				sortedMapAtributHarga = new TreeMap<>(mapAtributHarga);

				List<ProdukHarga> produkHarga = ProdukHarga.findRiwayatByProdukId(id);
				Map<String, List<ProdukHarga>> groupHarga = produkHarga.stream()
						.collect(
								Collectors.groupingBy(
										x -> x.getStandarDateString()
								)
						);
				NavigableMap<String, List<ProdukHarga>> gh = new TreeMap<>(groupHarga).descendingMap();
				renderArgs.put("produkHarga", gh);

				String hargaUtama = "";
				if ((!TextUtils.isEmpty(type) && (type.equalsIgnoreCase("province") || type.equalsIgnoreCase("regency")) && location_id != null) && produkHarga.size() > 0) {
					KomoditasAtributHarga kah = atributHargaList.stream().parallel().filter(it -> it.apakah_harga_utama == true).findFirst().orElse(new KomoditasAtributHarga());
					ProdukHarga ph = gh.firstEntry().getValue().stream().parallel().filter(it -> it.komoditas_harga_atribut_id.equals(kah.id)).findFirst().orElse(new ProdukHarga());
					List<ProdukHargaJson> hargaProdukJson = new Gson().fromJson(ph.harga, new TypeToken<List<ProdukHargaJson>>() {
					}.getType());
					ProdukHargaJson hkj = hargaProdukJson.stream()
							.filter(it -> (type.equalsIgnoreCase("province") ? it.provinsiId : it.kabupatenId) == location_id)
							.findFirst().orElse(new ProdukHargaJson());
					produk.harga_utama = new BigDecimal(hkj.harga);
					hargaUtama = FormatUtils.formatCurrencyRupiah(produk.harga_utama);
					produk.daerah_id = location_id;
				}else{
					hargaUtama = FormatUtils.formatCurrencyRupiah(produk.harga_utama);
				}
				renderArgs.put("hargaUtama", hargaUtama);

				Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
				boolean showBuyButton = AktifUser.isUserAllowedToBuy(komoditas);
				renderArgs.put("showBuyButton", showBuyButton);
				List<SpesifikasiProdukJson> spesifikasiProdukJson = new ArrayList<>();
				try{
					spesifikasiProdukJson = new Gson().fromJson(produk.spesifikasi, new TypeToken<List<SpesifikasiProdukJson>>(){}.getType());
				}catch (Exception e){}
				renderArgs.put("spesifikasiProduk", spesifikasiProdukJson);

				notFoundIfNull(produk);
				produk.withPictures();
				produk.withDocuments(true);
				produk.withProvider();
				produk.withCommodity();
				produk.withSpecifications();
				produk.withPenawaran();

				if (frontendDetail) {
					produk.withProvincePrice(location_id);
					produk.withRegencyPrice(location_id);
				}

				if (produk.getProviderId() != null && produk.apakah_ditayangkan && produk.active) {
					new ElasticsearchRepository().inputViewCounter(produk, getAktifUser());
				}

				List<Kabupaten> kabupatenList = new ArrayList<>();
				List<Provinsi> provinsiList = new ArrayList<>();

				if (produk.komoditas.isRegency()) {
					LogUtil.d(TAG, "get regencies");
					kabupatenList = ProdukWilayahJualKabupaten.findWilayahJual(produk.getId());
					renderArgs.put("kabupatenList", kabupatenList);
					if(location_id != null) {
						Kabupaten kabupaten = Kabupaten.findById(location_id);
						produk.daerah_name = kabupaten.nama_kabupaten;
					}
				} else if (produk.komoditas.isProvince()) {
					LogUtil.d(TAG, "get provinces");
					provinsiList = ProdukWilayahJualProvinsi.findWilayahJual(produk.getId());
					renderArgs.put("provinsiList", provinsiList);
					if(location_id != null) {
						Provinsi provinsi = Provinsi.findById(location_id);
						produk.daerah_name = provinsi.nama_provinsi;
					}
				} else {
					LogUtil.d(TAG, "get national");
					// List<HargaProdukDaerah> hargaProdukDaerahs = ProdukService.getHargaByDaerah(id,null,Komoditas.NASIONAL);
					// renderArgs.put("priceHistoryNasionalList",hargaProdukDaerahs);
				}
				if (produk.komoditas.perlu_ongkir) {
					List<OngkosKirim> listOngkir = new Gson().fromJson(produk.ongkir, new TypeToken<ArrayList<OngkosKirim>>() {
					}.getType());
					if (listOngkir.size() > 0)
						listOngkir = setKabupatenProvinsi(listOngkir, provinsiList, kabupatenList);
					renderArgs.put("listOngkir", listOngkir);
				}

				renderArgs.put("frontendDetail", frontendDetail);
				renderArgs.put("tabLaporan", laporan);

				if (rpt == null) {
					rpt = new Report();
				}
				if (AktifUser.getAktifUser() != null) {
					rpt.reporter_name = AktifUser.getAktifUser().nama_lengkap;
					rpt.reporter_telp = AktifUser.getAktifUser().no_tlp;
					rpt.reporter_email = AktifUser.getAktifUser().email;
				}

				/*chat*/
				AktifUser aktifUser = getAktifUser();

				if (vUser != null) {
					valueUser = vUser;
				} else {
					valueUser = "0";
				}

				if (aktifUser != null) {
					if (valueUser != "0") {
						chat = Chat.findByIdPengirimAndIdProdukForLink(aktifUser.user_id, id, valueUser);
						if (chat == null) {
							chat = Chat.findByIdPenerimaAndIdProdukForLink(aktifUser.user_id, id, valueUser);
						}
					} else {
						chat = Chat.findByIdPengirimAndIdProduk(aktifUser.user_id, id);
						if (chat == null) {
							chat = Chat.findByIdPenerimaAndIdProduk(aktifUser.user_id, id);
						}
					}

				} else {
					chat = null;
				}


				if (chat != null) {
					chatCheck = true;
				} else {
					chatCheck = false;
				}

				Penyedia penyedia = Penyedia.findById(produk.penyedia_id);
				user_produk = User.findById(penyedia.user_id);
			}
				/* get list produk diskusi */
				List<ProdukDiskusi> listGetProdukDiskusiByIdProduk = ProdukDiskusi.findProdukDiskusiByIdProduk(id);

				/* get list produk diskusi balas */
				List<ProdukDiskusiBalas> listGetProdukDiskusiBalasByIdProduk = ProdukDiskusiBalas.findProdukDiskusiBalasByIdProduk(id);


//		}catch (Exception e){
//			e.printStackTrace();
//			throw new UnsupportedOperationException(e);
//		}

		render("katalog/ProdukCtr/web/detail.html", listGetProdukDiskusiBalasByIdProduk, resultProdukGambar, listGetProdukDiskusiByIdProduk, valueUser, user_produk, id, chat, chatCheck, produk, sortedMapAtributHarga, randomID, rpt);

	}

	public static List<OngkosKirim> setKabupatenProvinsi(List<OngkosKirim> listOngkir, List<Provinsi> listPropinsi, List<Kabupaten> listKabupaten){
		if(listPropinsi.isEmpty()){
			List<String> pvIds = listOngkir.stream().map(it -> it.provinsiId.toString()).collect(Collectors.toList());
			if(!pvIds.isEmpty())
				listPropinsi = Provinsi.findByIds(pvIds);
		}
		if(listKabupaten.isEmpty()){
			List<String> kbIds = listOngkir.stream().map(it -> it.kabupatenId).filter(it->it!=null).map(it->it.toString()).collect(Collectors.toList());
			if(!kbIds.isEmpty())
				listKabupaten = Kabupaten.findByIds(kbIds);
		}
		for(OngkosKirim ok : listOngkir){
			ok.provinsi = listPropinsi.stream().filter(it -> it.id.equals(ok.provinsiId)).findFirst().orElse(new Provinsi()).nama_provinsi;
			ok.kabupaten = listKabupaten.stream().filter(it -> it.id.equals(ok.kabupatenId)).findFirst().orElse(new Kabupaten()).nama_kabupaten;
		}
		return listOngkir;
	}

	public static void allDetail(Long id){
		//try{
			Produk produk = Produk.findById(id);
			List<KomoditasAtributHarga> atributHargaList = new ArrayList<>();
			Map<Long,String> mapAtributHarga = new HashMap<>();
			List<ProdukHarga> produkHarga = new ArrayList<>();
			Map<Long,String> sortedMapAtributHarga = new HashMap<>();

			try{
				atributHargaList = KomoditasAtributHarga.findByKomoditas(produk.komoditas_id);
				mapAtributHarga = atributHargaList.stream().collect(
						Collectors.toMap(x -> x.id, x -> x.label_harga));

				produkHarga = ProdukHarga.findRiwayatByProdukId(id);
				Map<String, List<ProdukHarga>> groupHarga = produkHarga.stream()
						.collect(
								Collectors.groupingBy(
										x -> x.getStandarDateString()
								)
						);
				renderArgs.put("produkHarga", new TreeMap<>(groupHarga).descendingMap());

				sortedMapAtributHarga = new TreeMap<>(mapAtributHarga);

				notFoundIfNull(produk);
				produk.withPictures();
				produk.withDocuments(true);
				produk.withProvider();
				//User user_produk = User.findById(produk.penyedia.user_id);
				produk.withCommodity();
				produk.withSpecifications();
				produk.withPenawaran();

			}catch (Exception e){
				Logger.debug("error fill with paket: "+e.getMessage());
			}
			User user_produk = User.findById(produk.penyedia.user_id);

			if (produk != null) {
				if (produk.getProviderId() != null && produk.apakah_ditayangkan && produk.active) {
					new ElasticsearchRepository().inputViewCounter(produk, getAktifUser());
				}
			}
			List<Kabupaten> kabupatenList = new ArrayList<>();
			List<Provinsi> provinsiList = new ArrayList<>();
			if (produk.komoditas.isRegency()) {
				LogUtil.d(TAG, "get regencies");
				kabupatenList = ProdukWilayahJualKabupaten.findWilayahJual(produk.getId());
				renderArgs.put("kabupatenList", kabupatenList);
			} else if(produk.komoditas.isProvince()) {
				LogUtil.d(TAG, "get provinces");
				provinsiList = ProdukWilayahJualProvinsi.findWilayahJual(produk.getId());
				renderArgs.put("provinsiList", provinsiList);
			}else{
				LogUtil.d(TAG, "get national");
			}
			if(produk.komoditas.perlu_ongkir){
				List<OngkosKirim> listOngkir = new Gson().fromJson(produk.ongkir, new TypeToken<ArrayList<OngkosKirim>>(){}.getType());
				listOngkir = setKabupatenProvinsi(listOngkir, provinsiList, kabupatenList);
				renderArgs.put("listOngkir", listOngkir);
			}

			Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
			boolean showBuyButton = AktifUser.isUserAllowedToBuy(komoditas);
			renderArgs.put("showBuyButton", showBuyButton);

			render("katalog/ProdukCtr/web/detail.html", produk, sortedMapAtributHarga, user_produk);

//		}catch (Exception e){
//			e.printStackTrace();
//			throw new UnsupportedOperationException();
//		}

	}

	@AllowAccess({Acl.COM_PRODUK})
	public static void getRiwayatProduk(long id, Long pId, Long kId) {
		List<ProdukRiwayat> produkRiwayatList = ProdukRiwayat.findByProdukId(id);
		
		renderJSON(produkRiwayatList);
	}

	public static void beliSubmit() {

	}

	@AllowAccess({Acl.COM_PRODUK})
	public static void getPenyedia(String komoditas_id){
		List<Penyedia> penyediaList = new ArrayList<>();
		JsonArray jsonArray = new JsonArray();
		if (!TextUtils.isEmpty(komoditas_id)) {
			penyediaList.addAll(ProviderRepository.findByKomoditas(Long.parseLong(komoditas_id)));
			for (Penyedia model : penyediaList) {
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("id", model.id);
				jsonObject.addProperty("nama_penyedia", model.nama_penyedia);
				jsonObject.addProperty("active", model.active);
				jsonArray.add(jsonObject);
			}
		}
		renderJSON(jsonArray);
	}

	@AllowAccess({Acl.COM_PRODUK})
	public static void getProvOrKab() {

		renderJSON(Provinsi.findAllActive());
	}

	@AllowAccess({Acl.COM_PRODUK})
	public static void getKabByProv(String checkedProv) {
		LogUtil.d("data", checkedProv);
		Type listType = new TypeToken<ArrayList<CheckedProvJson>>(){}.getType();
		List<CheckedProvJson> checkedProvJsonList = CommonUtil.fromJson(checkedProv,listType);

		LogUtil.d("checkedProvJson",CommonUtil.toJson(checkedProvJsonList));

		List<ProvKabJson> result = new ArrayList<>();
		for(CheckedProvJson item : checkedProvJsonList){
			List<Kabupaten> kabList = Kabupaten.findByProvId(item.provId);

			for(Kabupaten kab : kabList){

				ProvKabJson provKabJson = new ProvKabJson();
				provKabJson.provId = item.provId;
				provKabJson.provName = item.provName;
				provKabJson.kabId = kab.id;
				provKabJson.kabName = kab.nama_kabupaten;

				result.add(provKabJson);
			}

		}

		LogUtil.d("ProvKabJson", CommonUtil.toJson(result));
		renderJSON(result);

	}

    @AllowAccess({Acl.COM_PRODUK})
    public static void getAllKabAllProv() {
        LogUtil.d("all prov and kav, %s", "oke");
        List<ProvKabJson> result = new ArrayList<>();
        List<Provinsi> allProv = Provinsi.findAllActive();

        for(Provinsi item : allProv){
            List<Kabupaten> kabList = Kabupaten.findByProvId(item.id);

            for(Kabupaten kab : kabList){

                ProvKabJson provKabJson = new ProvKabJson();
                provKabJson.provId = item.id;
                provKabJson.provName = item.nama_provinsi;
                provKabJson.kabId = kab.id;
                provKabJson.kabName = kab.nama_kabupaten;

                result.add(provKabJson);
            }

        }

        LogUtil.d("AllProvKabJson", CommonUtil.toJson(result));
        renderJSON(result);

    }
	@AllowAccess({Acl.COM_PRODUK})
	public static void getProdukKategoriByParent(long value) {
		List<ProdukKategori> kategoriList = ProdukKategori.find("active = ? and parent_id = ?",
				1, value).fetch();

		renderJSON(kategoriList);
	}

	@AllowAccess({Acl.COM_PRODUK})
	public static void getKetagoriAtributByKatId(long kategori_id) {
		List<ProdukKategoriAtribut> kategoriAtributList = ProdukKategoriAtribut.find("active = ? " +
				"and produk_kategori_id = ?", 1, kategori_id).fetch();

		renderJSON(kategoriAtributList);
	}

	@AllowAccess({Acl.COM_PRODUK_MEMBERI_PERSETUJUAN})
	public static void setujuProduk(long produk_id) {
		int status = Produk.tolakSetujuProduk(produk_id, 1);
		JsonObject json = new JsonObject();
		if (status == 1) {
			json.addProperty("status", 1);
			json.addProperty("message", "Produk telah disetujui");
		} else {
			json.addProperty("status", -1);
			json.addProperty("message", "Persetujuan produk gagal");
		}

		renderJSON(json);
	}

	@AllowAccess({Acl.COM_PRODUK_MEMBERI_PERSETUJUAN})
	public static void tolakProduk(long produk_id) {
		int status = Produk.tolakSetujuProduk(produk_id, 0);
		JsonObject json = new JsonObject();
		if (status == 0) {
			json.addProperty("status", 1);
			json.addProperty("message", "Produk telah ditolak");
		} else {
			json.addProperty("status", -1);
			json.addProperty("message", "Persetujuan produk gagal");
		}

		renderJSON(json);
	}

	@AllowAccess({Acl.COM_PRODUK})
	public static void spesifikasi(long id) {
		List<Perbandingan> perbandinganList = ProdukAtributValue.getPerbandinganSpek(id);

		render(perbandinganList);
	}

	@AllowAccess({Acl.COM_PRODUK})
	public static void harga(long id) {
		Produk produk = Produk.findById(id);
		List<KomoditasAtributHarga> atributHargaList = KomoditasAtributHarga.findByKomoditas(produk.komoditas_id);
			
		Map<Long,String> mapAtributHarga = atributHargaList.stream().collect(
					Collectors.toMap(x -> x.id, x -> x.label_harga));

		Map<Long,String> sortedMapAtributHarga = new TreeMap<>(mapAtributHarga);
		Long[] attrIds = sortedMapAtributHarga.keySet().toArray(new Long[sortedMapAtributHarga.keySet().size()]);
		List<Perbandingan> perbandinganList = ProdukHarga.getPerbandinganHarga(id, attrIds);
		render(perbandinganList, sortedMapAtributHarga);
	}

	@AllowAccess({Acl.COM_PRODUK_MEMBERI_PERSETUJUAN})
	public static void lihatPermintaan(long id) {
		ProdukTungguSetuju produkTungguSetuju = ProdukTungguSetuju.findById(id);
		Produk produk = Produk.findById(produkTungguSetuju.produk_id);
		Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
		Penyedia penyedia = Penyedia.findById(produk.penyedia_id);

		long penawaran_id = produk.penawaran_id;
		render(produk, komoditas, penyedia, penawaran_id);
	}


	public static void esProdukPerKat(long id){
		final String keyKomo = "elasticsFirsByKomoditasid";
		SearchQuery model = new SearchQuery(params);
		model.commodityId = id;
		SearchResult searchResult = null;
		if(model.isFirstReq){
			Logger.info("FirsReq list produk");
			searchResult = Cache.get(keyKomo+id,SearchResult.class);
			if(null == searchResult){
				searchResult = ElasticsearchRepository.open().searchProduct(model);
				Cache.set(keyKomo+id,searchResult,"30mn");
			}else{
				Logger.info("FirsReq list produk from cache");
			}
		}else {
			Logger.info("Non FirsReq list produk");
			searchResult = ElasticsearchRepository.open().searchProduct(model);
		}
		searchResult.setChecked();
		Komoditas komoditas = Komoditas.findById(id);
		boolean showBuyButton = AktifUser.isUserAllowedToBuy(komoditas);

//		cek wilayah sudah dihandle pada halaman esProdukPerKat.html
//		if (komoditas != null && !komoditas.isNational()) {
//			Logger.debug(" masuk not nasional, cek juga wilayahnya ada gak.?");
//			showBuyButton = model.isLocationExists();
//		}

		renderArgs.put("aktifUser",AktifUser.getAktifUser());
		renderArgs.put("showBuyButton", showBuyButton);
		renderArgs.put("searchResult", searchResult);
		render("katalog/ProdukCtr/web/esProdukPerKat.html", komoditas, model);
	}

	public static void listProduk(String language, String komoditas_slug, long id, Integer umkm) {
		Integer isUmkm = params.get("umkm") != null ? params.get("umkm", Integer.class) : 0;
		if (umkm == null) {
			umkm = 0;
		}
		AktifUser aktifUser = AktifUser.getAktifUser();
		SearchQuery model = new SearchQuery();
		model.commodityId = id;
		//SearchResult searchResult = ElasticsearchRepository.open().searchProduct(model);
		//searchResult.setChecked();
		Komoditas komoditas = Komoditas.findById(id);
		Boolean isIklan = true;

		List<Manufaktur> manufakturList = Manufaktur.findManufakturTayangByKomoditas(id);
		List<Penyedia> penyediaList = ProviderRepository.findPenyediaKontrakByKomoditas(id);

		//boolean showBuyButton = AktifUser.isUserAllowedToBuy(komoditas);

		if (komoditas != null && !komoditas.isNational()) {
			List<Provinsi> provinsiList = Provinsi.findAllActive();
			renderArgs.put("provinsiList", provinsiList);
			isIklan = komoditas.apakah_iklan != true;
			//komoditas.apakah_iklan != true
			//jika komoditas selain nasional buttonnya disable mesti tentukan wilayah
			//Logger.debug(" masuk not nasional, cek juga wilayahnya ada gak.?");
//			showBuyButton = model.isLocationExists();
		}



		Map<String, ProdukKategori> categories = ProdukKategori.getKategoriMapByKomId(id);
		//renderArgs.put("searchResult", searchResult);
		String urls = request.url;
		renderArgs.put("urls", urls);
		renderArgs.put("manufakturList", manufakturList);
		renderArgs.put("penyediaList", penyediaList);
		//renderArgs.put("daftarProduk", daftarProduk);
		//renderArgs.put("showBuyButton", showBuyButton);
		renderArgs.put("kategori", categories);
		renderArgs.put("umkm", umkm);
		renderArgs.put("isUmkm", isUmkm);
		//renderArgs.put("aktifUser",aktifUser);
		Boolean isFirst = params.get("first") != null ? params.get("first", Boolean.class) : false;
		renderArgs.put("first", isFirst);
		render("katalog/ProdukCtr/web/listProduk.html", komoditas, model);
	}


	public static void getRiwayatHarga(Long produk_id,Long daerah_id, String kelas_harga){
		LogUtil.d(TAG, "getRiwayatHarga");
		List<HargaProdukDaerah> hargaProdukDaerahs = ProdukService.getHargaByDaerah(produk_id,daerah_id,kelas_harga);

		renderJSON(hargaProdukDaerahs);
	}

	public static void downloadTemplateOngkir(String namaProduk, String namaPenyedia){

		String path = Play.applicationPath+"/public/files/upload/template/template_import_ongkir.xlsx";
		File file = new File(path);
		String fileExtension = FilenameUtils.getExtension(file.getName());

		String dateString = new SimpleDateFormat("ddMMyyyy").format(new Date());
		String namaFile = "["+dateString+"] "+namaPenyedia+"_"+namaProduk+"."+fileExtension;

		renderBinary(file,namaFile);
	}

	public static void productListProvider() {
		List<Komoditas> komoditasList = new ArrayList<>();
		try{
			komoditasList = Komoditas.findByPenyedia(Penyedia.getProviderByUserId(AktifUser.getActiveUserId().longValue()).id);
		}catch (Exception e){}
//		ProductSearchResult model = ProdukService.searchProduct(new controllers.katalog.form.SearchQuery(params));
//		renderArgs.put("searchResult", model);
		renderArgs.put("komoditasList", komoditasList);
		renderTemplate("katalog/ProdukCtr/listProdukPenyedia.html");
	}

	public static void captcha(String id) {
		Images.Captcha captcha = Images.captcha();
		String code = captcha.getText("#8C1919");
		Cache.set(id, code, "10mn");
		renderBinary(captcha);
	}

	public static void reportSubmit(Report rpt, File file,
									@Required(message="Please type the code") String code,
									String randomID) {

		AktifUser aktifUser = getAktifUser();
		ReportRiwayat rptRiwayat = new ReportRiwayat();

		if(aktifUser != null) {
			rpt.reporter_id = aktifUser.user_id;
		}

		rpt.item_type = "produk";
		rpt.status = "proses";

		if(validation.equals(code, Cache.get(randomID)).ok){
			if(file != null){
				if(file.length() <= 5242880) { //in bytes
					LogUtil.d("file size : ", file.length());
					long reportId = rpt.save();
					rptRiwayat.simpanReportRiwayat(reportId, rpt, file);
					flash.success("Laporan telah disimpan dan akan ditindaklanjut oleh tim Admin. ");
				} else {
					flash.error("File upload melebihi batas maksimum ukuran !");
				}
			} else {
				long reportId = rpt.save();
				rptRiwayat.simpanReportRiwayat(reportId, rpt, file);

				flash.success("Laporan telah disimpan dan akan ditindaklanjut oleh tim Admin. ");
			}

			Cache.delete(randomID);
			//new report again
			 Long ids = rpt.item_id;
			 rpt = new Report();
			detail(ids, null, null, true,rpt, "");
		}
		if(validation.hasErrors()){
			flash.error("Invalid Captcha. Please type it again");
			detail(rpt.item_id, null, null, true, rpt, "");
		}
	}

	public static void reportReply(Report rpt, File file) {
		Report report = Report.findById(rpt.id);
		ReportRiwayat rptRiwayat = new ReportRiwayat();
		Date date = new Date();

		rpt.reporter_id = Long.valueOf(AktifUser.getActiveUserId());
		report.reply = rpt.message;
		report.reply_by = rpt.reporter_id;
		report.reply_date = date;
		if(report.reporter_id == null){
			report.reporter_id = Long.valueOf(0);
		}

		if(file != null) {
			if(file.length() <= 5242880) { //in bytes
				if(!report.reporter_id.equals(rpt.reporter_id)) {
					report.save();
				}
				rptRiwayat.simpanReportRiwayat(rpt.id, rpt, file);
				flash.success("Balasan laporan telah disimpan. ");
			} else {
				flash.error("File upload melebihi batas maksimum ukuran !");
			}
		} else {
			if(!report.reporter_id.equals(rpt.reporter_id)) {
				report.save();
			}
			rptRiwayat.simpanReportRiwayat(rpt.id, rpt, file);
			flash.success("Balasan laporan telah disimpan. ");
		}

		redirect("katalog.laporanprodukctr.detail", rpt.id);
	}

	public static void reportClose(Long id){
		JsonObject jsonObject = new JsonObject();
		Report report = new Report();
		if (report.closeReport(id)) {
			jsonObject.addProperty("isSuccess", true);
			jsonObject.addProperty("message", "Diskusi laporan berhasil ditutup");
		} else {
			jsonObject.addProperty("isSuccess", false);
			jsonObject.addProperty("message", "Diskusi laporan gagal ditutup!");
		}
		renderJSON(jsonObject);
	}

	public static void infoSetujuTayang (long prodId){
		StringBuilder content = new StringBuilder();
		JsonObject json = new JsonObject();
		List<String> breadcrumb = new ArrayList<>();

		Produk produk = Produk.getProdukInfoTayang(prodId);
		ProdukKategori produkKategori = ProdukKategori.find("id = ? and active = 1", produk.produk_kategori_id).first();

		boolean isTheLast = false;
		int idx = 0;
		content.append("<p>");
		while (!isTheLast){
			if(produkKategori == null){
				break;
			}
			breadcrumb.add(idx, produkKategori.nama_kategori);
			idx += 1;

			if(produkKategori.parent_id == null || produkKategori.parent_id == 0){
				isTheLast = true;
				continue;
			}
			produkKategori = ProdukKategori.find("id = ? and active = 1", produkKategori.parent_id).first();
		}
		for(int i=breadcrumb.size()-1; i >= 0; i--){
			content.append(breadcrumb.get(i));

			if(i != 0){
				content.append(" <i class='fa fa-caret-right'></i> ");
			}
		}
		content.append("</p>");

		String notAvailable = "<span style='color:#ABABAB'>n/a</span>";
		String mintaDisetujuiOleh = produk.nama_minta_disetujui == null ? notAvailable : produk.nama_minta_disetujui;
		String mintaDisetujuiTanggal = produk.minta_disetujui_tanggal == null ? notAvailable : DateTimeUtils.convertToDdMmYyyy(produk.minta_disetujui_tanggal);
		String mintaDisetujuiAlasan = produk.minta_disetujui_alasan == null ? notAvailable : produk.minta_disetujui_alasan;
		String setujuTolakOleh = produk.nama_setuju_tolak == null ? notAvailable : produk.nama_setuju_tolak;
		String setujuTolakTanggal = produk.setuju_tolak_tanggal == null ? notAvailable : DateTimeUtils.convertToDdMmYyyy(produk.setuju_tolak_tanggal);
		String setujuTolakAlasan = produk.setuju_tolak_alasan == null ? notAvailable : produk.setuju_tolak_alasan;

		String setujuTolak = "";
		if(produk.setuju_tolak != null){
			if(produk.setuju_tolak.matches("setuju")){
				setujuTolak = "<i style='color:green' class='fa fa-check-circle fa-lg'></i> ";
			}else {
				setujuTolak = "<i style='color:red' class='fa fa-minus-circle fa-lg'></i> ";
			}
			setujuTolak += produk.setuju_tolak;
		}else{
			setujuTolak = notAvailable;
		}

		json.addProperty("kategori", content.toString());
		json.addProperty("url_image", produk.getActiveImageUrl());
		json.addProperty("no_produk", produk.no_produk);
		json.addProperty("nama_produk", produk.nama_manufaktur + " " + produk.nama_produk);
		json.addProperty("oleh_permintaan", mintaDisetujuiOleh);
		json.addProperty("tgl_permintaan", mintaDisetujuiTanggal);
		json.addProperty("alasan_permintaan", mintaDisetujuiAlasan);
		json.addProperty("hasil_keputusan", setujuTolak);
		json.addProperty("oleh_keputusan", setujuTolakOleh);
		json.addProperty("tgl_keputusan", setujuTolakTanggal);
		json.addProperty("alasan_keputusan", setujuTolakAlasan);
		renderJSON(json);
	}

	public static void infoProdukTayang (long prodId){
		StringBuilder content = new StringBuilder();
		JsonObject json = new JsonObject();

//		Map<Long, models.product.Produk> modelTayangList = new HashMap<>();
//		modelTayangList = modelTayangListDefault;

		models.product.Produk produk = models.product.Produk.getProdukInfoSyaratTayang(prodId);

		String notAvailable = "<span style='color:#ABABAB'>n/a</span>";
		String notAvailable2 = "<span style='color:red' class='fa fa-close fa-lg'></span>";
		String syarat_tayang_nama_produk = "";
		String syarat_tayang_nomor_produk = "";
		String syarat_tayang_produk_masih_aktif_tidak = "";
		String syarat_tayang_produk_dapat_ditayangkan = "";
		String syarat_tayang_produk_disetujui_ditolak = "";
		String syarat_tayang_tanggal_berlaku_produk = "";
		String syarat_tayang_jumlah_stok_produk = "";
		String syarat_tayang_komoditas = "";
		String syarat_tayang_status_komoditas = "";
		String syarat_tayang_stok_unlimited_tidak = "";
		String syarat_tayang_penawaran = "";
		String syarat_tayang_tanggal_mulai_berlaku_kontrak = "";
		String syarat_tayang_tanggal_selesai_berkalu_kontrak = "";
		String syarat_tayang_kontrak = "";
		String syarat_tayang_status_kontrak = "";
		String syarat_tayang_status_penayang_produk = "" ;
		String syarat_tayang_kontrak_berlaku = "";


			syarat_tayang_nama_produk = produk.nama_produk == null ? notAvailable : produk.nama_produk ;
			syarat_tayang_nomor_produk = produk.no_produk == null ? notAvailable : produk.no_produk ;
			syarat_tayang_produk_masih_aktif_tidak = produk.produk_aktif == null ? notAvailable : (produk.produk_aktif.equals(1) ? "Ya":"Tidak") ;
			syarat_tayang_produk_dapat_ditayangkan = produk.produk_apakah_ditayangkan == null ? notAvailable : (produk.produk_apakah_ditayangkan.equals(1) ? "Ya":"Tidak") ;
			syarat_tayang_produk_disetujui_ditolak = produk.produk_setuju_tolak == null ? notAvailable : produk.produk_setuju_tolak ;

			syarat_tayang_tanggal_berlaku_produk = produk.produk_tanggal_berlaku == null ? notAvailable : DateTimeUtils.convertToDdMmYyyy(produk.produk_tanggal_berlaku) ;
			syarat_tayang_jumlah_stok_produk = produk.produk_jumlah_stok == null ? notAvailable : produk.produk_jumlah_stok.toString() ;
			syarat_tayang_komoditas = produk.komoditas_nama == null ? notAvailable : produk.komoditas_nama ;
			syarat_tayang_status_komoditas = produk.komoditas_aktif == null ? notAvailable : (produk.komoditas_aktif.equals(1) ? "Aktif":"Tidak Aktif") ;
			syarat_tayang_stok_unlimited_tidak = produk.komoditas_apakah_stok_unlimited == null ? notAvailable : (produk.komoditas_apakah_stok_unlimited.equals(1) ? "Ya":"Tidak") ;

			syarat_tayang_penawaran = produk.id_penawaran == null ? notAvailable : (produk.id_penawaran != 0 ? "Ada":"Tidak Ada") ;
			syarat_tayang_tanggal_mulai_berlaku_kontrak = produk.kontrak_tgl_masa_berlaku_mulai == null ? notAvailable : DateTimeUtils.convertToDdMmYyyy(produk.kontrak_tgl_masa_berlaku_mulai) ;
			syarat_tayang_tanggal_selesai_berkalu_kontrak = produk.kontrak_tgl_masa_berlaku_selesai == null ? notAvailable : DateTimeUtils.convertToDdMmYyyy(produk.kontrak_tgl_masa_berlaku_selesai) ;
			syarat_tayang_kontrak = produk.id_kontrak == null ? "Tidak Ada" : (!produk.id_kontrak.equals(0) ? "Ada":"Tidak Ada") ;
			syarat_tayang_status_kontrak = produk.kontrak_aktif == null ? notAvailable : (produk.kontrak_aktif.equals(1) ? "Aktif":"Tidak Aktif") ;
			syarat_tayang_status_penayang_produk = produk.sudah_tayang == null ? notAvailable : produk.sudah_tayang ;
			syarat_tayang_kontrak_berlaku = produk.kontrak_berlaku == null ? notAvailable : (produk.kontrak_berlaku.equals(1) ? "Ya":"Tidak");


		String tayangAtauTidak = "";
		if(produk.sudah_tayang != null){
			if(produk.sudah_tayang.matches("Tayang")){
				tayangAtauTidak = "<i style='color:green' class='fa fa-check-circle fa-lg'></i> "+"<b style='color:green'>"+produk.sudah_tayang+"</b>";
			}else {
				tayangAtauTidak = "<i style='color:red' class='fa fa-minus-circle fa-lg'></i> "+"<b style='color:red'>"+produk.sudah_tayang+"</b>";
			}
		}else{
			tayangAtauTidak = notAvailable;
		}

		syarat_tayang_status_penayang_produk = tayangAtauTidak;

		String st_produk_dapat_ditayangkan = produk.produk_apakah_ditayangkan == null ? notAvailable2 : (produk.produk_apakah_ditayangkan.equals(1) ? "<span style='color:green' class='fa fa-check fa-lg'></span>" : "<span style='color:red' class='fa fa-close fa-lg'></span>") ;
		String st_produk_disetujui_ditolak = produk.produk_setuju_tolak == null ? notAvailable2 : (produk.produk_setuju_tolak.equalsIgnoreCase("setuju") ? "<span style='color:green' class='fa fa-check fa-lg'></span>" : "<span style='color:red' class='fa fa-close fa-lg'></span>") ;
		String st_status_komoditas = produk.komoditas_aktif == null ? notAvailable2 : (produk.komoditas_aktif.equals(1) ? "<span style='color:green' class='fa fa-check fa-lg'></span>" : "<span style='color:red' class='fa fa-close fa-lg'></span>") ;
		String st_status_kontrak = produk.kontrak_aktif == null ? notAvailable2 : (produk.kontrak_aktif.equals(1) ? "<span style='color:green' class='fa fa-check fa-lg'></span>" : "<span style='color:red' class='fa fa-close fa-lg'></span>") ;
		String st_kontrak = produk.id_kontrak == null ? notAvailable2 : (produk.id_kontrak != 0 ? "<span style='color:green' class='fa fa-check fa-lg'></span>" : "<span style='color:red' class='fa fa-close fa-lg'></span>") ;
		String st_penawaran = produk.id_penawaran == null ? notAvailable2 : (produk.id_penawaran != 0 ? "<span style='color:green' class='fa fa-check fa-lg'></span>" : "<span style='color:red' class='fa fa-close fa-lg'></span>") ;;
		String st_tgl_berlaku_produk = produk.produk_tanggal_berlaku_status == null ? notAvailable2 : (produk.produk_tanggal_berlaku_status.equals(1) ? "<span style='color:green' class='fa fa-check fa-lg'></span>" : "<span style='color:red' class='fa fa-close fa-lg'></span>") ;
		String st_tgl_mulai_berlaku_kontrak = produk.kontrak_tgl_masa_berlaku_mulai_status == null ? notAvailable2 : (produk.kontrak_tgl_masa_berlaku_mulai_status.equals(1) ? "<span style='color:green' class='fa fa-check fa-lg'></span>" : "<span style='color:red' class='fa fa-close fa-lg'></span>") ;
		String st_tgl_selesai_berlaku_kontrak = produk.kontrak_tgl_masa_berlaku_selesai_status == null ? notAvailable2 : (produk.kontrak_tgl_masa_berlaku_selesai_status.equals(1) ? "<span style='color:green' class='fa fa-check fa-lg'></span>" : "<span style='color:red' class='fa fa-close fa-lg'></span>") ;
		String st_stok_produk_unlimited = produk.status_stok == null ? notAvailable2 : (produk.status_stok.equals(1) ? "<span style='color:green' class='fa fa-check fa-lg'></span>" : "<span style='color:red' class='fa fa-close fa-lg'></span>") ;
		String st_kontrak_berlaku = produk.kontrak_berlaku == null ? notAvailable2 : (produk.kontrak_berlaku.equals(1) ? "<span style='color:green' class='fa fa-check fa-lg'></span>" : "<span style='color:red' class='fa fa-close fa-lg'></span>") ;

		json.addProperty("syarat_tayang_nama_produk",syarat_tayang_nama_produk);
		json.addProperty("syarat_tayang_nomor_produk",syarat_tayang_nomor_produk);
		json.addProperty("syarat_tayang_produk_masih_aktif_tidak",syarat_tayang_produk_masih_aktif_tidak);
		json.addProperty("syarat_tayang_produk_dapat_ditayangkan",syarat_tayang_produk_dapat_ditayangkan);
		json.addProperty("syarat_tayang_produk_disetujui_ditolak",syarat_tayang_produk_disetujui_ditolak);

		json.addProperty("syarat_tayang_tanggal_berlaku_produk",syarat_tayang_tanggal_berlaku_produk);
		json.addProperty("syarat_tayang_jumlah_stok_produk",syarat_tayang_jumlah_stok_produk);
		json.addProperty("syarat_tayang_komoditas",syarat_tayang_komoditas);
		json.addProperty("syarat_tayang_status_komoditas",syarat_tayang_status_komoditas);
		json.addProperty("syarat_tayang_stok_unlimited_tidak",syarat_tayang_stok_unlimited_tidak);

		json.addProperty("syarat_tayang_penawaran",syarat_tayang_penawaran);
		json.addProperty("syarat_tayang_tanggal_mulai_berlaku_kontrak",syarat_tayang_tanggal_mulai_berlaku_kontrak);
		json.addProperty("syarat_tayang_tanggal_selesai_berkalu_kontrak",syarat_tayang_tanggal_selesai_berkalu_kontrak);
		json.addProperty("syarat_tayang_status_kontrak",syarat_tayang_status_kontrak);
		json.addProperty("syarat_tayang_kontrak",syarat_tayang_kontrak);
		json.addProperty("syarat_tayang_status_penayang_produk",syarat_tayang_status_penayang_produk);
		json.addProperty("syarat_tayang_kontrak_berlaku",syarat_tayang_kontrak_berlaku);

		json.addProperty("st_produk_dapat_ditayangkan",st_produk_dapat_ditayangkan);
		json.addProperty("st_produk_disetujui_ditolak",st_produk_disetujui_ditolak);
		json.addProperty("st_status_komoditas",st_status_komoditas);
		json.addProperty("st_kontrak",st_kontrak);
		json.addProperty("st_status_kontrak",st_status_kontrak);
		json.addProperty("st_penawaran",st_penawaran);
		json.addProperty("st_tgl_berlaku_produk",st_tgl_berlaku_produk);
		json.addProperty("st_tgl_mulai_berlaku_kontrak",st_tgl_mulai_berlaku_kontrak);
		json.addProperty("st_tgl_selesai_berlaku_kontrak",st_tgl_selesai_berlaku_kontrak);
		json.addProperty("st_stok_produk_unlimited",st_stok_produk_unlimited);
		json.addProperty("st_kontrak_berlaku",st_kontrak_berlaku);
		renderJSON(json);
	}

	public static void updateStatusUmkm(List<Long> ids, Boolean currentStatus) {
		if (ids == null || ids.isEmpty() || currentStatus == null) {
			renderJSON(new ServiceResult<>("Missing params!"));
		}
		for (Long id : ids) {
			models.product.Produk product = models.product.Produk.findById(id);
			if (product == null) {
				renderJSON(new ServiceResult<>("Product not found!"));
			}
			try {
				product.is_umkm = currentStatus;
				product.save();
				ElasticsearchLog obj = new ElasticsearchLog();
				obj.createJobUpdateElastic(id);
				Cache.delete("elasticsFirsByKomoditasid" + product.komoditas_id);
			} catch (Exception e) {
				LogUtil.error(TAG, e);
			}
		}
		renderJSON(new ServiceResult<>(true, "success", ids));
	}

	class ProdukHargaKab{
		Long kabupatenId;
		Long provinsiId;
		String harga;

		public ProdukHargaKab(Long kabupatenId, Long provinsiId, String harga) {
			this.kabupatenId = kabupatenId;
			this.provinsiId = provinsiId;
			this.harga = harga;
		}
	}

	class ProdukHargaProv{
		Long provinsiId;
		String harga;

		public ProdukHargaProv(Long provinsiId, String harga) {
			this.provinsiId = provinsiId;
			this.harga = harga;
		}
	}

	static class ProdukHeader{
		Long id;
		String nama_penyedia;
		String nama_produk;
		String nama_manufaktur;
		String nama_unit_pengukuran;
		String nama_komoditas;
		String kode_komoditas;
		String kelas_harga;
		Long komoditas_id;
		String tkdn_produk;
		Date berlaku_sampai;
		String nama_kurs;
		Date harga_tanggal;

		public static ProdukHeader findById(Long id) {
			String query = "select \n" +
					"p.id, \n" +
					"py.nama_penyedia, \n" +
					"p.nama_produk, \n" +
					"m.nama_manufaktur, \n" +
					"up.nama_unit_pengukuran, \n" +
					"kom.nama_komoditas,\n" +
					"kom.kode_komoditas,\n" +
					"kom.kelas_harga,\n" +
					"p.komoditas_id,\n" +
					"p.tkdn_produk, \n" +
					"p.berlaku_sampai, \n" +
					"k.nama_kurs, \n" +
					"p.harga_tanggal\n" +
					"from produk p \n" +
					"left join penyedia py on p.penyedia_id = py.id\n" +
					"left join manufaktur m on p.manufaktur_id = m.id\n" +
					"left join komoditas kom on p.komoditas_id = kom.id\n" +
					"left join unit_pengukuran up on p.unit_pengukuran_id = up.id\n" +
					"left join kurs k on p.harga_kurs_id = k.id\n" +
					"where p.id = ?";
			return Query.find(query, ProdukHeader.class, id).first();
		}
	}

	public static void export(Long produk_id_export, String kelas_harga){
		if(produk_id_export == null){
			flash.error("Tidak ada produk yang dipilih!");
		}else {
			String[] detailItem = {"Nama Penyedia",
					"Nama Produk", "Nama Manufaktur", "Unit Pengukuran",
					"Nama Komoditas", "Kode Komoditas", "Kelas Harga",
					"TKDN Produk", "Berlaku Sampai", "KURS", "Harga Tanggal"};

			ProdukHeader produk = ProdukHeader.findById(produk_id_export);
			Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
			Map<Long, String> headerExcel = new TreeMap<>();
			Map<Long, Object> dataBody = new TreeMap<>();
			List<KomoditasAtributHarga> atributHargaList = KomoditasAtributHarga.findByKomoditas(produk.komoditas_id);
			Map<Long, String> mapAtributHarga = atributHargaList.stream().collect(
					Collectors.toMap(x -> x.id, x -> x.label_harga));
			for (int i = 0; i < detailItem.length; i++){
				headerExcel.put(Long.valueOf(i-(detailItem.length+3)),detailItem[i]);
			}
			if (komoditas.isProvince()){
				headerExcel.put(-3L,"Provinsi");
			} else {
				headerExcel.put(-3L,"Kabupaten");
			}

			headerExcel.put(-2L,"Tanggal");
			headerExcel.put(-1L,"Kurs");
			headerExcel.putAll(mapAtributHarga);

			List<ProdukHarga> list = ProdukHarga.findRiwayatAllByProdukIdAndTanggal(produk.id, produk.harga_tanggal);

			HashMap<Long,ArrayList<String>> dataHarga = new HashMap<Long, ArrayList<String>>();
			HashMap<Long,ArrayList<String>> dataProduk = new HashMap<Long, ArrayList<String>>();
			String pattern = "yyyy-MM-dd";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			String date = "";
			String kurs = "";
			int jumlahKolomTambahan = 3+detailItem.length; //kab/prov,tgl,kurs

			if(list == null || list.size() == 0){
				String msg = "Produk harga tidak ada yang disetujui atau tanggal harga produk tidak cocok dengan harga tanggal produk atau tidak ada harga yang aktif!";
				flash.error(msg);

				dataHarga.put(0L,new ArrayList<String>());
				dataHarga.get(0L).add(0, produk.nama_penyedia);
				dataHarga.get(0L).add(1, produk.nama_produk);
				dataHarga.get(0L).add(2, produk.nama_manufaktur);
				dataHarga.get(0L).add(3, produk.nama_unit_pengukuran);
				dataHarga.get(0L).add(4, produk.nama_komoditas);
				dataHarga.get(0L).add(5, produk.kode_komoditas);
				dataHarga.get(0L).add(6, produk.kelas_harga);
				dataHarga.get(0L).add(7, produk.tkdn_produk);
				dataHarga.get(0L).add(8, produk.berlaku_sampai != null ? simpleDateFormat.format(produk.berlaku_sampai):"");
				dataHarga.get(0L).add(9, produk.nama_kurs);
				dataHarga.get(0L).add(10, produk.harga_tanggal != null ? simpleDateFormat.format(produk.harga_tanggal):"");
				dataHarga.get(0L).add(11, msg);

				//Create blank workbook
				XSSFWorkbook workbook = new XSSFWorkbook();

				//Create a blank sheet
				XSSFSheet spreadsheet = workbook.createSheet("Produk Harga e-Katalog");

				//Create row object
				XSSFRow row;

				//This data needs to be written (Object[])
				Map < Integer, Object[] > empinfo = new TreeMap < Integer, Object[]>();
				//header tabel produk harga
				empinfo.put( 1, headerExcel.values().toArray() );
				empinfo.put( 2, dataHarga.get(0L).toArray() );
				//body tabel produk harga

				//Iterate over data and write to sheet
				Set < Integer > keyid = empinfo.keySet();
				int rowid = 0;

				for (Integer key : keyid) {
					row = spreadsheet.createRow(rowid++);
					Object [] objectArr = empinfo.get(key);
					int cellid = 0;

					for (Object obj : objectArr) {
						Cell cell = row.createCell(cellid++);
						cell.setCellValue((String)obj);
					}
				}

				//Create file system using specific name
				File file = new File("produk_harga_" + produk_id_export + ".xlsx");
				try{
					OutputStream out = new FileOutputStream(file);
					//write operation workbook using file out object
					workbook.write(out);
					out.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				renderBinary(file);

			} else {

				Map<Long,String> kab = new HashMap<>();
				Map<Long,String> prov = new HashMap<>();

				kab = Kabupaten.findAllActive();
				prov = Provinsi.findAllActiveMap();

				List<ProdukHargaProv> produkHargaProv = new ArrayList<>();
				List<ProdukHargaKab> produkHargaKab = new ArrayList<>();

				//satukan produk harga dari row menjadi column
				if (list!=null && list.size() > 0){
					for (int pdata = 0; pdata < list.size(); pdata++){
						date = simpleDateFormat.format(list.get(pdata).tanggal_dibuat);
						kurs = list.get(pdata).nama_kurs;

						if (komoditas.isProvince()) {
							produkHargaProv = new Gson().fromJson(list.get(pdata).harga, new TypeToken<List<ProdukHargaProv>>(){}.getType());
							for (int i = 0; i < produkHargaProv.size(); i++){
								if (pdata == 0){
									dataHarga.put(produkHargaProv.get(i).provinsiId,new ArrayList<String>());
									dataHarga.get(produkHargaProv.get(i).provinsiId).add(0, produk.nama_penyedia);
									dataHarga.get(produkHargaProv.get(i).provinsiId).add(1, produk.nama_produk);
									dataHarga.get(produkHargaProv.get(i).provinsiId).add(2, produk.nama_manufaktur);
									dataHarga.get(produkHargaProv.get(i).provinsiId).add(3, produk.nama_unit_pengukuran);
									dataHarga.get(produkHargaProv.get(i).provinsiId).add(4, produk.nama_komoditas);
									dataHarga.get(produkHargaProv.get(i).provinsiId).add(5, produk.kode_komoditas);
									dataHarga.get(produkHargaProv.get(i).provinsiId).add(6, produk.kelas_harga);
									dataHarga.get(produkHargaProv.get(i).provinsiId).add(7, produk.tkdn_produk);
									dataHarga.get(produkHargaProv.get(i).provinsiId).add(8, produk.berlaku_sampai != null ? simpleDateFormat.format(produk.berlaku_sampai):"");
									dataHarga.get(produkHargaProv.get(i).provinsiId).add(9, produk.nama_kurs);
									dataHarga.get(produkHargaProv.get(i).provinsiId).add(10, produk.harga_tanggal != null ? simpleDateFormat.format(produk.harga_tanggal):"");
									dataHarga.get(produkHargaProv.get(i).provinsiId).add(11, prov.get(produkHargaProv.get(i).provinsiId));
									dataHarga.get(produkHargaProv.get(i).provinsiId).add(12, date);
									dataHarga.get(produkHargaProv.get(i).provinsiId).add(13, kurs);
								}
								dataHarga.get(produkHargaProv.get(i).provinsiId).add(pdata+jumlahKolomTambahan, produkHargaProv.get(i).harga);
							}
						} else {
							produkHargaKab = new Gson().fromJson(list.get(pdata).harga, new TypeToken<List<ProdukHargaKab>>(){}.getType());
							for (int i = 0; i < produkHargaKab.size(); i++){
								if (pdata == 0){
									dataHarga.put(produkHargaKab.get(i).kabupatenId, new ArrayList<String>());
									dataHarga.get(produkHargaKab.get(i).kabupatenId).add(0, produk.nama_penyedia);
									dataHarga.get(produkHargaKab.get(i).kabupatenId).add(1, produk.nama_produk);
									dataHarga.get(produkHargaKab.get(i).kabupatenId).add(2, produk.nama_manufaktur);
									dataHarga.get(produkHargaKab.get(i).kabupatenId).add(3, produk.nama_unit_pengukuran);
									dataHarga.get(produkHargaKab.get(i).kabupatenId).add(4, produk.nama_komoditas);
									dataHarga.get(produkHargaKab.get(i).kabupatenId).add(5, produk.kode_komoditas);
									dataHarga.get(produkHargaKab.get(i).kabupatenId).add(6, produk.kelas_harga);
									dataHarga.get(produkHargaKab.get(i).kabupatenId).add(7, produk.tkdn_produk);
									dataHarga.get(produkHargaKab.get(i).kabupatenId).add(8, produk.berlaku_sampai != null ? simpleDateFormat.format(produk.berlaku_sampai):"");
									dataHarga.get(produkHargaKab.get(i).kabupatenId).add(9, produk.nama_kurs);
									dataHarga.get(produkHargaKab.get(i).kabupatenId).add(10, produk.harga_tanggal != null ? simpleDateFormat.format(produk.harga_tanggal):"");
									dataHarga.get(produkHargaKab.get(i).kabupatenId).add(11, kab.get(produkHargaKab.get(i).kabupatenId));
									dataHarga.get(produkHargaKab.get(i).kabupatenId).add(12, date);
									dataHarga.get(produkHargaKab.get(i).kabupatenId).add(13, kurs);
								}
								dataHarga.get(produkHargaKab.get(i).kabupatenId).add(pdata+jumlahKolomTambahan, produkHargaKab.get(i).harga);
							}
						}
					}
				}

				//Create blank workbook
				XSSFWorkbook workbook = new XSSFWorkbook();

				//Create a blank sheet
				XSSFSheet spreadsheet = workbook.createSheet("Produk Harga e-Katalog");

				//Create row object
				XSSFRow row;

				//This data needs to be written (Object[])
				Map < Integer, Object[] > empinfo = new TreeMap < Integer, Object[]>();
				//header tabel produk harga
				empinfo.put( 1, headerExcel.values().toArray() );
				//body tabel produk harga
				if (komoditas.isProvince()) {
					for (int i = 1; i <= produkHargaProv.size(); i++) {
						empinfo.put(i + 1, dataHarga.get(produkHargaProv.get(i - 1).provinsiId).toArray());
					}
				} else {
					for (int i = 1; i <= produkHargaKab.size(); i++) {
						empinfo.put(i + 1, dataHarga.get(produkHargaKab.get(i - 1).kabupatenId).toArray());
					}
				}

				//Iterate over data and write to sheet
				Set < Integer > keyid = empinfo.keySet();
				int rowid = 0;

				for (Integer key : keyid) {
					row = spreadsheet.createRow(rowid++);
					Object [] objectArr = empinfo.get(key);
					int cellid = 0;

					for (Object obj : objectArr) {
						Cell cell = row.createCell(cellid++);
						cell.setCellValue((String)obj);
					}
				}

				//Create file system using specific name
				File file = new File("produk_harga_" + produk_id_export + ".xlsx");
				try{
					OutputStream out = new FileOutputStream(file);
					//write operation workbook using file out object
					workbook.write(out);
					out.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				renderBinary(file);
			}
		}
		index();
	}
}
