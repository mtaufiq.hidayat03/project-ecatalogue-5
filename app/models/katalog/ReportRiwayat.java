package models.katalog;

import models.BaseKatalogTable;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.user.User;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import utils.*;

import javax.persistence.Transient;
import java.io.File;
import java.util.List;

/**
 * Created by fajar on 5/28/18.
 */
@Table(name = "report_riwayat")
public class ReportRiwayat extends BaseKatalogTable{
    @Id
    public Long id;

    public Long report_id;

    public Long reporter_id;

    public String message;

    public String nama_file;

    public String file_hash;

    public Long blb_id_content;

    @Transient
    public String reporter_name;

    @Transient
    public String url_download;

    public void simpanReportRiwayat(Long report_id, Report rpt, File file){
        try {
            BlobTable blob = null;
            if(file != null) {
                blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, this.blb_id_content, ReportRiwayat.class.getSimpleName());
                this.nama_file = blob.blb_nama_file;
                this.file_hash = blob.blb_hash;
                this.blb_id_content = blob.id;

            }
            this.report_id = report_id;
            this.reporter_id = rpt.reporter_id;
            this.message = rpt.message;

            Long idr = new Long(this.save());
            if(null != blob){
                blob.blb_id_content = idr;
                blob.save();
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            LogUtil.d("***************** error ***************", e.getMessage());
            e.printStackTrace();
        }
    }

    public String getDetailUrl() {
        return UrlUtil.builder()
                .setUrl("katalog.laporanprodukctr.detail")
                .setParam("id", this.id)
                .build();
    }

    public static List<ReportRiwayat> findByReportId(long report_id){
        List<ReportRiwayat> results = ReportRiwayat.find("report_id = ?", report_id).fetch();
        Report rpt = Report.find("id = ?", report_id).first();
        for(ReportRiwayat rw: results){
            if(rw.reporter_id != null){
                User usr = User.find("id = ?",rw.reporter_id).first();
                rw.reporter_name = usr.nama_lengkap;
            }else{
                rw.reporter_name = rpt.reporter_name;
            }

            if(rw.blb_id_content != null) {
                //BlobTable bt = BlobTable.find("blb_id_content = ?", rw.blb_id_content).first();
                BlobTable bt = BlobTable.findByBlobId(rw.blb_id_content);
                rw.url_download = bt==null ? null : bt.getDownloadUrl(null); // tambal sulam, seharusnya bt!=null
            }
        }
        return results;
    }
}
