package services;

import play.Logger;
import play.Play;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ManualDbConnect {
//    private static Connection connection;
//    public static void Connect() {
//        try {
//            Play.configuration = new Properties();
//            Play.applicationPath = new File(System.getProperty("application.path", "."));
//            Play.configuration.load(new FileInputStream(Play.applicationPath+"/conf/application.conf"));
//            String dbUrl = Play.configuration.getProperty("db.default.url");
//            String user = Play.configuration.getProperty("db.default.user");
//            String pass = Play.configuration.getProperty("db.default.pass");
//            final String mode = Play.configuration.getProperty("application.mode");
//            connection = DriverManager.getConnection(dbUrl, user, pass);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        //Logger.info("Connected:"+!connection.isClosed());
//    }
    public static Connection createNewConn(){
        try{
            Play.configuration = new Properties();
            Play.applicationPath = new File(System.getProperty("application.path", "."));
            Play.configuration.load(new FileInputStream(Play.applicationPath+"/conf/application.conf"));
            String dbUrl = Play.configuration.getProperty("db.default.url");
            String user = Play.configuration.getProperty("db.default.user");
            String pass = Play.configuration.getProperty("db.default.pass");
            final String mode = Play.configuration.getProperty("application.mode");
            Connection conn = DriverManager.getConnection(dbUrl, user, pass);
            return conn;
        } catch (IOException e) {
            Logger.error("Error file config:"+e.getMessage());
            e.printStackTrace();
        } catch (SQLException e) {
            Logger.error("error konektifitas db:"+e.getMessage());
            e.printStackTrace();
        }
        return null;
    }
}
