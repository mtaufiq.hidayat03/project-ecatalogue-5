package models.penyedia;

import ext.DatetimeBinder;
import models.BaseKatalogTable;
import models.common.AktifUser;
import models.penyedia.contracts.DistributorContract;
import models.permohonan.PembaruanStaging;
import models.purchasing.Paket;
import play.data.binding.As;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.*;

/**
 * @author HanusaCloud on 11/6/2017
 */
@Table(name = "penyedia_distributor")
public class PenyediaDistributor extends BaseKatalogTable implements DistributorContract {

    @Id
    public Long id;
    public Long penyedia_id;
    public Long user_id;
    public String nama_distributor;
    public String alamat;
    public String website;
    public String email;
    public String no_telp;
    public String no_fax;
    public String no_hp;
    public String kode_pos;
    public String npwp;
    public String pkp;
    public boolean active;
    public Long rkn_id;
    public Long minta_disetujui;
    public Long setuju_tolak_oleh;
    @As(binder = DatetimeBinder.class)
    public Date setuju_tolak_tanggal;
    public String setuju_tolak;
    public String setuju_tolak_alasan;

    @Transient
    public List<PenyediaDistributorRepresentatif> representatifs = new ArrayList<PenyediaDistributorRepresentatif>();

    @Transient
    public String nama_penyedia;

    @Transient
    public String username;

    public static PenyediaDistributor getByIdAndProviderId(Long id, Long providerId) {
        return find("id =? AND penyedia_id =?", id, providerId).first();
    }

    public static List<PenyediaDistributor> findByPenyedia(long penyedia_id){
        return find("penyedia_id = ? and active = ? and minta_disetujui = 0 and setuju_tolak like 'Setuju'",penyedia_id, AKTIF).fetch();
    }

    public static List<PenyediaDistributor> findByPenyediaExist(long user_id, long penyedia_id){
        return find("user_id = ? and active = ? and penyedia_id = ? and minta_disetujui = 0 and setuju_tolak like 'Setuju'",user_id, AKTIF, penyedia_id).fetch();
    }

    public static List<PenyediaDistributor> findByPenyediaAll(long penyedia_id){
        return find("penyedia_id = ? and active = ?",penyedia_id, AKTIF).fetch();
    }

    public static PenyediaDistributor findByUser(long user_id){
        return find("user_id = ? and active = ?",user_id, AKTIF).first();
    }

    public static PenyediaDistributor findByPenyediaUser(long penyedia_id, long user_id){
        return find("penyedia_id = ? and user_id = ?", penyedia_id, user_id).first();
    }

    public static PenyediaDistributor findByRknId(long rkn_id){
        return find("rkn_id = ? and active = ? limit 1", rkn_id, AKTIF).first();
    }

    public static int persetujuanDistributor(long distributor_id, String alasan, String setuju_tolak, long uid){
        String query = "update penyedia_distributor " +
                "set minta_disetujui = 0, " +
                "setuju_tolak_tanggal = ?, " +
                "setuju_tolak_alasan = ?, " +
                "setuju_tolak = ?, " +
                "setuju_tolak_oleh = ? " +
                "where id = ?";

        return Query.update(query, new Date(), alasan, setuju_tolak, uid, distributor_id);
    }

    public static void checkAndSaveRknIdByUser(Long userId, Long rknId){
        PenyediaDistributor distributor = findByUser(userId);
        if(distributor != null){
            distributor.rkn_id = rknId;
            distributor.active = true;
            distributor.save();
        }
    }

    public static PenyediaDistributor getUserDistributorById(Long id) {
        final String query = "SELECT pd.*, py.nama_penyedia, usr.user_name as username " +
                "FROM penyedia_distributor pd " +
                "LEFT JOIN penyedia py ON pd.penyedia_id = py.id " +
                "LEFT JOIN user usr ON pd.user_id = usr.id " +
                "WHERE pd.id = ? AND pd.active > 0 ";
        return Query.find(query, PenyediaDistributor.class, id).first();
    }

    public static PenyediaDistributor findDetailById(long id){
        String query = "select pd.nama_distributor, pd.alamat, pd.email, pd.website, pd.no_telp, pd.no_fax, pd.npwp, p.nama_penyedia, u.user_name as username " +
                "from penyedia_distributor pd join penyedia p on pd.penyedia_id = p.id join user u on pd.user_id = u.id " +
                "where pd.id = ?";

        PenyediaDistributor result = Query.find(query, PenyediaDistributor.class, id).first();

        List<PenyediaDistributorRepresentatif> representatifs = PenyediaDistributorRepresentatif.findByDistributor(id);
        result.representatifs = representatifs;

        return result;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setRepresentatives(List<PenyediaDistributorRepresentatif> list) {
        this.representatifs = list;
    }

    public boolean isUserIdEqual(Long user_id) {
        return this.user_id.equals(user_id);
    }

    public boolean isAllowToBeDeleted() {
        return Paket.getTotalPackageByDistributor(this.id) == 0;
    }

    public void softDelete() {
        this.active = false;
        setDeleted();
        save();
    }

    public static List<PembaruanStaging> getDistributorStagging(Long pmb_id, Long idDist){
        List<PembaruanStaging> pmbs = PembaruanStaging.getDistributorStaggingByPmbIdAndDistId(pmb_id, idDist);
        return pmbs;
    }

    public static String createDistributorPembaruanId(Long idPembaruan, Long idDistributor) {
        List<PembaruanStaging> list = getDistributorStagging(idPembaruan, idDistributor);

        StringBuilder sb = new StringBuilder("");


        for (PembaruanStaging stg : list) {
//            String url = "";
            Map<String, Object> ids = new HashMap<>();
//            ids.put("id", stg.id);
            sb.append(stg.id);
        }

        return sb.toString();
    }

    public void setFromEditStaging(PenyediaDistributor distributor){
        this.nama_distributor = distributor.nama_distributor;
        this.npwp = distributor.npwp;
        this.alamat = distributor.alamat;
        this.kode_pos = distributor.kode_pos;
        this.email = distributor.email;
        this.website = distributor.website;
        this.no_telp = distributor.no_telp;
        this.no_hp = distributor.no_hp;
        this.no_fax = distributor.no_fax;
        this.pkp = distributor.pkp;
        this.active = distributor.active;
        this.minta_disetujui = 0l;
        this.setuju_tolak = "Setuju";
        this.setuju_tolak_oleh = -99l;
        this.setuju_tolak_tanggal = new Date();
        this.setuju_tolak_alasan = "Approved by Permohonan Pembaruan";
    }

}
