package utils;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BaseFont;
import org.apache.commons.codec.binary.Base64;
import org.jsoup.Jsoup;
import org.xhtmlrenderer.pdf.ITextRenderer;
import play.Logger;
import play.Play;
import play.templates.Template;
import play.templates.TemplateLoader;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.Map;

/**
 * Created by rayhanfrds on 9/14/17.
 */
public class HtmlUtil {

    public synchronized static InputStream generatePDF(String content, Map<String, Object> params) {
        InputStream is = null;
        try {
            Template template = TemplateLoader.loadString("#{extends 'borderTemplate.html' /} " + content);
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ITextRenderer renderer = new ITextRenderer();
            String templateString = template.render(params);
            renderer.setDocumentFromString(templateString);
            renderer.layout();
            renderer.createPDF(os);
            renderer.finishPDF();
            is = new ByteArrayInputStream(os.toByteArray());
            os.close();
        } catch (DocumentException e) {
            Logger.error(e, "Document Exception %s", e.getLocalizedMessage());
        } catch (Exception e) {
            Logger.error(e, "IO Exception %s", e.getLocalizedMessage());
        }
        return is;
    }

    public static String renderHtml(String content, Map<String, Object> params) {
        Template template = TemplateLoader.loadString("#{extends 'borderTemplate.html' /} " + content);

        String templateString = template.render(params);
        return templateString;
    }

    public static String getTerbilang(String angka) {
        angka = angka.substring(0, angka.indexOf(","));
        String str = angka.replaceAll("\\D+", "");
        Long bil = new Long(str);
        return angkaToTerbilang(bil);
    }

    public static String getTanggalSekarang(String tanggal) {
        String[] monthName = {"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli",
                "Agustus", "September", "Oktober", "November", "Desember"};

        Calendar cal = Calendar.getInstance();
        String month = monthName[cal.get(Calendar.MONTH)];

        String tglSekarang = cal.get(Calendar.DATE) + " " + month + " " + cal.get(Calendar.YEAR);


//		DateTimeFormatter tgl = DateTimeFormatter.ofPattern("dd" + month + "yyyy");
//		LocalDateTime now = LocalDateTime.now();
        return tglSekarang;
    }

    static String[] huruf = {"", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"};

    public static String angkaToTerbilang(Long angka) {
        if (angka < 12)
            return huruf[angka.intValue()];
        if (angka >= 12 && angka <= 19)
            return huruf[angka.intValue() % 10] + " Belas";
        if (angka >= 20 && angka <= 99)
            return angkaToTerbilang(angka / 10) + " Puluh " + huruf[angka.intValue() % 10];
        if (angka >= 100 && angka <= 199)
            return "Seratus " + angkaToTerbilang(angka % 100);
        if (angka >= 200 && angka <= 999)
            return angkaToTerbilang(angka / 100) + " Ratus " + angkaToTerbilang(angka % 100);
        if (angka >= 1000 && angka <= 1999)
            return "Seribu " + angkaToTerbilang(angka % 1000);
        if (angka >= 2000 && angka <= 999999)
            return angkaToTerbilang(angka / 1000) + " Ribu " + angkaToTerbilang(angka % 1000);
        if (angka >= 1000000 && angka <= 999999999)
            return angkaToTerbilang(angka / 1000000) + " Juta " + angkaToTerbilang(angka % 1000000);
        if (angka >= 1000000000 && angka <= 999999999999L)
            return angkaToTerbilang(angka / 1000000000) + " Milyar " + angkaToTerbilang(angka % 1000000000);
        if (angka >= 1000000000000L && angka <= 999999999999999L)
            return angkaToTerbilang(angka / 1000000000000L) + " Triliun " + angkaToTerbilang(angka % 1000000000000L);
        if (angka >= 1000000000000000L && angka <= 999999999999999999L)
            return angkaToTerbilang(angka / 1000000000000000L) + " Quadrilyun " + angkaToTerbilang(angka % 1000000000000000L);
        return "";
    }

    public synchronized static InputStream generatePDFPesanan(Map<String, Object> params) {
        InputStream is = null;
        try {

            Template template = TemplateLoader.loadString("#{extends 'purchasing/PaketCtr/cetakPesanan.html' /} ");
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ITextRenderer renderer = new ITextRenderer();
            String pathFont = Play.applicationPath + "/public/fonts/calibri.ttf";
            //Path path = FileSystems.getDefault().getPath("utils/calibri.ttf");
            Logger.debug(pathFont);
            renderer.getFontResolver().addFont(
                    //path.toString(),
                    pathFont,
                    BaseFont.IDENTITY_H,
                    BaseFont.EMBEDDED
            );
            String templateString = template.render(params);
            renderer.setDocumentFromString(templateString);
            renderer.layout();
            renderer.createPDF(os);
            renderer.finishPDF();
            is = new ByteArrayInputStream(os.toByteArray());
            os.close();
        } catch (DocumentException e) {
            Logger.error(e, "Document Exception %s", e.getLocalizedMessage());
        } catch (Exception e) {
            Logger.error(e, "IO Exception %s", e.getLocalizedMessage());
        }
        return is;
    }

    public synchronized static InputStream generatePDFByHtml(Map<String, Object> params, String templateHtml) {
        InputStream is = null;
        try {

            Template template = TemplateLoader.loadString("#{extends '" + templateHtml + "' /} ");
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ITextRenderer renderer = new ITextRenderer();
            String pathFont = Play.applicationPath + "/public/fonts/calibri.ttf";
            Logger.debug(pathFont);
            renderer.getFontResolver().addFont(
                    //path.toString(),
                    pathFont,
                    BaseFont.IDENTITY_H,
                    BaseFont.EMBEDDED
            );
            String templateString = template.render(params);
            renderer.setDocumentFromString(templateString);
            renderer.layout();
            renderer.createPDF(os);
            renderer.finishPDF();
            is = new ByteArrayInputStream(os.toByteArray());
            os.close();
        } catch (DocumentException e) {
            Logger.error(e, "Document Exception %s", e.getLocalizedMessage());
        } catch (Exception e) {
            Logger.error(e, "IO Exception %s", e.getLocalizedMessage());
        }
        return is;
    }

    public static String decodeFromTinymce(String textSource) {
        //decode
        //replace default <p> pada tinymce
        //pastikan tinymce.getcontent
        String decodedString = textSource;
        try {
            String pureText = Jsoup.parse(textSource).text();
            byte[] decodedBytes = Base64.decodeBase64(pureText);
            decodedString = new String(decodedBytes);
        } catch (Exception e) {
        }
        return decodedString;
    }

    public static boolean fileExistsAtUrl(String URLName) {
        try {
            HttpURLConnection.setFollowRedirects(false);
            // note : you may also need
            //        HttpURLConnection.setInstanceFollowRedirects(false)
            HttpURLConnection con =
                    (HttpURLConnection) new URL(URLName).openConnection();
            con.setRequestMethod("HEAD");
            Logger.info(con.getResponseCode() + " RESPONSE CODE");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
