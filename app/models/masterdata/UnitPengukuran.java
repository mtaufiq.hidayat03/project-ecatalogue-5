package models.masterdata;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;


@Table(name = "unit_pengukuran")
public class UnitPengukuran extends BaseKatalogTable {

	@Id
	public Long id;
	public String nama_unit_pengukuran;
	public String deskripsi;
	public boolean active;

	public static final String EXCEL_SHEET_NAME = "UNIT PENGUKURAN";
	public static final String EXCEL_FIELD_NAMA = "Unit Pengukuran";
	public static final String EXCEL_FIELD_DESKRIPSI = "Deskripsi";

	public static List<UnitPengukuran> findAllActive(){

		return UnitPengukuran.find("active = 1").fetch();
	}

	public static void deleteById(long id){
		UnitPengukuran up = UnitPengukuran.findById(id);
		up.active = false;
		up.save();
	}

	public static Integer getTotalRelationOnProduct(long id) {
		final String query = "SELECT COUNT(*) as total FROM produk WHERE unit_pengukuran_id =? AND active > 0";
		return Query.find(query, Integer.class, id).first();
	}

	public void softDelete() {
		this.active = false;
		setDeleted();
		save();
	}

	public void setAktif() {
		this.active = true;
		prePersist();
		save();
	}

	public void setNonAktif() {
		this.active = false;
		prePersist();
		save();
	}

}
