CREATE TABLE `paket_pembayaran_lampiran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paket_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `file_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `original_file_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_size` bigint(20) NOT NULL DEFAULT '0',
  `file_sub_location` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `deskripsi` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `posisi_file` int(11) NOT NULL DEFAULT '1',
  `doc_attachment_id` int(11) DEFAULT NULL,
  `item_param` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` int(1) NOT NULL DEFAULT '1',
  `active` int(11) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `modified_date` timestamp NULL DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `deleted_date` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ppenlamp_ppi` (`model_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

ALTER TABLE `paket_penerimaan` ADD COLUMN `paket_pengiriman_id` int(11) NULL;
ALTER TABLE `paket_penerimaan` ADD COLUMN `paket_pembayaran_id` int(11) NULL;
ALTER TABLE `paket_pengiriman` ADD COLUMN `paket_penerimaan_pembayaran_id` int(11) NULL;

ALTER TABLE `paket_pembayaran` ADD COLUMN `transaction_type` int(1) NOT NULL DEFAULT 1;
