package models.lpse;

import java.util.Date;

/**
 * Created by dadang on 12/8/17.
 */
public class UserFromLpse {
	public String username;
	public String name;
	public String role;
	public Long ppsId;
	public boolean isLatihan;
	public Date tanggalLogin;


	public UserFromLpse(){

	}

	public UserFromLpse(String username, String name, String role, Long ppsId, boolean isLatihan, Date tanggalLogin){
		this.username = username;
		this.name = name;
		this.role = role;
		this.ppsId = ppsId;
		this.isLatihan = isLatihan;
		this.tanggalLogin = tanggalLogin;
	}
}
