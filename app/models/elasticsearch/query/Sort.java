package models.elasticsearch.query;

import com.google.gson.JsonObject;
import org.apache.http.util.TextUtils;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class Sort {

    public static final String DESC = "desc";
    public static final String ASC = "asc";

    private String field = "";
    private String order = "asc";
    private String nestedPath;

    public Sort(String field, String order) {
        this.field = field;
        this.order = order;
    }

    public Sort nestedPath(String nestedPath) {
        this.nestedPath = nestedPath;
        return this;
    }

    public JsonObject getSortObject() {
        JsonObject result = new JsonObject();
        result.addProperty(field, order);
        return result;
    }

    public JsonObject build() {
        JsonObject orderJson = new JsonObject();
        orderJson.addProperty("order", order);
        if (!TextUtils.isEmpty(nestedPath)) {
            orderJson.addProperty("nested_path", nestedPath);
        }
        JsonObject model = new JsonObject();
        model.add(field, orderJson);
        return model;
    }

}
