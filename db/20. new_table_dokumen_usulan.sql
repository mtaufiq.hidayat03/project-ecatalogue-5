CREATE TABLE `dokumen_usulan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usulan_id` int(11) DEFAULT NULL,
  `dok_judul` varchar(255) DEFAULT NULL,
  `dok_hash` varchar(255) DEFAULT NULL,
  `dok_signature` varchar(255) DEFAULT NULL,
  `dok_id_attachment` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `modified_date` timestamp NULL,
  `modified_by` int(11) NULL,
  `deleted_date` timestamp NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `audittype` varchar(255) DEFAULT NULL,
  `auditupdate` timestamp NULL,
  `audituser` varchar(255) DEFAULT NULL,
  `dok_label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

