INSERT into user_role_item (user_role_grup_id, role_item_id, active, created_date, created_by, modified_date, modified_by)
    VALUES
      (2, (select id from role_item where nama_role = "comUsulan"), 1, curdate(), -99, curdate(), -99),
      (2, (select id from role_item where nama_role = "comPenawaran"), 1, curdate(), -99, curdate(), -99),
      (2, (select id from role_item where nama_role = "comPenawaranAdd"), 1, curdate(), -99, curdate(), -99),
      (2, (select id from role_item where nama_role = "comPenawaranEdit"), 1, curdate(), -99, curdate(), -99),
      (2, (select id from role_item where nama_role = "comPenawaranDelete"), 1, curdate(), -99, curdate(), -99),
      (2, (select id from role_item where nama_role = "comPenawaranProdukList"), 1, curdate(), -99, curdate(), -99),
      (2, (select id from role_item where nama_role = "comPenawaranProdukAdd"), 1, curdate(), -99, curdate(), -99);
