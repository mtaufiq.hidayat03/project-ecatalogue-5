package ext;

import groovy.lang.Closure;
import models.entity.Menu;
import models.katalog.Komoditas;
import models.masterdata.Kldi;
import play.i18n.Lang;
import play.i18n.Messages;
import play.mvc.Router;
import play.templates.FastTags;
import play.templates.GroovyTemplate;
import utils.KatalogUtils;
import utils.UrlUtil;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PublicMenu extends FastTags {
    public static void _subKomoditasListItemUmum(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        List<Komoditas> komoditasList = Komoditas.findAllActive();
        List<Komoditas> kldiSektoralTemp = Komoditas.findSektoralKldi();
        List<Komoditas> kldiLokalTemp = Komoditas.findLokalKldi();
        List<Kldi> kldiLokal = Kldi.findLokal();
        List<Kldi> Sektoral = Kldi.findSektoral();

        StringBuilder content = new StringBuilder();

        StringBuilder contentNasional = new StringBuilder().append("<div class=\"subkomoditas-list-item\" id=\"nasional-item\">");
        StringBuilder contentLokal = new StringBuilder().append("<div class=\"subkomoditas-list-item\" id=\"lokal-item\">");
        StringBuilder contentSektoral = new StringBuilder().append("<div class=\"subkomoditas-list-item\" id=\"sektoral-item\">");

        for (Komoditas kldis : kldiSektoralTemp){
            for (Kldi k : Sektoral){
//				Map<String,Object> params = new HashMap<String, Object>();
//				params.put("id",k.id);
//				String url = Router.getFullUrl("PublikCtr.komoditasKldiIndex",params);

                String lang = !Lang.get().isEmpty() ? Lang.get() : "id";
                UrlUtil.Builder builder = UrlUtil.builder()
                        .setUrl("PublikCtr.komoditasKldiIndex")
                        .setParam("language", lang)
                        //.setParam("komoditas_slug", KatalogUtils.generateSlug(komoditas.nama_komoditas))
                        .setParam("id", k.id);
                String url = builder.build();
                //Logger.info("urlsektoral -> :"+url);
                if (kldis.kldi_id.equalsIgnoreCase(k.id)){
                    String result = "<div class=\"komoditas-list-item\"><a class=\"komoditas-item-link\" href=\""+url+"?first=true"+"\">"+k.nama+"</a></div>";
                    contentSektoral.append(result);
                }
            }

        }

        for (Komoditas kldil : kldiLokalTemp){
            for(Kldi k : kldiLokal){
                if(kldil.kldi_id.equalsIgnoreCase(k.id)){
//					Map<String,Object> params = new HashMap<String, Object>();
//					params.put("id",k.id);
//					String url = Router.getFullUrl("PublikCtr.komoditasKldiIndex",params);

                    String lang = !Lang.get().isEmpty() ? Lang.get() : "id";
                    UrlUtil.Builder builder = UrlUtil.builder()
                            .setUrl("PublikCtr.komoditasKldiIndex")
                            .setParam("language", lang)
                            //.setParam("komoditas_slug", KatalogUtils.generateSlug(komoditas.nama_komoditas))
                            .setParam("id", k.id);
                    String url = builder.build();
                    //Logger.info("urllokal -> :"+url);
                    String result = "<div class=\"komoditas-list-item\"><a class=\"komoditas-item-link\" href=\""+url+"?first=true"+"\">"+k.nama+"</a></div>";
                    contentLokal.append(result);
                }
            }
        }

        for (Komoditas komoditas : komoditasList){
            Map<String,Object> params = new HashMap<String, Object>();

//			params.put("language", Lang.get().isEmpty() ? "id" : Lang.get());
//			params.put("komoditas_slug", KatalogUtils.generateSlug(komoditas.nama_komoditas));
//			params.put("id",komoditas.id);
//			String url = Router.getFullUrl("katalog.ProdukCtr.listProduk",params);

            String lang = !Lang.get().isEmpty() ? Lang.get() : "id";
            UrlUtil.Builder builder = UrlUtil.builder()
                    .setUrl("katalog.ProdukCtr.listProduk")
                    .setParam("language", lang)
                    .setParam("komoditas_slug", KatalogUtils.generateSlug(komoditas.nama_komoditas))
                    .setParam("id", komoditas.id);
            String url = builder.build();
            String result = "<div class=\"komoditas-list-item\"><a class=\"komoditas-item-link\" href=\""+url+"?first=true"+"\">"+komoditas.nama_komoditas+"</a></div>";

            if(komoditas.komoditas_kategori_id == Komoditas.KOMODITAS_LOKAL){//				contentLokal.append(result);

            }else if(komoditas.komoditas_kategori_id == Komoditas.KOMODITAS_SEKTORAL){
//				contentSektoral.append(result);
            }else {
                contentNasional.append(result);
            }

        }

        contentNasional.append("</div>");
        contentLokal.append("</div>");
        contentSektoral.append("</div>");

        content.append(contentNasional);
        content.append(contentLokal);
        content.append(contentSektoral);

        out.print(content.toString());
    }

    public static void _desktopmenuPublic(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
//		Request request = Request.current();
        StringBuilder content = new StringBuilder();

        String openUl = "<ul class=\"nav navbar-nav\">";

        String openMenuList = "<li class=\"dropdown\">";

        String openAnchorMenu = "<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
        String closeAnchorMenu = " <span class=\"caret\"></span></a>";

        String menuDropDown = "<ul class=\"dropdown-menu\">" +
                "<li><div class=\"container-fluid\"><div class=\"row\">";

        String closeMenuList = "</div></div></li>" +
                "</ul></li>";

        content.append(openUl);

        //HOME
        content.append("<li><a href=\"").append(Menu.HOMEPUBLIC.page).append("\">").append(Messages.get(Menu.HOME.caption)).append("</a></li>");

        //Pengumuman
        content.append("<li><a href=\"").append(Menu.PENGUMUMAN.page).append("\">").append(Messages.get(Menu.PENGUMUMAN.caption)).append("</a></li>");

        //Berita
        content.append("<li><a href=\"").append(Menu.BERITA.page).append("\">").append(Messages.get(Menu.BERITA.caption)).append("</a></li>");

        //Unduh
        content.append("<li><a href=\"").append(Menu.UNDUH.page).append("\">").append(Messages.get(Menu.UNDUH.caption)).append("</a></li>");

        //Faq
        content.append("<li><a href=\"").append(Menu.FAQ.page).append("\">").append(Messages.get(Menu.FAQ.caption)).append("</a></li>");

        //Hubungi Kami
        content.append("<li><a href=\"").append(Menu.HUB.page).append("\">").append(Messages.get(Menu.HUB.caption)).append("</a></li>");

        //Katalog Lokal/Sektoral
        content.append("<li><a href=\"").append(Menu.KATALOG_LOKAL_SEKTORAL.page).append("\">").append(Messages.get(Menu.KATALOG_LOKAL_SEKTORAL.caption)).append("</a></li>");

        content.append("</ul>");



        out.print(content.toString());
    }

    public static void _mobilemenuPublic(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {

        StringBuilder content = new StringBuilder();

        String komoditasMenu = "komoditasMenu";
        String komoditasManagementMenu = "komoditasManagementMenu";
        String prakatalogMenu = "prakatalogMenu";
        String katalogMenu = "katalogMenu";
        String userManagementMenu = "userManagementMenu";
        String masterDataMenu = "masterDataMenu";
        String cmsMenu = "cmsMenu";
        String apiMenu = "apiMenu";
        String profilMenu = "profilMenu";

        String changeLangUrl = Router.reverse("PublikCtr.changeLang").url;

        content.append("<ul>");

        //Beranda
        content.append("<li><a href=\"").append(Menu.HOMEPUBLIC.page).append("\">").append(Messages.get(Menu.HOME.caption)).append("</a></li>");

        content.append("<li>");
        content.append("<a href=\"#"+komoditasMenu+"\" class=\"collapsed\" data-toggle=\"collapse\" aria-expanded=\"false\" aria-controls=\""+komoditasMenu+"\">");
        content.append(Messages.get("komoditas.label"));
        content.append("</a>");

        content.append("<ul class=\"collapse\" id=\""+komoditasMenu+"\">");
        for(Menu menu_komoditas : Menu.MENU_KOMODITAS) {
            content.append("<li>");
            content.append("<a href=\"").append(menu_komoditas.page).append("\"><i class=\"fa fa-angle-double-right\"></i> ")
                    .append(Messages.get(menu_komoditas.caption)).append("</a>");
        }
        content.append("</ul></li>");

        //Pengumuman
        content.append("<li><a href=\"").append(Menu.PENGUMUMAN.page).append("\">").append(Messages.get(Menu.PENGUMUMAN.caption)).append("</a></li>");

        //Berita
        content.append("<li><a href=\"").append(Menu.BERITA.page).append("\">").append(Messages.get(Menu.BERITA.caption)).append("</a></li>");

        //Unduh
        content.append("<li><a href=\"").append(Menu.UNDUH.page).append("\">").append(Messages.get(Menu.UNDUH.caption)).append("</a></li>");

        //Faq
        content.append("<li><a href=\"").append(Menu.FAQ.page).append("\">").append(Messages.get(Menu.FAQ.caption)).append("</a></li>");

        //Hubungi Kami
        content.append("<li><a href=\"").append(Menu.HUB.page).append("\">").append(Messages.get(Menu.HUB.caption)).append("</a></li>");

        //Katalog Lokal/Sektoral
        content.append("<li><a href=\"").append(Menu.KATALOG_LOKAL_SEKTORAL.page).append("\">").append(Messages.get(Menu.KATALOG_LOKAL_SEKTORAL.caption)).append("</a></li>");

        //Login
        content.append("<li><a href=\"").append(Menu.LOGIN.page).append("\">").append(Messages.get(Menu.LOGIN.caption)).append("</a></li>");

        content.append("</ul>");
        out.print(content.toString());
    }
}
