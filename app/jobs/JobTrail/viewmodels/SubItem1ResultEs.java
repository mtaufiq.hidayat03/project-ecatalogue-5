package jobs.JobTrail.viewmodels;

public class SubItem1ResultEs {
    public String _index;
    public String _type;
    public String _id;
    public String _version;
    public SubItem2ResultEs _shards;
    //status
    //409 -> document_already_exists_exception
    //201 -> successful
    public int status = 0;
    public SubItem2ErResult error;
}
