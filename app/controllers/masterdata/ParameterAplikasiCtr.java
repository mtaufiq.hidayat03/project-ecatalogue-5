package controllers.masterdata;

import controllers.BaseController;
import controllers.security.AllowAccess;
import ext.DatetimeBinder;
import models.masterdata.ParameterAplikasi;
import models.secman.Acl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ParameterAplikasiCtr extends BaseController {

    public static void index(){
        render();
    }

    public static void edit(long id) {
        ParameterAplikasi parameter = ParameterAplikasi.findById(id);
        render(parameter);
    }

    public static void editSubmit(ParameterAplikasi parameter) {
        validation.valid(parameter);
        if(validation.hasErrors()){
//            params.flash();
//            flash.error("");
            edit(parameter.id);
        }else{
            parameter.active = true;
            parameter.save();
        }

        index();
    }
}
