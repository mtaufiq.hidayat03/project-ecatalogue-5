/**
 * 
 */
package models.user;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.google.gson.Gson;

/**
 * @author AndikYulianto@yahoo.com
 
{"access_token":"6861d318-0914-488d-9830-c5d7309765b5",
"token_type":"bearer",
"expires_in":1496824594922,
"refresh_token":false,
"scope":"read_write",
"info":
	{"user_id":1,
	"role_codes":["PPK"],
	"jenis_kelamin":"P",
	"user_name":"andik@lkpp.go.id",
	"nama":"Andik Yulianto",
	"alamat":"Jalan Warga Gg. H HUsin 58A Pasar Minggu",
	"nip":"081315698939",
	"nik":"081315698939",
	"npwp":"11.111.111.1-111.111",
	"tanggal_lahir":"Sep 22,
	 1978",
	"email":"andik@lkpp.go.id",
	"phone":"081315698939",
	"is_pns":false,
	"tanggal_daftar":"Apr 7,
	 2016 10:17:55 AM",
	"status_user":"AKTIF"}
}
 
 */

public class CentrumUser implements Serializable{

	public class LoginInfo
	{
		public Long user_id;
		public String[] role_codes;
		public String user_name;
		public String nama;
		public String jenis_kelamin;
		public String alamat;
		public String NIP;
		public String NPWP;
		public String email;
		public String phone;
		public String is_pns;
		public Date tanggal_daftar;
		public String status_user;
		public Long rkn_id;
		
		public String toString()
		{
			return ToStringBuilder.reflectionToString(this, ToStringStyle.SIMPLE_STYLE);
		}
	}
	
	public LoginInfo info;
	public String access_token;
	public String token_type;
	public Long expires_in;
	public boolean refresh_token;
	public String scope;
	
	public static CentrumUser getInstance(String str)
	{
		return new Gson().fromJson(str, CentrumUser.class);
	}
	
	
	
}
