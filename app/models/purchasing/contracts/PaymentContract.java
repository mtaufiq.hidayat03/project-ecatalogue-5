package models.purchasing.contracts;

import com.google.gson.Gson;
import models.purchasing.pembayaran.AcceptanceItem;
import models.purchasing.pembayaran.DeliveryItem;
import models.purchasing.pembayaran.PembayaranLampiran;
import models.purchasing.pembayaran.form.AcceptanceItemJson;
import repositories.paket.PaymentRepository;
import utils.UrlUtil;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 11/2/2017
 */
public interface PaymentContract {

    void setAcceptancePayment(List<AcceptanceItem> list);

    void setDeliveryPayment(List<DeliveryItem> list);

    void setAttachment(PembayaranLampiran model);

    boolean isPayment();

    Long getPackageId();

    Long getId();

    String getAttachmentDownloadUrl();

    List<AcceptanceItem> getAcceptances();

    List<DeliveryItem> getDeliveryItem();

    Integer getTransactionType();

    default void withAttachment() {
        if (getId() != null) {
            setAttachment(PaymentRepository.getAttachment(getId(), getTransactionType()));
        }
    }

    default void withAcceptancePayments() {
        if (getId() != null && getPackageId() != null) {
            setAcceptancePayment(PaymentRepository.getAcceptanceByPaymentId(getPackageId(), getId()));
        }
    }

    default void withDeliveries() {
        if (getId() != null && getPackageId() != null) {
            setDeliveryPayment(PaymentRepository.getDeliveredForPaymentVerification(getPackageId(), getId()));
        }
    }

    default String getAcceptanceJson() {
        if (getAcceptances() != null && !getAcceptances().isEmpty()) {
            Map<Long, AcceptanceItemJson> result = getAcceptances().stream()
                    .map(a -> new AcceptanceItemJson(a))
                    .collect(Collectors.toMap(AcceptanceItemJson::getId, a -> a));
            return new Gson().toJson(result);
        }
        return "{}";
    }

    default String generateUrl(String path) {
        return UrlUtil.builder()
                .setUrl(path)
                .setParam("id", getId())
                .setParam("paket_id", getPackageId())
                .build();
    }

}
