package jobs;

import models.katalog.ProdukHarga;
import models.katalog.ProdukHargaNasional;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import play.jobs.On;
import play.jobs.OnApplicationStart;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by dadang on 11/28/17.
 */
//@On("0 18 9 30 NOV ? 2017")
//@OnApplicationStart(async = true)
@NoTransaction
public class MigrasiHargaJob extends Job {

	public void doJob() throws Exception {

		ProdukHarga.migrasiHargaNasional();
	}
}
