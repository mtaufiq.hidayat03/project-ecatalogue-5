package unit;

import features.komoditas.KomoditasService;
import features.komoditas.repositories.KomoditasRepositoryContract;
import models.katalog.Komoditas;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;


/**
 * @author HanusaCloud on 3/26/2020 1:10 PM
 */
@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class GroupCommodityByNameTest {

    @InjectMocks
    KomoditasService service = new KomoditasService();
    @Mock
    KomoditasRepositoryContract repository;
    List<Komoditas> commodities;

    @BeforeEach
    public void setUp() {
        commodities = Arrays.asList(
                generateKomoditas("",1L),
                generateKomoditas("",5L),
                generateKomoditas("",6L),
                generateKomoditas("",2L),
                generateKomoditas("", 9L)
        );
    }

    private static Komoditas generateKomoditas(String name, Long category) {
        Komoditas komoditas = new Komoditas();
        komoditas.nama_komoditas = name;
        komoditas.komoditas_kategori_id = category;
        return komoditas;
    }

    private void setMocks() {
        when(repository.getUmkms()).thenReturn(Arrays.asList(
                generateKomoditas("TEST UMKM", 8L),
                generateKomoditas("TEST UMKM 2", 8L)
        ));
    }

    @Test
    public void shouldBeAbleToGroupCommoditiesByCategoryName() {
        setMocks();
        assertEquals(2, service.groupByCategory(commodities).get("Nasional").size());
    }

    @Test
    public void shouldReturn5TotalGroup() {
        setMocks();
        assertEquals(5, service.groupByCategory(commodities).size());
    }

    @Test
    public void shouldReturnOneItemInsideUmkm() {
        setMocks();
        assertEquals(2, service.groupByCategory(commodities).get("UKM").size());
    }

    @Test
    public void shouldNotReturnUMKMIfArrayEmpty() {
        when(repository.getUmkms()).thenReturn(new ArrayList<>());
        assertFalse(service.groupByCategory(commodities).containsKey("UKM"));
    }

}
