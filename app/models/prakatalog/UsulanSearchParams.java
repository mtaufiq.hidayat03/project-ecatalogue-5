package models.prakatalog;

import com.google.gson.reflect.TypeToken;
import ext.contracts.ParamAble;
import models.jcommon.util.CommonUtil;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.TextUtils;
import play.mvc.Scope;
import utils.DateTimeUtils;

import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author HanusaCloud on 12/18/2017
 */
public class UsulanSearchParams implements ParamAble {

    public int page = 0;
    public String keyword;
    public String startDate;
    public String finishDate;
    public String commoditiesJson;
    public String decodedCommodities = "[]";
    public List<String> commodities = new ArrayList<>();

    public UsulanSearchParams(Scope.Params params) {
        this.keyword = params.get("keyword");
        this.commoditiesJson = params.get("commodities");
        this.commodities = getCommodities();
        this.startDate = params.get("start");
        this.finishDate = params.get("finish");
        this.page = getCurrentPage(params);
    }

    private List<String> getCommodities() {
        String jsonSet = this.commoditiesJson;
        List<String> cats = new ArrayList<>();
        try {
            if (!TextUtils.isEmpty(jsonSet)) {
                this.decodedCommodities = URLDecoder.decode(jsonSet);
                Type listType = new TypeToken<HashMap<String, String>>() {
                }.getType();
                HashMap<String, String> map = CommonUtil.fromJson(this.decodedCommodities, listType);
                cats.addAll(map.keySet());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cats;
    }

    public String getIds() {
        StringBuilder sb = new StringBuilder();
        if (!commodities.isEmpty()) {
            String glue = "";
            for (String s : commodities) {
                sb.append(glue).append("'").append(s).append("'");
                glue = ",";
            }
        }
        return sb.toString();
    }

    public String parseStartDate() {
        return DateTimeUtils.parseDdMmYyyyToStandardDate(startDate);
    }

    public String parseFinishDate() {
        return DateTimeUtils.parseDdMmYyyyToStandardDate(finishDate);
    }

    public String getKeyword() {
        return StringEscapeUtils.escapeSql(keyword);
    }

    @Override
    public List<BasicNameValuePair> getParams() {
        List<BasicNameValuePair> parameters = new ArrayList<>();
        parameters.add(new BasicNameValuePair("keyword", keyword != null ? keyword : ""));
        parameters.add(new BasicNameValuePair("start", startDate != null ? startDate : ""));
        parameters.add(new BasicNameValuePair("finish", finishDate != null ? finishDate : ""));
        parameters.add(new BasicNameValuePair("commodities", !TextUtils.isEmpty(commoditiesJson) ? commoditiesJson : ""));
        return parameters;
    }

    @Override
    public int getPage() {
        return this.page;
    }

    public String getCommoditiesJson() {
        try {
            return !TextUtils.isEmpty(commoditiesJson) ? commoditiesJson : URLEncoder.encode("{}", "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

}
