package models.purchasing.status;

import models.BaseKatalogTable;
import models.common.AktifUser;
import models.purchasing.Paket;
import models.purchasing.contracts.PackageStatusContract;
import models.purchasing.negosiasi.PaketProdukHeader;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import utils.DateTimeUtils;

import javax.persistence.Transient;

import static constants.paket.PaketStatusConstant.*;
import static controllers.BaseController.getAktifUser;


/**
 * @author HanusaCloud on 11/7/2017
 */
//@CacheMerdeka(duration="10s")
@Table(name = "paket_status")
public class PaketStatus extends BaseKatalogTable implements PackageStatusContract {

    @Id
    public Long id;
    public Long paket_id;
    public Long nego_header_id;
    public String status_negosiasi;
    public String status_paket;
    public String from_status;
    public String to_status;
    public String deskripsi;
    public boolean di_tolak;

    public static final String DRAFT = "draft";
    public static final String STATUS_PERSIAPAN = "persiapan";
    public static final String STATUS_NEGOSIASI_PERLU_NEGO = "negosiasi";

    // status paket
    public static final String STATUS_PAKET_PERSIAPAN = "persiapan";
    public static final String STATUS_PAKET_PANITIA_SETUJU = "panitia_setuju";
    public static final String STATUS_PAKET_PENYEDIA_SETUJU = "penyedia_setuju";
    public static final String STATUS_PAKET_PPK_SETUJU = "ppk_setuju";
    public static final String STATUS_PAKET_PPK_SETUJU_BARU = "ppk_setuju_baru"; //status apabila yg buat paket adalah ppk, status utk setuju negosiasi
    public static final String STATUS_PAKET_PENYEDIA_TOLAK = "penyedia_tolak";
    public static final String STATUS_PAKET_PPK_TOLAK = "ppk_tolak";
    public static final String STATUS_PAKET_PROSES_KIRIM = "proses_kirim";
    public static final String STATUS_PAKET_PAKET_SELESAI = "paket_selesai";
    public static final String STATUS_PAKET_BATAL = "batal";

    // status negosiasi
    public static final String STATUS_NEGOSIASI_NEGOSIASI = "negosiasi";
    public static final String STATUS_NEGOSIASI_SEPAKAT = "sepakat";

    // from / to status
    public static final String STATUS_DISTRIBUTOR = "distributor";
    public static final String STATUS_PPK = "ppk";
    public static final String STATUS_PANITIA = "pejabat pengadaan";
    public static final String STATUS_PANITIA_PPK = "ppk";
    public static final String STATUS_PENYEDIA = "penyedia";
    public static final String STATUS_PANITIA_PAKET = "panitia";
    public static final String STATUS_ADMIN = "admin";
    public static final String STATUS_ADMIN_LOKAL = "admin lokal sektoral";

    @Transient
    public PaketProdukHeader header;

    //paket status cache
    //@CacheMerdeka(duration="30s")
    //@CacheMerdeka(duration="10s")
    public static PaketStatus getLatestStatusByPackageId(Long id) {
        return find("paket_id =? AND active =?", id, AKTIF).first();
    }

    public void updateStatusToDelivery() {
        if (!this.from_status.equalsIgnoreCase(USER_DISTRIBUTOR)
                && !this.to_status.equalsIgnoreCase(USER_PPK)) {
            this.status_paket = STATUS_DELIVERY;
            this.from_status = USER_DISTRIBUTOR;
            this.to_status = USER_PPK;
            save();
        }
    }

    public void updateStatusToFinish() {
        if (this.from_status.equalsIgnoreCase(USER_DISTRIBUTOR)
                && this.to_status.equalsIgnoreCase(USER_PPK)) {
            this.status_paket = STATUS_FINISHED;
            this.from_status = USER_PPK;
            this.to_status = USER_PPK;
            save();
        }
    }

    public void updateStatusCancel() {
            this.status_paket = STATUS_CANCEL;
        Logger.info("status " +this.status_paket);
            save();
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public Long getPackageId() {
        return this.paket_id;
    }

    @Override
    public String getCurrentPackageStatus() {
        return this.status_paket;
    }

    @Override
    public String getCurrentNegotiationStatus() {
        return this.status_negosiasi;
    }

    @Override
    public String getTo() {
        return this.to_status;
    }

    @Override
    public String getFrom() {
        return this.from_status;
    }

    @Override
    public PaketProdukHeader getLatestHeader() {
        return this.header;
    }

    @Override
    public Integer getModifiedBy() {
        return this.modified_by;
    }

    @Override
    public void setLatestNegotiationHeader(PaketProdukHeader model) {
        this.header = model;
    }

    public String getReadableModifiedDate() {
        return DateTimeUtils.parseToReadableDate(this.modified_date);
    }

    public String getDetailStatus(){
        StringBuilder builder = new StringBuilder(status_paket);
        return builder.append("-").append(from_status).append("-").append(to_status).toString();
    }

    public void setStatusUsingFrom(boolean isApproved) {
        switch (from_status) {
            case USER_PANITIA:
                this.status_paket = isApproved
                        ? STATUS_PANITIA_APPROVED
                        : STATUS_PANITIA_REJECTED;
                break;
            case USER_PPK:
                this.status_paket = isApproved
                        ? STATUS_PPK_APPROVED
                        : STATUS_PPK_REJECTED;
                break;
            case USER_PROVIDER:
                this.status_paket = isApproved
                        ? STATUS_PROVIDER_APPROVED
                        : STATUS_PROVIDER_REJECTED;
                break;
        }
    }

    public boolean allApproved() {
        return header != null && header.isBuyerApproved() && header.isProviderApproved();
    }

    public boolean isNegotiationBuyerApproved() {
        return header != null && header.isBuyerApproved();
    }

    public boolean isNegotiationProviderApproved() {
        return header != null && header.isProviderApproved();
    }

    public String getToEmpty() {
        AktifUser aktifUser = getAktifUser();
        Boolean panitiaIsPpk = false;
        Paket paket = Paket.findById(this.paket_id);

        if (paket.ppk_user_id.equals(aktifUser.user_id) && paket.created_by.equals(new Integer(Math.toIntExact(aktifUser.user_id))) && paket.panitia_user_id.equals(aktifUser.user_id)){
            panitiaIsPpk = true;
        }

        if (TextUtils.isEmpty(this.to_status)) {

            if (panitiaIsPpk){
                return STATUS_PANITIA_PPK;
            }else {
                return STATUS_PANITIA;
            }

        }
        if("panitia".equalsIgnoreCase(this.to_status)) {
            return STATUS_PANITIA;// jadi "pejabat pengadaan"
        }
        return this.to_status;
    }

    public String getReadableStatus() {
        return this.status_paket.replace("_", " ").toUpperCase();
    }

    public String getReadableNegotiationStatus() {
        if (TextUtils.isEmpty(this.status_negosiasi)) {
            return "-";
        }
        return this.status_negosiasi.toUpperCase();
    }

}
