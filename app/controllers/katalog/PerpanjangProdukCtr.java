package controllers.katalog;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import controllers.BaseController;
import controllers.katalog.form.ProductForm;
import controllers.prakatalog.PenawaranCtr;
import controllers.prakatalog.PerpanjangPenawaranCtr;
import controllers.security.AllowAccess;
import models.chat.Chat;
import models.cms.KategoriKonten;
import models.common.*;
import models.elasticsearch.SearchQuery;
import models.elasticsearch.SearchResult;
import models.jcommon.blob.BlobTable;
import models.jcommon.util.CommonUtil;
import models.katalog.*;
import models.katalog.komoditasmodel.KomoditasAtributHarga;
import models.masterdata.*;
import models.penyedia.Penyedia;
import models.prakatalog.Penawaran;
import models.prakatalog.Usulan;
import models.prakatalog.UsulanKategoriProduk;
import models.secman.Acl;
import models.user.User;
import models.user.UserRoleKomoditasOverride;
import models.util.produk.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.data.validation.Required;
import play.libs.Codec;
import play.libs.Files;
import play.libs.Images;
import repositories.WilayahRepository;
import repositories.elasticsearch.ElasticsearchRepository;
import repositories.penyedia.ProviderRepository;
import repositories.produkkategori.ProdukKategoriRepository;
import services.produk.ProdukService;
import services.produkkategori.ProdukKategoriService;
import utils.DateTimeUtils;
import utils.LogUtil;

import java.io.File;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class PerpanjangProdukCtr extends BaseController {

	public static final String TAG = "PerpanjangProdukCtr";

	@AllowAccess({Acl.COM_PRODUK_ADD})
	public static void create(long pid) {
		setArgs(true, pid);
		renderArgs.put("action", "create");
		render("katalog/PerpanjangProdukCtr/create.html");
	}

	@AllowAccess({Acl.COM_PRODUK_EDIT})
	public static void edit(long produk_id) {
		Komoditas komoditas = setArgs(false, produk_id);
		List<ProdukAtributValue> produkAtributValues = ProdukAtributValue.findByProdukId(produk_id);
		Map<String, String> attributeMap = produkAtributValues.stream()
				.collect(Collectors.toMap(ProdukAtributValue::getCategoriAttrId, a -> a.atribut_value));
		Long[] wilJualId = new Long[0];
		List<Long> wilJualIds = new ArrayList<>();
		if (komoditas.isRegency()) {
			List<ProdukWilayahJualKabupaten> wilJual = ProdukWilayahJualKabupaten.find("produk_id = ?", produk_id).fetch();
			wilJualIds = new ArrayList<>();
			for (ProdukWilayahJualKabupaten p : wilJual) {
				wilJualIds.add(p.id);
			}
			wilJualId = wilJualIds.toArray(new Long[wilJualIds.size()]);
		} else if (komoditas.isProvince()) {
			List<ProdukWilayahJualProvinsi> wilJual = ProdukWilayahJualProvinsi.find("produk_id = ?", produk_id).fetch();
			wilJualIds = new ArrayList<>();
			for (ProdukWilayahJualProvinsi p : wilJual) {
				wilJualIds.add(p.id);
			}
			wilJualId = wilJualIds.toArray(new Long[wilJualIds.size()]);
		}

		List<ProdukHarga> listProdukHarga = ProdukHarga.findRiwayatByProdukId(produk_id);
		Map<String, List<ProdukHarga>> groupHarga = listProdukHarga.stream()
		.collect(
			Collectors.groupingBy(
				x -> x.getStandarDateString()
			)
		);
		NavigableMap<String, List<ProdukHarga>> riwayatProdukHarga = new TreeMap<>(groupHarga).descendingMap();
		Map<Long, List<RiwayatHargaProdukJson>> groupRiwayat = komoditas.isNational() ? null : listProdukHarga.stream()
				.map(x -> x.riwayatHarga())
				.flatMap(List::stream)
				.collect(
						Collectors.groupingBy(
								x -> komoditas.isProvince() ? x.provinsiId : x.kabupatenId
								)
						);
		NavigableMap<Long, List<RiwayatHargaProdukJson>> riwayatProdukHargaPerLokasi = groupRiwayat==null ? null : new TreeMap<>(groupRiwayat).descendingMap();
		
		ProdukHarga produkHarga = ProdukHarga.getByProductId(produk_id);
		String checkedLocationsJson = "{}";
		if(!komoditas.isNational()){
			checkedLocationsJson = produkHarga.checked_locations_json;
		}
		LinkedTreeMap checkedLocationMap = (new Gson()).fromJson(checkedLocationsJson, LinkedTreeMap.class);
		List<String> checkedLocationList = new ArrayList<String>();
		Map<Long, String> checkedLocations = new HashMap<Long, String>();
		if(komoditas.isRegency()) {
			checkedLocationList = new ArrayList<String>(((Map)(checkedLocationMap.get("regency"))).keySet());
			List<Kabupaten> kabupatenList = Kabupaten.findByIds(checkedLocationList);
			checkedLocations = kabupatenList.stream().collect(Collectors.toMap(Kabupaten::getId, Kabupaten::getNama));
		}
		else if(komoditas.isProvince()) {
			checkedLocationList = new ArrayList<String>(((Map)(checkedLocationMap.get("province"))).keySet());
			List<Provinsi> provinsiList = Provinsi.findByIds(checkedLocationList);
			checkedLocations = provinsiList.stream().collect(Collectors.toMap(Provinsi::getId, Provinsi::getNama));
		}

		renderArgs.put("checkedLocations", checkedLocations);
		renderArgs.put("riwayatProdukHarga", riwayatProdukHarga);
		renderArgs.put("riwayatProdukHargaPerLokasi", riwayatProdukHargaPerLokasi);
		renderArgs.put("produkHarga", produkHarga);
		renderArgs.put("listProdukHarga", listProdukHarga);
		renderArgs.put("action", "update");
		renderArgs.put("attrMapJson", CommonUtil.toJson(attributeMap));
		render("katalog/PerpanjangProdukCtr/create.html", produkAtributValues, wilJualId, wilJualIds);
	}

	private static Komoditas setArgs(boolean isCreated, long id) {
		AktifUser aktifUser = getAktifUser();
		User user = User.findById(aktifUser.user_id);

		Penawaran penawaran;
		Usulan usulan = null;
		Produk produk;
		Komoditas komoditas;

		if (isCreated) {
			produk = new Produk();
			penawaran = Penawaran.findById(id);
			notFoundIfNull(penawaran);
			allowToAddProduct(penawaran);
			usulan = Usulan.findById(penawaran.usulan_id);
			komoditas = Komoditas.findById(penawaran.komoditas_id);
		} else {
			produk = Produk.findById(id);
			notFoundIfNull(produk);
			produk.withPictures();
			produk.withDocuments(false);
			komoditas = Komoditas.findById(produk.komoditas_id);
			if(komoditas.isNational())
				produk.withPrice();
			if (!produk.allowToUpdate(getAktifUser().user_id)) {
				notFound();
			}
			
			penawaran = Penawaran.findById(produk.penawaran_id);
			if (penawaran != null && !produk.isEditAble()) {
				allowToAddProduct(penawaran);
			}
			if (penawaran != null) {
				usulan = Usulan.findById(penawaran.usulan_id);
			}
		}

		List<UnitPengukuran> unitPengukuranList = UnitPengukuran.findAllActive();
		List<Manufaktur> manufakturList = Manufaktur.findManufakturByKomoditas(komoditas.id);

		List<ProdukKategori> produkKategoriList = ProdukKategoriRepository.getProdukKategoriList(komoditas.id);
		List<UsulanKategoriProduk> usulanKategoriProdukList = null;
		if (usulan != null) {
			usulanKategoriProdukList = UsulanKategoriProduk.findByUsulan(usulan.id);
		}
		List<AllLevelProdukKategori> kategoriList = ProdukKategoriService
				.generateSingleRowKategoriByUsulan(produkKategoriList, usulanKategoriProdukList);

		List<Kurs> kursList = Kurs.findAll();
		Kurs kur = kursList.stream().filter(i->"IDR".equals(i.nama_kurs)).findAny().orElse(null);
		List<String> jenisProduk = new ArrayList<>(Arrays.asList(Produk.TYPE_LOCAL, Produk.TYPE_IMPORT));
		LogUtil.d(TAG, "unlimited: " + komoditas.apakah_stok_unlimited);

		List<String> atributHargaNamaList = KomoditasAtributHarga.findNamaNotOngkirByKomoditas(komoditas.id);
		if(komoditas.isRegency() && komoditas.perlu_ongkir){
			// atributHargaNamaList.add(KomoditasAtributHarga.findNamaOngkirByKomoditas(komoditas.id));
		}

		String atributHargaJson = new Gson().toJson(atributHargaNamaList);
		List<KomoditasAtributHarga> atributHargaList = KomoditasAtributHarga.findHargaNonOngkirByKomoditas(komoditas.id);

		//edit purpose
		if(produk != null){
			produk.jumlah_stok_form = produk.jumlah_stok.intValue();
			produk.jumlah_stok_inden_form = produk.jumlah_stok_inden.intValue();
		}

		renderArgs.put("user", user);
		renderArgs.put("komoditas", komoditas);
		renderArgs.put("unitPengukuranList", unitPengukuranList);
		renderArgs.put("manufakturList", manufakturList);
		renderArgs.put("kategoriList", kategoriList);
		renderArgs.put("kursList", kursList);
		renderArgs.put("kur", kur);
		renderArgs.put("jenisProduk", jenisProduk);
		if (penawaran != null) {
			renderArgs.put("pid", penawaran.id);
		}
		renderArgs.put("produk", produk);
		renderArgs.put("atributHargaJson", atributHargaJson);
		renderArgs.put("atributHargaList", atributHargaList);
		renderArgs.put("provKab", WilayahRepository.instance.getAllWilayah());
		return komoditas;
	}

	private static void allowToAddProduct(Penawaran penawaran) {
		if (!penawaran.allowToAddProduct()) {
			notFound();
		}
	}

	//@AllowAccess({Acl.COM_PRODUK_ADD})
	public static void validasiProduk(){
		String nama =params.get("nama") != null ? params.get("nama", String.class) : "";
		String noprodukp =params.get("noprodukp") != null ? params.get("noprodukp", String.class) : "";
		String pids = params.get("pid") != null ? params.get("pid", String.class) : "";
		Long ids = params.get("id") != null ? params.get("id", Long.class) : null;

		Map<String, Object> result = new HashMap<>(1);
		Produk temp = new Produk();
		Long penawaranIds = Long.valueOf(pids);

		temp.id = ids;
		temp.nama_produk = nama;
		//Logger.debug("namaproduk: "+nama);
		temp.no_produk_penyedia = noprodukp;
		Boolean isOk = false;
		if(!nama.isEmpty() && !noprodukp.isEmpty()){
			isOk = !Produk.isExistByNameAndNoProduct(temp, penawaranIds);
			isOk = !Produk.isExistByName(temp, penawaranIds);
			isOk = !Produk.isExistNoProduct(temp, penawaranIds);
			if(Produk.isExistByNameAndNoProduct(temp, penawaranIds)){
				result.put("message","Nama Produk : "+temp.nama_produk+" atau No Produk Penyedia: "+temp.no_produk_penyedia+" sudah digunakan");
			}else if(Produk.isExistNoProduct(temp, penawaranIds)){
				result.put("message","No Produk Penyedia:"+temp.no_produk_penyedia+" sudah digunakan");
			}else if(Produk.isExistByName(temp, penawaranIds)){
				result.put("message","Nama Produk : "+temp.nama_produk+" sudah digunakan");
			}else{
				result.put("message","Nama Produk atau No Produk Penyedia valid digunakan");
			}
		}else if(!nama.isEmpty() && noprodukp.isEmpty()){
			isOk = !Produk.isExistByName(temp, penawaranIds);
			if(Produk.isExistByName(temp, penawaranIds)){
				result.put("message","Nama Produk : "+temp.nama_produk+" sudah digunakan");
			}else{
				result.put("message","Nama Produk valid digunakan");
			}
		}else if(nama.isEmpty() && !noprodukp.isEmpty()){
			isOk = !Produk.isExistNoProduct(temp, penawaranIds);
			if(Produk.isExistNoProduct(temp, penawaranIds)){
				result.put("message","No Produk Penyedia:"+temp.no_produk_penyedia+" sudah digunakan");
			}else{
				result.put("message","No Produk Penyedia valid digunakan");
			}
		}
		result.put("valid",isOk);

		renderJSON(result);
	}

	@AllowAccess({Acl.COM_PRODUK_ADD})
	public static void createSubmit(Produk produk, ProductForm model) {
		checkAuthenticity();

		validation.valid(produk);
		if (validation.hasErrors()) {
			params.flash();
			flash.error("sempurnakan isian");
			Logger.debug(new Gson().toJson(validation.errorsMap()));
			create(produk.penawaran_id);
		} else {

			if(model.isCreatedMode() && Produk.isExistByNameAndNoProduct(produk,produk.penawaran_id)){
				params.flash();
				flash.error("Produk dengan nama: "+produk.nama_produk+" dan NO.Produk Penyedia="+produk.no_produk_penyedia+" sudah ada.");
				setArgs(true, produk.penawaran_id);
				//replace again with produk
				renderArgs.put("produk", produk);
				renderArgs.put("action", "create");
				render("katalog/PerpanjangProdukCtr/create.html");
			}else{
				Penawaran penawaran = Penawaran.findById(produk.penawaran_id);
				ProdukService.saveProduk(produk, getAktifUser(), model);
				if(Penawaran.STATUS_MENUNGGU_VERIFIKASI.equalsIgnoreCase(penawaran.status)) {
					produk.status = Produk.STATUS_MENUNGGU_PERSETUJUAN;
					produk.save();
				}
				else if(!penawaran.status.equals(Penawaran.STATUS_NEGOSIASI)){
					penawaran.changeStatus(Penawaran.STATUS_NEGOSIASI);
				}
			}
			//PerpanjangPenawaranCtr.daftarProduk(produk.penawaran_id);
		}
	}

	@AllowAccess({Acl.COM_PRODUK_ADD, Acl.COM_PRODUK_EDIT})
	public static void upload(File file, Long produkId){
		Map<String, Object> result = new HashMap<>(1);
		try {
			DokumenInfo dokumenInfo = ProdukLampiran.simpanLampiran(file, produkId);
			List<DokumenInfo> files = new ArrayList<>();
			files.add(dokumenInfo);
			result.put("files", files);
		} catch (Exception e) {
			e.printStackTrace();
		}

		renderJSON(result);
	}

	@AllowAccess({Acl.COM_PRODUK_ADD, Acl.COM_PRODUK_EDIT})
	public static void imageUpload(File imageFile, Long produkId){
		LogUtil.d(TAG, "product id: " + produkId);
		Map<String, Object> result = new HashMap<String, Object>(1);
		try {
			ProdukGambar gambar = new ProdukGambar(produkId);
			gambar.simpanGambarProduk(imageFile);
			if(gambar.posisi_file==ProdukGambar._POSISI_FILE_LOKAL){
				gambar.file_url = gambar.getLocalImageStorage()+gambar.file_sub_location+gambar.file_name;
			}else {
				gambar.file_url = gambar.getActiveUrl();
			}
			List<ProdukGambar> files = new ArrayList<>();
			files.add(gambar);
			result.put("files", files);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Logger.debug(result.toString());
		renderJSON(result);
	}

	@AllowAccess({Acl.COM_PRODUK_ADD, Acl.COM_PRODUK_EDIT})
	public static void hapusLampiran(Long fileId, Integer versi){

		BlobTable blobTable = BlobTable.findByBlobId(fileId);
		if(blobTable != null){
			blobTable.delete();
		}
	}

	@AllowAccess({Acl.COM_PRODUK_ADD, Acl.COM_PRODUK_EDIT})
	public static void hapusProdukLampiran(Long fileId){

		// ProdukLampiran pl = ProdukLampiran.findById(fileId);
		// if(pl != null){
		// 	BlobTable blobTable = BlobTable.findById(pl.dok_id_attachment, 0);
		// 	if(blobTable != null){
		// 		blobTable.delete();
		// 	}
		// 	pl.delete();
		// }
		BlobTable blobTable = BlobTable.findByBlobId(fileId) ;//.findById(fileId, 0);
		ProdukLampiran pl = ProdukLampiran.find("dok_id_attachment = ?",blobTable.id).first();
		if(blobTable!=null){
			blobTable.preDelete();
			blobTable.delete();
		}
		int result = 0;
		if(pl != null){
			result = pl.delete();
		}
		renderJSON(result);
	}

	@AllowAccess({Acl.COM_PRODUK_ADD,Acl.COM_PRODUK_EDIT})
	public static void hapusGambar(Long fileId){

		ProdukGambar gambar = ProdukGambar.findById(fileId);
		String path = Play.applicationPath+"/public/files/image/produk_gambar/"+gambar.file_sub_location+gambar.file_name;

		try {
			File file = new File(path);
			Files.delete(file);
			gambar.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void detail(Long id, Long location_id, String type, Boolean laporan, Report rpt, String vUser) {

        String randomID = Codec.UUID();

        List<KategoriKonten> listJenisLaporan = KategoriKonten.findAllByKonten(5);
        renderArgs.put("listJenisLaporan", listJenisLaporan);

//        try{

            boolean frontendDetail = true;
            if(!TextUtils.isEmpty(type) && (type.equalsIgnoreCase("province") || type.equalsIgnoreCase("regency"))
					&& location_id == null){
                LogUtil.d(TAG, "redirecting to all detail");

                frontendDetail = false;
            }

            LogUtil.d("frontendDetail",frontendDetail);

			Produk produk = Produk.findById(id);
			List<KomoditasAtributHarga> atributHargaList = KomoditasAtributHarga.findByKomoditas(produk.komoditas_id);
			Map<Long,String> mapAtributHarga = atributHargaList.stream().collect(
					Collectors.toMap(x -> x.id, x -> x.label_harga));

			Map<Long,String> sortedMapAtributHarga = new TreeMap<>(mapAtributHarga);

			List<ProdukHarga> produkHarga = ProdukHarga.findRiwayatByProdukId(id);
			Map<String, List<ProdukHarga>> groupHarga = produkHarga.stream()
			.collect(
				Collectors.groupingBy(
					x -> x.getStandarDateString()
				)
			);
			NavigableMap<String, List<ProdukHarga>> gh = new TreeMap<>(groupHarga).descendingMap();
			renderArgs.put("produkHarga", gh);

			if(!TextUtils.isEmpty(type) && (type.equalsIgnoreCase("province") || type.equalsIgnoreCase("regency")) && location_id != null){
				KomoditasAtributHarga kah = atributHargaList.stream().parallel().filter(it -> it.apakah_harga_utama == true).findFirst().orElse(new KomoditasAtributHarga());
				ProdukHarga ph = gh.firstEntry().getValue().stream().parallel().filter(it -> it.komoditas_harga_atribut_id.equals(kah.id)).findFirst().orElse(new ProdukHarga());
				List<ProdukHargaJson> hargaProdukJson = new Gson().fromJson(ph.harga, new TypeToken<List<ProdukHargaJson>>(){}.getType());
				ProdukHargaJson hkj = hargaProdukJson.stream()
					.filter(it -> (type.equalsIgnoreCase("province") ? it.provinsiId : it.kabupatenId) == location_id)
					.findFirst().orElse(new ProdukHargaJson());
				produk.harga_utama = new BigDecimal(hkj.harga);	
			}

			notFoundIfNull(produk);
			produk.withPictures();
			produk.withDocuments(true);
			produk.withProvider();
			produk.withCommodity();
			produk.withSpecifications();
			produk.withPenawaran();

			if(frontendDetail){
				produk.withProvincePrice(location_id);
				produk.withRegencyPrice(location_id);
			}

			if (produk.getProviderId() != null && produk.apakah_ditayangkan && produk.active) {
				new ElasticsearchRepository().inputViewCounter(produk, getAktifUser());
			}

			List<Kabupaten> kabupatenList = new ArrayList<>();
			List<Provinsi> provinsiList = new ArrayList<>();

			if (produk.komoditas.isRegency()) {
				LogUtil.d(TAG, "get regencies");
				kabupatenList = ProdukWilayahJualKabupaten.findWilayahJual(produk.getId());
				renderArgs.put("kabupatenList", kabupatenList);
			} else if(produk.komoditas.isProvince()) {
				LogUtil.d(TAG, "get provinces");
				provinsiList = ProdukWilayahJualProvinsi.findWilayahJual(produk.getId());
				renderArgs.put("provinsiList", provinsiList);
			}else{
				LogUtil.d(TAG, "get national");
				// List<HargaProdukDaerah> hargaProdukDaerahs = ProdukService.getHargaByDaerah(id,null,Komoditas.NASIONAL);
				// renderArgs.put("priceHistoryNasionalList",hargaProdukDaerahs);
			}
			if(produk.komoditas.perlu_ongkir){
				List<OngkosKirim> listOngkir = new Gson().fromJson(produk.ongkir, new TypeToken<ArrayList<OngkosKirim>>(){}.getType());
				if(listOngkir.size() > 0)
					listOngkir = setKabupatenProvinsi(listOngkir, provinsiList, kabupatenList);
				renderArgs.put("listOngkir", listOngkir);
			}

			renderArgs.put("frontendDetail", frontendDetail);
			renderArgs.put("tabLaporan", laporan);

			Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
			boolean showBuyButton = AktifUser.isUserAllowedToBuy(komoditas);
			renderArgs.put("showBuyButton", showBuyButton);

			if(rpt==null){
				rpt = new Report();
			}
			if(AktifUser.getAktifUser() != null) {
				rpt.reporter_name =  AktifUser.getAktifUser().nama_lengkap;
				rpt.reporter_telp = AktifUser.getAktifUser().no_tlp;
				rpt.reporter_email = AktifUser.getAktifUser().email;
			}

			/*chat*/
			AktifUser aktifUser = getAktifUser();
            String valueUser;
            if (vUser != null){
                valueUser = vUser;
            }else{
                valueUser = "0";
            }
			Chat chat;
			if(aktifUser != null){
			    if (valueUser != "0"){
                    chat = Chat.findByIdPengirimAndIdProdukForLink(aktifUser.user_id, id, valueUser);
                    if (chat == null){
                        chat = Chat.findByIdPenerimaAndIdProdukForLink(aktifUser.user_id, id, valueUser);
                    }
                }else {
                    chat = Chat.findByIdPengirimAndIdProduk(aktifUser.user_id, id);
                    if (chat == null){
                        chat = Chat.findByIdPenerimaAndIdProduk(aktifUser.user_id, id);
                    }
                }

			}else {
				chat = null;
			}

			boolean chatCheck;
			if (chat != null){
				chatCheck = true;
			}else {
				chatCheck = false;
			}

			Penyedia penyedia = Penyedia.findById(produk.penyedia_id);
			User user_produk = User.findById(penyedia.user_id);

			/* get list produk diskusi */
            List<ProdukDiskusi> listGetProdukDiskusiByIdProduk = ProdukDiskusi.findProdukDiskusiByIdProduk(id);

            /* get list produk diskusi balas */
            List<ProdukDiskusiBalas> listGetProdukDiskusiBalasByIdProduk = ProdukDiskusiBalas.findProdukDiskusiBalasByIdProduk(id);


//		}catch (Exception e){
//			e.printStackTrace();
//			throw new UnsupportedOperationException(e);
//		}

		render("katalog/ProdukCtr/web/detail.html", listGetProdukDiskusiBalasByIdProduk, listGetProdukDiskusiByIdProduk, valueUser, user_produk, id, chat, chatCheck, produk, sortedMapAtributHarga, randomID, rpt);

	}

	public static List<OngkosKirim> setKabupatenProvinsi(List<OngkosKirim> listOngkir, List<Provinsi> listPropinsi, List<Kabupaten> listKabupaten){
		if(listPropinsi.isEmpty()){
			List<String> pvIds = listOngkir.stream().map(it -> it.provinsiId.toString()).collect(Collectors.toList());
			if(!pvIds.isEmpty())
				listPropinsi = Provinsi.findByIds(pvIds);
		}
		if(listKabupaten.isEmpty()){
			List<String> kbIds = listOngkir.stream().map(it -> it.kabupatenId).filter(it->it!=null).map(it->it.toString()).collect(Collectors.toList());
			if(!kbIds.isEmpty())
				listKabupaten = Kabupaten.findByIds(kbIds);
		}
		for(OngkosKirim ok : listOngkir){
			ok.provinsi = listPropinsi.stream().filter(it -> it.id.equals(ok.provinsiId)).findFirst().orElse(new Provinsi()).nama_provinsi;
			ok.kabupaten = listKabupaten.stream().filter(it -> it.id.equals(ok.kabupatenId)).findFirst().orElse(new Kabupaten()).nama_kabupaten;
		}
		return listOngkir;
	}

	public static void allDetail(Long id){
		//try{
			Produk produk = Produk.findById(id);
			List<KomoditasAtributHarga> atributHargaList = new ArrayList<>();
			Map<Long,String> mapAtributHarga = new HashMap<>();
			List<ProdukHarga> produkHarga = new ArrayList<>();
			Map<Long,String> sortedMapAtributHarga = new HashMap<>();

			try{
				atributHargaList = KomoditasAtributHarga.findByKomoditas(produk.komoditas_id);
				mapAtributHarga = atributHargaList.stream().collect(
						Collectors.toMap(x -> x.id, x -> x.label_harga));

				produkHarga = ProdukHarga.findRiwayatByProdukId(id);
				Map<String, List<ProdukHarga>> groupHarga = produkHarga.stream()
						.collect(
								Collectors.groupingBy(
										x -> x.getStandarDateString()
								)
						);
				renderArgs.put("produkHarga", new TreeMap<>(groupHarga).descendingMap());

				sortedMapAtributHarga = new TreeMap<>(mapAtributHarga);

				notFoundIfNull(produk);
				produk.withPictures();
				produk.withDocuments(true);
				produk.withProvider();
				//User user_produk = User.findById(produk.penyedia.user_id);
				produk.withCommodity();
				produk.withSpecifications();
				produk.withPenawaran();

			}catch (Exception e){
				Logger.debug("error fill with paket: "+e.getMessage());
			}
			User user_produk = User.findById(produk.penyedia.user_id);

			if (produk != null) {
				if (produk.getProviderId() != null && produk.apakah_ditayangkan && produk.active) {
					new ElasticsearchRepository().inputViewCounter(produk, getAktifUser());
				}
			}
			List<Kabupaten> kabupatenList = new ArrayList<>();
			List<Provinsi> provinsiList = new ArrayList<>();
			if (produk.komoditas.isRegency()) {
				LogUtil.d(TAG, "get regencies");
				kabupatenList = ProdukWilayahJualKabupaten.findWilayahJual(produk.getId());
				renderArgs.put("kabupatenList", kabupatenList);
			} else if(produk.komoditas.isProvince()) {
				LogUtil.d(TAG, "get provinces");
				provinsiList = ProdukWilayahJualProvinsi.findWilayahJual(produk.getId());
				renderArgs.put("provinsiList", provinsiList);
			}else{
				LogUtil.d(TAG, "get national");
			}
			if(produk.komoditas.perlu_ongkir){
				List<OngkosKirim> listOngkir = new Gson().fromJson(produk.ongkir, new TypeToken<ArrayList<OngkosKirim>>(){}.getType());
				listOngkir = setKabupatenProvinsi(listOngkir, provinsiList, kabupatenList);
				renderArgs.put("listOngkir", listOngkir);
			}

			Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
			boolean showBuyButton = AktifUser.isUserAllowedToBuy(komoditas);
			renderArgs.put("showBuyButton", showBuyButton);

			render("katalog/ProdukCtr/web/detail.html", produk, sortedMapAtributHarga, user_produk);

//		}catch (Exception e){
//			e.printStackTrace();
//			throw new UnsupportedOperationException();
//		}

	}

	@AllowAccess({Acl.COM_PRODUK})
	public static void getRiwayatProduk(long id, Long pId, Long kId) {
		List<ProdukRiwayat> produkRiwayatList = ProdukRiwayat.findByProdukId(id);
		
		renderJSON(produkRiwayatList);
	}

	public static void beliSubmit() {

	}

	@AllowAccess({Acl.COM_PRODUK})
	public static void getPenyedia(String komoditas_id){
		List<Penyedia> penyediaList = new ArrayList<>();
		JsonArray jsonArray = new JsonArray();
		if (!TextUtils.isEmpty(komoditas_id)) {
			penyediaList.addAll(ProviderRepository.findByKomoditas(Long.parseLong(komoditas_id)));
			for (Penyedia model : penyediaList) {
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("id", model.id);
				jsonObject.addProperty("nama_penyedia", model.nama_penyedia);
				jsonObject.addProperty("active", model.active);
				jsonArray.add(jsonObject);
			}
		}
		renderJSON(jsonArray);
	}

	@AllowAccess({Acl.COM_PRODUK})
	public static void getProvOrKab() {

		renderJSON(Provinsi.findAllActive());
	}

	@AllowAccess({Acl.COM_PRODUK})
	public static void getKabByProv(String checkedProv) {
		LogUtil.d("data", checkedProv);
		Type listType = new TypeToken<ArrayList<CheckedProvJson>>(){}.getType();
		List<CheckedProvJson> checkedProvJsonList = CommonUtil.fromJson(checkedProv,listType);

		LogUtil.d("checkedProvJson",CommonUtil.toJson(checkedProvJsonList));

		List<ProvKabJson> result = new ArrayList<>();
		for(CheckedProvJson item : checkedProvJsonList){
			List<Kabupaten> kabList = Kabupaten.findByProvId(item.provId);

			for(Kabupaten kab : kabList){

				ProvKabJson provKabJson = new ProvKabJson();
				provKabJson.provId = item.provId;
				provKabJson.provName = item.provName;
				provKabJson.kabId = kab.id;
				provKabJson.kabName = kab.nama_kabupaten;

				result.add(provKabJson);
			}

		}

		LogUtil.d("ProvKabJson", CommonUtil.toJson(result));
		renderJSON(result);

	}

    @AllowAccess({Acl.COM_PRODUK})
    public static void getAllKabAllProv() {
        LogUtil.d("all prov and kav, %s", "oke");
        List<ProvKabJson> result = new ArrayList<>();
        List<Provinsi> allProv = Provinsi.findAllActive();

        for(Provinsi item : allProv){
            List<Kabupaten> kabList = Kabupaten.findByProvId(item.id);

            for(Kabupaten kab : kabList){

                ProvKabJson provKabJson = new ProvKabJson();
                provKabJson.provId = item.id;
                provKabJson.provName = item.nama_provinsi;
                provKabJson.kabId = kab.id;
                provKabJson.kabName = kab.nama_kabupaten;

                result.add(provKabJson);
            }

        }

        LogUtil.d("AllProvKabJson", CommonUtil.toJson(result));
        renderJSON(result);

    }
	@AllowAccess({Acl.COM_PRODUK})
	public static void getProdukKategoriByParent(long value) {
		List<ProdukKategori> kategoriList = ProdukKategori.find("active = ? and parent_id = ?",
				1, value).fetch();

		renderJSON(kategoriList);
	}

	@AllowAccess({Acl.COM_PRODUK})
	public static void getKetagoriAtributByKatId(long kategori_id) {
		List<ProdukKategoriAtribut> kategoriAtributList = ProdukKategoriAtribut.find("active = ? " +
				"and produk_kategori_id = ?", 1, kategori_id).fetch();

		renderJSON(kategoriAtributList);
	}

	@AllowAccess({Acl.COM_PRODUK_MEMBERI_PERSETUJUAN})
	public static void setujuProduk(long produk_id) {
		int status = Produk.tolakSetujuProduk(produk_id, 1);
		JsonObject json = new JsonObject();
		if (status == 1) {
			json.addProperty("status", 1);
			json.addProperty("message", "Produk telah disetujui");
		} else {
			json.addProperty("status", -1);
			json.addProperty("message", "Persetujuan produk gagal");
		}

		renderJSON(json);
	}

	@AllowAccess({Acl.COM_PRODUK_MEMBERI_PERSETUJUAN})
	public static void tolakProduk(long produk_id) {
		int status = Produk.tolakSetujuProduk(produk_id, 0);
		JsonObject json = new JsonObject();
		if (status == 0) {
			json.addProperty("status", 1);
			json.addProperty("message", "Produk telah ditolak");
		} else {
			json.addProperty("status", -1);
			json.addProperty("message", "Persetujuan produk gagal");
		}

		renderJSON(json);
	}

	@AllowAccess({Acl.COM_PRODUK})
	public static void spesifikasi(long id) {
		List<Perbandingan> perbandinganList = ProdukAtributValue.getPerbandinganSpek(id);

		render(perbandinganList);
	}

	@AllowAccess({Acl.COM_PRODUK})
	public static void harga(long id) {
		Produk produk = Produk.findById(id);
		List<KomoditasAtributHarga> atributHargaList = KomoditasAtributHarga.findByKomoditas(produk.komoditas_id);
			
		Map<Long,String> mapAtributHarga = atributHargaList.stream().collect(
					Collectors.toMap(x -> x.id, x -> x.label_harga));

		Map<Long,String> sortedMapAtributHarga = new TreeMap<>(mapAtributHarga);
		Long[] attrIds = sortedMapAtributHarga.keySet().toArray(new Long[sortedMapAtributHarga.keySet().size()]);
		List<Perbandingan> perbandinganList = ProdukHarga.getPerbandinganHarga(id, attrIds);
		render(perbandinganList, sortedMapAtributHarga);
	}

	@AllowAccess({Acl.COM_PRODUK_MEMBERI_PERSETUJUAN})
	public static void lihatPermintaan(long id) {
		ProdukTungguSetuju produkTungguSetuju = ProdukTungguSetuju.findById(id);
		Produk produk = Produk.findById(produkTungguSetuju.produk_id);
		Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
		Penyedia penyedia = Penyedia.findById(produk.penyedia_id);

		long penawaran_id = produk.penawaran_id;
		render(produk, komoditas, penyedia, penawaran_id);
	}

	public static void listProduk(String language, String komoditas_slug, long id) {
		SearchQuery model = new SearchQuery(params);
		model.commodityId = id;
		SearchResult searchResult = ElasticsearchRepository.open().searchProduct(model);
		searchResult.setChecked();
		Komoditas komoditas = Komoditas.findById(id);
		Boolean isIklan = true;
		if (komoditas != null && !komoditas.isNational()) {
			List<Provinsi> provinsiList = Provinsi.findAllActive();
			renderArgs.put("provinsiList", provinsiList);
			isIklan = komoditas.apakah_iklan != true;
			//komoditas.apakah_iklan != true
		}
		List<Manufaktur> manufakturList = Manufaktur.findManufakturByKomoditas(id);
		List<Penyedia> penyediaList = ProviderRepository.findPenyediaKontrakByKomoditas(id);


		final boolean showBuyButton = isUserAllowedToBuy(komoditas);
		Map<String, ProdukKategori> categories = ProdukKategori.getKategoriMapByKomId(id);
		renderArgs.put("searchResult", searchResult);
		renderArgs.put("manufakturList", manufakturList);
		renderArgs.put("penyediaList", penyediaList);
		//renderArgs.put("daftarProduk", daftarProduk);
		renderArgs.put("showBuyButton", showBuyButton);
		renderArgs.put("kategori", categories);

		render("katalog/ProdukCtr/web/listProduk.html", komoditas, model);
	}

	static boolean isUserAllowedToBuy(Komoditas komoditas){
		boolean isAllowedToBuy = getAktifUser() != null && (getAktifUser().isPp() || getAktifUser().isPpk()) && getAktifUser().isAuthorized("comProdukBelibtnBuy");
		if(!isAllowedToBuy || komoditas.apakah_iklan)
			return false;

//		Hardcode untuk komoditas nasional
		if(komoditas.komoditas_kategori_id != 5 && komoditas.komoditas_kategori_id != 6)
			return true;

		long user_id = getAktifUser().user_id;
//		if(getAktifUser().akses_seluruh_komoditas)
//			return true;
		User user = User.findById(user_id);

		if(!user.override_role_komoditas)
//			return true;
		{
			List<UserRoleKomoditasOverride> komoditasOverrides = UserRoleKomoditasOverride.findByUserId(user.id);
			if (komoditasOverrides != null && komoditasOverrides.size() > 0) {
				List<Long> komoditasList = komoditasOverrides.stream().
						map(obj -> obj.komoditas_id).
						collect(Collectors.toList());
				if (komoditasList.contains(komoditas.id))
					return true;
			}
		}
		return false;
	}

	public static void getRiwayatHarga(Long produk_id, Long komoditas_id, Long daerah_id, String kelas_harga){
		LogUtil.d(TAG, "getRiwayatHarga");
		List<HargaProdukDaerah> hargaProdukDaerahs = ProdukService.getHargaByDaerah(produk_id,daerah_id,kelas_harga);

		renderJSON(hargaProdukDaerahs);
	}

	public static void downloadTemplateOngkir(String namaProduk, String namaPenyedia){

		String path = Play.applicationPath+"/public/files/upload/template/template_import_ongkir.xlsx";
		File file = new File(path);
		String fileExtension = FilenameUtils.getExtension(file.getName());

		String dateString = new SimpleDateFormat("ddMMyyyy").format(new Date());
		String namaFile = "["+dateString+"] "+namaPenyedia+"_"+namaProduk+"."+fileExtension;

		renderBinary(file,namaFile);
	}

	public static void productListProvider() {
		List<Komoditas> komoditasList = new ArrayList<>();
		try{
			komoditasList = Komoditas.findByPenyedia(Penyedia.getProviderByUserId(AktifUser.getActiveUserId().longValue()).id);
		}catch (Exception e){}
//		ProductSearchResult model = ProdukService.searchProduct(new controllers.katalog.form.SearchQuery(params));
//		renderArgs.put("searchResult", model);
		renderArgs.put("komoditasList", komoditasList);
		renderTemplate("katalog/ProdukCtr/listProdukPenyedia.html");
	}

	public static void captcha(String id) {
		Images.Captcha captcha = Images.captcha();
		String code = captcha.getText("#8C1919");
		Cache.set(id, code, "10mn");
		renderBinary(captcha);
	}

	public static void reportSubmit(Report rpt, File file,
									@Required(message="Please type the code") String code,
									String randomID) {

		AktifUser aktifUser = getAktifUser();
		ReportRiwayat rptRiwayat = new ReportRiwayat();

		if(aktifUser != null) {
			rpt.reporter_id = aktifUser.user_id;
		}

		rpt.item_type = "produk";
		rpt.status = "proses";

		if(validation.equals(code, Cache.get(randomID)).ok){
			if(file != null){
				if(file.length() <= 5242880) { //in bytes
					LogUtil.d("file size : ", file.length());
					long reportId = rpt.save();
					rptRiwayat.simpanReportRiwayat(reportId, rpt, file);
					flash.success("Laporan telah disimpan dan akan ditindaklanjut oleh tim Admin. ");
				} else {
					flash.error("File upload melebihi batas maksimum ukuran !");
				}
			} else {
				long reportId = rpt.save();
				rptRiwayat.simpanReportRiwayat(reportId, rpt, file);

				flash.success("Laporan telah disimpan dan akan ditindaklanjut oleh tim Admin. ");
			}

			Cache.delete(randomID);
			//new report again
			 Long ids = rpt.item_id;
			 rpt = new Report();
			detail(ids, null, null, true,rpt, "");
		}
		if(validation.hasErrors()){
			flash.error("Invalid Captcha. Please type it again");
			detail(rpt.item_id, null, null, true, rpt, "");
		}
	}

	public static void reportReply(Report rpt, File file) {
		Report report = Report.findById(rpt.id);
		ReportRiwayat rptRiwayat = new ReportRiwayat();
		Date date = new Date();

		rpt.reporter_id = Long.valueOf(AktifUser.getActiveUserId());
		report.reply = rpt.message;
		report.reply_by = rpt.reporter_id;
		report.reply_date = date;

		if(file != null) {
			if(file.length() <= 5242880) { //in bytes
				if(!report.reporter_id.equals(rpt.reporter_id)) {
					report.save();
				}
				rptRiwayat.simpanReportRiwayat(rpt.id, rpt, file);
				flash.success("Balasan laporan telah disimpan. ");
			} else {
				flash.error("File upload melebihi batas maksimum ukuran !");
			}
		} else {
			if(!report.reporter_id.equals(rpt.reporter_id)) {
				report.save();
			}
			rptRiwayat.simpanReportRiwayat(rpt.id, rpt, file);
			flash.success("Balasan laporan telah disimpan. ");
		}

		redirect("katalog.laporanprodukctr.detail", rpt.id);
	}

	public static void reportClose(Long id){
		JsonObject jsonObject = new JsonObject();
		Report report = new Report();
		if (report.closeReport(id)) {
			jsonObject.addProperty("isSuccess", true);
			jsonObject.addProperty("message", "Diskusi laporan berhasil ditutup");
		} else {
			jsonObject.addProperty("isSuccess", false);
			jsonObject.addProperty("message", "Diskusi laporan gagal ditutup!");
		}
		renderJSON(jsonObject);
	}

	public static void infoSetujuTayang (long prodId){
		StringBuilder content = new StringBuilder();
		JsonObject json = new JsonObject();
		List<String> breadcrumb = new ArrayList<>();

		Produk produk = Produk.getProdukInfoTayang(prodId);
		ProdukKategori produkKategori = ProdukKategori.find("id = ? and active = 1", produk.produk_kategori_id).first();

		boolean isTheLast = false;
		int idx = 0;
		content.append("<p>");
		while (!isTheLast){
			if(produkKategori == null){
				break;
			}
			breadcrumb.add(idx, produkKategori.nama_kategori);
			idx += 1;

			if(produkKategori.parent_id == null || produkKategori.parent_id == 0){
				isTheLast = true;
				continue;
			}
			produkKategori = ProdukKategori.find("id = ? and active = 1", produkKategori.parent_id).first();
		}
		for(int i=breadcrumb.size()-1; i >= 0; i--){
			content.append(breadcrumb.get(i));

			if(i != 0){
				content.append(" <i class='fa fa-caret-right'></i> ");
			}
		}
		content.append("</p>");

		String notAvailable = "<span style='color:#ABABAB'>n/a</span>";
		String mintaDisetujuiOleh = produk.nama_minta_disetujui == null ? notAvailable : produk.nama_minta_disetujui;
		String mintaDisetujuiTanggal = produk.minta_disetujui_tanggal == null ? notAvailable : DateTimeUtils.convertToDdMmYyyy(produk.minta_disetujui_tanggal);
		String mintaDisetujuiAlasan = produk.minta_disetujui_alasan == null ? notAvailable : produk.minta_disetujui_alasan;
		String setujuTolakOleh = produk.nama_setuju_tolak == null ? notAvailable : produk.nama_setuju_tolak;
		String setujuTolakTanggal = produk.setuju_tolak_tanggal == null ? notAvailable : DateTimeUtils.convertToDdMmYyyy(produk.setuju_tolak_tanggal);
		String setujuTolakAlasan = produk.setuju_tolak_alasan == null ? notAvailable : produk.setuju_tolak_alasan;

		String setujuTolak = "";
		if(produk.setuju_tolak != null){
			if(produk.setuju_tolak.matches("setuju")){
				setujuTolak = "<i style='color:green' class='fa fa-check-circle fa-lg'></i> ";
			}else {
				setujuTolak = "<i style='color:red' class='fa fa-minus-circle fa-lg'></i> ";
			}
			setujuTolak += produk.setuju_tolak;
		}else{
			setujuTolak = notAvailable;
		}

		json.addProperty("kategori", content.toString());
		json.addProperty("url_image", produk.getActiveImageUrl());
		json.addProperty("no_produk", produk.no_produk);
		json.addProperty("nama_produk", produk.nama_manufaktur + " " + produk.nama_produk);
		json.addProperty("oleh_permintaan", mintaDisetujuiOleh);
		json.addProperty("tgl_permintaan", mintaDisetujuiTanggal);
		json.addProperty("alasan_permintaan", mintaDisetujuiAlasan);
		json.addProperty("hasil_keputusan", setujuTolak);
		json.addProperty("oleh_keputusan", setujuTolakOleh);
		json.addProperty("tgl_keputusan", setujuTolakTanggal);
		json.addProperty("alasan_keputusan", setujuTolakAlasan);
		renderJSON(json);
	}

}
