package models.api.produk.detail;

import models.api.produk.detail.lampiran.Item;
import play.Logger;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dadang on 1/5/18.
 */
public class Lampiran {
	public List<Item> item = new ArrayList<>();
	public String tanggal_update;

	public Timestamp getTglUpdateAsTimestamp(){
		Date dt = parseAsDate(tanggal_update);
		Timestamp ts=new Timestamp(dt.getTime());
		return ts;
	}

	public Date parseAsDate(String dateStr){
		try{
			DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime dateTime = LocalDateTime.parse(dateStr, format);
			//Date in = new Date();
			//LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
			Date result = Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
			return result;
		}catch (Exception e){

		}
		return null;
	}

	public Date getTglUpdateAsDate(){
		return parseAsDate(tanggal_update);
	}
}
