package models.purchasing;

import constants.paket.PaketStatusConstant;
import ext.FormatUtils;
import jobs.JobTrail.JobUpdatePaket;
import models.BaseKatalogTable;
import models.common.AktifUser;
import models.jcommon.config.Configuration;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.katalog.Komoditas;
import models.masterdata.Kldi;
import models.masterdata.Kurs;
import models.masterdata.KursNilai;
import models.masterdata.Satker;
import models.penyedia.Penyedia;
import models.penyedia.PenyediaDistributor;
import models.purchasing.contracts.PackageAccessContract;
import models.purchasing.contracts.PackageContract;
import models.purchasing.contracts.PackageUrlContract;
import models.purchasing.kontrak.PaketKontrak;
import models.purchasing.negosiasi.PaketProdukHeader;
import models.purchasing.pembayaran.Pembayaran;
import models.purchasing.penerimaan.PaketRiwayatPenerimaan;
import models.purchasing.penerimaan.ProdukPenerimaan;
import models.purchasing.pengiriman.PaketRiwayatPengiriman;
import models.purchasing.pengiriman.ProdukPengiriman;
import models.purchasing.riwayat.PaketRiwayat;
import models.purchasing.riwayat.PaketRiwayatNegosiasi;
import models.purchasing.status.PaketStatus;
import models.purchasing.status.StatusMessage;
import models.user.User;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.data.validation.Email;
import play.data.validation.Phone;
import play.data.validation.Required;
import play.db.jdbc.*;
import utils.DateTimeUtils;

import javax.persistence.Transient;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Table
public class Paket extends BaseKatalogTable implements PackageContract, PackageAccessContract, PackageUrlContract {

	@Id
	public Long id;
	@Required
	public Integer komoditas_id;

	public Integer provinsi_id;
	public Integer kabupaten_id;

	@Required
	public String nama_paket;
	@Required
	public String satuan_kerja_nama;
	@Required
	public String satuan_kerja_alamat;
	@Required
	public Long satuan_kerja_kabupaten;
	@Transient
	public String nama_satker;

	@Required
	public String pengiriman_alamat;
	@Required
	public Long pengiriman_kabupaten;

	@Required
	public String no_paket;

	public String getNo_paket() {
		String[] idsp = no_paket.split("-");
		if(idsp[2].trim().equalsIgnoreCase("0")){
			no_paket = idsp[0]+"-"+idsp[1]+"-"+ id;
			//Paket.updateNopaketJob(id);
		}

		if (null != id && id > 0 && no_paket.contains("null")){
			no_paket = no_paket.replaceAll("-null","-"+id);
			updateNomorPaket(no_paket,id);
		}

		return no_paket;
	}

	public void setNo_paket(String no_paket) {
		this.no_paket = no_paket;
	}

	@Required
	public String tahun_anggaran;

	public String kode_anggaran;
	@Required
	public String kldi_id;
	@Required
	public String satker_id;
	@Required
	public String satuan_kerja_npwp;
	@Required
	public Integer agr_paket_produk;
	@Required
	public Integer agr_paket_produk_sudah_diterima_semua;
	@Required
	public Long ppk_user_id;
	@Required
	public String ppk_jabatan;
	@Required
	public String ppk_nip;

	public String ppk_sertifikat;
	@Required @Phone
	public String ppk_telepon;
	@Required
	public Long panitia_user_id;
	
	public String pp_sertifikat;
	@Required @Phone
	public String panitia_no_telp;
	@Required
	public String panitia_jabatan;
	@Required
	public String panitia_nip;
	public Double agr_total_harga_diterima_dan_ongkir=0.0;
	@Required
	public Long penyedia_id;
	public Long penyedia_distributor_id;
	@Required @Email
	public String panitia_email;
	@Required
	public Long rup_id;
	public Long kurs_id_from;
	public Double kurs_nilai;
	public Date kurs_tanggal;
	public Double agr_konversi_harga_total;
	@Required @Email
	public String ppk_email;
	public String kldi_jenis;
	public Long instansi_id;
	public Long instansi_jenis_id;

	public Integer apakah_selesai = 0;
	public Integer sudah_dirating = 0;
	public double rating_nilai_avg = 0;
	public Integer rating_nilai_pelayanan = 0;
	public Integer rating_nilai_akurasi = 0;
	public Integer rating_nilai_kecepatan = 0;
	public String rating_komentar = "";

	public Date tanggal_selesai;
	public Integer diselesaikan_oleh;

	public Integer apakah_batal = 0;
	public String alasan_batal = "";
	public Date tanggal_batal;
	public Integer dibatalkan_oleh;
	@Transient
	public String nama_penyedia;
	@Transient
	public long totalRevisi = 0;
	public long getTotalRevisi() {
		return totalRevisi;
	}

	public void setTotalRevisi(long totalRevisi) {
		this.totalRevisi = totalRevisi;
	}
	@Transient
	public Double contractValue = 0D;
	@Transient
	public Integer totalContractUploaded = 0;

	@Transient
	public Kldi kldi;
	@Transient
	public Satker satker;
	@Transient
	public User buyer;
	@Transient
	public User userPp;
	@Transient
	public PenyediaDistributor distributor;
	@Transient
	public Penyedia provider;
	@Transient
	public PaketStatus latestStatus;

	@Transient
	public PaketProduk paketProduk;
	@Transient
	public PaketRiwayatPengiriman deliveryHistory;
	@Transient
	public PaketRiwayatPenerimaan acceptanceHistory;
	@Transient
	public Pembayaran payment;
	@Transient
	public Komoditas commodity;
	@Transient
	public StatusMessage statusMessage;
	@Transient
	public KursNilai kursNilai;

	@Transient
	public List<PaketRiwayat> packageHistories = new ArrayList<>();
	@Transient
	public List<PaketRiwayatNegosiasi> totalNegotiations = new ArrayList<>();
	@Transient
	public List<PaketRiwayatNegosiasi> negotiationsHistory = new ArrayList<>();
	@Transient
	public List<PaketRiwayatPengiriman> deliveryHistories = new ArrayList<>();
	@Transient
	public List<PaketRiwayatPenerimaan> acceptanceHistories = new ArrayList<>();
	@Transient
	public List<PaketProduk> products = new ArrayList<>();

	@Transient
	public List<Pembayaran> paymentHistories = new ArrayList<>();
	@Transient
	public List<PaketKontrak> contracts = new ArrayList<>();
	@Transient
	public Kurs kurs;
	@Transient
	public PaketProdukHeader negoHeader;
	@Transient
	public PaketStatus paketStatus;
	@Transient
	public Penyedia penyedia;
	@Transient
	public List<PaketSumberDana> sumberDana = new ArrayList<>();
	@Transient
	public List<PaketAnggotaUlp> ulpMembers = new ArrayList<>();

	@Transient
	public String satuan_kerja_provinsi_nama;
	@Transient
	public String satuan_kerja_kabupaten_nama;
	@Transient
	public String pengiriman_provinsi_nama;
	@Transient
	public String pengiriman_kabupaten_nama;

	@Transient
	public Integer gunakan_alamat_satker;

	@Transient
	public boolean is_distributor = false;

	@Transient
	public boolean bolehEdit = true;

	@Transient
	public boolean tampilkan_persetujuan_ketika_ditolak = false;

	public Paket(){

	}

	public Paket(Long id) {
		this.id = id;
	}

	public Paket(int komoditas_id, long penyedia_id, long rup_id, String no_paket, String nama_paket, String satuan_kerja_nama,
				 String satuan_kerja_alamat, Long satuan_kerja_kabupaten, String pengiriman_alamat, Long pengiriman_kabupaten,
				 String satuan_kerja_npwp, long panitia_user_id, String panitia_email, String panitia_no_telp,
				 long ppk_user_id, String ppk_jabatan, String ppk_nip, long kurs_id_from, Double kurs_nilai, Date kurs_tanggal,
				 int agr_paket_produk, Double agr_konversi_harga_total, String panitia_nip, String panitia_jabatan, String ppk_email, String ppk_telepon,
				 String satker_id, String kldi_jenis, String kldi_id, String tahun_anggaran, String ppk_sertifikat, String pp_sertifikat,
				 Long prpid, Long kabid, Integer alamat
				 ){

		this.komoditas_id = komoditas_id;
		this.penyedia_id = penyedia_id;
		this.rup_id = rup_id;
		this.no_paket = no_paket;
		this.nama_paket = nama_paket;
		this.satuan_kerja_nama = satuan_kerja_nama;
		this.satuan_kerja_alamat = satuan_kerja_alamat;
		this.satuan_kerja_kabupaten = satuan_kerja_kabupaten;
		this.pengiriman_alamat = pengiriman_alamat;
		this.pengiriman_kabupaten = pengiriman_kabupaten;
		this.satuan_kerja_npwp = satuan_kerja_npwp;
		this.panitia_user_id = panitia_user_id;
		this.panitia_email = panitia_email;
		this.panitia_no_telp = panitia_no_telp;
		this.ppk_user_id = ppk_user_id;
		this.ppk_jabatan = ppk_jabatan;
		this.ppk_nip = ppk_nip;
		this.kurs_id_from = kurs_id_from;
		this.kurs_nilai = kurs_nilai;
		this.kurs_tanggal = kurs_tanggal;
		this.agr_paket_produk = agr_paket_produk;
		this.agr_konversi_harga_total = agr_konversi_harga_total;
		this.panitia_nip = panitia_nip;
		this.panitia_jabatan = panitia_jabatan;
		this.ppk_email = ppk_email;
		this.ppk_telepon = ppk_telepon;
		this.satker_id = satker_id;
		this.kldi_jenis = kldi_jenis;
		this.kldi_id = kldi_id;
		this.tahun_anggaran = tahun_anggaran;
		this.ppk_sertifikat = ppk_sertifikat;
		this.pp_sertifikat = pp_sertifikat;
		this.provinsi_id = prpid.intValue();
		this.kabupaten_id = kabid.intValue();
		this.gunakan_alamat_satker = alamat;
	}

	@Override
	public List<ProdukPengiriman> getDeliveryProducts() {
		if (deliveryHistory != null) {
			return deliveryHistory.products;
		}
		return new ArrayList<>();
	}

	@Override
	public List<ProdukPenerimaan> getAcceptanceProducts() {
		if (acceptanceHistory != null) {
			return acceptanceHistory.products;
		}
		return new ArrayList<>();
	}

	@Override
	public List<Pembayaran> getPayments() {
		return this.paymentHistories;
	}

	@Override
	public Long getBuyerId() {
		return this.ppk_user_id;
	}

	@Override
	public Long getPpId() {
		return this.panitia_user_id;
	}

	@Override
	public Long getPpkId() {
		return this.ppk_user_id;
	}

	@Override
	public Long getDistributorId() {
		return this.penyedia_distributor_id;
	}

	@Override
	public Integer getTotalContractUploaded() {
		return this.totalContractUploaded;
	}

	@Override
	public PenyediaDistributor getDistributor() {
		return this.distributor;
	}

	@Override
	public User getBuyer() {
		return this.buyer;
	}

	@Override
	public User getCommittee() {
		return this.userPp;
	}

	@Override
	public String getRejectedReason() {
		if (!TextUtils.isEmpty(this.alasan_batal)) {
			return this.alasan_batal;
		}
		return "";
	}

	@Override
	public PaketStatus getStatus() {
		return this.latestStatus;
	}

	@Override
	public PaketRiwayatPenerimaan getAcceptanceHistory() {
		return this.acceptanceHistory;
	}

	@Override
	public User getUserPp() {
		return this.userPp;
	}

	@Override
	public Penyedia getProvider() {
		return this.provider;
	}

	@Override
	public Integer getCommodityId() {
		return this.komoditas_id;
	}

	@Override
	public Long getKursId() {
		return this.kurs_id_from;
	}

	@Override
	public Komoditas getCommodity() {
		return this.commodity;
	}

	@Override
	public Kurs getKurs() {
		return this.kurs;
	}

	@Override
	public PaketProdukHeader getProdukNegoHeader() {
		return this.negoHeader;
	}

	@Override
	public PaketStatus getPaketStatus() {
		return this.paketStatus;
	}

	@Override
	public List<PaketSumberDana> getPaketSumberDana() {
		return this.sumberDana;
	}

	@Override
	public Long getProviderId() {
		return this.penyedia_id;
	}

	public int getTotalProduct() {
		return agr_paket_produk;
	}

	public String getReadableCreatedDate() {
		return DateTimeUtils.parseToReadableDate(created_date);
	}

	public String getReadableUpdatedDate() {
		return DateTimeUtils.parseToReadableDate(modified_date);
	}

	public String getFormattedContractValue() {
		return FormatUtils.formatCurrencyRupiah(contractValue);
	}

	public String getInstanceName() {
		if (kldi != null) {
			return kldi.nama;
		}
		return "";
	}

	public String getWorkUnitName() {
		if (satker != null) {
			return satker.nama;
		}
		return "";
	}

	public String getWorkUnitAddress() {
		if (satker != null) {
			return satker.alamat;
		}
		return "";
	}

	public String getPpkName() {
		if (buyer != null) {
			return buyer.nama_lengkap;
		}
		return "";
	}

	public String getPpName() {
		if (userPp != null) {
			return userPp.nama_lengkap;
		}
		return "";
	}

	@Override
	public Integer getTotalFullyAccepted() {
		return agr_paket_produk_sudah_diterima_semua;
	}

	@Override
	public boolean isProductFullyAccepted() {
		final boolean isFullyAccepted = getTotalFullyAccepted() >= getTotalProduct();
		Logger.debug("total products: " + getTotalProduct());
		Logger.debug("total fully accepted: " + getTotalFullyAccepted());
		Logger.debug("is product fully accepted: " + isFullyAccepted);
		return isFullyAccepted;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setPayment(List<Pembayaran> payments) {
		this.paymentHistories = payments;
	}

	@Override
	public void setDeliveries(List<PaketRiwayatPengiriman> deliveries) {
		this.deliveryHistories = deliveries;
	}

	@Override
	public void setAcceptances(List<PaketRiwayatPenerimaan> acceptances) {
		this.acceptanceHistories = acceptances;
	}

	@Override
	public void setPackageHistories(List<PaketRiwayat> histories) {
		this.packageHistories = histories;
	}

	@Override
	public void setSatker(Satker satker) {
		this.satker = satker;
	}

	@Override
	public void setKldi(Kldi kldi) {
		this.kldi = kldi;
	}

	@Override
	public void setBuyer(User model) {
		this.buyer = model;
	}

	@Override
	public void setPp(User model) {
		this.userPp = model;
	}

	@Override
	public void setProvider(Penyedia model) {
		this.provider = model;
	}

	@Override
	public void setDistributor(PenyediaDistributor model) {
		this.distributor = model;
	}

	@Override
	public void setTotalNegotiations(List<PaketRiwayatNegosiasi> list) {
		this.totalNegotiations = list;
	}

	@Override
	public void setNegotiationHistory(List<PaketRiwayatNegosiasi> list) {
		this.negotiationsHistory = list;
	}

	@Override
	public void setProducts(List<PaketProduk> list) {
		this.products = list;
	}

	@Override
	public void setContracts(List<PaketKontrak> list) {
		this.contracts = list;
	}

	@Override
	public void setContractValue(Double value) {
		this.contractValue = value;
	}

	@Override
	public void setTotalContractUploaded(Integer value) {
		this.totalContractUploaded = value;
	}

	@Override
	public void setUlPMembers(List<PaketAnggotaUlp> list) {
		this.ulpMembers = list;
	}

	@Override
	public void setLatestStatus(PaketStatus model) {
		this.latestStatus = model;
	}

	@Override
	public void setCommodity(Komoditas model) {
		this.commodity = model;
	}

	@Override
	public void setStatusMessage(StatusMessage model) {
		this.statusMessage = model;
	}

	@Override
	public StatusMessage getAccessMessage() {
		return this.statusMessage;
	}

	@Override
	public void setKurs(Kurs kurs) {
		this.kurs = kurs;
	}

	@Override
	public void setNegoHeader(PaketProdukHeader paketProdukHeader) {
		this.negoHeader = paketProdukHeader;
	}

	@Override
	public void setPaketStatus(PaketStatus paketStatus) {
		this.paketStatus = paketStatus;
	}

	@Override
	public void setPenyedia(Penyedia penyedia) {
		this.penyedia = penyedia;
	}

	@Override
	public void setSumberDana(List<PaketSumberDana> sumberDana) {
		this.sumberDana = sumberDana;
	}

	@Override
	public Long getPenyediaId() {
		return penyedia_id;
	}

	@Override
	public String getSatketId() {
		return this.satker_id;
	}

	@Override
	public String getKldiId() {
		return this.kldi_id;
	}

	public String getTotalPrice() {
		return FormatUtils.formatCurrencyRupiah(agr_konversi_harga_total);
	}
//		String query = "select id, no_paket, nama_paket "
//				+ "from paket where active = 1";
//
//		if(komoditas_id != 0){
//			query+= " and komoditas_id = "+komoditas_id;
//		}
//
//		if(!keyword.isEmpty() && keyword != null) {
//			query+=" and (no_paket = '"+keyword+"' or nama_paket like '%"+keyword+"%')";
//		}
//
//
//		return Query.find(query,Paket.class).fetch();

	public String formatedTanggalKurs(){
		return DateTimeUtils.formatDateToString(this.kurs_tanggal, "dd-MM-yyyy");
	}

	public void generateNoPaket(String kode_komoditas){
		SimpleDateFormat sdf = new SimpleDateFormat("yyMM");
		String yearMonth = sdf.format(new Date());
		Long idPaket = 0l;//TableStatus.getByNameLike("paket").Auto_increment;
		StringBuilder sb = new StringBuilder(kode_komoditas);
		this.no_paket = sb.append("-P").append(yearMonth).append("-").append(idPaket).toString();
	}

	public Paket setFromRating(PaketRating model) {
		this.sudah_dirating = 1;
		this.rating_nilai_akurasi = model.accuracy;
		this.rating_nilai_kecepatan = model.speed;
		this.rating_nilai_pelayanan = model.service;
		this.rating_komentar = model.additionalMessage;
		this.rating_nilai_avg = model.getAverageRating();
		return this;
	}

	public Paket setFinished() {
		this.apakah_selesai = 1;
		this.diselesaikan_oleh = AktifUser.getActiveUserId();
		this.tanggal_selesai = new Timestamp(System.currentTimeMillis());
		return this;
	}

	//salah sasaran, ini malah untuk batal bukan tolak
	public Paket setReject(String reason) {
		this.apakah_batal = 1;
		this.dibatalkan_oleh = AktifUser.getActiveUserId();
		this.tanggal_batal = new Timestamp(System.currentTimeMillis());
		this.alasan_batal = reason;
		return this;
	}

	public static List<Paket> findNamaLike(String nama_paket)
	{
		return find("nama_paket like ? ", "%" + nama_paket+ "%").fetch();
	}

	public static List<Paket> findNamaMatch(String nama_paket)
	{
		return find("MATCH(nama_paket) AGAINST(?) ",  nama_paket).fetch();
	}

	public static List<Paket> findByKeywordAndKomoditas(String keyword, Integer komoditas_id, String exclude){
		QueryBuilder qb = new QueryBuilder("select id, no_paket, nama_paket from paket where active = 1");
		if (komoditas_id != 0) {
			qb.append("AND komoditas_id=? ", komoditas_id);
		}
		if (!keyword.isEmpty() && !TextUtils.isEmpty(keyword)) {
			qb.append("AND (no_paket=? OR nama_paket LIKE ?) ", keyword, "%" + keyword + "%");
		}
		if (!TextUtils.isEmpty(exclude)) {
			qb.append("AND id NOT IN (" + exclude + ") ");
		}
		qb.append(" LIMIT 30");
		return new JdbcQuery(qb, Paket.class).fetch();
	}

	public static Integer getTotalPackageByKurs(Long id) {
		final String query = "SELECT COUNT(*) as total FROM paket WHERE kurs_id_from =? AND active =?";
		return Query.find(query, Integer.class, id, AKTIF).first();
	}

	public static Integer getTotalPackageByDistributor(Long id) {
		final String query = "SELECT COUNT(*) as total FROM paket WHERE penyedia_distributor_id =? AND active =?";
		return Query.find(query, Integer.class, id, AKTIF).first();
	}

	public void withTransient(){
		final String query = 
			"select p.*, k.nama_kabupaten satuan_kerja_kabupaten_nama, pr.nama_provinsi satuan_kerja_provinsi_nama,	" +
				" 	kp.nama_kabupaten pengiriman_kabupaten_nama, pp.nama_provinsi pengiriman_provinsi_nama " +
				"from paket p " +
				"left join kabupaten k on k.id = p.satuan_kerja_kabupaten " +
				"left join provinsi pr on pr.id = k.provinsi_id " +
				"left join kabupaten kp on kp.id = p.pengiriman_kabupaten " +
				"left join provinsi pp on pp.id = kp.provinsi_id " +
				"where p.id =?";
		Paket pkt = Query.find(query, Paket.class, this.id).first();
		this.satuan_kerja_provinsi_nama = pkt.satuan_kerja_provinsi_nama;
		this.satuan_kerja_kabupaten_nama = pkt.satuan_kerja_kabupaten_nama;
		this.pengiriman_provinsi_nama = pkt.pengiriman_provinsi_nama;
		this.pengiriman_kabupaten_nama = pkt.pengiriman_kabupaten_nama;
	}

	public boolean allowAddAcceptanceAndDelivery(){
		String negosiasi = latestStatus.status_negosiasi;
		String status_paket = latestStatus.status_paket;
		boolean sepakat = false;
		boolean selesai = false;

		if (negosiasi !=null ){
			sepakat = latestStatus.status_negosiasi.equalsIgnoreCase(PaketStatus.STATUS_NEGOSIASI_SEPAKAT);
		}

		if (status_paket != null){
			selesai = status_paket.equalsIgnoreCase(PaketStatus.STATUS_PAKET_PAKET_SELESAI);
		}

		try{
			if (negosiasi != null && !negosiasi.isEmpty()){
				if(this.apakah_selesai ==1 && sepakat==true && selesai == true){
					return false;
				}else{
					return true;
				}
			} else {
				if(this.apakah_selesai ==1 && selesai == true){
					return false;
				}else{
					return true;
				}
			}
		}catch (Exception e){
			Logger.debug("exception " + e.getMessage());
		}
		return false;
	}

	public boolean isIs_distributor() {
		return is_distributor;
	}

	public void setIs_distributor(boolean is_distributor) {
		this.is_distributor = is_distributor;
	}

	public boolean get_distributor_status(long paket_id, AktifUser aktif_user) {
		boolean userLoginIsDistributorForThisPaket = false;

		final String query = "SELECT pd.user_id, p.id, p.penyedia_id, p.penyedia_distributor_id, p.no_paket from penyedia_distributor pd " +
						"join paket p on p.penyedia_distributor_id = pd.id " +
						"where p.id =?";
		PenyediaDistributor pd = Query.find(query, PenyediaDistributor.class, paket_id).first();

		if (pd != null && pd.user_id != null){
			if ((long)pd.user_id == aktif_user.user_id) {
				userLoginIsDistributorForThisPaket = true;
			}
			if (null != aktif_user.penyedia_id && (long)pd.penyedia_id==aktif_user.penyedia_id){
				userLoginIsDistributorForThisPaket = false;
			}
		}

		return userLoginIsDistributorForThisPaket;
	}

	public static void updateNopaketJob(long _id){

		JobUpdatePaket job1 = new JobUpdatePaket(_id);
		job1.now();
//		new Job(){
//			public void doJob(){
//				//pengecekan kembali kesesuaian no_paket dan id paket
//				try{
//					String sql = "update paket set no_paket = CONCAT(left(no_paket,10), id) where id = ?";
//					int result = Query.update(sql,_id);
//					Logger.info("Update no paket total:"+result);
//				}catch (Exception e){
//					Logger.error("PAKET SAVE: gagal save paket, di pengecekan no_paket, err:"+e.getMessage());
//				}
//			}
//		}.now();
	}

	public void withAllowEdit(Paket model){
		//status paket yang tidak diperbolehkan untuk edit paket
		if (!model.latestStatus.status_paket.isEmpty()){
			if (model.latestStatus.status_paket.equalsIgnoreCase(PaketStatusConstant.STATUS_DELIVERY)
					|| model.latestStatus.status_paket.equalsIgnoreCase(PaketStatusConstant.STATUS_FINISHED)
					|| model.latestStatus.status_paket.equalsIgnoreCase(PaketStatusConstant.STATUS_PPK_APPROVED)
					|| model.latestStatus.status_paket.equalsIgnoreCase(PaketStatusConstant.STATUS_PPK_REJECTED)
					|| model.latestStatus.status_paket.equalsIgnoreCase(PaketStatusConstant.STATUS_CANCEL)
					|| model.latestStatus.status_paket.equalsIgnoreCase(PaketStatusConstant.STATUS_PANITIA_REJECTED)
					|| model.latestStatus.status_paket.equalsIgnoreCase(PaketStatusConstant.STATUS_PROVIDER_REJECTED)){
				model.bolehEdit = false;
			}
		}
	}

	public static void sendMail(String email, String noPaket, String instansi, String nama_satker, String tanggal_dibuat, String harga){
		final String PAKET_EMAIL_BODY = "mail.body.paket";
		final String PAKET_EMAIL_SUBJECT ="mail.subject.paket";

		String subjectEmail = Configuration.getConfigurationValue(PAKET_EMAIL_SUBJECT);
		subjectEmail = subjectEmail.replaceAll("no_paket",noPaket);

		String bodyEmail = Configuration.getConfigurationValue(PAKET_EMAIL_BODY);
		bodyEmail = bodyEmail.replaceAll("no_paket",noPaket);
		bodyEmail = bodyEmail.replaceAll("nama_instansi",instansi);
		bodyEmail = bodyEmail.replaceAll("nama_satker",nama_satker);
		bodyEmail = bodyEmail.replaceAll("tanggal_dibuat",tanggal_dibuat);
		bodyEmail = bodyEmail.replaceAll("nilai_paket",harga);
		bodyEmail = bodyEmail.replaceAll("Rp. ","");

		String reportText = bodyEmail;

		try {
			String report = Configuration.getConfigurationValue(MailQueue.SMTP_MAIL_ADMIN, "");
			Logger.info("create mail prop job report to: " + email);
			MailQueue mq = EmailManager.createEmail(email, reportText, subjectEmail);
			mq.save();
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error("error create mail:" + e.getMessage());
		}
	}

	public static int updateNomorPaket(String no_paket, Long id){
		//update produk set jumlah_stok = (jumlah_stok-10) where id = 6905;
		String query = "update paket " +
				" set no_paket = '"+no_paket+"'" +
				" where id = "+id;

		return Query.update(query);
	}

}
