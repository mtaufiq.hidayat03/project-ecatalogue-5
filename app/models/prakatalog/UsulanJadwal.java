package models.prakatalog;

import models.BaseKatalogTable;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

/**
 * Created by raihaniqbal on 8/3/17.
 */
@Table(name = "usulan_jadwal")
public class UsulanJadwal extends BaseKatalogTable {

	@Id
	public Long id;
	public Long usulan_id;
	public Long tahapan_id;

	@Required
	public Date tanggal_mulai;
	@Required
	public Date tanggal_selesai;

	public boolean active;
	public boolean adalah_awal_penawaran = false;
	public boolean adalah_akhir_penawaran = false;

	@Transient
	public String nama_tahapan;

	@Transient
	public Integer jumlah_perubahan;


	public static List<UsulanJadwal> listJadwal(long usulan_id){
		String query = "select uj.id as id, uj.tahapan_id as tahapan_id, uj.tanggal_mulai as tanggal_mulai, " +
				"uj.tanggal_selesai as tanggal_selesai, uj.adalah_awal_penawaran as adalah_awal_penawaran, uj.adalah_akhir_penawaran as adalah_akhir_penawaran, " +
				"tahapan.nama_tahapan_alias as nama_tahapan, ujr.total as jumlah_perubahan " +
				"from usulan_jadwal uj " +
				"join tahapan on uj.tahapan_id = tahapan.id " +
				"LEFT join (SELECT tahapan_id, count(*) as total FROM usulan_jadwal_riwayat" +
				" WHERE active > 0 AND usulan_id =? GROUP BY tahapan_id) ujr on uj.tahapan_id = ujr.tahapan_id " +
				"where uj.usulan_id = ? order by uj.id asc";

		return Query.find(query,UsulanJadwal.class, usulan_id, usulan_id).fetch();
	}

	public static Date getTanggalAkhir(long usulan_id, long tahapan_id){
		UsulanJadwal uj = new UsulanJadwal();
		try{
			uj = UsulanJadwal.find("usulan_id = ? and tahapan_id = ?",usulan_id,tahapan_id).first();
			return uj.tanggal_selesai;
		}catch (Exception e){}

		return new Date();
	}

	public static Date getTanggalAwal(long usulan_id, long tahapan_id){
		try{
			UsulanJadwal uj = UsulanJadwal.find("usulan_id = ? and tahapan_id = ?",usulan_id,tahapan_id).first();
			return uj.tanggal_mulai;
		}catch (Exception e){}
		return new Date();
	}

	public static UsulanJadwal findByUsulanIdAndTahapanId(long usulan_id, long tahapan_id){
		return UsulanJadwal.find("usulan_id = ? and tahapan_id = ?",usulan_id,tahapan_id).first();
	}

	public static Date findLastDateSchedule(long usulan_id){
		String query = "select tanggal_selesai " +
						"from usulan_jadwal " +
						"where usulan_id = ? " +
						"order by tanggal_selesai desc";
		UsulanJadwal result = Query.find(query,UsulanJadwal.class, usulan_id).first();
		return result.tanggal_selesai;
	}

	public static UsulanJadwal getDateFlaggingTahapanPendaftaran(Long tahapan_id, Long usulan_id) {
	    return Query.find("select tanggal_mulai, tanggal_selesai from usulan_jadwal where tahapan_id='"+tahapan_id+"' && usulan_id='"+usulan_id+"'", UsulanJadwal.class).first();
    }

    public static UsulanJadwal getDateFlaggingTahapanPenjelasan(Long tahapan_id, Long usulan_id) {
        return Query.find("select tanggal_mulai, tanggal_selesai from usulan_jadwal where tahapan_id='"+tahapan_id+"' && usulan_id='"+usulan_id+"'", UsulanJadwal.class).first();
    }

	public static UsulanJadwal getBy(Long tahapanId, Long usulanId) {
		return Query.find(
				"SELECT * FROM usulan_jadwal where tahapan_id= ? AND usulan_id= ?",
				UsulanJadwal.class,
				tahapanId,
				usulanId
		).first();
	}

	public static UsulanJadwal getDateFlaggingTahapanNegosiasi(Long tahapan_id, Long usulan_id) {
		return Query.find("select tanggal_mulai, tanggal_selesai from usulan_jadwal where tahapan_id='"+tahapan_id+"' && usulan_id='"+usulan_id+"'", UsulanJadwal.class).first();
	}

}
