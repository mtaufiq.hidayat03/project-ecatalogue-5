package jobs.JobTrail;

import jobs.wilayah.ExtPropinsiData;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import java.util.UUID;

@NoTransaction
public class ExtPropinsiDataJob extends Job {
    String uniqueID = UUID.randomUUID().toString();
    ExtPropinsiData ext;
    int nextWaitTime;
    public ExtPropinsiDataJob(ExtPropinsiData _ext, int _nextWaitTime){
        ext = _ext;
        nextWaitTime = _nextWaitTime;
    }
    @Override
    public void before() {
        Logger.info("job id:" + uniqueID);
        super.before();
    }

    public void doJob() {
        Logger.info("on job execute, waittime:" + nextWaitTime);
        ext.getDceProvinsiData(nextWaitTime);
        Logger.info("%%%%done getDate, saving now%%%");
        ext.saveDataPropinsiToDb();
        Logger.info("done job:" + ext.propinsiList.size());
    }
}
