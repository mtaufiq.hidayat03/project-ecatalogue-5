package models.prakatalog;

import models.BaseKatalogTable;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

/**
 * Created by raihaniqbal on 8/3/17.
 */
@Table(name = "usulan_jadwal_riwayat")
public class UsulanJadwalRiwayat extends BaseKatalogTable {

	@Id
	public Long id;
	public Long user_id;
	public Long usulan_id;
	public Long tahapan_id;
	public String keterangan;

	@Required
	public Date tanggal_mulai;
	@Required
	public Date tanggal_selesai;

	public boolean active;

	@Transient
	public String nama_user;

	public UsulanJadwalRiwayat setFromUsulanJadwal(UsulanJadwal model) {
		this.usulan_id = model.usulan_id;
		this.tahapan_id = model.tahapan_id;
		this.tanggal_mulai = model.tanggal_mulai;
		this.tanggal_selesai = model.tanggal_selesai;
		this.active = true;
		return this;
	}

	public UsulanJadwalRiwayat setUser(Long userId) {
		this.user_id = userId;
		return this;
	}

	public UsulanJadwalRiwayat setNote(String note) {
		this.keterangan = note;
		return this;
	}

	public static List<UsulanJadwalRiwayat> findByUsulanAndTahapan(Long usulan_id, Long tahapan_id){
		String query = "SELECT u.nama_lengkap as nama_user, ujr.tanggal_mulai, ujr.tanggal_selesai, ujr.keterangan, ujr.created_date " +
				"FROM usulan_jadwal_riwayat ujr " +
				"JOIN user u ON ujr.user_id = u.id " +
				"WHERE ujr.usulan_id = ? AND ujr.tahapan_id = ? AND ujr.active = ? " +
				"ORDER BY ujr.created_date DESC";


		return Query.find(query, UsulanJadwalRiwayat.class, usulan_id,tahapan_id, AKTIF).fetch();
	}



}
