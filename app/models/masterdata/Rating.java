package models.masterdata;

import models.BaseKatalogTable;
import models.viewmodels.RatingVM;
import play.Logger;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.ArrayList;
import java.util.List;

@Table(name = "rating")
public class Rating extends BaseKatalogTable {

    @Id
    public Long id;

    //relasi kepada PointRating
    @Required
    public Long pointRating_id;

    @Required
    public Integer valueRating;

    //rating terhadap penyedia
    @Required
    public Long penyedia_id;

    public String feedBack;

    public static List<RatingVM> getRatingResult(){

        String query ="SELECT pr.id,pr.nama_point, pr.deskripsi, sum(r.valueRating) as jumlah, count(r.id) as banyak, ROUND(( sum(r.valueRating)/ count(r.id)) ,1)as rating " +
                " FROM point_rating pr RIGHT JOIN rating r on r.pointRating_id = pr.id WHERE r.penyedia_id =1 GROUP BY pr.id";
        List<RatingVM> result = new ArrayList<>();
        try{
            result = Query.find(query, RatingVM.class).fetch();
        }catch (Exception e){
            Logger.error("Error get RatingVM:"+e.getMessage(),e);
        }

        return result;
    }

}
