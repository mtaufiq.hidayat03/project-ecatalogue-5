package models.masterdata;

import models.BaseKatalogTable;
import play.db.jdbc.BaseTable;
import play.db.jdbc.CacheMerdeka;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by dadang on 8/7/17.
 */

@Table(name = "dce_kabupaten")
public class DceKabupaten extends BaseTable {

	@Id
	public Long id;

	public String nama;

	public Long dce_provinsi_id;

	public Long getId() {return id;}
	public String getNama() {return nama;}
	public static List<DceKabupaten> findByProvId(long provinsi_id){

		return find("provinsi_id = ?", provinsi_id).fetch();
	}

	public static DceKabupaten findByName(String name){
		return find("nama_kabupaten = ?",name).first();
	}

	public static List<DceKabupaten> findByIds(List<String> ids){
		return DceKabupaten.find("id in (" + String.join(",", ids) + ")").fetch();
	}

}
