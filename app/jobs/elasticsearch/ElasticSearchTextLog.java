package jobs.elasticsearch;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import jobs.JobTrail.SingleJobSendElastics;
import jobs.TrackableJob;
import models.elasticsearch.KategoriElastic;
import models.elasticsearch.TotalPurchased;
import models.elasticsearch.log.ProductLogJson;
import models.elasticsearch.product.ProductCategoryElastic;
import models.elasticsearch.product.ProductPriceElastic;
import models.jcommon.db.base.JdbcUtil;
import models.jcommon.util.CommonUtil;
import models.katalog.ProdukGambar;
import models.katalog.ProdukHarga;
import models.util.Config;
import models.util.produk.HargaKabupatenJson;
import models.util.produk.ProdukHargaJson;
import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import play.Logger;
import play.Play;
import play.db.jdbc.Query;
import play.libs.WS;
import repositories.produkkategori.ProdukKategoriRepository;
import utils.LogUtil;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;

public class ElasticSearchTextLog {
    static String indexFilePath = Play.applicationPath + "/logstash/index.json";
    public static String elasticsearchIndex = Config.getInstance().elasticsearchIndex;
    public static Map<Long, List<ProductCategoryElastic>> categories = new LinkedHashMap<>();
    static long tpage = 1000l;
    static ElasticConnection elasticConnection = new ElasticConnection(true);
    static Config conf = Config.getInstance();
    public static List<ProductLogJson> getProduk(long start){
        List<ProductLogJson> result = new ArrayList<>();
        String sql = "SELECT p.id, p.nama_produk, p.no_produk, p.no_produk_penyedia, \n" +
                "(CASE WHEN pn.is_umkm IS NULL THEN 0 WHEN pn.is_umkm = 0 THEN 0 ELSE 1 END) as is_umkm, \n" +
                "(CASE WHEN p.harga_utama IS NULL THEN 0 \n" +
                "ELSE p.harga_utama END) as harga_utama, \n" +
                "p.jenis_produk, p.berlaku_sampai, p.created_date, p.modified_date, p.jumlah_stok, \n" +
                "(CASE WHEN p.margin_harga > 0 THEN \n" +
                "ROUND((p.harga_utama + (p.harga_utama * p.margin_harga / 100 )), 2)\n" +
                "WHEN p.harga_utama IS NULL THEN 0 ELSE \n" +
                "p.harga_utama END) as harga_pemerintah, " +
                "k.id as komoditas_id, k.nama_komoditas, k.kelas_harga, k.apakah_iklan,\n" +
                "cat.id as produk_kategori_id, cat.nama_kategori, m.id as manufaktur_id, m.nama_manufaktur, pn.id as penyedia_id, pn.nama_penyedia,pn.produk_api_url\n" +
                "FROM produk p\n" +
                "JOIN produk_kategori cat on cat.id = p.produk_kategori_id\n" +
                "JOIN penawaran pw on pw.id = p.penawaran_id\n" +
                "JOIN penyedia_kontrak pk on pk.id = pw.kontrak_id\n" +
                "JOIN penyedia pn on pn.id = pk.penyedia_id\n" +
                "JOIN komoditas k ON p.komoditas_id = k.id\n" +
                "JOIN manufaktur m ON m.id = p.manufaktur_id\n" +
                "WHERE p.active = 1 AND p.apakah_ditayangkan = 1\n" +
                "AND k.active = 1 AND p.setuju_tolak = 'setuju'\n" +
                "AND ((p.berlaku_sampai >= CURDATE() AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1)\n" +
                "OR (p.berlaku_sampai is null AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1))\n" +
                "AND (k.apakah_stok_unlimited = 1 OR (k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0)) order by p.id asc limit "+tpage+" offset "+start;
        try {
            result = Query.find(sql, ProductLogJson.class).fetch();
        }catch (Exception e){}
        return result;
    }

    public static long countProduk(){
        String sql = "SELECT count(p.id) FROM produk p \n" +
                "JOIN produk_kategori cat on cat.id = p.produk_kategori_id\n" +
                "JOIN penawaran pw on pw.id = p.penawaran_id\n" +
                "JOIN penyedia_kontrak pk on pk.id = pw.kontrak_id\n" +
                "JOIN penyedia pn on pn.id = pk.penyedia_id\n" +
                "JOIN komoditas k ON p.komoditas_id = k.id\n" +
                "JOIN manufaktur m ON m.id = p.manufaktur_id\n" +
                "WHERE p.active = 1 AND p.apakah_ditayangkan = 1\n" +
                "AND k.active = 1 AND p.setuju_tolak = 'setuju'\n" +
                "AND ((p.berlaku_sampai >= CURDATE() AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1)\n" +
                "OR (p.berlaku_sampai is null AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1))\n" +
                "AND (k.apakah_stok_unlimited = 1 OR (k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0)) limit 0,10;";
        return Query.count(sql);
    }
    public static void loopLimitProduk(boolean overide, boolean deleteFileJson) {
        String logs = "";
        long total = 0;
        boolean connected = false;
        do{
            try{
                connected = false;
                connected = JdbcUtil.getConnection() != null;
                total = countProduk();
            }catch (Exception e){
                logs = "ERROR count produk: "+e.getMessage();
                writeLogGeneral(logs);
                Logger.info(logs);
            }
        }while (total == 0 && connected == false);
        long pages = Math.round(total/tpage);
        long number = 0;
        logs = total+" total produk";
        writeLogGeneral(logs);
        Logger.info(logs);
        categories = getCategories();
        String directory = conf.aktifUserLogTrail+"/elasticsearch/";
        directory = createSubDirDate(directory);
        //hapus index
        elasticConnection.delete();
        elasticConnection.indexing(new File(indexFilePath));
        int totalData = 0;
        for (long i = 1; i <= pages; i++){
            logs = " page :"+i+" from "+pages;
            writeLogGeneral(logs);
            Logger.info(logs);
            long start = 0; long stop = 0;
            if( i>1l) {
                start = (i*tpage);
                stop = start+tpage;
            }else {
                start = 0;
                stop = tpage;
            }
            String contentIfile = (i +".txt");
            File fileLog = new File(directory+"/"+ contentIfile);

            logs = "filejson ada :"+fileLog.exists()+", file:"+" "+contentIfile;
            writeLogGeneral(logs);
            Logger.info(logs);

            if(overide | !fileLog.exists()){
                List<ProductLogJson> list = new ArrayList<>();
                connected = false;
                do{
                    list = new ArrayList<>();
                    try {
                        connected = JdbcUtil.getConnection() != null;
                        logs = "range :"+(start +1)+", "+stop;
                        writeLogGeneral(logs);
                        list = getProduk(start);
                        list = executeProduk(list, i);
                        totalData +=list.size();
                    }catch (Exception e){
                        logs = "connected:"+connected;
                        logs +=", ERROR 2:"+e.getMessage();
                        Logger.info(logs);
                        writeLogGeneral(logs);
                    }
                }while (connected == false && list.size()==0);
                logs = "sub total data:"+list.size();
                writeLogGeneral(logs);
                Logger.info(logs);
            }else {
                logs = "get file produk json elastics existing:"+i+" from"+pages;
                Logger.info(logs);
                writeLogGeneral(logs);
                try {
                   byte[] dt = Files.readAllBytes(Paths.get(fileLog.getAbsolutePath()));
                   String sb = new String(dt);
                   int tData = new Long(tpage).intValue();
                    totalData +=tData;
                   int page = new Long(i).intValue();
                    logs = "sending file json produk page:"+i;
                    writeLogGeneral(logs);
                    Logger.info(logs);
                    SingleJobSendElastics job1 = new SingleJobSendElastics(sb,page,tData );
                    job1.in(3);
                } catch (IOException e) {
                    logs = "io exection, sending content file "+contentIfile;
                    Logger.info(logs);
                    writeLogGeneral(logs);
                }
            }
            number=i;
        }
        logs="done "+number +" from "+pages+" page , total data:"+total+", loop count estimate(with file)"+totalData;
        Logger.info(logs);
        writeLogGeneral(logs);
        //next clean file
        directory = conf.aktifUserLogTrail+"/elasticsearch/";
        directory = createSubDirDate(directory);
        File dirFile = new File(directory);
        if(dirFile.exists() && deleteFileJson){
            logs = "hapus file json existing";
            writeLogGeneral(logs);
            Logger.info(logs);
            try{
                FileUtils.deleteDirectory(dirFile);
            }catch (Exception e){
                logs = "ERROR hapus file json produk :"+ e.getMessage();
                writeLogGeneral(logs);
                Logger.info(logs);
            }
        }
        logs="FINISH LOOP execution";
        Logger.info(logs);
        writeLogGeneral(logs);
    }

    public static List<ProductLogJson> executeProduk(List<ProductLogJson> part, long page) throws SQLException{
        Gson gson = new Gson();
        StringBuilder sb = new StringBuilder();
        List<ProductLogJson> result = new ArrayList<>();
        final String productIds = getProductIds(part);
        final String productProvinsiIds = getProductAreaIds(part, "provinsi");
        final String productRegencyIds = getProductAreaIds(part, "kabupaten");

        Map<Long, List<ProductPriceElastic>> province = getProductPriceProvince(productProvinsiIds);
        Map<Long, List<ProductPriceElastic>> regencies = getProductPriceRegency(productRegencyIds);
        Map<Long, ProdukGambar> images = getProductImage(productIds);
        Map<Long, Long> totals = getTotalPurchased(productIds);
        for (ProductLogJson model : part) {
            //single send
            //StringBuilder sb = new StringBuilder();
            model.total_pembelian = totals.getOrDefault(model.id, 0L);
            if (model.isAllowedToSetGeneralPrice()) {
                model.priceList.add(new ProductPriceElastic(model));
            }
            if (province != null && province.containsKey(model.id)) {
                model.priceList.addAll(province.get(model.id));
            }
            if (regencies != null && regencies.containsKey(model.id)) {
                model.priceList.addAll(regencies.get(model.id));
            }
            if (categories.containsKey(model.produk_kategori_id)) {
                model.categories.addAll(categories.get(model.produk_kategori_id));
                model.setDefaultCategoryPosition();
            }
            if (images.containsKey(model.id)) {
                model.image_url = images.get(model.id).getActiveUrl();
            }
            model.timestamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZ")
                    .format(new java.util.Date());
            if (!model.priceList.isEmpty()) {
                // logging(gson.toJson(model));
                sb.append("{ \"create\":  { \"_index\": \""+elasticsearchIndex +"\", \"_type\": \"produk\", \"_id\": \"")
                        .append(model.id).append("\" }}");
                sb.append("\n").append(gson.toJson(model)).append("\n");
                //writeToFile(result);
                //single send
                //elasticConnection.bulkSend(sb.toString());
                result.add(model);
            }
        }
        //all send
        //elasticConnection.bulkSend(sb.toString());
        String directory = Config.getInstance().aktifUserLogTrail+"/elasticsearch/";
        directory = createSubDirDate(directory);
        File fileLog = new File(directory+"/"+ page +".txt");
        writeNioFile(fileLog, sb.toString());
        int _page = new Long(page).intValue();
        String log = "total data will be send in page:"+_page+" is "+part.size();
        writeLogGeneral(log);
        Logger.info(log);
        SingleJobSendElastics job1 = new SingleJobSendElastics(sb.toString(), _page , part.size());
        job1.in(3);
//        new TrackableJob(){
//            String _sb = sb.toString();
//            long _page = page;
//            int tdata = result.size();
//            ElasticConnection _elasticConnection = new ElasticConnection(false);
//            public void doJob(){
//                Logger.info("sending data, page:"+_page+", "+tdata+" produk");
//                _elasticConnection.bulkSend(_sb);
//            }
//        }.now();

        return result;
    }

    private static String getProductIds(List<ProductLogJson> list) {
        String result = "";
        if (!list.isEmpty()) {
            result = list.stream().map(ProductLogJson::getIdToString).collect(Collectors.joining(","));
        }
        return result;
    }
    private static String getProductAreaIds(List<ProductLogJson> list, String area) {
        if (!list.isEmpty()) {
            List<String> listIds = list.stream().parallel().filter(x -> x.kelas_harga.equals(area)).map(ProductLogJson::getIdToString).collect(Collectors.toList());
            return String.join(",", listIds);
        }
        return "";
    }
    private static Map<Long, ProdukGambar> getProductImage(String ids) {
        Map<Long, ProdukGambar> results = new HashMap<>();

            String query = "SELECT pg.produk_id, pg.dok_id_attachment, pg.file_name, pg.file_sub_location, pg.file_url \n" +
                    "FROM produk_gambar pg \n" +
                    "LEFT JOIN blob_table bb ON bb.blb_id_content = pg.dok_id_attachment \n" +
                    "WHERE pg.produk_id IN (" +ids+")\n" +
                    "AND pg.active > 0 \n" +
                    "GROUP BY pg.produk_id \n" +
                    "ORDER BY pg.created_date ASC";
            List<ProdukGambar> gambars =  Query.find(query,ProdukGambar.class).fetch();
            results = gambars.stream().collect(Collectors.toMap(ProdukGambar::getIdProduk,item-> item));
        return results;
    }

    public static Map<Long, List<ProductCategoryElastic>> getCategories(){
        Map<Long, List<ProductCategoryElastic>> results = new HashMap<>();
        try {
            if (null != JdbcUtil.getConnection()) {
                Logger.debug("produk kategori sql:"+ ProdukKategoriRepository.getCategoriesSql());
                ResultSet c = null;
                boolean connected = null != JdbcUtil.getConnection();
                do{
                    c = JdbcUtil.getConnection().createStatement().executeQuery(ProdukKategoriRepository.getCategoriesSql());
                }while (connected == false && c == null);

                if(null != c){
                    while (c.next()) {
                        KategoriElastic model = new KategoriElastic();
                        model.currentId = c.getLong("current_id");
                        model.levelOneId = c.getLong("one_id");
                        model.levelOneName = c.getString("one_name");
                        model.levelTwoId = c.getLong("two_id");
                        model.levelTwoName = c.getString("two_name");
                        model.levelThreeId = c.getLong("th_id");
                        model.levelThreeName = c.getString("th_name");
                        model.levelFourId = c.getLong("ft_id");
                        model.levelFourName = c.getString("ft_name");
                        model.levelFiveId = c.getLong("fi_id");
                        model.levelFiveName = c.getString("fi_name");
                        results.put(model.currentId, model.getCategories());
                    }
                }
            }
        } catch (SQLException e) {}
        return results;
    }

    public static Map<Long, List<ProductPriceElastic>> getProductPriceProvince(String productIds) throws SQLException {
        Map<Long, List<ProductPriceElastic>> results = new HashMap<>();

        List<ProdukHarga> produkHarga = findProdukHargaByProdukIds(productIds);
        Map<Long, List<ProdukHarga>> grPh = produkHarga.stream()
                .collect(
                        Collectors.groupingBy(
                                x -> x.produk_id
                        )
                );

        for (Map.Entry<Long, List<ProdukHarga>> entry : grPh.entrySet()) {

            NavigableMap<String, List<ProdukHarga>> grDate = (new TreeMap(entry.getValue().stream()
                    .collect(
                            Collectors.groupingBy(
                                    x -> x.getStandarDateString()
                            )
                    ))).descendingMap();

            ProdukHarga ph_utama = grDate.firstEntry().getValue().stream().parallel().filter(it -> it.harga_utama == true).findFirst().orElse(new ProdukHarga());
            ProdukHarga ph_pemerintah = grDate.firstEntry().getValue().stream().parallel().filter(it -> it.harga_pemerintah == true).findFirst().orElse(new ProdukHarga());

            List<ProdukHargaJson> harga_utama = new Gson().fromJson(ph_utama.harga, new TypeToken<List<ProdukHargaJson>>(){}.getType());
            List<ProdukHargaJson> harga_pemerintah = new Gson().fromJson(ph_pemerintah.harga, new TypeToken<List<ProdukHargaJson>>(){}.getType());
            JsonObject jsObj = CommonUtil.fromJson(ph_utama.harga_xls_json, JsonObject.class);

            //handling data kotor
            if(null == harga_utama){
                harga_utama = new ArrayList<>();
            }if(null == harga_pemerintah){
                harga_pemerintah = new ArrayList<>();
            }if(null == jsObj){
                jsObj = new JsonObject();
            }

            for(int i = 0; i < harga_utama.size(); i++){
                ProdukHargaJson phu = harga_utama.get(i);
                ProdukHargaJson php = harga_pemerintah.get(i);

                ProductPriceElastic price = new ProductPriceElastic();
                price.id = phu.provinsiId;
                price.type = ProductPriceElastic.TYPE_PROVINCE;
                price.hargaUtama = new BigDecimal(phu.harga).longValue();
                price.hargaPemerintah = new BigDecimal(php.harga).longValue();
                //Logger.info("json :"+ph_utama.harga_xls_json);
                //price.locationName = jsObj != null ? ((JsonObject)jsObj.getAsJsonArray("cost").get(i)).get("provinsi").getAsString() : "";
                //handle property ada yang Provinsi ada yang provinsi
                if( null != jsObj){
                    //((JsonObject)jsObj.getAsJsonArray("cost").get(i)).get("provinsi").getAsString()
                    try{
                        if(jsObj.getAsJsonArray("cost").size() > 0){
                            JsonObject jso = ((JsonObject)jsObj.getAsJsonArray("cost").get(i));
                            if(null != jso && jso.has("provinsi")){
                                price.locationName = jso.get("provinsi").getAsString();
                            }else if(null != jso && jso.has("Provinsi")){
                                price.locationName = jso.get("Provinsi").getAsString();
                            }else{
                                price.locationName = "";
                            }
                        }
                    }catch (Exception e){
                        LogUtil.d("Elastics dump",e);
                        price.locationName = "";
                    }
                }else{
                    price.locationName = "";
                }
                if (results.containsKey(entry.getKey())) {
                    results.get(entry.getKey()).add(price);
                } else {
                    List<ProductPriceElastic> list = new ArrayList<>();
                    list.add(price);
                    results.put(entry.getKey(), list);
                }
            }
        }
        return results;
    }

    public static  List<ProdukHarga> findProdukHargaByProdukIds(String produkIds) throws SQLException {
        String query =
                "Select ph.id, ph.produk_id, ph.tanggal_dibuat, ph.komoditas_harga_atribut_id, ph.harga, ph.harga_xls_json, kha.apakah_harga_utama harga_utama, kha.apakah_harga_pemerintah harga_pemerintah " +
                        "from produk_harga ph " +
                        "join komoditas_harga_atribut kha on ph.komoditas_harga_atribut_id = kha.id and (kha.apakah_harga_utama = 1 or kha.apakah_harga_pemerintah = 1) " +
                        "where ph.active = 1 and ph.produk_id in (" + (produkIds.isEmpty() ? "0" : produkIds) + ")";
        List<ProdukHarga> result = new ArrayList<>();
        if(null != JdbcUtil.getConnection()) {
            ResultSet c = JdbcUtil.getConnection().createStatement().executeQuery(query);
            while (c.next()) {
                ProdukHarga rec = new ProdukHarga();
                rec.id = c.getLong("id");
                rec.produk_id = c.getLong("produk_id");
                rec.tanggal_dibuat = c.getDate("tanggal_dibuat");
                rec.komoditas_harga_atribut_id = c.getLong("komoditas_harga_atribut_id");
                rec.harga = c.getString("harga");
                rec.harga_xls_json = c.getString("harga_xls_json");
                rec.harga_utama = c.getBoolean("harga_utama");
                rec.harga_pemerintah = c.getBoolean("harga_pemerintah");
                result.add(rec);
            }
        }

        return result;
    }

    public static Map<Long, List<ProductPriceElastic>> getProductPriceRegency(String productIds) throws SQLException {
        Map<Long, List<ProductPriceElastic>> results = new HashMap<>();

        List<ProdukHarga> produkHarga = findProdukHargaByProdukIds(productIds);
        Map<Long, List<ProdukHarga>> grPh = produkHarga.stream()
                .collect(
                        Collectors.groupingBy(
                                x -> x.produk_id
                        )
                );

        for (Map.Entry<Long, List<ProdukHarga>> entry : grPh.entrySet()) {
            NavigableMap<String, List<ProdukHarga>> grDate = (new TreeMap(entry.getValue().stream()
                    .collect(
                            Collectors.groupingBy(
                                    x -> x.getStandarDateString()
                            )
                    ))).descendingMap();

            ProdukHarga ph_utama = grDate.firstEntry().getValue().stream().parallel().filter(it -> it.harga_utama == true).findFirst().orElse(new ProdukHarga());
            ProdukHarga ph_pemerintah = grDate.firstEntry().getValue().stream().parallel().filter(it -> it.harga_pemerintah == true).findFirst().orElse(new ProdukHarga());

            List<HargaKabupatenJson> harga_utama = new Gson().fromJson(ph_utama.harga, new TypeToken<List<HargaKabupatenJson>>(){}.getType());
            List<HargaKabupatenJson> harga_pemerintah = new Gson().fromJson(ph_pemerintah.harga, new TypeToken<List<HargaKabupatenJson>>(){}.getType());

            JsonObject jsObj = CommonUtil.fromJson(ph_utama.harga_xls_json, JsonObject.class);

            if(null == harga_utama){
                harga_utama = new ArrayList<>();
            }if (null == harga_pemerintah){
                harga_pemerintah = new ArrayList<>();
            }if(null == jsObj){
                jsObj = new JsonObject();
            }

            for(int i = 0; i < harga_utama.size(); i++){
                HargaKabupatenJson hkj = harga_utama.get(i);
                HargaKabupatenJson hkjp = harga_pemerintah.get(i);
                ProductPriceElastic price = new ProductPriceElastic();
                price.id = hkj.kabupatenId;
                price.type = ProductPriceElastic.TYPE_REGENCY;
                price.hargaUtama = new BigDecimal(hkj.harga).longValue();
                price.hargaPemerintah = new BigDecimal(hkjp.harga).longValue();
                try {
                    price.locationName = jsObj != null ? ((JsonObject) (jsObj.getAsJsonArray("cost") == null ?  jsObj.getAsJsonArray("price") :  jsObj.getAsJsonArray("cost")).get(i)).get("Kabupaten").getAsString() : "";
                }catch(Exception e){
                }
                if (results.containsKey(entry.getKey())) {
                    results.get(entry.getKey()).add(price);
                } else {
                    List<ProductPriceElastic> list = new ArrayList<>();
                    list.add(price);
                    results.put(entry.getKey(), list);
                }
            }
        }
        return results;
    }

//    public Map<Long, Long> getTotalPurchased(String ids) throws SQLException {
//        final String query = "SELECT SUM(pp.kuantitas) as total, pp.produk_id" +
//                " FROM paket_produk pp" +
//                " JOIN paket p" +
//                " ON p.id = pp.paket_id" +
//                " WHERE p.active > 0 AND pp.active > 0" +
//                " AND pp.produk_id IN ("+ids+")" +
//                " GROUP BY pp.produk_id";
//        Map<Long, Long> results = new HashMap<>();
//        try{
//            if (isConnected()) {
//                ResultSet c = connection.createStatement().executeQuery(query);
//                while (c.next()) {
//                    Long produkId = c.getLong("produk_id");
//                    Long total = c.getLong("total");
//                    results.put(produkId, total);
//                }
//                c.close();
//            }
//        }catch (Exception e){
//            Logger.info("error getTotalPurchased:"+e.getMessage());e.printStackTrace();
//        }
//        return results;
//    }
    public static Map<Long, Long> getTotalPurchased(String ids) {
        final String query = "SELECT SUM(pp.kuantitas) as total, pp.produk_id\n" +
                "FROM paket_produk pp\n" +
                "JOIN paket p\n" +
                "ON p.id = pp.paket_id\n" +
                "WHERE p.active = 1 AND pp.active AND " +
                "pp.produk_id IN ("+ids+")\n" +
                "GROUP BY pp.produk_id";
        Map<Long, Long> results = new HashMap<>();
        List<TotalPurchased> list = Query.find(query, TotalPurchased.class).fetch();
        if (!list.isEmpty()) {
            for (TotalPurchased model : list) {
                results.put(model.produk_id, model.total);
            }
        }
        return results;
    }
//    String directory = conf.aktifUserFileLocation+"/";
//    File fileLog = new File(directory+"/"+session_id+".txt");
    public static void writeNioFile(File fileLog, String content){
        if(!fileLog.exists()){
            try {
                Files.write(Paths.get(fileLog.getPath()), (content + System.lineSeparator()).getBytes(UTF_8), StandardOpenOption.CREATE,StandardOpenOption.APPEND);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            Logger.info("produk file 1000 sudah ada");
//            try {
//                Files.write(Paths.get(fileLog.getPath()), (content + System.lineSeparator()).getBytes(UTF_8), StandardOpenOption.WRITE,StandardOpenOption.APPEND);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }
    }
    public static void createOrWriteNioFile(File fileLog, String content){
        if(!fileLog.exists()){
            try {
                Files.write(Paths.get(fileLog.getPath()), (content + System.lineSeparator()).getBytes(UTF_8), StandardOpenOption.CREATE,StandardOpenOption.APPEND);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            try {
                Files.write(Paths.get(fileLog.getPath()), (content + System.lineSeparator()).getBytes(UTF_8), StandardOpenOption.WRITE,StandardOpenOption.APPEND);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String createSubDirDate(String rootDir){
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        String strDatePath = rootDir+formatter.format(date);
        File fdir = new File(strDatePath+"/");
        if(!fdir.exists()){
            try{
                Files.createDirectories(Paths.get(fdir.getPath()));
            }catch (IOException e){}
        }
        return strDatePath;
    }

    public static Long getTotalDataElastics(){
        String urls = conf.getElasticurl()+"/produk/_count";
        WS.HttpResponse response = WS.url(urls).get();
        if(response.success()){
            String result = response.getString();
            JSONParser parser = new JSONParser();
            JSONObject json = null;
            try {
                json = (JSONObject)parser.parse(result);
                Long count = (Long)json.get("count");
                Logger.info("total data:"+count);
                return count;
            } catch (ParseException e) { }
        }else{
            Logger.info("Failed get totel");
        }
        return 0l;
    }

    public static String createFolderLogElastic(){
        String directory = conf.aktifUserLogTrail+"/elasticsearch/";
        directory = createSubDirDate(directory);
        return directory;
    }
    public static void writeLogGeneral(String logs){
        String fileLogGeneral = createFolderLogElastic()+"/logsJob.txt";
        File f = new File(fileLogGeneral);
        createOrWriteNioFile(f,logs);
    }
}
