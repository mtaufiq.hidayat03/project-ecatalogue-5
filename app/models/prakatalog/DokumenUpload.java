package models.prakatalog;

import models.BaseKatalogTable;
import models.cms.Berita;
import org.json.JSONObject;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;

/**
 * Created by dadang on 7/21/17.
 */
@Table(name = "dokumen_upload")
public class DokumenUpload extends BaseKatalogTable {

	@Id
	public Long id;

	public Long dokumen_kategori_id;

	public Long penyedia_id;

	public Long penawaran_id;

	public String nama_dokumen;

	public String file_path;

	public String data;

	public Boolean active;

	public static DokumenUpload getPreviousData(long penawaran_id, long penyedia_id, long dokumen_kategori_id){

		return DokumenUpload.find("penawaran_id = ? and dokumen_kategori_id = ? and active = 1",penawaran_id,dokumen_kategori_id).first();

	}

	public static void updateToNonActive(long id){
		String query ="UPDATE dokumen_upload SET active = 0 WHERE id = ?";
		Query.update(query,id);
	}

	public static  DokumenUpload getDataActive(long penawaran_id, long dokumen_kategori_id){
		return DokumenUpload.find("active = 1 AND penawaran_id = ? AND dokumen_kategori_id = ?", penawaran_id,dokumen_kategori_id).first();
	}
	

}
