package models.masterdata;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

@Table(name = "parameter_aplikasi")
public class ParameterAplikasi extends BaseKatalogTable {

    @Id
    public Long id;
    public String nama_parameter;
    public String isi_parameter;
    public String deskripsi;
    public boolean active;

    public static final String PARAMETER_KEPALA_LKPP = "kepala_lkpp";
    public static final String PARAMETER_LOKASI_LKPP = "lokasi_lkpp";
    public static final String PARAMETER_TELEPON_LKPP = "telepon_lkpp";
    public static final String PARAMETER_WEBSITE_LKPP = "website_lkpp";
    public static final String PARAMETER_FAKSIMILI_LKPP = "faksimili_lkpp";

    public static String findByNamaParameter(String nama){
        ParameterAplikasi p = ParameterAplikasi.find("nama_parameter = ?",nama).first();
        return p.isi_parameter;
    }

}