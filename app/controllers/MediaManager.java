package controllers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import models.util.Config;
import play.Logger;
import play.Play;
import play.mvc.Controller;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MediaManager extends BaseController {

    protected static String projectRoot = Play.applicationPath.getAbsolutePath();
    protected static String galleryPath = Config.getInstance().fileImageGallery;

    public MediaManager(){
        if(projectRoot.indexOf(".")>0){
            projectRoot = Play.applicationPath.getAbsolutePath().substring(0,Play.applicationPath.getAbsolutePath().indexOf("."));
        }
    }
    public static void index(){
        String pathuri = request.path;
        String[] patsplit = pathuri.split("/");
        Logger.info(patsplit.length+"");
        render();
    }

    public static void create(){
        String pathuri = request.path;
        String[] patsplit = pathuri.split("/");
        Logger.info(patsplit.length+"");
        render();
    }
    public static void upload(File file){

        String path = projectRoot+galleryPath+file.getName();
        File destFile = new File(path);
        //is exit.?
        if(destFile.exists()){
            //generate_uniq name
            String nameOld = file.getName().substring(0,file.getName().indexOf("."));
            String ext = file.getName().substring(file.getName().indexOf("."), file.getName().length());
            Random rand = new Random();
            int  n = rand.nextInt(1000000) + 1;
            destFile = new File(projectRoot+galleryPath+nameOld+"_"+ String.valueOf(n) +"."+ext);
        }
        file.renameTo(destFile);
        Logger.info("OKE");
        JsonObject obj = new JsonObject();
        obj.addProperty("location",galleryPath+file.getName());

        renderJSON(obj);
    }

    public static void listImageGallery(){
        File folder2 = new File(projectRoot+galleryPath) {
            @Override
            public String[] list(FilenameFilter filter) {
                String names[] = list();
                if ((names == null) || (filter == null)) {
                    return names;
                }
                List<String> v = new ArrayList<>();
                for (int i = 0 ; i < names.length ; i++) {
                    if (filter.accept(this, names[i])) {
                        String fnamePath = ""+Config.getInstance().fileImageGallery+names[i];
                        if(Config.getInstance().fileImageGallery.indexOf("/")>0){
                            fnamePath = "/"+Config.getInstance().fileImageGallery+names[i];
                        }
                        String spliter = "@@";
                        v.add(names[i]+spliter+fnamePath);
                    }
                }
                return v.toArray(new String[v.size()]);
            }

        };

        String[] listFiles = folder2.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return !new File(current, name).isDirectory();
            }
        });

        renderJSON(listFileToJson(listFiles));
    }
    private static List<JsonObject> listFileToJson(String[] names) {

        List<JsonObject> listJson = new ArrayList<>();
        JsonObject obj = new JsonObject();

        for (int i = 0 ; i < names.length ; i++) {

            String[] fnamePath = names[i].split("@@");
            obj = new JsonObject();
            obj.addProperty("title",fnamePath[0]);
            obj.addProperty("value",fnamePath[1]);

            listJson.add(obj);
        }
        return listJson;
    }


}
