package models.util.sikap;

import play.data.validation.Required;

/**
 * Created by dadang on 7/11/17.
 */
public class Pajak {

	@Required
	public long rkn_id;
	@Required
	public long pjk_id;
	@Required
	public String pjk_tahun;
	@Required
	public String pjk_tanggal;
	@Required
	public long pjk_bulan;
	@Required
	public String pjk_no;
	@Required
	public String pjk_periode;
	@Required
	public String jnp_id;
	@Required
	public String jnp_nama;

	public boolean is_verified = false;

	public String status_verifikasi;
	public String auditupdate;
	public String source_data;
	public String createdat;

	public boolean deleted = false;
	public String alasan_pembatalan = "";
	public String keterangan_pembuktian = "";

}
