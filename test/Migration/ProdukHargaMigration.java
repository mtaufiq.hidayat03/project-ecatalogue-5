package Migration;

import models.BaseKatalogTable;
import models.api.produk.Produk;
import models.katalog.ProdukHarga;

import java.sql.Timestamp;
import java.util.Date;

public class ProdukHargaMigration extends BaseKatalogTable {

    public Long produk_id;
    public Long kurs_id;
    public String harga;
    public Date tanggal_dibuat;
    public boolean apakah_disetujui;
    public Date tanggal_disetujui;
    public Date tanggal_ditolak;
    public Long setuju_tolak_oleh;
    public Long komoditas_harga_atribut_id;
    public String kelas_harga;
    public boolean active;
    public String alasan_ditolak;
    public String harga_xls_json;
    public String checked_locations_json;
    public String checked_locations_prop;
    public String checked_locations_kab;

    public ProdukHarga getAsProdukharga(){
        ProdukHarga ph = new ProdukHarga();

        ph.produk_id = produk_id;
        ph.kurs_id = kurs_id;
        ph.harga = harga;
        ph.tanggal_dibuat = tanggal_dibuat;
        ph.apakah_disetujui = apakah_disetujui;
        ph.tanggal_disetujui = tanggal_disetujui;
        ph.tanggal_ditolak = tanggal_ditolak;
        ph.setuju_tolak_oleh = setuju_tolak_oleh;
        ph.komoditas_harga_atribut_id = komoditas_harga_atribut_id;
        ph.kelas_harga = kelas_harga;
        ph.active = active;
        ph.alasan_ditolak = alasan_ditolak;
        ph.harga_xls_json = harga_xls_json;
        ph.checked_locations_json = checked_locations_json;

        return ph;
    }
}
