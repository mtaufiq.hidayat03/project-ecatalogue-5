package models.common;

import models.katalog.ProdukKategori;

import java.util.List;

/**
 * Created by dadang on 9/20/17.
 */
public class KomoditasKategori {

	public long komoditas_id;
	public String nama_komoditas;
	public String kode_komoditas;
	public List<ProdukKategori> produkKategoriList;

}
