package controllers.admin.usulan;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import controllers.BaseController;
import models.BaseKatalogTable;
import models.jcommon.datatable.DatatableQuery;
import models.user.User;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.util.TextUtils;
import utils.LogUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 12/1/2017
 */
public class UsulanDataTable extends BaseController {

    public static final String TAG = "UsulanDataTable";

    public static void cariUsulanByPokja(String keyword, String status, Long commodity){
        LogUtil.d(TAG, "keyword: " + keyword + " status: " + status + " commodity: " + commodity);
        User user = User.findById(getAktifUser().user_id);
        String usulanPokja = "";
        if(user.user_role_grup_id != 1) {
            usulanPokja =  " JOIN usulan_pokja up on up.usulan_id=usulan.id ";
        }

        DatatableQuery query = new DatatableQuery(UsulanResultHandler.DAFTAR_USULAN_POKJA)
                .select("usulan.id,usulan.komoditas_id,usulan.nama_usulan,usulan.status," +
                        "usulan.pengusul_nama,kldi.nama, k.nama_komoditas, usulan.doc_review_id")
                .from("usulan JOIN kldi on usulan.kldi_id = kldi.id JOIN komoditas k on usulan.komoditas_id = k.id " + usulanPokja);
        List<Object> params = new ArrayList<>();

        StringBuilder sb = new StringBuilder();
        if(user.user_role_grup_id==1){
            params.add(BaseKatalogTable.AKTIF);
            sb.append("k.active=1 AND usulan.active =?");
        }else {
            params.add(BaseKatalogTable.AKTIF);
            params.add(getAktifUser().user_id);
            sb.append("k.active=1 AND usulan.active =? AND up.pokja_id = ?");
        }
        if (!TextUtils.isEmpty(keyword)) {
            sb.append(" AND usulan.nama_usulan LIKE ?");
            params.add("%" + StringEscapeUtils.escapeSql(keyword) + "%");
        }
        if (!TextUtils.isEmpty(status)) {
            sb.append(" AND usulan.status = ?");
            params.add(status);
        }
        if (commodity > 0) {
            sb.append(" AND usulan.komoditas_id = ?");
            params.add(commodity);
        }
        query.where(sb.toString());
        query.params(params.toArray());
        LogUtil.d("**************************************", new Gson().toJson(query));
        renderJSON(query.executeQuery());
    }
    public static void cariPesertaByUsulan(Long id){
        LogUtil.d(TAG, "id: " + id);
        DatatableQuery query = new DatatableQuery(UsulanResultHandler.DAFTAR_USULAN_POKJA)
                .select("u.id, u.nama_lengkap")
                
                .from("usulan usl JOIN peserta_usulan pu on usl.id = pu.usulan_id JOIN user u on u.id = pu.user_id ");
        List<Object> params = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append("pu.active=1 AND usl.active =1 AND usl.id = ?");
        params.add(id);
        query.where(sb.toString());
        query.params(params.toArray());
        LogUtil.d("**************************************", new Gson().toJson(query));
        renderJSON(query.executeQuery());
    }




    public static void cariUsulan(long komoditas_id){
        JsonObject json= new DatatableQuery(UsulanResultHandler.DAFTAR_USULAN)
                .select("usulan.id,usulan.komoditas_id,usulan.nama_usulan,usulan.status," +
                        "usulan.pengusul_nama,kldi.nama")
                .from("usulan JOIN kldi on usulan.kldi_id = kldi.id")
                .where("active=? and usulan.komoditas_id = ?")
                .params(1, komoditas_id)
                .executeQuery();
        renderJSON(json);
    }

}
