package models.elasticsearch.query;

import com.google.gson.JsonObject;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class PriceLocation extends NestedObject {

    private Object value;
    private String innerHit;
    private JsonObject model = new JsonObject();

    public PriceLocation setValue(long value) {
        this.value = value;
        model.addProperty(path+"."+field, value);
        return this;
    }

    public PriceLocation setValue(String value) {
        this.value = value;
        model.addProperty(path+"."+field, value);
        return this;
    }

    public PriceLocation setField(String field) {
        this.field = field;
        return this;
    }

    public PriceLocation setInnerHit(String innerHit) {
        this.innerHit = innerHit;
        return this;
    }

    public PriceLocation setPath(String path) {
        this.path = path;
        return this;
    }

    public String getPath() {
        return this.path;
    }

    public String getInnerHit() { return this.innerHit; }

    @Override
    public JsonObject build() {
        JsonObject result = new JsonObject();
        result.add("match", model);
        return result;
    }

}
