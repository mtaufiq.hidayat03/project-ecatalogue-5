package jobs.Koneksi;

import jobs.TrackableJob;
import play.Logger;
import play.Play;
import play.db.jdbc.Query;
import play.jobs.Every;
import play.jobs.On;
import services.ManualDbConnect;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Properties;

//@Every("30s")
public class CleanSleep extends TrackableJob {
    private Connection connection;

    @Override
    public void doJob() throws Exception{
        //Logger.info("RUNNING Checkking SLeep");
        connection = ManualDbConnect.createNewConn();
        if(!connection.isClosed()){
            // belum selesai
            String query =  "SELECT p.id from information_schema.PROCESSLIST p WHERE COMMAND='Sleep'";
            //SELECT p.id from information_schema.PROCESSLIST p WHERE COMMAND='Sleep' and p.db= 'lkpp_katalog_portal_transisi_v5' and p.INFO IS NULL ORDER BY p.TIME DESC
            ResultSet rs = connection.createStatement().executeQuery(query);
            //StringBuilder sb = new StringBuilder();
            int total = 0;
            Connection ncon = ManualDbConnect.createNewConn();
            while (rs.next()){
                Long id = rs.getLong("id");
                String kills = "KILL "+id+";";
                try{
                    //Logger.info(kills);
                    ncon.createStatement().executeQuery(kills);
                    total++;
                }catch (Exception e){
                    Logger.error("error execute:"+kills+" "+e.getMessage());
                }
                //sb.append(kills);
            }
            ncon.close();
            Logger.info("Kill :"+total+" thread; ");//+ sb.toString()
            connection.close();
        }
    }
}
