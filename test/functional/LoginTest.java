package functional;

import models.jcommon.util.CommonUtil;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import play.mvc.Http;

import java.io.IOException;

/**
 * @author HanusaCloud on 5/19/2020 9:15 AM
 */
public class LoginTest extends BaseCase {

    @Before
    public void setUp() throws IOException {
        super.setUp();
    }

    // validate whether a user can successfully log-in
    // the rules are the same
    // 1. Arrange
    // 2. Act
    // 3. Assert
    @Test
    public void shouldBeAbleToLoginAsAdmin() {
       // Arrange and Act
        Http.Response response = loginAdmin();

        // assert
        String responseString = CommonUtil.toJson(response.headers);

        assertThat(responseString, Matchers.containsString("/publikctr/home"));
    }

}
