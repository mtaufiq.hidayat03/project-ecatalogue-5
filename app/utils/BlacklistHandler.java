package utils;

import java.util.List;
import play.Logger;
import java.util.stream.Collectors;

import jobs.elasticsearch.ElasticConnection;
import models.blacklist.Blacklist;
import models.jcommon.config.Configuration;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.katalog.Produk;
import models.penyedia.Penyedia;

public class BlacklistHandler {
	public void turunkanProdukBlacklistDariDaftarTayang(Blacklist obj) {
		if(obj.npwp == null || obj.npwp.isEmpty())
			return;
		Penyedia penyedia = Penyedia.getProviderByNpwp(obj.npwp);
		if(penyedia !=null){
			Long penyediaId = penyedia.id;
			List<Produk> produkList = Produk.getProdukByPenyedia(penyediaId);

			boolean isLogged=false;
			ElasticConnection elasticConnection = new ElasticConnection(isLogged);
			produkList.stream().map(p->
					"{ \"delete\":  { \"_index\": \"ecatalogue\", \"_type\": \"produk\", \"_id\": \""+
							p.id+
							"\" }}"+"\n"
			).map(s->elasticConnection.bulkDeleteProduk(s));
		}else{
			final String subject = "tidak dapat turun tayang ";
			try {
				String report = Configuration
						.getConfigurationValue(MailQueue.SMTP_MAIL_ADMIN, "");
				String[] ary = report.split(";");
				for (String to : ary) {
					String reportText = "Penyedia dengan nama "+obj.nama_penyedia+" (npwp "+obj.npwp+") telah masuk daftar hitam, npwp penyedia tidak ditemukan";
					Logger.info("gagal turun tayang:"+reportText);
					MailQueue mq = EmailManager.createEmail(to, reportText, subject);
					mq.save();
				}
			} catch (Exception e) {
				e.printStackTrace();
				Logger.error("error create mail:" + e.getMessage());
			}
		}
	}

	public void laporkanKeAdmin(Blacklist obj) {
		final String subject = "Info Daftar Hitam Baru";
		try {
			String report = Configuration
					.getConfigurationValue(MailQueue.SMTP_MAIL_ADMIN, "");
			String[] ary = report.split(";");
			for (String to : ary) {
				String reportText = "Penyedia dengan nama "+obj.nama_penyedia+" (npwp "+obj.npwp+") telah masuk daftar hitam";
				MailQueue mq = EmailManager.createEmail(to, reportText, subject);
				mq.save();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error("error create mail:" + e.getMessage());
		}
	}
}
