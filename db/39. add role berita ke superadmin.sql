INSERT into user_role_item (user_role_grup_id, role_item_id, active, created_date, created_by, modified_date, modified_by)
    VALUES
      (1, (select id from role_item where nama_role = "comBerita"), 1, curdate(), -99, curdate(), -99),
      (1, (select id from role_item where nama_role = "comBeritaAdd"), 1, curdate(), -99, curdate(), -99),
      (1, (select id from role_item where nama_role = "comBeritaEdit"), 1, curdate(), -99, curdate(), -99),
      (1, (select id from role_item where nama_role = "comBeritaDelete"), 1, curdate(), -99, curdate(), -99);
