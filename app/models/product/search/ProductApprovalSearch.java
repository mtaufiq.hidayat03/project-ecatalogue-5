package models.product.search;

import ext.contracts.ParamAble;
import org.apache.http.message.BasicNameValuePair;
import play.mvc.Scope;

import java.util.ArrayList;
import java.util.List;

public class ProductApprovalSearch implements ParamAble {

    public String keyword = "";
    public long komoditas = 0;
    public String penyedia = "";
    public String urutkan = "";
    public long uid = 0;
    public long pid = 0;
    public int page = 0;

    public ProductApprovalSearch(Scope.Params params) {
        this.keyword = !isNull(params,"keyword") ? params.get("keyword") : "";
        this.komoditas = !isNull(params, "komoditas") ? params.get("komoditas", Long.class) : 0;
        this.penyedia = !isNull(params, "penyedia") ? params.get("penyedia") : "";
        this.urutkan = !isNull(params, "urutkan") ? params.get("urutkan") : "";
        this.uid = !isNull(params, "uid") ? params.get("uid", Long.class) : 0;
        this.pid = !isNull(params, "pid") ? params.get("pid", Long.class) : 0;
        this.page = getCurrentPage(params);
    }

    @Override
    public List<BasicNameValuePair> getParams() {
        List<BasicNameValuePair> results = new ArrayList<>();
        results.add(new BasicNameValuePair("keyword", this.keyword));
        results.add(new BasicNameValuePair("komoditas", String.valueOf(this.komoditas)));
        results.add(new BasicNameValuePair("penyedia", this.penyedia));
        results.add(new BasicNameValuePair("urutkan", this.urutkan));
        results.add(new BasicNameValuePair("uid", String.valueOf(this.uid)));
        results.add(new BasicNameValuePair("pid", String.valueOf(this.pid)));
        return results;
    }

    @Override
    public int getPage() {
        return page;
    }
}
