package controllers.admin.komoditas;

import models.common.ButtonGroup;
import models.jcommon.datatable.DatatableResultsetHandler;
import models.katalog.Komoditas;
import org.apache.http.util.TextUtils;
import play.i18n.Messages;
import play.mvc.Router;
import utils.UrlUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 12/1/2017
 */
public class KomoditasResultHandler {

    public final static DatatableResultsetHandler<String[]> DAFTAR_KOMODITAS=
            new DatatableResultsetHandler("kom.id, kom.nama_komoditas, kom.kode_komoditas, kk.nama_kategori, kom.kelas_harga, kom.terkunci, kom.active , total_kategori_produk, total_kategori_produk_atribut, total_harga_atribut, edit, delete, kom.status") {

                public String[] handle(ResultSet rs) throws SQLException {
                    String[] results=new String[columns.length];
                    Map<String, Object> params = new HashMap<>();
                    params.put("id", rs.getString("kom.id"));
                    String urlProdukKategori = Router.reverse("admin.ProdukKategoriCtr.index", params).url;
                    String urlHargaAtribut = UrlUtil.builder().setUrl("admin.KomoditasAtributHargaCtr.index")
                            .setParam("komoditas_id", rs.getString("kom.id"))
                            .build();
                    String urlDetail = UrlUtil.builder().setUrl("admin.KomoditasCtr.detail")
                            .setParam("id", rs.getString("kom.id"))
                            .build();
                    String urlEdit = Router.reverse("admin.KomoditasCtr.edit", params).url;
                    String urlUpdate = Router.reverse("admin.KomoditasCtr.setKunciKomoditas").url;
                    String urlActive = Router.reverse("admin.KomoditasCtr.setAktivasiKomoditas").url;
                    boolean active = rs.getInt("active") == 1;
                    boolean terkunci = rs.getInt("kom.terkunci") == 0;
                    String classActive = active ? "class='btn btn-success btn-xs'":"class='btn btn-danger btn-xs'";
                    String classWarning = (terkunci && active) ? "class='btn btn-warning btn-xs'" : "class='btn btn-default btn-xs disabled'";
                    String classString = (terkunci && active) ? "" : "class='disabled' onclick='return false'";

                    results[0] = rs.getString("kom.id");
                    results[1] = String.format("<a href='%s'>%s</a>", urlDetail, rs.getString("kom.nama_komoditas"));
                    results[2] = rs.getString("kom.kode_komoditas");
                    results[3] = rs.getString("kk.nama_kategori");
                    results[4] = Komoditas.kelasHargaMap().get(rs.getString("kom.kelas_harga"));
                    results[5] = String.format("<a href='%s?id=%s&param=%s' %s style='width:60px'><i class='fa %s'></i> %s</a>", urlUpdate, rs.getString("kom.id"), "terkunci", classWarning, terkunci?"fa-unlock":"fa-lock", terkunci?Messages.get("tidak.label"):Messages.get("ya.label"));
                    results[6] = String.format("<a href='%s?id=%s&param=%s' %s style='width:60px'>%s</a>", urlActive, rs.getString("kom.id"), active?"non_aktif":"aktif", classActive, active? Messages.get("ya.label"):Messages.get("tidak.label"));
//                    results[7] = String.format("<a href='%s'>%s</a>", urlProdukKategori, rs.getInt("total_kategori_produk") + "/" + rs.getInt("total_kategori_produk_atribut"));
//                    results[8] = String.format("<a href='%s'>%s</a>", urlHargaAtribut, rs.getInt("total_harga_atribut"));
                    results[7] = TextUtils.isEmpty(rs.getString("kom.status")) ? Komoditas.PUBLISH : rs.getString("kom.status");
                    String btn_dropdown_open = "<div class='dropdown'><button class='btn btn-default dropdown-toggle' "
                            + "type='button' id='dropdown-option' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>"
                            + "Aksi <i class='fa fa-caret-down'></i></button>"
                            + "<ul class='dropdown-menu' aria-labelledby='dropdown-option'>";
                    String editButton = "<li %s><a href='%s'>%s</a></li>";
                    String uploadButton = "<li onclick='uploadIcon("+ rs.getString("kom.id") +");' "+ classString +"><a href='#' onclick='getIcon("+ rs.getString("kom.id") +");'>Upload</a></li>";
//                    String deleteButton = "<li %s><a href='%s'>%s</a></li>";
                    String btn_dropdown_close = "</ul></div>";

                    results[8] = String.format(btn_dropdown_open + editButton /*+ uploadButton*/ + btn_dropdown_close, classString, urlEdit, Messages.get("ubah_komoditas.label"));

                    return results;
                }

            };

    public final static DatatableResultsetHandler<String[]> DAFTAR_ATRIBUT_HARGA=
            new DatatableResultsetHandler("id,label_harga,apakah_ongkir,apakah_harga_utama,apakah_harga_retail,apakah_harga_pemerintah") {

                public String[] handle(ResultSet rs) throws SQLException {
                    String[] results=new String[columns.length];
                    Map<String, Object> params = new HashMap<>();
                    params.put("id", rs.getString("id"));
                    String urlHapus = Router.reverse("controllers.admin.KomoditasAtributHargaCtr.delete", params).url;
                    String urlEdit = Router.reverse("controllers.admin.KomoditasAtributHargaCtr.edit", params).url;

                    results[0] = rs.getString("id");
                    results[1] = rs.getString("label_harga");
                    results[2] = rs.getInt("apakah_ongkir") == 1 ? "Ya" : "Tidak";
                    results[3] = rs.getInt("apakah_harga_utama") == 1 ? "Ya" : "Tidak";
                    results[4] = rs.getInt("apakah_harga_pemerintah") == 1 ? "Ya" : "Tidak";
                    results[5] = rs.getInt("apakah_harga_retail") == 1 ? "Ya" : "Tidak";

                    List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();
                    buttonGroups.add(new ButtonGroup(Messages.get("ubah.label"),urlEdit));

                    ButtonGroup delete = new ButtonGroup(Messages.get("hapus.label"),urlHapus);
                    delete.isDelete = true;
                    buttonGroups.add(delete);

                    return results;
                }

            };

    public final static DatatableResultsetHandler<String[]> DAFTAR_TEMPLATE_JADWAL =
            new DatatableResultsetHandler("id,nama_template,agr_tahapan_jadwal,apakah_dipakai,edit,delete") {

                public String[] handle(ResultSet rs) throws SQLException {
                    String[] results = new String[columns.length];
                    results[0] = rs.getString("id");
                    Map<String, Object> map = new HashMap<>();
                    map.put("id", rs.getString("komoditas_id"));
                    map.put("template_id", results[0]);
                    String urlDelete = Router.reverse("admin.KomoditasCtr.deleteTemplateJadwal", map).url;
                    String urlEdit = Router.reverse("admin.KomoditasCtr.editTemplateJadwal", map).url;
                    String disabledClass = "";

                    results[1] = String.format("<a class='nama_template' template_id='%s'>%s</a>", rs.getLong("id"), rs.getString("nama_template"));
                    results[2] = rs.getString("agr_tahapan_jadwal");
                    results[3] = rs.getInt("apakah_dipakai") == 0 ? "Tidak" : "Ya" ;

                    if(rs.getInt("apakah_dipakai") >= 1){
                        disabledClass = "class='disabled'";
                        urlDelete ="#";
                        urlEdit = "#";
                    }

                    String btn_dropdown_open = "<div class='dropdown'><button class='btn btn-default dropdown-toggle' "
                            + "type='button' id='dropdown-option' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>"
                            + "Aksi <i class='fa fa-caret-down'></i></button>"
                            + "<ul class='dropdown-menu' aria-labelledby='dropdown-option'>";
                    String editButton = "<li %s><a href='%s'>%s</a></li>";
                    String deleteButton = "<li %s><a href='%s'>%s</a></li>";
                    String btn_dropdown_close = "</ul></div>";

                    results[4] = String.format(btn_dropdown_open + editButton + deleteButton + btn_dropdown_close,
                            disabledClass, urlEdit, "Edit Template Jadwal",
                            disabledClass, urlDelete,  "Delete Template Jadwal");

                    return results;
                }

            };


}
