package functional;

import models.aanwijzing.AanwijzingQuestion;
import org.junit.Test;

import static play.mvc.Http.Response;
import static play.mvc.Http.Request;

public class AanwijzingTest extends BaseCase {

    public AanwijzingQuestion sendRequest(String keyword) {
        Response loginResponse = loginAdmin();
        Request request = newRequest();
        request.cookies.putAll(loginResponse.cookies);
        request.method = "GET";
        request.remoteAddress = "0.0.0.0";
        GET(request,"" + keyword);
        return castObj(renderArgs("model"), AanwijzingQuestion.class);
    }

    @Test
    public void shouldBeAbleShowList() {
        AanwijzingQuestion result = sendRequest("");
        assertEquals(1, result.getQuestion());
    }

}
