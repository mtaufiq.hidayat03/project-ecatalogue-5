package controllers.admin;

import java.util.*;

import com.google.gson.Gson;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.katalog.ProdukKategori;
import models.katalog.payload.ProdukKategoriPayload;
import models.secman.Acl;
import play.Logger;
import play.i18n.Messages;
import repositories.produkkategori.ProdukKategoriRepository;
import services.produkkategori.ProdukKategoriService;
import utils.UrlUtil;


public class ProdukKategoriCtr extends BaseController {

    public static String TAG = "ProdukKategoriCtr";

    @AllowAccess({Acl.COM_KATEGORI_PRODUK})
    public static void index(long id) {
        List<ProdukKategori>  produkKategoriList = ProdukKategoriRepository.getProdukKategoriList(id);
        render("admin/ProdukKategoriCtr/index.html", produkKategoriList, id);
    }

    @AllowAccess({Acl.COM_KATEGORI_PRODUK_ADD})
    public static void create(long kid) {
    	String halaman = "Tambah Kategori Produk";
        List<ProdukKategori> results = ProdukKategoriRepository.getProdukKategoriList(kid);
        ProdukKategoriPayload produkKategoriPayload = new ProdukKategoriPayload(kid);
        render("admin/ProdukKategoriCtr/create.html", results, kid, produkKategoriPayload, halaman);
    }

    @AllowAccess({Acl.COM_KATEGORI_PRODUK_EDIT})
    public static void edit(long kid, long pkid) {
    	String halaman = "Edit Kategori Produk";
        List<ProdukKategori> results = ProdukKategoriRepository.getProdukKategoriList(kid);
        ProdukKategoriPayload produkKategoriPayload =
                new ProdukKategoriPayload(ProdukKategoriRepository.getDetail(pkid));
        render("admin/ProdukKategoriCtr/create.html", results, kid, produkKategoriPayload, halaman);
    }

    @AllowAccess({Acl.COM_KATEGORI_PRODUK})
    public static void createSubmit(long kid, ProdukKategoriPayload produkKategoriPayload) {
        Logger.debug(TAG + " " + new Gson().toJson(produkKategoriPayload));
        produkKategoriPayload.kid = kid;
        ProdukKategoriRepository.save(produkKategoriPayload);
        redirect(UrlUtil.builder()
                .setUrl("admin.ProdukKategoriCtr.index")
                .setParam("id", kid)
                .build());
    }

    @AllowAccess({Acl.COM_KATEGORI_PRODUK})
    public static void editSubmit() { render(); }

    @AllowAccess({Acl.COM_KATEGORI_PRODUK_HIRARKI})
    public static void createPosisi(long id) {
        List<ProdukKategori> results = ProdukKategoriRepository.getProdukKategoriList(id);
        render("admin/ProdukKategoriCtr/createPosisi.html", results, id);
    }

    @AllowAccess({Acl.COM_KATEGORI_PRODUK})
    public static void createPosisiSubmit(long id, String positions) {
        ProdukKategoriService.savePosition(positions, id);
        createPosisi(id);
    }

    @AllowAccess({Acl.COM_KATEGORI_PRODUK})
    public static void delete(long kid, long pkid) {
        if (ProdukKategoriService.delete(pkid)) {
            params.flash();
            flash.success(Messages.get("notif.success.delete"));
        } else {
            params.flash();
            flash.error(Messages.get("notif.failed.delete.relation"));
        }
        index(kid);
    }
}
