# User dan Role Terkait dengan Centrum dan ADP

## A. Login via SPSE

**Kelompok A**: Role di bawah ini login dari SPSE melalui link _Aplikasi E-Procurement Lainnya_:
1. PPK
2. Panitia atau ULP
3. Pejabat Pengadaan (?)

**Kelompok B**: Role di bawah ini login via ADP
1. Penyedia
2. Distributor	

**Kelompok C**: Login via Centrum 
1. Admin 
2. Auditor
3. CMS

## B. Pendaftaran User Kelompok C

User untuk katalog memiliki struktur role yang agak rumit dan tidak bisa disamakan dengan aplikasi lain. Oleh karena itu tidak perlu diakomodir oleh Centrum. Role manajemen
untuk katalog **tidak** ditangani Centrum namun dikelola oleh katalog. Centrum hanya menyimpan identitas dan password.

Untuk proses pendaftaran
1. Aplikasi katalog memyediakan halaman Tambah User, sedangkan Centrum menyediakan Web Service (WS) Undang User.
2. WS Undang User mengirim undangan ke User untuk mendaftar ke Centrum. Nantinya, user mengisi password dan identitas lengkap.
3. Admin katalog melakukan pendaftaran user di halaman Tambah User. Halaman ini berisi: nama pegawai, imel, tambah role, hak akses; namun **tidak berisi** password dan identitas lengkap pengawai. Identitas ini akan dilengkapi oleh User yang bersangkutan saat mendaftar di centrum.
   
## C. Script

Script untuk generate INSERT INTO dari Katalog ke Centrum staging.staging_users.
ROLE: Admin, CMS, Auditor (1,5,8)

```sql
select  CONCAT( 'INSERT INTO staging.staging_users (staging_user_id, user_name, nama, password_, email, role_id, tanggal_daftar, active, manage_user, app_id) \nVALUES('
, id , ',''' , user_name , ''',''' , nama_lengkap , ''',''' , user_password , ''',''' ,  user_email 
, ''',''KATALOG$' , role_basic_id , ''',''' ,  created_date , ''',true, false,''KATALOG'');\n' ) as sq
from user where role_basic_id IN (1,5,8)
```