$("#addHarga").on("click",function(){
    var str = "<tr>" +
        "<td><a class='btn btn-danger removeHarga'><i class='fa fa-trash'></i></a></td>" +
        "<td><input type='hidden' name='atributHarga.atribut_id["+index+"]' value='0' /> <input class='form-control' name='atributHarga.nama_atribut["+index+"]' required maxlength='100' /></td>" +
        "<td class='header-center'><input type ='radio' name='atributHarga.ongkir' value = '"+index+"'/></td>" +
        "<td class='header-center'><input type ='radio' name='atributHarga.harga_utama' value = '"+index+"'/></td>" +
        "<td class='header-center'><input type ='radio' name='atributHarga.harga_pemerintah' value = '"+index+"'/></td>" +
        "<td class='header-center'><input type ='radio' name='atributHarga.harga_retail' value = '"+index+"'/></td>" +
        "</tr>";

    index++;
    $("#tabel-atribut-harga").append(str);
});

$("body").on("click",".removeHarga",function(){
    if($(this).hasClass("existingData")){

        if(confirm('Atribut Harga yang akan Anda hapus adalah data yang telah tersimpan sebelumnya. Anda yakin untuk menghapus Atribut ini?')){

            var id = $(this).attr("idatribut");
            var l = Ladda.create(this);
            l.start();
            $.ajax({
                type:'post',
                url:urlDelete,
                dataType:'json',
                data:{
                    id:id
                },
                success: function(data){
                    // console.log(data);
                    if(data.status){
                        $("a[idatribut='"+data.id+"']").parent().parent().remove();
                        $("#alert-atribut-harga-delete").hide();
                    }else{
                        $("#alert-atribut-harga-delete").show();
                    }
                    l.stop();
                }
            });
        }

    }else{
        $(this).parent().parent().remove();
    }

});

function radioValidation(currentId){
    if(atributArray.indexOf(currentId) >= 0){
        $("#alert-atribut-harga-change-radio").show();
        return false;
    }else{
        $("#alert-atribut-harga-change-radio").hide();
        return true;
    }
}

$("body").on("click",".ongkirValidation",function (e) {
    e.preventDefault();
    var id = $(this).attr("idatribut");

    if(radioValidation(currentOngkir)){
        currentOngkir = id;
        $(this).prop("checked",true);
        return true;
    }


});

$("body").on("click",".utamaValidation",function (e) {
    e.preventDefault();
    var id = $(this).attr("idatribut");
    if(radioValidation(currentUtama)){
        currentUtama = id;
        return true;
    }

    return false;

});

$("body").on("click",".pemerintahValidation",function (e) {
    e.preventDefault();
    var id = $(this).attr("idatribut");
    if(radioValidation(currentPemerintah)){
        currentPemerintah = id;
        return true;
    }

    return false;

});

$("body").on("click",".retailValidation",function (e) {
    e.preventDefault();
    var id = $(this).attr("idatribut");
    if(radioValidation(currentRetail)){
        currentRetail = id;
        return true;
    }

    return false;

});

$("#removeAllHarga").on("click",function(){
    $("#tabel-atribut-harga tr").remove();
});

$("tbody#tabel-atribut-harga").on("click", "input[type='radio']", function(){
    if($(this).prop('checked') == true && $(this).attr('name') != 'atributHarga.harga_utama'){
        $(this).closest("tr").find("input[type='radio']").each(function(){
            if ($(this).attr('name') != 'atributHarga.harga_utama') {
                $(this).prop('checked', false);
            }
        });
        $(this).prop('checked',true);

    }
    else{
        $(this).closest("tr").find("input[type='radio']").each(function(){
            $(this).closest('.none').prop('checked', false);
        });
    }

    if($(this).prop('checked') == true && $(this).attr('name') == 'atributHarga.ongkir'){
        $(this).closest("tr").find("input[type='radio']").each(function(){
            $(this).prop('checked', false);
        });
        $(this).prop('checked',true);

    }

    if($(this).prop('checked') == true && $(this).attr('name') == 'atributHarga.harga_utama'){
        $(this).closest("tr").find("input[type='radio']").each(function(){
            if ($(this).attr('name') == 'atributHarga.ongkir') {
                $(this).prop('checked', false);
            }
        });
        $(this).prop('checked',true);
    }

});
