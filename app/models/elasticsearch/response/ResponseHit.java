package models.elasticsearch.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class ResponseHit {

    @SerializedName("total")
    public long total = 0;
    @SerializedName("max_score")
    public float maxScore = 0;

    @SerializedName("hits")
    public List<MetaProduct> metaProducts = new ArrayList<>();

    public boolean isEmpty() {
        return total == 0 && metaProducts.isEmpty();
    }

}
