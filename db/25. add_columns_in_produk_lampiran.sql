ALTER TABLE `produk_lampiran`
ADD COLUMN `dok_hash` VARCHAR(255);

ALTER TABLE `produk_lampiran`
ADD COLUMN `dok_signature` VARCHAR(255);

ALTER TABLE `produk_lampiran`
ADD COLUMN `dok_id_attachment` int(11);