package services.paket;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import models.common.AktifUser;
import models.purchasing.pembayaran.AcceptanceItem;
import models.purchasing.pembayaran.Pembayaran;
import models.purchasing.pembayaran.PembayaranLampiran;
import models.purchasing.pembayaran.form.AcceptanceItemJson;
import models.purchasing.pembayaran.form.PaymentDataForm;
import models.purchasing.pembayaran.form.SelectedItem;
import models.purchasing.penerimaan.PaketRiwayatPenerimaan;
import models.purchasing.pengiriman.PaketRiwayatPengiriman;
import models.purchasing.riwayat.PaketRiwayat;
import play.Logger;
import play.i18n.Messages;
import repositories.paket.AcceptanceRepository;
import repositories.paket.DeliveryRepository;
import repositories.paket.PaymentRepository;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 10/31/2017
 */
public class PaymentService extends BaseService {

    public static final String TAG = "PaymentService";

    public static PaymentService instance() {
        return new PaymentService();
    }

    public String storeGetMessage(PaymentDataForm model, Integer transactionType) {
        if (model != null) {
            Logger.debug(TAG + " " + new GsonBuilder().setPrettyPrinting().create().toJson(model));
            Pembayaran payment;
            if (model.isCreatedMode()) {
                payment = new Pembayaran(model);
                payment.transaction_type = transactionType;
            } else {
                payment = Pembayaran.findById(model.modelId);
                payment.set(model);
            }

            if((!payment.isPaymentExist() && model.isCreatedMode()) || // data baru sudah ada belum
                    (payment.isPaymentExist() && !model.isCreatedMode())){//atau data lama dan sifatnya update
                payment.savePayment();
                new PaketRiwayat().addPaymentHistory(payment);
                saveAttachment(payment, model);
                if (payment.isPayment()) {
                    deleteAcceptance(payment, model);
                    updateAcceptance(payment, model);
                } else {
                    deleteDeliveries(payment, model);
                    updateDeliveries(payment, model);
                }
                //save berhasil
                return Messages.get("notif.success.submit");
            }
        }
        //save gagal
        return Messages.get("notif.failed.save.causeexist");
    }

    public void store(PaymentDataForm model, Integer transactionType) {
        if (model != null) {
            Logger.debug(TAG + " " + new GsonBuilder().setPrettyPrinting().create().toJson(model));
            Pembayaran payment;
            if (model.isCreatedMode()) {
                payment = new Pembayaran(model);
                payment.transaction_type = transactionType;
            } else {
                payment = Pembayaran.findById(model.modelId);
                payment.set(model);
            }
            payment.savePayment();
            new PaketRiwayat().addPaymentHistory(payment);
            saveAttachment(payment, model);
            if (payment.isPayment()) {
                deleteAcceptance(payment, model);
                updateAcceptance(payment, model);
            } else {
                deleteDeliveries(payment, model);
                updateDeliveries(payment, model);
            }
        }
    }

    private void saveAttachment(Pembayaran payment, PaymentDataForm model) {
        if (model.attachment != null) {
            int position = 1;
            if(!model.isCreatedMode()) {
                position = PaymentRepository.getTotalAttachment(payment.getId(), payment.getTransactionType()) + 1;
                PaymentRepository.deactivatePaymentAttachments(payment.getId(), payment.getTransactionType());
            }

            new PembayaranLampiran()
                    .setPayment(payment)
                    .setFile(model.attachment)
                    .position(position)
                    .saveAttachment();
        }
    }

    private void updateAcceptance(Pembayaran payment, PaymentDataForm model) {
        List<SelectedItem> selectedItems = model.getItems();
        if (!selectedItems.isEmpty()) {
            final String ids = getSelectedIds(selectedItems);
            Map<Long, PaketRiwayatPenerimaan> maps = AcceptanceRepository.getMapByIds(payment.getPackageId(), ids);
            if (!maps.isEmpty()) {
                for (SelectedItem selectedItem : selectedItems) {
                    if (maps.containsKey(selectedItem.id)) {
                        AcceptanceRepository.setPaymentId(selectedItem.id, payment.getPackageId(), payment.getId());
                    }
                }
            }
        }
    }

    private void deleteAcceptance(Pembayaran payment, PaymentDataForm dataForm) {
        if (!dataForm.isCreatedMode()) {
            List<SelectedItem> deletedItems = dataForm.getDeletedItems();
            if (!deletedItems.isEmpty()) {
                final String ids = getSelectedIds(deletedItems);
                Map<Long, PaketRiwayatPenerimaan> maps = AcceptanceRepository.getMapByIds(payment.getPackageId(), ids);
                if (!maps.isEmpty()) {
                    for (SelectedItem selectedItem : deletedItems) {
                        if (maps.containsKey(selectedItem.id)) {
                            AcceptanceRepository.resetPaymentId(selectedItem.id, payment.getPackageId());
                        }
                    }
                }
            }
        }
    }

    private void deleteDeliveries(Pembayaran payment, PaymentDataForm dataForm) {
        if (!dataForm.isCreatedMode()) {
            List<SelectedItem> deletedItems = dataForm.getDeletedItems();
            if (!deletedItems.isEmpty()) {
                final String ids = getSelectedIds(deletedItems);
                Map<Long, PaketRiwayatPengiriman> maps = DeliveryRepository.getMapByIds(payment.getPackageId(), ids);
                if (!maps.isEmpty()) {
                    for (SelectedItem selectedItem : deletedItems) {
                        if (maps.containsKey(selectedItem.id)) {
                            DeliveryRepository.resetVerificationId(selectedItem.id, payment.getPackageId());
                        }
                    }
                }
            }
        }
    }

    private void updateDeliveries(Pembayaran payment, PaymentDataForm model) {
        List<SelectedItem> selectedItems = model.getItems();
        if (!selectedItems.isEmpty()) {
            final String ids = getSelectedIds(selectedItems);
            Map<Long, PaketRiwayatPengiriman> maps = DeliveryRepository.getMapByIds(payment.getPackageId(), ids);
            Logger.debug(new Gson().toJson(maps));
            if (!maps.isEmpty()) {
                for (SelectedItem selectedItem : selectedItems) {
                    if (maps.containsKey(selectedItem.id)) {
                        DeliveryRepository.setVerificationId(selectedItem.id, payment.getPackageId(), payment.getId());
                    }
                }
            }
        }
    }

    private String getSelectedIds(List<SelectedItem> selectedItems) {
        StringBuilder sb = new StringBuilder();
        if (!selectedItems.isEmpty()) {
            String glue = "";
            for (SelectedItem selectedItem : selectedItems) {
                sb.append(glue).append("'").append(selectedItem.id).append("'");
                glue = ",";
            }
        }
        return sb.toString();
    }

    public boolean deletePayment(Long paymentId, AktifUser aktifUser) {
        Logger.debug(TAG + " delete payment");
        Pembayaran model = Pembayaran.findById(paymentId);
        if (model != null && (model.isAllowedBuyer(aktifUser) || model.isAllowedProvider(aktifUser))) {
            if (model.isPayment()) {
                List<PaketRiwayatPenerimaan> results = AcceptanceRepository.getByPaymentId(model.getPackageId(), model.getId());
                if (!results.isEmpty()) {
                    for (PaketRiwayatPenerimaan acceptance : results) {
                        AcceptanceRepository.resetPaymentId(acceptance.getId(), acceptance.getPackageId());
                    }
                }
            } else {
                List<PaketRiwayatPengiriman> results = DeliveryRepository.getByPaymentId(model.getPackageId(), model.getId());
                if (!results.isEmpty()) {
                    for (PaketRiwayatPengiriman delivery : results) {
                        DeliveryRepository.resetVerificationId(delivery.getId(), delivery.getPackageId());
                    }
                }
            }
            PaymentRepository.deactivatePaymentAttachments(model.getId(), model.getTransactionType());
            model.softDelete();
            return true;
        } else {
            Logger.debug(TAG + " model null or user not allowed");
            return false;
        }
    }

    public List<AcceptanceItemJson> searchAcceptanceApi(Long packageId, String keyword, String exception) {
        List<AcceptanceItem> results = PaymentRepository.searchAcceptanceItems(packageId, keyword, generateExcludeIds(exception));
        if (!results.isEmpty()) {
            return results.stream().map(e -> new AcceptanceItemJson(e)).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    public List<AcceptanceItemJson> searchDeliveryApi(Long packageId, String keyword, String exception) {
        List<AcceptanceItem> results = PaymentRepository.searchDeliveryItems(packageId, keyword, generateExcludeIds(exception));
        if (!results.isEmpty()) {
            return results.stream().map(e -> new AcceptanceItemJson(e)).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

}
