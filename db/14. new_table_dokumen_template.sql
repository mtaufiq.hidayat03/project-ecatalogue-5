CREATE TABLE `dokumen_template`(
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`konten_kategori_id` Int(11) NOT NULL,
	`jenis_dokumen` VARCHAR(255) NOT NULL,
	`template` text NULL,
	`created_date` DateTime NULL,
	`created_by` Int( 5 ) NULL,
	`modified_date` DateTime NULL,
	`modified_by` Int( 5 ) NULL,
	`deleted_date` DateTime NULL,
	`deleted_by` Int( 5 ) NULL,
	`audittype` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`auditdate` Timestamp NULL,
	`audituser` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`active` INT(1) NULL,
	PRIMARY KEY (`id`)
)
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;

INSERT INTO `dokumen_template`
(`jenis_dokumen`)
VALUES ('BERITA ACARA NEGOSIASI'),('BERITA ACARA HASIL EVALUASI KUALIFIKASI'),('SURAT PESANAN');

