package controllers.masterdata;

import controllers.BaseController;
import controllers.security.AllowAccess;
import models.common.AktifUser;
import models.jcommon.config.Configuration;
import models.masterdata.PointRating;
import models.masterdata.Rating;
import models.secman.Acl;
import models.viewmodels.RatingVM;
import play.Logger;
import play.i18n.Messages;

import java.sql.Timestamp;
import java.util.List;

public class PointRatingCtr extends BaseController {


    @AllowAccess({Acl.COM_MASTER_POINT_RATING_ADD})
    public static void create(){
        PointRating pr = new PointRating();
        render(pr);
    }

    @AllowAccess({Acl.COM_MASTER_POINT_RATING_ADD})
    public static void save(PointRating pr){
        params.flash();
        try{
            PointRating prObj = new PointRating();
            if(pr.id != null){
                prObj = PointRating.findById(pr.id);
                prObj.modified_date = new Timestamp(new java.util.Date().getTime());
                prObj.modified_by = AktifUser.getActiveUserId();
            }else{
                prObj.created_by = AktifUser.getActiveUserId();//created by new user
            }
            prObj.nama_point = pr.nama_point;
            prObj.deskripsi = pr.deskripsi;
            prObj.save();
            flash.success(Messages.get("notif.success.submit"));
        }catch (Exception e){

            flash.error(Messages.get("notif.failed.submit"));
        }

        index();
    }

    @AllowAccess({Acl.COM_MASTER_POINT_RATING_INDEX})
    public static void edit(Long id){
        PointRating pr = PointRating.findById(id);
        render("masterdata/PointRatingCtr/create.html",pr);
    }

    public static void enableFeature(Integer aktif){
        params.flash();
        Configuration.updateConfigurationValue("rating.enable", aktif.toString());

        if(aktif>0){
            flash.success(Messages.get("enabled.feature.message"));
        }else{
            flash.success(Messages.get("disabled.feature.message"));
        }
        index();
    }
    @AllowAccess({Acl.COM_MASTER_POINT_RATING_INDEX})
    public static void index(){
        int prFitur = 0;
        try{
           prFitur = Integer.parseInt(Configuration.getConfigurationValue("rating.enable","0"));
        }catch (Exception e){}
        if(prFitur <= 0){
            renderArgs.put("prFitur","1");
            renderArgs.put("textButton",Messages.get("label_enable_fitur"));
        }else{
            renderArgs.put("prFitur","0");
            renderArgs.put("textButton",Messages.get("label_disable_fitur"));
        }
        render();
    }

    @AllowAccess({Acl.COM_MASTER_POINT_RATING_INDEX})
    public static void dataSrc(){

        renderJSON(PointRating.dataSrc());
    }

    @AllowAccess({Acl.COM_MASTER_POINT_RATING_INDEX})
    public static void detail(){

        render();
    }
    @AllowAccess({Acl.COM_MASTER_POINT_RATING_DELETE})
    public static void delete(Long id){
        params.flash();

        try{
            PointRating pr = PointRating.findById(id);
            pr.active = false;
            //pr.delete();
            flash.success(Messages.get("notif.success.delete"));
        }catch (Exception e){

            flash.error(Messages.get("notif.failed.delete"));
        }

        index();
    }

    @AllowAccess({Acl.COM_RATING_VIEW})
    public static void getRatingVM(){
        List<RatingVM> ratingResult = Rating.getRatingResult();
        for (RatingVM vm: ratingResult) {
            Logger.info(vm.deskripsi);
        }
    }
}
