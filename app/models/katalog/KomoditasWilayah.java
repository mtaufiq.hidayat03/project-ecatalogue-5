package models.katalog;

import models.BaseKatalogTable;
import models.masterdata.Kldi;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.List;

@Table(name = "komoditas_wilayah")
public class KomoditasWilayah extends BaseKatalogTable {
    @Id
    public Long id;
    public Long kbp_id;
    public Long prp_id;
    public Long komoditas_id;

    //ini hanya direferensikan, waktu ambil wilayahnya itu di kldi apa, acuanya
    //wilayah bukan kldinya, hanya kebutuhan info saja
    public String kldi_id;

    public static  boolean saveNew(String kldiwilayah, Long komoditas_id){
        try{
            Kldi kldi = Kldi.findById(kldiwilayah);
            KomoditasWilayah kw = new KomoditasWilayah();
            kw.kldi_id = kldi.id;
            kw.kbp_id = kldi.kbp_id;
            kw.prp_id = kldi.prp_id;
            kw.komoditas_id = komoditas_id;
            kw.save();
        }catch (Exception e){
            return false;
        }
        return true;
    }

    public static void deleteNotIn(Komoditas komoditas, List<String> selected){
        List<KomoditasWilayah> list = komoditas.getKomoditasWilayah();
        for (KomoditasWilayah kw: list) {
            if(!selected.contains(kw.kldi_id)){
                kw.delete();
            }
        }
    }

}
