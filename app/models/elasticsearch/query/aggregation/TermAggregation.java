package models.elasticsearch.query.aggregation;

import com.google.gson.JsonObject;
import models.elasticsearch.query.Sort;

/**
 * @author HanusaCloud on 10/10/2017
 */
public class TermAggregation {

    private String alias;
    private String field;
    private int size = 0;
    private Sort sort;
    private InnerTermAggregation innerAggregation;

    public TermAggregation setAlias(String alias) {
        this.alias = alias;
        return this;
    }

    public TermAggregation setField(String field) {
        this.field = field;
        return this;
    }

    public TermAggregation setSize(int size) {
        this.size = size;
        return this;
    }

    public TermAggregation setInnerAggregation(InnerTermAggregation aggregation) {
        this.innerAggregation = aggregation;
        return this;
    }

    public TermAggregation setSort(Sort sort) {
        this.sort = sort;
        return this;
    }

    public String getAlias() {
        return alias;
    }

    public JsonObject build() {
        JsonObject result = new JsonObject();
        JsonObject terms = new JsonObject();
        terms.addProperty("field", this.field);
        terms.addProperty("size", size);
        if (sort != null) {
            terms.add("order", sort.getSortObject());
        }
        if (innerAggregation != null) {
            result.add("aggs", innerAggregation.build());
        }
        result.add("terms", terms);
        return result;
    }

}
