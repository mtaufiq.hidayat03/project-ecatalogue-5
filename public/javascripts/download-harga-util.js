//convert ke array buffer
function s2ab(s) {
    if(typeof ArrayBuffer !== 'undefined') {
        var buf = new ArrayBuffer(s.length);
        var view = new Uint8Array(buf);
        for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    } else {
        var buf = new Array(s.length);
        for (var i=0; i!=s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    }
}
//create-workbook
function generateXlsx(data, kelasHarga, param) {

    var ws_name = "IMPORT_HARGA";

    var wscols = [
        {wch:25},
        {wch:25}
    ];

    var atributHarga = param.atributHarga;

    for(var i = 0; i < atributHarga.length; i++){
        wscols.push({wch:25});
    }

    if (param.requiredDeliveryCost) {
        wscols.push({wch:25});
    }

    var wsrows = [
        {hpt: 17},
        {hpt: 47},
    ];

    var ws_data = null;
    if(kelasHarga == 'kabupaten'){
        ws_data = defineKabData(data, param);
    } else {
        ws_data = defineProvData(data, param);
    }

    var wb = XLSX.utils.book_new();

    var ws = XLSX.utils.aoa_to_sheet(ws_data);

    ws['!merges'] = [{ s: 'A1', e: 'E1' }, { s: 'A2', e: 'E2' }, { s: 'A3', e: 'E3' }];

    ws['!cols'] = wscols;

    ws['!rows'] = wsrows;

    ws['!autofilter'] = { ref: "A4:E4" };

    XLSX.utils.book_append_sheet(wb, ws, ws_name);

    var wopts = { bookType:'xlsx', bookSST:false, type:'binary' };

    var wbout = XLSX.write(wb,wopts);

    var filename = "Template Harga Produk " +param.namaProduk+" - "+param.namaPenyedia+".xlsx";

    saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), filename);
}

function generateXlsxOngkir(data, kelasHarga, param) {

    var ws_name = "IMPORT_HARGA";

    var wscols = [
        {wch:25},
        {wch:25},
        {wch:25}
    ];

    var wsrows = [
        {hpt: 17},
        {hpt: 47}
    ];

    var ws_data = defineOngkirData(data, param);

    var wb = XLSX.utils.book_new();

    var ws = XLSX.utils.aoa_to_sheet(ws_data);

    ws['!merges'] = [{ s: 'A1', e: 'C1' }, { s: 'A2', e: 'C2' }, { s: 'A3', e: 'C3' }];

    ws['!cols'] = wscols;

    ws['!rows'] = wsrows;

    ws['!autofilter'] = { ref: "A4:C4" };

    XLSX.utils.book_append_sheet(wb, ws, ws_name);

    var wopts = { bookType:'xlsx', bookSST:false, type:'binary' };

    var wbout = XLSX.write(wb,wopts);

    var dateString = getDateString();

    var filename = "["+dateString+"] " +param.namaPenyedia+"_"+param.namaProduk+".xlsx";

    saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), filename);
}

function getDateString(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd = '0'+dd
    }

    if(mm<10) {
        mm = '0'+mm
    }

    today = dd + '/' + mm + '/' + yyyy;

    return today;
}

function defineOngkirData (data, param) {

    var disclaim = "a) Jangan merubah atau menambahkan nama column, nama provinsi, atau nama kabupaten." +
        "\nb) Daftar provinsi atau kabupaten di sini berdasarkan wilayah jual yang telah ditentukan oleh penyedia." +
        "\nc) Cell yang tidak memiliki harga, harap diisikan dengan 0 (nol).";

    var title = 'UPLOAD HARGA PRODUK '+param.namaProduk+' - '+param.namaPenyedia;

    var header = ["Provinsi", "Kabupaten","Referensi Ongkos Kirim"];


    var ws_data = [
        [title],
        [disclaim],
        [null],
        header
    ];

    // console.log(result);
    for(var i = 0; i < data.length; i++){
        var model = [data[i]['provName'], data[i]['kabName'], "0"];
        ws_data.push(model);
    }

    return ws_data;



}

function defineKabData (data, param) {

    var disclaim = "a) Jangan merubah atau menambahkan nama column, nama provinsi, atau nama kabupaten." +
        "\nb) Daftar provinsi atau kabupaten di sini berdasarkan wilayah jual yang telah ditentukan oleh penyedia." +
        "\nc) Cell yang tidak memiliki harga, harap diisikan dengan 0 (nol).";

    var title = 'UPLOAD HARGA ONGKIR '+param.namaProduk+' - '+param.namaPenyedia;
    
    var atributHarga = param.atributHarga;

    var header = ["Provinsi", "Kabupaten", "Referensi Ongkos Kirim"];
    var attrLength = atributHarga.length + 1;
    if(!param.requiredDeliveryCost){
        header.pop();
        attrLength = attrLength -1;
    }
    header = header.concat(atributHarga);

    var ws_data = [
        [title],
        [disclaim],
        [null],
        header
    ];

    

    for(var i = 0; i < data.length; i++){
        var model = [data[i]['provName'], data[i]['kabName']];
        for(var j = 0; j < attrLength; j++){
            model.push("0");
        }
        ws_data.push(model);
    }

    return ws_data;

}

function defineProvData (data, param) {

    var disclaim = "a) Jangan merubah atau menambahkan nama column, nama provinsi, atau nama kabupaten." +
        "\nb) Daftar provinsi atau kabupaten di sini berdasarkan wilayah jual yang telah ditentukan oleh penyedia." +
        "\nc) Cell yang tidak memiliki harga, harap diisikan dengan 0 (nol).";

    var title = 'UPLOAD HARGA PRODUK PROVINSI '+param.namaProduk+' - '+param.namaPenyedia;

    var header = ["Provinsi"];
    var atributHarga = param.atributHarga;

    for(i = 0; i < atributHarga.length; i++){
        header.push(atributHarga[i]);
    }

    var ws_data = [
        [title],
        [disclaim],
        [null],
        header
    ];

    for(var i = 0; i < data.length; i++){
        var model = [data[i]['provName']];
        for(var j = 0; j < atributHarga.length; j++){
            model.push("0");
        }

        ws_data.push(model);
    }

    return ws_data;

}
