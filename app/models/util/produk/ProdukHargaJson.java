package models.util.produk;

public class ProdukHargaJson {
    public long produkId;
    public long provinsiId;
    public long kabupatenId;
    public double harga_utama;
    public double harga;
    public double harga_pemerintah;
    public double ongkos_kirim;
    public String nama_produk;
    public String nama_manufaktur;
    public String unit_pengukuran;
}
