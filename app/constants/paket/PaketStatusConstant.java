package constants.paket;

import play.i18n.Messages;

/**
 * @author HanusaCloud on 11/8/2017
 */
public class PaketStatusConstant {

    public static final String STATUS_DRAFT = "draft";
    public static final String STATUS_PREPARATION = "persiapan";
    public static final String STATUS_DELIVERY = "proses_kirim";
    public static final String STATUS_NEGOTIATION = "negosiasi";
    public static final String STATUS_FINISHED = "paket_selesai";
    public static final String STATUS_PPK_APPROVED = "ppk_setuju";
    public static final String STATUS_PPK_REJECTED = "ppk_tolak";
    public static final String STATUS_PROVIDER_APPROVED = "penyedia_setuju";
    public static final String STATUS_PROVIDER_REJECTED = "penyedia_tolak";
    public static final String STATUS_PANITIA_REJECTED = "panitia_tolak";
    public static final String STATUS_PANITIA_APPROVED = "panitia_setuju";
    public static final String STATUS_CANCEL = "batal";

    public static final String USER_PROVIDER = "penyedia";
    public static final String USER_PPK = "ppk";
    public static final String USER_PANITIA = "pejabat pengadaan";
    public static final String USER_PANITIA_PP = "panitia";
    public static final String USER_DISTRIBUTOR = "distributor";

    public static final String NEGOTIATION_AGGREED = "sepakat";
    public static final String NEGOTIATION_INPROGRESS = "negosiasi";

    public static final String DESC = "desc";
    public static final String ASC = "asc";

    public static final String[] USERS = {USER_DISTRIBUTOR, USER_PANITIA, USER_PROVIDER, USER_PPK};
    public static final String[] NEGOTIATION_STATUSES = {NEGOTIATION_INPROGRESS, NEGOTIATION_AGGREED};
    public static final String[] STATUSES = {STATUS_PREPARATION, STATUS_DELIVERY, STATUS_FINISHED,
            STATUS_PPK_APPROVED, STATUS_PPK_REJECTED, STATUS_PROVIDER_APPROVED,
            STATUS_PROVIDER_REJECTED, STATUS_PANITIA_APPROVED, STATUS_PANITIA_REJECTED, STATUS_CANCEL, STATUS_DRAFT};
    public static final String[] SORTS = {DESC, ASC};

    public static String getLocalizedStatus(String status) {
        switch (status) {
            case STATUS_PANITIA_APPROVED:
                return getMessage(Messages.get("panitia_setuju.label"));
            case STATUS_PANITIA_REJECTED:
                return getMessage(Messages.get("panitia_tolak.label"));
            case STATUS_PPK_APPROVED:
                return getMessage(Messages.get("ppk_setuju.label"));
            case STATUS_PPK_REJECTED:
                return getMessage(Messages.get("ppk_tolak.label"));
            case STATUS_PROVIDER_APPROVED:
                return getMessage(Messages.get("penyedia_setuju.label"));
            case STATUS_PROVIDER_REJECTED:
                return getMessage(Messages.get("penyedia_tolak.label"));
            case STATUS_DELIVERY:
                return getMessage(Messages.get("proses_kirim.label"));
            case STATUS_PREPARATION:
                return getMessage(Messages.get("persiapan.label"));
            case STATUS_FINISHED:
                return getMessage(Messages.get("paket_selesai.label"));
            case STATUS_CANCEL:
                return getMessage(Messages.get("batal.label"));
        }
        return status;
    }

    public static String getLocalizedUser(String user) {
        switch (user) {
            case USER_PROVIDER:
                return getMessage(Messages.get("penyedia.label"));
            case USER_PANITIA:
                return getMessage(Messages.get("panitia.label"));
            case USER_DISTRIBUTOR:
                return getMessage(Messages.get("distributor.label"));
            case USER_PPK:
                return getMessage(Messages.get("ppk.label"));
        }
        return user;
    }

    public static String getKey(String append) {
//        return "paket." + append;
        return append;
    }

    public static String getMessage(String key) {
        return Messages.get(getKey(key));
    }

}
