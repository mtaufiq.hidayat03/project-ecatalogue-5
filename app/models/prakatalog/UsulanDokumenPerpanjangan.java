package models.prakatalog;

import com.google.gson.JsonObject;
import models.BaseKatalogTable;
import models.common.ButtonGroup;
import models.jcommon.datatable.DatatableQuery;
import models.jcommon.datatable.DatatableResultsetHandler;
import play.Logger;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.i18n.Messages;
import play.mvc.Router;
import services.datatable.DataTableService;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;


@Table(name = "usulan_dokumen_perpanjangan")
public class UsulanDokumenPerpanjangan extends BaseKatalogTable {

	@Id
	public Long id;

	@Required
	public Long usulan_id;

	@Required
	public String dok_label;

	public String deskripsi;
	public boolean active;

	@Transient
	private static String qField = "perp.id, perp.usulan_id, u.nama_usulan, perp.dok_label, perp.deskripsi, perp.active";

	@Transient
	private static String qFrom = "usulan_dokumen_perpanjangan perp left join usulan u on u.id = perp.usulan_id";

	@Transient
	private static String qWhere = "perp.active=1 and perp.usulan_id=?";

	public static JsonObject dataSrc(Long usulan_id){
		JsonObject result = new JsonObject();
		try {

			DatatableQuery query =
					new DatatableQuery(dataResult)
							.select(qField)
							.from(qFrom)
							.where(qWhere)
							.params(usulan_id);
			result = query.executeQuery();
		}catch (Exception e){
			Logger.error("Error getDataSrc datatable :"+ e.getMessage());
		}
		return result;
	}

	private static DatatableResultsetHandler<String[]> dataResult =
			new DatatableResultsetHandler<String[]>(qField) {
				@Override
				public String[] handle(ResultSet rs) throws SQLException {
					Map<String, Object> params = new HashMap<String, Object>();
					final String id = rs.getString("perp.id");
					params.put("id", id);
					String urlEdit = Router.reverse("controllers.admin.usulanCtr.editUsulanDokPerp",params).url;
					String urlHapus =Router.reverse("controllers.admin.usulanCtr.deleteUsulanDokPerp",params).url;

					List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();
					buttonGroups.add(new ButtonGroup(Messages.get("ubah.label"),urlEdit));
					buttonGroups.add(new ButtonGroup(Messages.get("hapus.label"),urlHapus));
					String[] results=new String[columns.length+1];
					results[0] = rs.getString("perp.id");
					results[1] = rs.getString("u.nama_usulan");
					results[2] = rs.getString("perp.dok_label");
					results[3] = rs.getString("perp.deskripsi");
					results[4] = Messages.get("tidak_aktif.label");
					if(rs.getString("perp.active").equals("1")){
						results[4] = Messages.get("aktif.label");
					}

					results[5] = DataTableService.generateButtonGroup(buttonGroups);

					return results;
				}
			};
}
