package unit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import services.prakatalog.UsulanService;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class aftreDateTest {
    public Date finish;
    public Boolean expectation;

    public aftreDateTest(Date finish, Boolean expectation) {
        this.expectation = expectation;
        this.finish = finish;
    }

    @Test
    public void checkAfterDateTest() {
        assertEquals(
                expectation,
                UsulanService.isAfter(finish)
        );
    }

    @Parameterized.Parameters
    public static Collection primeNumbers() {
        return Arrays.asList(
                new Object[][]{
                        {
                                generateDate(2020, 5, 30),
                                false
                        },
                        {
                                generateDate(2020, 6, 30),
                                false
                        },
                        {
                                generateDate(2020, 4, 20),
                                true
                        },
                        {
                                generateDate(2020, 3, 29),
                                true
                        },
                }
        );
    }

    private static Date generateDate(Integer year, Integer month, Integer day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        return calendar.getTime();
    }
}
