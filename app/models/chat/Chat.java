package models.chat;


import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

@Table(name = "chat")

public class Chat extends BaseKatalogTable {
	@Id
	public Long id;
	public Long id_pengirim;
	public Long id_penerima;
    public Long ppk_user_id;
    public Long penyedia_distributor_id;
	public int baca_penerima;
	public int baca_pengirim;
	public Long paket_id;
	public Long produk_id;
	public String file;
	public String pesan_akhir;
	public Boolean active;
	@Transient
	public int count;

	public static Chat findByIdPengirim(Long id_pengirim){

		return Chat.find("id_pengirim = ?", id_pengirim).first();
	}

	public static Chat findByIdPengirimAndIdPaket(Long id_pengirim, Long paket_id){

		return Chat.find("id_pengirim = ? and paket_id = ?", id_pengirim, paket_id).first();
	}

	public static Chat findByIdPengirimAndIdProduk(Long id_pengirim, Long produk_id){

		return Chat.find("id_pengirim = ? and produk_id = ?", id_pengirim, produk_id).first();
	}

    public static Chat findByIdPengirimAndIdProdukForLink(Long id_pengirim, Long produk_id, String vUser){

        return Chat.find("id_pengirim = ? and produk_id = ? and id_penerima = ?", id_pengirim, produk_id, vUser).first();
    }

    public static Chat findByIdPenerimaAndIdProdukForLink(Long id_penerima, Long produk_id, String vUser){

        return Chat.find("id_penerima = ? and produk_id = ? and id_pengirim = ?", id_penerima, produk_id, vUser).first();
    }

	public static Chat findByIdPenerimaAndIdProduk(Long id_penerima, Long produk_id){

		return Chat.find("id_penerima = ? and produk_id = ?", id_penerima, produk_id).first();
	}

	public static Chat findByIdPenerimaAndIdPaket(Long id_penerima, Long paket_id){

		return Chat.find("id_penerima = ? and paket_id = ?", id_penerima, paket_id).first();
	}

    public static Chat findByIdPaketPpk(Long ppk_user_id, Long paket_id){

        return Chat.find("ppk_user_id = ? and paket_id = ?", ppk_user_id, paket_id).first();
    }

	public static List<Chat> countBacaPenerima(){

		return Chat.find("baca_penerima = 0 and id_penerima = 2").fetch();
	}

	public static List<Chat> countBacaPengirim(Long id_pengirim){

		return Chat.find("baca_pengirim = 0 and id_pengirim = ?", id_pengirim).fetch();
	}


	public static List<Chat> listAllMessagePenerima(Long id_penerima){

		return Chat.find("id_penerima = ? ORDER BY modified_date desc", id_penerima).fetch();
	}

	public static List<Chat> listAllMessagePengirim(Long id_pengirim){

		return Chat.find("id_pengirim = ? ORDER BY modified_date desc", id_pengirim).fetch();
	}

    public static List<Chat> listAllMessagePpkPaket(Long ppk_user_id){

        return Chat.find("ppk_user_id = ? ORDER BY modified_date desc", ppk_user_id).fetch();
    }

    public static List<Chat> listAllMessagePpkProduk(Long ppk_user_id){

        return Chat.find("id_pengirim = ? ORDER BY modified_date desc", ppk_user_id).fetch();
    }

	public static List<Chat> FindDataNavbarCountListProduk(Long user_id){

		return Chat.find("baca_penerima = 0 and id_penerima = ? ORDER BY created_date desc LIMIT 5", user_id).fetch();
	}

	public static List<Chat> FinDataNavbarCountListProdukFromPengirim(Long user_id){

		return Chat.find("baca_pengirim = 0 and id_pengirim = ? ORDER BY created_date desc LIMIT 5", user_id).fetch();
	}

    public static long checkChatFromPenerima(long user_id){
        return Chat.count("baca_penerima = 0 and id_penerima = ? ORDER BY created_date desc", user_id);
    }

    public static long checkChatFromPengirim(long user_id){
        return Chat.count("baca_pengirim = 0 and id_pengirim = ? ORDER BY created_date desc", user_id);
    }

	public static Chat findByIdPaket(Long paket_id){

		return Chat.find("paket_id = ?", paket_id).first();
	}
}