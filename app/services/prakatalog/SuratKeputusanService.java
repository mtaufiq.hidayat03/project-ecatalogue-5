package services.prakatalog;

import models.common.DokumenPenetapanProduk;
import models.katalog.ProdukHarga;

import java.util.List;

public class SuratKeputusanService {

    public static DokumenPenetapanProduk populateDataForDocument(DokumenPenetapanProduk dok, List<ProdukHarga> listDataSK){
        StringBuilder sb = new StringBuilder();
        String tableProdukHarga = generateListProdukHarga(listDataSK);
        sb.append(tableProdukHarga);
        dok.tblProdukHarga = sb.toString();
        return dok;
    }

    public static String generateListProdukHarga(List<ProdukHarga> listTable){
        String listHargaProduk =
                "<table class='table table-bordered' align='center' border='1' cellspacing='0' style='width:100%'>" +
                "<thead><tr>" +
                    "<th style='width:5%;text-align:center'><b>No</b></th>" +
                    "<th style='width:30%;text-align:center'><b>Nama Produk</b></th>" +
                    "<th style='width:20%;text-align:center'><b>Merk</b></th>" +
                    "<th style='width:10%;text-align:center'><b>Satuan</b></th>" +
                    "<td style='width:20%;text-align:center'><b>Wilayah Jual</b></th>" +
                    "<td style='width:15%;text-align:center'><b>Harga Pemerintah (Rp)</b></th>" +
                "</tr></thead><tbody>";

        for(int i=0; i<listTable.size(); i++){
            listHargaProduk +=
                    "<tr>" +
                        "<td style='width:5%'><p style='text-align:center;margin:0;'>" + (i+1) +"</p></td>" +
                        "<td style='width:30%'><p style='text-align:left;margin:0;'>" + listTable.get(i).nama_produk + "</p></td>" +
                        "<td style='width:20%'><p style='text-align:left;margin:0;'>" + listTable.get(i).nama_manufaktur + "</p></td>" +
                        "<td style='width:10%'><p style='text-align:center;margin:0;'>" + listTable.get(i).nama_unit_pengukuran + "</p></td>" +
                        "<td style='width:20%'><p style='text-align:left;margin:0;'>" + listTable.get(i).wilayah_jual + "</p></td>" +
                        "<td style='width:15%'><p style='text-align:right;margin:0;'>" + listTable.get(i).harga + "</p></td>" +
                    "</tr>";
        }

        listHargaProduk += "</tbody></table>";

        return listHargaProduk;
    }
}
