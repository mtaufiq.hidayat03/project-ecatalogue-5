package models.prakatalog;

import models.BaseKatalogTable;
import models.prakatalog.form.FormBukaPenawaran;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;

/**
 * Created by raihaniqbal on 8/4/2017.
 */
@Table(name = "usulan_penyedia")
public class UsulanPenyedia extends BaseKatalogTable {

	@Id
	public Long id;
	@Required
	public Long usulan_id;
	@Required
	public Long user_id;
	public Long penyedia_id;
	public boolean active;

	public UsulanPenyedia() {}

	public UsulanPenyedia(Long usulanId, Long providerId, Long userId) {
		this.usulan_id = usulanId;
		this.penyedia_id = providerId;
		this.user_id = userId;
		this.active = true;
	}

	public static List<Long> findUserIdsByUsulan(Long usulan){
		String query = "select user_id from usulan_penyedia where usulan_id = ?";

		return Query.find(query,Long.class,usulan).fetch();
	}

}
