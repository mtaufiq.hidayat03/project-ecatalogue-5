package Migration;

import play.jobs.Job;
import play.jobs.On;

/** job di eksekusi setiap jam 19:47 **/
//@On("0 47 19 * * ?")
public class ExecuteWithjob extends Job {

    @Override
    public void doJob(){
        System.out.println("JOB MIGRASI NASIONAL START");
        new MigrasiHargaNasional().start();
    }
}
