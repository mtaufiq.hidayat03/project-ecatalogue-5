package models.product.search;

import ext.contracts.PaginateAble;
import ext.contracts.ParamAble;
import models.permohonan.PembaruanStaging;
import models.product.Produk;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class ProductSearchResult implements PaginateAble {

    public long total = 0;
    public List<Produk> items = new ArrayList<>();
    public ParamAble contract;


    public ProductSearchResult(ParamAble paramAble) {
        this.contract = paramAble;
    }

    @Override
    public long getTotal() {
        return total;
    }

    @Override
    public long getTotalCurrentPage() {
        return items.size();
    }

    @Override
    public int getMax() {
        return 20;
    }

    @Override
    public int getPage() {
        return contract.getPage();
    }

    @Override
    public List<BasicNameValuePair> getParams() {
        return contract.getParams();
    }

    @Override
    public void setTotal(Long total) {
        this.total = total;
    }

    public static int searchFromListStaging(List<PembaruanStaging> list, Long produk_id){
        int result = 0;
        PembaruanStaging stg = list.stream().filter(f-> f.getIfProdukStagging().produk_id.equals(produk_id)).findFirst().orElse(null);
        if(stg!= null){
            result = stg.apakah_disetujui;
        }
        return result;
    }
}
