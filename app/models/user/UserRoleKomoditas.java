package models.user;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Table(name="user_role_komoditas")
public class UserRoleKomoditas extends BaseKatalogTable {

	@Id
	public Long id;
	public boolean active;
	public Long komoditas_id;
	public Long user_role_grup_id;

	@Transient
	public String nama_komoditas;

	public UserRoleKomoditas() {}

	public UserRoleKomoditas(Long commodityId, Long groupId) {
		this.komoditas_id = commodityId;
		this.user_role_grup_id = groupId;
		this.active = true;
	}

	public static List<UserRoleKomoditas> findByUserRoleGrupId(long id){
		String query = "select uri.id, uri.komoditas_id, uri.user_role_grup_id, ri.nama_komoditas as nama_komoditas from user_role_komoditas uri " +
				"inner join komoditas ri on uri.komoditas_id = ri.id where uri.active = 1 and uri.user_role_grup_id = ?";

		return Query.find(query, UserRoleKomoditas.class, id).fetch();
	}

	public static Set<Long> findByUserRoleGrupIdMap(long id){
		List<UserRoleKomoditas> userRoleItems = findByUserRoleGrupId(id);
		return userRoleItems.stream().map(u -> u.komoditas_id).collect(
				Collectors.toSet());
	}

	public static void deleteByUserRoleGrupId(long id){
		String query = "update user_role_komoditas set active = ? where user_role_grup_id = ?";
		Query.update(query, NOT_AKTIF, id);
	}

	public static Integer getTotalCommodityByUserRoleGrupId(Long id) {
		return Query.find("SELECT COUNT(*) FROM user_role_komoditas " +
				"WHERE user_role_grup_id =? AND active =?", Integer.class, id, AKTIF).first();
	}

	public void saveRole() {
		final int result = save();
		if (this.id == null) {
			this.id = (long) result;
		}
	}

}
