package controllers.masterdata;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.jcommon.util.CommonUtil;
import models.masterdata.UnitPengukuran;
import models.secman.Acl;
import play.i18n.Messages;

import java.util.*;

public class UnitPengukuranCtr extends BaseController {

	@AllowAccess({Acl.COM_UNIT_PENGUKURAN})
	public static void index() {
		render();
	}

	@AllowAccess({Acl.COM_UNIT_PENGUKURAN_ADD})
	public static void create() { render(); }

	@AllowAccess({Acl.COM_UNIT_PENGUKURAN_ADD})
	public static void createSubmit(String[] nama_unit_arr, String[] deskrip_unit_arr) {
		checkAuthenticity();
		if(nama_unit_arr != null){

			List<Map<String, String>> unitList = new ArrayList<>();

			//validasi unit
			boolean validate = true;

			for(int i = 0; i < nama_unit_arr.length; i++){

				Map<String, String> unit = new HashMap<>();

				unit.put("unit", nama_unit_arr[i]);
				unit.put("deskripsi", deskrip_unit_arr[i]);

				unitList.add(unit);

				if(CommonUtil.isEmpty(nama_unit_arr[i])) {

					validate = false;

					validation.addError("unit_"+(i+1), "Nama Unit Pengukuran Harus Diisi", "var");

				}

			}

			if(!validate){

				if (validation.hasErrors())
					flash.error("Gagal Simpan Unit Pengukuran, periksa kembali inputan anda.");

				render("masterdata/UnitPengukuranCtr/create.html", unitList);

			}

			for(int i = 0; i < nama_unit_arr.length; i++){
				UnitPengukuran unit = new UnitPengukuran();
				unit.nama_unit_pengukuran = nama_unit_arr[i];
				unit.deskripsi = deskrip_unit_arr[i];
				unit.active = true;
				unit.save();
			}

			flash.success("Data berhasil disimpan");
			index();

		}

	}

	@AllowAccess({Acl.COM_UNIT_PENGUKURAN_EDIT})
	public static void editSubmit(long id_unit, String nama_unit, String deskripsi_unit) {
		checkAuthenticity();
		if (CommonUtil.isEmpty(nama_unit)) {
			flash.error("Gagal Ubah unit pengukuran, nama unit pengukuran tidak boleh kosong");
		} else {
			UnitPengukuran unit = UnitPengukuran.findById(id_unit);
			unit.nama_unit_pengukuran = nama_unit;
			unit.deskripsi = deskripsi_unit;
			unit.active = true;
			unit.save();
		}
		flash.success("Data berhasil disimpan");
		index();
	}

	@AllowAccess({Acl.COM_UNIT_PENGUKURAN_EDIT})
	public static void setAktifUnit(long id) {
		UnitPengukuran unit = UnitPengukuran.findById(id);
		notFoundIfNull(unit);
		unit.setAktif();
		flash.success(Messages.get("record_update_aktifnonaktif_relation_success"));
		index();
	}

	@AllowAccess({Acl.COM_UNIT_PENGUKURAN_EDIT})
	public static void setNonAktifUnit(long id) {
		UnitPengukuran unit = UnitPengukuran.findById(id);
		notFoundIfNull(unit);
		if(UnitPengukuran.getTotalRelationOnProduct(unit.id) == 0){
			unit.setNonAktif();
			flash.success(Messages.get("record_update_aktifnonaktif_relation_success"));
		}else{
			flash.error(Messages.get("record_update_aktifnonaktif_relation_failed"));
		}
		index();
	}

	@AllowAccess({Acl.COM_UNIT_PENGUKURAN_DELETE})
	public static void delete(long id) {
		UnitPengukuran model = UnitPengukuran.findById(id);
		notFoundIfNull(model);
		if (UnitPengukuran.getTotalRelationOnProduct(model.id) == 0) {
			model.softDelete();
			flash.success(Messages.get("notif.success.delete"));
		} else {
			flash.error(Messages.get("notif.failed.delete.relation"));
		}
		index();
	}

	@AllowAccess({Acl.COM_UNIT_PENGUKURAN_IMPORT})
	public static void importExcel(){
        Map<String,String> excelParams = new HashMap<String, String>();
        excelParams.put("sheetName", UnitPengukuran.EXCEL_SHEET_NAME);
        excelParams.put("nama", UnitPengukuran.EXCEL_FIELD_NAMA);
        excelParams.put("deskripsi", UnitPengukuran.EXCEL_FIELD_DESKRIPSI);

        render("masterdata/UnitPengukuranCtr/importExcel.html", excelParams);
    }

	@AllowAccess({Acl.COM_UNIT_PENGUKURAN_IMPORT})
	public static void importSubmit(String resultJson){
		checkAuthenticity();
		JsonArray results = new Gson().fromJson(resultJson, JsonArray.class);

		if(results != null){
			for(int i = 0; i < results.size(); i++){
				JsonObject obj = results.get(i).getAsJsonObject();
				UnitPengukuran unit = new UnitPengukuran();
				unit.nama_unit_pengukuran = obj.get(UnitPengukuran.EXCEL_FIELD_NAMA).getAsString();
				unit.deskripsi = obj.get(UnitPengukuran.EXCEL_FIELD_DESKRIPSI).getAsString();
				unit.active = true;

				unit.save();

			}

			flash.success("Data berhasil disimpan");
			index();
		}
		else {
			flash.error("Data gagal disimpan");
			index();
		}
	}

	private static boolean validateUnitPengukuran(String[] nama_unit_arr){

		boolean validate = true;

		for(int i = 0; i < nama_unit_arr.length; i++){

			if(CommonUtil.isEmpty(nama_unit_arr[i])) {

				validate = false;

				validation.addError("kurs_"+(i+1), "Nama Unit Pengukuran Harus Diisi", "var");

			}

		}

		return validate;

	}

}
