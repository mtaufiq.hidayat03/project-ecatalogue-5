package jobs.JobTrail;

import jobs.ExtProduct.ExtProduct;
import jobs.ExtProduct.PenyediaKomoditasApi;
import models.api.produk.HistoryDatafeed;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;

import java.util.UUID;

@NoTransaction
public class ExtProdukJob3 extends Job {
    UUID id = UUID.randomUUID();
    int _defaultWaitime = 0;
    PenyediaKomoditasApi _pk = new PenyediaKomoditasApi();
    HistoryDatafeed hdf = new HistoryDatafeed();
    int _nextWaitime = 0;
    int _seqRetry = 0;
    String _urlForExecutejob = "";

    public ExtProdukJob3(int defaultWaitime,
                         PenyediaKomoditasApi penyediaKomoditasApi, HistoryDatafeed _hdf,
                         int nextWaitime, int seqRetry, String urlForExecutejob){
        _defaultWaitime = defaultWaitime;
        _pk = penyediaKomoditasApi;
        hdf = _hdf;
        _nextWaitime = nextWaitime;
        _seqRetry = seqRetry;
        _urlForExecutejob = urlForExecutejob;
    }

    @Override
    public void doJob() {
        ExtProduct ext = new ExtProduct();
        ext.penyediaKomoditasApi = _pk;
        ext.defaultWaitime = _defaultWaitime;
        ext.nextWaitime = _nextWaitime;
        ext.seqRetry = _seqRetry;
        ext.historyDatafeed = hdf;
        ext.getData(ext.nextWaitime, _urlForExecutejob,hdf);
    }

    @Override
    public void before(){
        Logger.info("retry failed proses job, new job id:"+id+", penyedia:"+_pk.nama_penyedia);
    }
    @Override
    public void after() {
        //after finish send mail
        Logger.info(_pk.nama_penyedia+" job is done, reporting failed proses on job id:"+id);
    }
}
