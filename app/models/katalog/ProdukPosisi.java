package models.katalog;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProdukPosisi {

    public long id = 0;
    public String name = "";
    @SerializedName("parent_id")
    public long parentId = 0;
    public int depth = 0;
    public long left;
    public long right;
    public long modelId;

    public List<ProdukPosisi> children;

}
