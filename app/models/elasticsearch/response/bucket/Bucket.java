package models.elasticsearch.response.bucket;

import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 10/10/2017
 */
public class Bucket {

    @SerializedName("key")
    public long key = 0;
    @SerializedName("doc_count")
    public long doc_count = 0;
    @SerializedName("hit_list")
    public HitList hitList;

}
