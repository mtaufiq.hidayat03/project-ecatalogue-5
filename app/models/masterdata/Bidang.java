package models.masterdata;

import models.BaseKatalogTable;
import models.katalog.komoditasmodel.KomoditasTemplateJadwal;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;

@Table(name = "bidang")
public class Bidang extends BaseKatalogTable {
    @Id
    public Long id;
    public String nama_bidang;
    public boolean active;

    public static String generateName(String alias){
        String replaceSpaceWithUnderscore = alias.replace(" ","_");
        String result = replaceSpaceWithUnderscore.toLowerCase();

        return result;
    }

    public static void saveBidang(String[] nama_bidang_arr){
        if(nama_bidang_arr != null){
            for(int i = 0; i < nama_bidang_arr.length; i++){
                Bidang bidang = new Bidang();
                bidang.nama_bidang = nama_bidang_arr[i];
                bidang.active = true;
                bidang.save();
            }

        }
    }

    public static void updateBidang(long id, String nama_bidang){
        Bidang bidang = Bidang.findById(id);
        bidang.nama_bidang = nama_bidang;
        bidang.active = true;
        bidang.save();
    }

    public static void deleteBidang(long bidang_id){
        Bidang bidang = Bidang.findById(bidang_id);
        bidang.active = false;
        bidang.save();
    }

    public static List<Bidang> findAllActive(){
        String query = "select id, nama_bidang from bidang where active = 1";

        return Query.find(query, Bidang.class).fetch();
    }

}
