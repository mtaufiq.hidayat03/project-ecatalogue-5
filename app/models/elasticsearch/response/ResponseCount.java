package models.elasticsearch.response;

import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class ResponseCount {

    @SerializedName("count")
    public int count = 0;

}
