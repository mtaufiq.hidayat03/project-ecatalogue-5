package controllers.cms;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import controllers.BaseController;
import controllers.security.AllowAccess;
import ext.DatetimeBinder;
import static models.cms.KategoriKonten.*;

import models.cms.KategoriKonten;
import models.secman.Acl;
import models.cms.Faq;
import play.data.binding.As;
import play.i18n.Messages;
import utils.HtmlUtil;

/**
 * Created by user on 10/3/2017.
 */
public class FaqCtr extends BaseController {
    @AllowAccess({Acl.COM_FAQ})
    public static void index(){
        List<KategoriKonten> kategoriKontenList = findAllByKonten(3);
        render(kategoriKontenList);
    }

    @AllowAccess({Acl.COM_FAQ_ADD})
    public static void create(){
        List<KategoriKonten> kategoriKontenList = findAllByKonten(3);
        render("cms/FaqCtr/create.html", kategoriKontenList);
    }

    @AllowAccess({Acl.COM_FAQ_ADD})
    public static void createSubmit(String judul, Long kategori,
                                    @As(binder = DatetimeBinder.class)Date publish_date_from,
                                    @As(binder = DatetimeBinder.class)Date publish_date_end,
                                    String isi_konten){
        Faq faq = new Faq();
        faq.konten_kategori_id = kategori;
        faq.judul_konten = judul;
        faq.slug = judul.replaceAll("\\s","0").replaceAll("\\W","").replace("0","-");

        String decodedText = HtmlUtil.decodeFromTinymce(isi_konten);
        faq.isi_konten = decodedText;

        faq.status = "draft";
        faq.publish_date_from = publish_date_from;
        faq.publish_date_to = publish_date_end;
        faq.active = true;
        long id = faq.save();
        flash.success(Messages.get("notif.success.draft"));
        edit(id);
    }

    @AllowAccess({Acl.COM_FAQ_EDIT})
    public static void edit(Long id){
        Faq viewFaq = Faq.findById(id);
        String publish_date_from;
        String publish_date_to;

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        List<KategoriKonten> kategoriKontenList = findAllByKonten(3);
        publish_date_from = dateFormat.format(viewFaq.publish_date_from);
        if(viewFaq.publish_date_to != null){
            publish_date_to = dateFormat.format(viewFaq.publish_date_to);
        }else{
            publish_date_to = null;
        }

        render("cms/FaqCtr/edit.html", viewFaq, publish_date_from, publish_date_to, kategoriKontenList);
    }

    @AllowAccess({Acl.COM_FAQ_EDIT})
    public static void editSubmit(Long data_id, String judul, String status, Long kategori,
                                  @As(binder = DatetimeBinder.class)Date publish_date_from,
                                  @As(binder = DatetimeBinder.class)Date publish_date_end,
                                  String isi_konten){
        Faq faq = Faq.findById(data_id);
        faq.konten_kategori_id = kategori;
        faq.judul_konten = judul;
        faq.slug = judul.replaceAll("\\s","0").replaceAll("\\W","").replace("0","-");

        String decodedText = HtmlUtil.decodeFromTinymce(isi_konten);
        faq.isi_konten = decodedText;

        faq.status = status;
        faq.publish_date_from = publish_date_from;
        faq.publish_date_to = publish_date_end;
        faq.active = true;
        faq.save();
        flash.success(Messages.get("notif.success.update"));
        edit(data_id);
    }

    @AllowAccess({Acl.COM_FAQ_ADD})
    public static void detail(Long id){
        Faq faq = Faq.findById(id);
        render("cms/FaqCtr/detail.html",faq);
    }

    @AllowAccess({Acl.COM_FAQ_DELETE})
    public static void delete(Long id){
        Faq.deleteById(id);
        flash.success(Messages.get("notif.success.delete"));
        index();
    }
}
