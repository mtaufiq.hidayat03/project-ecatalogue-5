package constants.models;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class ElasticProductConstant {

    public static final String TYPE = "produk";

    public static final String ID = "id";
    public static final String PRODUK_ID = "produk_id";
    public static final String NAMA_PRODUK = "nama_produk";
    public static final String NO_PENYEDIA = "no_penyedia";
    public static final String NAMA_PENYEDIA = "nama_penyedia";
    public static final String NO_PRODUK = "no_produk";
    public static final String NO_PRODUK_PENYEDIA = "no_produk_penyedia";
    public static final String NAMA_KOMODITAS = "nama_komoditas";
    public static final String ID_KOMODITAS = "komoditas_id";
    public static final String ID_PENYEDIA = "penyedia_id";
    public static final String IMAGE_URL = "image_url";
    public static final String NAMA_MANUFAKTUR = "nama_manufaktur";
    public static final String MANUFAKTUR_ID = "manufaktur_id";
    public static final String TOTAL_PEMBELIAN = "total_pembelian";
    public static final String JENIS_PRODUK = "jenis_produk";
    public static final String STOCK = "stock";
    public static final String IS_UMKM = "is_umkm";

    public static final String PRICE_LIST = "priceList";
    public static final String PRICE_HARGA_UTAMA = "harga_utama";
    public static final String PRICE_HARGA_PEMERINTAH = "harga_pemerintah";

    public static final String CATEGORIES = "categories";
    public static final String CATEGORY_NAME = CATEGORIES+".name";

    public static final String STATUS = "status";

}
