package models.util.sikap;

/**
 * Created by dadang on 7/11/17.
 */
public class TenagaAhliDetail {

	public long cva_id;
	public long sta_id;
	public boolean deleted;
	public String dts_uraian;
	public String dts_waktu;
	public String dts_kategori;
	public String auditupdate;
	public String createdAt;

}
