package models.util.sikap;

import play.data.validation.Required;

/**
 * Created by dadang on 7/11/17.
 */
public class Pemilik {

	@Required
	public long rkn_id;
	@Required
	public long pml_id;
	@Required
	public long pml_saham;
	@Required
	public String pml_satuan;
	@Required
	public String pml_nama;
	@Required
	public String pml_alamat;
	@Required
	public String pml_npwp;
	public String status_verifikasi;

	public String auditupdate;
	public String source_data;
	public String createdat;
	public boolean is_verified = false;

	public boolean deleted = false;
	public String alasan_pembatalan = "";
	public String keterangan_pembuktian = "";

}
