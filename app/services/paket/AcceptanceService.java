package services.paket;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import models.common.AktifUser;
import models.purchasing.Paket;
import models.purchasing.PaketProduk;
import models.purchasing.base.BaseProductForm;
import models.purchasing.base.ProductJson;
import models.purchasing.pembayaran.AcceptanceItem;
import models.purchasing.penerimaan.LampiranPenerimaan;
import models.purchasing.penerimaan.ProdukPenerimaan;
import models.purchasing.penerimaan.form.AcceptanceDataForm;
import models.purchasing.penerimaan.PaketRiwayatPenerimaan;
import models.purchasing.pengiriman.PaketRiwayatPengiriman;
import models.purchasing.pengiriman.ProdukPengiriman;
import models.purchasing.pengiriman.form.DeliveryItemJson;
import models.purchasing.riwayat.PaketRiwayat;
import play.Logger;
import play.db.jdbc.Query;
import repositories.paket.AcceptanceRepository;
import repositories.paket.PackageProductRepository;
import repositories.paket.PackageRepository;
import utils.LogUtil;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 10/23/2017
 */
public class AcceptanceService extends BaseService {

    public static final String TAG = "AcceptanceService";

    public static AcceptanceService instance() {
        return new AcceptanceService();
    }

    public void store(Long package_id, AcceptanceDataForm dataForm) {
        if (dataForm != null) {
            List<BaseProductForm> acceptedProducts = dataForm.getSavedProducts();
            deleteProductsOnUpdate(dataForm);
            dataForm.totalPenerimaan(AcceptanceRepository.getCurrentTotalDelivered(package_id));
            PaketRiwayatPenerimaan model;
            if (dataForm.isCreateMode()) {
                model = new PaketRiwayatPenerimaan(dataForm);
            } else {
                model = PaketRiwayatPenerimaan.findById(dataForm.model);
                model.dataForm(dataForm);
            }
            setDelivery(model, dataForm);
            model.saveHistory();
            new PaketRiwayat().addAcceptanceHistory(model);
            saveAttachment(model, dataForm);
            saveProducts(model, dataForm, acceptedProducts);
            updateTotalAccepted(dataForm);
            updateTotalPrice(model);
        }
    }

    private void updateTotalPrice(PaketRiwayatPenerimaan model) {
         Long paket_pengiriman_id = model.paket_pengiriman_id;
         Double total = getTotalHargaUntukPengiriman(paket_pengiriman_id);
         Logger.debug(TAG, "total = "+total);
         model.agr_total_harga_diterima_dan_ongkir = total;
         model.save();

         Long paket_id = model.paket_id;
         Double total2 = getTotalHargaUntukPaket(paket_id);
         Logger.debug(TAG, "total2 = "+total2);
         Paket paket = Paket.findById(paket_id);
         paket.agr_total_harga_diterima_dan_ongkir = total2;
         paket.save();
    }

	public Double getTotalHargaUntukPaket(Long paket_id) {
		String query2 = "select coalesce(sum(pp.`agr_total_harga_diterima_dan_ongkir`),0) as harga_dengan_ongkir \n" + 
         		"FROM  paket p \n" + 
         		"JOIN `paket_penerimaan` pp on pp.paket_id=p.id \n" + 
         		"where p.id=? " + 
         		"";
         List<Double> totals2 = Query.find(query2, Double.class, paket_id).fetch();
         Double total2 = totals2.isEmpty() ? null : totals2.get(0);
		return total2;
	}

	public Double getTotalHargaUntukPengiriman(Long paket_pengiriman_id) {
		String query = "select ap.harga + coalesce(kr.ongkir,0) as harga_dengan_ongkir\n" + 
         		"FROM `paket_penerimaan` pp \n" + 
         		"JOIN `paket_pengiriman` kr on pp.`paket_pengiriman_id`=kr.id \n" + 
         		"join\n" + 
         		"(select acp.paket_penerimaan_id, sum(acp.kuantitas*pro.harga_satuan) harga\n" + 
         		"from paket_penerimaan_produk acp\n" + 
         		"join paket_produk pro ON acp.paket_produk_id = pro.id WHERE acp.active > 0 AND pro.active > 0\n" + 
         		"group by acp.paket_penerimaan_id) ap ON ap.paket_penerimaan_id = pp.id \n" + 
         		"where pp.id=?";
         final List<Double> totals = Query.find(query, Double.class, paket_pengiriman_id).fetch();
         Double total = totals.isEmpty() ? null : totals.get(0);
		return total;
	}

	private void setDelivery(PaketRiwayatPenerimaan model, AcceptanceDataForm dataForm) {
        final DeliveryItemJson delivery = dataForm.getSavedDelivery();
        Logger.debug(" json " + new Gson().toJson(delivery));
        if (delivery != null) {
            final PaketRiwayatPengiriman savedDelivery = PaketRiwayatPengiriman.findById(delivery.id);
            if (savedDelivery != null) {
                model.setDeliveryId(savedDelivery.id);
                model.deliveryId(savedDelivery);
            }
        }
    }
    private void saveProducts(PaketRiwayatPenerimaan model,
                                     AcceptanceDataForm dataForm,
                                     List<BaseProductForm> acceptedProducts) {
        Map<Long, PaketProduk> packageProducts = PackageProductRepository
                .getProducts(dataForm.packageId, dataForm.getIds(acceptedProducts));
        LogUtil.d(TAG, "total package product: " + packageProducts.size());
        if (!acceptedProducts.isEmpty()) {
            Map<Long, ProdukPenerimaan> saved = null;
            if (!dataForm.isCreateMode()) {
                saved = AcceptanceRepository.getCurrentProductsIdKey(dataForm.packageId, dataForm.model);
            }
            for (BaseProductForm productForm : acceptedProducts) {
                PaketProduk paketProduk = packageProducts.get(productForm.packageProductId);
                if (paketProduk != null) {
                    if (productForm.isCreatedMode()) {
                        Logger.debug("create mode");
                        new ProdukPenerimaan()
                                .paketProduk(paketProduk)
                                .dataForm(productForm)
                                .penerimaan(model)
                                .saveProduct();
                    } else {
                        Logger.debug("update mode");
                        if (saved != null && saved.containsKey(productForm.modelId)) {
                            ProdukPenerimaan produkPenerimaan = saved.get(productForm.modelId);
                            produkPenerimaan.dataForm(productForm).saveProduct();
                        }
                    }
                }
            }
        }
    }

    private void saveAttachment(PaketRiwayatPenerimaan model, AcceptanceDataForm dataForm) {
        if (dataForm.attachment != null) {
            int position = 1;
            if (!dataForm.isCreateMode()) {
                position = AcceptanceRepository
                        .getAcceptanceAttachments(dataForm.packageId, dataForm.model).size() + 1;
                AcceptanceRepository.deactivateAttachments(dataForm.model);
            }
            new LampiranPenerimaan()
                    .setPaketData(model)
                    .dataForm(dataForm)
                    .position(position)
                    .saveAttachment();
        }
    }

    private void updateTotalAccepted(AcceptanceDataForm dataForm) {
        final Integer total = AcceptanceRepository.getTotalProductFullyAccepted(dataForm.packageId);
        PackageRepository.updateTotalProductCompleted(dataForm.packageId, total);
    }

    private void deleteProductsOnUpdate(AcceptanceDataForm model) {
        if (!model.isCreateMode()) {
            List<BaseProductForm> deleted = model.getDeletedProducts();
            if (!deleted.isEmpty()) {
                Logger.debug("deleted not empty deleting...");
                AcceptanceRepository
                        .deactivateAllProducts(model.packageId,
                                model.model,
                                model.getIds(deleted)
                        );
            }
        }
    }

    public boolean deleteAcceptance(Long acceptanceId, AktifUser aktifUser) {
        Logger.debug(TAG + " delete acceptance");
        PaketRiwayatPenerimaan model = PaketRiwayatPenerimaan.findById(acceptanceId);
        if (model != null && model.isUserAllowed(aktifUser)) {
            model.softDelete();
            AcceptanceRepository.deactivateAttachments(model.id);
            AcceptanceRepository.deactivateProducts(model.id);
            return true;
        } else {
            Logger.debug(TAG + " model null or user not allowed");
            return false;
        }
    }

    public List<ProductJson> getProducts(Long packageId, String keyword, String excepted) {
        List<ProdukPenerimaan> products = AcceptanceRepository
                .searchProductsToBeAccepted(packageId, keyword, generateExcludeIds(excepted));
        return products.stream().map(p -> new ProductJson(p)).collect(Collectors.toList());
    }

}
