@echo off
setLocal EnableDelayedExpansion
SET PROJECT_DIR=D:\web\projects\e-katalog5
SET PLAY_DIR=D:\web\projects\play

SET UNIT_SOURCE=%PROJECT_DIR%\test\unit
SET FUNCTIONAL_SOURCE=%PROJECT_DIR%\test\functional
SET TEST_DIR=%PROJECT_DIR%\compiled-test
SET UNIT_DIR=%TEST_DIR%\unit
SET FUNCTIONAL_DIR=%TEST_DIR%\functional
SET TEST_RESULT=%PROJECT_DIR%\test-result
SET SOURCE_FILE=%PROJECT_DIR%\sources.txt
SET PRECOMPILED_DIR=%PROJECT_DIR%\precompiled\java\
SET CLASSPATH='

ECHO %1%

if not exist %TEST_DIR% mkdir %TEST_DIR%
if not exist %TEST_RESULT% mkdir %TEST_RESULT%
if "%1%"=="clean" (
    ECHO "clean.. delete precompiled directory"
	if exist %PRECOMPILED_DIR% ( 
		RD /S /Q "%PRECOMPILED_DIR%"
	)
)

if not exist %PRECOMPILED_DIR% (
	ECHO "build app first"
	%PLAY_DIR%\play precompile
)

CALL :run_unit_test
CALL :run_functional_test

:init_class_path
	for /R %PLAY_DIR%\framework\lib\ %%a in (*.jar) do (
		SET CLASSPATH=!CLASSPATH!;%%a
	)

	for /R %PROJECT_DIR%\lib\ %%a in (*.jar) do (
		SET CLASSPATH=!CLASSPATH!;%%a
	)

	SET CLASSPATH=!CLASSPATH!;%PLAY_DIR%\framework\play-lkpp.jar;%PROJECT_DIR%\junit\junit-platform-console-standalone-1.6.2.jar;%TEST_DIR%;%PRECOMPILED_DIR%;!PROJECT_DIR!\junit\junit-platform-console-standalone-1.6.2.jar;'
    GOTO :END
EXIT /B

:compile_test
	ECHO 'list test dir and dump to a file'
	dir /s /B %UNIT_SOURCE%\*.java >> %SOURCE_FILE%
	dir /s /B %FUNCTIONAL_SOURCE%\*.java >> %SOURCE_FILE%
	ECHO 'compile test classes..'
	javac -cp %CLASSPATH% @%SOURCE_FILE% -d %TEST_DIR%
	DEL %SOURCE_FILE%
    GOTO :END
EXIT /B


:run_unit_test
	CALL :init_class_path
	CALL :compile_test
	ECHO "run unit testing"
	for /R %UNIT_DIR% %%a in (*.class) do (
		SET FILE=%%~nfa
		SET ONE_TEMP=!FILE:%TEST_DIR%\=!
		SET FILENAME=!ONE_TEMP:\=.!
		SET RESULT=!FILENAME:.class=!
		java -cp %CLASSPATH% org.junit.platform.console.ConsoleLauncher -c !RESULT! --reports-dir=!TEST_RESULT!\!RESULT!
	)
    GOTO :END
EXIT /B

:run_functional_test
	ECHO "run unit testing"
	for /R %FUNCTIONAL_DIR% %%a in (*.class) do (
		SET FILE=%%~nfa
		SET ONE_TEMP=!FILE:%TEST_DIR%\=!
		SET FILENAME=!ONE_TEMP:\=.!
		SET RESULT=!FILENAME:.class=!
		java -ea -Dplay.id=test. -cp %CLASSPATH% org.junit.platform.console.ConsoleLauncher -c !RESULT! --reports-dir=!TEST_RESULT!\!RESULT!
	)
    GOTO :END
EXIT /B

:END

EXIT /B

