package models.purchasing.search;

import ext.contracts.PaginateAble;
import ext.contracts.ParamAble;
import models.penyedia.PenyediaDistributor;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class DistributorSearchResult implements PaginateAble {
    public long total = 0;
    public List<PenyediaDistributor> items = new ArrayList<>();
    public ParamAble contract;


    public DistributorSearchResult(ParamAble paramAble) {
        this.contract = paramAble;
    }

    @Override
    public long getTotal() {
        return total;
    }

    @Override
    public long getTotalCurrentPage() {
        return items.size();
    }

    @Override
    public int getMax() {
        return 20;
    }

    @Override
    public int getPage() {
        return contract.getPage();
    }

    @Override
    public List<BasicNameValuePair> getParams() {
        return contract.getParams();
    }

    @Override
    public void setTotal(Long total) {
        this.total = total;
    }
}
