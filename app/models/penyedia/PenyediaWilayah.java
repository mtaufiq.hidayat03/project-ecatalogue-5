package models.penyedia;

import com.google.gson.Gson;
import models.BaseKatalogTable;
import models.masterdata.Kldi;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import utils.LogUtil;

import java.util.List;

//base on kldi wilayah, mengakomodir kebutuhan katalog lokal, dan sektoral
@Table(name = "penyedia_wilayah")
public class PenyediaWilayah extends BaseKatalogTable {

    @Id
    public Long id;
    public Long kbp_id;
    public Long prp_id;
    public Long penyedia_id;

    //ini hanya direferensikan, waktu ambil wilayahnya itu di kldi apa, acuanya
    //wilayah bukan kldinya, hanya kebutuhan info saja
    public String kldi_id;

    public PenyediaWilayah(){}

    public static boolean saveNew(String kldiwilayah, Long penyedia_id){
        try{
            Kldi kldi = Kldi.findById(kldiwilayah);
            PenyediaWilayah kw = new PenyediaWilayah();
            kw.kldi_id = kldi.id;
            kw.kbp_id = kldi.kbp_id;
            kw.prp_id = kldi.prp_id;
            kw.penyedia_id = penyedia_id;
            kw.save();
        }catch (Exception e){
            return false;
        }
        return true;
    }

    public static void deleteNotIn(Penyedia penyedia, List<String> selected){
        List<PenyediaWilayah> list = penyedia.getPenyediaWilayah();
        for (PenyediaWilayah kw: list) {
            if(!selected.contains(kw.kldi_id)){
                kw.delete();
            }
        }
    }

    public PenyediaWilayah(String kldiwilayah, long penyedia_id){
        Kldi kldi = Kldi.findById(kldiwilayah);
        this.kldi_id = kldi.id;
        this.kbp_id = kldi.kbp_id;
        this.prp_id = kldi.prp_id;
        this.penyedia_id = penyedia_id;
    }

    public static void addNew(String kldiwilayah, long penyedia_id){
        PenyediaWilayah pw = find("kldi_id = ? AND penyedia_id = ?",kldiwilayah, penyedia_id).first();
        LogUtil.d("pw new ",new Gson().toJson(pw));
        if(pw == null){
            pw = new PenyediaWilayah(kldiwilayah, penyedia_id);
            pw.save();
        }
    }

    public static void deleteByPenyediaWilayah(String kldiwilayah, long penyedia_id){
        delete("penyedia_id = ? AND kldi_id = ?", penyedia_id, kldiwilayah);
    }
}
