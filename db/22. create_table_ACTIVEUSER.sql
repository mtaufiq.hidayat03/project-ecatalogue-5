CREATE TABLE activeuser
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    user_id VARCHAR(255),
    ip_address VARCHAR(255),
    session_id VARCHAR(255),
    payload JSON
);