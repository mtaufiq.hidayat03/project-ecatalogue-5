package models.util.produk;

import java.math.BigDecimal;

/**
 * Created by dadang on 8/23/17.
 */
public class HargaKabupatenProdukJson {
	public BigDecimal harga;
	public Long provinsiId;
	public Long kabupatenId;
}
