package models.elasticsearch;

import ext.contracts.PaginateAble;
import ext.contracts.ParamAble;
import models.elasticsearch.response.MetaProduct;
import models.elasticsearch.response.ResponseProduct;
import org.apache.http.message.BasicNameValuePair;
import utils.CookieComparison;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author HanusaCloud on 10/5/2017
 */
public class SearchResult implements Serializable, PaginateAble {

    public long total = 0;
    public List<ProductCommodity> commodities = new ArrayList<>();
    public List<MetaProduct> products = new ArrayList<>();
    public List<MetaProduct> recommendations = new ArrayList<>();
    public List<MetaProduct> aggregateProducts = new ArrayList<>();
    public ParamAble paramAble;

    public SearchResult() {}

    public SearchResult(ParamAble paramAble) {
        this.paramAble = paramAble;
    }

    public void set(ResponseProduct model) {
        this.total = model.hits != null ? model.hits.total : 0;
        if (model.hits != null) {
            products.addAll(model.hits.metaProducts);
        }
    }

    public void setRecommendations(ResponseProduct model) {
        this.total = model.hits != null ? model.hits.total : 0;
        if (model.hits != null) {
            recommendations.addAll(model.hits.metaProducts);
        }
    }

    public void injectQueryIntoCommodity(String query) {
        if (!commodities.isEmpty()) {
            for (ProductCommodity model : commodities) {
                model.query = query;
            }
        }
    }

    public void setChecked() {
        if (products != null && !products.isEmpty()) {
            Set<Long> comparisons = CookieComparison.getKeys();
            if (!comparisons.isEmpty()) {
                for (MetaProduct model : products) {
                    model.isChecked = comparisons.contains(model.getId());
                }
            }
        }
    }

    @Override
    public long getTotal() {
        return total;
    }

    @Override
    public long getTotalCurrentPage() {
        return products.size();
    }

    @Override
    public int getMax() {
        return 20;
    }

    @Override
    public int getPage() {
        return paramAble.getPage();
    }

    @Override
    public List<BasicNameValuePair> getParams() {
        return paramAble.getParams();
    }

    @Override
    public void setTotal(Long total) {
        this.total = total;
    }
}
