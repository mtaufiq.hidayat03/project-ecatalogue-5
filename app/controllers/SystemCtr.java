package controllers;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import jobs.TrackableJob;
import models.jcommon.sysinfo.SystemInformation;
import utils.performance.ActionEntry;
import utils.performance.ActionMonitor;

public class SystemCtr extends BaseController {

	public static void jobs() {
		Deque<TrackableJob> jobs=new ArrayDeque<>();
		Queue<TrackableJob> queue=TrackableJob.lastJobs;
		for(TrackableJob job: queue)
			jobs.push(job);
		render(jobs);
	}
	
	public static void information()
	{
		SystemInformation info=SystemInformation.getSystemInformation();
		Set<Map.Entry<String, Object>> entries= info.informations.entrySet();
		render(entries, info);
	}
	
	public static void performance()
	{
		List<ActionEntry> entryCounts=ActionMonitor.getActionsByCount();
		List<ActionEntry> entryDurations=ActionMonitor.getActionsByDuration();
		List<ActionEntry[]> entries=new ArrayList<>();
		//limit 100
		for(int i=0;i<entryCounts.size();i++)
		{
			if(i==100)
				break;
			entries.add(new ActionEntry[]{entryCounts.get(i)
					, entryDurations.get(i)});
		}
		render(entries);
	}
}
