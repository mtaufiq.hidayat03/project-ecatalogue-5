package jobs.sumberdana;

import java.util.List;
import java.util.UUID;

import jobs.TrackableJob;
import models.jcommon.config.Configuration;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.masterdata.SumberDana;
import play.Logger;
import play.jobs.Job;
import play.jobs.On;

//@On("0 0 23 * * ?") 
// daily at 08 : 39
//@On("0 39 08 * * ?")
public class JobSumberDana extends TrackableJob {

	public void doJob() {
		pullData();
//		new Job() {
//			String uniqueID = UUID.randomUUID().toString();
//			@Override
//			public void before() {
//				Logger.info("job id:" + uniqueID);
//				super.before();
//			}
//
//			public void doJob() {
//				pullData();
//			}
//		}.in(1);
	}

	private void pullData() {
		Logger.info("Job SumberDana Start");
		ExtSumberDanaData ext = new ExtSumberDanaData();
		ext.getSumberDanaData();
		ext.jalankan();

		String reportText = "Import data Sumber Dana selesai";
		try {
			String report = Configuration
					.getConfigurationValue(MailQueue.SMTP_MAIL_ADMIN, "");
			String[] ary = report.split(";");
			for (String to : ary) {
				Logger.info("create mail prop job report to: " + to);
				MailQueue mq = EmailManager.createEmail(to, reportText,
						"Report Job Dce Sumber Dana");
				mq.save();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error("error create mail:" + e.getMessage());
		}
	}
}
