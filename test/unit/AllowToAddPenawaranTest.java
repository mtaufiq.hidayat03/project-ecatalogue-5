package unit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import services.prakatalog.UsulanService;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * @author HanusaCloud on 4/29/2020 9:21 AM
 */
@RunWith(Parameterized.class)
public class AllowToAddPenawaranTest {

    public Date start;
    public Date finish;
    public Date currentDate;
    public Boolean expected;

    public AllowToAddPenawaranTest(Date start, Date finish, Date currentDate, Boolean expected) {
        this.start = start;
        this.finish = finish;
        this.currentDate = currentDate;
        this.expected = expected;
    }

    @Test
    public void shouldBeAbleToGroupByCategory() throws ParseException {
        assertEquals(
                expected,
                UsulanService.allowToActionFrom(start, finish, currentDate)
        );
    }

    @Parameterized.Parameters
    public static Collection dateDataSource() {
        return Arrays.asList(
                new Object[][] {
                        {
                                generateDataFrom(2020,1,10),
                                generateDataFrom(2020,1,15),
                                generateDataFrom(2020,1,10),
                                true
                        },
                        {
                                generateDataFrom(2020,1,10),
                                generateDataFrom(2020,1,20),
                                generateDataFrom(2020,1,11),
                                true
                        },
                        {
                                generateDataFrom(2020,1,10),
                                generateDataFrom(2020,1,20),
                                generateDataFrom(2020,1,20),
                                true
                        },
                        {
                                generateDataFrom(2020,1,10),
                                generateDataFrom(2020,1,20),
                                generateDataFrom(2020,1,21),
                                false
                        },
                        {
                                generateDataFrom(2020,1,10),
                                generateDataFrom(2020,1,20),
                                generateDataFrom(2020,1,9),
                                false
                        },
                        {
                                generateDataFrom(2020,1,10),
                                generateDataFrom(2020,1,10),
                                generateDataFrom(2020,1,10),
                                true
                        }
                }
        );
    }

    private static Date generateDataFrom(Integer year, Integer month, Integer day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        return calendar.getTime();
    }

}
