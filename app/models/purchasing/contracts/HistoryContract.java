package models.purchasing.contracts;

import models.common.AktifUser;
import play.i18n.Lang;
import utils.UrlUtil;


/**
 * @author HanusaCloud on 10/26/2017
 */
public interface HistoryContract {

    String getDetailUrl();

    String getDeleteUrl();

    String getUpdateUrl();

    String getAttachmentDownloadUrl();

    void saveHistory();

    Long getId();

    Long getPackageId();

    boolean isUserAllowed(AktifUser model);

    default String generateUrl(String path) {
        return UrlUtil.builder()
                .setUrl(path)
                .setParam("id", getId())
                .setParam("paket_id", getPackageId())
                .setParam("language", Lang.get())
                .build();
    }

}
