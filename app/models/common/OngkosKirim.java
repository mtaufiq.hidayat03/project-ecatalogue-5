package models.common;

import java.math.BigDecimal;

/**
 *
 * @author ipeng
 *
 */

 public class OngkosKirim {
     public Long provinsiId;
     public String provinsi;
     public Long kabupatenId;
     public String kabupaten;
     public BigDecimal ongkir;
 }