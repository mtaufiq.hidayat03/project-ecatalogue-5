package services.dokumen;

import play.Logger;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.*;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by raihaniqbal on 10/06/17.
 */
public class DokumenService {

	public static byte[] zipFiles(Map<String, InputStream> items) throws Exception {
		byte[] buffer = new byte[1024];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ZipOutputStream stream = new ZipOutputStream(baos);
		stream.setLevel(Deflater.DEFAULT_COMPRESSION);

		for(Iterator it = items.keySet().iterator(); it.hasNext();) {
			String name = it.next().toString();
			Logger.debug(name);
			ZipEntry zipEntry = new ZipEntry(name);
			stream.putNextEntry(zipEntry);

			InputStream in = items.get(name);

			int count;
			while ((count = in.read(buffer)) > 0) {
				stream.write(buffer, 0, count);
			}

			in.close();
		}

		Logger.debug(stream.toString());

		stream.closeEntry();
		stream.close();

		return baos.toByteArray();
	}

}
