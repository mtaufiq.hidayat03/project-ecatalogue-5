package controllers.purchasing;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import controllers.BaseController;
import controllers.security.AllowAccess;
import models.common.AktifUser;
import models.jcommon.util.CommonUtil;
import models.katalog.Komoditas;
import models.katalog.Produk;
import models.katalog.ProdukHarga;
import models.masterdata.Kabupaten;
import models.masterdata.Kurs;
import models.masterdata.KursNilai;
import models.masterdata.Provinsi;
import models.penyedia.Penyedia;
import models.purchasing.keranjangBelanja.CookieCart;
import models.purchasing.keranjangBelanja.PenyediaKeranjang;
import models.purchasing.keranjangBelanja.ProdukKeranjang;
import models.secman.Acl;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.i18n.Messages;
import play.mvc.Http;
import services.produk.ProdukService;
import utils.LogUtil;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;

public class KeranjangBelanjaCtr extends BaseController {

	public static final String TAG = "KeranjangBelanjaCtr";
	public static List<Map<String, String>> produkList = new ArrayList<>();

	@AllowAccess({Acl.COM_PAKET})
	public static void index() {
		List<PenyediaKeranjang> keranjangs = populateProdukKeranjang(-1);
//		Map<Long, Double> kuantitas = populateKuantitasKeranjang();

		render(keranjangs);
	}

	@AllowAccess({Acl.COM_PAKET_ADD})
	public static void buatPaket(long pid, Long wilayahId, Long kursId, Long komoditasId ) { //pid = penyedia id
		List<Produk> produkList = new ArrayList<>();
		KursNilai kursNilai = null;
		Kurs kurs = null;
		AktifUser aktifUser = getAktifUser();
		String kelasHarga = "";

		Http.Cookie cookie = request.cookies.get(generateKey(aktifUser.user_id));
		Map<Long, Double> kuantitas = populateKuantitasKeranjang();
		Penyedia penyedia = Penyedia.findById(pid);
		Kurs kursUtama = Kurs.findKursUtama();
		Long kabId = new Long(0);
		Long prId = new Long(0);
		Double ongkir = new Double(0);

		Komoditas kom = Komoditas.findById(komoditasId);



		double totalHarga = 0;
		if(cookie != null){
			JsonParser parser = new JsonParser();
			JsonArray jsonArray =  parser.parse(URLDecoder.decode(cookie.value)).getAsJsonArray();
			Logger.info("[PRODUK] - " + jsonArray);
			for(int i = 0; i < jsonArray.size(); i++){
				JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
				Long produkId = jsonObject.get("produkId").getAsLong();
				Long daerahId = jsonObject.get("daerahId").getAsLong();
				Long komId = jsonObject.get("komoditasId").getAsLong();
				Produk produk = Produk.getProdukKomoditas(produkId);
				produk.komoditas_id = komId.intValue();

				Logger.info("JSONNYA == " +produk.komoditas_id);
                Logger.info("DATANYA == " +komoditasId);

                if(produk.komoditas_id == komoditasId.intValue()){

					kelasHarga = produk.kelas_harga;
					Logger.info("[PAKET] - get detail from product : " + produkId + " daerah : " + daerahId);
					if(!kelasHarga.equalsIgnoreCase(ProdukHarga.KELAS_HARGA_NASIONAL)){
						if(kelasHarga.equalsIgnoreCase(ProdukHarga.KELAS_HARGA_KABUPATEN)){
							prId = ((Kabupaten)Kabupaten.findById(wilayahId)).provinsi_id;
							kabId = ((Kabupaten)Kabupaten.findById(wilayahId)).id;
						}else{
							prId = wilayahId;
						}
						renderArgs.put("provinsiId", prId);
						renderArgs.put("kabupatenId", kabId);
					}
					produk = ProdukService.getDetailProduk(produkId, daerahId);
					Logger.debug("[PRODUK] penyedia_id = %d , daerah_id = %d, kursid %d", produk.penyedia_id, produk.daerah_id, produk.harga_kurs_id);
					if(produk.penyedia_id.longValue() != pid || (produk.daerah_id!=null && wilayahId != null && !produk.daerah_id.equals(wilayahId))){
						Logger.info("next here !!!!" + produk.nama_produk);
						continue;
					}

					try{
						ongkir = ProdukHarga.getOngkosKirim(produkId, prId, kabId);
					}catch (Exception e){}
					produk.harga_ongkir = new BigDecimal(ongkir);
					kelasHarga = produk.kelas_harga;
					Logger.info("produk.harga_ongkir " + produk.harga_ongkir);

					//kedepannya jika kurs dinamis, maka perlu penyesuaian kursnya list, sekarang kurs hanya dolar dan rp
					if(null == kursNilai && null != produk.kurs_id){
						kursNilai = KursNilai.getNewestKursByKursId(produk.kurs_id);
						//Logger.debug("kurs set, id %s", kursNilai.id);
						if(kursNilai != null){
							kurs = Kurs.findById(kursNilai.kurs_id_from);
							renderArgs.put("kurs", kurs);
						}
						renderArgs.put("kursNilai", kursNilai);
					}
					//handling total harga jika kurs dolar
					//butuh improvement jika lebih dari beberapa kurs selain idr dan dollar
					Logger.info("kurs produk %s", produk.kurs_id);
					//handling kursid null, maka default anggap rupiah tidak usah kalkulasi kurs
					if(null != produk.kurs_id){
						//pengecekan kurs
						KursNilai	kursNilaiTmp = KursNilai.getNewestKursByKursId(produk.kurs_id);
						Kurs tmpKurs = null;
						if(null != kursNilaiTmp ){
							tmpKurs = Kurs.findById(kursNilaiTmp.kurs_id_from);
						}
						BigDecimal hgu = new BigDecimal(0);
						//jika kurs tidak null
						if(tmpKurs!= null){
							if(tmpKurs.nama_kurs.trim().toLowerCase().equals("IDR")){
								//jika rupiah langsung
								totalHarga += (produk.harga_utama.multiply(new BigDecimal(kuantitas.get(produk.id))).doubleValue());
							}else{
								//jika dolar kalkulasi kurs
								hgu = produk.harga_utama.multiply(new BigDecimal(kuantitas.get(produk.id)));
								Logger.info("harga utama %s", produk.harga_utama );
								Logger.info("harga dikurskan %s", hgu );
								Logger.info("jumlah dikurskan %s", hgu.multiply(new BigDecimal(kursNilai.nilai_tengah)).doubleValue() );
								totalHarga += hgu.multiply(new BigDecimal(kursNilai.nilai_tengah)).doubleValue();
								Logger.info("harga dikurskan %s", totalHarga );
							}
						}else{
							//jika kurs tidak ditemukan
							totalHarga += (produk.harga_utama.multiply(new BigDecimal(kuantitas.get(produk.id))).doubleValue());
						}
					}else {
						//handling kursid null, maka default anggap rupiah tidak usah kalkulasi kurs
						totalHarga += (produk.harga_utama.multiply(new BigDecimal(kuantitas.get(produk.id))).doubleValue());
					}
					try{
						if(produk.daerah_id == null){
							produk.daerah_id = wilayahId;
						}
						if(!produk.daerah_id.equals(wilayahId) ^ !produk.penyedia_id.equals(new Long(pid)) ^ !produk.harga_kurs_id.equals(kursId)){
							Logger.info("next here(one is not equal)");
							continue;
						}else {
							Logger.info("------------------next there(all equal) %s %s %s %s", produk.daerah_id, produk.penyedia_id, produk.harga_kurs_id, produk.komoditas_id);
							Logger.info("------------------next there(all equal) %s %s %s %s", wilayahId, pid, kursId, komoditasId);
						}
					}catch (Exception e){
						Logger.debug("excepsi buat paket samakan daerah,penyedia,kurs");
						continue;
					}
					produkList.add(produk);

				}

			}

			Logger.info("size produk paket: %s", produkList.size());
		}

		long adaYangStokHabis = 0;

		if(!kom.apakah_stok_unlimited){
			for (Produk lp : produkList){
				if (lp.jumlah_stok.longValue() == 0){
					adaYangStokHabis += 1;
				}
			}
		}

		Logger.info("adaYangStokHabis " + adaYangStokHabis);
		if (produkList.size() == 0){
			index();
		}
		if (adaYangStokHabis > 0){
			flash.error(Messages.get("Ada Produk yang stoknya sedang kosong"));
			index();
		} else {
			if (produkList.size() > 0){
				//kurs pindah ke atas
				Komoditas komoditas = Komoditas.findById(produkList.get(0).komoditas_id);
				renderArgs.put("komoditas", komoditas);
			}
			renderArgs.put("listProvinsi", Provinsi.findAllActive());
			renderArgs.put("kursUtama", kursUtama);
			renderArgs.put("kuantitasProduk", kuantitas);
			renderArgs.put("produkList", produkList);
			renderArgs.put("totalHarga", totalHarga);
			renderArgs.put("penyedia", penyedia);
			renderArgs.put("kuantitas", kuantitas);
			renderArgs.put("harga_ongkir", ongkir);
			renderArgs.put("ongkir", ongkir);

			renderArgs.put("satker_prov_id", 0);
			renderArgs.put("satker_kab_id", 0);
			renderArgs.put("pengiriman_prov_id", 0);
			renderArgs.put("pengiriman_kab_id", 0);
			renderArgs.put("listSatkerKab",  new ArrayList<Kabupaten>() );
			renderArgs.put("listPengirimanKab",  new ArrayList<Kabupaten>() );

			if(kelasHarga.equals("kabupaten")){
				Long pId = ((Kabupaten)Kabupaten.findById(wilayahId)).provinsi_id;
				renderArgs.put("provinsiId", pId);
			}
			renderArgs.put("wilayahId", wilayahId);
			renderArgs.put("kelasHarga", kelasHarga);
			renderArgs.put("kursId",kursId);


			render("purchasing/PaketCtr/create.html");
		}

	}

	@AllowAccess({Acl.COM_PAKET_ADD})
	public static void getOngosKirim(List<Long> pIds, Long wId){
		renderJSON(pIds);
	}

	@AllowAccess({Acl.COM_PAKET_ADD})
	public static void getKabupatens(Long provinsiId){
		renderJSON(Kabupaten.findByProvId(provinsiId));
	}
	
	public static void buatPaketKonsolidasi() {
		render("purchasing/PaketKonsolidasiCtr/create.html");
	}

	public static void viewAddToBasket(long produkId, long daerahId) {
		Produk produk = Produk.findById(produkId);
		produk.withCommodity();
		boolean exist = false;
		AktifUser aktifUser = getAktifUser();
		List<Map<String, String>> pl = new ArrayList<>();
		Http.Cookie cookie = null;
		try{
			cookie = request.cookies.get(generateKey(aktifUser.user_id));
		}catch (Exception e){}

		if(cookie != null) {
			JsonParser parser = new JsonParser();
			JsonArray jsonArray = parser.parse(URLDecoder.decode(cookie.value)).getAsJsonArray();
			for (int i = 0; i < jsonArray.size(); i++) {
				JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
				Map<String, String> productMap = new HashMap<>();
				productMap.put("produkId", jsonObject.get("produkId").getAsString());
				if(productMap.get("produkId").equalsIgnoreCase(String.valueOf(produkId))){
					exist = true;
				}
			}
		}
		Logger.info(String.valueOf(exist));

		if(exist && !produk.komoditas.apakah_paket_produk_duplikat){
			render("katalog/ProdukCtr/addToBasketFormFailed.html", produk, daerahId);
		}else{
			render("katalog/ProdukCtr/addToBasketForm.html", produk, daerahId);
		}
//		for (int i = 0; i < pl.size(); i ++){
//			if (pl.get(i).get(produkId).equalsIgnoreCase(String.valueOf(produkId))) {
//				render("katalog/ProdukCtr/addToBasketFormFailed.html", produk, daerahId);
//			}else {
//				render("katalog/ProdukCtr/addToBasketForm.html", produk, daerahId);
//			}
//		}

	}

	public static void listBasket(){
//		List<Map<String, String>> produkList = new ArrayList<>();
		AktifUser aktifUser = getAktifUser();
		Http.Cookie cookie = request.cookies.get(generateKey(aktifUser.user_id));
		if(cookie != null){
			JsonParser parser = new JsonParser();
			JsonArray jsonArray =  parser.parse(URLDecoder.decode(cookie.value)).getAsJsonArray();
			for(int i = 0; i < jsonArray.size(); i++){
				JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
				Map<String, String> produkMap = new HashMap<>();
				Produk produk = Produk.findById(jsonObject.get("produkId").getAsLong());
				produkMap.put("produkId", jsonObject.get("produkId").getAsString());
				produkMap.put("produkName", produk.nama_produk);
				produkMap.put("jumlah", jsonObject.get("jumlah").getAsString());
				produkMap.put("komoditasId", jsonObject.get("komoditasId").getAsString());
				produkMap.put("daerahId", jsonObject.get("daerahId") != null? jsonObject.get("daerahId").getAsString() : null);

				produkList.add(produkMap);

			}
		}

		render("katalog/ProdukCtr/listBasket.html", produkList);
	}

	public static void updateFromBasket(List<Long> jumlah) {
		AktifUser aktifUser = getAktifUser();
		final String key = generateKey(aktifUser.user_id);
		Http.Cookie cookie = request.cookies.get(key);
		JsonArray newJsonArray = new JsonArray();
		Map<String, String> result = new HashMap<>();
		if(cookie != null && jumlah.size() != 0){
			try {
				JsonParser parser = new JsonParser();
				JsonArray jsonArray = parser.parse(URLDecoder.decode(cookie.value)).getAsJsonArray();
				for (int i = 0; i < jsonArray.size(); i++) {
					JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
					JsonObject newJsonObject = new JsonObject();
					newJsonObject.addProperty("produkId", jsonObject.get("produkId").getAsString());
					newJsonObject.addProperty("jumlah", jumlah.get(i));
					newJsonObject.addProperty("komoditasId", jsonObject.get("komoditasId").getAsString());
					newJsonObject.addProperty("daerahId", jsonObject.get("daerahId") != null? jsonObject.get("daerahId").getAsString() : null);
					newJsonArray.add(newJsonObject);

				}

				if (newJsonArray.size() > 0) {
					cookie.value = URLEncoder.encode(newJsonArray.toString(), "UTF-8");
					cookie.name = key;
					cookie.path = "/";
					response.cookies.put(key, cookie);
				}
				result.put("result", "ok");

			} catch (Exception e) {
				Logger.error("Terjadi Kesalahan : "+e.getLocalizedMessage());
				result.put("result", "fail");
			}

		} else {
			result.put("result", "empty");
		}

		renderJSON(result);
	}

	public static void removeFromBasket(Long produkId) {
		AktifUser aktifUser = getAktifUser();
		final String key = generateKey(aktifUser.user_id);
		Http.Cookie cookie = request.cookies.get(key);
		JsonArray jsonArray = new JsonArray();
		JsonObject jsonObject = new JsonObject();
		Map<String, String> result = new HashMap<>();

		if(cookie != null && produkId != null){
			try {
				JsonParser parser = new JsonParser();
				jsonArray = parser.parse(URLDecoder.decode(cookie.value)).getAsJsonArray();
				JsonArray newJsonArray = new JsonArray();
				for (int i = 0; i < jsonArray.size(); i++) {
					jsonObject = jsonArray.get(i).getAsJsonObject();
					if (jsonObject.get("produkId").getAsLong() != produkId)
						newJsonArray.add(jsonObject);

				}

				if (newJsonArray.size() > 0) {
					cookie.value = URLEncoder.encode(newJsonArray.toString(), "UTF-8");
					cookie.name = key;
					cookie.path = "/";
					response.cookies.put(key, cookie);
				} else {
					response.removeCookie(key);
				}
				result.put("result", "ok");
			} catch (Exception e){
				Logger.error("Terjadi Kealahan : "+e.getLocalizedMessage());
				result.put("result", "fail");
			}

		} else {
			result.put("result", "empty");
		}

		renderJSON(result);

	}

	public static void addToBasketSubmit(Long produkId, String daerahId, Long komoditasId, Long jumlah) {
		AktifUser aktifUser = getAktifUser();
		Produk produk = Produk.findById(produkId);
		Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
		Map<String, String> result = new HashMap<>();
		if (produk != null && jumlah != null && null != aktifUser) {
			try {
				JsonArray jsonArray = new JsonArray();
				JsonObject jsonObject = new JsonObject();
				Long totalAll = 0L;
				final String key = generateKey(aktifUser.user_id);
				Http.Cookie cookie = request.cookies.get(key);
				if (cookie != null) {
					JsonParser parser = new JsonParser();
					JsonArray newJsonArray = new JsonArray();
					boolean isDuplicate = false;
					jsonArray = parser.parse(URLDecoder.decode(cookie.value)).getAsJsonArray();

					for (int i = 0; i < jsonArray.size(); i++) {
						jsonObject = jsonArray.get(i).getAsJsonObject();
						JsonObject newJsonObject = new JsonObject();
						newJsonObject.addProperty("produkId", produk.id);
                        newJsonObject.addProperty("komoditasId", komoditas.id);
//						newJsonObject.addProperty("namaKomoditas", komoditas.nama_komoditas);
						//jika ada produk yg sama, update hanya jumlah nya saja
						long newProdukId = jsonObject.get("produkId").getAsLong();
						String newDaerahId = jsonObject.get("daerahId").getAsString();
						if (newProdukId == produk.id && !komoditas.apakah_paket_produk_duplikat && newDaerahId.equals(daerahId)) {
							isDuplicate = true;
							Long total = jsonObject.get("jumlah").getAsLong() + jumlah;
							newJsonObject.addProperty("jumlah", total);
							newJsonObject.addProperty("komoditasId", komoditasId);
							newJsonObject.addProperty("daerahId", daerahId);
						} else {
							newJsonObject.addProperty("produkId", jsonObject.get("produkId").getAsString());
							newJsonObject.addProperty("jumlah", jsonObject.get("jumlah").getAsLong());
							newJsonObject.addProperty("komoditasId", jsonObject.get("komoditasId").getAsString());
							newJsonObject.addProperty("daerahId", jsonObject.get("daerahId").getAsString());
						}
						newJsonArray.add(newJsonObject);
						totalAll += newJsonObject.get("jumlah").getAsLong();
					}
					// jika tidak ada produk yg sama, buat sebagai produk baru
					if (!isDuplicate) {
						jsonObject = generateBasketSubmit(produk.id, daerahId, komoditasId, jumlah);
						newJsonArray.add(jsonObject);
						totalAll += jumlah;
					}
					LogUtil.d("****************** object :", newJsonArray);
					cookie.value = URLEncoder.encode(newJsonArray.toString(), "UTF-8");

				} else {
					jsonObject = generateBasketSubmit(produk.id, daerahId, komoditasId, jumlah);
					jsonArray.add(jsonObject);
					cookie = new Http.Cookie();
					cookie.value = URLEncoder.encode(jsonArray.toString(), "UTF-8");
					totalAll += jumlah;
				}

				cookie.name = key;
				cookie.path = "/";
				response.cookies.put(key, cookie);
				result.put("result", "ok");
				result.put("total", String.valueOf(totalAll));

			} catch (Exception e){
				e.printStackTrace();
				Logger.error("Terjadi Kesalahan : "+e.getLocalizedMessage());
				result.put("result", "fail");
			}

		} else {
			result.put("result", "empty");
		}

		renderJSON(result);

	}

	private static JsonObject generateBasketSubmit(long produkId, String daerahId, Long komoditasId, Long jumlah){

		JsonObject json = new JsonObject();
		json.addProperty("produkId", produkId);
		json.addProperty("jumlah", jumlah);
		json.addProperty("daerahId", daerahId);
		json.addProperty("komoditasId", komoditasId);

		return json;
	}

	private static List<PenyediaKeranjang> populateProdukKeranjang(long penyedia_id){
		AktifUser aktifUser = getAktifUser();
		List<PenyediaKeranjang> penyediaKeranjangList = new ArrayList<>();
		List<Produk> produkList = new ArrayList<>();

		Http.Cookie cookie = null;
		if(null != aktifUser){
			cookie  = request.cookies.get(generateKey(aktifUser.user_id));
		}
		if(cookie != null){
			JsonParser parser = new JsonParser();
			JsonArray jsonArray =  parser.parse(URLDecoder.decode(cookie.value)).getAsJsonArray();
			Map<Produk, Long> kuantitas = new HashMap<>();
//			Map<Produk, String> namaKomoditas = new HashMap<>();
			for(int i = 0; i < jsonArray.size(); i++){
				JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
				Produk produk = ProdukService.getDetailProduk(jsonObject.get("produkId").getAsLong(), jsonObject.get("daerahId").getAsLong());
				produkList.add(produk);
				try{
					kuantitas.put(produk, jsonObject.get("jumlah").getAsLong());
				} catch (Exception e){
					kuantitas.put(produk, 1L);
				}

//				Long komoditasID = jsonObject.get("komoditasId").getAsLong();
//				Komoditas komoditas = Komoditas.findById(komoditasID);
//				namaKomoditas.put(produk, jsonObject.get("namaKomoditas").getAsString());
//				jsonObject.addProperty("namaKomoditas", komoditas.nama_komoditas);
			}

			List<String> penyediaSaved = new ArrayList<>();

			for(int i = 0; i < produkList.size(); i++){
				final Produk produk = produkList.get(i);

				if(penyedia_id != -1 && produk.penyedia_id.longValue() != penyedia_id){
					continue;
				}

				PenyediaKeranjang penyediaKeranjang = new PenyediaKeranjang();

				if(!penyediaSaved.contains(produk.penyedia_id + "-" +produk.kurs_id+"-"+produk.daerah_id+"-"+produk.komoditas_id)){

					penyediaKeranjang.penyediaId = produk.penyedia_id;
					penyediaKeranjang.kursId = produk.kurs_id;
					penyediaKeranjang.namaPenyedia = produk.nama_penyedia;
					penyediaKeranjang.produkKeranjangList = new ArrayList<>();
					penyediaKeranjang.totalKeseluruhan = new Double(0);
					penyediaKeranjang.namaKurs = produk.nama_kurs;
					penyediaKeranjang.wilayahId = produk.daerah_id;
					penyediaKeranjang.komoditasId = produk.komoditas_id.longValue();

					Komoditas komod = Komoditas.findById(penyediaKeranjang.komoditasId);

					penyediaKeranjang.namaKomoditas = komod.nama_komoditas;

					if(penyediaKeranjang.wilayahId != null){
						if(produk.kelas_harga.equals("provinsi")){
							Logger.debug("wilayahId = %d", penyediaKeranjang.wilayahId);
							penyediaKeranjang.namaWilayah = ((Provinsi)Provinsi.findById(penyediaKeranjang.wilayahId)).nama_provinsi;
						}else if(produk.kelas_harga.equals("kabupaten")){
							penyediaKeranjang.namaWilayah = ((Kabupaten)Kabupaten.findById(penyediaKeranjang.wilayahId)).nama_kabupaten;
						}else{
							penyediaKeranjang.namaWilayah = "";
							penyediaKeranjang.wilayahId=new Long(0);
						}
					}else{
						penyediaKeranjang.namaWilayah = "";
						penyediaKeranjang.wilayahId=new Long(0);
					}

					for(int j = 0; j < produkList.size(); j++){
						final Produk produk2 = produkList.get(j);
						//handling kekosongan wilayah id

						if(produk2.daerah_id == null){
							produk2.daerah_id = new Long(0);
						}else if(produk2.daerah_id.equals(0l) ^ produk2.daerah_id.equals(-999l)){
							produk2.daerah_id = new Long(0);
						}
						if(penyediaKeranjang.wilayahId == null ){
							penyediaKeranjang.wilayahId=0l;
						}else if(penyediaKeranjang.wilayahId.equals(-999l) ^ penyediaKeranjang.wilayahId.equals(-0l)){
							penyediaKeranjang.wilayahId=0l;
						}
						//handling kekosongan wilayah id

						if(penyediaKeranjang.penyediaId.equals(produk2.penyedia_id) && penyediaKeranjang.komoditasId.equals(produk2.komoditas_id.longValue()) && penyediaKeranjang.wilayahId.equals(produk2.daerah_id) && penyediaKeranjang.kursId.equals(produk2.kurs_id)){
							Logger.info("produk 2 " + produk2.nama_produk);
						    ProdukKeranjang produkKeranjang = new ProdukKeranjang();
							produkKeranjang.hargaSatuan = produk2.harga_utama.doubleValue();
							produkKeranjang.produkId = produk2.id;
							produkKeranjang.namaProduk = produk2.nama_produk;
							produkKeranjang.kursId = produk2.harga_kurs_id;
							Logger.debug("produkId = %d, kursid %d harga %d", produk2.id, produk2.kurs_id, produk2.harga_kurs_id);
							produkKeranjang.namaKurs = produk2.nama_kurs;
							produkKeranjang.urlImage = produk2.url_gambar_utama;
							produkKeranjang.kuantitas = kuantitas.get(produk2);
							produkKeranjang.komoditasId = produk2.komoditas_id.longValue();
//							produkKeranjang.namaKomoditas = namaKomoditas.get(produk2);

							Double ttl = penyediaKeranjang.totalKeseluruhan +  (produkKeranjang.getHargaSatuanRupiah()*produkKeranjang.kuantitas);
							Logger.debug("tottal all is: %s", ttl);
							penyediaKeranjang.totalKeseluruhan = ttl;


							penyediaKeranjang.produkKeranjangList.add(produkKeranjang);
						}
					}

					penyediaKeranjangList.add(penyediaKeranjang);


					penyediaSaved.add(produk.penyedia_id + "-" +produk.kurs_id+ "-" +produk.daerah_id+ "-" +produk.komoditas_id);
				}
			}
		}

		return penyediaKeranjangList;
	}

	public static void deleteAllProducts(Long id, Long kursId, Long wilayahId, Long komoditasId) {
		AktifUser aktifUser = getAktifUser();
		try {
			final String key = generateKey(aktifUser.user_id);
			Http.Cookie cookie = request.cookies.get(key);
			if (cookie != null) {
				Type collectionType = new TypeToken<Collection<CookieCart>>() {
				}.getType();
				final String json = URLDecoder.decode(cookie.value);
				if (!TextUtils.isEmpty(json)) {
					List<CookieCart> cookieCarts = CommonUtil.fromJson(json, collectionType);
					if (cookieCarts != null && !cookieCarts.isEmpty()) {
						int totalAll = 0;
						LogUtil.d(TAG, cookieCarts);
						Iterator<CookieCart> iterator = cookieCarts.iterator();
						while (iterator.hasNext()) {
							totalAll++;
							CookieCart model = iterator.next();
							if (model != null) {
								Produk produk = Produk.findById(model.productId);
								try{
									Logger.debug("delete cookie produk : %s %s %s", produk.kurs_id, produk.penyedia_id, produk.daerah_id);
									if ((produk.penyedia_id != null && produk.penyedia_id.equals(id)) &&
											(produk.kurs_id != null && produk.kurs_id.equals(kursId)) &&
											((wilayahId.equals(-999l) || wilayahId.equals(0l)) || // handling ketika tidak punya wilayah_id
											(produk.daerah_id != null && produk.daerah_id.equals(wilayahId))) &&
											(produk.komoditas_id != null && produk.komoditas_id.equals(komoditasId.intValue()))){
										LogUtil.d(TAG, "found one: " + model.productId + " provider: " + produk.penyedia_id );
										iterator.remove();
									}
								}catch (Exception e){}
							}
						}
						if (cookieCarts.size() != totalAll) {
							cookie.value = URLEncoder.encode(CommonUtil.toJson(cookieCarts), "UTF-8");
							cookie.name = key;
							cookie.path = "/";
							response.cookies.put(key, cookie);
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtil.e(TAG, e);
		}
		index();
	}

}
