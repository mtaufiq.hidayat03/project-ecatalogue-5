package models.permohonan.search;

import ext.contracts.ParamAble;
import org.apache.http.message.BasicNameValuePair;
import play.mvc.Scope;

import java.util.ArrayList;
import java.util.List;

public class PermohonanPembaruanSearch implements ParamAble {

    public String keyword = "";
    public String tglpengajuan = "";
    public String tipe = "";
    public String jenis = "";
    public String status = "";
    public String order = "";
    public long uid = 0;
    public long dis_ui = 0;
    public long per_page = 20;
    public int page = 0;
    public long penyedia = 0;
    public String posisi = "0";

    public PermohonanPembaruanSearch(Scope.Params params) {
        this.keyword = !isNull(params,"keyword") ? params.get("keyword") : "";
        this.tglpengajuan = !isNull(params, "tglpengajuan") ? params.get("tglpengajuan") : "";
        this.tipe = !isNull(params, "tipe") ? params.get("tipe") : "";
        this.jenis = !isNull(params, "jenis") ? params.get("jenis") : "";
        this.status = !isNull(params, "status") ? params.get("status") : "";
        this.order = !isNull(params, "order") ? params.get("order") : "";
        this.posisi = !isNull(params, "posisi") ? params.get("posisi") : "0";
        this.penyedia = !isNull(params, "penyedia") ? params.get("penyedia", Long.class) : 0;
        this.uid = !isNull(params, "uid") ? params.get("uid", Long.class) : 0;
        this.dis_ui = !isNull(params, "dis_ui") ? params.get("dis_ui", Long.class) : 0;
        this.per_page = !isNull(params, "per_page") ? params.get("per_page", Long.class) : 20;
        this.page = getCurrentPage(params);
    }

    @Override
    public List<BasicNameValuePair> getParams() {
        List<BasicNameValuePair> results = new ArrayList<>();
        results.add(new BasicNameValuePair("keyword", this.keyword));
        results.add(new BasicNameValuePair("tglpengajuan", this.tglpengajuan));
        results.add(new BasicNameValuePair("tipe", this.tipe));
        results.add(new BasicNameValuePair("jenis", this.jenis));
        results.add(new BasicNameValuePair("status", this.status));
        results.add(new BasicNameValuePair("posisi", this.posisi));
        results.add(new BasicNameValuePair("penyedia", String.valueOf(this.penyedia)));
        results.add(new BasicNameValuePair("order", this.order));
        results.add(new BasicNameValuePair("uid", String.valueOf(this.uid)));
        results.add(new BasicNameValuePair("dis_ui", String.valueOf(this.dis_ui)));
        results.add(new BasicNameValuePair("per_page", String.valueOf(this.per_page)));
        return results;
    }

    @Override
    public int getPage() {
        return page;
    }
}
