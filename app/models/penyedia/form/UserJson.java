package models.penyedia.form;

import models.BaseKatalogTable;
import models.penyedia.Penyedia;
import models.util.RekananDetail;
import play.db.jdbc.CacheMerdeka;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;

public class UserJson  {

	public String message;
	public List<RekananDetail> results;
	public List<Penyedia> penyediaList;
	public List<UserDistributorJson> distributorJsonList;

}
