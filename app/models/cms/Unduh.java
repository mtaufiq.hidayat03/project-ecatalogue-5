package models.cms;

import ext.DatetimeBinder;
import models.BaseKatalogTable;
import models.common.AktifUser;
import org.apache.commons.io.FilenameUtils;
import play.data.binding.As;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Table(name = "pustaka_file")
public class Unduh extends BaseKatalogTable {
	
	public static int LIMIT_CHAR_TITLE = 50;
	public static int LIMIT_DESC = 200;

    @Id
    public Long id;
    public Long konten_kategori_id;
    public String file_name;
    public String original_file_name;
    public Long file_size;
    public String file_sub_location;
    public int posisi_file;
    public String deskripsi;
    public String status;

    @Transient
    public String nama_kategori;
    public boolean active;

    @Transient
    public Long jumlah_file;

    @As(binder = DatetimeBinder.class)
    public Date publish_date_from;

    @As(binder = DatetimeBinder.class)
    public Date publish_date_to;

    public static void deleteById(long id){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String date_now = dateFormat.format(date);

        String query = "update pustaka_file set active = 0, deleted_by = " + AktifUser.getActiveUserId() + ", deleted_date = '"+date_now+"' " +
                "where id = ?";

        Query.update(query, id);
    }

    public static List<Unduh> findAllActive(){
        return Unduh.find("active = 1").fetch();
    }

    public static List<Unduh> findAllPublished(){
        return Unduh.find("active = 1 and status = 'published' and publish_date_from <= now() and (publish_date_to is NULL or publish_date_to >= now())").fetch();
    }

    public static List<Unduh> findAllCountUnduh(){
        String query = "SELECT kk.id, kk.nama_kategori, COUNT(pf.konten_kategori_id) AS jumlah_file FROM pustaka_file pf LEFT JOIN konten_kategori kk ON kk.id = pf.konten_kategori_id " +
                "WHERE pf.active = 1 and pf.status = 'published' and pf.publish_date_from <= now() and (pf.publish_date_to is NULL or pf.publish_date_to >= now()) GROUP BY pf.konten_kategori_id";
        return Query.find(query,Unduh.class).fetch();
    }

    public static List<Unduh> findAllUnduhFromKategori(String kategori){
        String query = "SELECT pf.id, pf.original_file_name, pf.deskripsi, pf.file_size, pf.created_date, kk.nama_kategori " +
                "FROM pustaka_file pf JOIN konten_kategori kk ON pf.konten_kategori_id = kk.id " +
                "WHERE pf.active = 1 AND pf.status = 'published' AND kk.active =1  " +
                "AND publish_date_from <= now() AND (publish_date_to is NULL OR publish_date_to >= now()) "+
                "AND kk.nama_kategori = '"+kategori+"' ORDER BY pf.created_date DESC";

        return Query.find(query,Unduh.class).fetch();
    }


    public static List<Unduh> findJoinKategoriAllActiveByLimit(Long start, Long end){
        String query = "SELECT pf.id, pf.original_file_name, pf.deskripsi, pf.file_size, pf.created_date, kk.nama_kategori " +
                        "FROM pustaka_file pf JOIN konten_kategori kk ON pf.konten_kategori_id = kk.id " +
                        "WHERE pf.active = 1 AND kk.active =1 AND pf.status = 'published' " +
                        "AND publish_date_from <= now() AND (publish_date_to is NULL OR publish_date_to >= now()) "+
                        "ORDER BY pf.created_date DESC LIMIT "+start+","+end;

        return Query.find(query,Unduh.class).fetch();
    }

    public static List<Unduh> findUnduh(String keyword, Long start, Long end){
        keyword = keyword == null ? "": "%"+keyword+"%";
        start = start == null ? 0 : start;
        end = start == null ? 12 : end;
        String query = "SELECT pf.id, pf.original_file_name, pf.deskripsi, pf.file_size, pf.created_date, kk.nama_kategori FROM pustaka_file pf " +
                        "JOIN konten_kategori kk ON pf.konten_kategori_id = kk.id WHERE pf.active = 1 AND pf.status = 'published' " +
                        "AND publish_date_from <= now() AND (publish_date_to is NULL OR publish_date_to >= now()) "+
                        "AND kk.active =1 AND pf.deskripsi like ? ORDER BY pf.created_date DESC LIMIT ?,?";

        return Query.find(query,Unduh.class,keyword,start,end).fetch();
    }

    public static List<Unduh> findUnduhByKategori(String kategori, Long start, Long end){
        String query = "SELECT pf.id, pf.original_file_name, pf.deskripsi, pf.file_size, pf.created_date, kk.nama_kategori "+
                        "FROM pustaka_file pf JOIN konten_kategori kk ON pf.konten_kategori_id = kk.id " +
                        "WHERE pf.active = 1 AND pf.status = 'published' AND kk.active =1 AND kk.nama_kategori = '"+kategori+"' " +
                        "AND publish_date_from <= now() AND (publish_date_to is NULL OR publish_date_to >= now()) "+
                        "ORDER BY pf.created_date DESC LIMIT "+start+" , "+end;

        return Query.find(query,Unduh.class).fetch();
    }

    public String getTanggal(String format){
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        if(created_date != null){
            return sdf.format(created_date);
        }
        return null;
    }

    public String getFileName(){
        return FilenameUtils.removeExtension(this.original_file_name);
    }

    public String formatFileSize(long size) {
        String hrSize;

        double k = size/1024.0;
        double m = ((size/1024.0)/1024.0);
        double g = (((size/1024.0)/1024.0)/1024.0);
        double t = ((((size/1024.0)/1024.0)/1024.0)/1024.0);

        DecimalFormat dec = new DecimalFormat("0.00");

        if ( t>1 ) {
            hrSize = dec.format(t).concat(" TB");
        } else if ( g>1 ) {
            hrSize = dec.format(g).concat(" GB");
        } else if ( m>1 ) {
            hrSize = dec.format(m).concat(" MB");
        } else if ( k>1 ) {
            hrSize = dec.format(k).concat(" KB");
        } else {
            hrSize = dec.format(size).concat(" Bytes");
        }

        return hrSize;
    }
}