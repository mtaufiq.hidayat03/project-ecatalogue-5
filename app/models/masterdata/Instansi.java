package models.masterdata;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

/**
 * @author Andik Yulianto<andikyulianto@yahoo.com>
 */
//TODO silakan lengkapi field-nya. Ini masih prototype
@Table
public class Instansi extends BaseKatalogTable{

	@Id
	public String id;
	public String nama;
	public Long prp_id;
	public Long kbp_id;
	public String kldi_id;
	public Boolean active;
	
	public void setAktif() {
		this.active = true;
		prePersist();
		save();
	}

	public void setNonAktif() {
		this.active = false;
		prePersist();
		save();
	}
}
