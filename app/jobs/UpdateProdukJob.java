package jobs;

import jobs.elasticsearch.ElasticsearchLog;

import models.api.MailQueueF;
import models.jcommon.config.Configuration;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.penyedia.Penyedia;
import play.Logger;
import play.cache.Cache;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import play.jobs.On;
import repositories.elasticsearch.ElasticsearchRepository;

import java.util.Date;


/** job di eksekusi setiap jam 02:00 **/
//@On("0 0 2 * * ?")
@NoTransaction
public class UpdateProdukJob extends Job {

//    public static void main(String[] args){
//        System.out.println("hello world");
//        ElasticsearchLog es = new ElasticsearchLog();
//        try {
//            es.processWithJob();
//        } catch (Exception e) {
//            Logger.info("error jalankan job elastic");
//            e.printStackTrace();
//        }
//    }

    public void doJob()
    {
        sendEmailReport("Start job at:"+ new Date(),"Start Job Elastic");
        Logger.info("jalankan cron job elastic");
        ElasticsearchLog es = new ElasticsearchLog();
        try {
            es.processWithJob();
        } catch (Exception e) {
            Logger.info("error jalankan job elastic");
            e.printStackTrace();
        }
        Penyedia.getAllPenyediaOnElastic();
        sendEmailReport("End Job Elastic at:"+new Date(),"End Job Elastic");
        Logger.info("Job Elastic selesai "+ new Date().toString());
        try{
            Cache.delete(ElasticsearchRepository.POPULAR_CACHE_KEY);
            Cache.delete(ElasticsearchRepository.RECOMMENDATION_CACHE_KEY);
        }catch (Exception e){
            try{
                Cache.safeDelete(ElasticsearchRepository.POPULAR_CACHE_KEY);
                Cache.safeDelete(ElasticsearchRepository.RECOMMENDATION_CACHE_KEY);
            }catch (Exception ex){}
        }
    }

    public void sendEmailReport(String body, String title){
        //String title = "Report JOB elastics";
        String report = Configuration.getConfigurationValue(MailQueue.SMTP_MAIL_ADMIN, "");
        String[] ary = report.split(";");
        for (String to : ary) {
            MailQueue mq = new MailQueue();
            try {
                Logger.info(to+" "+body+" "+title);
                mq = EmailManager.createEmail(to, body, title);

            } catch (Exception e) {
                Logger.error("error create mail:"+e.getMessage());
            }
            Logger.info(mq.save()+"");
        }
    }
}
