package models.elasticsearch.query;

import com.google.gson.JsonObject;

/**
 * @author HanusaCloud on 10/4/2017
 */
public class CategoryFilter extends NestedObject {

    private Long categoryId;

    public CategoryFilter setPath(String path) {
        this.path = path;
        return this;
    }

    public CategoryFilter setField(String field) {
        this.field = field;
        return this;
    }

    public CategoryFilter setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    @Override
    public JsonObject build() {
        JsonObject result = new JsonObject();
        JsonObject cat = new JsonObject();
        cat.addProperty(this.getFieldPath(), categoryId);
        result.add("match", cat);
        return result;
    }
}
