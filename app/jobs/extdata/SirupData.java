package jobs.extdata;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import jobs.JobTrail.JobSirupData3;
import jobs.JobTrail.JobSirupData4;
import jobs.JobTrail.JobSirupData5;
import models.api.Rup;
import models.api.Sirup;
import models.jcommon.http.SimpleHttpRequest;
import models.util.Config;
import play.Logger;
import play.db.jdbc.Query;
import play.jobs.Job;
import play.libs.URLs;
import play.libs.WS;
import utils.KatalogUtils;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SirupData {
    int minuteWait = 1;
    public boolean resultGet = false;
    public String jsResult = "[]";
    private String url = Config.getInstance().sirup_api;
    //"https://sirup.lkpp.go.id/sirup/service/paketPenyediaTampilEpurchasing?auditupdate=2018-08-06";//
    private List<Sirup> sirupList = new ArrayList<>();
    private int secondTimeout = 120;

    public SirupData(){

    }
    public int getMinutetimeout(){
        if(secondTimeout>0){
            return(secondTimeout / 60);
        }
        return secondTimeout;
    }
    public void getData(Date date){
         getData(secondTimeout,date);
    }
    public void getData(Integer minuteTimeout, Date tgl_update){
        resultGet = false;
        jsResult = "[]";
        String date = KatalogUtils.toDateyyyyMMdd(tgl_update);
        Integer seconds = minuteTimeout;
        if(seconds>0){
            seconds = (minuteTimeout*60);
            secondTimeout = seconds;
        }

        Logger.info("run with minuteTimeout:"+minuteTimeout+" "+ tgl_update.toLocaleString());
        try (
            SimpleHttpRequest connect = new SimpleHttpRequest(secondTimeout)) {
            String params = "?auditupdate=" + URLs.encodePart(date);
            connect.get(url+params);
            jsResult = connect.getString();
            resultGet = true;
        } catch (InterruptedException e) {
            Logger.error("Interupted Exception:"+e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Logger.error("IO Exception:"+e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            Logger.error("Exception:"+e.getMessage());
            e.printStackTrace();
        }
        if(resultGet == false){//jika gagal create job

            JobSirupData3 jsd = new JobSirupData3(minuteWait,tgl_update);
            jsd.in(10);
//            new Job() {
//                public void doJob(){
//                    Logger.info("create JOb next 10 second");
//                    SirupData newobj = new SirupData();
//                    int newWait = minuteWait;
//                    newobj.getData(newWait++,tgl_update);
//                }
//            }.in(10);
        }
    }

    public List<Sirup> parseAndGetData(){
        try{
            sirupList = new Gson().fromJson(jsResult, new TypeToken<List<Sirup>>(){}.getType());
            Logger.info("total:"+sirupList.size());
        }catch (Exception e){
            Logger.error("error parse datasirup:"+e.getMessage());
            e.printStackTrace();
        }
        return sirupList;
    }

    public void executeGrabData(Date currentDate){
        SirupData obj = new SirupData();
        obj.getData(minuteWait,currentDate);
        if(!obj.resultGet){
            Logger.info("create new job");
            JobSirupData4 jsd4 = new  JobSirupData4(minuteWait, currentDate, this);
            jsd4.in(10);
//            new Job() {
//                public void doJob(){
//                    int newWait = minuteWait++;
//                    obj.getData(newWait,currentDate);
//                }
//            }.in(10);
        }
        List<Sirup> result = obj.parseAndGetData();
        Logger.info(result.size()+"");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void executeGrabData_test(){

        Calendar c = Calendar.getInstance();
        Date currentDate = new Date();
        c.setTime(currentDate);
        c.add(Calendar.DATE,-3);//mundur 3 hari tempat yang banyak, testin gpurpose
        for(int i = 0; i<10;i++){

            c.add(Calendar.DATE,-1);
            Logger.info("-------------"+ c.getTime().toString()+"-------------");
            SirupData obj = new SirupData();

            obj.getData(minuteWait,c.getTime());
            if(!obj.resultGet){
                Logger.info("create new job");
                JobSirupData5 js5 = new JobSirupData5(minuteWait);
                js5.in(10);

//                JobSirupData4 jsd4 = new JobSirupData4(minuteWait,currentDate,this);
//                jsd4.in(10);
//                new Job() {
//                    public void doJob(){
//                        int newWait = minuteWait;
//                        SirupData objs = new SirupData();
//                        objs.getData(newWait++,c.getTime());
//                    }
//                }.in(10);
            }
            List<Sirup> result = obj.parseAndGetData();
            Logger.info(result.size()+"");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public Timestamp getLatestAuditUpdate(){
        List<Rup> data = Query.find("select r.* from rup r order by r.audit_update asc limit 1",Rup.class).fetch();
        if(data.size()> 0){
            return data.get(0).audit_update;
        }
        return new Timestamp(new Date().getTime());
    }
}
