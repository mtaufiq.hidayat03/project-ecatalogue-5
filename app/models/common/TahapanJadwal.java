package models.common;

import models.katalog.komoditasmodel.KomoditasTahapanTemplateJadwal;

import java.util.ArrayList;
import java.util.List;

/**
 * model untuk list tahapan untuk template jadwal komoditas
 * @author lkpp
 *
 */
public class TahapanJadwal {

	public int index;

	public String nama_tahapan;
	
	public boolean required;

	public static List<TahapanJadwal> getList(List<KomoditasTahapanTemplateJadwal> templatetahapanList){
		List<TahapanJadwal> tahapanJadwals = new ArrayList<TahapanJadwal>();
		int index = 0;
		for(KomoditasTahapanTemplateJadwal templateTahapan:templatetahapanList){
			TahapanJadwal tahapanJadwal = new TahapanJadwal();
			tahapanJadwal.index = index;
			tahapanJadwal.nama_tahapan = templateTahapan.nama_tahapan;
			tahapanJadwals.add(tahapanJadwal);
			index++;
		}

		return tahapanJadwals;
	}

}
