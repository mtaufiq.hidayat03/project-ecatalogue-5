package services.datatable;

import models.common.ButtonGroup;
import play.i18n.Messages;

import java.util.List;

public class DataTableService {

	public static String generateButtonGroup(List<ButtonGroup> buttonGroups){

		StringBuilder sb = new StringBuilder();

		for (ButtonGroup button : buttonGroups){
			String attribute = "";
			if(button.isDelete){
				attribute = " class = 'delete' ";
			}

			if(button.isSelesaiNegosiasi){
				attribute = " id = 'selesaiNegosiasi' ";
			}
			if (button.openEdit && button.id != null) {
				attribute = " class='open-edit' data-value='" + button.id + "'";
			}
			sb.append("<li><a href='").append(button.url).append("'").append(attribute).append(">").append(button.nama).append("</a></li>");
		}

		return  "<div class='dropdown dropdown-option' style='text-align:center'><button class='btn btn-default dropdown-toggle' "
				+ "type='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>" //id='dropdown-option'
				+ Messages.get("aksi.label") + " <i class='fa fa-caret-down'></i></button>"
				+ "<ul class='dropdown-menu' aria-labelledby='dropdown-option'>" + sb.toString() + "</ul>"
				+ "</div>";
	}

}
