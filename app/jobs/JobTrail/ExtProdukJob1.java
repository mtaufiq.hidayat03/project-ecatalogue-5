package jobs.JobTrail;

import jobs.ExtProduct.ExtProduct;
import jobs.ExtProduct.PenyediaKomoditasApi;
import models.api.produk.HistoryDatafeed;
import play.db.jpa.NoTransaction;
import play.jobs.Job;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@NoTransaction
public class ExtProdukJob1 extends Job {
    UUID id = UUID.randomUUID();
    PenyediaKomoditasApi _pk = new PenyediaKomoditasApi();
    HistoryDatafeed hdf = new HistoryDatafeed();
    public ExtProdukJob1(PenyediaKomoditasApi pk,HistoryDatafeed _hdf){
        _pk = pk;
        hdf = _hdf;
    }
    public void doJob(){
        ExtProduct ext = new ExtProduct();
        hdf.id_job = id.toString();
        hdf.penyedia_id = _pk.penyediaid;
        hdf.penyedia_name = _pk.nama_penyedia;
        hdf.start_time = new Timestamp(new Date().getTime()).toString();
        hdf.save();
        ext.executeOne(_pk,hdf);
    }
}
