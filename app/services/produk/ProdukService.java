package services.produk;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import controllers.katalog.ProductSearchResult;
import controllers.katalog.form.ProductForm;
import controllers.katalog.form.SearchQuery;
import models.common.AktifUser;
import models.common.DokumenInfo;
import models.common.HargaProdukDaerah;
import models.common.OngkosKirim;
import models.elasticsearch.log.ProductLogJson;
import models.jcommon.blob.BlobTable;
import models.jcommon.util.CommonUtil;
import models.katalog.*;
import models.katalog.komoditasmodel.KomoditasAtributHarga;
import models.masterdata.Kabupaten;
import models.masterdata.Kurs;
import models.masterdata.Manufaktur;
import models.masterdata.Provinsi;
import models.penyedia.Penyedia;
import models.util.produk.*;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.util.TextUtils;
import play.Logger;
import play.cache.Cache;
import play.db.jdbc.CacheMerdeka;
import play.db.jdbc.Query;
import repositories.penyedia.ProviderRepository;
import utils.DateTimeUtils;
import utils.LogUtil;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

import static constants.models.ProdukConstant.*;

/**
 * Created by rayhanfrds on 9/15/17.
 */
public class ProdukService {

	public static final String TAG = "ProdukService";

	private static final String Q_DAFTAR_PRODUK_NASIONAL = "from produk p " +
			"join penyedia pn on p.penyedia_id = pn.id left join penyedia_kontrak pk on pn.id = pk.penyedia_id " +
			"join komoditas k on p.komoditas_id = k.id " +
			"where (p.harga_utama is not null or p.harga_utama <> 0) and p.active = 1 and p.apakah_ditayangkan = 1 " +
			"and p.minta_disetujui = 0 and berlaku_sampai >= curdate() and p.setuju_tolak = 'setuju' " +
			"and pk.tgl_masa_berlaku_mulai <= curdate() and pk.tgl_masa_berlaku_selesai >= curdate() and pk.active = 1 AND pk.apakah_berlaku = 1 " +
			"and (k.apakah_stok_unlimited = 1 or p.jumlah_stok > 0) ";

	private static final String Q_DAFTAR_PRODUK_PROVINSI = "from produk p " +
			"join penyedia pn on p.penyedia_id = pn.id left join penyedia_kontrak pk on pn.id = pk.penyedia_id " +
			"join komoditas k on p.komoditas_id = k.id join produk_wilayah_jual_provinsi pwj " +
			"on p.id = pwj.produk_id " +
			"where p.active = 1 and p.apakah_ditayangkan = 1 " +
			"and p.minta_disetujui = 0 and p.setuju_tolak = 'setuju' " +
			"and pk.tgl_masa_berlaku_mulai <= curdate() and pk.tgl_masa_berlaku_selesai >= curdate() and pk.active = 1 AND pk.apakah_berlaku = 1 " +
			"and (k.apakah_stok_unlimited = 1 or p.jumlah_stok > 0) and pwj.provinsi_id = ? " +
			"and pwj.active = 1";

	private static final String Q_DAFTAR_PRODUK_KABUPATEN = "from produk p " +
			"join penyedia pn on p.penyedia_id = pn.id left join penyedia_kontrak pk on pn.id = pk.penyedia_id " +
			"join komoditas k on p.komoditas_id = k.id join produk_wilayah_jual_kabupaten pwj " +
			"on p.id = pwj.produk_id " +
			"where p.active = 1 and p.apakah_ditayangkan = 1 " +
			"and p.minta_disetujui = 0 and p.setuju_tolak = 'setuju' " +
			"and pk.tgl_masa_berlaku_mulai <= curdate() and pk.tgl_masa_berlaku_selesai >= curdate() and pk.active = 1 AND pk.apakah_berlaku = 1 " +
			"and (k.apakah_stok_unlimited = 1 or p.jumlah_stok > 0) and pwj.kabupaten_id = ? " +
			"and pwj.active = 1";

	public static final String Q_DAFTAR_PRODUK_GENERAL = "SELECT p." + ID +
			", p." + NAMA_PRODUK +
			", p." + NO_PRODUK +
			", p.no_produk_penyedia" +
			", p." + HARGA_UTAMA +
			", p." + JENIS_PRODUK +
			", p.berlaku_sampai"+
			", p."+ CREATED_DATE +
			", p."+ MODIFIED_DATE +
			", p."+ JUMLAH_STOK +
			", p." + IS_UMKM +
			", (CASE WHEN p.margin_harga > 0 THEN" +
			" ROUND((p.harga_utama + (p.harga_utama * p.margin_harga / 100 )), 2)" +
			" ELSE" +
			" p.harga_utama" +
			" END) as " + HARGA_PEMERINTAH +
			", k.id as komoditas_id" +
			", k.nama_komoditas" +
			", k.kelas_harga" +
			", k.apakah_iklan" +
			", cat.id as produk_kategori_id" +
			", cat.nama_kategori" +
			", m.id as " + MANUFAKTUR_ID +
			", m." + NAMA_MANUFAKTUR +
			", pn.id as penyedia_id" +
			", pn.nama_penyedia" +
			",pn.produk_api_url" +
			" FROM produk p" +
			" JOIN produk_kategori cat on cat.id = p.produk_kategori_id" +
			" JOIN penawaran pw on pw.id = p.penawaran_id" +
			" JOIN penyedia_kontrak pk on pk.id = pw.kontrak_id" +
			" JOIN penyedia pn on pn.id = pk.penyedia_id" +
			" JOIN komoditas k" +
			" ON p.komoditas_id = k.id" +
			" JOIN manufaktur m" +
			" ON m.id = p.manufaktur_id" +
			" WHERE p.active = 1 AND p.apakah_ditayangkan = 1" +
			" AND k.active = 1" +
			// see : catatan minta disetujui
			// " AND p.minta_disetujui = 0 "+
			" AND p.setuju_tolak = 'setuju'" +
			" AND p.status NOT IN ('draft')" +
			// " AND p.berlaku_sampai <> ''" +
			// " AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1" +
			" AND ((p.berlaku_sampai >= CURDATE() AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1)" +
			" OR (p.berlaku_sampai is null AND pk.tgl_masa_berlaku_mulai <= curdate() AND pk.tgl_masa_berlaku_selesai >= curdate() AND pk.active = 1 AND pk.apakah_berlaku = 1))" +
			" AND (k.apakah_stok_unlimited = 1 OR (k.apakah_stok_unlimited = 0 AND p.jumlah_stok > 0)) ";

	public static List<Produk> daftarProduk(Integer offset, Integer max, String order, String sort, String keyword,
											Long komoditas_id, Long penyedia_id, Long manufaktur_id, Long provinsi_id,
											Long kabupaten_id, Double harga_dari, Double harga_sampai, String wilayah_jual,
											Object... params) {

		if(komoditas_id == null || komoditas_id == 0){
			return null;
		}

		StringBuilder sql = new StringBuilder();
		if(wilayah_jual.equalsIgnoreCase("kabupaten")){
			sql.append("SELECT p.*, pwj.harga_utama as harga_utama_daerah ");
			sql.append(Q_DAFTAR_PRODUK_KABUPATEN);
			if(kabupaten_id == null || kabupaten_id == 0){
				return null;
			}
			params = ArrayUtils.add(params, kabupaten_id);

		} else if(wilayah_jual.equalsIgnoreCase("provinsi")){
			sql.append("SELECT p.*, pwj.harga_utama as harga_utama_daerah ");
			sql.append(Q_DAFTAR_PRODUK_PROVINSI);
			if(provinsi_id == null || provinsi_id == 0){
				return null;
			}
			params = ArrayUtils.add(params, provinsi_id);

		} else if(wilayah_jual.equalsIgnoreCase("nasional")){
			sql.append("SELECT p.* ");
			sql.append(Q_DAFTAR_PRODUK_NASIONAL);

		}

		// TODO : filtering & how to avoid SQL injection?
		String filter = "";
		if(!CommonUtil.isEmpty(keyword)){
			filter += " AND p.nama_produk LIKE ?";
			keyword = "%"+keyword+"%";
			params = ArrayUtils.add(params, keyword);
		}

		if(komoditas_id != null && komoditas_id != 0){
			filter += " AND k.id = ?";
			params = ArrayUtils.add(params, komoditas_id);
		}

		if(penyedia_id != null && penyedia_id != 0){
			filter += " AND kn.id = ?";
			params = ArrayUtils.add(params, penyedia_id);
		}

		if(manufaktur_id != null && manufaktur_id != 0){
			filter += " AND p.manufaktur_id = ?";
			params = ArrayUtils.add(params, manufaktur_id);
		}

		if(harga_dari != null && harga_dari != 0){
			filter += " AND p.harga_utama >= ?";
			params = ArrayUtils.add(params, harga_dari);
		}

		if(harga_sampai != null && harga_sampai != 0){
			filter += " AND p.harga_utama <= ?";
			params = ArrayUtils.add(params, harga_sampai);
		}

		if (!StringUtils.isEmpty(filter)) {
//			sql.append(sql.toString().toUpperCase().contains("WHERE") ? " AND " : " WHERE ");
			sql.append(filter);
		}

		// paging
		sql.append(" LIMIT ? OFFSET ? ");
		params = ArrayUtils.add(params, max);
		params = ArrayUtils.add(params, offset);

		if(StringUtils.isEmpty(order)){
			// TODO : ordering & how to avoid SQL injection?
		}

		Logger.info("query elastics produk:"+sql.toString());
		return Query.find(sql.toString(), Produk.class, params).fetch();
	}

	public static Long jumlahProduk(String keyword, Long komoditas_id, Long penyedia_id, Long manufaktur_id, Long provinsi_id,
									Long kabupaten_id, Double harga_dari, Double harga_sampai, Object... params) {

		StringBuilder sql = new StringBuilder("SELECT COUNT(p.id) ");
		sql.append(Q_DAFTAR_PRODUK_NASIONAL);
		String filter = "";
		if(!CommonUtil.isEmpty(keyword)){
			filter += " AND p.nama_produk LIKE ?";
			keyword = "%"+keyword+"%";
			params = ArrayUtils.add(params, keyword);
		}

		if(komoditas_id != null && komoditas_id != 0){
			filter += " AND k.id = ?";
			params = ArrayUtils.add(params, komoditas_id);
		}

		if(penyedia_id != null && penyedia_id != 0){
			filter += " AND kn.id = ?";
			params = ArrayUtils.add(params, penyedia_id);
		}

		if(manufaktur_id != null && manufaktur_id != 0){
			filter += " AND p.manufaktur_id = ?";
			params = ArrayUtils.add(params, manufaktur_id);
		}

		if(harga_dari != null && harga_dari != 0){
			filter += " AND p.harga_utama >= ?";
			params = ArrayUtils.add(params, harga_dari);
		}

		if(harga_sampai != null && harga_sampai != 0){
			filter += " AND p.harga_utama <= ?";
			params = ArrayUtils.add(params, harga_sampai);
		}

		if (!StringUtils.isEmpty(filter)) {
//			sql.append(sql.toString().toUpperCase().contains("WHERE") ? " AND " : " WHERE ");
			sql.append(filter);
		}

		return Query.count(sql.toString(), params);
	}

	public static Produk getProduk(Long id, Long daerah_id, String type, Object... params){
		if(id == null || id == 0){
			return null;
		}

		StringBuilder sql = new StringBuilder();
		if(type.equalsIgnoreCase("regency")){
			sql.append("SELECT p.*, pwj.harga_utama as harga_utama_daerah ");
			sql.append(Q_DAFTAR_PRODUK_KABUPATEN);
			sql.append(" and p.id = ?");
			params = ArrayUtils.add(params, daerah_id);
			params = ArrayUtils.add(params, id);

		} else if(type.equalsIgnoreCase("province")){
			sql.append("SELECT p.*, pwj.harga_utama as harga_utama_daerah ");
			sql.append(Q_DAFTAR_PRODUK_PROVINSI);
			sql.append(" and p.id = ?");
			params = ArrayUtils.add(params, daerah_id);
			params = ArrayUtils.add(params, id);

		} else {
			sql.append("SELECT p.* ");
			sql.append(Q_DAFTAR_PRODUK_NASIONAL);
			sql.append(" and p.id = ?");
			params = ArrayUtils.add(params, id);
		}

		// TODO : filtering & how to avoid SQL injection?
		String filter = " and p.id = ?";
		params = ArrayUtils.add(params, id);

		if (!StringUtils.isEmpty(filter)) {
			sql.append(filter);
		}

		return Query.find(sql.toString(), Produk.class, params).first();
	}



	public static List<HargaProdukJson> getHargaProdukJson(ProductForm model, KomoditasAtributHarga harga, boolean isNeedDeliveryCost){
		LogUtil.d(TAG, "generate price from json");
		LogUtil.d(TAG, model.hargaJson);
		LogUtil.d(TAG, model.ongkirJson);

		List<HargaProdukJson> hargaProdukJsonList = new ArrayList<>();

		if (!TextUtils.isEmpty(model.hargaJson)) {

			JsonArray hargaJsonArray = model.getHargaJsonArray();
			if (hargaJsonArray != null && hargaJsonArray.size() > 0) {

				List<Kabupaten> kabupatenList = Kabupaten.find("active = 1").fetch();
				List<Provinsi> provinsiList = Provinsi.find("active = 1").fetch();
				for (int i = 0; i < hargaJsonArray.size(); i++) {
					JsonObject obj = hargaJsonArray.get(i).getAsJsonObject();
					HargaProdukJson hargaProdukJson = new HargaProdukJson();
					String namaProvinsiFromJson = obj.get("Provinsi").getAsString();
					Provinsi provinsi = provinsiList.stream().filter(f-> f.nama_provinsi.equalsIgnoreCase(namaProvinsiFromJson)).findFirst().get(); //Provinsi.findByName(namaProvinsiFromJson);
					hargaProdukJson.provinsiId = provinsi.id;
					if(model.kelas_harga.equalsIgnoreCase("kabupaten")){
						String namaKabupatenFromJson = obj.get("Kabupaten").getAsString();
						Kabupaten kabupaten =  kabupatenList.stream().filter(m-> m.nama_kabupaten.equalsIgnoreCase(namaKabupatenFromJson)).findFirst().get(); //Kabupaten.findByName(namaKabupatenFromJson);
						hargaProdukJson.kabupatenId = kabupaten.id;
					}

					hargaProdukJson.harga = obj.get(harga.label_harga).getAsBigDecimal();
					hargaProdukJsonList.add(hargaProdukJson);

				}

			}

		}
		return hargaProdukJsonList;
	}
	public static List<OngkirProdukJson> getOngkirProdukJson(ProductForm model, long komoditas_id){
		LogUtil.d(TAG, "generate price from json");
		LogUtil.d(TAG, model.hargaJson);
		LogUtil.d(TAG, model.ongkirJson);

		List<OngkirProdukJson> ongkirJsonList = new ArrayList<>();

		if (!TextUtils.isEmpty(model.ongkirJson) || !TextUtils.isEmpty(model.ongkirJson)) {

			JsonArray ongkirJsonArray = model.getOngkirJsonArray();
			if (ongkirJsonArray != null && ongkirJsonArray.size() > 0) {
				List<Kabupaten> kabupatenList = Kabupaten.find("active = 1").fetch();
				List<Provinsi> provinsiList = Provinsi.find("active = 1").fetch();
				for (int i = 0; i < ongkirJsonArray.size(); i++) {
					JsonObject obj = ongkirJsonArray.get(i).getAsJsonObject();
					OngkirProdukJson ongkirProdukJson = new OngkirProdukJson();

					String namaProvinsiFromJson = obj.get("Provinsi").getAsString();
					//Logger.info(namaProvinsiFromJson+" nama provinsi");
					Provinsi provinsi = provinsiList.stream().filter(f-> f.nama_provinsi.equalsIgnoreCase(namaProvinsiFromJson)).findFirst().get() ;//Provinsi.findByName(namaProvinsiFromJson);
					ongkirProdukJson.provinsiId = provinsi.id;

					// ipeng - 21-03-2018 : perubahan ongkir untuk semua kelas harga berdasarkan kabupaten
					if(obj.get("Kabupaten") != null){
						String namaKabupatenFromJson = obj.get("Kabupaten").getAsString();
						Kabupaten kabupaten = kabupatenList.stream().filter(f-> f.nama_kabupaten.equalsIgnoreCase(namaKabupatenFromJson)).findFirst().get(); //Kabupaten.findByName(namaKabupatenFromJson);
						ongkirProdukJson.kabupatenId = kabupaten.id;
					}

					ongkirProdukJson.ongkir = obj.get("Referensi Ongkos Kirim").getAsBigDecimal();
					ongkirJsonList.add(ongkirProdukJson);
				}
			}
		}
		return ongkirJsonList;
	}


	public static Produk saveProduk(Produk produk, AktifUser aktifUser, ProductForm model){
		//error logic ketika produk saveedit

//		Produk exProduk = produk.id == null ? produk : Produk.findById(produk.id);
//		Produk productModel = (model.isCreatedMode() || produk.id == null) ? new Produk() : produk;
//		productModel.setData(exProduk, model, produk.jumlah_stok_form, produk.jumlah_stok_inden_form);

		//produk adalah objek yang di kirim ketika submit baik new atau update
		//produkmodel adalah new objek jika produk dari form adalah objek baru(id null), selain itu maka di find untuk di
		//update dari produk

		Produk productModel = (model.isCreatedMode() || produk.id == null)  ? new Produk() : Produk.findById(produk.id);
		productModel.setData(produk, model, produk.jumlah_stok_form, produk.jumlah_stok_inden_form);

		Komoditas komoditas = Komoditas.findById(produk.komoditas_id);

		Map<Long,BigDecimal> mapProductPrice = model.createMapFromProductPriceForm();

		boolean isLelang = true;

		if(komoditas.isLelang() && !isLelang){//sementara dihardcode untuk lelang
			productModel.status = Produk.STATUS_TERIMA;
			productModel.minta_disetujui = false;
			productModel.setuju_tolak = Produk.SETUJU;

		}else if(!komoditas.isDatafeed()){
			if(komoditas.perlu_approval_produk == true){
				productModel.status = Produk.STATUS_MENUNGGU_PERSETUJUAN;
				productModel.minta_disetujui = true;
				productModel.minta_disetujui_alasan = model.reason;
				productModel.minta_disetujui_oleh = aktifUser.user_id;
				productModel.minta_disetujui_tanggal = new Timestamp(new java.util.Date().getTime());
				productModel.apakah_dapat_diedit = 1;
				productModel.apakah_ditayangkan = false;
				productModel.apakah_dapat_dibeli = false;
				productModel.setuju_tolak = null;
			} else {
				productModel.status = Produk.STATUS_TERIMA;
				productModel.minta_disetujui = false;
				productModel.setuju_tolak = Produk.SETUJU;
				productModel.apakah_ditayangkan = true;
				productModel.apakah_dapat_dibeli = true;
				productModel.setuju_tolak_alasan = "Approved by system";
				productModel.setuju_tolak_oleh = Long.valueOf(-99);
				productModel.setuju_tolak_tanggal = new Timestamp(new java.util.Date().getTime());
				productModel.minta_disetujui_alasan = null;
				productModel.minta_disetujui_oleh = null;
				productModel.minta_disetujui_tanggal = null;
			}
		}
		if (model.isDefaultDraft()) {
			LogUtil.debug(TAG, "set default status draft, this product is set to temporary");
			productModel.status = "draft";
		}
		//notes:
		// karena yang buat produk rekeanan/penyedia
		Penyedia penyedia = ProviderRepository.findByUserId(aktifUser.user_id);
		productModel.penyedia_id = penyedia.id;

		/*
			todo: ada perhitungan margin harga
			nantinya akan berimbas pada produk tayang
		 */
		productModel.margin_harga = new Double(0);
		if(komoditas.isNational() && produk.harga_utama != model.getMainPrice()){
			// productModel.harga_tanggal = model.parseKursDate();
			// 19 juli, harga tangggal diambil dari inputan tab upload harga
			productModel.harga_tanggal = DateTimeUtils.parseDdMmYyyy(model.tanggal_harga);
			productModel.harga_utama = model.getMainPrice();

		}
		if(!komoditas.isRegency() && komoditas.isNeedDeliveryCost() && !model.ongkirJson.isEmpty()){
			productModel.ongkir = CommonUtil.toJson(getOngkirProdukJson(model,komoditas.id));
		}
		if(komoditas.isRegency() && komoditas.isNeedDeliveryCost() && !model.hargaJson.isEmpty()){
			productModel.ongkir = model.getOngkirJsonArrayFromPrice();
		}

		//save produk
		productModel.saveProduct();
		addHistory(productModel, model);

		//save produk harga
		List<KomoditasAtributHarga> hargaNonOngkir = KomoditasAtributHarga.findHargaNonOngkirByKomoditas(komoditas.id);
		LogUtil.debug(TAG, hargaNonOngkir);
		//nonaktifkan produkharga yang eksisting
		if(null != produk.id) {
			List<ProdukHarga> list = ProdukHarga.findRiwayatByProdukId(produk.id);
			for (ProdukHarga ph: list) {
				ph.active = false;
				ph.created_date = ph.created_date;
				ph.created_by = ph.created_by;
				ph.modified_date = new Timestamp(System.currentTimeMillis());
				ph.modified_by = AktifUser.getActiveUserId();
				ph.save();
			}
		}

		for(KomoditasAtributHarga harga : hargaNonOngkir){
			String hargaJsonResult;

			if(komoditas.isNational()){
				HargaProdukJson hargaProdukJson = new HargaProdukJson();
				hargaProdukJson.harga = mapProductPrice.get(harga.id);
				hargaJsonResult = CommonUtil.toJson(hargaProdukJson);

				// // save to ProdukHargaNasional
				// ProdukService.saveHargaNasional(productModel, harga, mapProductPrice);
			}else{
				List<HargaProdukJson> hargaProdukJson = getHargaProdukJson(model,harga, komoditas.isNeedDeliveryCost());

				if(harga.apakah_harga_utama){
					saveWilayahJual(model, hargaProdukJson, productModel);
				}

				hargaJsonResult = CommonUtil.toJson(hargaProdukJson);
			}

			LogUtil.d("hargaJsonResult",hargaJsonResult);

			// productModel.harga_tanggal = model.parseKursDate();
			// 19 juli, harga tangggal diambil dari inputan tab upload harga
			Date tglharga = DateTimeUtils.parseDdMmYyyy(model.tanggal_harga);
			if (productModel.harga_tanggal == null) {
				productModel.harga_tanggal = new Date();
			}
			ProdukHarga phExist = ProdukHarga.getByProdAndTglHarga(productModel.id, productModel.harga_tanggal, harga.id);
			//nonaktifkan produkharga yang lalu
			if(phExist!=null){
				phExist.active = false;
				phExist.created_date = phExist.created_date;
				phExist.created_by = phExist.created_by;
				phExist.deleted_date = new Timestamp(System.currentTimeMillis());
				phExist.deleted_by = AktifUser.getActiveUserId();
				phExist.save();
			}
			new ProdukHarga(
					productModel.id,
					harga.id,
					productModel.harga_kurs_id,
					hargaJsonResult,
					//model.parseKursDate(),
					tglharga,
					model.getXlsJson(),
					model.checkedLocations).save();
		}

		//start collect info spek produk
		Map<String, String> infoMap = getInformationFromJson(model.infoJson);
		if (!infoMap.isEmpty()) {
			for (Map.Entry<String, String> entry : infoMap.entrySet()) {
				if (!model.isCreatedMode()) {
					ProdukAtributValue.UpdateProdukAtributValue(productModel.id, Long.valueOf(entry.getKey()));
				}
				new ProdukAtributValue(productModel.id, entry.getValue(), Long.valueOf(entry.getKey())).save();
			}
		}
		//end of collect info spek produk

		//save lampiran
		if (!ArrayUtils.isEmpty(model.file_id)) {
			for (int id : model.file_id) {
				ProdukLampiran.updateProdukLampiran(id, productModel.id);
			}
		}

		//save gambar
		if (!ArrayUtils.isEmpty(model.pict_id)) {
			//bisa jadi file banyak, tapi asimsikan tetep yang ke 1, yang di set ke gambar produk
			if(null != productModel.id && model.pict_id.length > 0 ){
				ProdukGambar pgs = ProdukGambar.findById(model.pict_id[0]);
				productModel.produk_gambar_file_name = pgs.file_name;
				productModel.produk_gambar_file_sub_location = pgs.file_sub_location;
			}
			for (int id : model.pict_id) {
				//ProdukGambar.updateProdukGambar(id, productModel.id);
				//update produkgambar ke produk id yang di maksud, tapi sesuaikan dengan requirement terakhir
				//karena produkgambar sekarang defaultnya setelah di upload di form ceate produk adalah notaktif
				ProdukGambar gmb = ProdukGambar.findById(id);
				gmb.produk_id = productModel.id;
				gmb.active = 1;
				gmb.save();

			}
		}

		if(komoditas.perlu_approval_produk == true){
			if (ProdukTungguSetuju.countByProdukId(productModel.id) == 0) {
				new ProdukTungguSetuju().setApproval(productModel.id).save();
			}
		}

		return productModel;
	}

	public static Produk saveEditProduk(Produk produk, AktifUser aktifUser, ProductForm model){

		Produk productModel = (model.isCreatedMode() || produk.id == null)  ? new Produk() : Produk.findById(produk.id);
		productModel.setData(produk, model, produk.jumlah_stok_form, produk.jumlah_stok_inden_form);

		Komoditas komoditas = Komoditas.findById(produk.komoditas_id);

		Map<Long,BigDecimal> mapProductPrice = model.createMapFromProductPriceForm();

		boolean isLelang = true;

		if(komoditas.isLelang() && !isLelang){ //sementara dihardcode untuk lelang
			productModel.status = Produk.STATUS_TERIMA;
			productModel.minta_disetujui = false;
			productModel.setuju_tolak = Produk.SETUJU;

		}else{
			if(komoditas.perlu_approval_produk == true){
				productModel.status = Produk.STATUS_MENUNGGU_PERSETUJUAN;
				productModel.minta_disetujui = true;
				productModel.minta_disetujui_alasan = model.reason;
				productModel.minta_disetujui_oleh = aktifUser.user_id;
				productModel.minta_disetujui_tanggal = new Timestamp(new java.util.Date().getTime());
				productModel.apakah_dapat_diedit = 1;
				productModel.apakah_ditayangkan = false;
				productModel.apakah_dapat_dibeli = false;
				productModel.setuju_tolak = null;
			} else {
				productModel.status = Produk.STATUS_TERIMA;
				productModel.minta_disetujui = false;
				productModel.setuju_tolak = Produk.SETUJU;
				productModel.apakah_ditayangkan = true;
				productModel.apakah_dapat_dibeli = true;
				productModel.setuju_tolak_alasan = "Approved by system";
				productModel.setuju_tolak_oleh = Long.valueOf(-99);
				productModel.setuju_tolak_tanggal = new Timestamp(new java.util.Date().getTime());
				productModel.minta_disetujui_alasan = null;
				productModel.minta_disetujui_oleh = null;
				productModel.minta_disetujui_tanggal = null;
			}
		}

		//notes:
		// karena yang buat produk rekeanan/penyedia
		Penyedia penyedia = ProviderRepository.findByUserId(aktifUser.user_id);
		productModel.penyedia_id = penyedia.id;

		/*
			todo: ada perhitungan margin harga
			nantinya akan berimbas pada produk tayang
		 */
		productModel.margin_harga = new Double(0);
		if(komoditas.isNational() && produk.harga_utama != model.getMainPrice()){
			// productModel.harga_tanggal = model.parseKursDate();
			// 19 juli, harga tangggal diambil dari inputan tab upload harga
			productModel.harga_tanggal = DateTimeUtils.parseDdMmYyyy(model.tanggal_harga);
			productModel.harga_utama = model.getMainPrice();

		}
		if(!komoditas.isRegency() && komoditas.isNeedDeliveryCost() && !model.ongkirJson.isEmpty()){
			productModel.ongkir = CommonUtil.toJson(getOngkirProdukJson(model,komoditas.id));
		}
		if(komoditas.isRegency() && komoditas.isNeedDeliveryCost() && !model.hargaJson.isEmpty()){
			productModel.ongkir = model.getOngkirJsonArrayFromPrice();
		}

		//save produk
		productModel.saveProduct();
		addHistory(productModel, model);

		//save produk harga
		List<KomoditasAtributHarga> hargaNonOngkir = KomoditasAtributHarga.findHargaNonOngkirByKomoditas(komoditas.id);

		//nonaktifkan produkharga yang eksisting
		if(null != produk.id) {
			List<ProdukHarga> list = ProdukHarga.findRiwayatByProdukId(produk.id);
			for (ProdukHarga ph: list) {
				ph.active = false;
				ph.created_date = ph.created_date;
				ph.created_by = ph.created_by;
				ph.modified_date = new Timestamp(System.currentTimeMillis());
				ph.modified_by = AktifUser.getActiveUserId();
				ph.save();
			}
		}

		for(KomoditasAtributHarga harga : hargaNonOngkir){
			String hargaJsonResult;

			if(komoditas.isNational()){
				HargaProdukJson hargaProdukJson = new HargaProdukJson();
				hargaProdukJson.harga = mapProductPrice.get(harga.id);
				hargaJsonResult = CommonUtil.toJson(hargaProdukJson);

				// // save to ProdukHargaNasional
				// ProdukService.saveHargaNasional(productModel, harga, mapProductPrice);
			}else{
				List<HargaProdukJson> hargaProdukJson = getHargaProdukJson(model,harga, komoditas.isNeedDeliveryCost());

				if(harga.apakah_harga_utama){
					saveWilayahJual(model, hargaProdukJson, productModel);
				}

				hargaJsonResult = CommonUtil.toJson(hargaProdukJson);
			}

			LogUtil.d("hargaJsonResult",hargaJsonResult);

			// productModel.harga_tanggal = model.parseKursDate();
			// 19 juli, harga tangggal diambil dari inputan tab upload harga
			Date tglharga = DateTimeUtils.parseDdMmYyyy(model.tanggal_harga);
			ProdukHarga phExist = ProdukHarga.getByProdAndTglHarga(productModel.id, productModel.harga_tanggal, harga.id);
			//nonaktifkan produkharga yang lalu
			if(phExist!=null){
				phExist.active = false;
				phExist.created_date = phExist.created_date;
				phExist.created_by = phExist.created_by;
				phExist.deleted_date = new Timestamp(System.currentTimeMillis());
				phExist.deleted_by = AktifUser.getActiveUserId();
				phExist.save();
			}
			new ProdukHarga(
					productModel.id,
					harga.id,
					productModel.harga_kurs_id,
					hargaJsonResult,
					//model.parseKursDate(),
					tglharga,
					model.getXlsJson(),
					model.checkedLocations).save();
		}

		//start collect info spek produk
		Map<String, String> infoMap = getInformationFromJson(model.infoJson);
		if (!infoMap.isEmpty()) {
			for (Map.Entry<String, String> entry : infoMap.entrySet()) {
				if (!model.isCreatedMode()) {
					ProdukAtributValue.UpdateProdukAtributValue(productModel.id, Long.valueOf(entry.getKey()));
				}
				new ProdukAtributValue(productModel.id, entry.getValue(), Long.valueOf(entry.getKey())).save();
			}
		}
		//end of collect info spek produk

		//save lampiran
		if (!ArrayUtils.isEmpty(model.file_id)) {
			for (int id : model.file_id) {
				ProdukLampiran.updateProdukLampiran(id, productModel.id);
			}
		}

		//save gambar
		if (!ArrayUtils.isEmpty(model.pict_id)) {
			//bisa jadi file banyak, tapi asimsikan tetep yang ke 1, yang di set ke gambar produk
			if(null != productModel.id && model.pict_id.length > 0 ){
				ProdukGambar pgs = ProdukGambar.findById(model.pict_id[0]);
				productModel.produk_gambar_file_name = pgs.file_name;
				productModel.produk_gambar_file_sub_location = pgs.file_sub_location;
			}
			for (int id : model.pict_id) {
				//ProdukGambar.updateProdukGambar(id, productModel.id);
				//update produkgambar ke produk id yang di maksud, tapi sesuaikan dengan requirement terakhir
				//karena produkgambar sekarang defaultnya setelah di upload di form ceate produk adalah notaktif
				ProdukGambar gmb = ProdukGambar.findById(id);
				gmb.produk_id = productModel.id;
				gmb.active = 1;
				gmb.save();

			}
		}

		if(komoditas.perlu_approval_produk == true){
			if (ProdukTungguSetuju.countByProdukId(productModel.id) == 0) {
				new ProdukTungguSetuju().setApproval(productModel.id).save();
			}
		}

		return productModel;
	}

	// private static void saveHargaNasional(Produk produk, KomoditasAtributHarga harga, Map<Long,BigDecimal> mapProductPrice){
	
	// 	String filterDate = "";
	// 	filterDate = DateTimeUtils.formatDateToString(new Date(),"YYYY-MM-dd");

	// 	ProdukHargaNasional phn = (ProdukHargaNasional)ProdukHargaNasional.find("harga_tanggal = ? and komoditas_harga_atribut_id = ?", filterDate, harga.id).fetch().stream().findAny().orElse(null);
		
	// 	if(phn == null){
	// 		phn = new ProdukHargaNasional(
	// 					produk.id,
	// 					produk.harga_kurs_id,
	// 					mapProductPrice.get(harga.id).doubleValue(),
	// 					harga.id
	// 		);
	// 	}
	// 	else phn.harga = mapProductPrice.get(harga.id).doubleValue();
	// 	phn.save();
	// }

	public static Map<String, String> getInformationFromJson(String json) {
		if (TextUtils.isEmpty(json)) {
			json = "{}";
		}
		Type type = new TypeToken<Map<String, String>>(){}.getType();
		Map<String, String> results = new HashMap<>();
		results.putAll(CommonUtil.fromJson(json, type));
		return results;
	}

	public static void savePublicWilayahJual (ProductForm model, List<HargaProdukJson> hargaProdukJson, Produk produk){
		saveWilayahJual(model, hargaProdukJson, produk);
	}
	private static void saveWilayahJual(ProductForm model, List<HargaProdukJson> hargaProdukJson, Produk produk){
		LogUtil.d(TAG, "saving market share: " + model.kelas_harga);
		Date kursDate = model.parseKursDate();

		if (!model.isCreatedMode() && hargaProdukJson != null && !hargaProdukJson.isEmpty()) {
			LogUtil.d(TAG, "delete all price coverage");
			LogUtil.d(TAG, "total in json: " + hargaProdukJson.size());
			int ok = ProdukWilayahJualKabupaten.deleteByProdukId(produk.id);
			LogUtil.d(TAG,"masuk sini gan " + ok);
			ProdukWilayahJualProvinsi.delete("produk_id = ?", produk.id);
		}

		if (!hargaProdukJson.isEmpty()) {
			if (model.kelas_harga.equalsIgnoreCase(Komoditas.KABUPATEN)) {
				LogUtil.d(TAG, "regency market");
				for (HargaProdukJson harga : hargaProdukJson) {
					ProdukWilayahJualKabupaten wKabupaten = new ProdukWilayahJualKabupaten();
					wKabupaten.kabupaten_id = harga.kabupatenId;
					wKabupaten.produk_id = produk.id;
					wKabupaten.harga_utama = harga.harga;
					wKabupaten.harga_ongkir = new BigDecimal(0);
					wKabupaten.harga_tanggal = kursDate;
					wKabupaten.harga_kurs_id = produk.harga_kurs_id;
					wKabupaten.margin_harga = new Double(0);
					wKabupaten.active = true;
					wKabupaten.save();
				}
			} else if (model.kelas_harga.equalsIgnoreCase(Komoditas.PROVINSI)) {
				LogUtil.d(TAG, "province market");
				for (HargaProdukJson harga : hargaProdukJson) {
					ProdukWilayahJualProvinsi wProvinsi = new ProdukWilayahJualProvinsi();
					wProvinsi.provinsi_id = harga.provinsiId;
					wProvinsi.produk_id = produk.id;
					wProvinsi.harga_utama = harga.harga;
					wProvinsi.harga_ongkir = new BigDecimal(0);
					wProvinsi.harga_tanggal = kursDate;
					wProvinsi.harga_kurs_id = produk.harga_kurs_id;
					wProvinsi.margin_harga = new Double(0);
					wProvinsi.active = true;
					wProvinsi.save();
				}
			}
		}
	}

	private static void addHistory(Produk productModel, ProductForm model) {
		if (model.isCreatedMode()) {
			new ProdukRiwayat()
					.addProduct(productModel)
					.setDataForm(model)
					.saveHistory();
			new ProdukRiwayat()
					.addForApproval(productModel)
					.setDataForm(model)
					.saveHistory();
		} else {
			new ProdukRiwayat()
					.editProduct(productModel)
					.setDataForm(model)
					.saveHistory();
		}
	}

	public static List<ProductLogJson> getProductsForElastic() {
		Logger.debug("get products");
		try {
			return Query.find("select * from produk limit 10", ProductLogJson.class).fetch();
		} catch (Exception e) {
			e.printStackTrace();
			return Collections.emptyList();
		}
	}

	public static List<HargaProdukDaerah> getHargaByDaerah(Long produk_id, Long daerah_id, String kelasHarga){
		List<HargaProdukDaerah> results = new ArrayList<>();

		Produk produk = Produk.findById(produk_id);

		List<ProdukHarga> list = ProdukHarga.findRiwayatByProdukId(produk.id);

		Map<String, List<ProdukHarga>> groupHarga = list.stream()
		.collect(
			Collectors.groupingBy(
				x -> x.getKursDateString()
			)
		);

		// long totalPriceAtribute = KomoditasAtributHarga.countByKomoditas(produk.komoditas_id);

		// int i = 1;

		for(Map.Entry<String,List<ProdukHarga>> record: groupHarga.entrySet()){
			HargaProdukDaerah hpd = new HargaProdukDaerah();
			hpd.tanggal = record.getKey();
			hpd.nama_kurs = record.getValue().get(0).nama_kurs;
			for(ProdukHarga ph: record.getValue()){
				hpd.harga.put(ph.komoditas_harga_atribut_id,getHargaProduk(ph,produk,kelasHarga,daerah_id));
			}
			Logger.info("Harga " + hpd.harga);
			results.add(hpd);
		}
		// Map<Long, BigDecimal> harga = new LinkedMap<>();
		// HargaProdukDaerah hpd = new HargaProdukDaerah();
		// for(ProdukHarga item : list){
		// 	if(i == 1){
		// 		hpd = new HargaProdukDaerah();
		// 		// hpd.tanggal = item.tanggal_dibuat;
		// 		hpd.nama_kurs = item.nama_kurs;
		// 	}

		// 	harga.put(item.komoditas_harga_atribut_id,getHargaProduk(item,produk,kelasHarga,daerah_id));
		// 	LogUtil.d("harga",CommonUtil.toJson(harga));

		// 	if(totalPriceAtribute == i){
		// 		hpd.harga = harga;
		// 		results.add(hpd);
		// 		i = 1;
		// 	}else{
		// 		i++;
		// 	}

		// }

		return results;

	}

	private static BigDecimal getHargaProduk(ProdukHarga ph, Produk produk, String kelasHarga, Long daerah_id){
		if(ph.apakah_ongkir){

			if(produk.ongkir == null){
				return new BigDecimal(0);
			}

			Type listOngkirType = new TypeToken<ArrayList<OngkirProdukJson>>(){}.getType();
			List<OngkirProdukJson> ongkirProdukJsonList = CommonUtil.fromJson(produk.ongkir,listOngkirType);

			for(OngkirProdukJson ongkirProdukJson : ongkirProdukJsonList){
				Long daerahOngkir = kelasHarga.equals(Komoditas.KABUPATEN) ? ongkirProdukJson.kabupatenId : ongkirProdukJson.provinsiId;

				if(daerahOngkir.equals(daerah_id)){
					return ongkirProdukJson.ongkir;
				}

			}
		}

		if(kelasHarga.equals("regency")){
			Type listType = new TypeToken<ArrayList<HargaKabupatenProdukJson>>(){}.getType();
			List<HargaKabupatenProdukJson> hargaProdukJsonList = CommonUtil.fromJson(ph.harga,listType);

			for(HargaKabupatenProdukJson hargaProdukJson : hargaProdukJsonList){

				if(hargaProdukJson.kabupatenId.equals(daerah_id)){
					return hargaProdukJson.harga;
				}

			}

		}

		Type listType = new TypeToken<ArrayList<HargaProvinsiProdukJson>>(){}.getType();
		List<HargaProvinsiProdukJson> hargaProdukJsonList = CommonUtil.fromJson(ph.harga,listType);

		for(HargaProvinsiProdukJson hargaProdukJson : hargaProdukJsonList){
			Logger.info("Provinsi === " + hargaProdukJson.provinsiId);
			if(hargaProdukJson.provinsiId.equals(daerah_id)){
				return hargaProdukJson.harga;
			}

		}

		return new BigDecimal(0);
	}



	public static List<String> getProdukgambar(Long produk_id){
		List<ProdukGambar> produkGambars = ProdukGambar.findByProduk(produk_id);
		List<String> results = new ArrayList<>();
		for(ProdukGambar pg : produkGambars){
			if(pg.dok_id_attachment != null && pg.dok_id_attachment != 0){
				BlobTable blob = BlobTable.findById(pg.dok_id_attachment);
				if(blob != null){
					DokumenInfo dokumenInfo = DokumenInfo.findByBlob(blob);
					results.add(dokumenInfo.download_url);
				}
			} else if(!CommonUtil.isEmpty(pg.file_url)){
				results.add(pg.file_url);
			} else if(!CommonUtil.isEmpty(pg.file_sub_location)){
				results.add(pg.getUrl());
			}
		}

		return results;
	}

	public static Produk getDetailProduk(Long produk_id, Long daerah_id){
		Produk produk = Produk.findById(produk_id);
		if(produk != null){
			Komoditas komoditas = Komoditas.findById(produk.komoditas_id);
			Penyedia penyedia = Penyedia.findById(produk.penyedia_id);
			Manufaktur manufaktur = Manufaktur.findById(produk.manufaktur_id);
			String kelas_harga = komoditas.kelas_harga;
			produk.nama_produk = StringEscapeUtils.unescapeHtml(produk.nama_produk);
			produk.kelas_harga = kelas_harga;
			produk.nama_penyedia = penyedia.nama_penyedia;
			produk.nama_manufaktur = manufaktur.nama_manufaktur;
			produk.perlu_negosiasi = komoditas.perlu_negosiasi_harga;
			//harus di replace dari ProdukHarga
			produk.harga_ongkir = new BigDecimal(0);
			// if(kelas_harga.equalsIgnoreCase("kabupaten") && daerah_id != -999){
			// 	ProdukWilayahJualKabupaten pwj = ProdukWilayahJualKabupaten.getHargaByKabupatenId(produk_id, daerah_id);
			// 	if(pwj != null){
			// 		produk.harga_utama = pwj.harga_utama;
			// 		produk.daerah_id = daerah_id;
			// 	}
			// } else if (kelas_harga.equalsIgnoreCase("provinsi") && daerah_id != -999) {
			// 	ProdukWilayahJualProvinsi pwj = ProdukWilayahJualProvinsi.getHargaByProvinsiId(produk_id, daerah_id);
			// 	if(pwj != null){
			// 		produk.harga_utama = pwj.harga_utama;
			// 		produk.daerah_id = daerah_id;
			// 	}
			// }
			ProdukGambar produkGambar = produk.getGambarUtama();
			if (produkGambar != null) {
				produk.url_gambar_utama = produkGambar.getUrl();
			}
			produk.produkAtributValues = ProdukAtributValue.findByProdukId(produk.id);
			Kurs kurs = Kurs.findById(produk.harga_kurs_id);
			produk.nama_kurs = kurs.nama_kurs;
			produk.kurs_id = kurs.id;

			if(!TextUtils.isEmpty(kelas_harga) && (kelas_harga.equalsIgnoreCase("provinsi") || kelas_harga.equalsIgnoreCase("kabupaten")) && daerah_id != -999){
				
				List<KomoditasAtributHarga> atributHargaList = KomoditasAtributHarga.findByKomoditas(produk.komoditas_id);
				List<ProdukHarga> produkHarga = ProdukHarga.findRiwayatByProdukId(produk_id);
				Map<String, List<ProdukHarga>> groupHarga = produkHarga.stream()
				.collect(
					Collectors.groupingBy(
						x -> x.getStandarDateString()
					)
				);
				NavigableMap<String, List<ProdukHarga>> gh = new TreeMap<>(groupHarga).descendingMap();

				KomoditasAtributHarga kah = atributHargaList.stream().parallel().filter(it -> it.apakah_harga_utama == true).findFirst().orElse(new KomoditasAtributHarga());
				ProdukHarga ph = gh.firstEntry().getValue().stream().parallel().filter(it -> it.komoditas_harga_atribut_id.equals(kah.id)).findFirst().orElse(new ProdukHarga());
				List<ProdukHargaJson> hargaProdukJson = new ArrayList<>();

				ProdukHargaJson hkj = new ProdukHargaJson();
				if(kelas_harga.equalsIgnoreCase("provinsi") ){
					hargaProdukJson = new Gson().fromJson(ph.harga, new TypeToken<List<ProdukHargaJson>>(){}.getType());
					hkj = hargaProdukJson.stream()
							.filter(it -> it.provinsiId ==daerah_id)
							.findFirst()
							.orElse(new ProdukHargaJson());

					produk.daerah_id = daerah_id;

				}else if(kelas_harga.equalsIgnoreCase("kabupaten") ){
					hargaProdukJson = new Gson().fromJson(ph.harga, new TypeToken<List<ProdukHargaJson>>(){}.getType());
					hkj = hargaProdukJson.stream()
							.filter(it -> it.kabupatenId ==daerah_id)
							.findFirst()
							.orElse(new ProdukHargaJson());

					produk.daerah_id = daerah_id;
				} else{
					hkj = new Gson().fromJson(ph.harga, ProdukHargaJson.class);
					produk.daerah_id = new Long(0);
				}
				produk.harga_utama = new BigDecimal(hkj.harga);
			}
			if(komoditas.perlu_ongkir && komoditas.kelas_harga.equals("kabupaten")){
				List<OngkosKirim> oks = CommonUtil.fromJson(produk.ongkir, new TypeToken<List<OngkosKirim>>(){}.getType());
				OngkosKirim ok = oks.stream().filter(x -> x.kabupatenId.equals(daerah_id)).findFirst().orElse(new OngkosKirim());
				produk.harga_ongkir = ok.ongkir;
			}

		}

		return produk;
	}

	public static Integer getTotalProductByManufacture(Long manufactureId) {
		final String query = "SELECT COUNT(*) as total FROM produk WHERE manufaktur_id =? AND active =?";
		return Query.find(query, Integer.class, manufactureId, 1).first();
	}

	public static Integer getTotalProductByKurs(Long id) {
		final String query = "SELECT COUNT(*) as total FROM produk WHERE harga_kurs_id =? AND active =?";
		return Query.find(query, Integer.class, id, 1).first();
	}

	public static void setRejectedAllUnapprovedProductByPenawaran(long penawaran_id, AktifUser aktifUser){
		Produk.setRejectedAllUnapprovedProductByPenawaran(penawaran_id);
		// List<Produk> produkList = Produk.findByPenawaranAndMenungguPersetujuan(penawaran_id);
		// if(!produkList.isEmpty()){

		// 	for(Produk produk : produkList){
		// 		produk.setuju_tolak = Produk.TOLAK;
		// 		produk.setuju_tolak_oleh = aktifUser.user_id;
		// 		produk.setuju_tolak_tanggal = new Date();
		// 		produk.minta_disetujui = false;
		// 		produk.status = Produk.STATUS_TOLAK;
				
		// 		produk.save();
		// 	}

		// }
	}

	public static ProductSearchResult searchProduct(SearchQuery searchQuery) {
		ProductSearchResult model = new ProductSearchResult(searchQuery);
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT p.id, p.nama_produk, p.no_produk, py.nama_penyedia, " +
				"p.no_produk_penyedia, p.apakah_dapat_dibeli, p.apakah_ditayangkan, " +
				"k.kelas_harga, p.edit_able, pn.status as status_penawaran "+
				"FROM produk p ")
				.append("JOIN komoditas k ON k.id = p.komoditas_id ")
				.append("JOIN penawaran pn ON pn.id = p.penawaran_id ")
				.append("LEFT JOIN penyedia py ON py.id = p.penyedia_id ");

		sb.append("WHERE p.created_by = '").append(AktifUser.getActiveUserId()).append("'");

		if (!TextUtils.isEmpty(searchQuery.keyword)) {
			sb.append(" AND p.nama_produk LIKE '%").append(searchQuery.keyword).append("%'");
		}

		if (searchQuery.commodity > 0) {
			sb.append(" AND p.komoditas_id = '").append(searchQuery.commodity).append("'");
		}

		if (searchQuery.isDisplayed > -1) {
			sb.append(" AND p.apakah_ditampilkan = '").append(searchQuery.isDisplayed).append("'");
		}

		if (!TextUtils.isEmpty(searchQuery.type)) {
			sb.append(" AND p.jenis_produk = '").append(searchQuery.type).append("'");
		}

		if (searchQuery.buy > -1) {
			sb.append(" AND p.apakah_dapat_dibeli = '").append(searchQuery.buy).append("'");
		}

		model.generateTotal(sb.toString());
		if (!model.isEmpty()) {
			sb.append(" ORDER BY p.modified_date DESC ");
			sb.append(" LIMIT ").append(model.getMax()).append(" OFFSET ").append(model.getFrom());
			model.products = Query.find(sb.toString(), Produk.class).fetch();
			if (!model.products.isEmpty()) {
				for (Produk produk : model.products) {
					produk.withPictures();
				}
			}
		}
		return model;
	}

	public static boolean openByProduct(Long productId, String date) {
		LogUtil.d(TAG, "open edit: " + productId + " date: " + date);
		if (!TextUtils.isEmpty(date)) {
			Date expired = DateTimeUtils.parseDdMmYyyyToDate(date);
			LogUtil.d(TAG, expired);
			if (expired == null) {
				return false;
			}
			final String query = "UPDATE produk " +
					"SET edit_able =?, modified_date =?, modified_by =? WHERE id = ?";
			deleteCache();
			return Query.update(query, expired, DateTimeUtils.getStandardDateTime(), AktifUser.getActiveUserId(), productId) == 1;
		}
		return false;
	}

	public static boolean openByProvider(Long providerId, String date) {
		if (TextUtils.isEmpty(date)) {
			return false;
		}
		Date expired = DateTimeUtils.parseDdMmYyyy(date);
		if (expired == null) {
			return false;
		}
		Penyedia penyedia = Penyedia.findById(providerId);
		if (penyedia == null) {
			return false;
		}
		if (penyedia.user_id == null) {
			return false;
		}
		final long totalByProviderId = Produk.count("penyedia_id =? OR created_by =?", penyedia.id, penyedia.user_id);
		LogUtil.d(TAG, "total product by penyedia: " + totalByProviderId);
		if (totalByProviderId == 0) {
			return false;
		}
		final String query = "UPDATE produk " +
				"SET edit_able =?, modified_date =?, modified_by =? WHERE penyedia_id = ? OR created_by =?";
		final int result = Query.update(query, expired, DateTimeUtils.getStandardDateTime(), AktifUser.getActiveUserId(), penyedia.id, penyedia.user_id);
		LogUtil.d(TAG, "update result: " + result);
		final boolean status = result >= 1;
		if (status) {
			deleteCache();
		}
		return status;
	}

	public static void deleteCache() {
		LogUtil.d(TAG, "Delete product cached");
		final String key = CacheMerdeka.class.getName() +  "$" + Produk.class.getName();
		Map<Object, Set<String>> cacheTracker = (Map<Object, Set<String>>) Cache.get(key);
		LogUtil.d(TAG, cacheTracker);
		if (cacheTracker != null) {
			Collection<Set<String>> lists = cacheTracker.values();
			LogUtil.d(TAG, lists);
			for (Set<String>  listOfCacheKey : lists) {
				for (String cacheKey: listOfCacheKey) {
					Cache.safeDelete(cacheKey);
				}
			}
			Cache.safeDelete(key);
		}
	}

}
//catatan:
/*
- minta disetujui :
kasusnya ketika produk disetujui, where minta_disetujui=0
ketika produk akan dinaikan ke elastik tidak ditemukan ketika minta_disetujui=0 atau false
yang menset dia adalah
isLenang() di produk yang mengindikasikan apakah dia produk datafeed atau bukan (dalam negoisasi atau bukan)
------
field apakah_tayang defaultnya false, baru di true kan ketika kontraknya apakah berlaku true,
ketika di form create kontrak

*/