package services.paket;

import utils.KatalogUtils;


/**
 * @author HanusaCloud on 10/29/2017
 */
public class BaseService {

    protected String generateExcludeIds(String excluded) {
        return KatalogUtils.generateExclude(excluded);
    }

}
