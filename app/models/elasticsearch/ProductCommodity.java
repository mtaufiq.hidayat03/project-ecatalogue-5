package models.elasticsearch;

import models.elasticsearch.product.ProductElastic;
import org.apache.http.util.TextUtils;
import play.i18n.Lang;
import utils.UrlUtil;

/**
 * @author HanusaCloud on 10/10/2017
 */
public class ProductCommodity {

    public long id;
    public String name;
    public String query;
    public boolean apakahIklan;
    public long total = 0;

    public ProductCommodity(ProductElastic model) {
        this.id = model.commodityId;
        this.name = model.commodityName;
//        this.apakahIklan = model.apakahIklan;
    }

    public String getSlug() {
        if (!TextUtils.isEmpty(name)) {
            return name.replace(" ", "-").toLowerCase();
        }
        return "";
    }

    public String getTitleWithTotal() {
        return name + " (" + total + ")";
    }

    public String getUrl() {
        return generateUrl(null);
    }

    public String generateUrl(String keyword) {
        String lang = !Lang.get().isEmpty() ? Lang.get() : "id";
        UrlUtil.Builder builder = UrlUtil.builder()
                .setUrl("katalog.ProdukCtr.listProduk")
                .setParam("language", lang)
                .setParam("komoditas_slug", getSlug())
                .setParam("id", id);
        if (!TextUtils.isEmpty(keyword)) {
            builder.setParam("q", keyword);
        }
        String result = builder.build();
        if (!TextUtils.isEmpty(query)) {
            result += query;
        }
        return result;
    }

}
