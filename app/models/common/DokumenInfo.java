package models.common;

import ext.FormatUtils;
import models.jcommon.blob.BlobTable;
import models.penyedia.DokumenUkm;
import models.prakatalog.*;
import play.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * model untuk informasi file yang diupload oleh user
 * @author lkpp
 *
 */
public class DokumenInfo implements Serializable {

	public Long id;

	public Long blb_id_content;

	public String name;

	public String name_alias;
	
	public String size;
	
	public Integer versi;
	
	public String upload_date;
	
	public String download_url;

	public String file_label;

	public String file_description;

	public String jenis_evaluasi;

	public DokumenInfo() {}

	public DokumenInfo(BlobTable model) {
		setFromBlob(model);
	}

	public DokumenInfo setFromBlob(BlobTable model) {
		this.id = model.id;
		this.blb_id_content = model.blb_id_content;
		this.name = model.blb_nama_file;
		this.versi = model.blb_versi;
		this.download_url = model.getDownloadUrl(null);
		this.size = FormatUtils.formatFileSize(model.getFile().length());
		this.upload_date = FormatUtils.formatDateTimeInd(model.blb_date_time);
		return this;
	}

	public static DokumenInfo findforDokumenUsulan(BlobTable blob) {
		DokumenInfo info = new DokumenInfo(blob);
		DokumenUsulan dok = DokumenUsulan.findByDokId(blob.id);
		if(dok != null){
			info.file_label = dok.dok_label;
		}

		return info;
	}

	public static DokumenInfo findforDokumenUkm(BlobTable blob) {
		DokumenInfo info = new DokumenInfo(blob);
		DokumenUkm dok = DokumenUkm.findByDokIdAttach(blob.id);
		if(dok != null){
			info.file_label = dok.dok_label;
		}

		return info;
	}

	public static DokumenInfo findforDokumenUsulanPokja(BlobTable blob) {
		DokumenInfo info = new DokumenInfo(blob);
		DokumenUsulanPokja dok = DokumenUsulanPokja.findByDokId(blob.id);
		if(dok != null){
			info.file_label = dok.dok_label;
		}

		return info;
	}

	public static DokumenInfo findByBlob(BlobTable blob) {
		return new DokumenInfo(blob);
	}

	public static DokumenInfo findDokumenPenawaran(BlobTable blob, String dok_keterangan, String judul, String file_label, String jenis_evaluasi) {
		DokumenInfo info = new DokumenInfo();
		info.id = blob.blb_id_content;
		info.name = judul;
		info.size = FormatUtils.formatBytes(blob.getFile().length());
		info.versi = blob.blb_versi;
		info.download_url = blob.getDownloadUrl(null);
		info.upload_date = FormatUtils.formatDateTimeInd(blob.blb_date_time);
		info.file_label = file_label;
		info.file_description = dok_keterangan;
		info.jenis_evaluasi = jenis_evaluasi;

		return info;
	}

	public static List<DokumenInfo> listDokumenPenawaran(List<DokumenPenawaran> dokumenPenawaranList){

		List<DokumenInfo> dokumenInfoList = new ArrayList<DokumenInfo>();

		for(DokumenPenawaran dok : dokumenPenawaranList){

			//BlobTable blobTable = BlobTable.find("blb_id_content = ?",).first();
			if(null != dok.dok_id_attachment){
				BlobTable blobTable = BlobTable.findByBlobId(dok.dok_id_attachment);
				if(blobTable != null){
					DokumenInfo dokumenInfo = findDokumenPenawaran(blobTable, dok.dok_keterangan, dok.dok_judul, dok.dok_file_name,dok.jenis_evaluasi);
					Logger.debug("downloadUrl: "+dokumenInfo.download_url);
					dokumenInfoList.add(dokumenInfo);
				}
			}

		}

		return dokumenInfoList;
	}
	public static List<DokumenInfo> listDokumenPenawaran(long penawaran_id){


		List<DokumenPenawaran> dokumenPenawaranList = DokumenPenawaran.listDokumenPenawaran(penawaran_id);
		List<DokumenInfo> dokumenInfoList = new ArrayList<DokumenInfo>();

		for(DokumenPenawaran dok : dokumenPenawaranList){

			//BlobTable blobTable = BlobTable.find("blb_id_content = ?",).first();
			BlobTable blobTable = BlobTable.findByBlobId(dok.dok_id_attachment);
			if(blobTable != null){
				DokumenInfo dokumenInfo = findDokumenPenawaran(blobTable, dok.dok_keterangan, dok.dok_judul, dok.dok_file_name,dok.jenis_evaluasi);
				Logger.debug("downloadUrl: "+dokumenInfo.download_url);
				dokumenInfoList.add(dokumenInfo);
			}
		}

		return dokumenInfoList;
	}

	public static List<DokumenInfo> listDokumenUsulan(long usulan_id){
		List<DokumenUsulan> dokumenUsulanList = DokumenUsulan.find("usulan_id = ?",usulan_id).fetch();
		List<DokumenInfo> dokumenInfoList = new ArrayList<DokumenInfo>();

		for(DokumenUsulan dok : dokumenUsulanList){

//			BlobTable blobTable = BlobTable.find("id = ?",dok.dok_id_attachment).first();
//			DokumenInfo dokumenInfo = findDokumenUsulan(blobTable, dok.dok_judul, dok.dok_label);
			//perbaikan logik lama ke logik baru di blobtable
			DokumenInfo dokumenInfo = dok.getAsdocInfo();
			dokumenInfoList.add(dokumenInfo);
		}

		return dokumenInfoList;
	}

	public static List<DokumenInfo> listDokumenUkm(long penyediaId){
		List<DokumenUkm> dokumenUkmList = DokumenUkm.find("penyedia_id = ?",penyediaId).fetch();
		List<DokumenInfo> dokumenInfoList = new ArrayList<DokumenInfo>();

		for(DokumenUkm dok : dokumenUkmList){
			DokumenInfo dokumenInfo = dok.getAsdocInfo();
			dokumenInfoList.add(dokumenInfo);
		}

		return dokumenInfoList;
	}

	public static List<DokumenInfo> listDokumenPengadaan(long usulan_id){
		List<UsulanDokumenPengadaan> dokumenPengadaanList = UsulanDokumenPengadaan.findByUsulan(usulan_id);
		List<DokumenInfo> dokumenInfoList = new ArrayList<DokumenInfo>();

		for(UsulanDokumenPengadaan dok : dokumenPengadaanList){

			//BlobTable blobTable = BlobTable.find("blb_id_content = ?",dok.dok_id_attachment).first();
			BlobTable blobTable = BlobTable.findByBlobId(dok.dok_id_attachment);
			DokumenInfo dokumenInfo = findDokumenPengadaan(blobTable, dok.dok_judul);

			dokumenInfoList.add(dokumenInfo);
		}

		return dokumenInfoList;
	}

	public static DokumenInfo findDokumenKontrak(BlobTable blob){
		DokumenInfo info = new DokumenInfo();
		info.id = blob.blb_id_content;
		info.download_url = blob.getDownloadUrl(null);

		return info;
	}

	public static DokumenInfo findDokumenUsulan(BlobTable blob, String judul, String label){
		DokumenInfo info = new DokumenInfo();
		info.id = blob.blb_id_content;
		info.file_label = label;
		info.name = judul;
		info.download_url = blob.getDownloadUrl(null);

		return info;
	}


	public static List<DokumenInfo> getUploadInfoList(List<DokumenUsulan> dokumenUsulanList){
		List<DokumenInfo> dokumenInfoList = new ArrayList<DokumenInfo>();

		for(DokumenUsulan dokumenUsulan: dokumenUsulanList){

			//BlobTable blobTable = BlobTable.find("blb_id_content = ?",dokumenUsulan.dok_id_attachment).first();
			BlobTable blobTable = BlobTable.findByBlobId(dokumenUsulan.dok_id_attachment);
			if(blobTable != null){
				DokumenInfo dokumenInfo = DokumenInfo.findforDokumenUsulan(blobTable);
				dokumenInfoList.add(dokumenInfo);
			}
		}

		return dokumenInfoList;
	}

	public static List<DokumenInfo> getUploadInfoUkmList(List<DokumenUkm> dokumenUkmList){
		List<DokumenInfo> dokumenInfoList = new ArrayList<DokumenInfo>();

		for(DokumenUkm dokumenUkm: dokumenUkmList){
			BlobTable blobTable = BlobTable.findByBlobId(dokumenUkm.dok_id_attachment);
			if(blobTable != null){
				DokumenInfo dokumenInfo = DokumenInfo.findforDokumenUkm(blobTable);
				dokumenInfoList.add(dokumenInfo);
			}
		}

		return dokumenInfoList;
	}

	public static List<DokumenInfo> getUploadUsulanPokjaList(List<DokumenUsulanPokja> dokumenPokjaList){
		List<DokumenInfo> dokumenInfoList = new ArrayList<DokumenInfo>();

		for(DokumenUsulanPokja dokumenUsulan: dokumenPokjaList){

			BlobTable blobTable = BlobTable.findByBlobId(dokumenUsulan.dok_id_attachment);
			if(blobTable != null){
				DokumenInfo dokumenInfo = DokumenInfo.findforDokumenUsulan(blobTable);
				dokumenInfoList.add(dokumenInfo);
			}
		}

		return dokumenInfoList;
	}

	public static DokumenInfo findDokumenPengadaan(BlobTable blob, String judul){
		DokumenInfo info = new DokumenInfo();
		info.id = blob.blb_id_content;
		info.download_url = blob.getDownloadUrl(null);
		info.name = judul;

		return info;
	}

	public static List<DokumenInfo> listDokumenPerpanjangan(List<DokumenPerpanjangan> dokumenPerpanjanganList){

		List<DokumenInfo> dokumenInfoList = new ArrayList<DokumenInfo>();
		for(DokumenPerpanjangan dok : dokumenPerpanjanganList){

			//BlobTable blobTable = BlobTable.find("blb_id_content = ?",).first();
			if(null != dok.dok_id_attachment){
				BlobTable blobTable = BlobTable.findByBlobId(dok.dok_id_attachment);
				if(blobTable != null){
					UsulanDokumenPerpanjangan dok_label = UsulanDokumenPerpanjangan.find("id=?",dok.usulan_dok_perp_id).first();
					DokumenInfo dokumenInfo = findDokumenPerpanjangan(blobTable, dok.dok_judul,dok.id, dok_label.dok_label, dok.keterangan);
					Logger.debug("downloadUrl: "+dokumenInfo.download_url);
					dokumenInfoList.add(dokumenInfo);
				}
			}

		}

		return dokumenInfoList;
	}

	public static DokumenInfo findDokumenPerpanjangan(BlobTable blob,String judul, Long id_dok, String dok_label, String keterangan) {
		DokumenInfo info = new DokumenInfo();
		info.id = blob.id;
		info.name = judul;
		info.size = FormatUtils.formatBytes(blob.getFile().length());
		info.versi = blob.blb_versi;
		info.download_url = blob.getDownloadUrl(null);
		info.upload_date = FormatUtils.formatDateTimeInd(blob.blb_date_time);
		info.file_label = dok_label;
		info.file_description=keterangan;
		return info;
	}

	public void setDownloadUrlFromBlob(){
		getDownloadUrlFromBlob();
	}
	public String getDownloadUrlFromBlob(){
		BlobTable blob = BlobTable.find("id = ?", blb_id_content).first();
		if (blob != null) {
			download_url = blob.getDownloadUrl(null);
		}
		return this.download_url;
	}
}
