package repositories.paket;

import models.purchasing.kontrak.PaketKontrak;
import play.Logger;
import play.db.jdbc.Query;

import java.util.List;

/**
 * @author HanusaCloud on 11/7/2017
 */
public class PackageContractRepository {

    public static List<PaketKontrak> getByPackageIdFront(Long id) {
        final String query = "SELECT id, paket_id, no_kontrak, nilai_kontrak, tanggal_kontrak, deskripsi, file_name, original_file_name, file_url, blb_id" +
                " FROM paket_kontrak" +
                " WHERE paket_id =?" +
                " AND active > 0 ";
        return Query.find(query,PaketKontrak.class, id).fetch();
    }

    public static Double getContractValue(Long id) {
        final String query = "SELECT SUM(harga_total) as total" +
                " FROM `paket_produk`" +
                " WHERE paket_id =?" +
                " AND active  > 0;";
        return Query.find(query, Double.class, id).first();
    }

    public static Integer getTotalContract(Long id) {
        Logger.debug("get total contract uploaded");
        final String query = "SELECT COUNT(*) as total" +
                " FROM `paket_kontrak`" +
                " WHERE paket_id =?" +
                " AND active  > 0;";
        return Query.find(query, Integer.class, id).first();
    }

}
