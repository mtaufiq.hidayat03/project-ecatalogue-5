package repositories.prakatalog;

import models.common.AktifUser;
import models.prakatalog.Penawaran;
import models.prakatalog.search.PenawaranSearch;
import models.prakatalog.search.PenawaranSearchResult;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.util.TextUtils;
import play.db.jdbc.Query;
import utils.DateTimeUtils;

import java.util.Date;

public class PenawaranRepository {

    public static PenawaranSearchResult searchPackage(PenawaranSearch search) {
        PenawaranSearchResult model = new PenawaranSearchResult(search);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT DISTINCT pn.id" +
                ", pn.penyedia_id" +
                ", pn.user_id" +
                ", p.nama_lengkap" +
                ", pn.status" +
                ", pn.created_date" +
                ", u.nama_usulan" +
                ", pn.komoditas_id" +
                ", k.nama_komoditas" +
                ", komoditas_kategori_id" +
//                ", up.pokja_id" +
                ", u.doc_review_id" +
                ", pn.usulan_id " +
                ", pn.batas_akhir_revisi ")
                .append("FROM penawaran pn ")
                .append("JOIN user p on p.id = pn.user_id ")
                .append("JOIN usulan u on pn.usulan_id = u.id ")
                .append("JOIN usulan_pokja up on up.usulan_id = u.id ")
                .append("JOIN komoditas k ON pn.komoditas_id = k.id ");

        sb.append(" WHERE pn.active = 1 ");
        if (!TextUtils.isEmpty(search.keyword)) {
            sb.append(" AND (u.nama_usulan LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%'");
            sb.append(" OR k.nama_komoditas LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%'");
            sb.append(" OR p.nama_lengkap LIKE '%").append(StringEscapeUtils.escapeSql(search.keyword)).append("%')");
        }

        if (search.uid > 0) {
            if(AktifUser.getAktifUser().isPokja()){
                sb.append(" AND up.pokja_id = ").append(search.uid);
            }else if(AktifUser.getAktifUser().isProvider()){
                sb.append(" AND pn.user_id = ").append(search.uid);
            }else if(AktifUser.getAktifUser().isAdminLokalSektoral()){
                sb.append(" AND k.kldi_id = '").append(AktifUser.getAktifUser().kldi_id).append("' ");
            }
        }

        if (search.komoditas > 0) {
            sb.append(" AND pn.komoditas_id = ").append(search.komoditas);
        }

        if(!TextUtils.isEmpty(search.tglpengajuan)){
            String result = "";
            try{
                Date tanggal = DateTimeUtils.formatStringToDate(search.tglpengajuan);
                result = DateTimeUtils.formatDateToString(tanggal,"YYYY-MM-dd");
                result = StringEscapeUtils.escapeSql(result);
            }catch (Exception e){
                e.printStackTrace();
            }
            sb.append(" AND pn.created_date LIKE '%").append(result).append("%'");
        }

        if(!TextUtils.isEmpty(search.status)){
            sb.append(" AND pn.status = '").append(search.status).append("'");
        }

        Long total = PenawaranRepository.generateTotal(sb.toString());
        model.setTotal(total);

        if(!TextUtils.isEmpty(search.urutkan)){
            if(search.urutkan.equalsIgnoreCase("1")){
                sb.append(" ORDER BY u.nama_usulan ASC ");
            }else if(search.urutkan.equalsIgnoreCase("2")){
                sb.append(" ORDER BY u.nama_usulan DESC ");
            }else if(search.urutkan.equalsIgnoreCase("3")){
                sb.append(" ORDER BY k.nama_komoditas ASC ");
            }else if(search.urutkan.equalsIgnoreCase("4")){
                sb.append(" ORDER BY k.nama_komoditas DESC ");
            }else if(search.urutkan.equalsIgnoreCase("5")){
                sb.append(" ORDER BY p.nama_lengkap ASC ");
            }else if(search.urutkan.equalsIgnoreCase("6")){
                sb.append(" ORDER BY p.nama_lengkap DESC ");
            }else if(search.urutkan.equalsIgnoreCase("7")){
                sb.append(" ORDER BY pn.created_date ASC ");
            }else if(search.urutkan.equalsIgnoreCase("8")){
                sb.append(" ORDER BY pn.created_date DESC ");
            }
        }

        if (!model.isEmpty()) {
            sb.append(" LIMIT ").append(search.per_page).append(" OFFSET ").append(model.getFrom());
            model.items = Query.find(sb.toString(), Penawaran.class).fetch();
        }
//        Logger.info("##### Penawaran ", sb.toString());
//        System.out.println("##### Penawaran "+ sb.toString());
        return model;
    }

    private static Long generateTotal(String query) {
        Long total = Query.find("SELECT COUNT(id) FROM (" + query + ") p", Long.class).first();
        return total;
    }
}
