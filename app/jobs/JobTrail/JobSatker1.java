package jobs.JobTrail;

import jobs.instansisatker.ExtSatkerData;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;

import java.util.UUID;

@NoTransaction
public class JobSatker1 extends Job {
    String uniqueID = UUID.randomUUID().toString();
    ExtSatkerData ext;
    public JobSatker1(ExtSatkerData _ext){
        ext = _ext;
    }

    @Override
    public void before() {
        Logger.info("job id:" + uniqueID);
        super.before();
    }

    public void doJob() {
        ext.jalankan();
    }
}
