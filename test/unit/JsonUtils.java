package unit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import functional.BaseCase;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author HanusaCloud on 5/28/2020 4:11 PM
 */
public class JsonUtils {

    private static Gson gson;

    public static Gson getGson() {
        if (gson == null) {
            GsonBuilder builder = new GsonBuilder();
            TypeAdapter<Boolean> integerTypeAdapter = getBooleanTypeAdapter();
            builder.registerTypeAdapter(Boolean.class, integerTypeAdapter);
            builder.registerTypeAdapter(boolean.class, integerTypeAdapter);
            builder.setDateFormat("yyyy-MM-dd hh:mm:ss");
            gson = builder.create();
        }

        return gson;
    }

    public static String get(String filename) throws IOException {
        try (BufferedReader br = new BufferedReader(
                new FileReader(
                        new File( BaseCase.BASE_RESOURCE_PATH + filename)
                )
        )) {
            String line;
            while ((line = br.readLine()) != null) {
                if (StringUtils.isEmpty(line)) {
                    continue;
                }
                return line;
            }
        }
        return null;
    }

    private static TypeAdapter<Boolean> getBooleanTypeAdapter() {
        return new TypeAdapter<Boolean>() {

            @Override
            public void write(JsonWriter out, Boolean value) throws IOException {
                if (value == null) {
                    out.nullValue();
                    return;
                }
                out.value(value);
            }

            @Override
            public Boolean read(JsonReader in) throws IOException {
                if (in.peek() == JsonToken.NULL) {
                    in.nextNull();
                    return null;
                }
                if (in.peek() == JsonToken.BOOLEAN) {
                    return in.nextBoolean();
                }
                try {
                    Integer value = in.nextInt();
                    return value.equals(1);
                } catch (NumberFormatException nfe) {
                    return false;
                }
            }
        };
    }

}
