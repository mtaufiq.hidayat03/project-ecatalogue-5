package services.prakatalog;

import models.common.AktifUser;
import models.common.DokumenHasilEvaluasi;
import models.prakatalog.Penawaran;
import models.prakatalog.Usulan;
//import models.prakatalog.UsulanPenyedia;
import models.prakatalog.form.FormBukaPenawaran;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import play.Logger;
import utils.DateTimeUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PenawaranService {

	public static DokumenHasilEvaluasi populateDataForDocument(DokumenHasilEvaluasi dok, Penawaran penawaran){
		String[] splitDateTime = dok.tanggalForm.split(" ");
		String dateString = splitDateTime[0];
		dok.jam = splitDateTime[1];

		try{
			Date date = DateTimeUtils.formatStringToDate(dateString);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);

			dok.bulan = DateTimeUtils.getReadableMonth(cal.get(Calendar.MONTH));
			dok.tanggal = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
			dok.tahun = String.valueOf(cal.get(Calendar.YEAR));
			dok.hari = DateTimeUtils.getReadableDay(cal.get(Calendar.DAY_OF_WEEK) - 1); //jika menggunakan day_of_week maka harus dikurangi 1

		}catch (Exception e){
			e.printStackTrace();
		}

		dok.listKualifikasi = generateQualificationListForEvaluationDoc(dok);
		dok.listEvaluasiAdministrasi = generateEvaluationResultForAdministrationEvaluationDoc(dok);

		if(dok.namaDokAdmTambahan != null) {
			dok.listEvaluasiAdministrasiTambahan = generateEvaluationResultForSecondAdministrationEvaluationDoc(dok);
		}

		if(dok.namaHarga != null) {
			dok.listEvaluasiHarga = generateEvaluationResultForPriceEvaluationDoc(dok);
		}

		if(dok.namaTeknis != null){
			dok.listEvaluasiTeknis = generateEvaluationResultForTechnicalEvaluationDoc(dok);
		}

		dok.timPengembangKatalog = generateCatalogueTeamListForEvaluationDoc(dok);
		dok.ttdPenyedia = generateProviderTeamListForEvaluationDoc(dok);

		Logger.debug(penawaran.status);

		if(penawaran.status.equals(Penawaran.STATUS_LOLOS_KUALIFIKASI)){
			dok.kesimpulan = "Telah Lulus Evaluasi Kualifikasi dan Administrasi";
		}else if(penawaran.status.equals(Penawaran.STATUS_GAGAL_KUALIFIKASI)){
			dok.kesimpulan = "Tidak Lulus Evaluasi Kualifikasi dan Administrasi";
		}

		return dok;
	}

	public static String generateQualificationListForEvaluationDoc(DokumenHasilEvaluasi dok){

		String listKualifikasi = "<table align=\"center\" border=\"1\" cellspacing=\"0\" style=\"width:446.95pt\">\n" +
				"\t<tr>\n" +
				"\t\t<td style=\"height:3.5pt; width:27.2pt\">\n" +
				"\t\t\t<p style=\"text-align:center\"><strong>No</strong></p>\n" +
				"\t\t\t</td>\n" +
				"\t\t\t<td style=\"height:3.5pt; width:137.0pt\">\n" +
				"\t\t\t<p style=\"text-align:center\"><strong>Uraian kualifikasi sebagai berikut:</strong></p>\n" +
				"\t\t\t</td>\n" +
				"\t\t\t<td style=\"height:3.5pt; width:74.85pt\">\n" +
				"\t\t\t<p style=\"text-align:center\"><strong>Hasil Evaluasi</strong></p>\n" +
				"\t\t\t</td>\n" +
				"\t\t\t<td style=\"height:3.5pt; width:207.9pt\">\n" +
				"\t\t\t<p style=\"text-align:center\"><strong>Hasil Pembuktian</strong><strong> Kualifikasi</strong></p>\n" +
				"\t\t</td>\n" +
				"\t</tr>\n";
		int no = 0;
		for(int i = 0; i < dok.namaKualifikasi.length; i++){
			String nk = dok.namaKualifikasi[i];
			listKualifikasi += "\t<tr>\n" +
					"\t\t<td style=\"height:3.5pt; width:27.2pt\"><p style=\"text-align:center\">"+(i+1)+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:137.0pt\"><p style=\"text-align:left\">"+nk+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:74.85pt\"><p style=\"text-align:center\">"+dok.hasilPembuktianKualifikasi[i]+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:207.9pt\"><p style=\"text-align:center\">"+dok.hasilKualifikasi[i]+"</p></td>\n" +
					"\t</tr>\n";
			no = i;
		}

		if(dok.namaKualifikasiTambahan != null) {
			for (int j = 0; j < dok.namaKualifikasiTambahan.length; j++) {
				no++;
				String nk = dok.namaKualifikasiTambahan[j];
				listKualifikasi += "\t<tr>\n" +
						"\t\t<td style=\"height:3.5pt; width:27.2pt\"><p style=\"text-align:center\">" + (no + 1) + "</p></td>\n" +
						"\t\t\t<td style=\"height:3.5pt; width:137.0pt\"><p style=\"text-align:left\">" + nk + "</p></td>\n" +
						"\t\t\t<td style=\"height:3.5pt; width:74.85pt\"><p style=\"text-align:center\">"+dok.hasilPembuktianKualifikasiTambahan[j]+"</p></td>\n" +
						"\t\t\t<td style=\"height:3.5pt; width:207.9pt\"><p style=\"text-align:center\">" + dok.hasilKualifikasiTambahan[j] + "</p></td>\n" +
						"\t</tr>\n";
			}
		}
			listKualifikasi += "</table>";

		return listKualifikasi;

	}

	public static String generateEvaluationResultForAdministrationEvaluationDoc(DokumenHasilEvaluasi dok){
		String listEvaluasiAdministrasi = "<table align=\"center\" border=\"1\" cellspacing=\"0\" style=\"width:445.5pt\">\n" +
				"\t<tr>\n" +
				"\t\t<td style=\"height:25pt; width:26.8pt\"><p style=\"text-align:center\"><strong>No</strong></p></td>\n" +
				"\t\t<td style=\"height:25pt; width:175.7pt\"><p style=\"text-align:center\"><strong>Uraian Dokumen Administrasi sebagai berikut:</strong></p></td>\n" +
				"\t\t<td style=\"height:25pt; width:1.5in\"><p style=\"text-align:center\"><strong>Hasil Evaluasi</strong></p></td>\n" +
				"\t\t<td style=\"height:25pt; width:135.0pt\"><p style=\"text-align:center\"><strong>Keterangan</strong></p>\n" +
				"\t\t\t</td></tr>\n";

		for (int i = 0; i < dok.namaAdministrasi.length; i++){
			String na = dok.namaAdministrasi[i];
			listEvaluasiAdministrasi += "\t<tr>\n" +
					"\t\t<td style=\"height:3.5pt; width:27.2pt\"><p style=\"text-align:center\">"+(i+1)+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:137.0pt\"><p style=\"text-align:left\">"+na+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:74.85pt\"><p style=\"text-align:center\">"+dok.hasilPembuktianAdministrasi[i]+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:207.9pt\"><p style=\"text-align:center\">"+dok.hasilAdministrasi[i]+"</p></td>\n" +
					"\t</tr>\n";
		}

		listEvaluasiAdministrasi += "</table>";

		return listEvaluasiAdministrasi;
	}

	public static String generateEvaluationResultForSecondAdministrationEvaluationDoc(DokumenHasilEvaluasi dok){
		String listEvaluasiAdministrasiTambahan = "<table align=\"center\" border=\"1\" cellspacing=\"0\" style=\"width:445.5pt\">\n" +
				"\t<tr>\n" +
				"\t\t<td style=\"height:23pt; width:26.8pt\"><p style=\"text-align:center\"><strong>No</strong></p></td>\n" +
				"\t\t<td style=\"height:23pt; width:175.7pt\"><p style=\"text-align:center\"><strong>Uraian sebagai berikut:</strong></p></td>\n" +
				"\t\t<td style=\"height:23pt; width:1.5in\"><p style=\"text-align:center\"><strong>Hasil Evaluasi</strong></p></td>\n" +
				"\t\t<td style=\"height:23pt; vertical-align:top; width:135.0pt\"><p style=\"text-align:center\"><strong>Keterangan</strong></p>\n" +
				"\t\t\t</td></tr>\n";

		for (int i = 0; i < dok.namaDokAdmTambahan.length; i++){
			String na = dok.namaDokAdmTambahan[i];
			listEvaluasiAdministrasiTambahan += "\t<tr>\n" +
					"\t\t<td style=\"height:3.5pt; width:27.2pt\"><p style=\"text-align:center\">"+(i+1)+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:137.0pt\"><p style=\"text-align:left\">"+na+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:74.85pt\"><p style=\"text-align:center\">"+dok.hasilPembuktianAdmTambahan[i]+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:207.9pt\"><p style=\"text-align:center\">"+dok.hasilDokAdmTambahan[i]+"</p></td>\n" +
					"\t</tr>\n";
		}

		listEvaluasiAdministrasiTambahan += "</table>";

		return listEvaluasiAdministrasiTambahan;
	}

	public static String generateEvaluationResultForPriceEvaluationDoc(DokumenHasilEvaluasi dok){
		String listEvaluasiHarga = "<table align=\"center\" border=\"1\" cellspacing=\"0\" style=\"width:445.5pt\">\n" +
				"\t<tr>\n" +
				"\t\t<td style=\"height:20pt; width:26.8pt\"><p style=\"text-align:center\"><strong>No</strong></p></td>\n" +
				"\t\t<td style=\"height:20pt; width:175.7pt\"><p style=\"text-align:center\"><strong>Uraian Dokumen Harga sebagai berikut:</strong></p></td>\n" +
				"\t\t<td style=\"height:20pt; width:1.5in\"><p style=\"text-align:center\"><strong>Hasil Evaluasi</strong></p></td>\n" +
				"\t\t<td style=\"height:20pt; width:135.0pt\">"+
				"\t\t\t<p style=\"text-align:center\"><strong>Keterangan</strong></p>\n" +
				"\t\t\t</td></tr>\n";

		for (int i = 0; i < dok.namaHarga.length; i++){
			String na = dok.namaHarga[i];
			listEvaluasiHarga += "\t<tr>\n" +
					"\t\t<td style=\"height:3.5pt; width:27.2pt\"><p style=\"text-align:center\">"+(i+1)+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:137.0pt\"><p style=\"text-align:left\">"+na+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:74.85pt\"><p style=\"text-align:center\">"+dok.hasilPembuktianHarga[i]+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:207.9pt\"><p style=\"text-align:center\">"+dok.hasilHarga[i]+"</p></td>\n" +
					"\t</tr>\n";
		}

		listEvaluasiHarga += "</table>";

		return listEvaluasiHarga;
	}


	public static String generateEvaluationResultForTechnicalEvaluationDoc(DokumenHasilEvaluasi dok){
		String listEvaluasiTeknis = "<table align=\"center\" border=\"1\" cellspacing=\"0\" style=\"width:445.5pt\">\n" +
				"\t<tr>\n" +
				"\t\t<td style=\"height:28.1pt; width:26.8pt\"><p style=\"text-align:center\"><strong>No</strong></p></td>\n" +
				"\t\t<td style=\"height:28.1pt; width:175.7pt\"><p style=\"text-align:center\"><strong>Uraian Dokumen Teknis sebagai berikut:</strong></p></td>\n" +
				"\t\t<td style=\"height:28.1pt; width:1.5in\"><p style=\"text-align:center\"><strong>Hasil Evaluasi</strong></p></td>\n" +
				"\t\t<td style=\"height:28.1pt; vertical-align:top; width:135.0pt\"><p style=\"text-align:center\">&nbsp;</p>\n" +
				"\n" +
				"\t\t\t<p style=\"text-align:center\"><strong>Keterangan</strong></p>\n" +
				"\n" +
				"\t\t\t<p style=\"text-align:center\">&nbsp;</p>\n" +
				"\t\t\t</td>\n" +
				"\t\t</tr>\n";

		for (int i = 0; i < dok.namaTeknis.length; i++){
			String na = dok.namaTeknis[i];
			listEvaluasiTeknis += "\t<tr>\n" +
					"\t\t<td style=\"height:3.5pt; width:27.2pt\"><p style=\"text-align:center\">"+(i+1)+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:137.0pt\"><p style=\"text-align:left\">"+na+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:74.85pt\"><p style=\"text-align:center\">"+dok.hasilPembuktianTeknis[i]+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:207.9pt\"><p style=\"text-align:center\">"+dok.hasilTeknis[i]+"</p></td>\n" +
					"\t</tr>\n";
		}

		listEvaluasiTeknis += "</table>";

		return listEvaluasiTeknis;
	}

	public static String generateCatalogueTeamListForEvaluationDoc(DokumenHasilEvaluasi dok){
		String timPengembangKatalog = "\t<table align=\"center\" border=\"1\" cellspacing=\"0\">\n" +
				"\t\t<tr>\n" +
				"\t\t\t<td style=\"height:24.45pt; vertical-align:top; width:33.6pt\">\n" +
				"\t\t\t<p style=\"text-align:center\"><strong>No</strong></p>\n" +
				"\t\t\t</td>\n" +
				"\t\t\t<td style=\"height:24.45pt; vertical-align:top; width:142.95pt\">\n" +
				"\t\t\t<p style=\"text-align:center\"><strong>Nama</strong></p>\n" +
				"\t\t\t</td>\n" +
				"\t\t\t<td style=\"height:24.45pt; vertical-align:top; width:122.9pt\">\n" +
				"\t\t\t<p style=\"text-align:center\"><strong>Jabatan</strong></p>\n" +
				"\t\t\t</td>\n" +
				"\t\t\t<td style=\"height:24.45pt; vertical-align:top; width:111.0pt\">\n" +
				"\t\t\t<p style=\"text-align:center\"><strong>Tanda Tangan</strong></p>\n" +
				"\t\t\t</td>\n" +
				"\t\t</tr>\n";

		for(int i = 0; i < dok.namaTimKatalog.length; i++){
			String nt = dok.namaTimKatalog[i];
			timPengembangKatalog += "\t<tr>\n" +
					"\t\t<td style=\"height:3.5pt; width:27.2pt\"><p style=\"text-align:center\">"+(i+1)+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:137.0pt\"><p style=\"text-align:left\">"+nt+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:74.85pt\"><p style=\"text-align:center\">"+dok.jabatanTimKatalog[i]+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:207.9pt\"><p style=\"text-align:center\"></p></td>\n" +
					"\t</tr>\n";
		}

		timPengembangKatalog += "</table>";

		return timPengembangKatalog;
	}

	public static String generateProviderTeamListForEvaluationDoc(DokumenHasilEvaluasi dok){
		String ttdPenyedia = "\t<table align=\"center\" border=\"1\" cellspacing=\"0\">\n" +
				"\t\t<tr>\n" +
				"\t\t\t<td style=\"height:22.45pt; width:33.6pt\">\n" +
				"\t\t\t<p style=\"text-align:center\"><strong>No</strong></p>\n" +
				"\t\t\t</td>\n" +
				"\t\t\t<td style=\"height:22.45pt; width:142.95pt\">\n" +
				"\t\t\t<p style=\"text-align:center\"><strong>Nama</strong></p>\n" +
				"\t\t\t</td>\n" +
				"\t\t\t<td style=\"height:22.45pt; width:122.9pt\">\n" +
				"\t\t\t<p style=\"text-align:center\"><strong>Jabatan</strong></p>\n" +
				"\t\t\t</td>\n" +
				"\t\t\t<td style=\"height:22.45pt; width:111.0pt\">\n" +
				"\t\t\t<p style=\"text-align:center\"><strong>Tanda Tangan</strong></p>\n" +
				"\t\t\t</td>\n" +
				"\t\t</tr>\n";

		for(int i = 0; i < dok.namaTimPenyedia.length; i++){
			String nt = dok.namaTimPenyedia[i];
			ttdPenyedia += "\t<tr>\n" +
					"\t\t<td style=\"height:3.5pt; width:27.2pt\"><p style=\"text-align:center\">"+(i+1)+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:137.0pt\"><p style=\"text-align:left\">"+nt+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:74.85pt\"><p style=\"text-align:center\">"+dok.jabatanTimPenyedia[i]+"</p></td>\n" +
					"\t\t\t<td style=\"height:3.5pt; width:207.9pt\"><p style=\"text-align:center\"></p></td>\n" +
					"\t</tr>\n";
		}

		ttdPenyedia += "</table>";
		return ttdPenyedia;
	}

	public static JSONObject createJsonData(DokumenHasilEvaluasi dok) throws JSONException {

		JSONObject json = new JSONObject();
		json.put("nomorSurat", dok.nomorSurat);
		json.put("tanggalForm", dok.tanggalForm);

		JSONObject dokKualifikasi = new JSONObject();

		for(int i = 0; i < dok.namaKualifikasi.length; i++) {

			String key = dok.namaKualifikasi[i].replace(" ","_").toLowerCase();
			JSONObject kualifikasi = new JSONObject();
			kualifikasi.put("nama_dokumen",dok.namaKualifikasi[i]);
			kualifikasi.put("hasil_pembuktian",dok.hasilPembuktianKualifikasi[i]);
			kualifikasi.put("nilai_dokumen",dok.hasilKualifikasi[i]);
			dokKualifikasi.put(key,kualifikasi);

		}
		json.put("dokumen_kualifikasi",dokKualifikasi);

		if(dok.namaKualifikasiTambahan != null){
			JSONObject dokKualifikasiTambahan = new JSONObject();
			for (int j = 0; j < dok.namaKualifikasiTambahan.length; j++) {
				String key = dok.namaKualifikasiTambahan[j].replace(" ","_").toLowerCase();
				JSONObject kualifikasiTambahan = new JSONObject();
				kualifikasiTambahan.put("nama_dokumen",dok.namaKualifikasiTambahan[j]);
				kualifikasiTambahan.put("hasil_pembuktian",dok.hasilPembuktianKualifikasiTambahan[j]);
				kualifikasiTambahan.put("nilai_dokumen",dok.hasilKualifikasiTambahan[j]);
				dokKualifikasiTambahan.put(key,kualifikasiTambahan);
			}
			json.put("dokumen_kualifikasi_tambahan",dokKualifikasiTambahan);
		}

		JSONObject dokAdministrasi = new JSONObject();
		for (int i = 0; i < dok.namaAdministrasi.length; i++){
			String key = dok.namaAdministrasi[i].replace(" ","_").toLowerCase();
			JSONObject admin = new JSONObject();
			admin.put("nama_dokumen",dok.namaAdministrasi[i]);
			admin.put("hasil_pembuktian",dok.hasilPembuktianAdministrasi[i]);
			admin.put("nilai_dokumen",dok.hasilAdministrasi[i]);
			dokAdministrasi.put(key,admin);
		}
		json.put("dokumen_administrasi",dokAdministrasi);

		if(dok.namaDokAdmTambahan != null) {
			JSONArray dokAdministrasiTambahan = new JSONArray();
			for (int i = 0; i < dok.namaDokAdmTambahan.length; i++) {
				JSONObject dokadm = new JSONObject();
				dokadm.put("nama_dokumen", dok.namaDokAdmTambahan[i]);
				dokadm.put("hasil_pembuktian", dok.hasilPembuktianAdmTambahan[i]);
				dokadm.put("nilai_dokumen", dok.hasilDokAdmTambahan[i]);
				dokAdministrasiTambahan.put(dokadm);
			}
			json.put("dokumen_administrasi_tambahan", dokAdministrasiTambahan);
		}

		if(dok.namaHarga != null){
			JSONObject dokHarga = new JSONObject();
			for (int i = 0; i < dok.namaHarga.length; i++) {
				String key = dok.namaHarga[i].replace(" ","_").toLowerCase();
				JSONObject harga = new JSONObject();
				harga.put("nama_dokumen",dok.namaHarga[i]);
				harga.put("hasil_pembuktian",dok.hasilPembuktianHarga[i]);
				harga.put("nilai_dokumen",dok.hasilHarga[i]);

				dokHarga.put(key,harga);
			}
			json.put("dokumen_harga",dokHarga);

		}

		if(dok.namaTeknis != null) {
			JSONObject dokTeknis = new JSONObject();
			for (int i = 0; i < dok.namaTeknis.length; i++) {
				String key = dok.namaTeknis[i].replace(" ", "_").toLowerCase();
				JSONObject teknis = new JSONObject();
				teknis.put("nama_dokumen", dok.namaTeknis[i]);
				teknis.put("hasil_pembuktian", dok.hasilPembuktianTeknis[i]);
				teknis.put("nilai_dokumen", dok.hasilTeknis[i]);
				dokTeknis.put(key, teknis);
			}
			json.put("dokumen_teknis", dokTeknis);
		}

		JSONArray timKatalog = new JSONArray();
		for (int i = 0; i < dok.namaTimKatalog.length; i++){
			JSONObject pokja = new JSONObject();
			pokja.put("nama",dok.namaTimKatalog[i]);
			pokja.put("jabatan",dok.jabatanTimKatalog[i]);
			timKatalog.put(pokja);
		}
		json.put("tim_katalog",timKatalog);

		JSONArray timPenyedia = new JSONArray();
		for (int i = 0; i < dok.namaTimPenyedia.length; i++){
			JSONObject penyedia = new JSONObject();
			penyedia.put("nama",dok.namaTimPenyedia[i]);
			penyedia.put("jabatan",dok.jabatanTimPenyedia[i]);
			timPenyedia.put(penyedia);
		}
		json.put("tim_penyedia",timPenyedia);
		return json;




	}

}
