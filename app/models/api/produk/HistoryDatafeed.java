package models.api.produk;

import models.BaseKatalogTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

@Table(name = "history_datafeed")
public class HistoryDatafeed extends BaseKatalogTable {

	@Id
	public Long id;
	public Long penyedia_id;
	public String penyedia_name;
	public int total_product;
	public int total_inserted;
	public int total_updated;
	public int total_failed;
	public String message;
	public String start_time;
	public String end_time;
	public String id_job;

	public static String cleansingDataMsg(String msg){
		String dataCleans = "";

		dataCleans = msg.replaceAll("(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]", "");
		dataCleans = dataCleans.replaceAll("URL : ", "");

		return dataCleans;
	}

}
