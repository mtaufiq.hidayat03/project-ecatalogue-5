package functional;

import com.google.gson.reflect.TypeToken;
import models.katalog.Komoditas;
import models.masterdata.KldiSatker;
import models.penyedia.Penyedia;
import models.purchasing.Paket;
import models.purchasing.status.PaketStatus;
import models.user.*;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import play.mvc.Http;
import play.test.FunctionalTest;
import unit.JsonUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static models.jcommon.util.CommonUtil.fromJson;
import static models.jcommon.util.CommonUtil.toJson;

/**
 * @author HanusaCloud on 5/19/2020 10:09 AM
 */
public class BaseCase extends FunctionalTest {

    public static final String BASE_RESOURCE_PATH = "test/resources";

    public Map<Long, UserRoleGrup> userRoleGrupMap;
    public Map<Long, RoleItem> roleItemMap;

    public void setUp() throws IOException {
        cleanUpUsers();
        userRoleGrupMap = initUserRoleGroup("/json/user_role_grup.json");
        roleItemMap = initRoleItem("/json/role_item.json");
        initUsers("/json/users.json", userRoleGrupMap, roleItemMap);
    }

    public void cleanUpUsers() {
        UserRoleItemOverride.deleteAll();
        User.deleteAll();
        UserRoleGrup.deleteAll();
        UserRoleItem.deleteAll();
        RoleItem.deleteAll();
    }

    public static Http.Response login(String username, String password) {
        String body = URLEncodedUtils.format(
                Arrays.asList(
                        new BasicNameValuePair("username", username),
                        new BasicNameValuePair("password", password)
                ),
                "UTF-8");
        // act
        Http.Request request = newRequest();
        request.method = "POST";
        request.remoteAddress = "0.0.0.0";
        return POST(
                request,
                "/admin.userctr/loginlokalsubmit",
                APPLICATION_X_WWW_FORM_URLENCODED,
                body
        );
    }

    public static Http.Response loginAdmin() {
        return login("apriyoutomo", "123456");
    }

    public static  <T> T castObj(Object o, Class<T> type) {
        final String json = toJson(o);
        return fromJson(json, type);
    }

    public Map<Long, UserRoleGrup> initUserRoleGroup(String pathfile) throws IOException {
        List<UserRoleGrup> users = JsonUtils.getGson().fromJson(
                JsonUtils.get(pathfile),
                new TypeToken<List<UserRoleGrup>>() {
                }.getType()
        );
        Map<Long, UserRoleGrup> userRoleGrupMap = new HashMap<>();
        for (UserRoleGrup roleGrup : users) {
            userRoleGrupMap.put(roleGrup.id, roleGrup);
            roleGrup.id = null;
            roleGrup.id = Long.valueOf(roleGrup.save());
        }
        return userRoleGrupMap;
    }

    public Map<Long, User> initUsers(
            String pathfile,
            Map<Long, UserRoleGrup> userRoleGrupMap,
            Map<Long, RoleItem> roleItemMap
    ) throws IOException {
        List<User> users = JsonUtils.getGson().fromJson(
                JsonUtils.get(pathfile),
                new TypeToken<List<User>>() {
                }.getType()
        );
        Map<Long, User> userMap = new HashMap<>();
        for (User user : users) {
            userMap.put(user.id, user);
            user.id = null;
            if (userRoleGrupMap.containsKey(user.user_role_grup_id)) {
                user.user_role_grup_id = userRoleGrupMap.get(user.user_role_grup_id).id;
            }
            user.saveUser();
            if (user.override_role_grup && roleItemMap != null) {
                for (RoleItem roleItem : roleItemMap.values()) {
                    UserRoleItemOverride userRoleItem = new UserRoleItemOverride();
                    userRoleItem.user_id = user.id;
                    userRoleItem.role_item_id = roleItem.id;
                    userRoleItem.active = true;
                    userRoleItem.save();
                }
            }
        }
        return userMap;
    }

    public Map<Long, Penyedia> initProvider(String pathFile, Map<Long, User> userMap) throws IOException {
        List<Penyedia> penyedias = JsonUtils.getGson().fromJson(
                JsonUtils.get(pathFile),
                new TypeToken<List<Penyedia>>() {
                }.getType()
        );
        Map<Long, Penyedia> penyediaMap = new HashMap<>();
        for (Penyedia penyedia : penyedias) {

            penyediaMap.put(penyedia.id, penyedia);
            penyedia.id = null;
            if (userMap.containsKey(penyedia.user_id)) {
                penyedia.user_id = userMap.get(penyedia.user_id).id;
            }
            penyedia.active = true;
            penyedia.id = Long.valueOf(penyedia.save());
        }
        return penyediaMap;
    }

    public Map<Long, Komoditas> initCommodity(String filePath) throws IOException {
        List<Komoditas> komoditasList = JsonUtils.getGson().fromJson(
                JsonUtils.get(filePath),
                new TypeToken<List<Komoditas>>() {
                }.getType()
        );
        Map<Long, Komoditas> komoditasHashMap = new HashMap<>();
        for (Komoditas komoditas : komoditasList) {
            komoditasHashMap.put(komoditas.id, komoditas);
            komoditas.id = null;
            komoditas.id = Long.valueOf(komoditas.save());
        }
        return komoditasHashMap;
    }

    public Map<Long, KldiSatker> initKldiSatker(String filePath) throws IOException {
        List<KldiSatker> kldiSatkers = JsonUtils.getGson().fromJson(
                JsonUtils.get(filePath),
                new TypeToken<List<KldiSatker>>() {
                }.getType()
        );
        Map<Long, KldiSatker> satkerMap = new HashMap<>();
        for (KldiSatker kldiSatker : kldiSatkers) {
            satkerMap.put(kldiSatker.id, kldiSatker);
            kldiSatker.id = Long.valueOf(kldiSatker.save());
        }
        return satkerMap;
    }

    public Map<Long, Paket> initPaket(
            String filePath,
            Map<Long, Komoditas> commodityMap,
            Map<Long, Penyedia> providerMap,
            Map<Long, KldiSatker> kldiSatkerMap
    ) throws IOException {
        List<Paket> pakets = JsonUtils.getGson().fromJson(
                JsonUtils.get(filePath),
                new TypeToken<List<Paket>>() {
                }.getType()
        );
        Map<Long, Paket> paketMap = new HashMap<>();
        for (Paket paket : pakets) {
            paketMap.put(paket.id, paket);
            paket.id = null;
            if (commodityMap.containsKey(paket.komoditas_id.longValue())) {
                paket.komoditas_id = commodityMap.get(paket.komoditas_id.longValue()).id.intValue();
            }
            if (kldiSatkerMap.containsKey(Long.valueOf(paket.satker_id))) {
                paket.satker_id = kldiSatkerMap.get(Long.valueOf(paket.satker_id)).id.toString();
            }
            if (providerMap.containsKey(paket.penyedia_id)) {
                paket.penyedia_id = providerMap.get(paket.penyedia_id).id;
            }
            paket.id = Long.valueOf(paket.save());
        }
        return paketMap;
    }

    public Map<Long, PaketStatus> initPaketStatus(
            String filePath,
            Map<Long, Paket> paketMap
    ) throws IOException {
        List<PaketStatus> paketStatuses = JsonUtils.getGson().fromJson(
                JsonUtils.get(filePath),
                new TypeToken<List<PaketStatus>>() {
                }.getType()
        );
        Map<Long, PaketStatus> paketStatusMap = new HashMap<>();
        for (PaketStatus paketStatus : paketStatuses) {
            paketStatusMap.put(paketStatus.id, paketStatus);
            paketStatus.id = null;
            if (paketMap.containsKey(paketStatus.paket_id)) {
                paketStatus.paket_id = paketMap.get(paketStatus.paket_id).id;
            }
            paketStatus.id = Long.valueOf(paketStatus.save());
        }
        return paketStatusMap;
    }

    public Map<Long, RoleItem> initRoleItem(String filePath) throws IOException {
        List<RoleItem> roleItems = JsonUtils.getGson().fromJson(
                JsonUtils.get(filePath),
                new TypeToken<List<RoleItem>>() {
                }.getType()
        );
        Map<Long, RoleItem> roleItemMap = new HashMap<>();
        for (RoleItem roleItem : roleItems) {
            roleItemMap.put(roleItem.id, roleItem);
            roleItem.id = Long.valueOf(roleItem.save());
        }
        return roleItemMap;
    }

    public static String getContent(String path) {
        try {
            FileInputStream fisTargetFile = new FileInputStream(new File(path));
            return IOUtils.toString(fisTargetFile, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}
