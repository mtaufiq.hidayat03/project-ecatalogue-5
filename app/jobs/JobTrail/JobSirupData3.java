package jobs.JobTrail;

import jobs.extdata.SirupData;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import java.util.Date;

@NoTransaction
public class JobSirupData3 extends Job {
    int minuteWait = 0;
    Date tgl_update = new Date();
    public JobSirupData3(int _minuteWait, Date _tgl_update){
    minuteWait =  _minuteWait;
    tgl_update = _tgl_update;
    }
    public void doJob(){
        Logger.info("create JOb next 10 second");
        SirupData newobj = new SirupData();
        int newWait = minuteWait;
        newobj.getData(newWait++,tgl_update);
    }
}
