package models.util;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Created by dadang on 7/11/17.
 */
public class Encryption {

	private static final String DCEKey = "Y95YrM1551CpRb1w";
	private static final String INAPROCkey = "mysecretkey";

	public static String encrypt(byte[] bytes) {
		try {
			SecretKey key = new SecretKeySpec(DCEKey.getBytes(), "AES");
			//System.out.println(">>>>> " + java.util.Base64.getEncoder().encodeToString(key.getEncoded()));
			Cipher ecipher = Cipher.getInstance("AES");
			ecipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] utf8 = bytes;
			byte[] enc = ecipher.doFinal(utf8);
			String result = Base64.encodeBase64String(enc);
			utf8 = null;
			enc = null;

			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static String decrypt(String str) {
		try {
			SecretKey key = new SecretKeySpec(DCEKey.getBytes(), "AES");
			Cipher dcipher = Cipher.getInstance("AES");
			dcipher.init(Cipher.DECRYPT_MODE, key);
			byte[] dec = Base64.decodeBase64(str.getBytes("UTF-8"));
			byte[] utf8 = dcipher.doFinal(dec);
			String result = new String(utf8, "UTF-8");
			dec = null;
			utf8 = null;
			key = null;
			dcipher = null;

			return result;
		} catch (Exception e) {
			e.getMessage();
		}
		return null;
	}

	/**
	 * Melakukan encrypt input string untuk login ke Inaproc (salah satunya
	 * Inaproc)
	 *
	 * @param plaintext
	 */
	public static String encryptInaproc(String plaintext) {
		byte[] ciphertext;

		try {
			SecretKeySpec key = new SecretKeySpec(INAPROCkey.getBytes(), "Blowfish");
			Cipher cipher = Cipher.getInstance("Blowfish");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			ciphertext = cipher.doFinal(plaintext.getBytes());
		} catch (Exception e) {
			return null;
		}

		return Base64.encodeBase64URLSafeString(ciphertext);
	}

	/**
	 * Melakukan decrypt input string yang digunakan untuk login ke Inaproc
	 * (salah satunya TTS)
	 *
	 * @param ciphertext
	 */
	public static String decryptInaproc(String ciphertext) {
		byte[] plaintext;

		try {
			SecretKeySpec key = new SecretKeySpec(INAPROCkey.getBytes(), "Blowfish");
			Cipher plain = Cipher.getInstance("Blowfish");
			plain.init(Cipher.DECRYPT_MODE, key);
			plaintext = plain.doFinal(Base64.decodeBase64(ciphertext));
		} catch (Exception e) {
			return null;
		}

		return new String(plaintext);
	}

	public static String encodeUrlSafe(String value) {
		try {
			return URLEncoder.encode(value,StandardCharsets.UTF_8.toString());
		} catch (UnsupportedEncodingException ex) {
			throw new RuntimeException(ex.getCause());
		}
	}
	public static String decodeUrlSafe(String value) {
		try {
			return URLDecoder.decode(value, StandardCharsets.UTF_8.toString());
		} catch (UnsupportedEncodingException ex) {
			throw new RuntimeException(ex.getCause());
		}
	}
}
