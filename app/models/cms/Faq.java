package models.cms;

import ext.DatetimeBinder;
import models.BaseKatalogTable;
import models.common.AktifUser;
import org.jsoup.Jsoup;
import play.data.binding.As;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 10/4/2017.
 */
@Table(name = "faq")
public class Faq extends BaseKatalogTable {
    @Id
    public Long id;
    public Long konten_kategori_id;
    public String judul_konten;
    public String slug;
    public String isi_konten;
    public String status;
    public Boolean active;
    @Transient
    public String nama_kategori;
    @Transient
    public Long jumlah_konten;

    @As(binder = DatetimeBinder.class)
    public Date publish_date_from;
    @As(binder = DatetimeBinder.class)
    public Date publish_date_to;

    public static void deleteById(Long id){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String dateNow = dateFormat.format(date);

        String query = "update faq set active = 0, deleted_by = " + AktifUser.getActiveUserId() + ", deleted_date = '" + dateNow + "' where id = ?";
        Query.update(query,id);
    }

    public static List<Faq> findAllActive(){
        return Faq.find("active = 1").fetch();
    }

    public static List<Faq> findAllPublished(){
        return Faq.find("active = 1 and status = 'published' and publish_date_from <= now() and (publish_date_to is NULL or publish_date_to >= now())").fetch();
    }

    public static List<Faq> findAllByLimit(String keyword, String kategori, Long start, Long end){
        String nama_kategori = "", search = "";
        String query = "SELECT faq.id, faq.judul_konten, faq.slug, faq.isi_konten, faq.status, faq.active FROM faq left join konten_kategori kk ON kk.id = faq.konten_kategori_id " +
                "WHERE faq.active = 1 and faq.status = 'published' and faq.publish_date_from <= now() and (faq.publish_date_to is NULL or faq.publish_date_to >= now()) and kk.active = 1 ";

        if(kategori != null){
            nama_kategori = " and kk.nama_kategori = '" + kategori + "' ";
        }

        if(keyword != null){
            search = " and (faq.judul_konten like '%" + keyword + "%' or faq.isi_konten like '%" + keyword + "%') ";
        }

        if(start != null && end != null) {
            query += nama_kategori + search + " ORDER BY faq.created_date DESC LIMIT " + start + "," + end;
        }else{
            query += nama_kategori + search + " ORDER BY faq.created_date DESC ";
        }

        return Query.find(query,Faq.class).fetch();
    }

    public static Faq findDetailFaq(String slug){
        String query = "SELECT faq.*, kk.nama_kategori FROM faq LEFT JOIN konten_kategori kk ON kk.id = faq.konten_kategori_id WHERE faq.slug = '" + slug + "' ";
        return Query.find(query,Faq.class).first();
    }

    public static List<Faq> findAllCountFaq(){
        String query = "SELECT kk.id, kk.nama_kategori, COUNT(faq.konten_kategori_id) AS jumlah_konten FROM faq LEFT JOIN konten_kategori kk ON kk.id = faq.konten_kategori_id " +
                        "WHERE faq.active = 1 and faq.status = 'published' and faq.publish_date_from <= now() and (faq.publish_date_to is NULL or faq.publish_date_to >= now()) GROUP BY faq.konten_kategori_id";
        return Query.find(query,Faq.class).fetch();
    }

    public String getTanggal(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        if(publish_date_from != null){
            return sdf.format(publish_date_from);
        }
        return null;
    }

    public String getShortFaq(){
        int size = Jsoup.parse(isi_konten).text().length();
        if(isi_konten != null && size > 200){
            return Jsoup.parse(isi_konten).text().substring(0,200);
        }else if(isi_konten != null){
            return Jsoup.parse(isi_konten).text();
        }
        return null;
    }
}
