package controllers.admin.usulan;

import models.common.AktifUser;
import models.common.ButtonGroup;
import models.jcommon.datatable.DatatableResultsetHandler;
import models.prakatalog.Usulan;
import play.i18n.Messages;
import play.mvc.Router;
import services.datatable.DataTableService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 12/1/2017
 */
public class UsulanResultHandler {

    public final static DatatableResultsetHandler<String[]> DAFTAR_USULAN_POKJA=
            new DatatableResultsetHandler("usulan.id,k.nama_komoditas,usulan.nama_usulan,kldi.nama,usulan.status,usulan.komoditas_id,usulan.pengusul_nama,detail,edit,hapus,usulan.doc_review_id") {

                public String[] handle(ResultSet rs) throws SQLException {
                    String[] results = new String[columns.length];

                    Map<String, Object> param = new HashMap<String, Object>();
                    param.put("id",rs.getString("usulan.id"));

                    Map<String, Object> paramUsulanAndKomoditas = new HashMap<String, Object>();
                    paramUsulanAndKomoditas.put("id",rs.getString("usulan.id"));
                    paramUsulanAndKomoditas.put("kid",rs.getString("usulan.komoditas_id"));

                    String urlDetail = Router.reverse("controllers.admin.UsulanCtr.detail",param).url;
                    String urlBukaPenawaran = Router.reverse("controllers.admin.UsulanCtr.bukaPenawaran",param).url;

                    results[0] = rs.getString("usulan.id");
                    results[1] = rs.getString("k.nama_komoditas");
                    results[2] = rs.getString("usulan.nama_usulan");
                    results[3] = rs.getString("kldi.nama");
                    results[4] = Usulan.statusMaps().get(rs.getString("usulan.status"));

                    List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();

                    buttonGroups.add(new ButtonGroup(Messages.get("detail.label"),urlDetail));
                    String status = rs.getString("usulan.status");

                    if(status.equals("baru")){
                        buttonGroups.add(new ButtonGroup(Messages.get("buat.label")+" "+Messages.get("pengumuman.label"),urlBukaPenawaran));
                    }

                    Map<String, Object> paramrev = new HashMap<String, Object>();
                    paramrev.put("usulan_id",rs.getString("usulan.id"));

                    String docReviewid = rs.getString("usulan.doc_review_id");
                    Boolean belumReview = docReviewid==null;
                    String urlReviewUsulan = Router.reverse("controllers.admin.UsulanCtr.reviewUsulan",paramrev).url;
                    String urlDetilReviewUsulan = Router.reverse("controllers.admin.UsulanCtr.DetailReviewUsulan",new HashMap<String, Object>()).url;
                    if(belumReview ){
                        //ada acl disini
                        if(AktifUser.getAktifUser().isAuthorized("comUsulanReview")){
                            buttonGroups.add(new ButtonGroup(Messages.get("review_usulan.label"), urlReviewUsulan));
                        }else{
                            //memberikan info no akses ACL
                            //buttonGroups.add(new ButtonGroup(Messages.get("review_usulan.label")+"(Acl)", "#"));
                        }
                    }else{
                        buttonGroups.add(new ButtonGroup(Messages.get("detail_review_usulan.label"), urlDetail));
                    }
                    results[5] = DataTableService.generateButtonGroup(buttonGroups);
                    return results;
                }
            };

    public final static DatatableResultsetHandler<String[]> DAFTAR_USULAN=
            new DatatableResultsetHandler("usulan.id,usulan.komoditas_id,usulan.nama_usulan,usulan.status,usulan.pengusul_nama,kldi.nama,detail,edit,hapus") {

                public String[] handle(ResultSet rs) throws SQLException {
                    String[] results = new String[columns.length];

                    Map<String, Object> param = new HashMap<String, Object>();
                    param.put("id",rs.getString("usulan.id"));

                    Map<String, Object> paramUsulanAndKomoditas = new HashMap<String, Object>();
                    paramUsulanAndKomoditas.put("id",rs.getString("usulan.id"));
                    paramUsulanAndKomoditas.put("kid",rs.getString("usulan.komoditas_id"));

                    String urlDetail = Router.reverse("controllers.admin.UsulanCtr.detail",param).url;
                    String urlBukaPenawaran = Router.reverse("controllers.admin.UsulanCtr.bukaPenawaran",param).url;
                    String urlEdit = Router.reverse("controllers.admin.UsulanCtr.edit",paramUsulanAndKomoditas).url;
                    String urlHapus = Router.reverse("controllers.admin.UsulanCtr.delete",paramUsulanAndKomoditas).url;
                    String urlListPendaftar = Router.reverse("controllers.admin.UsulanCtr.listPendaftar",paramUsulanAndKomoditas).url;

                    results[0] = rs.getString("usulan.id");
                    results[1] = rs.getString("usulan.nama_usulan");
                    results[2] = rs.getString("usulan.pengusul_nama");
                    results[3] = rs.getString("kldi.nama");
                    results[4] = Usulan.statusMaps().get(rs.getString("usulan.status"));

                    List<ButtonGroup> buttonGroups = new ArrayList<ButtonGroup>();

                    buttonGroups.add(new ButtonGroup(Messages.get("detail_usulan.label"),urlDetail));
                        buttonGroups.add(new ButtonGroup("List Pendaftar",urlListPendaftar));

                    String status = rs.getString("usulan.status");

                    if(status.equals("baru")){
                        buttonGroups.add(new ButtonGroup(Messages.get("buat.label")+" "+Messages.get("pengumuman.label"),urlBukaPenawaran));
                        buttonGroups.add(new ButtonGroup(Messages.get("ubah.label")+" "+Messages.get("usulan.label"),urlEdit));
                        buttonGroups.add(new ButtonGroup(Messages.get("hapus.label")+" "+Messages.get("usulan.label"),urlHapus));
                    }

                    results[5] = DataTableService.generateButtonGroup(buttonGroups);

                    return results;
                }
            };


}
